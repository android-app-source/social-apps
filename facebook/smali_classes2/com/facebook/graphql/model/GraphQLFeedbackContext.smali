.class public final Lcom/facebook/graphql/model/GraphQLFeedbackContext;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackContext$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackContext$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

.field public h:I

.field public i:I

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262082
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262083
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262080
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262081
    return-void
.end method

.method public constructor <init>(LX/4WK;)V
    .locals 1

    .prologue
    .line 262067
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262068
    iget-object v0, p1, LX/4WK;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 262069
    iget-object v0, p1, LX/4WK;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->f:Ljava/util/List;

    .line 262070
    iget-object v0, p1, LX/4WK;->d:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->g:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 262071
    iget v0, p1, LX/4WK;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->h:I

    .line 262072
    iget v0, p1, LX/4WK;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->i:I

    .line 262073
    iget-object v0, p1, LX/4WK;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n:Ljava/util/List;

    .line 262074
    iget-object v0, p1, LX/4WK;->h:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 262075
    iget-object v0, p1, LX/4WK;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j:Ljava/util/List;

    .line 262076
    iget-object v0, p1, LX/4WK;->j:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 262077
    iget-object v0, p1, LX/4WK;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l:Ljava/util/List;

    .line 262078
    iget-object v0, p1, LX/4WK;->l:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    .line 262079
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 262043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262044
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 262045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 262046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 262047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->p()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 262048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 262049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->r()LX/0Px;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/util/List;)I

    move-result v6

    .line 262050
    const/16 v7, 0xb

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 262051
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 262052
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 262053
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k()Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    if-ne v0, v7, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262054
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l()I

    move-result v2

    invoke-virtual {p1, v0, v2, v8}, LX/186;->a(III)V

    .line 262055
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m()I

    move-result v2

    invoke-virtual {p1, v0, v2, v8}, LX/186;->a(III)V

    .line 262056
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 262057
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o()Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    if-ne v0, v3, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262058
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 262059
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 262060
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 262061
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->s()Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    if-ne v2, v3, :cond_2

    :goto_2
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262063
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 262064
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k()Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    move-result-object v0

    goto :goto_0

    .line 262065
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o()Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-result-object v0

    goto :goto_1

    .line 262066
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->s()Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 262015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 262018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 262019
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 262020
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 262021
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 262023
    if-eqz v2, :cond_1

    .line 262024
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 262025
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->f:Ljava/util/List;

    move-object v1, v0

    .line 262026
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 262027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 262028
    if-eqz v2, :cond_2

    .line 262029
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 262030
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j:Ljava/util/List;

    move-object v1, v0

    .line 262031
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 262032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 262033
    if-eqz v2, :cond_3

    .line 262034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 262035
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l:Ljava/util/List;

    move-object v1, v0

    .line 262036
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 262037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    .line 262038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 262039
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 262040
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    .line 262041
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262042
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262012
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262013
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 262014
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 262008
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 262009
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->h:I

    .line 262010
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->i:I

    .line 262011
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 262007
    const v0, -0x498575b6

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262084
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262085
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->f:Ljava/util/List;

    .line 262086
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262004
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->g:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262005
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->g:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->g:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    .line 262006
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->g:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    return-object v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262001
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262002
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262003
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->h:I

    return v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261998
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261999
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262000
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->i:I

    return v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261995
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261996
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j:Ljava/util/List;

    .line 261997
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261992
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261993
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 261994
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261989
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261990
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l:Ljava/util/List;

    .line 261991
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261986
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261987
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    .line 261988
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->m:Lcom/facebook/graphql/model/GraphQLRelevantReactorsConnection;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261980
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261981
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n:Ljava/util/List;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n:Ljava/util/List;

    .line 261982
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261983
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261984
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    .line 261985
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o:Lcom/facebook/graphql/enums/GraphQLInlineCommentsInteractionLikelihood;

    return-object v0
.end method
