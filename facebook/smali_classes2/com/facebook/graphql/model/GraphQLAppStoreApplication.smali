.class public final Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAppStoreApplication$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAppStoreApplication$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301366
    const-class v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301367
    const-class v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 301368
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301369
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 301370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301371
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 301372
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 301373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 301374
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 301375
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 301376
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 301377
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q()LX/0Px;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/util/List;)I

    move-result v6

    .line 301378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r()LX/0Px;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 301379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 301380
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 301381
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->u()LX/0Px;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->d(Ljava/util/List;)I

    move-result v10

    .line 301382
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    invoke-static {p1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 301383
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->x()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p1, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 301384
    const/16 v13, 0x10

    invoke-virtual {p1, v13}, LX/186;->c(I)V

    .line 301385
    const/4 v13, 0x0

    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 301386
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 301387
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 301388
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 301389
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 301390
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 301391
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 301392
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 301393
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 301394
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 301395
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 301396
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 301397
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 301398
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 301399
    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->w()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 301400
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 301401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301402
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 301403
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v0

    goto :goto_0

    .line 301404
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 301405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 301407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 301408
    if-eqz v1, :cond_6

    .line 301409
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 301410
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->g:Ljava/util/List;

    move-object v1, v0

    .line 301411
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 301414
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 301415
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301416
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 301417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 301419
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 301420
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301421
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 301422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 301423
    if-eqz v2, :cond_2

    .line 301424
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 301425
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n:Ljava/util/List;

    move-object v1, v0

    .line 301426
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 301427
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 301428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 301429
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 301430
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 301431
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 301432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301433
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 301434
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 301435
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301436
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301437
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301438
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301439
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->e:Ljava/lang/String;

    .line 301440
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 301441
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 301442
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s:I

    .line 301443
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 301444
    const v0, 0x4ac531d0    # 6461672.0f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301445
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301446
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->f:Ljava/lang/String;

    .line 301447
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301448
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301449
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->g:Ljava/util/List;

    .line 301450
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301360
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301361
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301362
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301363
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->i:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301364
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->i:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->i:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    .line 301365
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->i:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301327
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301328
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j:Ljava/lang/String;

    .line 301329
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301333
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301334
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 301335
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301336
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301337
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301338
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final q()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301339
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301340
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m:Ljava/util/List;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m:Ljava/util/List;

    .line 301341
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301342
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301343
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n:Ljava/util/List;

    .line 301344
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301330
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301331
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o:Lcom/facebook/graphql/model/GraphQLApplication;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 301332
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301345
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301346
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p:Ljava/lang/String;

    .line 301347
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301348
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301349
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q:Ljava/util/List;

    .line 301350
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301351
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301352
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301353
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final w()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301354
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301355
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301356
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s:I

    return v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301357
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301358
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t:Ljava/lang/String;

    .line 301359
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t:Ljava/lang/String;

    return-object v0
.end method
