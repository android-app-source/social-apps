.class public final Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251331
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251332
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 251333
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 251334
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x4af7005f    # 8093743.5f

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 251335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->u:LX/0x2;

    .line 251336
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251337
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251338
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251339
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251340
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251341
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->t:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->t:Ljava/lang/String;

    .line 251342
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->t:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 251343
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->l:I

    .line 251344
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251345
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251346
    if-eqz v0, :cond_0

    .line 251347
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 251348
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 251349
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->j:Ljava/lang/String;

    .line 251350
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251351
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251352
    if-eqz v0, :cond_0

    .line 251353
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 251354
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 251355
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->k:Ljava/lang/String;

    .line 251356
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251357
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251358
    if-eqz v0, :cond_0

    .line 251359
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 251360
    :cond_0
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251361
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251362
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->j:Ljava/lang/String;

    .line 251363
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251364
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251365
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->k:Ljava/lang/String;

    .line 251366
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private v()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251367
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 251368
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 251369
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->l:I

    return v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251370
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251371
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->m:Ljava/lang/String;

    .line 251372
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251373
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251374
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251375
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251376
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251377
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->q:Ljava/lang/String;

    .line 251378
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251379
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251380
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->r:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->r:Ljava/lang/String;

    .line 251381
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->r:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 251382
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251383
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251384
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->h:Ljava/lang/String;

    .line 251385
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251386
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 251387
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 251388
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->i:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 251389
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 251327
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 251328
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->u:LX/0x2;

    if-nez v0, :cond_0

    .line 251329
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->u:LX/0x2;

    .line 251330
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->u:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 251217
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 251295
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 251296
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 251263
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251264
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 251265
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 251266
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->E_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 251267
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 251268
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 251269
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->w()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 251270
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 251271
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 251272
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 251273
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->y()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 251274
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->z()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 251275
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 251276
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->B()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 251277
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 251278
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 251279
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 251280
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 251281
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 251282
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 251283
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 251284
    const/4 v2, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->v()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 251285
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 251286
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 251287
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 251288
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 251289
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 251290
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 251291
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 251292
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 251293
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251294
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 251240
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251242
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    .line 251243
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 251244
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    .line 251245
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    .line 251246
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 251247
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251248
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 251249
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    .line 251250
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251251
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 251252
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 251254
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    .line 251255
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251256
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 251257
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251258
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 251259
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    .line 251260
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251261
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251262
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 251239
    new-instance v0, LX/4Wp;

    invoke-direct {v0, p1}, LX/4Wp;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251238
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 251236
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->i:J

    .line 251237
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 251232
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 251233
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->i:J

    .line 251234
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->l:I

    .line 251235
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 251218
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251219
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251220
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251221
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 251222
    :goto_0
    return-void

    .line 251223
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251224
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251225
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251226
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 251227
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251228
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 251229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 251230
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 251231
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 251320
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251321
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->a(Ljava/lang/String;)V

    .line 251322
    :cond_0
    :goto_0
    return-void

    .line 251323
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251324
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 251325
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251326
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251297
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251298
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251299
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o:Ljava/lang/String;

    .line 251300
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251301
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251302
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 251303
    :goto_0
    return-object v0

    .line 251304
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 251305
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 251306
    const v0, 0x4af7005f    # 8093743.5f

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251307
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251308
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->g:Ljava/lang/String;

    .line 251309
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 251310
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->I_()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16d;

    invoke-static {v0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 251311
    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251312
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251313
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    .line 251314
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251315
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251316
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251317
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 251318
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 251319
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
