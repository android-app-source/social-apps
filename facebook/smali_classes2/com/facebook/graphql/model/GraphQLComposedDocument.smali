.class public final Lcom/facebook/graphql/model/GraphQLComposedDocument;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLComposedDocument$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLComposedDocument$Serializer;
.end annotation


# instance fields
.field public e:J

.field public f:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public i:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:J

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326430
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedDocument$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326429
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedDocument$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 326427
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 326428
    return-void
.end method

.method private j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326424
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326425
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326426
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->e:J

    return-wide v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326350
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326351
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 326352
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326422
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 326423
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326418
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326419
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 326420
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326415
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->i:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326416
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->i:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->i:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    .line 326417
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->i:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326412
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326413
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->j:Ljava/lang/String;

    .line 326414
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326409
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326410
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326411
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->k:J

    return-wide v0
.end method

.method private q()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326406
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326407
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326408
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->l:J

    return-wide v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326403
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326404
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->m:Ljava/lang/String;

    .line 326405
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326301
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->n:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326302
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->n:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->n:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326303
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->n:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326304
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->o:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326305
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->o:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->o:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326306
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->o:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326307
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->p:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326308
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->p:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->p:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326309
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->p:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326310
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->q:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326311
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->q:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->q:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326312
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->q:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326313
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->r:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326314
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->r:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->r:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326315
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->r:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326316
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326317
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326318
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 326319
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326320
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 326321
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 326322
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 326323
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->r()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 326324
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 326325
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->t()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 326326
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->u()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 326327
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->v()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 326328
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->w()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 326329
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->x()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 326330
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 326331
    const/4 v3, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 326332
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 326333
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 326334
    const/4 v3, 0x4

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->m()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 326335
    const/4 v3, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->n()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 326336
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 326337
    const/4 v3, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->p()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 326338
    const/16 v3, 0x8

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->q()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 326339
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 326340
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 326341
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 326342
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 326343
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 326344
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326345
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326346
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326347
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 326348
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->m()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    goto :goto_0

    .line 326349
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->n()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 326353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326354
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326355
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326356
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 326357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326358
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->n:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326359
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->t()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 326360
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->t()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326361
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->t()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 326362
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326363
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->o:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326364
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->u()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 326365
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->u()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326366
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->u()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 326367
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326368
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->p:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326369
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->v()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 326370
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->v()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326371
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->v()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 326372
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326373
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->q:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326374
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 326375
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 326376
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 326377
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326378
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->f:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 326379
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->w()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 326380
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->w()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326381
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->w()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 326382
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326383
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->r:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326384
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->x()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 326385
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->x()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326386
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->x()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 326387
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326388
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->s:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 326389
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 326390
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 326391
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 326392
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 326393
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComposedDocument;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 326394
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326395
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    :cond_8
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326396
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLComposedDocument;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 326397
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 326398
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->e:J

    .line 326399
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->k:J

    .line 326400
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComposedDocument;->l:J

    .line 326401
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 326402
    const v0, 0x3fc1086d

    return v0
.end method
