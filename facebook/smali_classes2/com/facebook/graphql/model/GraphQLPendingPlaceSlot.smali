.class public final Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325638
    const-class v0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325639
    const-class v0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 325636
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325637
    return-void
.end method

.method public constructor <init>(LX/4Xt;)V
    .locals 1

    .prologue
    .line 325630
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325631
    iget-object v0, p1, LX/4Xt;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->e:Ljava/lang/String;

    .line 325632
    iget-object v0, p1, LX/4Xt;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->f:Ljava/util/List;

    .line 325633
    iget-object v0, p1, LX/4Xt;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->g:Ljava/lang/String;

    .line 325634
    iget-object v0, p1, LX/4Xt;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->h:Ljava/lang/String;

    .line 325635
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325627
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325628
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->h:Ljava/lang/String;

    .line 325629
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 325640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 325642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 325643
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 325644
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 325645
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 325646
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 325647
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 325648
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 325649
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 325650
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325651
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 325619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 325621
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 325622
    if-eqz v1, :cond_0

    .line 325623
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    .line 325624
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->f:Ljava/util/List;

    .line 325625
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325626
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 325617
    const v0, 0x77940fee

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325614
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325615
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->e:Ljava/lang/String;

    .line 325616
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325608
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325609
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->f:Ljava/util/List;

    .line 325610
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325611
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325612
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->g:Ljava/lang/String;

    .line 325613
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->g:Ljava/lang/String;

    return-object v0
.end method
