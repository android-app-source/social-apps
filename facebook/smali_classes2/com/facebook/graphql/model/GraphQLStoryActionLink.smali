.class public final Lcom/facebook/graphql/model/GraphQLStoryActionLink;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryActionLink$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryActionLink$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLDocumentElement;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLLeadGenData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

.field public aB:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

.field public aD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Z

.field public aK:Z

.field public aL:Z

.field public aM:Z

.field public aN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/model/GraphQLTopic;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

.field public ab:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public ae:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ah:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:I

.field public bA:I

.field public bB:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bC:Z

.field public bD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bG:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bK:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMisinformationAction;",
            ">;"
        }
    .end annotation
.end field

.field public ba:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bf:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bg:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bm:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public br:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

.field public bu:Lcom/facebook/graphql/model/GraphQLOffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bv:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bx:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTarotDigest;",
            ">;"
        }
    .end annotation
.end field

.field public by:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductionPrompt;",
            ">;"
        }
    .end annotation
.end field

.field public bz:Z

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

.field public g:Lcom/facebook/graphql/model/GraphQLAd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public m:I

.field public n:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Z

.field public q:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLCoupon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:J

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 203324
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 203325
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 203326
    const/16 v0, 0x8b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 203327
    return-void
.end method

.method public constructor <init>(LX/4Ys;)V
    .locals 2

    .prologue
    .line 203328
    const/16 v0, 0x8b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 203329
    iget-object v0, p1, LX/4Ys;->b:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 203330
    iget-object v0, p1, LX/4Ys;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK:Ljava/util/List;

    .line 203331
    iget-object v0, p1, LX/4Ys;->d:Lcom/facebook/graphql/model/GraphQLAd;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->g:Lcom/facebook/graphql/model/GraphQLAd;

    .line 203332
    iget-object v0, p1, LX/4Ys;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->h:Ljava/lang/String;

    .line 203333
    iget-object v0, p1, LX/4Ys;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->i:Ljava/lang/String;

    .line 203334
    iget-object v0, p1, LX/4Ys;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 203335
    iget-object v0, p1, LX/4Ys;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD:Ljava/lang/String;

    .line 203336
    iget-object v0, p1, LX/4Ys;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j:Ljava/lang/String;

    .line 203337
    iget-object v0, p1, LX/4Ys;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k:Ljava/lang/String;

    .line 203338
    iget v0, p1, LX/4Ys;->k:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l:I

    .line 203339
    iget v0, p1, LX/4Ys;->l:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->m:I

    .line 203340
    iget-object v0, p1, LX/4Ys;->m:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203341
    iget-object v0, p1, LX/4Ys;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 203342
    iget v0, p1, LX/4Ys;->o:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bA:I

    .line 203343
    iget-boolean v0, p1, LX/4Ys;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->o:Z

    .line 203344
    iget-boolean v0, p1, LX/4Ys;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->p:Z

    .line 203345
    iget-object v0, p1, LX/4Ys;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bl:Ljava/lang/String;

    .line 203346
    iget-object v0, p1, LX/4Ys;->s:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 203347
    iget-object v0, p1, LX/4Ys;->t:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 203348
    iget-object v0, p1, LX/4Ys;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf:Ljava/lang/String;

    .line 203349
    iget-object v0, p1, LX/4Ys;->v:Lcom/facebook/graphql/model/GraphQLCoupon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r:Lcom/facebook/graphql/model/GraphQLCoupon;

    .line 203350
    iget-object v0, p1, LX/4Ys;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE:Ljava/lang/String;

    .line 203351
    iget-wide v0, p1, LX/4Ys;->x:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->s:J

    .line 203352
    iget-object v0, p1, LX/4Ys;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203353
    iget-object v0, p1, LX/4Ys;->z:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 203354
    iget-object v0, p1, LX/4Ys;->A:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx:Ljava/util/List;

    .line 203355
    iget-object v0, p1, LX/4Ys;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw:Ljava/lang/String;

    .line 203356
    iget-object v0, p1, LX/4Ys;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v:Ljava/lang/String;

    .line 203357
    iget-object v0, p1, LX/4Ys;->D:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w:Ljava/lang/String;

    .line 203358
    iget-object v0, p1, LX/4Ys;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF:Ljava/lang/String;

    .line 203359
    iget-object v0, p1, LX/4Ys;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG:Ljava/lang/String;

    .line 203360
    iget-object v0, p1, LX/4Ys;->G:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 203361
    iget-object v0, p1, LX/4Ys;->H:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x:Ljava/util/List;

    .line 203362
    iget-object v0, p1, LX/4Ys;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y:Ljava/lang/String;

    .line 203363
    iget-object v0, p1, LX/4Ys;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->z:Ljava/lang/String;

    .line 203364
    iget-object v0, p1, LX/4Ys;->K:Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 203365
    iget-object v0, p1, LX/4Ys;->L:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B:Ljava/lang/String;

    .line 203366
    iget-object v0, p1, LX/4Ys;->M:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C:Ljava/lang/String;

    .line 203367
    iget-object v0, p1, LX/4Ys;->N:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 203368
    iget-object v0, p1, LX/4Ys;->O:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 203369
    iget-object v0, p1, LX/4Ys;->P:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F:Ljava/lang/String;

    .line 203370
    iget-object v0, p1, LX/4Ys;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G:Ljava/lang/String;

    .line 203371
    iget-object v0, p1, LX/4Ys;->R:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H:Ljava/util/List;

    .line 203372
    iget-object v0, p1, LX/4Ys;->S:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I:Ljava/lang/String;

    .line 203373
    iget-object v0, p1, LX/4Ys;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J:Ljava/lang/String;

    .line 203374
    iget-object v0, p1, LX/4Ys;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 203375
    iget-object v0, p1, LX/4Ys;->V:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    .line 203376
    iget-object v0, p1, LX/4Ys;->W:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M:Ljava/lang/String;

    .line 203377
    iget-object v0, p1, LX/4Ys;->X:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB:Ljava/util/List;

    .line 203378
    iget-object v0, p1, LX/4Ys;->Y:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N:Ljava/util/List;

    .line 203379
    iget-object v0, p1, LX/4Ys;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203380
    iget-object v0, p1, LX/4Ys;->aa:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 203381
    iget-object v0, p1, LX/4Ys;->ab:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by:Ljava/util/List;

    .line 203382
    iget-object v0, p1, LX/4Ys;->ac:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 203383
    iget-boolean v0, p1, LX/4Ys;->ad:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz:Z

    .line 203384
    iget-boolean v0, p1, LX/4Ys;->ae:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC:Z

    .line 203385
    iget-object v0, p1, LX/4Ys;->af:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q:Lcom/facebook/graphql/model/GraphQLNode;

    .line 203386
    iget-object v0, p1, LX/4Ys;->ag:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R:Ljava/lang/String;

    .line 203387
    iget-object v0, p1, LX/4Ys;->ah:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S:Ljava/lang/String;

    .line 203388
    iget-object v0, p1, LX/4Ys;->ai:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 203389
    iget-object v0, p1, LX/4Ys;->aj:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U:Ljava/lang/String;

    .line 203390
    iget-object v0, p1, LX/4Ys;->ak:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 203391
    iget-object v0, p1, LX/4Ys;->al:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 203392
    iget-object v0, p1, LX/4Ys;->am:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X:Ljava/lang/String;

    .line 203393
    iget-object v0, p1, LX/4Ys;->an:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y:Ljava/lang/String;

    .line 203394
    iget-object v0, p1, LX/4Ys;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203395
    iget-object v0, p1, LX/4Ys;->ap:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 203396
    iget-object v0, p1, LX/4Ys;->aq:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 203397
    iget-object v0, p1, LX/4Ys;->ar:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac:Ljava/lang/String;

    .line 203398
    iget-object v0, p1, LX/4Ys;->as:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 203399
    iget-object v0, p1, LX/4Ys;->at:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203400
    iget-object v0, p1, LX/4Ys;->au:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af:Ljava/lang/String;

    .line 203401
    iget-object v0, p1, LX/4Ys;->av:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag:Ljava/util/List;

    .line 203402
    iget-object v0, p1, LX/4Ys;->aw:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah:Ljava/lang/String;

    .line 203403
    iget-object v0, p1, LX/4Ys;->ax:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 203404
    iget-object v0, p1, LX/4Ys;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203405
    iget-object v0, p1, LX/4Ys;->az:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd:Ljava/lang/String;

    .line 203406
    iget-object v0, p1, LX/4Ys;->aA:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 203407
    iget-object v0, p1, LX/4Ys;->aB:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be:Ljava/util/List;

    .line 203408
    iget-object v0, p1, LX/4Ys;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203409
    iget-object v0, p1, LX/4Ys;->aD:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al:Ljava/lang/String;

    .line 203410
    iget-object v0, p1, LX/4Ys;->aE:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp:Ljava/lang/String;

    .line 203411
    iget-object v0, p1, LX/4Ys;->aF:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am:Ljava/lang/String;

    .line 203412
    iget-object v0, p1, LX/4Ys;->aG:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an:Ljava/lang/String;

    .line 203413
    iget-object v0, p1, LX/4Ys;->aH:Lcom/facebook/graphql/model/GraphQLOffer;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 203414
    iget-object v0, p1, LX/4Ys;->aI:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao:Lcom/facebook/graphql/model/GraphQLPage;

    .line 203415
    iget-object v0, p1, LX/4Ys;->aJ:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    .line 203416
    iget-object v0, p1, LX/4Ys;->aK:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq:Lcom/facebook/graphql/model/GraphQLStory;

    .line 203417
    iget-object v0, p1, LX/4Ys;->aL:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 203418
    iget-object v0, p1, LX/4Ys;->aM:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ar:Ljava/lang/String;

    .line 203419
    iget-object v0, p1, LX/4Ys;->aN:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as:Ljava/lang/String;

    .line 203420
    iget-object v0, p1, LX/4Ys;->aO:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 203421
    iget-object v0, p1, LX/4Ys;->aP:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au:Ljava/lang/String;

    .line 203422
    iget-object v0, p1, LX/4Ys;->aQ:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 203423
    iget-object v0, p1, LX/4Ys;->aR:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw:Ljava/lang/String;

    .line 203424
    iget-object v0, p1, LX/4Ys;->aS:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 203425
    iget-object v0, p1, LX/4Ys;->aT:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax:Ljava/lang/String;

    .line 203426
    iget-object v0, p1, LX/4Ys;->aU:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay:Ljava/lang/String;

    .line 203427
    iget v0, p1, LX/4Ys;->aV:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az:I

    .line 203428
    iget-object v0, p1, LX/4Ys;->aW:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh:Ljava/lang/String;

    .line 203429
    iget-object v0, p1, LX/4Ys;->aX:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi:Ljava/lang/String;

    .line 203430
    iget-object v0, p1, LX/4Ys;->aY:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aA:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 203431
    iget-object v0, p1, LX/4Ys;->aZ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH:Ljava/lang/String;

    .line 203432
    iget-object v0, p1, LX/4Ys;->ba:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI:Ljava/lang/String;

    .line 203433
    iget-object v0, p1, LX/4Ys;->bb:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 203434
    iget-object v0, p1, LX/4Ys;->bc:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 203435
    iget-object v0, p1, LX/4Ys;->bd:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD:Ljava/lang/String;

    .line 203436
    iget-object v0, p1, LX/4Ys;->be:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE:Ljava/lang/String;

    .line 203437
    iget-object v0, p1, LX/4Ys;->bf:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs:Ljava/lang/String;

    .line 203438
    iget-object v0, p1, LX/4Ys;->bg:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF:Ljava/lang/String;

    .line 203439
    iget-object v0, p1, LX/4Ys;->bh:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG:Ljava/lang/String;

    .line 203440
    iget-object v0, p1, LX/4Ys;->bi:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH:Ljava/lang/String;

    .line 203441
    iget-object v0, p1, LX/4Ys;->bj:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI:Ljava/lang/String;

    .line 203442
    iget-boolean v0, p1, LX/4Ys;->bk:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aJ:Z

    .line 203443
    iget-boolean v0, p1, LX/4Ys;->bl:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aK:Z

    .line 203444
    iget-boolean v0, p1, LX/4Ys;->bm:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aL:Z

    .line 203445
    iget-boolean v0, p1, LX/4Ys;->bn:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aM:Z

    .line 203446
    iget-object v0, p1, LX/4Ys;->bo:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN:Ljava/lang/String;

    .line 203447
    iget-object v0, p1, LX/4Ys;->bp:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO:Ljava/lang/String;

    .line 203448
    iget-object v0, p1, LX/4Ys;->bq:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP:Ljava/lang/String;

    .line 203449
    iget-object v0, p1, LX/4Ys;->br:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 203450
    iget-object v0, p1, LX/4Ys;->bs:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR:Lcom/facebook/graphql/model/GraphQLStory;

    .line 203451
    iget-object v0, p1, LX/4Ys;->bt:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS:Ljava/lang/String;

    .line 203452
    iget-object v0, p1, LX/4Ys;->bu:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bJ:Ljava/lang/String;

    .line 203453
    iget-object v0, p1, LX/4Ys;->bv:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn:Lcom/facebook/graphql/model/GraphQLNode;

    .line 203454
    iget-object v0, p1, LX/4Ys;->bw:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    .line 203455
    iget-object v0, p1, LX/4Ys;->bx:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 203456
    iget-object v0, p1, LX/4Ys;->by:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj:Ljava/lang/String;

    .line 203457
    iget-object v0, p1, LX/4Ys;->bz:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV:Ljava/lang/String;

    .line 203458
    iget-object v0, p1, LX/4Ys;->bA:Lcom/facebook/graphql/model/GraphQLTopic;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW:Lcom/facebook/graphql/model/GraphQLTopic;

    .line 203459
    iget-object v0, p1, LX/4Ys;->bB:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX:Ljava/lang/String;

    .line 203460
    iget-object v0, p1, LX/4Ys;->bC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203461
    iget-object v0, p1, LX/4Ys;->bD:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY:Ljava/lang/String;

    .line 203462
    iget-object v0, p1, LX/4Ys;->bE:Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 203463
    iget-object v0, p1, LX/4Ys;->bF:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba:Ljava/util/List;

    .line 203464
    iget-object v0, p1, LX/4Ys;->bG:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 203465
    iget-object v0, p1, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 203466
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203467
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203468
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w:Ljava/lang/String;

    .line 203469
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final B()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203470
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203471
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x:Ljava/util/List;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x:Ljava/util/List;

    .line 203472
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203473
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203474
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y:Ljava/lang/String;

    .line 203475
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203318
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203319
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->z:Ljava/lang/String;

    .line 203320
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203479
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A:Lcom/facebook/graphql/model/GraphQLEvent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203480
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A:Lcom/facebook/graphql/model/GraphQLEvent;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 203481
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A:Lcom/facebook/graphql/model/GraphQLEvent;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203482
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203483
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B:Ljava/lang/String;

    .line 203484
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203485
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203486
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C:Ljava/lang/String;

    .line 203487
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLDocumentElement;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203488
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203489
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 203490
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203491
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203492
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 203493
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final J()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203494
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203495
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F:Ljava/lang/String;

    .line 203496
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203497
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203498
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G:Ljava/lang/String;

    .line 203499
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final L()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203500
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203501
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H:Ljava/util/List;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H:Ljava/util/List;

    .line 203502
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202763
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202764
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I:Ljava/lang/String;

    .line 202765
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202736
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202737
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J:Ljava/lang/String;

    .line 202738
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202739
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202740
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 202741
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202742
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202743
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    .line 202744
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202745
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202746
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M:Ljava/lang/String;

    .line 202747
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final R()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202748
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202749
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N:Ljava/util/List;

    const/16 v1, 0x23

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N:Ljava/util/List;

    .line 202750
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final S()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202751
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202752
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 202753
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    return-object v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202754
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202755
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 202756
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202757
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202758
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q:Lcom/facebook/graphql/model/GraphQLNode;

    .line 202759
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202760
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202761
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R:Ljava/lang/String;

    .line 202762
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R:Ljava/lang/String;

    return-object v0
.end method

.method public final W()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202733
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202734
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S:Ljava/lang/String;

    .line 202735
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S:Ljava/lang/String;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/model/GraphQLLeadGenData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202766
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202767
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 202768
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    return-object v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202769
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202770
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U:Ljava/lang/String;

    .line 202771
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202772
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202773
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 202774
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 121

    .prologue
    .line 202775
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 202776
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 202777
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 202778
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 202779
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->m()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 202780
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 202781
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 202782
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 202783
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 202784
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 202785
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 202786
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->z()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 202787
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 202788
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v14

    .line 202789
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 202790
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 202791
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 202792
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 202793
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 202794
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 202795
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 202796
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 202797
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 202798
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L()LX/0Px;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v24

    .line 202799
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 202800
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 202801
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 202802
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P()Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 202803
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 202804
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R()LX/0Px;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v30

    .line 202805
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 202806
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 202807
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 202808
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 202809
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 202810
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 202811
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 202812
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 202813
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 202814
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 202815
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v41

    .line 202816
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 202817
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 202818
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 202819
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 202820
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj()Ljava/lang/String;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v46

    .line 202821
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak()LX/0Px;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v47

    .line 202822
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 202823
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 202824
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 202825
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 202826
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v52

    .line 202827
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 202828
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ar()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v54

    .line 202829
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 202830
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at()Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 202831
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 202832
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av()Ljava/lang/String;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v58

    .line 202833
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw()Ljava/lang/String;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v59

    .line 202834
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 202835
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay()Ljava/lang/String;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v61

    .line 202836
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 202837
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aA()Ljava/lang/String;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v63

    .line 202838
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB()Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v64

    .line 202839
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v65

    .line 202840
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 202841
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH()Ljava/lang/String;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v67

    .line 202842
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI()Ljava/lang/String;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v68

    .line 202843
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aJ()Ljava/lang/String;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v69

    .line 202844
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aK()Ljava/lang/String;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v70

    .line 202845
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aL()Ljava/lang/String;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v71

    .line 202846
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aM()Ljava/lang/String;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v72

    .line 202847
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR()Ljava/lang/String;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v73

    .line 202848
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS()Ljava/lang/String;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v74

    .line 202849
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT()Ljava/lang/String;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v75

    .line 202850
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 202851
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v77

    .line 202852
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW()Ljava/lang/String;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v78

    .line 202853
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX()Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 202854
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v80

    .line 202855
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v81

    move-object/from16 v0, p1

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v81

    .line 202856
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba()Lcom/facebook/graphql/model/GraphQLTopic;

    move-result-object v82

    move-object/from16 v0, p1

    move-object/from16 v1, v82

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v82

    .line 202857
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb()Ljava/lang/String;

    move-result-object v83

    move-object/from16 v0, p1

    move-object/from16 v1, v83

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v83

    .line 202858
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v84

    move-object/from16 v0, p1

    move-object/from16 v1, v84

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v84

    .line 202859
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v85

    move-object/from16 v0, p1

    move-object/from16 v1, v85

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v85

    .line 202860
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be()LX/0Px;

    move-result-object v86

    move-object/from16 v0, p1

    move-object/from16 v1, v86

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v86

    .line 202861
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v87

    move-object/from16 v0, p1

    move-object/from16 v1, v87

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v87

    .line 202862
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v88

    move-object/from16 v0, p1

    move-object/from16 v1, v88

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v88

    .line 202863
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh()Ljava/lang/String;

    move-result-object v89

    move-object/from16 v0, p1

    move-object/from16 v1, v89

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v89

    .line 202864
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi()LX/0Px;

    move-result-object v90

    move-object/from16 v0, p1

    move-object/from16 v1, v90

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v90

    .line 202865
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj()Ljava/lang/String;

    move-result-object v91

    move-object/from16 v0, p1

    move-object/from16 v1, v91

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v91

    .line 202866
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v92

    move-object/from16 v0, p1

    move-object/from16 v1, v92

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v92

    .line 202867
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bl()Ljava/lang/String;

    move-result-object v93

    move-object/from16 v0, p1

    move-object/from16 v1, v93

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v93

    .line 202868
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm()Ljava/lang/String;

    move-result-object v94

    move-object/from16 v0, p1

    move-object/from16 v1, v94

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v94

    .line 202869
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn()Ljava/lang/String;

    move-result-object v95

    move-object/from16 v0, p1

    move-object/from16 v1, v95

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v95

    .line 202870
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v96

    move-object/from16 v0, p1

    move-object/from16 v1, v96

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v96

    .line 202871
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp()Ljava/lang/String;

    move-result-object v97

    move-object/from16 v0, p1

    move-object/from16 v1, v97

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v97

    .line 202872
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v98

    move-object/from16 v0, p1

    move-object/from16 v1, v98

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v98

    .line 202873
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v99

    move-object/from16 v0, p1

    move-object/from16 v1, v99

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v99

    .line 202874
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v100

    move-object/from16 v0, p1

    move-object/from16 v1, v100

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v100

    .line 202875
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt()Ljava/lang/String;

    move-result-object v101

    move-object/from16 v0, p1

    move-object/from16 v1, v101

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v101

    .line 202876
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v102

    move-object/from16 v0, p1

    move-object/from16 v1, v102

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v102

    .line 202877
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v103

    move-object/from16 v0, p1

    move-object/from16 v1, v103

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v103

    .line 202878
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw()Ljava/lang/String;

    move-result-object v104

    move-object/from16 v0, p1

    move-object/from16 v1, v104

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v104

    .line 202879
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v105

    move-object/from16 v0, p1

    move-object/from16 v1, v105

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v105

    .line 202880
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v106

    move-object/from16 v0, p1

    move-object/from16 v1, v106

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v106

    .line 202881
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bA()Ljava/lang/String;

    move-result-object v107

    move-object/from16 v0, p1

    move-object/from16 v1, v107

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v107

    .line 202882
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB()LX/0Px;

    move-result-object v108

    move-object/from16 v0, p1

    move-object/from16 v1, v108

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v108

    .line 202883
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC()LX/0Px;

    move-result-object v109

    move-object/from16 v0, p1

    move-object/from16 v1, v109

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v109

    .line 202884
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF()LX/0Px;

    move-result-object v110

    move-object/from16 v0, p1

    move-object/from16 v1, v110

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v110

    .line 202885
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH()Ljava/lang/String;

    move-result-object v111

    move-object/from16 v0, p1

    move-object/from16 v1, v111

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v111

    .line 202886
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI()Ljava/lang/String;

    move-result-object v112

    move-object/from16 v0, p1

    move-object/from16 v1, v112

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v112

    .line 202887
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bJ()Ljava/lang/String;

    move-result-object v113

    move-object/from16 v0, p1

    move-object/from16 v1, v113

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v113

    .line 202888
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK()Ljava/lang/String;

    move-result-object v114

    move-object/from16 v0, p1

    move-object/from16 v1, v114

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v114

    .line 202889
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bL()Ljava/lang/String;

    move-result-object v115

    move-object/from16 v0, p1

    move-object/from16 v1, v115

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v115

    .line 202890
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bM()Ljava/lang/String;

    move-result-object v116

    move-object/from16 v0, p1

    move-object/from16 v1, v116

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v116

    .line 202891
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bN()Ljava/lang/String;

    move-result-object v117

    move-object/from16 v0, p1

    move-object/from16 v1, v117

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v117

    .line 202892
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v118

    move-object/from16 v0, p1

    move-object/from16 v1, v118

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v118

    .line 202893
    const/16 v119, 0x8a

    move-object/from16 v0, p1

    move/from16 v1, v119

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 202894
    const/16 v119, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v119

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 202895
    const/16 v119, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v2

    sget-object v120, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-object/from16 v0, v120

    if-ne v2, v0, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    move/from16 v1, v119

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 202896
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 202897
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 202898
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 202899
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 202900
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 202901
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->p()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 202902
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 202903
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 202904
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 202905
    const/16 v2, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 202906
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 202907
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 202908
    const/16 v3, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 202909
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 202910
    const/16 v3, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 202911
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 202912
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 202913
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 202914
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 202915
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202916
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202917
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202918
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202919
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202920
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202921
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202922
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202923
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202924
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202925
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202926
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202927
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202928
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202929
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202930
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202931
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202932
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202933
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202934
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202935
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202936
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202937
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202938
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202939
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202940
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202941
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202942
    const/16 v3, 0x30

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 202943
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202944
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202945
    const/16 v3, 0x33

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 202946
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202947
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202948
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202949
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202950
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202951
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202952
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202953
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202954
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202955
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202956
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202957
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202958
    const/16 v2, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202959
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202960
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202961
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202962
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202963
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202964
    const/16 v2, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202965
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202966
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202967
    const/16 v2, 0x49

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 202968
    const/16 v3, 0x4a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE()Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 202969
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202970
    const/16 v3, 0x4c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 202971
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202972
    const/16 v2, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202973
    const/16 v2, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202974
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202975
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202976
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202977
    const/16 v2, 0x53

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 202978
    const/16 v2, 0x54

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 202979
    const/16 v2, 0x55

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 202980
    const/16 v2, 0x56

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 202981
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202982
    const/16 v2, 0x58

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202983
    const/16 v2, 0x59

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202984
    const/16 v2, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202985
    const/16 v2, 0x5b

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202986
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202987
    const/16 v2, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202988
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202989
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202990
    const/16 v2, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202991
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v83

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202992
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v84

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202993
    const/16 v2, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v85

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202994
    const/16 v2, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v86

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202995
    const/16 v2, 0x65

    move-object/from16 v0, p1

    move/from16 v1, v87

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202996
    const/16 v2, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v88

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202997
    const/16 v2, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v89

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202998
    const/16 v2, 0x68

    move-object/from16 v0, p1

    move/from16 v1, v90

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 202999
    const/16 v2, 0x69

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203000
    const/16 v2, 0x6a

    move-object/from16 v0, p1

    move/from16 v1, v92

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203001
    const/16 v2, 0x6b

    move-object/from16 v0, p1

    move/from16 v1, v93

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203002
    const/16 v2, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v94

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203003
    const/16 v2, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v95

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203004
    const/16 v2, 0x6f

    move-object/from16 v0, p1

    move/from16 v1, v96

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203005
    const/16 v2, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v97

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203006
    const/16 v2, 0x71

    move-object/from16 v0, p1

    move/from16 v1, v98

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203007
    const/16 v2, 0x72

    move-object/from16 v0, p1

    move/from16 v1, v99

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203008
    const/16 v2, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v100

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203009
    const/16 v2, 0x74

    move-object/from16 v0, p1

    move/from16 v1, v101

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203010
    const/16 v2, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v102

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203011
    const/16 v2, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v103

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203012
    const/16 v2, 0x77

    move-object/from16 v0, p1

    move/from16 v1, v104

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203013
    const/16 v3, 0x78

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 203014
    const/16 v2, 0x79

    move-object/from16 v0, p1

    move/from16 v1, v105

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203015
    const/16 v2, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v106

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203016
    const/16 v2, 0x7b

    move-object/from16 v0, p1

    move/from16 v1, v107

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203017
    const/16 v2, 0x7c

    move-object/from16 v0, p1

    move/from16 v1, v108

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203018
    const/16 v2, 0x7d

    move-object/from16 v0, p1

    move/from16 v1, v109

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203019
    const/16 v2, 0x7e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 203020
    const/16 v2, 0x7f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 203021
    const/16 v2, 0x80

    move-object/from16 v0, p1

    move/from16 v1, v110

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203022
    const/16 v2, 0x81

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 203023
    const/16 v2, 0x82

    move-object/from16 v0, p1

    move/from16 v1, v111

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203024
    const/16 v2, 0x83

    move-object/from16 v0, p1

    move/from16 v1, v112

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203025
    const/16 v2, 0x84

    move-object/from16 v0, p1

    move/from16 v1, v113

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203026
    const/16 v2, 0x85

    move-object/from16 v0, p1

    move/from16 v1, v114

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203027
    const/16 v2, 0x86

    move-object/from16 v0, p1

    move/from16 v1, v115

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203028
    const/16 v2, 0x87

    move-object/from16 v0, p1

    move/from16 v1, v116

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203029
    const/16 v2, 0x88

    move-object/from16 v0, p1

    move/from16 v1, v117

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203030
    const/16 v2, 0x89

    move-object/from16 v0, p1

    move/from16 v1, v118

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 203031
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 203032
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 203033
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 203034
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v2

    goto/16 :goto_1

    .line 203035
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v2

    goto/16 :goto_2

    .line 203036
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v2

    goto/16 :goto_3

    .line 203037
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    goto/16 :goto_4

    .line 203038
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE()Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    move-result-object v2

    goto/16 :goto_5

    .line 203039
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v2

    goto/16 :goto_6

    .line 203040
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v2

    goto/16 :goto_7
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 203041
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 203042
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_32

    .line 203043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 203044
    if-eqz v1, :cond_32

    .line 203045
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203046
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK:Ljava/util/List;

    move-object v1, v0

    .line 203047
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAd;

    .line 203049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k()Lcom/facebook/graphql/model/GraphQLAd;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 203050
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203051
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->g:Lcom/facebook/graphql/model/GraphQLAd;

    .line 203052
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 203053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 203054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 203055
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203056
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 203057
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 203058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 203059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 203060
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203061
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203062
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 203063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 203064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 203065
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203066
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 203067
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 203068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 203069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 203070
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203071
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 203072
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 203073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 203074
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 203075
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203076
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 203077
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 203078
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCoupon;

    .line 203079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 203080
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203081
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r:Lcom/facebook/graphql/model/GraphQLCoupon;

    .line 203082
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 203083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 203085
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203086
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203087
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 203088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 203089
    if-eqz v2, :cond_8

    .line 203090
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203091
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx:Ljava/util/List;

    move-object v1, v0

    .line 203092
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 203093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 203094
    if-eqz v2, :cond_9

    .line 203095
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203096
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x:Ljava/util/List;

    move-object v1, v0

    .line 203097
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 203098
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 203099
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 203100
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203101
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->A:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 203102
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 203103
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 203104
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->H()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 203105
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203106
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 203107
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 203108
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 203109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 203110
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203111
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 203112
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 203113
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 203114
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 203115
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203116
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 203117
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P()Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 203118
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P()Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    .line 203119
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P()Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 203120
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203121
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L:Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    .line 203122
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 203123
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 203124
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 203125
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203126
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203127
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 203128
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 203129
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->S()Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 203130
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203131
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O:Lcom/facebook/graphql/model/GraphQLOverlayCallToActionInfo;

    .line 203132
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 203133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 203134
    if-eqz v2, :cond_11

    .line 203135
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203136
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by:Ljava/util/List;

    move-object v1, v0

    .line 203137
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 203138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 203139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 203140
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203141
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 203142
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 203143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 203144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 203145
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203146
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q:Lcom/facebook/graphql/model/GraphQLNode;

    .line 203147
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 203148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 203149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 203150
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203151
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->T:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 203152
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 203153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 203154
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 203155
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203156
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->V:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 203157
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 203158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 203159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 203160
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203161
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 203162
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 203163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 203164
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 203165
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203166
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203167
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 203168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 203169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 203170
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203171
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 203172
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 203173
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 203174
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 203175
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203176
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203177
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 203178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 203179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 203180
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203181
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 203182
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 203183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 203185
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203186
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203187
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 203188
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 203189
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 203190
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203191
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 203192
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 203193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 203195
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203196
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203197
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 203198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOffer;

    .line 203199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 203200
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203201
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 203202
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 203203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 203204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 203205
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203206
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao:Lcom/facebook/graphql/model/GraphQLPage;

    .line 203207
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at()Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 203208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at()Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    .line 203209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at()Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 203210
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203211
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    .line 203212
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 203213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203214
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 203215
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203216
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq:Lcom/facebook/graphql/model/GraphQLStory;

    .line 203217
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 203218
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 203219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 203220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203221
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 203222
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 203223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 203224
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 203225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203226
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 203227
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 203228
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 203229
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 203230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203231
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 203232
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 203233
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 203234
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 203235
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203236
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 203237
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 203238
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 203239
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 203240
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203241
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 203242
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 203243
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 203244
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 203245
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203246
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 203247
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 203248
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 203250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203251
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR:Lcom/facebook/graphql/model/GraphQLStory;

    .line 203252
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 203253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 203254
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 203255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203256
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn:Lcom/facebook/graphql/model/GraphQLNode;

    .line 203257
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX()Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 203258
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX()Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    .line 203259
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX()Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 203260
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203261
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    .line 203262
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 203263
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 203264
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 203265
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203266
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 203267
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba()Lcom/facebook/graphql/model/GraphQLTopic;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 203268
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba()Lcom/facebook/graphql/model/GraphQLTopic;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopic;

    .line 203269
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba()Lcom/facebook/graphql/model/GraphQLTopic;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 203270
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203271
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW:Lcom/facebook/graphql/model/GraphQLTopic;

    .line 203272
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 203273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203274
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 203275
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203276
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 203277
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 203278
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 203279
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 203280
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203281
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 203282
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 203283
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 203284
    if-eqz v2, :cond_2f

    .line 203285
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203286
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba:Ljava/util/List;

    move-object v1, v0

    .line 203287
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 203288
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 203289
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 203290
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 203291
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 203292
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 203293
    if-nez v1, :cond_31

    :goto_1
    return-object p0

    :cond_31
    move-object p0, v1

    goto :goto_1

    :cond_32
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203294
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 203295
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 203296
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 203297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 203298
    const/4 v0, 0x0

    .line 203299
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 203300
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 203301
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l:I

    .line 203302
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->m:I

    .line 203303
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->o:Z

    .line 203304
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->p:Z

    .line 203305
    const/16 v0, 0xe

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->s:J

    .line 203306
    const/16 v0, 0x49

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az:I

    .line 203307
    const/16 v0, 0x53

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aJ:Z

    .line 203308
    const/16 v0, 0x54

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aK:Z

    .line 203309
    const/16 v0, 0x55

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aL:Z

    .line 203310
    const/16 v0, 0x56

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aM:Z

    .line 203311
    const/16 v0, 0x7e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz:Z

    .line 203312
    const/16 v0, 0x7f

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bA:I

    .line 203313
    const/16 v0, 0x81

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC:Z

    .line 203314
    return-void
.end method

.method public final aA()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203315
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203316
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw:Ljava/lang/String;

    const/16 v1, 0x46

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw:Ljava/lang/String;

    .line 203317
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw:Ljava/lang/String;

    return-object v0
.end method

.method public final aB()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203563
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203564
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax:Ljava/lang/String;

    const/16 v1, 0x47

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax:Ljava/lang/String;

    .line 203565
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ax:Ljava/lang/String;

    return-object v0
.end method

.method public final aC()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203530
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203531
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay:Ljava/lang/String;

    const/16 v1, 0x48

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay:Ljava/lang/String;

    .line 203532
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay:Ljava/lang/String;

    return-object v0
.end method

.method public final aD()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203566
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203567
    const/16 v0, 0x9

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203568
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az:I

    return v0
.end method

.method public final aE()Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203569
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aA:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203570
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aA:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aA:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    .line 203571
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aA:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    return-object v0
.end method

.method public final aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203572
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203573
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 203574
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aB:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    return-object v0
.end method

.method public final aG()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203575
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203576
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    const/16 v1, 0x4c

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 203577
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    return-object v0
.end method

.method public final aH()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203578
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203579
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD:Ljava/lang/String;

    const/16 v1, 0x4d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD:Ljava/lang/String;

    .line 203580
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aD:Ljava/lang/String;

    return-object v0
.end method

.method public final aI()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203581
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203582
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE:Ljava/lang/String;

    const/16 v1, 0x4e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE:Ljava/lang/String;

    .line 203583
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE:Ljava/lang/String;

    return-object v0
.end method

.method public final aJ()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203584
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203585
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF:Ljava/lang/String;

    const/16 v1, 0x4f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF:Ljava/lang/String;

    .line 203586
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF:Ljava/lang/String;

    return-object v0
.end method

.method public final aK()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203608
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203609
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG:Ljava/lang/String;

    const/16 v1, 0x50

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG:Ljava/lang/String;

    .line 203610
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG:Ljava/lang/String;

    return-object v0
.end method

.method public final aL()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203587
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203588
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH:Ljava/lang/String;

    const/16 v1, 0x51

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH:Ljava/lang/String;

    .line 203589
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH:Ljava/lang/String;

    return-object v0
.end method

.method public final aM()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203590
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203591
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI:Ljava/lang/String;

    const/16 v1, 0x52

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI:Ljava/lang/String;

    .line 203592
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI:Ljava/lang/String;

    return-object v0
.end method

.method public final aN()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203593
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203594
    const/16 v0, 0xa

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203595
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aJ:Z

    return v0
.end method

.method public final aO()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203596
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203597
    const/16 v0, 0xa

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203598
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aK:Z

    return v0
.end method

.method public final aP()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203599
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203600
    const/16 v0, 0xa

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203601
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aL:Z

    return v0
.end method

.method public final aQ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203602
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 203603
    const/16 v0, 0xa

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 203604
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aM:Z

    return v0
.end method

.method public final aR()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203605
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203606
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN:Ljava/lang/String;

    const/16 v1, 0x57

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN:Ljava/lang/String;

    .line 203607
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aN:Ljava/lang/String;

    return-object v0
.end method

.method public final aS()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203557
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203558
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO:Ljava/lang/String;

    const/16 v1, 0x58

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO:Ljava/lang/String;

    .line 203559
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aO:Ljava/lang/String;

    return-object v0
.end method

.method public final aT()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203560
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203561
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP:Ljava/lang/String;

    const/16 v1, 0x59

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP:Ljava/lang/String;

    .line 203562
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aP:Ljava/lang/String;

    return-object v0
.end method

.method public final aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203506
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203507
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 203508
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aQ:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    return-object v0
.end method

.method public final aV()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203509
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203510
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x5b

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR:Lcom/facebook/graphql/model/GraphQLStory;

    .line 203511
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final aW()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203512
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203513
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS:Ljava/lang/String;

    const/16 v1, 0x5c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS:Ljava/lang/String;

    .line 203514
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS:Ljava/lang/String;

    return-object v0
.end method

.method public final aX()Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203515
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203516
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    const/16 v1, 0x5d

    const-class v2, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    .line 203517
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aT:Lcom/facebook/graphql/model/GraphQLAppendPostActionLinkTaggedAndMentionedUsersConnection;

    return-object v0
.end method

.method public final aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203518
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203519
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    .line 203520
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU:Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    return-object v0
.end method

.method public final aZ()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203521
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203522
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV:Ljava/lang/String;

    const/16 v1, 0x5f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV:Ljava/lang/String;

    .line 203523
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV:Ljava/lang/String;

    return-object v0
.end method

.method public final aa()Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203524
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203525
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    .line 203526
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->W:Lcom/facebook/graphql/model/GraphQLLeadGenUserStatus;

    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203527
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203528
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X:Ljava/lang/String;

    const/16 v1, 0x2d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X:Ljava/lang/String;

    .line 203529
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X:Ljava/lang/String;

    return-object v0
.end method

.method public final ac()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203503
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203504
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y:Ljava/lang/String;

    const/16 v1, 0x2e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y:Ljava/lang/String;

    .line 203505
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public final ad()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203533
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203534
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203535
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203536
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203537
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    .line 203538
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aa:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    return-object v0
.end method

.method public final af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203539
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203540
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    .line 203541
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab:Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    return-object v0
.end method

.method public final ag()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203542
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203543
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac:Ljava/lang/String;

    const/16 v1, 0x32

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac:Ljava/lang/String;

    .line 203544
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac:Ljava/lang/String;

    return-object v0
.end method

.method public final ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 203545
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203546
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 203547
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ad:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203548
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203549
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 203550
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aj()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203551
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203552
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af:Ljava/lang/String;

    const/16 v1, 0x35

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af:Ljava/lang/String;

    .line 203553
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af:Ljava/lang/String;

    return-object v0
.end method

.method public final ak()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203554
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203555
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag:Ljava/util/List;

    const/16 v1, 0x36

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag:Ljava/util/List;

    .line 203556
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final al()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203476
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203477
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah:Ljava/lang/String;

    const/16 v1, 0x37

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah:Ljava/lang/String;

    .line 203478
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah:Ljava/lang/String;

    return-object v0
.end method

.method public final am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203321
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 203322
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    .line 203323
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ai:Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    return-object v0
.end method

.method public final an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202570
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202571
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x39

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202572
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202576
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202577
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202578
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ap()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202579
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202580
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al:Ljava/lang/String;

    const/16 v1, 0x3b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al:Ljava/lang/String;

    .line 202581
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al:Ljava/lang/String;

    return-object v0
.end method

.method public final aq()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202582
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202583
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am:Ljava/lang/String;

    const/16 v1, 0x3c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am:Ljava/lang/String;

    .line 202584
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am:Ljava/lang/String;

    return-object v0
.end method

.method public final ar()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202585
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202586
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an:Ljava/lang/String;

    const/16 v1, 0x3d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an:Ljava/lang/String;

    .line 202587
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an:Ljava/lang/String;

    return-object v0
.end method

.method public final as()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202588
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202589
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao:Lcom/facebook/graphql/model/GraphQLPage;

    .line 202590
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ao:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final at()Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202591
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202592
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    .line 202593
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ap:Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    return-object v0
.end method

.method public final au()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202594
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202595
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq:Lcom/facebook/graphql/model/GraphQLStory;

    .line 202596
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aq:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final av()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202597
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ar:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202598
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ar:Ljava/lang/String;

    const/16 v1, 0x41

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ar:Ljava/lang/String;

    .line 202599
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ar:Ljava/lang/String;

    return-object v0
.end method

.method public final aw()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202573
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202574
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as:Ljava/lang/String;

    const/16 v1, 0x42

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as:Ljava/lang/String;

    .line 202575
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as:Ljava/lang/String;

    return-object v0
.end method

.method public final ax()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202603
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202604
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 202605
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->at:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final ay()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202606
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202607
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au:Ljava/lang/String;

    const/16 v1, 0x44

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au:Ljava/lang/String;

    .line 202608
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->au:Ljava/lang/String;

    return-object v0
.end method

.method public final az()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202609
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202610
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 202611
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final bA()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202612
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202613
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw:Ljava/lang/String;

    const/16 v1, 0x7b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw:Ljava/lang/String;

    .line 202614
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw:Ljava/lang/String;

    return-object v0
.end method

.method public final bB()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTarotDigest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202615
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202616
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx:Ljava/util/List;

    const/16 v1, 0x7c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx:Ljava/util/List;

    .line 202617
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bC()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductionPrompt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202618
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202619
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by:Ljava/util/List;

    const/16 v1, 0x7d

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by:Ljava/util/List;

    .line 202620
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bD()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202621
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202622
    const/16 v0, 0xf

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202623
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bz:Z

    return v0
.end method

.method public final bE()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202624
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202625
    const/16 v0, 0xf

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202626
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bA:I

    return v0
.end method

.method public final bF()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202600
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202601
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB:Ljava/util/List;

    const/16 v1, 0x80

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB:Ljava/util/List;

    .line 202602
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bB:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bG()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202519
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202520
    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202521
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC:Z

    return v0
.end method

.method public final bH()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202525
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202526
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD:Ljava/lang/String;

    const/16 v1, 0x82

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD:Ljava/lang/String;

    .line 202527
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bD:Ljava/lang/String;

    return-object v0
.end method

.method public final bI()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202528
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202529
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE:Ljava/lang/String;

    const/16 v1, 0x83

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE:Ljava/lang/String;

    .line 202530
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE:Ljava/lang/String;

    return-object v0
.end method

.method public final bJ()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202531
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202532
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF:Ljava/lang/String;

    const/16 v1, 0x84

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF:Ljava/lang/String;

    .line 202533
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF:Ljava/lang/String;

    return-object v0
.end method

.method public final bK()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202534
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202535
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG:Ljava/lang/String;

    const/16 v1, 0x85

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG:Ljava/lang/String;

    .line 202536
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG:Ljava/lang/String;

    return-object v0
.end method

.method public final bL()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202537
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202538
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH:Ljava/lang/String;

    const/16 v1, 0x86

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH:Ljava/lang/String;

    .line 202539
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH:Ljava/lang/String;

    return-object v0
.end method

.method public final bM()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202540
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202541
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI:Ljava/lang/String;

    const/16 v1, 0x87

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI:Ljava/lang/String;

    .line 202542
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI:Ljava/lang/String;

    return-object v0
.end method

.method public final bN()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202543
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bJ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202544
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bJ:Ljava/lang/String;

    const/16 v1, 0x88

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bJ:Ljava/lang/String;

    .line 202545
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bJ:Ljava/lang/String;

    return-object v0
.end method

.method public final bO()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMisinformationAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202546
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202547
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK:Ljava/util/List;

    const/16 v1, 0x89

    const-class v2, Lcom/facebook/graphql/model/GraphQLMisinformationAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK:Ljava/util/List;

    .line 202548
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bK:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final ba()Lcom/facebook/graphql/model/GraphQLTopic;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202549
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW:Lcom/facebook/graphql/model/GraphQLTopic;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202550
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW:Lcom/facebook/graphql/model/GraphQLTopic;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/model/GraphQLTopic;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopic;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW:Lcom/facebook/graphql/model/GraphQLTopic;

    .line 202551
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aW:Lcom/facebook/graphql/model/GraphQLTopic;

    return-object v0
.end method

.method public final bb()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202552
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202553
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX:Ljava/lang/String;

    const/16 v1, 0x61

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX:Ljava/lang/String;

    .line 202554
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aX:Ljava/lang/String;

    return-object v0
.end method

.method public final bc()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202555
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202556
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY:Ljava/lang/String;

    const/16 v1, 0x62

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY:Ljava/lang/String;

    .line 202557
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY:Ljava/lang/String;

    return-object v0
.end method

.method public final bd()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202558
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202559
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ:Lcom/facebook/graphql/model/GraphQLVideo;

    const/16 v1, 0x63

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 202560
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method public final be()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoAnnotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202561
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202562
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba:Ljava/util/List;

    const/16 v1, 0x64

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba:Ljava/util/List;

    .line 202563
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ba:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202564
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202565
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    const/16 v1, 0x65

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 202566
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    return-object v0
.end method

.method public final bg()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202567
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202568
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/16 v1, 0x66

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 202569
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final bh()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202522
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202523
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd:Ljava/lang/String;

    const/16 v1, 0x67

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd:Ljava/lang/String;

    .line 202524
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bd:Ljava/lang/String;

    return-object v0
.end method

.method public final bi()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202706
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202707
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be:Ljava/util/List;

    const/16 v1, 0x68

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be:Ljava/util/List;

    .line 202708
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bj()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202682
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202683
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf:Ljava/lang/String;

    const/16 v1, 0x69

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf:Ljava/lang/String;

    .line 202684
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf:Ljava/lang/String;

    return-object v0
.end method

.method public final bk()Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202685
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202686
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    const/16 v1, 0x6a

    const-class v2, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    .line 202687
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bg:Lcom/facebook/graphql/model/GraphQLMessengerExtensionsUserProfileInfo;

    return-object v0
.end method

.method public final bl()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202688
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202689
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh:Ljava/lang/String;

    const/16 v1, 0x6b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh:Ljava/lang/String;

    .line 202690
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bh:Ljava/lang/String;

    return-object v0
.end method

.method public final bm()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202691
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202692
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi:Ljava/lang/String;

    const/16 v1, 0x6c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi:Ljava/lang/String;

    .line 202693
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bi:Ljava/lang/String;

    return-object v0
.end method

.method public final bn()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202694
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202695
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj:Ljava/lang/String;

    const/16 v1, 0x6d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj:Ljava/lang/String;

    .line 202696
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj:Ljava/lang/String;

    return-object v0
.end method

.method public final bo()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202697
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202698
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x6f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    .line 202699
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bp()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202700
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bl:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202701
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bl:Ljava/lang/String;

    const/16 v1, 0x70

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bl:Ljava/lang/String;

    .line 202702
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bl:Ljava/lang/String;

    return-object v0
.end method

.method public final bq()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202703
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202704
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    const/16 v1, 0x71

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 202705
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    return-object v0
.end method

.method public final br()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202679
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202680
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x72

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn:Lcom/facebook/graphql/model/GraphQLNode;

    .line 202681
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202709
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202710
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    .line 202711
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo:Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    return-object v0
.end method

.method public final bt()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202712
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202713
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp:Ljava/lang/String;

    const/16 v1, 0x74

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp:Ljava/lang/String;

    .line 202714
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bp:Ljava/lang/String;

    return-object v0
.end method

.method public final bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202715
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202716
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x75

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202717
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202718
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202719
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    const/16 v1, 0x76

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 202720
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br:Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    return-object v0
.end method

.method public final bw()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202721
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202722
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs:Ljava/lang/String;

    const/16 v1, 0x77

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs:Ljava/lang/String;

    .line 202723
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs:Ljava/lang/String;

    return-object v0
.end method

.method public final bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202724
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202725
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    const/16 v1, 0x78

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 202726
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    return-object v0
.end method

.method public final by()Lcom/facebook/graphql/model/GraphQLOffer;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202727
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu:Lcom/facebook/graphql/model/GraphQLOffer;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202728
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu:Lcom/facebook/graphql/model/GraphQLOffer;

    const/16 v1, 0x79

    const-class v2, Lcom/facebook/graphql/model/GraphQLOffer;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOffer;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 202729
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu:Lcom/facebook/graphql/model/GraphQLOffer;

    return-object v0
.end method

.method public final bz()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202730
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202731
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x7a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    .line 202732
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 202654
    const v0, -0x6829c9fb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202630
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202631
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    .line 202632
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->f:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLAd;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202633
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->g:Lcom/facebook/graphql/model/GraphQLAd;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202634
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->g:Lcom/facebook/graphql/model/GraphQLAd;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLAd;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAd;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->g:Lcom/facebook/graphql/model/GraphQLAd;

    .line 202635
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->g:Lcom/facebook/graphql/model/GraphQLAd;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202636
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202637
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->h:Ljava/lang/String;

    .line 202638
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202639
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202640
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->i:Ljava/lang/String;

    .line 202641
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202642
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202643
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j:Ljava/lang/String;

    .line 202644
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202645
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202646
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k:Ljava/lang/String;

    .line 202647
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 202648
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202649
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202650
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l:I

    return v0
.end method

.method public final q()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202651
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202652
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202653
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->m:I

    return v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202627
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202628
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 202629
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202655
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202656
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202657
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->o:Z

    return v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202658
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202659
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202660
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->p:Z

    return v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202661
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202662
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 202663
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->q:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLCoupon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202664
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r:Lcom/facebook/graphql/model/GraphQLCoupon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202665
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r:Lcom/facebook/graphql/model/GraphQLCoupon;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCoupon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r:Lcom/facebook/graphql/model/GraphQLCoupon;

    .line 202666
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r:Lcom/facebook/graphql/model/GraphQLCoupon;

    return-object v0
.end method

.method public final w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202667
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202668
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202669
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->s:J

    return-wide v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202670
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202671
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202672
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202673
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202674
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    .line 202675
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->u:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202676
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202677
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v:Ljava/lang/String;

    .line 202678
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v:Ljava/lang/String;

    return-object v0
.end method
