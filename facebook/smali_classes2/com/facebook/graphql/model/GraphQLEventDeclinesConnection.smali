.class public final Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventDeclinesEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:I

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323465
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323529
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 323527
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323528
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventDeclinesEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323524
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323525
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventDeclinesEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->f:Ljava/util/List;

    .line 323526
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323521
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323522
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323523
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323518
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323519
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323520
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->h:I

    return v0
.end method

.method private m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323515
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323516
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323517
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->i:I

    return v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323512
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323513
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j:Ljava/util/List;

    .line 323514
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 323509
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323510
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323511
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 323496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323497
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 323498
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 323499
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->n()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 323500
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 323501
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->a()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 323502
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 323503
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 323504
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323505
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323506
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 323507
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323508
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 323478
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323479
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323480
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323481
    if-eqz v1, :cond_0

    .line 323482
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 323483
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->f:Ljava/util/List;

    .line 323484
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 323485
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323486
    if-eqz v1, :cond_1

    .line 323487
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 323488
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->j:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 323489
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 323490
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323491
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 323492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 323493
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323494
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323495
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 323472
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->e:I

    .line 323473
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 323474
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 323475
    if-eqz v0, :cond_0

    .line 323476
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 323477
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323467
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 323468
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->e:I

    .line 323469
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->h:I

    .line 323470
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;->i:I

    .line 323471
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 323466
    const v0, 0x34854315

    return v0
.end method
