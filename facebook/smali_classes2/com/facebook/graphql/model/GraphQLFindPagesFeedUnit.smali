.class public final Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:J

.field private g:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250511
    const-class v0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250510
    const-class v0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 250506
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 250507
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x46672d33

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 250508
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->g:LX/0x2;

    .line 250509
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 250505
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250504
    const/4 v0, 0x0

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 250501
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 250502
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 250503
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->f:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 250512
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->g:LX/0x2;

    if-nez v0, :cond_0

    .line 250513
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->g:LX/0x2;

    .line 250514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->g:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 250496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250497
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 250498
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 250499
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250500
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 250493
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250495
    return-object p0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 250491
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->f:J

    .line 250492
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 250488
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 250489
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->f:J

    .line 250490
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250484
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 250485
    :goto_0
    return-object v0

    .line 250486
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 250487
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 250482
    const v0, -0x46672d33

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250480
    const/4 v0, 0x0

    move-object v0, v0

    .line 250481
    return-object v0
.end method
