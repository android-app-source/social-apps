.class public final Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionAllData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSearchElectionAllData$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSearchElectionDate;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326523
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326522
    const-class v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 326520
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 326521
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 326506
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 326508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 326509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 326510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 326511
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 326512
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 326513
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 326514
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 326515
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 326516
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 326517
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 326518
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326519
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326503
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326504
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->e:Ljava/util/List;

    .line 326505
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 326470
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 326472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 326473
    if-eqz v1, :cond_0

    .line 326474
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    .line 326475
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->e:Ljava/util/List;

    .line 326476
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 326477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 326478
    if-eqz v1, :cond_1

    .line 326479
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    .line 326480
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->f:Ljava/util/List;

    .line 326481
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326482
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 326499
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 326500
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->h:I

    .line 326501
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->i:I

    .line 326502
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 326498
    const v0, 0x1c03a7aa

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSearchElectionDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326495
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326496
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLSearchElectionDate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->f:Ljava/util/List;

    .line 326497
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326492
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326493
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->g:Ljava/lang/String;

    .line 326494
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326489
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326490
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326491
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->h:I

    return v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326486
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326487
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326488
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->i:I

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326483
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326484
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j:Ljava/lang/String;

    .line 326485
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j:Ljava/lang/String;

    return-object v0
.end method
