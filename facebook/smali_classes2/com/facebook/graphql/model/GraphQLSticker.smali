.class public final Lcom/facebook/graphql/model/GraphQLSticker;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSticker$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSticker$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:I

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:I

.field public W:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field public y:Z

.field public z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 263171
    const-class v0, Lcom/facebook/graphql/model/GraphQLSticker$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 263172
    const-class v0, Lcom/facebook/graphql/model/GraphQLSticker$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 263173
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 263174
    return-void
.end method

.method public constructor <init>(LX/4Yq;)V
    .locals 1

    .prologue
    .line 263175
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 263176
    iget-object v0, p1, LX/4Yq;->b:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263177
    iget-object v0, p1, LX/4Yq;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263178
    iget-object v0, p1, LX/4Yq;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->g:Ljava/util/List;

    .line 263179
    iget v0, p1, LX/4Yq;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->h:I

    .line 263180
    iget v0, p1, LX/4Yq;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->i:I

    .line 263181
    iget v0, p1, LX/4Yq;->g:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->j:I

    .line 263182
    iget v0, p1, LX/4Yq;->h:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->k:I

    .line 263183
    iget-object v0, p1, LX/4Yq;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->l:Ljava/lang/String;

    .line 263184
    iget-object v0, p1, LX/4Yq;->j:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263185
    iget-object v0, p1, LX/4Yq;->k:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263186
    iget-object v0, p1, LX/4Yq;->l:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263187
    iget-object v0, p1, LX/4Yq;->m:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263188
    iget-object v0, p1, LX/4Yq;->n:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263189
    iget-object v0, p1, LX/4Yq;->o:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263190
    iget-object v0, p1, LX/4Yq;->p:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263191
    iget-object v0, p1, LX/4Yq;->q:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263192
    iget-object v0, p1, LX/4Yq;->r:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263193
    iget-object v0, p1, LX/4Yq;->s:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263194
    iget-object v0, p1, LX/4Yq;->t:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263195
    iget-boolean v0, p1, LX/4Yq;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->x:Z

    .line 263196
    iget-boolean v0, p1, LX/4Yq;->v:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->y:Z

    .line 263197
    iget-object v0, p1, LX/4Yq;->w:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263198
    iget-object v0, p1, LX/4Yq;->x:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263199
    iget-object v0, p1, LX/4Yq;->y:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263200
    iget-object v0, p1, LX/4Yq;->z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263201
    iget-object v0, p1, LX/4Yq;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->D:Ljava/lang/String;

    .line 263202
    iget-object v0, p1, LX/4Yq;->B:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263203
    iget-object v0, p1, LX/4Yq;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->F:Ljava/lang/String;

    .line 263204
    iget-object v0, p1, LX/4Yq;->D:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263205
    iget-object v0, p1, LX/4Yq;->E:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263206
    iget-object v0, p1, LX/4Yq;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->I:Ljava/lang/String;

    .line 263207
    iget v0, p1, LX/4Yq;->G:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->J:I

    .line 263208
    iget-object v0, p1, LX/4Yq;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->K:Ljava/lang/String;

    .line 263209
    iget-object v0, p1, LX/4Yq;->I:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263210
    iget-object v0, p1, LX/4Yq;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->M:Ljava/lang/String;

    .line 263211
    iget-object v0, p1, LX/4Yq;->K:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263212
    iget-object v0, p1, LX/4Yq;->L:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263213
    iget-object v0, p1, LX/4Yq;->M:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263214
    iget-object v0, p1, LX/4Yq;->N:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263215
    iget-object v0, p1, LX/4Yq;->O:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263216
    iget-object v0, p1, LX/4Yq;->P:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263217
    iget-object v0, p1, LX/4Yq;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263218
    iget-object v0, p1, LX/4Yq;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->U:Ljava/lang/String;

    .line 263219
    iget v0, p1, LX/4Yq;->S:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->V:I

    .line 263220
    iget-object v0, p1, LX/4Yq;->T:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263221
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263222
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->u:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263223
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->u:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263224
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->u:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263225
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->v:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263226
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->v:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263227
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->v:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263228
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->w:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263229
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->w:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263230
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->w:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263231
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263232
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263233
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->x:Z

    return v0
.end method

.method private E()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263234
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263235
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263236
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->y:Z

    return v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263237
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263238
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263239
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263240
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->A:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263241
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->A:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263242
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->A:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263243
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->B:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263244
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->B:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263245
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->B:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263246
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->C:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263247
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->C:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263248
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->C:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private J()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263252
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263253
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->D:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->D:Ljava/lang/String;

    .line 263254
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->D:Ljava/lang/String;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263249
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->E:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263250
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->E:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263251
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->E:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263285
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263286
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->F:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->F:Ljava/lang/String;

    .line 263287
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->F:Ljava/lang/String;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263282
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263283
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263284
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private N()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263279
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->H:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263280
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->H:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263281
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->H:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263276
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263277
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->I:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->I:Ljava/lang/String;

    .line 263278
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->I:Ljava/lang/String;

    return-object v0
.end method

.method private P()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263273
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263274
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263275
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->J:I

    return v0
.end method

.method private Q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263270
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263271
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->K:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->K:Ljava/lang/String;

    .line 263272
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->K:Ljava/lang/String;

    return-object v0
.end method

.method private R()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263267
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->L:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263268
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->L:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263269
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->L:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263264
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263265
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->M:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->M:Ljava/lang/String;

    .line 263266
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->M:Ljava/lang/String;

    return-object v0
.end method

.method private T()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263261
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->N:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263262
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->N:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263263
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->N:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private U()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263258
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->O:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263259
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->O:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263260
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->O:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private V()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263255
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->P:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263256
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->P:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263257
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->P:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private W()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private X()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262890
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->R:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262891
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->R:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262892
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->R:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private Y()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262887
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->S:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262888
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->S:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262889
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->S:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private Z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262884
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->T:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262885
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->T:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262886
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->T:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aa()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 262881
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262882
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262883
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->V:I

    return v0
.end method

.method private ab()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262878
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->W:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262879
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->W:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262880
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->W:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262875
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262876
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262877
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 262872
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262873
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->g:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->g:Ljava/util/List;

    .line 262874
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private o()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262869
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262870
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262871
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->h:I

    return v0
.end method

.method private p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262866
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262867
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262868
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->i:I

    return v0
.end method

.method private q()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262863
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262864
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262865
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->j:I

    return v0
.end method

.method private r()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262860
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262861
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262862
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->k:I

    return v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262857
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262858
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262859
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262854
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->n:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262855
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->n:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262856
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->n:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262851
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->o:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262852
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->o:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262853
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->o:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262893
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262894
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262895
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262896
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262897
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262898
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262899
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262900
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262901
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262902
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->s:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262903
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->s:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262904
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->s:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262905
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->t:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262906
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->t:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 262907
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->t:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 40

    .prologue
    .line 262908
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262909
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 262910
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 262911
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->n()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 262912
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->k()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 262913
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 262914
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 262915
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 262916
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 262917
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 262918
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 262919
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 262920
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 262921
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 262922
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 262923
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 262924
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 262925
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 262926
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 262927
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 262928
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->J()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 262929
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 262930
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->L()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 262931
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 262932
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 262933
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->O()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 262934
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Q()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 262935
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 262936
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->S()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 262937
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 262938
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 262939
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 262940
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 262941
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 262942
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 262943
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 262944
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->l()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 262945
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 262946
    const/16 v39, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 262947
    const/16 v39, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 262948
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 262949
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 262950
    const/4 v2, 0x4

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->o()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 262951
    const/4 v2, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->p()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 262952
    const/4 v2, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->q()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 262953
    const/4 v2, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->r()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 262954
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 262955
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 262956
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 262957
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 262958
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 262959
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 262960
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 262961
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 262962
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 262963
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 262964
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 262965
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262966
    const/16 v2, 0x14

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->D()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 262967
    const/16 v2, 0x15

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->E()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 262968
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262969
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262970
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262971
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262972
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262973
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262974
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262975
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262976
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262977
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262978
    const/16 v2, 0x21

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->P()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 262979
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262980
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262981
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262982
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262983
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262984
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262985
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262986
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262987
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262988
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262989
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262990
    const/16 v2, 0x2d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSticker;->aa()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 262991
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 262992
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262993
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 262994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262995
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262996
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 262997
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 262998
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 262999
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263000
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 263001
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263002
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 263003
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263004
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263005
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 263006
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 263007
    if-eqz v2, :cond_2

    .line 263008
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263009
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLSticker;->g:Ljava/util/List;

    move-object v1, v0

    .line 263010
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 263011
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263012
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 263013
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263014
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263015
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 263016
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263017
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 263018
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263019
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263020
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 263021
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 263023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263024
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263025
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 263026
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263027
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 263028
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263029
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263030
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 263031
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263032
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 263033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263034
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263035
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 263036
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263037
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 263038
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263039
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263040
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 263041
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 263043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263044
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263045
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 263046
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263047
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 263048
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263049
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263050
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 263051
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263052
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 263053
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263054
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263055
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 263056
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263057
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 263058
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263059
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263060
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 263061
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263062
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 263063
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263064
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263065
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 263066
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263067
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 263068
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263069
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263070
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 263071
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263072
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 263073
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263074
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263075
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 263076
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263077
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 263078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263079
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263080
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 263081
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263082
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 263083
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263084
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263085
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 263086
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263087
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 263088
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263089
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263090
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 263091
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263092
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 263093
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263094
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263095
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 263096
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263097
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 263098
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263099
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263100
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 263101
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263102
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 263103
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263104
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263105
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 263106
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263107
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 263108
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263109
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263110
    :cond_16
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 263111
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263112
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 263113
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263114
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263115
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 263116
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263117
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 263118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263119
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263120
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 263121
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263122
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 263123
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263124
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263125
    :cond_19
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 263126
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263127
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 263128
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263129
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263130
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 263131
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263132
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 263133
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263134
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263135
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 263136
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263137
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 263138
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263139
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263140
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 263141
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263142
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 263143
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 263144
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSticker;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263145
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 263146
    if-nez v1, :cond_1e

    :goto_0
    return-object p0

    :cond_1e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 263148
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 263149
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->h:I

    .line 263150
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->i:I

    .line 263151
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->j:I

    .line 263152
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->k:I

    .line 263153
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->x:Z

    .line 263154
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->y:Z

    .line 263155
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->J:I

    .line 263156
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->V:I

    .line 263157
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 263158
    const v0, -0xd725ee3

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->l:Ljava/lang/String;

    .line 263164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->U:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->U:Ljava/lang/String;

    .line 263167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSticker;->U:Ljava/lang/String;

    return-object v0
.end method
