.class public final Lcom/facebook/graphql/model/GraphQLTarotDigest;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotDigest$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTarotDigest$Serializer;
.end annotation


# instance fields
.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTarotCard;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:D

.field public q:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319829
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotDigest$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319830
    const-class v0, Lcom/facebook/graphql/model/GraphQLTarotDigest$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 319831
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 319832
    return-void
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->f:Ljava/lang/String;

    .line 319835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->f:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319836
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319837
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 319838
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319839
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319840
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->l:Ljava/lang/String;

    .line 319841
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->l:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319842
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->n:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319843
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->n:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->n:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 319844
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->n:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    return-object v0
.end method

.method private u()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319845
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 319846
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 319847
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->p:D

    return-wide v0
.end method

.method private v()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319848
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 319849
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 319850
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->q:D

    return-wide v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 319802
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319803
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->q()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 319804
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 319805
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->l()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 319806
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 319807
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 319808
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->n()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 319809
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->s()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 319810
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v15

    .line 319811
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->t()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 319812
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->p()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 319813
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 319814
    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 319815
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 319816
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 319817
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 319818
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 319819
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 319820
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 319821
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 319822
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 319823
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319824
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319825
    const/16 v3, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->u()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 319826
    const/16 v3, 0xc

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->v()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 319827
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319828
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 319851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319852
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->t()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 319853
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->t()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 319854
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->t()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 319855
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    .line 319856
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;->n:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 319857
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 319858
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 319859
    if-eqz v2, :cond_1

    .line 319860
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    .line 319861
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m:Ljava/util/List;

    move-object v1, v0

    .line 319862
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 319863
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 319864
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 319865
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    .line 319866
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 319867
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 319868
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 319869
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 319870
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    .line 319871
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 319872
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->p()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 319873
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->p()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 319874
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->p()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 319875
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;

    .line 319876
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 319877
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319878
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319774
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 319775
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 319776
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->e:J

    .line 319777
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->p:D

    .line 319778
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->q:D

    .line 319779
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 319780
    const v0, -0x11b13572    # -1.5999696E28f

    return v0
.end method

.method public final j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 319781
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 319782
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 319783
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->e:J

    return-wide v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319784
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319785
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    .line 319786
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->g:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319799
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319800
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->h:Ljava/lang/String;

    .line 319801
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319787
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319788
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->j:Ljava/lang/String;

    .line 319789
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319790
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319791
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k:Ljava/lang/String;

    .line 319792
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTarotCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319793
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319794
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTarotCard;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m:Ljava/util/List;

    .line 319795
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319796
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319797
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 319798
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTarotDigest;->o:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    return-object v0
.end method
