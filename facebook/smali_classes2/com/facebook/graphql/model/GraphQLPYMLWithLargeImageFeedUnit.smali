.class public final Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements Lcom/facebook/graphql/model/Sponsorable;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 200775
    const-class v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 200776
    const-class v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 200777
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 200778
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x275591d0

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 200779
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->s:LX/0x2;

    .line 200780
    return-void
.end method

.method public constructor <init>(LX/4XU;)V
    .locals 2

    .prologue
    .line 200781
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 200782
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x275591d0

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 200783
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->s:LX/0x2;

    .line 200784
    iget-object v0, p1, LX/4XU;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->f:Ljava/lang/String;

    .line 200785
    iget-object v0, p1, LX/4XU;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g:Ljava/lang/String;

    .line 200786
    iget-wide v0, p1, LX/4XU;->d:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->h:J

    .line 200787
    iget-object v0, p1, LX/4XU;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->i:Ljava/lang/String;

    .line 200788
    iget-object v0, p1, LX/4XU;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->j:Ljava/lang/String;

    .line 200789
    iget v0, p1, LX/4XU;->g:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->k:I

    .line 200790
    iget-object v0, p1, LX/4XU;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200791
    iget-object v0, p1, LX/4XU;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->l:Ljava/lang/String;

    .line 200792
    iget-object v0, p1, LX/4XU;->j:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    .line 200793
    iget-object v0, p1, LX/4XU;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200794
    iget-object v0, p1, LX/4XU;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->o:Ljava/lang/String;

    .line 200795
    iget-object v0, p1, LX/4XU;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200796
    iget-object v0, p1, LX/4XU;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->q:Ljava/lang/String;

    .line 200797
    iget-object v0, p1, LX/4XU;->o:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->s:LX/0x2;

    .line 200798
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 200799
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->k:I

    .line 200800
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 200801
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 200802
    if-eqz v0, :cond_0

    .line 200803
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 200804
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 200805
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->i:Ljava/lang/String;

    .line 200806
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 200807
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 200808
    if-eqz v0, :cond_0

    .line 200809
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 200810
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 200811
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->j:Ljava/lang/String;

    .line 200812
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 200813
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 200814
    if-eqz v0, :cond_0

    .line 200815
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 200816
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200817
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200818
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    .line 200819
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200820
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200821
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200822
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200823
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200824
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->o:Ljava/lang/String;

    .line 200825
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200826
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200827
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200828
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 200833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200829
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200830
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200831
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200873
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200874
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g:Ljava/lang/String;

    .line 200875
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 200870
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 200871
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 200872
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->h:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 200869
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 200868
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 200865
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->s:LX/0x2;

    if-nez v0, :cond_0

    .line 200866
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->s:LX/0x2;

    .line 200867
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->s:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 200864
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 200862
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 200863
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 200834
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 200835
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 200836
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 200837
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->w()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 200838
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->x()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 200839
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->z()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 200840
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 200841
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 200842
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->C()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 200843
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 200844
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 200845
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 200846
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 200847
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 200848
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 200849
    const/4 v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 200850
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 200851
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 200852
    const/4 v2, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->y()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 200853
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 200854
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 200855
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 200856
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 200857
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 200858
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 200859
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 200860
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 200861
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 200726
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 200727
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200728
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200729
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 200730
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 200731
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200732
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 200733
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    .line 200734
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 200735
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 200736
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    .line 200737
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 200738
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200739
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 200740
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 200741
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200742
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 200743
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200744
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 200745
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 200746
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200747
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 200748
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 200774
    new-instance v0, LX/4XV;

    invoke-direct {v0, p1}, LX/4XV;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 200749
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->h:J

    .line 200750
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 200722
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 200723
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->h:J

    .line 200724
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->k:I

    .line 200725
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 200708
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200709
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 200710
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 200711
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 200712
    :goto_0
    return-void

    .line 200713
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 200715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 200716
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 200717
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->y()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 200719
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 200720
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 200721
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 200701
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200702
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->a(Ljava/lang/String;)V

    .line 200703
    :cond_0
    :goto_0
    return-void

    .line 200704
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200705
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 200706
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200707
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200698
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200699
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->q:Ljava/lang/String;

    .line 200700
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200693
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 200695
    :goto_0
    return-object v0

    .line 200696
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 200697
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 200692
    const v0, -0x275591d0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200689
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200690
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->f:Ljava/lang/String;

    .line 200691
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200684
    invoke-static {p0}, LX/4Zj;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    .line 200685
    if-eqz v0, :cond_0

    .line 200686
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->z()Ljava/lang/String;

    move-result-object v0

    .line 200687
    :goto_0
    move-object v0, v0

    .line 200688
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 200683
    invoke-static {p0}, LX/2dv;->a(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v0

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200681
    const/4 v0, 0x0

    move-object v0, v0

    .line 200682
    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 200751
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->u()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 200752
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 200753
    invoke-static {p0}, LX/1w8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 200754
    invoke-static {p0}, LX/4Zj;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    .line 200755
    if-eqz v0, :cond_0

    .line 200756
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->A()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    .line 200757
    :goto_0
    move-object v0, v0

    .line 200758
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200759
    invoke-static {p0}, LX/1mc;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 1

    .prologue
    .line 200760
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 200761
    invoke-static {p0}, LX/18M;->a(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v0

    return v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200762
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200763
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->i:Ljava/lang/String;

    .line 200764
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200765
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200766
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->j:Ljava/lang/String;

    .line 200767
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final y()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 200768
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 200769
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 200770
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->k:I

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200771
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200772
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->l:Ljava/lang/String;

    .line 200773
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method
