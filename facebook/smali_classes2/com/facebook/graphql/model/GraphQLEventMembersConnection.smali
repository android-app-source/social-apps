.class public final Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventMembersConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventMembersConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventMembersEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323676
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323675
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 323673
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323674
    return-void
.end method

.method public constructor <init>(LX/4WC;)V
    .locals 1

    .prologue
    .line 323665
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323666
    iget v0, p1, LX/4WC;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->e:I

    .line 323667
    iget-object v0, p1, LX/4WC;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->f:Ljava/util/List;

    .line 323668
    iget-object v0, p1, LX/4WC;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->g:Ljava/util/List;

    .line 323669
    iget-object v0, p1, LX/4WC;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323670
    iget v0, p1, LX/4WC;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->i:I

    .line 323671
    iget v0, p1, LX/4WC;->g:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j:I

    .line 323672
    return-void
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323662
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323663
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323664
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method private n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323659
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323660
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323661
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j:I

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 323656
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323657
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323658
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 323677
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 323679
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 323680
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 323681
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 323682
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->a()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 323683
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 323684
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 323685
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 323686
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323687
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323689
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 323628
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323629
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323631
    if-eqz v1, :cond_0

    .line 323632
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 323633
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->f:Ljava/util/List;

    .line 323634
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 323635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323636
    if-eqz v1, :cond_1

    .line 323637
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 323638
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 323639
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 323640
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323641
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 323642
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 323643
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323644
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323645
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 323617
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->e:I

    .line 323618
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 323619
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 323620
    if-eqz v0, :cond_0

    .line 323621
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 323622
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323623
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 323624
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->e:I

    .line 323625
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->i:I

    .line 323626
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j:I

    .line 323627
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 323646
    const v0, -0x6e31e683

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventMembersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMembersEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->f:Ljava/util/List;

    .line 323655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323647
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323648
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->g:Ljava/util/List;

    .line 323649
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323650
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323651
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323652
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->i:I

    return v0
.end method
