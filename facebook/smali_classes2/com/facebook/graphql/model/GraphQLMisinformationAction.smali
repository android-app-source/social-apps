.class public final Lcom/facebook/graphql/model/GraphQLMisinformationAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMisinformationAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMisinformationAction$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319226
    const-class v0, Lcom/facebook/graphql/model/GraphQLMisinformationAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319235
    const-class v0, Lcom/facebook/graphql/model/GraphQLMisinformationAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 319233
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 319234
    return-void
.end method

.method public constructor <init>(LX/4XJ;)V
    .locals 1

    .prologue
    .line 319227
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 319228
    iget-object v0, p1, LX/4XJ;->b:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->e:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    .line 319229
    iget-object v0, p1, LX/4XJ;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->f:Ljava/lang/String;

    .line 319230
    iget-object v0, p1, LX/4XJ;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->g:Ljava/lang/String;

    .line 319231
    iget-object v0, p1, LX/4XJ;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->h:Ljava/lang/String;

    .line 319232
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 319214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 319216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 319217
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 319218
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 319219
    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->a()Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v4, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319220
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 319221
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 319222
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 319223
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319224
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 319225
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->a()Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 319236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319237
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319238
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319211
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->e:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319212
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->e:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->e:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    .line 319213
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->e:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 319201
    const v0, -0x49d68ad5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319208
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319209
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->f:Ljava/lang/String;

    .line 319210
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319205
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319206
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->g:Ljava/lang/String;

    .line 319207
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319202
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319203
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->h:Ljava/lang/String;

    .line 319204
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->h:Ljava/lang/String;

    return-object v0
.end method
