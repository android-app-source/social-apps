.class public final Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:J

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293704
    const-class v0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293685
    const-class v0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 293681
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 293682
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x342a59b5    # -2.8003478E7f

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 293683
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n:LX/0x2;

    .line 293684
    return-void
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293678
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293679
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->k:Ljava/lang/String;

    .line 293680
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293675
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293676
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293677
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 293674
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->i:Ljava/lang/String;

    .line 293673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 293668
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 293669
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 293670
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->j:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 293667
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 293664
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n:LX/0x2;

    if-nez v0, :cond_0

    .line 293665
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n:LX/0x2;

    .line 293666
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 293662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 293663
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 293643
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 293645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 293646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 293647
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 293648
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 293649
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 293650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 293651
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 293652
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 293653
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 293654
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 293655
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 293656
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 293657
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 293658
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 293659
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 293660
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293661
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 293686
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293687
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 293688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 293689
    if-eqz v1, :cond_3

    .line 293690
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;

    .line 293691
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g:Ljava/util/List;

    move-object v1, v0

    .line 293692
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293693
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 293695
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;

    .line 293696
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293697
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 293698
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293699
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 293700
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;

    .line 293701
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293702
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293703
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 293616
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->j:J

    .line 293617
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 293618
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 293619
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->j:J

    .line 293620
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293621
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293622
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->m:Ljava/lang/String;

    .line 293623
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293625
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 293626
    :goto_0
    return-object v0

    .line 293627
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 293628
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 293629
    const v0, -0x342a59b5    # -2.8003478E7f

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293630
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293631
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->f:Ljava/lang/String;

    .line 293632
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 293633
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293634
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293635
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g:Ljava/util/List;

    .line 293636
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293637
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293638
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293639
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 293640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 293641
    return-object v0
.end method

.method public final synthetic q()Ljava/util/List;
    .locals 1

    .prologue
    .line 293642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method
