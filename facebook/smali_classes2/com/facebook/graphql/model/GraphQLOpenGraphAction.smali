.class public final Lcom/facebook/graphql/model/GraphQLOpenGraphAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLOpenGraphAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLOpenGraphAction$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 260345
    const-class v0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 260399
    const-class v0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 260397
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 260398
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260394
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260395
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260396
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260391
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260392
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 260393
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260388
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260389
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->h:Ljava/lang/String;

    .line 260390
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->h:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260400
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260401
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j:Ljava/lang/String;

    .line 260402
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 260372
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 260374
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 260375
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 260376
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 260377
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 260378
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 260379
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 260380
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 260381
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 260382
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 260383
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 260384
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 260385
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 260386
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 260387
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 260354
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260355
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260356
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260357
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->l()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 260358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 260359
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260360
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 260361
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 260362
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->m()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 260363
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 260364
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 260365
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 260366
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 260367
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 260368
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 260369
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->i:Lcom/facebook/graphql/model/GraphQLNode;

    .line 260370
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 260371
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260353
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 260352
    const v0, -0x658b3fa6

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260349
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260350
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->e:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->e:Ljava/util/List;

    .line 260351
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260346
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->i:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260347
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->i:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->i:Lcom/facebook/graphql/model/GraphQLNode;

    .line 260348
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->i:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method
