.class public final Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public f:I

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262310
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262309
    const-class v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262262
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262263
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 262296
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262297
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->k()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 262298
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->l()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 262299
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 262300
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 262301
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne v0, v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 262302
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->j()I

    move-result v4

    invoke-virtual {p1, v0, v4, v5}, LX/186;->a(III)V

    .line 262303
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 262304
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 262305
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 262306
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262307
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 262308
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 262283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262284
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 262285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 262286
    if-eqz v1, :cond_0

    .line 262287
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 262288
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->g:Ljava/util/List;

    .line 262289
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 262290
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 262291
    if-eqz v1, :cond_1

    .line 262292
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 262293
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->h:Ljava/util/List;

    .line 262294
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262295
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262280
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->e:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262281
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->e:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->e:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 262282
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->e:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 262277
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 262278
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->f:I

    .line 262279
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 262276
    const v0, -0x2804373e

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262273
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262274
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262275
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->f:I

    return v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262270
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262271
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->g:Ljava/util/List;

    .line 262272
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262267
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262268
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->h:Ljava/util/List;

    .line 262269
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262264
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262265
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->i:Ljava/lang/String;

    .line 262266
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;->i:Ljava/lang/String;

    return-object v0
.end method
