.class public final Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/17w;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255287
    const-class v0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255288
    const-class v0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 255289
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 255290
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x197b5b5b

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 255291
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->m:LX/0x2;

    .line 255292
    return-void
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255265
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255266
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->i:Ljava/lang/String;

    .line 255267
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255293
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255294
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->j:Ljava/lang/String;

    .line 255295
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255296
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->k:Ljava/lang/String;

    .line 255298
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255299
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255300
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->h:Ljava/lang/String;

    .line 255301
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 255302
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255303
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255304
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->f:Ljava/lang/String;

    .line 255305
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 255306
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 255307
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 255308
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->g:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 255309
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 255310
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 255311
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->m:LX/0x2;

    if-nez v0, :cond_0

    .line 255312
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->m:LX/0x2;

    .line 255313
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->m:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 255314
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 255268
    const/4 v0, 0x0

    move-object v0, v0

    .line 255269
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 255270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->E_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 255272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->C_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 255273
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 255274
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 255275
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 255276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 255277
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 255278
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 255279
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 255280
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 255281
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 255282
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 255283
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 255284
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 255285
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255286
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 255247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255248
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255249
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 255246
    new-instance v0, LX/4YP;

    invoke-direct {v0, p1}, LX/4YP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255245
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 255243
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->g:J

    .line 255244
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 255238
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 255239
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->g:J

    .line 255240
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 255236
    invoke-virtual {p2}, LX/18L;->a()V

    .line 255237
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 255235
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255250
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255251
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255252
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->l:Ljava/lang/String;

    .line 255253
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255254
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 255255
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPymgfFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 255256
    :goto_0
    return-object v0

    .line 255257
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 255258
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 255259
    const v0, -0x197b5b5b

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255260
    const/4 v0, 0x0

    move-object v0, v0

    .line 255261
    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255262
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 255263
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 255264
    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 255241
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 255242
    return-object v0
.end method
