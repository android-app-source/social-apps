.class public final Lcom/facebook/graphql/model/GraphQLVideoChannel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoChannel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoChannel$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:I

.field public s:I

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

.field public w:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322344
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoChannel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322278
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoChannel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 322279
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322280
    return-void
.end method

.method public constructor <init>(LX/4ZQ;)V
    .locals 1

    .prologue
    .line 322281
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322282
    iget-object v0, p1, LX/4ZQ;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->f:Ljava/lang/String;

    .line 322283
    iget v0, p1, LX/4ZQ;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->g:I

    .line 322284
    iget-object v0, p1, LX/4ZQ;->d:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->v:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 322285
    iget-object v0, p1, LX/4ZQ;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 322286
    iget-object v0, p1, LX/4ZQ;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 322287
    iget-boolean v0, p1, LX/4ZQ;->g:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->j:Z

    .line 322288
    iget-boolean v0, p1, LX/4ZQ;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k:Z

    .line 322289
    iget-boolean v0, p1, LX/4ZQ;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->l:Z

    .line 322290
    iget-object v0, p1, LX/4ZQ;->j:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w:Lcom/facebook/graphql/model/GraphQLNode;

    .line 322291
    iget-object v0, p1, LX/4ZQ;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 322292
    iget-boolean v0, p1, LX/4ZQ;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->n:Z

    .line 322293
    iget-boolean v0, p1, LX/4ZQ;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->o:Z

    .line 322294
    iget-boolean v0, p1, LX/4ZQ;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->p:Z

    .line 322295
    iget-boolean v0, p1, LX/4ZQ;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q:Z

    .line 322296
    iget v0, p1, LX/4ZQ;->p:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r:I

    .line 322297
    iget v0, p1, LX/4ZQ;->q:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s:I

    .line 322298
    iget-object v0, p1, LX/4ZQ;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322299
    iget-object v0, p1, LX/4ZQ;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322300
    iget-object v0, p1, LX/4ZQ;->t:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 322301
    return-void
.end method

.method private A()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322302
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322303
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322304
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s:I

    return v0
.end method

.method private B()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322305
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->v:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322306
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->v:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->v:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 322307
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->v:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    return-object v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 322308
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->o:Z

    .line 322309
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 322310
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 322311
    if-eqz v0, :cond_0

    .line 322312
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 322313
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 322314
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->p:Z

    .line 322315
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 322316
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 322317
    if-eqz v0, :cond_0

    .line 322318
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 322319
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 322320
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q:Z

    .line 322321
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 322322
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 322323
    if-eqz v0, :cond_0

    .line 322324
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 322325
    :cond_0
    return-void
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322326
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322327
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 322328
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method private u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322329
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322330
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322331
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->j:Z

    return v0
.end method

.method private v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322332
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322333
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322334
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->l:Z

    return v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322335
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322336
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 322337
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322338
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322339
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322340
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->n:Z

    return v0
.end method

.method private y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322341
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322342
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322343
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->o:Z

    return v0
.end method

.method private z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322242
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322243
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322244
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->p:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 322245
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322246
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 322247
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 322248
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 322249
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 322250
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 322251
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 322252
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 322253
    const/16 v8, 0x14

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 322254
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 322255
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 322256
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->l()I

    move-result v2

    invoke-virtual {p1, v0, v2, v9}, LX/186;->a(III)V

    .line 322257
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 322258
    const/4 v2, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v3, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 322259
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->u()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 322260
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->n()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 322261
    const/16 v0, 0x8

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->v()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 322262
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 322263
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->x()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 322264
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->y()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 322265
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->z()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 322266
    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->o()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 322267
    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->p()I

    move-result v2

    invoke-virtual {p1, v0, v2, v9}, LX/186;->a(III)V

    .line 322268
    const/16 v0, 0xf

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->A()I

    move-result v2

    invoke-virtual {p1, v0, v2, v9}, LX/186;->a(III)V

    .line 322269
    const/16 v0, 0x10

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 322270
    const/16 v0, 0x11

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 322271
    const/16 v0, 0x12

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->B()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-ne v2, v3, :cond_2

    :goto_2
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 322272
    const/16 v0, 0x13

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 322273
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322274
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    :cond_0
    move-object v0, v1

    .line 322275
    goto/16 :goto_0

    .line 322276
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    goto/16 :goto_1

    .line 322277
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->B()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 322145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 322148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 322149
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 322150
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 322151
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 322152
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 322153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 322154
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 322155
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w:Lcom/facebook/graphql/model/GraphQLNode;

    .line 322156
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 322157
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 322158
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 322159
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 322160
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 322161
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 322162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 322164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 322165
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322166
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 322167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 322169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 322170
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChannel;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322171
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322172
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 322173
    new-instance v0, LX/4ZR;

    invoke-direct {v0, p1}, LX/4ZR;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 322174
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 322175
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->g:I

    .line 322176
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->j:Z

    .line 322177
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k:Z

    .line 322178
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->l:Z

    .line 322179
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->n:Z

    .line 322180
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->o:Z

    .line 322181
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->p:Z

    .line 322182
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q:Z

    .line 322183
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r:I

    .line 322184
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s:I

    .line 322185
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 322186
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322187
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->y()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 322188
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 322189
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    .line 322190
    :goto_0
    return-void

    .line 322191
    :cond_0
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322192
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->z()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 322193
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 322194
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 322195
    :cond_1
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 322197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 322198
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 322199
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 322200
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322201
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->a(Z)V

    .line 322202
    :cond_0
    :goto_0
    return-void

    .line 322203
    :cond_1
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322204
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->b(Z)V

    goto :goto_0

    .line 322205
    :cond_2
    const-string v0, "video_channel_is_viewer_pinned"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322206
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->c(Z)V

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 322207
    const v0, 0x2d116428

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322208
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 322209
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 322210
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 322211
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 322212
    const/4 v0, 0x0

    .line 322213
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322214
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322215
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->f:Ljava/lang/String;

    .line 322216
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322217
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322218
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322219
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->g:I

    return v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322220
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322221
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 322222
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322223
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322224
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322225
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k:Z

    return v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322226
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322227
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322228
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q:Z

    return v0
.end method

.method public final p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322229
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322230
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322231
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r:I

    return v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322232
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322233
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322234
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322235
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322236
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 322237
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322238
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322239
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w:Lcom/facebook/graphql/model/GraphQLNode;

    .line 322240
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChannel;->w:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method
