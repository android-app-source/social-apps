.class public final Lcom/facebook/graphql/model/GraphQLStructuredSurvey;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStructuredSurvey$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStructuredSurvey$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326966
    const-class v0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326927
    const-class v0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 326964
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 326965
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326961
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326962
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->g:Ljava/lang/String;

    .line 326963
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->g:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326958
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326959
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j:Ljava/lang/String;

    .line 326960
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 326942
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326943
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 326944
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 326945
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 326946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 326947
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 326948
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 326949
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 326950
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 326951
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 326952
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 326953
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->m()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 326954
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 326955
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326956
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 326957
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->m()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 326967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326969
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    .line 326970
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 326971
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    .line 326972
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->e:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    .line 326973
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 326974
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    .line 326975
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 326976
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    .line 326977
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->h:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    .line 326978
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326979
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326941
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 326940
    const v0, -0x3836a7d5

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326937
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->e:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326938
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->e:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->e:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    .line 326939
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->e:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326934
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326935
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->f:Ljava/lang/String;

    .line 326936
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326931
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->h:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326932
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->h:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->h:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    .line 326933
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->h:Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    .line 326930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->i:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    return-object v0
.end method
