.class public final Lcom/facebook/graphql/model/GraphQLLocation;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/1k1;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLocation$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLocation$Serializer;
.end annotation


# instance fields
.field public e:D

.field public f:D

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301484
    const-class v0, Lcom/facebook/graphql/model/GraphQLLocation$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301483
    const-class v0, Lcom/facebook/graphql/model/GraphQLLocation$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 301481
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301482
    return-void
.end method

.method public constructor <init>(LX/4X8;)V
    .locals 2

    .prologue
    .line 301476
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301477
    iget-wide v0, p1, LX/4X8;->b:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->e:D

    .line 301478
    iget-wide v0, p1, LX/4X8;->c:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->f:D

    .line 301479
    iget-object v0, p1, LX/4X8;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->g:Ljava/lang/String;

    .line 301480
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 301462
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301463
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301464
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 301468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 301470
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 301471
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 301472
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 301473
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 301474
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301475
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 301465
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301466
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301467
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 301458
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 301459
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->e:D

    .line 301460
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->f:D

    .line 301461
    return-void
.end method

.method public final b()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301455
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301456
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301457
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->f:D

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 301454
    const v0, 0x752a03d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301451
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301452
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->g:Ljava/lang/String;

    .line 301453
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLocation;->g:Ljava/lang/String;

    return-object v0
.end method
