.class public final Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255398
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255448
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 255449
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 255450
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x14583a39

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 255451
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l:LX/0x2;

    .line 255452
    return-void
.end method

.method public constructor <init>(LX/4Wa;)V
    .locals 2

    .prologue
    .line 255453
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 255454
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x14583a39

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 255455
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l:LX/0x2;

    .line 255456
    iget-object v0, p1, LX/4Wa;->b:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 255457
    iget-wide v0, p1, LX/4Wa;->c:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->g:J

    .line 255458
    iget-object v0, p1, LX/4Wa;->d:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->h:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 255459
    iget-object v0, p1, LX/4Wa;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->i:Ljava/lang/String;

    .line 255460
    iget-object v0, p1, LX/4Wa;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 255461
    iget-object v0, p1, LX/4Wa;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 255462
    iget-object v0, p1, LX/4Wa;->h:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l:LX/0x2;

    .line 255463
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 255464
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255465
    const/4 v0, 0x0

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 255466
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 255467
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 255468
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->g:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 255469
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l:LX/0x2;

    if-nez v0, :cond_0

    .line 255470
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l:LX/0x2;

    .line 255471
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 255472
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255473
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 255474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 255475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 255476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 255477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 255478
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 255479
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 255480
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 255481
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 255482
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 255483
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 255484
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 255485
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255486
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 255425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 255427
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 255428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 255429
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 255430
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 255431
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 255432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 255433
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 255434
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 255435
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->h:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 255436
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 255437
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 255438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 255439
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 255440
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 255441
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 255442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 255443
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 255444
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 255445
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 255446
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255447
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 255487
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->g:J

    .line 255488
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 255422
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 255423
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->g:J

    .line 255424
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 255418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 255419
    :goto_0
    return-object v0

    .line 255420
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 255421
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 255416
    const v0, 0x14583a39

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255414
    const/4 v0, 0x0

    move-object v0, v0

    .line 255415
    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255411
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255412
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 255413
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255408
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->h:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255409
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->h:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->h:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 255410
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->h:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255405
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255406
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->i:Ljava/lang/String;

    .line 255407
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255402
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255403
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 255404
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->j:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255399
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255400
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 255401
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
