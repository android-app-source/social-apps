.class public final Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:I

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321997
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321996
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321994
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321995
    return-void
.end method

.method public constructor <init>(LX/4WM;)V
    .locals 1

    .prologue
    .line 321981
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321982
    iget-object v0, p1, LX/4WM;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->e:Ljava/lang/String;

    .line 321983
    iget-object v0, p1, LX/4WM;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321984
    iget-object v0, p1, LX/4WM;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->f:Ljava/lang/String;

    .line 321985
    iget-boolean v0, p1, LX/4WM;->e:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->g:Z

    .line 321986
    iget v0, p1, LX/4WM;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->h:I

    .line 321987
    iget-object v0, p1, LX/4WM;->g:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321988
    iget-object v0, p1, LX/4WM;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j:Ljava/lang/String;

    .line 321989
    iget-object v0, p1, LX/4WM;->i:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->o:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 321990
    iget-object v0, p1, LX/4WM;->j:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321991
    iget-object v0, p1, LX/4WM;->k:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321992
    iget-object v0, p1, LX/4WM;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->m:Ljava/lang/String;

    .line 321993
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321978
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321979
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->e:Ljava/lang/String;

    .line 321980
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321975
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321976
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->f:Ljava/lang/String;

    .line 321977
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method private m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321972
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321973
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321974
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->g:Z

    return v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321969
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321970
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321971
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321966
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321967
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j:Ljava/lang/String;

    .line 321968
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321998
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321999
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 322000
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321963
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321964
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321965
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321960
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321961
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->m:Ljava/lang/String;

    .line 321962
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321957
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321958
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321959
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321954
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->o:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321955
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->o:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->o:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    .line 321956
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->o:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 321930
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321931
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 321932
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 321933
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 321934
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 321935
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 321936
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 321937
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 321938
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 321939
    const/16 v8, 0xc

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 321940
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 321941
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 321942
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 321943
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result v1

    const/4 v8, 0x0

    invoke-virtual {p1, v0, v1, v8}, LX/186;->a(III)V

    .line 321944
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 321945
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 321946
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 321947
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 321948
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 321949
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 321950
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->t()Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 321951
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321952
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 321953
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->t()Lcom/facebook/graphql/enums/GraphQLFeedbackReactionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 321907
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321908
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 321909
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 321910
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 321911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321912
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321913
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 321914
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 321915
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 321916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321917
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321918
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 321919
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 321920
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 321921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321922
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321923
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 321924
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 321925
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 321926
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321927
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321928
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321929
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321906
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 321898
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 321899
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->g:Z

    .line 321900
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->h:I

    .line 321901
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 321905
    const v0, -0x629d3544

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321902
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321903
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321904
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->h:I

    return v0
.end method
