.class public final Lcom/facebook/graphql/model/GraphQLAlbum;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAlbum$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAlbum$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public g:Z

.field public h:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public m:J

.field public n:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:J

.field public v:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 305937
    const-class v0, Lcom/facebook/graphql/model/GraphQLAlbum$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 305938
    const-class v0, Lcom/facebook/graphql/model/GraphQLAlbum$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 305939
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 305940
    return-void
.end method

.method public constructor <init>(LX/4Vp;)V
    .locals 2

    .prologue
    .line 305941
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 305942
    iget-object v0, p1, LX/4Vp;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305943
    iget-object v0, p1, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 305944
    iget-boolean v0, p1, LX/4Vp;->d:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->g:Z

    .line 305945
    iget-object v0, p1, LX/4Vp;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 305946
    iget-boolean v0, p1, LX/4Vp;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->i:Z

    .line 305947
    iget-boolean v0, p1, LX/4Vp;->g:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->j:Z

    .line 305948
    iget-boolean v0, p1, LX/4Vp;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->k:Z

    .line 305949
    iget-object v0, p1, LX/4Vp;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->l:Ljava/util/List;

    .line 305950
    iget-wide v0, p1, LX/4Vp;->j:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->m:J

    .line 305951
    iget-object v0, p1, LX/4Vp;->k:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->n:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305952
    iget-object v0, p1, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 305953
    iget-object v0, p1, LX/4Vp;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->p:Ljava/lang/String;

    .line 305954
    iget-object v0, p1, LX/4Vp;->n:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->q:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 305955
    iget-object v0, p1, LX/4Vp;->o:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->r:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 305956
    iget-object v0, p1, LX/4Vp;->p:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->s:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 305957
    iget-object v0, p1, LX/4Vp;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305958
    iget-wide v0, p1, LX/4Vp;->r:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->u:J

    .line 305959
    iget-object v0, p1, LX/4Vp;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->v:Ljava/lang/String;

    .line 305960
    iget-object v0, p1, LX/4Vp;->t:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->w:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305961
    iget-object v0, p1, LX/4Vp;->u:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->x:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 305962
    iget-object v0, p1, LX/4Vp;->v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->y:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 305963
    iget-object v0, p1, LX/4Vp;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305964
    iget-object v0, p1, LX/4Vp;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305965
    iget-object v0, p1, LX/4Vp;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->B:Ljava/lang/String;

    .line 305966
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305967
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305968
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->v:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->v:Ljava/lang/String;

    .line 305969
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305970
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->w:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305971
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->w:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->w:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305972
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->w:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305973
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->x:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305974
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->x:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->x:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 305975
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->x:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306113
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->y:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306114
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->y:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->y:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 306115
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->y:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305976
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305977
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305978
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305906
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305907
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305908
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305979
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305980
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->B:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->B:Ljava/lang/String;

    .line 305981
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 305982
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 305983
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 305984
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->m()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 305985
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 305986
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 305987
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 305988
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 305989
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 305990
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->w()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 305991
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 305992
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 305993
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->A()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 305994
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 305995
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 305996
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 305997
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 305998
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 305999
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->G()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 306000
    const/16 v5, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 306001
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 306002
    const/4 v5, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v2, v6, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 306003
    const/4 v2, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->l()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->a(IZ)V

    .line 306004
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 306005
    const/4 v2, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->n()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 306006
    const/4 v2, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->o()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 306007
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->p()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 306008
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 306009
    const/16 v3, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->r()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 306010
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 306011
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 306012
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 306013
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 306014
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 306015
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 306016
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 306017
    const/16 v3, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->z()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 306018
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 306019
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306020
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306021
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306022
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306023
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306024
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306025
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306026
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 306027
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 306028
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306030
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 306031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 306032
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306033
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 306034
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->m()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 306035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->m()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 306036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->m()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 306037
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306038
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 306039
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 306040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 306041
    if-eqz v2, :cond_2

    .line 306042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306043
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLAlbum;->l:Ljava/util/List;

    move-object v1, v0

    .line 306044
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 306045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 306046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 306047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306048
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->n:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 306049
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 306050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 306051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 306052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306053
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 306054
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 306055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 306057
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306058
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->q:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306059
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->w()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 306060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->w()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->w()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 306062
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306063
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->r:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306064
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 306065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 306066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 306067
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306068
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->s:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 306069
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 306070
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 306072
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306073
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306074
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 306075
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 306076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 306077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306078
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->w:Lcom/facebook/graphql/model/GraphQLActor;

    .line 306079
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 306080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 306082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306083
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->x:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306084
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 306085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 306086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 306087
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306088
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->y:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 306089
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 306090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 306092
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306093
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306094
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 306095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 306097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 306098
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAlbum;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306099
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306100
    if-nez v1, :cond_e

    :goto_0
    return-object p0

    :cond_e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306101
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 306102
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 306103
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->g:Z

    .line 306104
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->i:Z

    .line 306105
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->j:Z

    .line 306106
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->k:Z

    .line 306107
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->m:J

    .line 306108
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->u:J

    .line 306109
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 305936
    const v0, 0x3c68e4f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306110
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306111
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 306112
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305885
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305886
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 305887
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->f:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305888
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305889
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305890
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->g:Z

    return v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305891
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305892
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 305893
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305894
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305895
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305896
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->i:Z

    return v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305897
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305898
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305899
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->j:Z

    return v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305900
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305901
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305902
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->k:Z

    return v0
.end method

.method public final q()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305903
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305904
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->l:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->l:Ljava/util/List;

    .line 305905
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final r()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 305909
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305910
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305911
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->m:J

    return-wide v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305912
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->n:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305913
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->n:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->n:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305914
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->n:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305915
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305916
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 305917
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305918
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305919
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->p:Ljava/lang/String;

    .line 305920
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305921
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->q:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305922
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->q:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->q:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 305923
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->q:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305924
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->r:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305925
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->r:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->r:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 305926
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->r:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305927
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->s:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305928
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->s:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->s:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 305929
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->s:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305930
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305931
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305932
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final z()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305933
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305934
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305935
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAlbum;->u:J

    return-wide v0
.end method
