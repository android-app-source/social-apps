.class public final Lcom/facebook/graphql/model/GraphQLProfile;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLProfile$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLProfile$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Z

.field public aB:Z

.field public aC:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:I

.field public aG:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Z

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public aa:Z

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public ah:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:D

.field public ao:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public ap:Z

.field public aq:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public ar:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public as:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public at:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public au:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Z

.field public aw:Z

.field public ax:Z

.field public ay:Z

.field public az:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBylineFragment;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public o:D

.field public p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public u:I

.field public v:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

.field public w:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 264273
    const-class v0, Lcom/facebook/graphql/model/GraphQLProfile$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 264272
    const-class v0, Lcom/facebook/graphql/model/GraphQLProfile$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 264270
    const/16 v0, 0x60

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 264271
    return-void
.end method

.method public constructor <init>(LX/25F;)V
    .locals 2

    .prologue
    .line 264183
    const/16 v0, 0x60

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 264184
    iget-object v0, p1, LX/25F;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 264185
    iget-object v0, p1, LX/25F;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->g:Ljava/util/List;

    .line 264186
    iget-object v0, p1, LX/25F;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264187
    iget-object v0, p1, LX/25F;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->i:Ljava/util/List;

    .line 264188
    iget-boolean v0, p1, LX/25F;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->j:Z

    .line 264189
    iget-boolean v0, p1, LX/25F;->g:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->k:Z

    .line 264190
    iget-boolean v0, p1, LX/25F;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->l:Z

    .line 264191
    iget-boolean v0, p1, LX/25F;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->m:Z

    .line 264192
    iget-boolean v0, p1, LX/25F;->j:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aH:Z

    .line 264193
    iget-object v0, p1, LX/25F;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->n:Ljava/util/List;

    .line 264194
    iget-wide v0, p1, LX/25F;->l:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->o:D

    .line 264195
    iget-object v0, p1, LX/25F;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aK:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 264196
    iget-object v0, p1, LX/25F;->n:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 264197
    iget-object v0, p1, LX/25F;->o:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 264198
    iget-boolean v0, p1, LX/25F;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->r:Z

    .line 264199
    iget-object v0, p1, LX/25F;->q:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->s:Ljava/util/List;

    .line 264200
    iget-object v0, p1, LX/25F;->r:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->t:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 264201
    iget v0, p1, LX/25F;->s:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->u:I

    .line 264202
    iget-object v0, p1, LX/25F;->t:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->v:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 264203
    iget-object v0, p1, LX/25F;->u:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264204
    iget-object v0, p1, LX/25F;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 264205
    iget-object v0, p1, LX/25F;->w:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 264206
    iget-object v0, p1, LX/25F;->x:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 264207
    iget-object v0, p1, LX/25F;->y:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->A:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 264208
    iget-object v0, p1, LX/25F;->z:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aJ:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 264209
    iget-object v0, p1, LX/25F;->A:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->B:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 264210
    iget v0, p1, LX/25F;->B:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->C:I

    .line 264211
    iget-object v0, p1, LX/25F;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->D:Ljava/lang/String;

    .line 264212
    iget-object v0, p1, LX/25F;->D:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->E:Lcom/facebook/graphql/model/GraphQLUser;

    .line 264213
    iget-boolean v0, p1, LX/25F;->E:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ay:Z

    .line 264214
    iget-boolean v0, p1, LX/25F;->F:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aA:Z

    .line 264215
    iget-boolean v0, p1, LX/25F;->G:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ax:Z

    .line 264216
    iget-boolean v0, p1, LX/25F;->H:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->av:Z

    .line 264217
    iget-boolean v0, p1, LX/25F;->I:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->F:Z

    .line 264218
    iget-boolean v0, p1, LX/25F;->J:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->G:Z

    .line 264219
    iget-boolean v0, p1, LX/25F;->K:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->H:Z

    .line 264220
    iget-boolean v0, p1, LX/25F;->L:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->I:Z

    .line 264221
    iget-boolean v0, p1, LX/25F;->M:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aw:Z

    .line 264222
    iget-boolean v0, p1, LX/25F;->N:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->J:Z

    .line 264223
    iget-boolean v0, p1, LX/25F;->O:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aB:Z

    .line 264224
    iget-boolean v0, p1, LX/25F;->P:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->K:Z

    .line 264225
    iget-boolean v0, p1, LX/25F;->Q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->L:Z

    .line 264226
    iget-boolean v0, p1, LX/25F;->R:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->M:Z

    .line 264227
    iget-object v0, p1, LX/25F;->S:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->N:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 264228
    iget-object v0, p1, LX/25F;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->O:Ljava/lang/String;

    .line 264229
    iget-object v0, p1, LX/25F;->U:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->P:Ljava/util/List;

    .line 264230
    iget-object v0, p1, LX/25F;->V:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Q:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 264231
    iget-object v0, p1, LX/25F;->W:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->R:Lcom/facebook/graphql/model/GraphQLPage;

    .line 264232
    iget-object v0, p1, LX/25F;->X:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->S:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 264233
    iget-object v0, p1, LX/25F;->Y:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aD:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 264234
    iget-object v0, p1, LX/25F;->Z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->T:Ljava/lang/String;

    .line 264235
    iget-object v0, p1, LX/25F;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->U:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 264236
    iget-object v0, p1, LX/25F;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264237
    iget-object v0, p1, LX/25F;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264238
    iget-object v0, p1, LX/25F;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264239
    iget-object v0, p1, LX/25F;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264240
    iget-object v0, p1, LX/25F;->af:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264241
    iget-object v0, p1, LX/25F;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264242
    iget-boolean v0, p1, LX/25F;->ah:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aa:Z

    .line 264243
    iget-object v0, p1, LX/25F;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264244
    iget-object v0, p1, LX/25F;->aj:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ab:Ljava/lang/String;

    .line 264245
    iget-object v0, p1, LX/25F;->ak:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ac:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 264246
    iget-object v0, p1, LX/25F;->al:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->au:Ljava/lang/String;

    .line 264247
    iget-object v0, p1, LX/25F;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264248
    iget-object v0, p1, LX/25F;->an:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ae:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 264249
    iget-object v0, p1, LX/25F;->ao:Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->af:Lcom/facebook/graphql/model/GraphQLName;

    .line 264250
    iget-object v0, p1, LX/25F;->ap:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ag:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 264251
    iget-object v0, p1, LX/25F;->aq:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264252
    iget-object v0, p1, LX/25F;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ai:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264253
    iget-object v0, p1, LX/25F;->as:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aj:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 264254
    iget-object v0, p1, LX/25F;->at:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ak:Ljava/lang/String;

    .line 264255
    iget-object v0, p1, LX/25F;->au:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->al:Ljava/lang/String;

    .line 264256
    iget v0, p1, LX/25F;->av:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aF:I

    .line 264257
    iget-object v0, p1, LX/25F;->aw:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aG:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 264258
    iget-object v0, p1, LX/25F;->ax:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->am:Ljava/lang/String;

    .line 264259
    iget-object v0, p1, LX/25F;->ay:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aI:Ljava/lang/String;

    .line 264260
    iget-wide v0, p1, LX/25F;->az:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->an:D

    .line 264261
    iget-object v0, p1, LX/25F;->aA:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ao:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 264262
    iget-boolean v0, p1, LX/25F;->aB:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ap:Z

    .line 264263
    iget-object v0, p1, LX/25F;->aC:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aq:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 264264
    iget-object v0, p1, LX/25F;->aD:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ar:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 264265
    iget-object v0, p1, LX/25F;->aE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->as:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 264266
    iget-object v0, p1, LX/25F;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264267
    iget-object v0, p1, LX/25F;->aG:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->at:Ljava/util/List;

    .line 264268
    iget-object v0, p1, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 264269
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 3

    .prologue
    .line 264177
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ao:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 264178
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264179
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264180
    if-eqz v0, :cond_0

    .line 264181
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x46

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 264182
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 3

    .prologue
    .line 264171
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->as:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 264172
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264173
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264174
    if-eqz v0, :cond_0

    .line 264175
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x4a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 264176
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    .line 264165
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 264166
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264167
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264168
    if-eqz v0, :cond_0

    .line 264169
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 264170
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 3

    .prologue
    .line 264159
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aq:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 264160
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264161
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264162
    if-eqz v0, :cond_0

    .line 264163
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x48

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 264164
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 3

    .prologue
    .line 264153
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ac:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 264154
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264155
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264156
    if-eqz v0, :cond_0

    .line 264157
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 264158
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 3

    .prologue
    .line 264147
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ag:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 264148
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264149
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264150
    if-eqz v0, :cond_0

    .line 264151
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x3e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 264152
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 264141
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->r:Z

    .line 264142
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264143
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264144
    if-eqz v0, :cond_0

    .line 264145
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 264146
    :cond_0
    return-void
.end method

.method private aA()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ac:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ac:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ac:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 264098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ac:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method private aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264135
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264136
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264137
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aC()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264132
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ae:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264133
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ae:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ae:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 264134
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ae:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private aD()Lcom/facebook/graphql/model/GraphQLName;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264129
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->af:Lcom/facebook/graphql/model/GraphQLName;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264130
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->af:Lcom/facebook/graphql/model/GraphQLName;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/model/GraphQLName;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->af:Lcom/facebook/graphql/model/GraphQLName;

    .line 264131
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->af:Lcom/facebook/graphql/model/GraphQLName;

    return-object v0
.end method

.method private aE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264126
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ai:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264127
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ai:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ai:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264128
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ai:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aF()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264123
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aj:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264124
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aj:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aj:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 264125
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aj:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    return-object v0
.end method

.method private aG()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264120
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ak:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264121
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ak:Ljava/lang/String;

    const/16 v1, 0x42

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ak:Ljava/lang/String;

    .line 264122
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ak:Ljava/lang/String;

    return-object v0
.end method

.method private aH()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264117
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->al:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264118
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->al:Ljava/lang/String;

    const/16 v1, 0x43

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->al:Ljava/lang/String;

    .line 264119
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->al:Ljava/lang/String;

    return-object v0
.end method

.method private aI()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264114
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264115
    const/16 v0, 0x8

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264116
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->an:D

    return-wide v0
.end method

.method private aJ()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264111
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->at:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264112
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->at:Ljava/util/List;

    const/16 v1, 0x4b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->at:Ljava/util/List;

    .line 264113
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->at:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private aK()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264108
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->au:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264109
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->au:Ljava/lang/String;

    const/16 v1, 0x4c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->au:Ljava/lang/String;

    .line 264110
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->au:Ljava/lang/String;

    return-object v0
.end method

.method private aL()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x56

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aM()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264102
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264103
    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264104
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aF:I

    return v0
.end method

.method private aN()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aG:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aG:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aG:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 264101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aG:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    return-object v0
.end method

.method private aO()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264280
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264281
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aI:Ljava/lang/String;

    const/16 v1, 0x5c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aI:Ljava/lang/String;

    .line 264282
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aI:Ljava/lang/String;

    return-object v0
.end method

.method private ae()Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264277
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264278
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 264279
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    return-object v0
.end method

.method private af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264352
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264353
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264354
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private ag()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBylineFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264349
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264350
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->i:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLBylineFragment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->i:Ljava/util/List;

    .line 264351
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ah()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264346
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264347
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264348
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->o:D

    return-wide v0
.end method

.method private ai()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264343
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264344
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->s:Ljava/util/List;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->s:Ljava/util/List;

    .line 264345
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->s:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private aj()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264340
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264341
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264342
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->u:I

    return v0
.end method

.method private ak()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264337
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->v:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264338
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->v:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->v:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 264339
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->v:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    return-object v0
.end method

.method private al()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264334
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->w:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264335
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->w:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264336
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->w:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private am()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264331
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264332
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 264333
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private an()Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264328
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264329
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 264330
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    return-object v0
.end method

.method private ao()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264325
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->E:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264326
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->E:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->E:Lcom/facebook/graphql/model/GraphQLUser;

    .line 264327
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->E:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method private ap()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264322
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264323
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264324
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->F:Z

    return v0
.end method

.method private aq()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x4

    .line 264355
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264356
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264357
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->I:Z

    return v0
.end method

.method private ar()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264319
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264320
    const/4 v0, 0x4

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264321
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->L:Z

    return v0
.end method

.method private as()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264316
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->N:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264317
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->N:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->N:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 264318
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->N:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    return-object v0
.end method

.method private at()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264313
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->P:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264314
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->P:Ljava/util/List;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->P:Ljava/util/List;

    .line 264315
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->P:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private au()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264310
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->S:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264311
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->S:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->S:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 264312
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->S:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    return-object v0
.end method

.method private av()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264307
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->U:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264308
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->U:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->U:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 264309
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->U:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private aw()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264304
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->X:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264305
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->X:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264306
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->X:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ax()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264301
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264302
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 264303
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ay()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264298
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264299
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264300
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aa:Z

    return v0
.end method

.method private az()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264295
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ab:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264296
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ab:Ljava/lang/String;

    const/16 v1, 0x39

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ab:Ljava/lang/String;

    .line 264297
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ab:Ljava/lang/String;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 264289
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ap:Z

    .line 264290
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 264291
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 264292
    if-eqz v0, :cond_0

    .line 264293
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x47

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 264294
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264286
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264287
    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264288
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->J:Z

    return v0
.end method

.method public final B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264283
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264284
    const/4 v0, 0x4

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264285
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->K:Z

    return v0
.end method

.method public final C()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264138
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264139
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264140
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->M:Z

    return v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264274
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264275
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->O:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->O:Ljava/lang/String;

    .line 264276
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263943
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Q:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Q:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Q:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 263945
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Q:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263940
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->R:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263941
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->R:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->R:Lcom/facebook/graphql/model/GraphQLPage;

    .line 263942
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->R:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263937
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->T:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263938
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->T:Ljava/lang/String;

    const/16 v1, 0x30

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->T:Ljava/lang/String;

    .line 263939
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263934
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->V:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263935
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->V:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263936
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->V:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263931
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->W:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263932
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->W:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263933
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->W:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263925
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ag:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263926
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ag:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ag:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 263927
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ag:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263922
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263923
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263924
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263919
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->am:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263920
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->am:Ljava/lang/String;

    const/16 v1, 0x44

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->am:Ljava/lang/String;

    .line 263921
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->am:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263916
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ao:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263917
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ao:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ao:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 263918
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ao:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final O()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263913
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263914
    const/16 v0, 0x8

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263915
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ap:Z

    return v0
.end method

.method public final P()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263910
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aq:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263911
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aq:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aq:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 263912
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aq:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263907
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ar:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263908
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ar:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x49

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ar:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 263909
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ar:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263567
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->as:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263568
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->as:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->as:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 263569
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->as:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final S()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263571
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263572
    const/16 v0, 0x9

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263573
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->av:Z

    return v0
.end method

.method public final T()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263574
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263575
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263576
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aw:Z

    return v0
.end method

.method public final U()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263577
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263578
    const/16 v0, 0xa

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263579
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ax:Z

    return v0
.end method

.method public final V()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263580
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263581
    const/16 v0, 0xa

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263582
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ay:Z

    return v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->az:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->az:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x53

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->az:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final X()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263586
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263587
    const/16 v0, 0xa

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263588
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aA:Z

    return v0
.end method

.method public final Y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263589
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263590
    const/16 v0, 0xa

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263591
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aB:Z

    return v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aD:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aD:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x57

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aD:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 263594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aD:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 50

    .prologue
    .line 263595
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 263596
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 263597
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ae()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 263598
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->k()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 263599
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 263600
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ag()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 263601
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->p()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 263602
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 263603
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ai()LX/0Px;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 263604
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 263605
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->am()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 263606
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->an()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 263607
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 263608
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 263609
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 263610
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ao()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 263611
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->as()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 263612
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 263613
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->at()LX/0Px;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v19

    .line 263614
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->E()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 263615
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->F()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 263616
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->au()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 263617
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->G()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 263618
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->av()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 263619
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 263620
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 263621
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 263622
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 263623
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 263624
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->az()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 263625
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 263626
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aC()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 263627
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aD()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 263628
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 263629
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 263630
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aF()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 263631
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aG()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 263632
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aH()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    .line 263633
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    .line 263634
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aJ()LX/0Px;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v40

    .line 263635
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aK()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v41

    .line 263636
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 263637
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 263638
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 263639
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 263640
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aN()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 263641
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aO()Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v47

    .line 263642
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 263643
    const/16 v49, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 263644
    const/16 v49, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 263645
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 263646
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 263647
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 263648
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 263649
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->l()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263650
    const/16 v2, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->m()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263651
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->n()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263652
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->o()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263653
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 263654
    const/16 v3, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ah()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 263655
    const/16 v3, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->q()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263656
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 263657
    const/16 v2, 0x12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263658
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 263659
    const/16 v3, 0x14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->t()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263660
    const/16 v2, 0x15

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aj()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 263661
    const/16 v3, 0x16

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ak()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263662
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 263663
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 263664
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 263665
    const/16 v3, 0x1a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263666
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 263667
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 263668
    const/16 v2, 0x1d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->x()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 263669
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 263670
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263671
    const/16 v2, 0x21

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ap()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263672
    const/16 v2, 0x22

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263673
    const/16 v2, 0x23

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263674
    const/16 v2, 0x24

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aq()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263675
    const/16 v2, 0x25

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263676
    const/16 v2, 0x26

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->B()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263677
    const/16 v2, 0x27

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ar()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263678
    const/16 v2, 0x28

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->C()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263679
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263680
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263681
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263682
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263683
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263684
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263685
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263686
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263687
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263688
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263689
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263690
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263691
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263692
    const/16 v2, 0x38

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ay()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263693
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263694
    const/16 v3, 0x3a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aA()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263695
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263696
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263697
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263698
    const/16 v3, 0x3e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263699
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263700
    const/16 v2, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263701
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263702
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263703
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263704
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263705
    const/16 v3, 0x45

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aI()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 263706
    const/16 v3, 0x46

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->N()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263707
    const/16 v2, 0x47

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->O()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263708
    const/16 v3, 0x48

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->P()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v2, v4, :cond_8

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263709
    const/16 v3, 0x49

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->Q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_9

    const/4 v2, 0x0

    :goto_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263710
    const/16 v3, 0x4a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->R()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v4, :cond_a

    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263711
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263712
    const/16 v2, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263713
    const/16 v2, 0x4e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->S()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263714
    const/16 v2, 0x50

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->T()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263715
    const/16 v2, 0x51

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->U()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263716
    const/16 v2, 0x52

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->V()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263717
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263718
    const/16 v2, 0x54

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->X()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263719
    const/16 v2, 0x55

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->Y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263720
    const/16 v2, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263721
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263722
    const/16 v2, 0x58

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263723
    const/16 v2, 0x59

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aM()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 263724
    const/16 v2, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263725
    const/16 v2, 0x5b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ab()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 263726
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263727
    const/16 v2, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 263728
    const/16 v3, 0x5e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ad()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v2, v4, :cond_b

    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 263729
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 263730
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 263731
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 263732
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->q()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    goto/16 :goto_1

    .line 263733
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->t()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    goto/16 :goto_2

    .line 263734
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ak()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v2

    goto/16 :goto_3

    .line 263735
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    goto/16 :goto_4

    .line 263736
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aA()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    goto/16 :goto_5

    .line 263737
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    goto/16 :goto_6

    .line 263738
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->N()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    goto/16 :goto_7

    .line 263739
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->P()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    goto/16 :goto_8

    .line 263740
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->Q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto/16 :goto_9

    .line 263741
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->R()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    goto/16 :goto_a

    .line 263742
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ad()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    goto :goto_b
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 263743
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 263744
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ae()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263745
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ae()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 263746
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ae()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 263747
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263748
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 263749
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 263750
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263751
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->af()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 263752
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263753
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263754
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ag()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 263755
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ag()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 263756
    if-eqz v2, :cond_2

    .line 263757
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263758
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLProfile;->i:Ljava/util/List;

    move-object v1, v0

    .line 263759
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 263760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 263761
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 263762
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263763
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 263764
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 263765
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263766
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 263767
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263768
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263769
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->am()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 263770
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->am()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 263771
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->am()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 263772
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263773
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 263774
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->an()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 263775
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->an()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 263776
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->an()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 263777
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263778
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->y:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 263779
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 263780
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 263781
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->v()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 263782
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263783
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->A:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 263784
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 263785
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 263786
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 263787
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263788
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->aJ:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 263789
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 263790
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 263791
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 263792
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263793
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->B:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 263794
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ao()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 263795
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ao()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 263796
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ao()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 263797
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263798
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->E:Lcom/facebook/graphql/model/GraphQLUser;

    .line 263799
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->as()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 263800
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->as()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 263801
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->as()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 263802
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263803
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->N:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 263804
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->E()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 263805
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->E()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 263806
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->E()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 263807
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263808
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->Q:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 263809
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->F()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 263810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->F()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 263811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->F()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 263812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263813
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->R:Lcom/facebook/graphql/model/GraphQLPage;

    .line 263814
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->au()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 263815
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->au()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 263816
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->au()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 263817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263818
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->S:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 263819
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 263820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 263821
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 263822
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263823
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->aD:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 263824
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->av()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 263825
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->av()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 263826
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->av()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 263827
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263828
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->U:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 263829
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 263830
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263831
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 263832
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263833
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263834
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 263835
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263836
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 263837
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263838
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263839
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 263840
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 263842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263843
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263844
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 263845
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263846
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 263847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263848
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263849
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 263850
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263851
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 263852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263853
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263854
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 263855
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 263857
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263858
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263859
    :cond_16
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 263860
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263861
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 263862
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263863
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263864
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 263865
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263866
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aB()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 263867
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263868
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263869
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aC()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 263870
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aC()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 263871
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aC()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 263872
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263873
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->ae:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 263874
    :cond_19
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aD()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 263875
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aD()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    .line 263876
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aD()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 263877
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263878
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->af:Lcom/facebook/graphql/model/GraphQLName;

    .line 263879
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 263880
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263881
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 263882
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263883
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263884
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 263885
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263886
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 263887
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263888
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->ai:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263889
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aF()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 263890
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aF()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 263891
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aF()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 263892
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263893
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->aj:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 263894
    :cond_1d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aN()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 263895
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aN()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 263896
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aN()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 263897
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263898
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->aG:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 263899
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 263900
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263901
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 263902
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 263903
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfile;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 263904
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 263905
    if-nez v1, :cond_20

    :goto_0
    return-object p0

    :cond_20
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 263906
    new-instance v0, LX/4YO;

    invoke-direct {v0, p1}, LX/4YO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 263985
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 263986
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->j:Z

    .line 263987
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->k:Z

    .line 263988
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->l:Z

    .line 263989
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->m:Z

    .line 263990
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->o:D

    .line 263991
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->r:Z

    .line 263992
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->u:I

    .line 263993
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->C:I

    .line 263994
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->F:Z

    .line 263995
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->G:Z

    .line 263996
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->H:Z

    .line 263997
    const/16 v0, 0x24

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->I:Z

    .line 263998
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->J:Z

    .line 263999
    const/16 v0, 0x26

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->K:Z

    .line 264000
    const/16 v0, 0x27

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->L:Z

    .line 264001
    const/16 v0, 0x28

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->M:Z

    .line 264002
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aa:Z

    .line 264003
    const/16 v0, 0x45

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->an:D

    .line 264004
    const/16 v0, 0x47

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ap:Z

    .line 264005
    const/16 v0, 0x4e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->av:Z

    .line 264006
    const/16 v0, 0x50

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aw:Z

    .line 264007
    const/16 v0, 0x51

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ax:Z

    .line 264008
    const/16 v0, 0x52

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->ay:Z

    .line 264009
    const/16 v0, 0x54

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aA:Z

    .line 264010
    const/16 v0, 0x55

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aB:Z

    .line 264011
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aF:I

    .line 264012
    const/16 v0, 0x5b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aH:Z

    .line 264013
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 264062
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264064
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264065
    const/16 v0, 0x12

    iput v0, p2, LX/18L;->c:I

    .line 264066
    :goto_0
    return-void

    .line 264067
    :cond_0
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264070
    const/16 v0, 0x1a

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 264071
    :cond_1
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264072
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->aA()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264074
    const/16 v0, 0x3a

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 264075
    :cond_2
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 264076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264077
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264078
    const/16 v0, 0x3e

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 264079
    :cond_3
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 264080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->N()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264082
    const/16 v0, 0x46

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 264083
    :cond_4
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->O()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264086
    const/16 v0, 0x47

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 264087
    :cond_5
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 264088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->P()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264090
    const/16 v0, 0x48

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 264091
    :cond_6
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 264092
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->R()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 264093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 264094
    const/16 v0, 0x4a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 264095
    :cond_7
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 264045
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264046
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLProfile;->a(Z)V

    .line 264047
    :cond_0
    :goto_0
    return-void

    .line 264048
    :cond_1
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264049
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProfile;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 264050
    :cond_2
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 264051
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProfile;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_0

    .line 264052
    :cond_3
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 264053
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProfile;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0

    .line 264054
    :cond_4
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264055
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProfile;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_0

    .line 264056
    :cond_5
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 264057
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b(Z)V

    goto :goto_0

    .line 264058
    :cond_6
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 264059
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProfile;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0

    .line 264060
    :cond_7
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264061
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProfile;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto :goto_0
.end method

.method public final aa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264042
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264043
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x58

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264044
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ab()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264039
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264040
    const/16 v0, 0xb

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264041
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aH:Z

    return v0
.end method

.method public final ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264036
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aJ:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264037
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aJ:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    const/16 v1, 0x5d

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aJ:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 264038
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aJ:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    return-object v0
.end method

.method public final ad()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264033
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aK:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264034
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aK:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aK:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 264035
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->aK:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264030
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264031
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->D:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->D:Ljava/lang/String;

    .line 264032
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 264029
    const v0, 0x50c72189

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264023
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 264024
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 264025
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLProfile;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 264026
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 264027
    const/4 v0, 0x0

    .line 264028
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264020
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264021
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->g:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->g:Ljava/util/List;

    .line 264022
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264017
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264018
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264019
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->j:Z

    return v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 264014
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 264015
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 264016
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->k:Z

    return v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263946
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263947
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263948
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->l:Z

    return v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263982
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263983
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263984
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->m:Z

    return v0
.end method

.method public final p()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263979
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263980
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->n:Ljava/util/List;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->n:Ljava/util/List;

    .line 263981
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263976
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263977
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 263978
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263973
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263974
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 263975
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 263970
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263971
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263972
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->r:Z

    return v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263967
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->t:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263968
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->t:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->t:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 263969
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->t:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263964
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263965
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 263966
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->z:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263961
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->A:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263962
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->A:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->A:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 263963
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->A:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263958
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->B:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263959
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->B:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->B:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 263960
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->B:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    return-object v0
.end method

.method public final x()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 263955
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263956
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263957
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->C:I

    return v0
.end method

.method public final y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263952
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263953
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263954
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->G:Z

    return v0
.end method

.method public final z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 263949
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 263950
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 263951
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProfile;->H:Z

    return v0
.end method
