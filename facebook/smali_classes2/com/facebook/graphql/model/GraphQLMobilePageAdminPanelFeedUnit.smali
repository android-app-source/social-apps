.class public final Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 202220
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 202221
    const-class v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 202222
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 202223
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x5b14d858

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 202224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->q:LX/0x2;

    .line 202225
    return-void
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->i:Ljava/lang/String;

    .line 202228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202229
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202230
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 202231
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202232
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202233
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k:Ljava/lang/String;

    .line 202234
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 202235
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202236
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202237
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g:Ljava/lang/String;

    .line 202238
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202239
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202240
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202241
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 202242
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 202243
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->q:LX/0x2;

    if-nez v0, :cond_0

    .line 202244
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->q:LX/0x2;

    .line 202245
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->q:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 202190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 202191
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 202246
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 202247
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 202248
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 202249
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 202250
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 202251
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 202252
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 202253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 202254
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 202255
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 202256
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 202257
    const/16 v2, 0xc

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 202258
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 202259
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 202260
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 202261
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 202262
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 202263
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 202264
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 202265
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 202266
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 202267
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 202268
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 202269
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 202270
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 202192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 202193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 202194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 202195
    if-eqz v1, :cond_5

    .line 202196
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 202197
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n:Ljava/util/List;

    move-object v1, v0

    .line 202198
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 202201
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 202202
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202203
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 202204
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 202205
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 202206
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 202207
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 202208
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 202209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 202210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 202211
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 202212
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPage;

    .line 202213
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 202214
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 202216
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 202217
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202218
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 202219
    if-nez v1, :cond_4

    :goto_1
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 202156
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->h:J

    .line 202157
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 202158
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 202159
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->h:J

    .line 202160
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202161
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->m:Ljava/lang/String;

    .line 202164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 202167
    :goto_0
    return-object v0

    .line 202168
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 202169
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 202170
    const v0, -0x5b14d858

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202171
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202172
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->f:Ljava/lang/String;

    .line 202173
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 202174
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202175
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202176
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202177
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202178
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202179
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n:Ljava/util/List;

    .line 202180
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 202181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 202182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 202183
    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 202186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202187
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202188
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPage;

    .line 202189
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method
