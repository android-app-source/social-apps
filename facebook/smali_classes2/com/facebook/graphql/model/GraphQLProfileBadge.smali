.class public final Lcom/facebook/graphql/model/GraphQLProfileBadge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLProfileBadge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLProfileBadge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:J

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 307163
    const-class v0, Lcom/facebook/graphql/model/GraphQLProfileBadge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 307162
    const-class v0, Lcom/facebook/graphql/model/GraphQLProfileBadge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 307160
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 307161
    return-void
.end method

.method private k()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307157
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307158
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307159
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->f:J

    return-wide v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307154
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307155
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->g:Ljava/lang/String;

    .line 307156
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307124
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307125
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->h:Ljava/lang/String;

    .line 307126
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 307143
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 307144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 307145
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 307146
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 307147
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 307148
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 307149
    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->k()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 307150
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 307151
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 307152
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 307153
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 307135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 307136
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 307138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 307139
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 307140
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProfileBadge;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 307141
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 307142
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307134
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 307131
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 307132
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->f:J

    .line 307133
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 307130
    const v0, 0x511d179a

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307127
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307128
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 307129
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProfileBadge;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    return-object v0
.end method
