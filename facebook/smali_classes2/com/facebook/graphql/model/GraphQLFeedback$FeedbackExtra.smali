.class public final Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;
.super Lcom/facebook/graphql/model/extras/BaseExtra;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 338699
    new-instance v0, LX/208;

    invoke-direct {v0}, LX/208;-><init>()V

    sput-object v0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 338700
    invoke-direct {p0}, Lcom/facebook/graphql/model/extras/BaseExtra;-><init>()V

    .line 338701
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    .line 338702
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 338703
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/extras/BaseExtra;-><init>(B)V

    .line 338704
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    .line 338705
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/186;)I
    .locals 3

    .prologue
    .line 338706
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/graphql/model/extras/BaseExtra;->a(LX/186;)I

    move-result v0

    .line 338707
    iget-object v1, p0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    invoke-virtual {p1, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 338708
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 338709
    if-lez v0, :cond_0

    .line 338710
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 338711
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 338712
    invoke-virtual {p1}, LX/186;->d()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 338713
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/15i;I)V
    .locals 2

    .prologue
    .line 338714
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v0}, LX/15i;->g(II)I

    move-result v0

    .line 338715
    invoke-super {p0, p1, v0}, Lcom/facebook/graphql/model/extras/BaseExtra;->a(LX/15i;I)V

    .line 338716
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338717
    monitor-exit p0

    return-void

    .line 338718
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;)V
    .locals 1

    .prologue
    .line 338719
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    if-eq p1, v0, :cond_0

    .line 338720
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    .line 338721
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/graphql/model/extras/BaseExtra;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338722
    :cond_0
    monitor-exit p0

    return-void

    .line 338723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;
    .locals 1

    .prologue
    .line 338724
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->a:Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
