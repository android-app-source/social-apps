.class public final Lcom/facebook/graphql/model/GraphQLPhoto;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhoto$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhoto$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLVect2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Z

.field public E:I

.field public F:I

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:I

.field public aC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Z

.field public aN:Z

.field public aO:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:I

.field public aS:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aV:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Z

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:J

.field public aq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoEncoding;",
            ">;"
        }
    .end annotation
.end field

.field public ba:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Z

.field public bd:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:Z

.field public bg:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:J

.field public x:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 305270
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhoto$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 305004
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhoto$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 305268
    const/16 v0, 0x6e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 305269
    return-void
.end method

.method public constructor <init>(LX/4Xy;)V
    .locals 2

    .prologue
    .line 305159
    const/16 v0, 0x6e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 305160
    iget-object v0, p1, LX/4Xy;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->e:Ljava/lang/String;

    .line 305161
    iget-object v0, p1, LX/4Xy;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 305162
    iget-object v0, p1, LX/4Xy;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aU:Ljava/util/List;

    .line 305163
    iget-object v0, p1, LX/4Xy;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305164
    iget-object v0, p1, LX/4Xy;->f:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305165
    iget v0, p1, LX/4Xy;->g:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->i:I

    .line 305166
    iget-object v0, p1, LX/4Xy;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 305167
    iget-object v0, p1, LX/4Xy;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->k:Ljava/lang/String;

    .line 305168
    iget v0, p1, LX/4Xy;->j:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->l:I

    .line 305169
    iget-boolean v0, p1, LX/4Xy;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->m:Z

    .line 305170
    iget-boolean v0, p1, LX/4Xy;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->n:Z

    .line 305171
    iget-boolean v0, p1, LX/4Xy;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bc:Z

    .line 305172
    iget-boolean v0, p1, LX/4Xy;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->o:Z

    .line 305173
    iget-boolean v0, p1, LX/4Xy;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->p:Z

    .line 305174
    iget-boolean v0, p1, LX/4Xy;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->q:Z

    .line 305175
    iget-boolean v0, p1, LX/4Xy;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->r:Z

    .line 305176
    iget-boolean v0, p1, LX/4Xy;->r:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->s:Z

    .line 305177
    iget-boolean v0, p1, LX/4Xy;->s:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bf:Z

    .line 305178
    iget-boolean v0, p1, LX/4Xy;->t:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bg:Z

    .line 305179
    iget-boolean v0, p1, LX/4Xy;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->t:Z

    .line 305180
    iget-boolean v0, p1, LX/4Xy;->v:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->u:Z

    .line 305181
    iget-object v0, p1, LX/4Xy;->w:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 305182
    iget-object v0, p1, LX/4Xy;->x:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305183
    iget-wide v0, p1, LX/4Xy;->y:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->w:J

    .line 305184
    iget-object v0, p1, LX/4Xy;->z:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->x:Lcom/facebook/graphql/model/GraphQLStory;

    .line 305185
    iget-object v0, p1, LX/4Xy;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->y:Ljava/lang/String;

    .line 305186
    iget-object v0, p1, LX/4Xy;->B:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->z:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305187
    iget-object v0, p1, LX/4Xy;->C:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->A:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 305188
    iget-object v0, p1, LX/4Xy;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->B:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 305189
    iget-object v0, p1, LX/4Xy;->E:Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->C:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 305190
    iget-boolean v0, p1, LX/4Xy;->F:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->D:Z

    .line 305191
    iget v0, p1, LX/4Xy;->G:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->E:I

    .line 305192
    iget v0, p1, LX/4Xy;->H:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->F:I

    .line 305193
    iget-object v0, p1, LX/4Xy;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->G:Ljava/lang/String;

    .line 305194
    iget-object v0, p1, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305195
    iget-object v0, p1, LX/4Xy;->K:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305196
    iget-object v0, p1, LX/4Xy;->L:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305197
    iget-object v0, p1, LX/4Xy;->M:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305198
    iget-object v0, p1, LX/4Xy;->N:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305199
    iget-object v0, p1, LX/4Xy;->O:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305200
    iget-object v0, p1, LX/4Xy;->P:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305201
    iget-object v0, p1, LX/4Xy;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305202
    iget-object v0, p1, LX/4Xy;->R:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305203
    iget-object v0, p1, LX/4Xy;->S:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305204
    iget-object v0, p1, LX/4Xy;->T:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305205
    iget-object v0, p1, LX/4Xy;->U:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305206
    iget-object v0, p1, LX/4Xy;->V:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305207
    iget-object v0, p1, LX/4Xy;->W:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305208
    iget-object v0, p1, LX/4Xy;->X:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305209
    iget-object v0, p1, LX/4Xy;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305210
    iget-object v0, p1, LX/4Xy;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305211
    iget-object v0, p1, LX/4Xy;->aa:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305212
    iget-object v0, p1, LX/4Xy;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305213
    iget-object v0, p1, LX/4Xy;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305214
    iget-object v0, p1, LX/4Xy;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aa:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 305215
    iget-object v0, p1, LX/4Xy;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305216
    iget-boolean v0, p1, LX/4Xy;->af:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ac:Z

    .line 305217
    iget-boolean v0, p1, LX/4Xy;->ag:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ad:Z

    .line 305218
    iget-boolean v0, p1, LX/4Xy;->ah:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ae:Z

    .line 305219
    iget-boolean v0, p1, LX/4Xy;->ai:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->af:Z

    .line 305220
    iget-boolean v0, p1, LX/4Xy;->aj:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ag:Z

    .line 305221
    iget-object v0, p1, LX/4Xy;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305222
    iget-object v0, p1, LX/4Xy;->al:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305223
    iget-object v0, p1, LX/4Xy;->am:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aj:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305224
    iget-object v0, p1, LX/4Xy;->an:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305225
    iget-object v0, p1, LX/4Xy;->ao:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aW:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 305226
    iget-object v0, p1, LX/4Xy;->ap:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->al:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 305227
    iget-object v0, p1, LX/4Xy;->aq:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305228
    iget-object v0, p1, LX/4Xy;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305229
    iget-object v0, p1, LX/4Xy;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305230
    iget-wide v0, p1, LX/4Xy;->at:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ap:J

    .line 305231
    iget-object v0, p1, LX/4Xy;->au:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aq:Ljava/lang/String;

    .line 305232
    iget-object v0, p1, LX/4Xy;->av:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305233
    iget-object v0, p1, LX/4Xy;->aw:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->as:Ljava/lang/String;

    .line 305234
    iget-object v0, p1, LX/4Xy;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305235
    iget-object v0, p1, LX/4Xy;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->au:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305236
    iget-object v0, p1, LX/4Xy;->az:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->av:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 305237
    iget-object v0, p1, LX/4Xy;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aw:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305238
    iget-object v0, p1, LX/4Xy;->aB:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aY:Ljava/lang/String;

    .line 305239
    iget-object v0, p1, LX/4Xy;->aC:Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ax:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 305240
    iget-object v0, p1, LX/4Xy;->aD:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ay:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305241
    iget-object v0, p1, LX/4Xy;->aE:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->az:Ljava/util/List;

    .line 305242
    iget-object v0, p1, LX/4Xy;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aV:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305243
    iget-object v0, p1, LX/4Xy;->aG:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ:Ljava/lang/String;

    .line 305244
    iget-object v0, p1, LX/4Xy;->aH:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aA:Ljava/lang/String;

    .line 305245
    iget v0, p1, LX/4Xy;->aI:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aB:I

    .line 305246
    iget-object v0, p1, LX/4Xy;->aJ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aC:Ljava/lang/String;

    .line 305247
    iget-object v0, p1, LX/4Xy;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305248
    iget-object v0, p1, LX/4Xy;->aL:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aE:Ljava/lang/String;

    .line 305249
    iget-object v0, p1, LX/4Xy;->aM:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aF:Ljava/lang/String;

    .line 305250
    iget-object v0, p1, LX/4Xy;->aN:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aG:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 305251
    iget-object v0, p1, LX/4Xy;->aO:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->be:Ljava/lang/String;

    .line 305252
    iget-object v0, p1, LX/4Xy;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305253
    iget-object v0, p1, LX/4Xy;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305254
    iget-object v0, p1, LX/4Xy;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305255
    iget-object v0, p1, LX/4Xy;->aS:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aK:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 305256
    iget-object v0, p1, LX/4Xy;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305257
    iget-object v0, p1, LX/4Xy;->aU:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ba:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 305258
    iget-boolean v0, p1, LX/4Xy;->aV:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aM:Z

    .line 305259
    iget-boolean v0, p1, LX/4Xy;->aW:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aN:Z

    .line 305260
    iget-object v0, p1, LX/4Xy;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305261
    iget-object v0, p1, LX/4Xy;->aY:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aX:Ljava/lang/String;

    .line 305262
    iget-object v0, p1, LX/4Xy;->aZ:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aP:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 305263
    iget-object v0, p1, LX/4Xy;->ba:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ:Ljava/lang/String;

    .line 305264
    iget v0, p1, LX/4Xy;->bb:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aR:I

    .line 305265
    iget-object v0, p1, LX/4Xy;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305266
    iget-object v0, p1, LX/4Xy;->bd:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 305267
    return-void
.end method

.method private aW()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aX()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->I:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->I:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->I:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aY()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aZ()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305147
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->R:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305148
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->R:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305149
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->R:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ba()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305144
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->T:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305145
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->T:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305146
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->T:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bb()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305141
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->U:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305142
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->U:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305143
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->U:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bc()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305138
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305139
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305140
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bd()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305135
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305136
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305137
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ap:J

    return-wide v0
.end method

.method private be()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305132
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305133
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x53

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305134
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bf()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305129
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305130
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305131
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bg()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305081
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305082
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x55

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305083
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bh()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305123
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305124
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ:Ljava/lang/String;

    const/16 v1, 0x5c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ:Ljava/lang/String;

    .line 305125
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ:Ljava/lang/String;

    return-object v0
.end method

.method private bi()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305120
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aU:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305121
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aU:Ljava/util/List;

    const/16 v1, 0x60

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aU:Ljava/util/List;

    .line 305122
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aU:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bj()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305117
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aV:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305118
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aV:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aV:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305119
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aV:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private bk()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305114
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aX:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305115
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aX:Ljava/lang/String;

    const/16 v1, 0x63

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aX:Ljava/lang/String;

    .line 305116
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aX:Ljava/lang/String;

    return-object v0
.end method

.method private bl()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305111
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ba:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305112
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ba:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    const/16 v1, 0x66

    const-class v2, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ba:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 305113
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ba:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    return-object v0
.end method


# virtual methods
.method public final A()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305108
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305109
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305110
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->w:J

    return-wide v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->x:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->x:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->x:Lcom/facebook/graphql/model/GraphQLStory;

    .line 305107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->x:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305102
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305103
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->y:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->y:Ljava/lang/String;

    .line 305104
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->z:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->z:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->z:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->z:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->A:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->A:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->A:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 305098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->A:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305093
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->B:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305094
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->B:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->B:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 305095
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->B:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305090
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->C:Lcom/facebook/graphql/model/GraphQLVect2;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305091
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->C:Lcom/facebook/graphql/model/GraphQLVect2;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLVect2;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->C:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 305092
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->C:Lcom/facebook/graphql/model/GraphQLVect2;

    return-object v0
.end method

.method public final H()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305087
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305088
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305089
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->D:Z

    return v0
.end method

.method public final I()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 305084
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305085
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305086
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->E:I

    return v0
.end method

.method public final J()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305277
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305278
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305279
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->F:I

    return v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305274
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305275
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->G:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->G:Ljava/lang/String;

    .line 305276
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->H:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->H:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->H:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->J:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->J:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->J:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305858
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->K:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305859
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->K:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305860
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->K:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305861
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->L:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305862
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->L:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305863
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->L:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305864
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->M:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305865
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->M:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305866
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->M:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305867
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->N:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305868
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->N:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305869
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->N:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305870
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->O:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305871
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->O:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305872
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->O:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final S()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305873
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->P:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305874
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->P:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305875
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->P:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305876
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->S:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305877
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->S:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305878
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->S:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305879
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->V:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305880
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->V:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305881
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->V:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305882
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->W:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305883
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->W:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305884
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->W:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305846
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->X:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305847
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->X:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305848
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->X:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305849
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305850
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305851
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final Y()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305843
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305844
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305845
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305840
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aa:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305841
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aa:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aa:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 305842
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aa:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 82

    .prologue
    .line 305650
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 305651
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 305652
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 305653
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 305654
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 305655
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 305656
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 305657
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 305658
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->B()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 305659
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->C()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 305660
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->D()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 305661
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->E()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 305662
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 305663
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 305664
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 305665
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 305666
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 305667
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 305668
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 305669
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 305670
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 305671
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 305672
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 305673
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->S()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 305674
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 305675
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 305676
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 305677
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 305678
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 305679
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 305680
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 305681
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 305682
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 305683
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 305684
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Z()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 305685
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 305686
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 305687
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 305688
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 305689
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 305690
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aj()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 305691
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 305692
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 305693
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 305694
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->an()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 305695
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ao()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 305696
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ap()Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v47

    .line 305697
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 305698
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ar()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 305699
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->as()Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 305700
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 305701
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->au()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 305702
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->av()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 305703
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aw()LX/0Px;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v54

    .line 305704
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ax()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v55

    .line 305705
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->az()Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v56

    .line 305706
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 305707
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aB()Ljava/lang/String;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v58

    .line 305708
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aC()Ljava/lang/String;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v59

    .line 305709
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aD()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 305710
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 305711
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 305712
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 305713
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aE()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 305714
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 305715
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 305716
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 305717
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bh()Ljava/lang/String;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v68

    .line 305718
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 305719
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aM()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v70

    .line 305720
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bi()LX/0Px;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v71

    .line 305721
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bj()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v72

    .line 305722
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aN()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 305723
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bk()Ljava/lang/String;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v74

    .line 305724
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aO()Ljava/lang/String;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v75

    .line 305725
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aP()Ljava/lang/String;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v76

    .line 305726
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bl()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v77

    .line 305727
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v78

    .line 305728
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 305729
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aT()Ljava/lang/String;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v80

    .line 305730
    const/16 v81, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 305731
    const/16 v81, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 305732
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 305733
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 305734
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 305735
    const/4 v2, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->m()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 305736
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 305737
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 305738
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->p()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 305739
    const/16 v2, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->q()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305740
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->r()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305741
    const/16 v2, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305742
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305743
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->u()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305744
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305745
    const/16 v2, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305746
    const/16 v2, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305747
    const/16 v2, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305748
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 305749
    const/16 v3, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->A()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 305750
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 305751
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 305752
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 305753
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 305754
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 305755
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 305756
    const/16 v2, 0x1a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->H()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305757
    const/16 v2, 0x1b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->I()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 305758
    const/16 v2, 0x1c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->J()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 305759
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 305760
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305761
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305762
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305763
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305764
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305765
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305766
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305767
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305768
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305769
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305770
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305771
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305772
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305773
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305774
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305775
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305776
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305777
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305778
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305779
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305780
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305781
    const/16 v2, 0x33

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aa()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305782
    const/16 v2, 0x34

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ab()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305783
    const/16 v2, 0x35

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ac()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305784
    const/16 v2, 0x36

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ad()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305785
    const/16 v2, 0x37

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ae()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305786
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305787
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305788
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305789
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305790
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305791
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305792
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305793
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305794
    const/16 v3, 0x40

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bd()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 305795
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305796
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305797
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305798
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305799
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305800
    const/16 v2, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305801
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305802
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305803
    const/16 v2, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305804
    const/16 v2, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305805
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305806
    const/16 v2, 0x4d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ay()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 305807
    const/16 v2, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305808
    const/16 v2, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305809
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305810
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305811
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305812
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305813
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305814
    const/16 v2, 0x55

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305815
    const/16 v2, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305816
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305817
    const/16 v2, 0x58

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aG()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305818
    const/16 v2, 0x59

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aH()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305819
    const/16 v2, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305820
    const/16 v2, 0x5b

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305821
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305822
    const/16 v2, 0x5d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aK()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 305823
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305824
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305825
    const/16 v2, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305826
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305827
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305828
    const/16 v2, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305829
    const/16 v2, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305830
    const/16 v2, 0x65

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305831
    const/16 v2, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305832
    const/16 v2, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305833
    const/16 v2, 0x68

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aR()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305834
    const/16 v2, 0x69

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305835
    const/16 v2, 0x6a

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 305836
    const/16 v2, 0x6b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aU()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305837
    const/16 v2, 0x6c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aV()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 305838
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 305839
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 305332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 305333
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 305334
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 305335
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 305336
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305337
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 305338
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 305339
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305340
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 305341
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305342
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305343
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 305344
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 305346
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305347
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305348
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 305349
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 305350
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 305351
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305352
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 305353
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 305354
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 305355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 305356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305357
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 305358
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 305359
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 305361
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305362
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305363
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->B()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 305364
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->B()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 305365
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->B()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 305366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305367
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->x:Lcom/facebook/graphql/model/GraphQLStory;

    .line 305368
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->D()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 305369
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->D()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305370
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->D()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 305371
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305372
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->z:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305373
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->E()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 305374
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->E()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 305375
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->E()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 305376
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305377
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->A:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 305378
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 305379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 305380
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 305381
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305382
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->B:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 305383
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 305384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    .line 305385
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 305386
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305387
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->C:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 305388
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 305389
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305390
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 305391
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305392
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305393
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 305394
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305395
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 305396
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305397
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305398
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 305399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aQ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 305401
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305402
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305403
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 305404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 305406
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305407
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305408
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 305409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 305411
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305412
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305413
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 305414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305415
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 305416
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305417
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305418
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 305419
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305420
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 305421
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305422
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305423
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 305424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305425
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 305426
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305427
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305428
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 305429
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305430
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 305431
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305432
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305433
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->S()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 305434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->S()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->S()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 305436
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305437
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305438
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 305439
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305440
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 305441
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305442
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305443
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 305444
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305445
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 305446
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305447
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305448
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 305449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 305451
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305452
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305453
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 305454
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305455
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ba()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 305456
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305457
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305458
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 305459
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305460
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bb()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 305461
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305462
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305463
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 305464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305465
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 305466
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305467
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305468
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 305469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 305471
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305472
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305473
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 305474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 305476
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305477
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305478
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 305479
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 305481
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305482
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305483
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 305484
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 305486
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305487
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305488
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Z()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 305489
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Z()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 305490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->Z()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 305491
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305492
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aa:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 305493
    :cond_1f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 305494
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305495
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 305496
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305497
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305498
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 305499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 305501
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305502
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305503
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 305504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 305506
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305507
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305508
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 305509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 305511
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305512
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aj:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305513
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 305514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 305516
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305517
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305518
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aN()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 305519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aN()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 305520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aN()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 305521
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305522
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aW:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 305523
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aj()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 305524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aj()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 305525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aj()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 305526
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305527
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->al:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 305528
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 305529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 305531
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305532
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305533
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 305534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 305536
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305537
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305538
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 305539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 305541
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305542
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 305543
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ao()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 305544
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ao()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305545
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ao()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 305546
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305547
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305548
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 305549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 305551
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305552
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305553
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ar()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 305554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ar()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->ar()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 305556
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305557
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->au:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305558
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->as()Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 305559
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->as()Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 305560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->as()Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 305561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305562
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->av:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 305563
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 305564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 305565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 305566
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305567
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aw:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305568
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->au()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 305569
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->au()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 305570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->au()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 305571
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305572
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ax:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 305573
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->av()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 305574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->av()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->av()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 305576
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305577
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ay:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305578
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aw()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 305579
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aw()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 305580
    if-eqz v2, :cond_31

    .line 305581
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305582
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPhoto;->az:Ljava/util/List;

    move-object v1, v0

    .line 305583
    :cond_31
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bj()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 305584
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bj()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 305585
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bj()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 305586
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305587
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aV:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305588
    :cond_32
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 305589
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 305591
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305592
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305593
    :cond_33
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aD()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 305594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aD()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 305595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aD()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_34

    .line 305596
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305597
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aG:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 305598
    :cond_34
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 305599
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305600
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_35

    .line 305601
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305602
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305603
    :cond_35
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 305604
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305605
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 305606
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305607
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305608
    :cond_36
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 305609
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305610
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 305611
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305612
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305613
    :cond_37
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aE()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 305614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aE()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 305615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aE()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v2

    if-eq v2, v0, :cond_38

    .line 305616
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305617
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aK:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 305618
    :cond_38
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 305619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_39

    .line 305621
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305622
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305623
    :cond_39
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bl()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 305624
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bl()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 305625
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->bl()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v2

    if-eq v2, v0, :cond_3a

    .line 305626
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305627
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->ba:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 305628
    :cond_3a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 305629
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3b

    .line 305631
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305632
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305633
    :cond_3b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 305634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 305635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aJ()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3c

    .line 305636
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305637
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aP:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 305638
    :cond_3c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 305639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 305640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3d

    .line 305641
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305642
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305643
    :cond_3d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aM()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_3e

    .line 305644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aM()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 305645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aM()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3e

    .line 305646
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 305647
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhoto;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 305648
    :cond_3e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 305649
    if-nez v1, :cond_3f

    :goto_0
    return-object p0

    :cond_3f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 305301
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 305302
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->i:I

    .line 305303
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->l:I

    .line 305304
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->m:Z

    .line 305305
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->n:Z

    .line 305306
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->o:Z

    .line 305307
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->p:Z

    .line 305308
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->q:Z

    .line 305309
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->r:Z

    .line 305310
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->s:Z

    .line 305311
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->t:Z

    .line 305312
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->u:Z

    .line 305313
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->w:J

    .line 305314
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->D:Z

    .line 305315
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->E:I

    .line 305316
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->F:I

    .line 305317
    const/16 v0, 0x33

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ac:Z

    .line 305318
    const/16 v0, 0x34

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ad:Z

    .line 305319
    const/16 v0, 0x35

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ae:Z

    .line 305320
    const/16 v0, 0x36

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->af:Z

    .line 305321
    const/16 v0, 0x37

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ag:Z

    .line 305322
    const/16 v0, 0x40

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ap:J

    .line 305323
    const/16 v0, 0x4d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aB:I

    .line 305324
    const/16 v0, 0x58

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aM:Z

    .line 305325
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aN:Z

    .line 305326
    const/16 v0, 0x5d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aR:I

    .line 305327
    const/16 v0, 0x68

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bc:Z

    .line 305328
    const/16 v0, 0x6b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bf:Z

    .line 305329
    const/16 v0, 0x6c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bg:Z

    .line 305330
    return-void
.end method

.method public final aA()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305298
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305299
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305300
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aB()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305295
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aE:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305296
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aE:Ljava/lang/String;

    const/16 v1, 0x50

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aE:Ljava/lang/String;

    .line 305297
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aE:Ljava/lang/String;

    return-object v0
.end method

.method public final aC()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305292
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aF:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305293
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aF:Ljava/lang/String;

    const/16 v1, 0x51

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aF:Ljava/lang/String;

    .line 305294
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aF:Ljava/lang/String;

    return-object v0
.end method

.method public final aD()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305289
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aG:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305290
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aG:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aG:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 305291
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aG:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final aE()Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305286
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aK:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305287
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aK:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    const/16 v1, 0x56

    const-class v2, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aK:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 305288
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aK:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    return-object v0
.end method

.method public final aF()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305283
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305284
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x57

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305285
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aG()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305280
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305281
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305282
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aM:Z

    return v0
.end method

.method public final aH()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305126
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305127
    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305128
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aN:Z

    return v0
.end method

.method public final aI()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305271
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305272
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305273
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aJ()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304995
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aP:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304996
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aP:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    const/16 v1, 0x5b

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aP:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 304997
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aP:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    return-object v0
.end method

.method public final aK()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304992
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304993
    const/16 v0, 0xb

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304994
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aR:I

    return v0
.end method

.method public final aL()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304989
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304990
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304991
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aM()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304986
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304987
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 304988
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    return-object v0
.end method

.method public final aN()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304983
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aW:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304984
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aW:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/16 v1, 0x62

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aW:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 304985
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aW:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final aO()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304980
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aY:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304981
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aY:Ljava/lang/String;

    const/16 v1, 0x64

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aY:Ljava/lang/String;

    .line 304982
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aY:Ljava/lang/String;

    return-object v0
.end method

.method public final aP()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304977
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304978
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ:Ljava/lang/String;

    const/16 v1, 0x65

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ:Ljava/lang/String;

    .line 304979
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method public final aQ()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304974
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x67

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304976
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aR()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304971
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304972
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304973
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bc:Z

    return v0
.end method

.method public final aS()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x69

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aT()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304965
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->be:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304966
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->be:Ljava/lang/String;

    const/16 v1, 0x6a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->be:Ljava/lang/String;

    .line 304967
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->be:Ljava/lang/String;

    return-object v0
.end method

.method public final aU()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304962
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304963
    const/16 v0, 0xd

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304964
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bf:Z

    return v0
.end method

.method public final aV()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304959
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304960
    const/16 v0, 0xd

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304961
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->bg:Z

    return v0
.end method

.method public final aa()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304956
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304957
    const/4 v0, 0x6

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304958
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ac:Z

    return v0
.end method

.method public final ab()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304953
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304954
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304955
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ad:Z

    return v0
.end method

.method public final ac()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304911
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304912
    const/4 v0, 0x6

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304913
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ae:Z

    return v0
.end method

.method public final ad()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x6

    .line 304917
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304918
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304919
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->af:Z

    return v0
.end method

.method public final ae()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304920
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304921
    const/4 v0, 0x6

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304922
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ag:Z

    return v0
.end method

.method public final af()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304923
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304924
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304925
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ag()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304926
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304927
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x39

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304928
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ah()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304929
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aj:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304930
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aj:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aj:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304931
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aj:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304932
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304933
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304934
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304935
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->al:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304936
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->al:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->al:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 304937
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->al:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    return-object v0
.end method

.method public final ak()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304938
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->am:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304939
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->am:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304940
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->am:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final al()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304941
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->an:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304942
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->an:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304943
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->an:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final am()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304944
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304945
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304946
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ao:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final an()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304947
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aq:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304948
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aq:Ljava/lang/String;

    const/16 v1, 0x41

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aq:Ljava/lang/String;

    .line 304949
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aq:Ljava/lang/String;

    return-object v0
.end method

.method public final ao()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304950
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304951
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304952
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ap()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304914
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->as:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304915
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->as:Ljava/lang/String;

    const/16 v1, 0x43

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->as:Ljava/lang/String;

    .line 304916
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->as:Ljava/lang/String;

    return-object v0
.end method

.method public final aq()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305041
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->at:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305042
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->at:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x44

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305043
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->at:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ar()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305078
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->au:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305079
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->au:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->au:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305080
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->au:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final as()Lcom/facebook/graphql/model/GraphQLOpenGraphAction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305075
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->av:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305076
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->av:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->av:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    .line 305077
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->av:Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    return-object v0
.end method

.method public final at()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305072
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aw:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305073
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aw:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x47

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aw:Lcom/facebook/graphql/model/GraphQLActor;

    .line 305074
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aw:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final au()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305069
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ax:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305070
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ax:Lcom/facebook/graphql/model/GraphQLVideo;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ax:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 305071
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ax:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method public final av()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305066
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ay:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305067
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ay:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x49

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ay:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 305068
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->ay:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final aw()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoEncoding;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305063
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->az:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305064
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->az:Ljava/util/List;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->az:Ljava/util/List;

    .line 305065
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->az:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final ax()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305060
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aA:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305061
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aA:Ljava/lang/String;

    const/16 v1, 0x4b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aA:Ljava/lang/String;

    .line 305062
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aA:Ljava/lang/String;

    return-object v0
.end method

.method public final ay()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305057
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305058
    const/16 v0, 0x9

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305059
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aB:I

    return v0
.end method

.method public final az()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305054
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aC:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305055
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aC:Ljava/lang/String;

    const/16 v1, 0x4e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aC:Ljava/lang/String;

    .line 305056
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->aC:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 305053
    const v0, 0x4984e12

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305050
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305051
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->e:Ljava/lang/String;

    .line 305052
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305047
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305048
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 305049
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305044
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305045
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 305046
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304998
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304999
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305000
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->i:I

    return v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305038
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305039
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 305040
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->k:Ljava/lang/String;

    .line 305037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305032
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305033
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305034
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->l:I

    return v0
.end method

.method public final q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 305029
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305030
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305031
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->m:Z

    return v0
.end method

.method public final r()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305026
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305027
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305028
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->n:Z

    return v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305023
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305024
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305025
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->o:Z

    return v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305020
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305021
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305022
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->p:Z

    return v0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305017
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305018
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305019
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->q:Z

    return v0
.end method

.method public final v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305014
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305015
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305016
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->r:Z

    return v0
.end method

.method public final w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305011
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305012
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305013
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->s:Z

    return v0
.end method

.method public final x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305008
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305009
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305010
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->t:Z

    return v0
.end method

.method public final y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 305005
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 305006
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 305007
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->u:Z

    return v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 305001
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->v:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 305002
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->v:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 305003
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhoto;->v:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method
