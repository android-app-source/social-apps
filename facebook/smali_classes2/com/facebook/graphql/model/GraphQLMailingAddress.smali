.class public final Lcom/facebook/graphql/model/GraphQLMailingAddress;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMailingAddress$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMailingAddress$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Z

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326593
    const-class v0, Lcom/facebook/graphql/model/GraphQLMailingAddress$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326592
    const-class v0, Lcom/facebook/graphql/model/GraphQLMailingAddress$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 326590
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 326591
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326587
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->e:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326588
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->e:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->e:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 326589
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->e:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326581
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->f:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326582
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->f:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326583
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->f:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326578
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326579
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->g:Ljava/lang/String;

    .line 326580
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326575
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326576
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->h:Ljava/lang/String;

    .line 326577
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326572
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326573
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326574
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->i:Z

    return v0
.end method

.method private o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326584
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326585
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326586
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->j:Z

    return v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326569
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326570
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k:Ljava/lang/String;

    .line 326571
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326566
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326567
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->l:Ljava/lang/String;

    .line 326568
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326563
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326564
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->m:Ljava/lang/String;

    .line 326565
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 326543
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326544
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 326545
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 326546
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 326547
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 326548
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 326549
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 326550
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 326551
    const/16 v7, 0xa

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 326552
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 326553
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 326554
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 326555
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 326556
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->n()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 326557
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 326558
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 326559
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 326560
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 326561
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326562
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 326524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326525
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326526
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 326527
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 326528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMailingAddress;

    .line 326529
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMailingAddress;->e:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 326530
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 326531
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 326532
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 326533
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMailingAddress;

    .line 326534
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMailingAddress;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326535
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326536
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326542
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMailingAddress;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 326538
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 326539
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->i:Z

    .line 326540
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMailingAddress;->j:Z

    .line 326541
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 326537
    const v0, 0x19a3b2c9

    return v0
.end method
