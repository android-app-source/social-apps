.class public final Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public f:I

.field public g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301213
    const-class v0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301212
    const-class v0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 301210
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301211
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 301202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301203
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 301204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 301205
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 301206
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 301207
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301208
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 301209
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 301199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301201
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301196
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->e:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301197
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->e:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->e:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 301198
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->e:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 301192
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 301193
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->f:I

    .line 301194
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->g:I

    .line 301195
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 301185
    const v0, 0x10db5b32

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301189
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301190
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301191
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->f:I

    return v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301186
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301187
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301188
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->g:I

    return v0
.end method
