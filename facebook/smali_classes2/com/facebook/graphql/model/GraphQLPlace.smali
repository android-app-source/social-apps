.class public final Lcom/facebook/graphql/model/GraphQLPlace;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlace$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlace$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Z

.field public L:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLRedirectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Q:Z

.field public R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public W:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

.field public l:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimeRange;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 261106
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlace$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 261107
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlace$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 261108
    const/16 v0, 0x2f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 261109
    return-void
.end method

.method public constructor <init>(LX/4Y6;)V
    .locals 1

    .prologue
    .line 261110
    const/16 v0, 0x2f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 261111
    iget-object v0, p1, LX/4Y6;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 261112
    iget-boolean v0, p1, LX/4Y6;->c:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->g:Z

    .line 261113
    iget-boolean v0, p1, LX/4Y6;->d:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->h:Z

    .line 261114
    iget-object v0, p1, LX/4Y6;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261115
    iget-object v0, p1, LX/4Y6;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->j:Ljava/util/List;

    .line 261116
    iget-object v0, p1, LX/4Y6;->g:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->k:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 261117
    iget-object v0, p1, LX/4Y6;->h:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->l:Lcom/facebook/graphql/model/GraphQLPage;

    .line 261118
    iget-object v0, p1, LX/4Y6;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->m:Ljava/lang/String;

    .line 261119
    iget-boolean v0, p1, LX/4Y6;->j:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->n:Z

    .line 261120
    iget-boolean v0, p1, LX/4Y6;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->o:Z

    .line 261121
    iget-object v0, p1, LX/4Y6;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->p:Ljava/lang/String;

    .line 261122
    iget-object v0, p1, LX/4Y6;->m:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->q:Ljava/util/List;

    .line 261123
    iget-object v0, p1, LX/4Y6;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->r:Ljava/lang/String;

    .line 261124
    iget-boolean v0, p1, LX/4Y6;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->s:Z

    .line 261125
    iget-object v0, p1, LX/4Y6;->p:Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->t:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 261126
    iget-object v0, p1, LX/4Y6;->q:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->u:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 261127
    iget-object v0, p1, LX/4Y6;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->v:Ljava/lang/String;

    .line 261128
    iget-object v0, p1, LX/4Y6;->s:Lcom/facebook/graphql/model/GraphQLRating;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->w:Lcom/facebook/graphql/model/GraphQLRating;

    .line 261129
    iget-object v0, p1, LX/4Y6;->t:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->x:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 261130
    iget-object v0, p1, LX/4Y6;->u:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->y:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 261131
    iget-object v0, p1, LX/4Y6;->v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->z:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 261132
    iget-object v0, p1, LX/4Y6;->w:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261133
    iget-object v0, p1, LX/4Y6;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261134
    iget-object v0, p1, LX/4Y6;->y:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->C:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 261135
    iget-object v0, p1, LX/4Y6;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->D:Ljava/lang/String;

    .line 261136
    iget-object v0, p1, LX/4Y6;->A:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 261137
    iget-object v0, p1, LX/4Y6;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->F:Ljava/lang/String;

    .line 261138
    iget-object v0, p1, LX/4Y6;->C:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261139
    iget-object v0, p1, LX/4Y6;->D:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261140
    iget-object v0, p1, LX/4Y6;->E:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 261141
    iget-object v0, p1, LX/4Y6;->F:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261142
    iget-boolean v0, p1, LX/4Y6;->G:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->K:Z

    .line 261143
    iget-object v0, p1, LX/4Y6;->H:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->L:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    .line 261144
    iget-object v0, p1, LX/4Y6;->I:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->M:Ljava/util/List;

    .line 261145
    iget-object v0, p1, LX/4Y6;->J:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->N:Ljava/util/List;

    .line 261146
    iget-object v0, p1, LX/4Y6;->K:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->O:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 261147
    iget-object v0, p1, LX/4Y6;->L:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->P:Ljava/util/List;

    .line 261148
    iget-boolean v0, p1, LX/4Y6;->M:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->Q:Z

    .line 261149
    iget-object v0, p1, LX/4Y6;->N:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->R:Ljava/util/List;

    .line 261150
    iget-object v0, p1, LX/4Y6;->O:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->S:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 261151
    iget-object v0, p1, LX/4Y6;->P:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->T:Ljava/lang/String;

    .line 261152
    iget-object v0, p1, LX/4Y6;->Q:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->U:Ljava/util/List;

    .line 261153
    iget-object v0, p1, LX/4Y6;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 261154
    iget-object v0, p1, LX/4Y6;->S:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->W:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 261155
    iget-object v0, p1, LX/4Y6;->T:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->X:Ljava/util/List;

    .line 261156
    iget-object v0, p1, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 261157
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 3

    .prologue
    .line 261158
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPlace;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 261159
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261160
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261161
    if-eqz v0, :cond_0

    .line 261162
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 261163
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 261164
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPlace;->v:Ljava/lang/String;

    .line 261165
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261166
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261167
    if-eqz v0, :cond_0

    .line 261168
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 261169
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261170
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261171
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->v:Ljava/lang/String;

    .line 261172
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLRating;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261173
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->w:Lcom/facebook/graphql/model/GraphQLRating;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261174
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->w:Lcom/facebook/graphql/model/GraphQLRating;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLRating;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRating;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->w:Lcom/facebook/graphql/model/GraphQLRating;

    .line 261175
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->w:Lcom/facebook/graphql/model/GraphQLRating;

    return-object v0
.end method

.method public final C()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261176
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->x:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261177
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->x:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->x:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 261178
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->x:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261179
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->y:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261180
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->y:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->y:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 261181
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->y:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261182
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->z:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261183
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->z:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->z:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 261184
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->z:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261185
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->A:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261186
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->A:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261187
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->A:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261188
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261189
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261190
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261191
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->C:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261192
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->C:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->C:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 261193
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->C:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261194
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261195
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->D:Ljava/lang/String;

    .line 261196
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 261102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261197
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261198
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->F:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->F:Ljava/lang/String;

    .line 261199
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261236
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261237
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261238
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261233
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->H:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261234
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->H:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261235
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->H:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261230
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261231
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 261232
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261227
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->J:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261228
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->J:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261229
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->J:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final P()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261224
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261225
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261226
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->K:Z

    return v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261221
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->L:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261222
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->L:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->L:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    .line 261223
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->L:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    return-object v0
.end method

.method public final R()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLRedirectionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261218
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->M:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261219
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->M:Ljava/util/List;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->M:Ljava/util/List;

    .line 261220
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->M:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final S()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261215
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->N:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261216
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->N:Ljava/util/List;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->N:Ljava/util/List;

    .line 261217
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->N:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261212
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->O:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261213
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->O:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->O:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 261214
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->O:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    return-object v0
.end method

.method public final U()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261209
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->P:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261210
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->P:Ljava/util/List;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->P:Ljava/util/List;

    .line 261211
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->P:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final V()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261206
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261207
    const/4 v0, 0x4

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261208
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->Q:Z

    return v0
.end method

.method public final W()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261203
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->R:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261204
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->R:Ljava/util/List;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->R:Ljava/util/List;

    .line 261205
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->R:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261103
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->S:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261104
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->S:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->S:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 261105
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->S:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261200
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->T:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261201
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->T:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->T:Ljava/lang/String;

    .line 261202
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final Z()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260803
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->U:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260804
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->U:Ljava/util/List;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->U:Ljava/util/List;

    .line 260805
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->U:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 36

    .prologue
    .line 260968
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260969
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 260970
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 260971
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 260972
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->o()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 260973
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 260974
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->r()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 260975
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->u()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 260976
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->v()LX/0Px;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 260977
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 260978
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 260979
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 260980
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 260981
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 260982
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->C()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 260983
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 260984
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 260985
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 260986
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->I()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 260987
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->K()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 260988
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 260989
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 260990
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 260991
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 260992
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Q()Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 260993
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->R()LX/0Px;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v26

    .line 260994
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->S()LX/0Px;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v27

    .line 260995
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->T()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 260996
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->U()LX/0Px;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v29

    .line 260997
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->W()LX/0Px;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v30

    .line 260998
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Y()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 260999
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Z()LX/0Px;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v32

    .line 261000
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->ab()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 261001
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->ac()LX/0Px;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v34

    .line 261002
    const/16 v35, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 261003
    const/16 v35, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 261004
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 261005
    const/4 v2, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->l()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261006
    const/4 v2, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->m()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261007
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 261008
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 261009
    const/4 v3, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->p()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 261010
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 261011
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 261012
    const/16 v2, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261013
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261014
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 261015
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 261016
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 261017
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261018
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 261019
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 261020
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 261021
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 261022
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 261023
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261024
    const/16 v3, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->E()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 261025
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261026
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261027
    const/16 v3, 0x18

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 261028
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261029
    const/16 v3, 0x1a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->J()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 261030
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261031
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261032
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261033
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261034
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261035
    const/16 v2, 0x20

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->P()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261036
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261037
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261038
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261039
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261040
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261041
    const/16 v2, 0x26

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->V()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261042
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261043
    const/16 v3, 0x28

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->X()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 261044
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261045
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261046
    const/16 v3, 0x2b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->aa()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 261047
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261048
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261049
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 261050
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 261051
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 261052
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->p()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v2

    goto/16 :goto_1

    .line 261053
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->E()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v2

    goto/16 :goto_2

    .line 261054
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v2

    goto/16 :goto_3

    .line 261055
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->J()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    goto/16 :goto_4

    .line 261056
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->X()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v2

    goto :goto_5

    .line 261057
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPlace;->aa()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto :goto_6
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 260865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260866
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 260868
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 260869
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260870
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 260871
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 260872
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260873
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 260874
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260875
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260876
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 260877
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 260878
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 260879
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260880
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->l:Lcom/facebook/graphql/model/GraphQLPage;

    .line 260881
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->v()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 260882
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->v()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 260883
    if-eqz v2, :cond_3

    .line 260884
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260885
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPlace;->q:Ljava/util/List;

    move-object v1, v0

    .line 260886
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 260887
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 260888
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 260889
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260890
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->t:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 260891
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 260892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 260893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 260894
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260895
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->u:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 260896
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 260897
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRating;

    .line 260898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 260899
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260900
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->w:Lcom/facebook/graphql/model/GraphQLRating;

    .line 260901
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->C()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 260902
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->C()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 260903
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->C()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 260904
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260905
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->x:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 260906
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 260907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 260908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 260909
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260910
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->y:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 260911
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 260912
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260913
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 260914
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260915
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260916
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 260917
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260918
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 260919
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260920
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260921
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 260922
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260923
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 260924
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260925
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260926
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 260927
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260928
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 260929
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260930
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260931
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 260932
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 260933
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 260934
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260935
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->I:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 260936
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 260937
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260938
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 260939
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260940
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260941
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Q()Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 260942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Q()Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    .line 260943
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Q()Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 260944
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260945
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->L:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    .line 260946
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->R()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 260947
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->R()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 260948
    if-eqz v2, :cond_10

    .line 260949
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260950
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPlace;->M:Ljava/util/List;

    move-object v1, v0

    .line 260951
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->S()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 260952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->S()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 260953
    if-eqz v2, :cond_11

    .line 260954
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260955
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPlace;->N:Ljava/util/List;

    move-object v1, v0

    .line 260956
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->T()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 260957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->T()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 260958
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->T()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 260959
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260960
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->O:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 260961
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->ab()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 260962
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->ab()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 260963
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->ab()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 260964
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260965
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlace;->W:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 260966
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 260967
    if-nez v1, :cond_14

    :goto_0
    return-object p0

    :cond_14
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 260864
    new-instance v0, LX/4Y7;

    invoke-direct {v0, p1}, LX/4Y7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260863
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 260854
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 260855
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->g:Z

    .line 260856
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->h:Z

    .line 260857
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->n:Z

    .line 260858
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->o:Z

    .line 260859
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->s:Z

    .line 260860
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->K:Z

    .line 260861
    const/16 v0, 0x26

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->Q:Z

    .line 260862
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 260838
    const-string v0, "address.full_address"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    .line 260840
    if-eqz v0, :cond_2

    .line 260841
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 260842
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 260843
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 260844
    :goto_0
    return-void

    .line 260845
    :cond_0
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 260847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 260848
    const/16 v0, 0x11

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 260849
    :cond_1
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->aa()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 260851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 260852
    const/16 v0, 0x2b

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 260853
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 260825
    const-string v0, "address.full_address"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    .line 260827
    if-eqz v0, :cond_0

    .line 260828
    if-eqz p3, :cond_1

    .line 260829
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 260830
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->a(Ljava/lang/String;)V

    .line 260831
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 260832
    :cond_0
    :goto_0
    return-void

    .line 260833
    :cond_1
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 260834
    :cond_2
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260835
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPlace;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 260836
    :cond_3
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260837
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPlace;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto :goto_0
.end method

.method public final aa()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260822
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260823
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 260824
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final ab()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260819
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->W:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260820
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->W:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->W:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 260821
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->W:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    return-object v0
.end method

.method public final ac()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260816
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->X:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260817
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->X:Ljava/util/List;

    const/16 v1, 0x2d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->X:Ljava/util/List;

    .line 260818
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->X:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 260815
    const v0, 0x499e8e7

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260809
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 260810
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 260811
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLPlace;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 260812
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 260813
    const/4 v0, 0x0

    .line 260814
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260806
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260807
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 260808
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260800
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260801
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260802
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->g:Z

    return v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261058
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261059
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261060
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->h:Z

    return v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261061
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261062
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 261063
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261064
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261065
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->j:Ljava/util/List;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->j:Ljava/util/List;

    .line 261066
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261067
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->k:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261068
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->k:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->k:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 261069
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->k:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261070
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->l:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261071
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->l:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->l:Lcom/facebook/graphql/model/GraphQLPage;

    .line 261072
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->l:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261073
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261074
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->m:Ljava/lang/String;

    .line 261075
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 261076
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261077
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261078
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->n:Z

    return v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261079
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261080
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261081
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->o:Z

    return v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261082
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261083
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->p:Ljava/lang/String;

    .line 261084
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final v()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimeRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261085
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261086
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimeRange;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->q:Ljava/util/List;

    .line 261087
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261088
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261089
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->r:Ljava/lang/String;

    .line 261090
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261091
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261092
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261093
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->s:Z

    return v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261094
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->t:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261095
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->t:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->t:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 261096
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->t:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261097
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->u:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261098
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->u:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->u:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 261099
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlace;->u:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    return-object v0
.end method
