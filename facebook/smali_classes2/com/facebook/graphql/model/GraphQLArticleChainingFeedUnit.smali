.class public final Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16m;
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/17w;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293441
    const-class v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293442
    const-class v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 293443
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 293444
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x56fb7e27

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 293445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->q:LX/0x2;

    .line 293446
    return-void
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293447
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293448
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->j:Ljava/lang/String;

    .line 293449
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293408
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293409
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->k:Ljava/lang/String;

    .line 293410
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293450
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293451
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293452
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293453
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293454
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293455
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293460
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293461
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->p:Ljava/lang/String;

    .line 293462
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 293456
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293457
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293458
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->h:Ljava/lang/String;

    .line 293459
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 293463
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 293464
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 293465
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->i:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 293466
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 293467
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->q:LX/0x2;

    if-nez v0, :cond_0

    .line 293468
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->q:LX/0x2;

    .line 293469
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->q:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 293439
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 293440
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 293470
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 293472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 293473
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->E_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 293474
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 293475
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 293476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 293477
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 293478
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 293479
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 293480
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 293481
    const/16 v3, 0xc

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 293482
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 293483
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 293484
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 293485
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 293486
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 293487
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 293488
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 293489
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 293490
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 293491
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 293492
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 293493
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293494
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 293385
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293386
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293387
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293388
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 293389
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    .line 293390
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293391
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 293392
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    .line 293393
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 293394
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    .line 293395
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    .line 293396
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 293397
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293398
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 293399
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    .line 293400
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293401
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 293402
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293403
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 293404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    .line 293405
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293406
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293407
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293411
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 293412
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->i:J

    .line 293413
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 293414
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 293415
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->i:J

    .line 293416
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293417
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293418
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293419
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o:Ljava/lang/String;

    .line 293420
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293421
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 293423
    :goto_0
    return-object v0

    .line 293424
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 293425
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 293426
    const v0, -0x56fb7e27

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293427
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293428
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->g:Ljava/lang/String;

    .line 293429
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 293430
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293431
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293432
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293433
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293434
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293435
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    .line 293436
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 293437
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Ljava/util/List;
    .locals 1

    .prologue
    .line 293438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method
