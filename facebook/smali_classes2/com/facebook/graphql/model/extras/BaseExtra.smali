.class public abstract Lcom/facebook/graphql/model/extras/BaseExtra;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/flatbuffers/Flattenable;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 338732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338733
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/extras/BaseExtra;->a:Z

    .line 338734
    return-void
.end method

.method public constructor <init>(B)V
    .locals 1

    .prologue
    .line 338729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338730
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/extras/BaseExtra;->a:Z

    .line 338731
    return-void
.end method


# virtual methods
.method public a(LX/186;)I
    .locals 1

    .prologue
    .line 338728
    const/4 v0, 0x0

    return v0
.end method

.method public a(LX/15i;I)V
    .locals 0

    .prologue
    .line 338735
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 338725
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 338727
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 338726
    return-void
.end method
