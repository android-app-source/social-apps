.class public final Lcom/facebook/graphql/model/GraphQLPageCallToAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageCallToAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageCallToAction$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLPhoneNumber;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325484
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageCallToAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325485
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageCallToAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 325486
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325487
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325488
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325489
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u:Ljava/lang/String;

    .line 325490
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u:Ljava/lang/String;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/enums/GraphQLCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325491
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->e:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325492
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->e:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->e:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 325493
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->e:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325494
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325495
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 325496
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325535
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325536
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->l:Ljava/lang/String;

    .line 325537
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 325497
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325498
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 325499
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->k()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 325500
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->y()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 325501
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m()Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 325502
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->z()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 325503
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 325504
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->p()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 325505
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 325506
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 325507
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 325508
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u()Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 325509
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 325510
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->A()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 325511
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->w()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 325512
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 325513
    const/16 v17, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->x()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    sget-object v18, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object/from16 v0, v18

    if-ne v2, v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 325514
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 325515
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 325516
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 325517
    const/4 v2, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->l()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 325518
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 325519
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 325520
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 325521
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 325522
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 325523
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 325524
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 325525
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 325526
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 325527
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 325528
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 325529
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 325530
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325531
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325532
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 325533
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->x()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    goto/16 :goto_0

    .line 325534
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 325543
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325544
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->y()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 325545
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->y()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 325546
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->y()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 325547
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 325548
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 325549
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m()Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 325550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m()Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    .line 325551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m()Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 325552
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 325553
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j:Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    .line 325554
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 325555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    .line 325556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 325557
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 325558
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    .line 325559
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->w()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 325560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->w()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 325561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->w()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 325562
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 325563
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 325564
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u()Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 325565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u()Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    .line 325566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->u()Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 325567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 325568
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s:Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    .line 325569
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325570
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 325539
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 325540
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->i:Z

    .line 325541
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q:Z

    .line 325542
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 325538
    const v0, 0x7f1ad5e

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325481
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325482
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->f:Ljava/lang/String;

    .line 325483
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325442
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325443
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->g:Ljava/lang/String;

    .line 325444
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325478
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325479
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325480
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->i:Z

    return v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325445
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j:Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325446
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j:Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j:Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    .line 325447
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->j:Lcom/facebook/graphql/model/GraphQLPageCallToActionAdminInfo;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325448
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->k:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325449
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->k:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->k:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 325450
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->k:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325451
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325452
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m:Ljava/lang/String;

    .line 325453
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325454
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325455
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n:Ljava/lang/String;

    .line 325456
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325457
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325458
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    .line 325459
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->o:Lcom/facebook/graphql/model/GraphQLPageCallToActionConfigFieldsConnection;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325460
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325461
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->p:Ljava/lang/String;

    .line 325462
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325463
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325464
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325465
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->q:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325466
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325467
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r:Ljava/lang/String;

    .line 325468
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLPhoneNumber;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325469
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s:Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325470
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s:Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s:Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    .line 325471
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->s:Lcom/facebook/graphql/model/GraphQLPhoneNumber;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325472
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325473
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t:Ljava/lang/String;

    .line 325474
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325475
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325476
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 325477
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->v:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    return-object v0
.end method
