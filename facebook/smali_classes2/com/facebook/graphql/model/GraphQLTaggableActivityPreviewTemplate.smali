.class public final Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325674
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325675
    const-class v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 325676
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325677
    return-void
.end method

.method public constructor <init>(LX/4Z6;)V
    .locals 1

    .prologue
    .line 325678
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325679
    iget-object v0, p1, LX/4Z6;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->e:Ljava/lang/String;

    .line 325680
    iget-object v0, p1, LX/4Z6;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->f:Ljava/util/List;

    .line 325681
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 325682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325683
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 325684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 325685
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 325686
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 325687
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 325688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325689
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 325690
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325691
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 325692
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 325693
    if-eqz v1, :cond_0

    .line 325694
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 325695
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->f:Ljava/util/List;

    .line 325696
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325697
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325698
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325699
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->e:Ljava/lang/String;

    .line 325700
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 325701
    const v0, -0x342e4db4

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325702
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325703
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->f:Ljava/util/List;

    .line 325704
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
