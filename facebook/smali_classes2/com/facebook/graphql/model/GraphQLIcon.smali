.class public final Lcom/facebook/graphql/model/GraphQLIcon;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLIcon$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLIcon$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262167
    const-class v0, Lcom/facebook/graphql/model/GraphQLIcon$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262166
    const-class v0, Lcom/facebook/graphql/model/GraphQLIcon$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262164
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262165
    return-void
.end method

.method public constructor <init>(LX/4Wu;)V
    .locals 1

    .prologue
    .line 262158
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262159
    iget v0, p1, LX/4Wu;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->e:I

    .line 262160
    iget-object v0, p1, LX/4Wu;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->f:Ljava/lang/String;

    .line 262161
    iget-object v0, p1, LX/4Wu;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->g:Ljava/lang/String;

    .line 262162
    iget v0, p1, LX/4Wu;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->h:I

    .line 262163
    return-void
.end method

.method private k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 262155
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262156
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262157
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->e:I

    return v0
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262152
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262153
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262154
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->h:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 262142
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLIcon;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 262144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLIcon;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 262145
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 262146
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLIcon;->k()I

    move-result v2

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 262147
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 262148
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 262149
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLIcon;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 262150
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262151
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 262128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262130
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262139
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262140
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->f:Ljava/lang/String;

    .line 262141
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 262135
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 262136
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->e:I

    .line 262137
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->h:I

    .line 262138
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 262134
    const v0, 0x22b099

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262131
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262132
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->g:Ljava/lang/String;

    .line 262133
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLIcon;->g:Ljava/lang/String;

    return-object v0
.end method
