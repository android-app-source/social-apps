.class public final Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323200
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323201
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 323205
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323206
    return-void
.end method

.method private a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 323202
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323203
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323204
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->e:I

    return v0
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->f:Ljava/util/List;

    .line 323170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323197
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323198
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323199
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 323188
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323189
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 323190
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 323191
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 323192
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->a()I

    move-result v2

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 323193
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 323194
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 323195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323196
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 323175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323176
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 323177
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323178
    if-eqz v1, :cond_2

    .line 323179
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 323180
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->f:Ljava/util/List;

    move-object v1, v0

    .line 323181
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323182
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323183
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 323184
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 323185
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323186
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323187
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 323172
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 323173
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;->e:I

    .line 323174
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 323171
    const v0, -0x124407cd

    return v0
.end method
