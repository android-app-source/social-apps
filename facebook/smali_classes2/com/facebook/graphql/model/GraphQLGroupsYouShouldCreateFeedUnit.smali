.class public final Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251471
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251472
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 251473
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 251474
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x42554c1c

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 251475
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->p:LX/0x2;

    .line 251476
    return-void
.end method

.method public constructor <init>(LX/4Wq;)V
    .locals 2

    .prologue
    .line 251477
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 251478
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x42554c1c

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 251479
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->p:LX/0x2;

    .line 251480
    iget-object v0, p1, LX/4Wq;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->f:Ljava/lang/String;

    .line 251481
    iget-object v0, p1, LX/4Wq;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g:Ljava/lang/String;

    .line 251482
    iget-wide v0, p1, LX/4Wq;->d:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->h:J

    .line 251483
    iget-object v0, p1, LX/4Wq;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->i:Ljava/util/List;

    .line 251484
    iget-object v0, p1, LX/4Wq;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251485
    iget-object v0, p1, LX/4Wq;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k:Ljava/lang/String;

    .line 251486
    iget-object v0, p1, LX/4Wq;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->l:Ljava/util/List;

    .line 251487
    iget-object v0, p1, LX/4Wq;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->m:Ljava/lang/String;

    .line 251488
    iget-object v0, p1, LX/4Wq;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251489
    iget-object v0, p1, LX/4Wq;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o:Ljava/lang/String;

    .line 251490
    iget-object v0, p1, LX/4Wq;->l:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->p:LX/0x2;

    .line 251491
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 251492
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251493
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251494
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g:Ljava/lang/String;

    .line 251495
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251496
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 251497
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 251498
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 251499
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 251500
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->p:LX/0x2;

    if-nez v0, :cond_0

    .line 251501
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->p:LX/0x2;

    .line 251502
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->p:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 251503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 251504
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 251505
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 251507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 251508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 251509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 251510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 251511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->s()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 251512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 251513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 251514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 251515
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 251516
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 251517
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 251518
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 251519
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 251520
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 251521
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 251522
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 251523
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 251524
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 251525
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 251526
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251527
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 251528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 251530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 251531
    if-eqz v1, :cond_4

    .line 251532
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 251533
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->i:Ljava/util/List;

    move-object v1, v0

    .line 251534
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 251537
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 251538
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251539
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 251540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->s()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 251541
    if-eqz v2, :cond_1

    .line 251542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 251543
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->l:Ljava/util/List;

    move-object v1, v0

    .line 251544
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 251545
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251546
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 251547
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 251548
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251549
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251550
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 251466
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->h:J

    .line 251467
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 251468
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 251469
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->h:J

    .line 251470
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251420
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251422
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o:Ljava/lang/String;

    .line 251423
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251425
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 251426
    :goto_0
    return-object v0

    .line 251427
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 251428
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 251429
    const v0, 0x42554c1c

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251430
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251431
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->f:Ljava/lang/String;

    .line 251432
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 6

    .prologue
    .line 251433
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 251434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->s()LX/0Px;

    move-result-object v0

    .line 251435
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 251436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 251437
    :goto_0
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251438
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 251439
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v5

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    if-eq v5, p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_0
    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 251440
    if-eqz v5, :cond_1

    .line 251441
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 251442
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 251443
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 251444
    return-object v0

    :cond_3
    move-object v1, v0

    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251445
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251446
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->i:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->i:Ljava/util/List;

    .line 251447
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251448
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251449
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251450
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 251451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 251452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->s()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 251453
    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251454
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251455
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k:Ljava/lang/String;

    .line 251456
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251457
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251458
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->l:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->l:Ljava/util/List;

    .line 251459
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251460
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251461
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->m:Ljava/lang/String;

    .line 251462
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251463
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251464
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251465
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
