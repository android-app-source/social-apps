.class public final Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 157311
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 157310
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 157308
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 157309
    return-void
.end method

.method public constructor <init>(LX/0uq;)V
    .locals 1

    .prologue
    .line 157297
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 157298
    iget v0, p1, LX/0uq;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->e:I

    .line 157299
    iget-object v0, p1, LX/0uq;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->f:Ljava/lang/String;

    .line 157300
    iget-object v0, p1, LX/0uq;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->g:Ljava/util/List;

    .line 157301
    iget-object v0, p1, LX/0uq;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->h:Ljava/util/List;

    .line 157302
    iget-boolean v0, p1, LX/0uq;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->i:Z

    .line 157303
    iget-object v0, p1, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 157304
    iget-object v0, p1, LX/0uq;->h:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 157305
    iget-object v0, p1, LX/0uq;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l:Ljava/lang/String;

    .line 157306
    iget-object v0, p1, LX/0uq;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m:Ljava/lang/String;

    .line 157307
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 157294
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 157295
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 157296
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 157274
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 157275
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 157276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 157277
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 157278
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 157279
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->o()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 157280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 157281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 157282
    const/16 v7, 0x9

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 157283
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->a()I

    move-result v7

    invoke-virtual {p1, v8, v7, v8}, LX/186;->a(III)V

    .line 157284
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 157285
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 157286
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 157287
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 157288
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 157289
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 157290
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 157291
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 157292
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 157293
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 157256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 157257
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 157258
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 157259
    if-eqz v1, :cond_3

    .line 157260
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 157261
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->g:Ljava/util/List;

    move-object v1, v0

    .line 157262
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157263
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 157264
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 157265
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 157266
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 157267
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->o()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 157268
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->o()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 157269
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->o()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 157270
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 157271
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 157272
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 157273
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157254
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->g:Ljava/util/List;

    .line 157255
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 157250
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 157251
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->e:I

    .line 157252
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->i:Z

    .line 157253
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 157312
    const v0, -0x5a5cfa2a

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 157224
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 157225
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->f:Ljava/lang/String;

    .line 157226
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157227
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 157228
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->g:Ljava/util/List;

    .line 157229
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157230
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 157231
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->h:Ljava/util/List;

    .line 157232
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 157233
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 157234
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 157235
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->i:Z

    return v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 157236
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 157237
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 157238
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 157239
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 157240
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 157241
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 157242
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 157243
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l:Ljava/lang/String;

    .line 157244
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 157245
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 157246
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m:Ljava/lang/String;

    .line 157247
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 157248
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "size"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "pi"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "di"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "ledk"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 157249
    return-object v0
.end method
