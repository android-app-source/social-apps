.class public final Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

.field public i:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322144
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 322143
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 322141
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322142
    return-void
.end method

.method public constructor <init>(LX/4X1;)V
    .locals 1

    .prologue
    .line 322134
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 322135
    iget v0, p1, LX/4X1;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->i:I

    .line 322136
    iget-object v0, p1, LX/4X1;->c:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->h:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 322137
    iget v0, p1, LX/4X1;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->f:I

    .line 322138
    iget v0, p1, LX/4X1;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->g:I

    .line 322139
    iget v0, p1, LX/4X1;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->e:I

    .line 322140
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 322128
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322129
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322130
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 322118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322119
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 322120
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->a()I

    move-result v0

    invoke-virtual {p1, v3, v0, v3}, LX/186;->a(III)V

    .line 322121
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 322122
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 322123
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->l()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 322124
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 322125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322126
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 322127
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->l()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 322131
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 322132
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 322133
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 322112
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 322113
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->e:I

    .line 322114
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->f:I

    .line 322115
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->g:I

    .line 322116
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->i:I

    .line 322117
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 322111
    const v0, 0xaf49346

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322099
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322100
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322101
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->f:I

    return v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322108
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322109
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322110
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->g:I

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->h:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 322106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->h:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->h:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    .line 322107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->h:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    return-object v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 322102
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 322103
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 322104
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->i:I

    return v0
.end method
