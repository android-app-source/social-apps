.class public final Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I

.field public l:Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 252534
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 252535
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 252536
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 252537
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x21900448

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 252538
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->p:LX/0x2;

    .line 252539
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 252540
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->k:I

    .line 252541
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252542
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252543
    if-eqz v0, :cond_0

    .line 252544
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 252545
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 252546
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->i:Ljava/lang/String;

    .line 252547
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252548
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252549
    if-eqz v0, :cond_0

    .line 252550
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 252551
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 252552
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->j:Ljava/lang/String;

    .line 252553
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 252554
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 252555
    if-eqz v0, :cond_0

    .line 252556
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 252557
    :cond_0
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252558
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252559
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->i:Ljava/lang/String;

    .line 252560
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252515
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252516
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->j:Ljava/lang/String;

    .line 252517
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private v()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252561
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252562
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252563
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->k:I

    return v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252564
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252565
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->m:Ljava/lang/String;

    .line 252566
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 252567
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252568
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252569
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g:Ljava/lang/String;

    .line 252570
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 252571
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 252572
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 252573
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->h:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 252574
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 252575
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 252576
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->p:LX/0x2;

    if-nez v0, :cond_0

    .line 252577
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->p:LX/0x2;

    .line 252578
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->p:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 252579
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 252532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 252533
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 252580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 252582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 252583
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 252584
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 252585
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 252586
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 252587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 252588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 252589
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 252590
    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 252591
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 252592
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 252593
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 252594
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 252595
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->v()I

    move-result v1

    invoke-virtual {p1, v0, v1, v12}, LX/186;->a(III)V

    .line 252596
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 252597
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 252598
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 252599
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 252600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252601
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 252469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 252470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    .line 252472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 252473
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    .line 252474
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    .line 252475
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 252476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 252478
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    .line 252479
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252480
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 252481
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 252482
    new-instance v0, LX/4Xb;

    invoke-direct {v0, p1}, LX/4Xb;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 252484
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->h:J

    .line 252485
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 252486
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 252487
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->h:J

    .line 252488
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->k:I

    .line 252489
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 252490
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252491
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252492
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252493
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 252494
    :goto_0
    return-void

    .line 252495
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252496
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252497
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252498
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 252499
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252500
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 252501
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 252502
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 252503
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 252504
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252505
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->a(Ljava/lang/String;)V

    .line 252506
    :cond_0
    :goto_0
    return-void

    .line 252507
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252508
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 252509
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252510
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252511
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252512
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252513
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o:Ljava/lang/String;

    .line 252514
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252465
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 252466
    :goto_0
    return-object v0

    .line 252467
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 252468
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 252518
    const v0, -0x21900448

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252519
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252520
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->f:Ljava/lang/String;

    .line 252521
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 252522
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->I_()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16d;

    invoke-static {v0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 252523
    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252524
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252525
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    .line 252526
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 252527
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 252528
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 252529
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 252530
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 252531
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
