.class public final Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackReaction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedbackReaction$Serializer;
.end annotation


# instance fields
.field public e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321836
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321835
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321833
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321834
    return-void
.end method

.method public constructor <init>(LX/4WL;)V
    .locals 1

    .prologue
    .line 321830
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321831
    iget v0, p1, LX/4WL;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;->e:I

    .line 321832
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 321837
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321838
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321839
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 321825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321826
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 321827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 321828
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321829
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 321822
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321824
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 321819
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 321820
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;->e:I

    .line 321821
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 321818
    const v0, 0x6f218ee

    return v0
.end method
