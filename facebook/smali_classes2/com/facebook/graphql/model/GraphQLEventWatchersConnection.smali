.class public final Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventWatchersConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventWatchersConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventWatchersEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323861
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323860
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 323858
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323859
    return-void
.end method

.method public constructor <init>(LX/4WD;)V
    .locals 1

    .prologue
    .line 323798
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323799
    iget v0, p1, LX/4WD;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->e:I

    .line 323800
    iget-object v0, p1, LX/4WD;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->f:Ljava/util/List;

    .line 323801
    iget-object v0, p1, LX/4WD;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->g:Ljava/util/List;

    .line 323802
    iget-object v0, p1, LX/4WD;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323803
    iget v0, p1, LX/4WD;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->i:I

    .line 323804
    iget v0, p1, LX/4WD;->g:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j:I

    .line 323805
    return-void
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method private n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323852
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323853
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323854
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j:I

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 323849
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323850
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323851
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 323836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323837
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 323838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 323839
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 323840
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 323841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->a()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 323842
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 323843
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 323844
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 323845
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->l()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323846
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->n()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323848
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 323818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323821
    if-eqz v1, :cond_0

    .line 323822
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 323823
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->f:Ljava/util/List;

    .line 323824
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 323825
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323826
    if-eqz v1, :cond_1

    .line 323827
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 323828
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 323829
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 323830
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323831
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->m()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 323832
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 323833
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->h:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323834
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323835
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 323812
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->e:I

    .line 323813
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 323814
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 323815
    if-eqz v0, :cond_0

    .line 323816
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 323817
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323807
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 323808
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->e:I

    .line 323809
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->i:I

    .line 323810
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j:I

    .line 323811
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 323806
    const v0, 0x7a36f48f

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventWatchersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323795
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323796
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventWatchersEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->f:Ljava/util/List;

    .line 323797
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323792
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323793
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->g:Ljava/util/List;

    .line 323794
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323789
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323790
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323791
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->i:I

    return v0
.end method
