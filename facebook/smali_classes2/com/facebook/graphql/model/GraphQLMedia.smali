.class public final Lcom/facebook/graphql/model/GraphQLMedia;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMedia$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMedia$Serializer;
.end annotation


# instance fields
.field public A:J

.field public B:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLVect2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:I

.field public L:I

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:I

.field public O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoEncoding;",
            ">;"
        }
    .end annotation
.end field

.field public aJ:I

.field public aK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:I

.field public aM:I

.field public aN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:I

.field public aR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Z

.field public aZ:Z

.field public aa:I

.field public ab:I

.field public ac:I

.field public ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;",
            ">;"
        }
    .end annotation
.end field

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:Z

.field public ak:Z

.field public al:Z

.field public am:Z

.field public an:Z

.field public ao:Z

.field public ap:Z

.field public aq:Z

.field public ar:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:I

.field public av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:I

.field public ax:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bB:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

.field public bF:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public bG:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:Z

.field public bI:D

.field public bJ:D

.field public bK:Z

.field public bL:Z

.field public bM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bR:Z

.field public bS:I

.field public bT:Z

.field public ba:Z

.field public bb:Z

.field public bc:Z

.field public bd:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public be:D

.field public bf:D

.field public bg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:I

.field public bj:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bm:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Z

.field public bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:I

.field public bq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public br:Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:I

.field public bt:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bu:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public bv:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bw:I

.field public bx:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:J

.field public m:I

.field public n:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 260180
    const-class v0, Lcom/facebook/graphql/model/GraphQLMedia$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 259864
    const-class v0, Lcom/facebook/graphql/model/GraphQLMedia$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 259865
    const/16 v0, 0x96

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 259866
    return-void
.end method

.method public constructor <init>(LX/4XB;)V
    .locals 2

    .prologue
    .line 259867
    const/16 v0, 0x96

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 259868
    iget-object v0, p1, LX/4XB;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->f:Ljava/lang/String;

    .line 259869
    iget-object v0, p1, LX/4XB;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259870
    iget-object v0, p1, LX/4XB;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259871
    iget v0, p1, LX/4XB;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->i:I

    .line 259872
    iget-object v0, p1, LX/4XB;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 259873
    iget-object v0, p1, LX/4XB;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->k:Ljava/lang/String;

    .line 259874
    iget-object v0, p1, LX/4XB;->h:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 259875
    iget-wide v0, p1, LX/4XB;->i:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->l:J

    .line 259876
    iget v0, p1, LX/4XB;->j:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->m:I

    .line 259877
    iget-object v0, p1, LX/4XB;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->n:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 259878
    iget-boolean v0, p1, LX/4XB;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->o:Z

    .line 259879
    iget-boolean v0, p1, LX/4XB;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->p:Z

    .line 259880
    iget-boolean v0, p1, LX/4XB;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bK:Z

    .line 259881
    iget-boolean v0, p1, LX/4XB;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->q:Z

    .line 259882
    iget-boolean v0, p1, LX/4XB;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->r:Z

    .line 259883
    iget-boolean v0, p1, LX/4XB;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->s:Z

    .line 259884
    iget-boolean v0, p1, LX/4XB;->r:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->t:Z

    .line 259885
    iget-boolean v0, p1, LX/4XB;->s:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->u:Z

    .line 259886
    iget-boolean v0, p1, LX/4XB;->t:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->v:Z

    .line 259887
    iget-boolean v0, p1, LX/4XB;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bR:Z

    .line 259888
    iget-boolean v0, p1, LX/4XB;->v:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->w:Z

    .line 259889
    iget-boolean v0, p1, LX/4XB;->w:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->x:Z

    .line 259890
    iget-object v0, p1, LX/4XB;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->y:Ljava/lang/String;

    .line 259891
    iget-object v0, p1, LX/4XB;->y:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 259892
    iget-object v0, p1, LX/4XB;->z:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bG:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 259893
    iget-wide v0, p1, LX/4XB;->A:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->A:J

    .line 259894
    iget-object v0, p1, LX/4XB;->B:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 259895
    iget-object v0, p1, LX/4XB;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->by:Ljava/lang/String;

    .line 259896
    iget-boolean v0, p1, LX/4XB;->D:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bH:Z

    .line 259897
    iget-object v0, p1, LX/4XB;->E:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->C:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 259898
    iget-object v0, p1, LX/4XB;->F:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->D:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 259899
    iget-object v0, p1, LX/4XB;->G:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 259900
    iget-object v0, p1, LX/4XB;->H:Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->F:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 259901
    iget-wide v0, p1, LX/4XB;->I:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bI:D

    .line 259902
    iget-object v0, p1, LX/4XB;->J:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->G:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 259903
    iget-boolean v0, p1, LX/4XB;->K:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->H:Z

    .line 259904
    iget-boolean v0, p1, LX/4XB;->L:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->I:Z

    .line 259905
    iget-boolean v0, p1, LX/4XB;->M:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->J:Z

    .line 259906
    iget v0, p1, LX/4XB;->N:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->K:I

    .line 259907
    iget v0, p1, LX/4XB;->O:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->L:I

    .line 259908
    iget-object v0, p1, LX/4XB;->P:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bN:Ljava/lang/String;

    .line 259909
    iget-object v0, p1, LX/4XB;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bO:Ljava/lang/String;

    .line 259910
    iget-object v0, p1, LX/4XB;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->M:Ljava/lang/String;

    .line 259911
    iget v0, p1, LX/4XB;->S:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->N:I

    .line 259912
    iget-object v0, p1, LX/4XB;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->O:Ljava/lang/String;

    .line 259913
    iget-object v0, p1, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259914
    iget-object v0, p1, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259915
    iget-object v0, p1, LX/4XB;->W:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259916
    iget-object v0, p1, LX/4XB;->X:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259917
    iget-object v0, p1, LX/4XB;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259918
    iget-object v0, p1, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259919
    iget-object v0, p1, LX/4XB;->aa:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259920
    iget-object v0, p1, LX/4XB;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259921
    iget-object v0, p1, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259922
    iget-object v0, p1, LX/4XB;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259923
    iget-object v0, p1, LX/4XB;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259924
    iget-object v0, p1, LX/4XB;->af:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259925
    iget-object v0, p1, LX/4XB;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259926
    iget v0, p1, LX/4XB;->ah:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aa:I

    .line 259927
    iget v0, p1, LX/4XB;->ai:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ab:I

    .line 259928
    iget v0, p1, LX/4XB;->aj:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ac:I

    .line 259929
    iget-object v0, p1, LX/4XB;->ak:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 259930
    iget-object v0, p1, LX/4XB;->al:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ae:Ljava/util/List;

    .line 259931
    iget-boolean v0, p1, LX/4XB;->am:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->af:Z

    .line 259932
    iget-boolean v0, p1, LX/4XB;->an:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ag:Z

    .line 259933
    iget-boolean v0, p1, LX/4XB;->ao:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ah:Z

    .line 259934
    iget-boolean v0, p1, LX/4XB;->ap:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ai:Z

    .line 259935
    iget-boolean v0, p1, LX/4XB;->aq:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bL:Z

    .line 259936
    iget-boolean v0, p1, LX/4XB;->ar:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aj:Z

    .line 259937
    iget-boolean v0, p1, LX/4XB;->as:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ak:Z

    .line 259938
    iget-boolean v0, p1, LX/4XB;->at:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->al:Z

    .line 259939
    iget-boolean v0, p1, LX/4XB;->au:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bT:Z

    .line 259940
    iget-boolean v0, p1, LX/4XB;->av:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->am:Z

    .line 259941
    iget-boolean v0, p1, LX/4XB;->aw:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->an:Z

    .line 259942
    iget-boolean v0, p1, LX/4XB;->ax:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ao:Z

    .line 259943
    iget-boolean v0, p1, LX/4XB;->ay:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ap:Z

    .line 259944
    iget-boolean v0, p1, LX/4XB;->az:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aq:Z

    .line 259945
    iget-object v0, p1, LX/4XB;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259946
    iget-object v0, p1, LX/4XB;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259947
    iget-object v0, p1, LX/4XB;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259948
    iget-object v0, p1, LX/4XB;->aD:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bz:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259949
    iget v0, p1, LX/4XB;->aE:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bF:I

    .line 259950
    iget v0, p1, LX/4XB;->aF:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->au:I

    .line 259951
    iget-object v0, p1, LX/4XB;->aG:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 259952
    iget v0, p1, LX/4XB;->aH:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aw:I

    .line 259953
    iget-object v0, p1, LX/4XB;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259954
    iget v0, p1, LX/4XB;->aJ:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bS:I

    .line 259955
    iget-object v0, p1, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 259956
    iget-object v0, p1, LX/4XB;->aL:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->az:Ljava/lang/String;

    .line 259957
    iget-object v0, p1, LX/4XB;->aM:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aA:Ljava/util/List;

    .line 259958
    iget-object v0, p1, LX/4XB;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259959
    iget-object v0, p1, LX/4XB;->aO:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aC:Ljava/lang/String;

    .line 259960
    iget-object v0, p1, LX/4XB;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259961
    iget-object v0, p1, LX/4XB;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259962
    iget-wide v0, p1, LX/4XB;->aR:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bJ:D

    .line 259963
    iget-object v0, p1, LX/4XB;->aS:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    .line 259964
    iget-object v0, p1, LX/4XB;->aT:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bC:Ljava/lang/String;

    .line 259965
    iget-object v0, p1, LX/4XB;->aU:Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aG:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 259966
    iget-object v0, p1, LX/4XB;->aV:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aH:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 259967
    iget-object v0, p1, LX/4XB;->aW:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aI:Ljava/util/List;

    .line 259968
    iget-object v0, p1, LX/4XB;->aX:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bD:Ljava/lang/String;

    .line 259969
    iget v0, p1, LX/4XB;->aY:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aJ:I

    .line 259970
    iget-object v0, p1, LX/4XB;->aZ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aK:Ljava/lang/String;

    .line 259971
    iget v0, p1, LX/4XB;->ba:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aL:I

    .line 259972
    iget v0, p1, LX/4XB;->bb:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aM:I

    .line 259973
    iget-object v0, p1, LX/4XB;->bc:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aN:Ljava/lang/String;

    .line 259974
    iget-object v0, p1, LX/4XB;->bd:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aO:Ljava/lang/String;

    .line 259975
    iget-object v0, p1, LX/4XB;->be:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259976
    iget v0, p1, LX/4XB;->bf:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aQ:I

    .line 259977
    iget-object v0, p1, LX/4XB;->bg:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aR:Ljava/lang/String;

    .line 259978
    iget-object v0, p1, LX/4XB;->bh:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 259979
    iget-object v0, p1, LX/4XB;->bi:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bM:Ljava/lang/String;

    .line 259980
    iget-object v0, p1, LX/4XB;->bj:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aT:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 259981
    iget-object v0, p1, LX/4XB;->bk:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aU:Ljava/lang/String;

    .line 259982
    iget-object v0, p1, LX/4XB;->bl:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aV:Ljava/lang/String;

    .line 259983
    iget-object v0, p1, LX/4XB;->bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aW:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 259984
    iget-object v0, p1, LX/4XB;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259985
    iget-object v0, p1, LX/4XB;->bo:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bP:Ljava/lang/String;

    .line 259986
    iget-boolean v0, p1, LX/4XB;->bp:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aY:Z

    .line 259987
    iget-boolean v0, p1, LX/4XB;->bq:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aZ:Z

    .line 259988
    iget-boolean v0, p1, LX/4XB;->br:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ba:Z

    .line 259989
    iget-boolean v0, p1, LX/4XB;->bs:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bb:Z

    .line 259990
    iget-boolean v0, p1, LX/4XB;->bt:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bc:Z

    .line 259991
    iget-boolean v0, p1, LX/4XB;->bu:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bd:Z

    .line 259992
    iget-wide v0, p1, LX/4XB;->bv:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->be:D

    .line 259993
    iget-wide v0, p1, LX/4XB;->bw:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bf:D

    .line 259994
    iget-object v0, p1, LX/4XB;->bx:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bg:Ljava/lang/String;

    .line 259995
    iget-object v0, p1, LX/4XB;->by:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bh:Ljava/lang/String;

    .line 259996
    iget v0, p1, LX/4XB;->bz:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bi:I

    .line 259997
    iget-object v0, p1, LX/4XB;->bA:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bQ:Ljava/lang/String;

    .line 259998
    iget-object v0, p1, LX/4XB;->bB:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bj:Lcom/facebook/graphql/model/GraphQLPage;

    .line 259999
    iget-object v0, p1, LX/4XB;->bC:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260000
    iget-object v0, p1, LX/4XB;->bD:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bl:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 260001
    iget-object v0, p1, LX/4XB;->bE:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bm:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 260002
    iget-boolean v0, p1, LX/4XB;->bF:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bn:Z

    .line 260003
    iget-object v0, p1, LX/4XB;->bG:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 260004
    iget v0, p1, LX/4XB;->bH:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bp:I

    .line 260005
    iget-object v0, p1, LX/4XB;->bI:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bq:Ljava/util/List;

    .line 260006
    iget-object v0, p1, LX/4XB;->bJ:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->br:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 260007
    iget v0, p1, LX/4XB;->bK:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bs:I

    .line 260008
    iget-object v0, p1, LX/4XB;->bL:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bt:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 260009
    iget-object v0, p1, LX/4XB;->bM:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bu:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 260010
    iget-object v0, p1, LX/4XB;->bN:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260011
    iget v0, p1, LX/4XB;->bO:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bw:I

    .line 260012
    iget-object v0, p1, LX/4XB;->bP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bx:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 260013
    iget-object v0, p1, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 260014
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 260015
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 260016
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 260017
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 260018
    if-eqz v0, :cond_0

    .line 260019
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x73

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 260020
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPlace;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 260021
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLMedia;->C:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260022
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 260023
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 260024
    if-eqz v0, :cond_0

    .line 260025
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 260026
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 260027
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLMedia;->av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 260028
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 260029
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 260030
    if-eqz v0, :cond_0

    .line 260031
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x45

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 260032
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 260033
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260034
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 260035
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 260036
    if-eqz v0, :cond_0

    .line 260037
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x48

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 260038
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLPlace;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 260039
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aH:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260040
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 260041
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 260042
    if-eqz v0, :cond_0

    .line 260043
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x51

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 260044
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260045
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260046
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260047
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->v:Z

    return v0
.end method

.method public final B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 259858
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259859
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259860
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->w:Z

    return v0
.end method

.method public final C()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260051
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260052
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260053
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->x:Z

    return v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260054
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260055
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->y:Ljava/lang/String;

    .line 260056
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260057
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->z:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260058
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->z:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 260059
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->z:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final F()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260060
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260061
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260062
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->A:J

    return-wide v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260063
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->B:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260064
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->B:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 260065
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->B:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260066
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->C:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260067
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->C:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->C:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260068
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->C:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260069
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->D:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260070
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->D:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->D:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 260071
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->D:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260072
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260073
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 260074
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLVect2;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260075
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->F:Lcom/facebook/graphql/model/GraphQLVect2;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260076
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->F:Lcom/facebook/graphql/model/GraphQLVect2;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLVect2;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->F:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 260077
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->F:Lcom/facebook/graphql/model/GraphQLVect2;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259265
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->G:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259266
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->G:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->G:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 259267
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->G:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    return-object v0
.end method

.method public final M()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259235
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259236
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259237
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->H:Z

    return v0
.end method

.method public final N()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259238
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259239
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259240
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->I:Z

    return v0
.end method

.method public final O()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259241
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259242
    const/4 v0, 0x3

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259243
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->J:Z

    return v0
.end method

.method public final P()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259244
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259245
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259246
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->K:I

    return v0
.end method

.method public final Q()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259247
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259248
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259249
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->L:I

    return v0
.end method

.method public final R()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259250
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259251
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->M:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->M:Ljava/lang/String;

    .line 259252
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final S()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259253
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259254
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259255
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->N:I

    return v0
.end method

.method public final T()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259256
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259257
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->O:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->O:Ljava/lang/String;

    .line 259258
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259259
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->P:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259260
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->P:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259261
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->P:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259262
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259263
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259264
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259232
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->R:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259233
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->R:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259234
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->R:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259268
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->S:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259269
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->S:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259270
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->S:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final Y()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259271
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->T:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259272
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->T:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259273
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->T:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259274
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->U:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259275
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->U:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259276
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->U:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 83

    .prologue
    .line 259277
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 259278
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 259279
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 259280
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 259281
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 259282
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 259283
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 259284
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->D()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 259285
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->E()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 259286
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 259287
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 259288
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 259289
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 259290
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 259291
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 259292
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->R()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 259293
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 259294
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 259295
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 259296
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 259297
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 259298
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 259299
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 259300
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 259301
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 259302
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 259303
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 259304
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 259305
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 259306
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v30

    .line 259307
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 259308
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 259309
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 259310
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 259311
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 259312
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 259313
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aE()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 259314
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aF()LX/0Px;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v38

    .line 259315
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 259316
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aH()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 259317
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 259318
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 259319
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 259320
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 259321
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aM()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 259322
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v46

    .line 259323
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aP()Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v47

    .line 259324
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 259325
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 259326
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 259327
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aW()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v51

    .line 259328
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 259329
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 259330
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aZ()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v54

    .line 259331
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ba()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v55

    .line 259332
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 259333
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 259334
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bl()Ljava/lang/String;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v58

    .line 259335
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bm()Ljava/lang/String;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v59

    .line 259336
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bo()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 259337
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 259338
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bq()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 259339
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->br()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 259340
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 259341
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bv()LX/0Px;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v65

    .line 259342
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 259343
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 259344
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v68

    .line 259345
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 259346
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bD()Ljava/lang/String;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v70

    .line 259347
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bE()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v71

    .line 259348
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v72

    .line 259349
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 259350
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bH()Ljava/lang/String;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v74

    .line 259351
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bI()Ljava/lang/String;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v75

    .line 259352
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 259353
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bR()Ljava/lang/String;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v77

    .line 259354
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bS()Ljava/lang/String;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v78

    .line 259355
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bT()Ljava/lang/String;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v79

    .line 259356
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bU()Ljava/lang/String;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v80

    .line 259357
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bV()Ljava/lang/String;

    move-result-object v81

    move-object/from16 v0, p1

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v81

    .line 259358
    const/16 v82, 0x95

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 259359
    const/16 v82, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 259360
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 259361
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 259362
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 259363
    const/4 v2, 0x4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->n()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259364
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 259365
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 259366
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->q()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 259367
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->r()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259368
    const/16 v3, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 259369
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259370
    const/16 v2, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259371
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259372
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259373
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259374
    const/16 v2, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259375
    const/16 v2, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259376
    const/16 v2, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259377
    const/16 v2, 0x12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->B()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259378
    const/16 v2, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->C()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259379
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 259380
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 259381
    const/16 v3, 0x16

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->F()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 259382
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 259383
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 259384
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 259385
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 259386
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 259387
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 259388
    const/16 v2, 0x1d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->M()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259389
    const/16 v2, 0x1e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->N()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259390
    const/16 v2, 0x1f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->O()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259391
    const/16 v2, 0x20

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->P()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259392
    const/16 v2, 0x21

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Q()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259393
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259394
    const/16 v2, 0x23

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259395
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259396
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259397
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259398
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259399
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259400
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259401
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259402
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259403
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259404
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259405
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259406
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259407
    const/16 v2, 0x30

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->af()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259408
    const/16 v2, 0x31

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ag()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259409
    const/16 v2, 0x32

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ah()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259410
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259411
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259412
    const/16 v2, 0x35

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ak()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259413
    const/16 v2, 0x36

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->al()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259414
    const/16 v2, 0x37

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->am()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259415
    const/16 v2, 0x38

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->an()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259416
    const/16 v2, 0x39

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259417
    const/16 v2, 0x3a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ap()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259418
    const/16 v2, 0x3b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259419
    const/16 v2, 0x3c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ar()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259420
    const/16 v2, 0x3d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->as()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259421
    const/16 v2, 0x3e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259422
    const/16 v2, 0x3f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259423
    const/16 v2, 0x40

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->av()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259424
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259425
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259426
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259427
    const/16 v2, 0x44

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->az()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259428
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259429
    const/16 v2, 0x46

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aB()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259430
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259431
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259432
    const/16 v2, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259433
    const/16 v2, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259434
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259435
    const/16 v2, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259436
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259437
    const/16 v2, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259438
    const/16 v2, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259439
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259440
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259441
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259442
    const/16 v2, 0x53

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259443
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259444
    const/16 v2, 0x56

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aQ()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259445
    const/16 v2, 0x57

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259446
    const/16 v2, 0x58

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259447
    const/16 v2, 0x59

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259448
    const/16 v2, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259449
    const/16 v2, 0x5b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aV()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259450
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259451
    const/16 v2, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259452
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259453
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259454
    const/16 v2, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259455
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259456
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259457
    const/16 v2, 0x63

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bd()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259458
    const/16 v2, 0x64

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->be()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259459
    const/16 v2, 0x65

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bf()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259460
    const/16 v2, 0x66

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bg()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259461
    const/16 v2, 0x67

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bh()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259462
    const/16 v2, 0x68

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bi()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259463
    const/16 v3, 0x69

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bj()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 259464
    const/16 v3, 0x6a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bk()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 259465
    const/16 v2, 0x6b

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259466
    const/16 v2, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259467
    const/16 v2, 0x6d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bn()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259468
    const/16 v2, 0x6e

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259469
    const/16 v2, 0x6f

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259470
    const/16 v2, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259471
    const/16 v2, 0x71

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259472
    const/16 v2, 0x72

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bs()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259473
    const/16 v2, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259474
    const/16 v2, 0x74

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bu()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259475
    const/16 v2, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259476
    const/16 v2, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259477
    const/16 v2, 0x77

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259478
    const/16 v2, 0x78

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259479
    const/16 v3, 0x79

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bz()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 259480
    const/16 v2, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259481
    const/16 v2, 0x7b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259482
    const/16 v2, 0x7c

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259483
    const/16 v2, 0x7e

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259484
    const/16 v2, 0x7f

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259485
    const/16 v2, 0x80

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259486
    const/16 v2, 0x81

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259487
    const/16 v2, 0x82

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259488
    const/16 v2, 0x83

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259489
    const/16 v3, 0x84

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bJ()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 259490
    const/16 v2, 0x85

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bK()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259491
    const/16 v2, 0x87

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259492
    const/16 v2, 0x88

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bM()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259493
    const/16 v3, 0x89

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bN()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 259494
    const/16 v3, 0x8a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bO()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 259495
    const/16 v2, 0x8b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bP()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259496
    const/16 v2, 0x8c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259497
    const/16 v2, 0x8d

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259498
    const/16 v2, 0x8e

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259499
    const/16 v2, 0x8f

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259500
    const/16 v2, 0x90

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259501
    const/16 v2, 0x91

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 259502
    const/16 v2, 0x92

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bW()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259503
    const/16 v2, 0x93

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bX()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 259504
    const/16 v2, 0x94

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bY()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 259505
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 259506
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 259507
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 259508
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    goto/16 :goto_1

    .line 259509
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bz()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v2

    goto/16 :goto_2

    .line 259510
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bJ()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 259511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 259512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 259515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259516
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259517
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 259518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 259520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259521
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259522
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 259523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 259524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 259525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259526
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 259527
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->E()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 259528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->E()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 259529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->E()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 259530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259531
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 259532
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 259533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 259534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 259535
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259536
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bG:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 259537
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 259538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 259539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 259540
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259541
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 259542
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 259543
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 259544
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 259545
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259546
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->C:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 259547
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 259548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 259549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 259550
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259551
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->D:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 259552
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 259553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 259554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 259555
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259556
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->E:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 259557
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 259558
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVect2;

    .line 259559
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 259560
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259561
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->F:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 259562
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 259563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 259564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 259565
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259566
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->G:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 259567
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 259568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259569
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 259570
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259571
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259572
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 259573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 259575
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259576
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259577
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 259578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259579
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 259580
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259581
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259582
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 259583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 259585
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259586
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259587
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 259588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259589
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 259590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259591
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259592
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 259593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 259595
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259596
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259597
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 259598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259599
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 259600
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259601
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259602
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 259603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 259605
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259606
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259607
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 259608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259609
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 259610
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259611
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259612
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 259613
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 259615
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259616
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259617
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 259618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 259620
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259621
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259622
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 259623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 259625
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259626
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259627
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 259628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259629
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 259630
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259631
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259632
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 259633
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 259634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 259635
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259636
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 259637
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 259638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 259639
    if-eqz v2, :cond_19

    .line 259640
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259641
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMedia;->ae:Ljava/util/List;

    move-object v1, v0

    .line 259642
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 259643
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 259645
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259646
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259647
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 259648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259649
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 259650
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259651
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259652
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 259653
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259654
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 259655
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259656
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259657
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bE()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 259658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bE()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259659
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bE()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 259660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259661
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bz:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259662
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 259663
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 259664
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 259665
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259666
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 259667
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 259668
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259669
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 259670
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259671
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259672
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 259673
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 259674
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 259675
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259676
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 259677
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aF()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 259678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aF()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 259679
    if-eqz v2, :cond_21

    .line 259680
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259681
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMedia;->aA:Ljava/util/List;

    move-object v1, v0

    .line 259682
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 259683
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 259685
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259686
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259687
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 259688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259689
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 259690
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259691
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259692
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 259693
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 259695
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259696
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259697
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 259698
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 259699
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 259700
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259701
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    .line 259702
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 259703
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 259704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 259705
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259706
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aG:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 259707
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aM()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 259708
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aM()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 259709
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aM()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 259710
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259711
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aH:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 259712
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 259713
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 259714
    if-eqz v2, :cond_28

    .line 259715
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259716
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLMedia;->aI:Ljava/util/List;

    move-object v1, v0

    .line 259717
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 259718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259719
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 259720
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259721
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259722
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 259723
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 259724
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 259725
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259726
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 259727
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 259728
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 259729
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 259730
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259731
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aT:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 259732
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 259733
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 259734
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 259735
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259736
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aW:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 259737
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 259738
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259739
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 259740
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259741
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259742
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bo()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 259743
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bo()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 259744
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bo()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 259745
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259746
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bj:Lcom/facebook/graphql/model/GraphQLPage;

    .line 259747
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 259748
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 259750
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259751
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259752
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bq()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 259753
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bq()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 259754
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bq()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 259755
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259756
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bl:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 259757
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->br()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 259758
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->br()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 259759
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->br()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 259760
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259761
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bm:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 259762
    :cond_31
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 259763
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 259764
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 259765
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259766
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 259767
    :cond_32
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 259768
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 259769
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 259770
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259771
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->br:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 259772
    :cond_33
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 259773
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 259774
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v2

    if-eq v2, v0, :cond_34

    .line 259775
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259776
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bt:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 259777
    :cond_34
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 259778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 259779
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_35

    .line 259780
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259781
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259782
    :cond_35
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 259783
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 259784
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 259785
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 259786
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMedia;->bx:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 259787
    :cond_36
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 259788
    if-nez v1, :cond_37

    :goto_0
    return-object p0

    :cond_37
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 259789
    new-instance v0, LX/4XC;

    invoke-direct {v0, p1}, LX/4XC;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259790
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 259791
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 259792
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->i:I

    .line 259793
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->l:J

    .line 259794
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->m:I

    .line 259795
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->o:Z

    .line 259796
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->p:Z

    .line 259797
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->q:Z

    .line 259798
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->r:Z

    .line 259799
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->s:Z

    .line 259800
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->t:Z

    .line 259801
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->u:Z

    .line 259802
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->v:Z

    .line 259803
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->w:Z

    .line 259804
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->x:Z

    .line 259805
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->A:J

    .line 259806
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->H:Z

    .line 259807
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->I:Z

    .line 259808
    const/16 v0, 0x1f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->J:Z

    .line 259809
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->K:I

    .line 259810
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->L:I

    .line 259811
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->N:I

    .line 259812
    const/16 v0, 0x30

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aa:I

    .line 259813
    const/16 v0, 0x31

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ab:I

    .line 259814
    const/16 v0, 0x32

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ac:I

    .line 259815
    const/16 v0, 0x35

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->af:Z

    .line 259816
    const/16 v0, 0x36

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ag:Z

    .line 259817
    const/16 v0, 0x37

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ah:Z

    .line 259818
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ai:Z

    .line 259819
    const/16 v0, 0x39

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aj:Z

    .line 259820
    const/16 v0, 0x3a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ak:Z

    .line 259821
    const/16 v0, 0x3b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->al:Z

    .line 259822
    const/16 v0, 0x3c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->am:Z

    .line 259823
    const/16 v0, 0x3d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->an:Z

    .line 259824
    const/16 v0, 0x3e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ao:Z

    .line 259825
    const/16 v0, 0x3f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ap:Z

    .line 259826
    const/16 v0, 0x40

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aq:Z

    .line 259827
    const/16 v0, 0x44

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->au:I

    .line 259828
    const/16 v0, 0x46

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aw:I

    .line 259829
    const/16 v0, 0x53

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aJ:I

    .line 259830
    const/16 v0, 0x56

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aL:I

    .line 259831
    const/16 v0, 0x57

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aM:I

    .line 259832
    const/16 v0, 0x5b

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aQ:I

    .line 259833
    const/16 v0, 0x63

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aY:Z

    .line 259834
    const/16 v0, 0x64

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aZ:Z

    .line 259835
    const/16 v0, 0x65

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ba:Z

    .line 259836
    const/16 v0, 0x66

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bb:Z

    .line 259837
    const/16 v0, 0x67

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bc:Z

    .line 259838
    const/16 v0, 0x68

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bd:Z

    .line 259839
    const/16 v0, 0x69

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->be:D

    .line 259840
    const/16 v0, 0x6a

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bf:D

    .line 259841
    const/16 v0, 0x6d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bi:I

    .line 259842
    const/16 v0, 0x72

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bn:Z

    .line 259843
    const/16 v0, 0x74

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bp:I

    .line 259844
    const/16 v0, 0x77

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bs:I

    .line 259845
    const/16 v0, 0x7b

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bw:I

    .line 259846
    const/16 v0, 0x85

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bF:I

    .line 259847
    const/16 v0, 0x88

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bH:Z

    .line 259848
    const/16 v0, 0x89

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bI:D

    .line 259849
    const/16 v0, 0x8a

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bJ:D

    .line 259850
    const/16 v0, 0x8b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bK:Z

    .line 259851
    const/16 v0, 0x8c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bL:Z

    .line 259852
    const/16 v0, 0x92

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bR:Z

    .line 259853
    const/16 v0, 0x93

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bS:I

    .line 259854
    const/16 v0, 0x94

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bT:Z

    .line 259855
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 259856
    invoke-virtual {p2}, LX/18L;->a()V

    .line 259857
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 260139
    const-string v0, "explicit_place"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260140
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLMedia;->a(Lcom/facebook/graphql/model/GraphQLPlace;)V

    .line 260141
    :cond_0
    :goto_0
    return-void

    .line 260142
    :cond_1
    const-string v0, "location_tag_suggestion"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260143
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLMedia;->a(Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;)V

    goto :goto_0

    .line 260144
    :cond_2
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260145
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLMedia;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 260146
    :cond_3
    const-string v0, "pending_place"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260147
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLMedia;->b(Lcom/facebook/graphql/model/GraphQLPlace;)V

    goto :goto_0

    .line 260148
    :cond_4
    const-string v0, "tags"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260149
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLMedia;->a(Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 260108
    return-void
.end method

.method public final aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 260152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->av:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    return-object v0
.end method

.method public final aB()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260153
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260154
    const/16 v0, 0x8

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260155
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aw:I

    return v0
.end method

.method public final aC()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x47

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ay:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aE()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->az:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->az:Ljava/lang/String;

    const/16 v1, 0x49

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->az:Ljava/lang/String;

    .line 260164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->az:Ljava/lang/String;

    return-object v0
.end method

.method public final aF()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aA:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aA:Ljava/util/List;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aA:Ljava/util/List;

    .line 260167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aA:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aG()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aH()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260171
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aC:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260172
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aC:Ljava/lang/String;

    const/16 v1, 0x4c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aC:Ljava/lang/String;

    .line 260173
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aC:Ljava/lang/String;

    return-object v0
.end method

.method public final aI()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260177
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260178
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260179
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aJ()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260174
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260175
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260176
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aK()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260199
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260200
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    .line 260201
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final aL()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260196
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aG:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260197
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aG:Lcom/facebook/graphql/model/GraphQLVideo;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aG:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 260198
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aG:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method public final aM()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260193
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aH:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260194
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aH:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x51

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aH:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 260195
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aH:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final aN()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoEncoding;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260190
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aI:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260191
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aI:Ljava/util/List;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aI:Ljava/util/List;

    .line 260192
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aI:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aO()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260187
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260188
    const/16 v0, 0xa

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260189
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aJ:I

    return v0
.end method

.method public final aP()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aK:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aK:Ljava/lang/String;

    const/16 v1, 0x54

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aK:Ljava/lang/String;

    .line 260186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aK:Ljava/lang/String;

    return-object v0
.end method

.method public final aQ()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260181
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260182
    const/16 v0, 0xa

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260183
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aL:I

    return v0
.end method

.method public final aR()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260136
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260137
    const/16 v0, 0xa

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260138
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aM:I

    return v0
.end method

.method public final aS()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259169
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aN:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259170
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aN:Ljava/lang/String;

    const/16 v1, 0x58

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aN:Ljava/lang/String;

    .line 259171
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aN:Ljava/lang/String;

    return-object v0
.end method

.method public final aT()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260081
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aO:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260082
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aO:Ljava/lang/String;

    const/16 v1, 0x59

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aO:Ljava/lang/String;

    .line 260083
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aO:Ljava/lang/String;

    return-object v0
.end method

.method public final aU()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260084
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260085
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260086
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aV()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260087
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260088
    const/16 v0, 0xb

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260089
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aQ:I

    return v0
.end method

.method public final aW()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260090
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aR:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260091
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aR:Ljava/lang/String;

    const/16 v1, 0x5c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aR:Ljava/lang/String;

    .line 260092
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aR:Ljava/lang/String;

    return-object v0
.end method

.method public final aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260093
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260094
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x5d

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 260095
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aS:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aT:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aT:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aT:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 260098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aT:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    return-object v0
.end method

.method public final aZ()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aU:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aU:Ljava/lang/String;

    const/16 v1, 0x5f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aU:Ljava/lang/String;

    .line 260101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aU:Ljava/lang/String;

    return-object v0
.end method

.method public final aa()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260102
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->V:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260103
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->V:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260104
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->V:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ab()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->W:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->W:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->W:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ac()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260078
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->X:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260079
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->X:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260080
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->X:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ad()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260109
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260110
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260111
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ae()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260112
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260113
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260114
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final af()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260115
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260116
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260117
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aa:I

    return v0
.end method

.method public final ag()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260118
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260119
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260120
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ab:I

    return v0
.end method

.method public final ah()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260121
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260122
    const/4 v0, 0x6

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260123
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ac:I

    return v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260124
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260125
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 260126
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    return-object v0
.end method

.method public final aj()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260127
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ae:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260128
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ae:Ljava/util/List;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ae:Ljava/util/List;

    .line 260129
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ae:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final ak()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260130
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260131
    const/4 v0, 0x6

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260132
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->af:Z

    return v0
.end method

.method public final al()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x6

    .line 260133
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260134
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260135
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ag:Z

    return v0
.end method

.method public final am()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260048
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260049
    const/4 v0, 0x6

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260050
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ah:Z

    return v0
.end method

.method public final an()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259861
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259862
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259863
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ai:Z

    return v0
.end method

.method public final ao()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259048
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259049
    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259050
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aj:Z

    return v0
.end method

.method public final ap()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259051
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259052
    const/4 v0, 0x7

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259053
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ak:Z

    return v0
.end method

.method public final aq()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259054
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259055
    const/4 v0, 0x7

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259056
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->al:Z

    return v0
.end method

.method public final ar()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259057
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259058
    const/4 v0, 0x7

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259059
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->am:Z

    return v0
.end method

.method public final as()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259060
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259061
    const/4 v0, 0x7

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259062
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->an:Z

    return v0
.end method

.method public final at()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259063
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259064
    const/4 v0, 0x7

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259065
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ao:Z

    return v0
.end method

.method public final au()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x7

    .line 259066
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259067
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259068
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ap:Z

    return v0
.end method

.method public final av()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259069
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259070
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259071
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aq:Z

    return v0
.end method

.method public final aw()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259072
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259073
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259074
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ax()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259075
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->as:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259076
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->as:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259077
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->as:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ay()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259045
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->at:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259046
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->at:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259047
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->at:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final az()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259081
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259082
    const/16 v0, 0x8

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259083
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->au:I

    return v0
.end method

.method public final bA()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259084
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259085
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x7a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259086
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bB()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259087
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259088
    const/16 v0, 0xf

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259089
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bw:I

    return v0
.end method

.method public final bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259090
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bx:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259091
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bx:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    const/16 v1, 0x7c

    const-class v2, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bx:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 259092
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bx:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    return-object v0
.end method

.method public final bD()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259093
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->by:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259094
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->by:Ljava/lang/String;

    const/16 v1, 0x7e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->by:Ljava/lang/String;

    .line 259095
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->by:Ljava/lang/String;

    return-object v0
.end method

.method public final bE()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bz:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bz:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/16 v1, 0x7f

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bz:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bz:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final bF()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bA:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bA:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x80

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bA:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bG()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259102
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259103
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x81

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259104
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bH()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bC:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bC:Ljava/lang/String;

    const/16 v1, 0x82

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bC:Ljava/lang/String;

    .line 259107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bC:Ljava/lang/String;

    return-object v0
.end method

.method public final bI()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259078
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bD:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259079
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bD:Ljava/lang/String;

    const/16 v1, 0x83

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bD:Ljava/lang/String;

    .line 259080
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bD:Ljava/lang/String;

    return-object v0
.end method

.method public final bJ()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 258985
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 258986
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    const/16 v1, 0x84

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 258987
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    return-object v0
.end method

.method public final bK()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 258988
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 258989
    const/16 v0, 0x10

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 258990
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bF:I

    return v0
.end method

.method public final bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 258991
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bG:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 258992
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bG:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    const/16 v1, 0x87

    const-class v2, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bG:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 258993
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bG:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    return-object v0
.end method

.method public final bM()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 258994
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 258995
    const/16 v0, 0x11

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 258996
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bH:Z

    return v0
.end method

.method public final bN()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 258997
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 258998
    const/16 v0, 0x11

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 258999
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bI:D

    return-wide v0
.end method

.method public final bO()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259000
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259001
    const/16 v0, 0x11

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259002
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bJ:D

    return-wide v0
.end method

.method public final bP()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259003
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259004
    const/16 v0, 0x11

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259005
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bK:Z

    return v0
.end method

.method public final bQ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259006
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259007
    const/16 v0, 0x11

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259008
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bL:Z

    return v0
.end method

.method public final bR()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259009
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bM:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259010
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bM:Ljava/lang/String;

    const/16 v1, 0x8d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bM:Ljava/lang/String;

    .line 259011
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bM:Ljava/lang/String;

    return-object v0
.end method

.method public final bS()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259012
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bN:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259013
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bN:Ljava/lang/String;

    const/16 v1, 0x8e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bN:Ljava/lang/String;

    .line 259014
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bN:Ljava/lang/String;

    return-object v0
.end method

.method public final bT()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259015
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bO:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259016
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bO:Ljava/lang/String;

    const/16 v1, 0x8f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bO:Ljava/lang/String;

    .line 259017
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bO:Ljava/lang/String;

    return-object v0
.end method

.method public final bU()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259018
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bP:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259019
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bP:Ljava/lang/String;

    const/16 v1, 0x90

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bP:Ljava/lang/String;

    .line 259020
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bP:Ljava/lang/String;

    return-object v0
.end method

.method public final bV()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259021
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bQ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259022
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bQ:Ljava/lang/String;

    const/16 v1, 0x91

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bQ:Ljava/lang/String;

    .line 259023
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bQ:Ljava/lang/String;

    return-object v0
.end method

.method public final bW()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259024
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259025
    const/16 v0, 0x12

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259026
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bR:Z

    return v0
.end method

.method public final bX()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259027
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259028
    const/16 v0, 0x12

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259029
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bS:I

    return v0
.end method

.method public final bY()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259030
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259031
    const/16 v0, 0x12

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259032
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bT:Z

    return v0
.end method

.method public final ba()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259033
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aV:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259034
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aV:Ljava/lang/String;

    const/16 v1, 0x60

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aV:Ljava/lang/String;

    .line 259035
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aV:Ljava/lang/String;

    return-object v0
.end method

.method public final bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259036
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aW:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259037
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aW:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aW:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 259038
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aW:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final bc()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259039
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259040
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x62

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259041
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bd()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259042
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259043
    const/16 v0, 0xc

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259044
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aY:Z

    return v0
.end method

.method public final be()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259202
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259203
    const/16 v0, 0xc

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259204
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->aZ:Z

    return v0
.end method

.method public final bf()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259175
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259176
    const/16 v0, 0xc

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259177
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->ba:Z

    return v0
.end method

.method public final bg()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259178
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259179
    const/16 v0, 0xc

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259180
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bb:Z

    return v0
.end method

.method public final bh()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259181
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259182
    const/16 v0, 0xc

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259183
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bc:Z

    return v0
.end method

.method public final bi()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 259184
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259185
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259186
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bd:Z

    return v0
.end method

.method public final bj()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259187
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259188
    const/16 v0, 0xd

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259189
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->be:D

    return-wide v0
.end method

.method public final bk()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259190
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259191
    const/16 v0, 0xd

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259192
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bf:D

    return-wide v0
.end method

.method public final bl()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259193
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bg:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259194
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bg:Ljava/lang/String;

    const/16 v1, 0x6b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bg:Ljava/lang/String;

    .line 259195
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bg:Ljava/lang/String;

    return-object v0
.end method

.method public final bm()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259196
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bh:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259197
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bh:Ljava/lang/String;

    const/16 v1, 0x6c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bh:Ljava/lang/String;

    .line 259198
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bh:Ljava/lang/String;

    return-object v0
.end method

.method public final bn()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259199
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259200
    const/16 v0, 0xd

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259201
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bi:I

    return v0
.end method

.method public final bo()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259172
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bj:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259173
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bj:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x6e

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bj:Lcom/facebook/graphql/model/GraphQLPage;

    .line 259174
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bj:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final bp()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259205
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259206
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x6f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259207
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bk:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bq()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259208
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bl:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259209
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bl:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x70

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bl:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 259210
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bl:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method public final br()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259211
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bm:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259212
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bm:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x71

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bm:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 259213
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bm:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method public final bs()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259214
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259215
    const/16 v0, 0xe

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259216
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bn:Z

    return v0
.end method

.method public final bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259217
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259218
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 259219
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bo:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    return-object v0
.end method

.method public final bu()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259220
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259221
    const/16 v0, 0xe

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259222
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bp:I

    return v0
.end method

.method public final bv()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259223
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bq:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259224
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bq:Ljava/util/List;

    const/16 v1, 0x75

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bq:Ljava/util/List;

    .line 259225
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bq:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->br:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->br:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    const/16 v1, 0x76

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->br:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 259228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->br:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    return-object v0
.end method

.method public final bx()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259229
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259230
    const/16 v0, 0xe

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259231
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bs:I

    return v0
.end method

.method public final by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259142
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bt:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259143
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bt:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    const/16 v1, 0x78

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bt:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 259144
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bt:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    return-object v0
.end method

.method public final bz()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259111
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bu:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259112
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bu:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const/16 v1, 0x79

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bu:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 259113
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->bu:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 259114
    const v0, 0x46c7fc4

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259115
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 259116
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 259117
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLMedia;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 259118
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 259119
    const/4 v0, 0x0

    .line 259120
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259121
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259122
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->f:Ljava/lang/String;

    .line 259123
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259124
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259125
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 259126
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->g:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259127
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259128
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 259129
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259130
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259131
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259132
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->i:I

    return v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259133
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259134
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 259135
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->j:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 259136
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259137
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->k:Ljava/lang/String;

    .line 259138
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final q()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259139
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259140
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259141
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->l:J

    return-wide v0
.end method

.method public final r()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259108
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259109
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259110
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->m:I

    return v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259145
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->n:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 259146
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->n:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->n:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 259147
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->n:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259148
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259149
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259150
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->o:Z

    return v0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259151
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259152
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259153
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->p:Z

    return v0
.end method

.method public final v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259154
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259155
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259156
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->q:Z

    return v0
.end method

.method public final w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259157
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259158
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259159
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->r:Z

    return v0
.end method

.method public final x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259160
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259161
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259162
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->s:Z

    return v0
.end method

.method public final y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259163
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259164
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259165
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->t:Z

    return v0
.end method

.method public final z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 259166
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 259167
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 259168
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLMedia;->u:Z

    return v0
.end method
