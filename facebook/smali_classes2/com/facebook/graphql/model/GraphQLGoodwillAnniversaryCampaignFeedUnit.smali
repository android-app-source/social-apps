.class public final Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I

.field public n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 202107
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 202108
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 202109
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 202110
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x655ab173

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 202111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->r:LX/0x2;

    .line 202112
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 202113
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->m:I

    .line 202114
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 202115
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 202116
    if-eqz v0, :cond_0

    .line 202117
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 202118
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 202119
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->k:Ljava/lang/String;

    .line 202120
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 202121
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 202122
    if-eqz v0, :cond_0

    .line 202123
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 202124
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 202125
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->l:Ljava/lang/String;

    .line 202126
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 202127
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 202128
    if-eqz v0, :cond_0

    .line 202129
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 202130
    :cond_0
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202131
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202132
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->k:Ljava/lang/String;

    .line 202133
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->l:Ljava/lang/String;

    .line 202155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202134
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202135
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202136
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->m:I

    return v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202137
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202138
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 202139
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202140
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202141
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o:Ljava/lang/String;

    .line 202142
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202143
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202144
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->p:Ljava/lang/String;

    .line 202145
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202147
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->j:Ljava/lang/String;

    .line 202148
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 202149
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->h:Ljava/lang/String;

    .line 202152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 202103
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 202104
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 202105
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->i:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 202106
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 202014
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->r:LX/0x2;

    if-nez v0, :cond_0

    .line 202015
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->r:LX/0x2;

    .line 202016
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->r:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 202017
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 202018
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 202019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 202020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 202021
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->E_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 202022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->C_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 202023
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 202024
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 202025
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 202026
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 202027
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 202028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 202029
    const/16 v3, 0xc

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 202030
    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 202031
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 202032
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 202033
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 202034
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 202035
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 202036
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 202037
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->r()I

    move-result v1

    invoke-virtual {p1, v0, v1, v13}, LX/186;->a(III)V

    .line 202038
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 202039
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 202040
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 202041
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 202042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 202043
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 202044
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 202045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 202047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 202048
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 202049
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 202050
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 202051
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 202052
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 202053
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 202054
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 202055
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 202056
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 202057
    new-instance v0, LX/4WU;

    invoke-direct {v0, p1}, LX/4WU;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 202059
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->i:J

    .line 202060
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 202061
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 202062
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->i:J

    .line 202063
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->m:I

    .line 202064
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 202065
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202066
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 202067
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 202068
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 202069
    :goto_0
    return-void

    .line 202070
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202071
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 202072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 202073
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 202074
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202075
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->r()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 202076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 202077
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 202078
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 202079
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202080
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->a(Ljava/lang/String;)V

    .line 202081
    :cond_0
    :goto_0
    return-void

    .line 202082
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202083
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 202084
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202085
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202086
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202087
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202088
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->q:Ljava/lang/String;

    .line 202089
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 202092
    :goto_0
    return-object v0

    .line 202093
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 202094
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 202095
    const v0, 0x655ab173

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g:Ljava/lang/String;

    .line 202098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202099
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 202100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 202101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 202102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    return-object v0
.end method
