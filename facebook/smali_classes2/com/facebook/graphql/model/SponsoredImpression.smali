.class public Lcom/facebook/graphql/model/SponsoredImpression;
.super Lcom/facebook/graphql/model/BaseImpression;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/model/SponsoredImpression;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Lcom/facebook/graphql/model/SponsoredImpression;


# instance fields
.field private final o:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public q:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public r:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public s:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public t:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public u:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public v:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public w:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public x:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public y:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public z:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206407
    new-instance v0, Lcom/facebook/graphql/model/SponsoredImpression;

    invoke-direct {v0}, Lcom/facebook/graphql/model/SponsoredImpression;-><init>()V

    sput-object v0, Lcom/facebook/graphql/model/SponsoredImpression;->n:Lcom/facebook/graphql/model/SponsoredImpression;

    .line 206408
    new-instance v0, LX/18N;

    invoke-direct {v0}, LX/18N;-><init>()V

    sput-object v0, Lcom/facebook/graphql/model/SponsoredImpression;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 206438
    invoke-direct {p0}, Lcom/facebook/graphql/model/BaseImpression;-><init>()V

    .line 206439
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    .line 206440
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->p:Z

    .line 206441
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->q:Z

    .line 206442
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->u:Z

    .line 206443
    iput v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->t:I

    .line 206444
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->v:Z

    .line 206445
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->w:Z

    .line 206446
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->x:Z

    .line 206447
    iput v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->r:I

    .line 206448
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->s:Z

    .line 206449
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->y:Z

    .line 206450
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->z:Z

    .line 206451
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 206426
    invoke-direct {p0, p1}, Lcom/facebook/graphql/model/BaseImpression;-><init>(Landroid/os/Parcel;)V

    .line 206427
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    .line 206428
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->p:Z

    .line 206429
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->q:Z

    .line 206430
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->t:I

    .line 206431
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->u:Z

    .line 206432
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->w:Z

    .line 206433
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->x:Z

    .line 206434
    iput v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->r:I

    .line 206435
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->s:Z

    .line 206436
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->z:Z

    .line 206437
    return-void
.end method

.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLSponsoredData;)V
    .locals 0

    .prologue
    .line 206423
    invoke-direct {p0}, Lcom/facebook/graphql/model/SponsoredImpression;-><init>()V

    .line 206424
    invoke-direct {p0, p1}, Lcom/facebook/graphql/model/SponsoredImpression;->b(Lcom/facebook/graphql/model/GraphQLSponsoredData;)V

    .line 206425
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 206412
    invoke-direct {p0}, Lcom/facebook/graphql/model/SponsoredImpression;-><init>()V

    .line 206413
    if-eqz p1, :cond_1

    .line 206414
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/SponsoredImpression;->b(Lcom/facebook/graphql/model/GraphQLSponsoredData;)V

    .line 206415
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 206416
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/SponsoredImpression;->b(Lcom/facebook/graphql/model/GraphQLSponsoredData;)V

    .line 206417
    :cond_0
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 206418
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 206419
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 206420
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/SponsoredImpression;->b(Lcom/facebook/graphql/model/GraphQLSponsoredData;)V

    .line 206421
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 206422
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1

    .prologue
    .line 206409
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206410
    new-instance v0, Lcom/facebook/graphql/model/SponsoredImpression;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/SponsoredImpression;-><init>(Lcom/facebook/graphql/model/GraphQLSponsoredData;)V

    .line 206411
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/model/SponsoredImpression;->n:Lcom/facebook/graphql/model/SponsoredImpression;

    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLSponsoredData;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 206347
    if-eqz p1, :cond_1

    invoke-static {p1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206348
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    .line 206349
    if-nez v0, :cond_4

    .line 206350
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->p:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->O_()Z

    move-result v3

    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->p:Z

    .line 206351
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->q:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->c()Z

    move-result v3

    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->q:Z

    .line 206352
    iget v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->t:I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->e()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->t:I

    .line 206353
    iget v0, p0, Lcom/facebook/graphql/model/BaseImpression;->j:I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->l()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->j:I

    .line 206354
    iget v0, p0, Lcom/facebook/graphql/model/BaseImpression;->k:I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->k()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->k:I

    .line 206355
    iget-boolean v3, p0, Lcom/facebook/graphql/model/SponsoredImpression;->u:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->d()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->u:Z

    .line 206356
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->v:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->N_()Z

    move-result v3

    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->v:Z

    .line 206357
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->x:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->j()Z

    move-result v3

    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->x:Z

    .line 206358
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->y:Z

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->g()Z

    move-result v3

    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->y:Z

    .line 206359
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->a()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->z:Z

    .line 206360
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 206361
    goto :goto_1

    :cond_3
    move v1, v2

    .line 206362
    goto :goto_2

    .line 206363
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 206364
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/3EA;LX/162;JILjava/lang/Integer;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EA;",
            "LX/162;",
            "JI",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206387
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 206388
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 206389
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 206390
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 206391
    sget-object v1, LX/3EA;->SUBSEQUENT:LX/3EA;

    if-ne p1, v1, :cond_1

    const-string v1, "IS_ORIGINAL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206392
    :cond_1
    sget-object v1, LX/3EA;->VIEWABILITY:LX/3EA;

    if-ne p1, v1, :cond_2

    const-string v1, "IS_VIEWABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206393
    :cond_2
    sget-object v1, LX/3EA;->ORIGINAL:LX/3EA;

    if-ne p1, v1, :cond_4

    .line 206394
    iget-boolean v1, p0, Lcom/facebook/graphql/model/SponsoredImpression;->s:Z

    move v1, v1

    .line 206395
    if-eqz v1, :cond_4

    const-string v1, "1"

    .line 206396
    :goto_1
    const-string v5, "IS_ORIGINAL"

    sget-object v2, LX/3EA;->ORIGINAL:LX/3EA;

    if-ne p1, v2, :cond_5

    const-string v2, "1"

    :goto_2
    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "IS_VIEWABLE"

    sget-object v0, LX/3EA;->VIEWABILITY:LX/3EA;

    if-ne p1, v0, :cond_6

    const-string v0, "1"

    :goto_3
    invoke-virtual {v2, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "TRACKING"

    .line 206397
    if-eqz p2, :cond_8

    .line 206398
    :try_start_0
    invoke-virtual {p2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "utf-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 206399
    :goto_4
    move-object v5, v5

    .line 206400
    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "CLIENT_TIMESTAMP"

    const-wide/16 v6, 0x3e8

    div-long v6, p3, v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "CLIENT_STORY_POSITION"

    iget v5, p0, Lcom/facebook/graphql/model/SponsoredImpression;->r:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "SEQUENCE_ID"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "FROM_AUTOSCROLL"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 206401
    if-eqz p6, :cond_3

    .line 206402
    const-string v1, "IMAGE_LOAD_STATE"

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 206403
    :cond_3
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 206404
    :cond_4
    const-string v1, "0"

    goto :goto_1

    .line 206405
    :cond_5
    const-string v2, "0"

    goto :goto_2

    :cond_6
    const-string v0, "0"

    goto :goto_3

    .line 206406
    :cond_7
    return-object v3

    :catch_0
    :cond_8
    const-string v5, ""

    goto :goto_4
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 206452
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 206382
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 206383
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "IS_ORIGINAL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206384
    const/4 v2, 0x1

    .line 206385
    :cond_0
    return v2

    .line 206386
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 206377
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 206378
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "IS_VIEWABLE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206379
    const/4 v2, 0x1

    .line 206380
    :cond_0
    return v2

    .line 206381
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 206376
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 206375
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 206374
    const/4 v0, 0x3

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 206373
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 206365
    invoke-super {p0, p1, p2}, Lcom/facebook/graphql/model/BaseImpression;->writeToParcel(Landroid/os/Parcel;I)V

    .line 206366
    iget-object v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->o:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 206367
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 206368
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->q:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 206369
    iget v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->t:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 206370
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->u:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 206371
    iget-boolean v0, p0, Lcom/facebook/graphql/model/SponsoredImpression;->z:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 206372
    return-void
.end method
