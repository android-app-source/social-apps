.class public final Lcom/facebook/graphql/model/GraphQLPhotoEncoding;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoEncoding$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoEncoding$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoTile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 303549
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 303548
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 303546
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 303547
    return-void
.end method

.method public constructor <init>(LX/4Xz;)V
    .locals 1

    .prologue
    .line 303537
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 303538
    iget-object v0, p1, LX/4Xz;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->e:Ljava/lang/String;

    .line 303539
    iget-object v0, p1, LX/4Xz;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->f:Ljava/lang/String;

    .line 303540
    iget-object v0, p1, LX/4Xz;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->g:Ljava/lang/String;

    .line 303541
    iget-object v0, p1, LX/4Xz;->e:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->h:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    .line 303542
    iget-object v0, p1, LX/4Xz;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k:Ljava/util/List;

    .line 303543
    iget-object v0, p1, LX/4Xz;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->i:Ljava/lang/String;

    .line 303544
    iget v0, p1, LX/4Xz;->h:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->j:I

    .line 303545
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303534
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303535
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->i:Ljava/lang/String;

    .line 303536
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 303517
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 303518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 303519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 303520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 303521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->m()Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 303522
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 303523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->o()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 303524
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 303525
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 303526
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 303527
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 303528
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 303529
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 303530
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->n()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 303531
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 303532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 303533
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 303504
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 303505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->m()Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 303506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->m()Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    .line 303507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->m()Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 303508
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    .line 303509
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->h:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    .line 303510
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 303511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 303512
    if-eqz v2, :cond_1

    .line 303513
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    .line 303514
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k:Ljava/util/List;

    move-object v1, v0

    .line 303515
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 303516
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 303500
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 303501
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->j:I

    .line 303502
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 303481
    const v0, -0x62ec527b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303497
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303498
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->e:Ljava/lang/String;

    .line 303499
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303494
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303495
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->f:Ljava/lang/String;

    .line 303496
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303491
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303492
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->g:Ljava/lang/String;

    .line 303493
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303488
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->h:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303489
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->h:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->h:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    .line 303490
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->h:Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    return-object v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303485
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303486
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303487
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->j:I

    return v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoTile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303482
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303483
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoTile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k:Ljava/util/List;

    .line 303484
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
