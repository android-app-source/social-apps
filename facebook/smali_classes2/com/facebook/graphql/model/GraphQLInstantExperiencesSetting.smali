.class public final Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324477
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324476
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 324474
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 324475
    return-void
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324471
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324472
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->e:Ljava/lang/String;

    .line 324473
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->e:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324422
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->g:Ljava/lang/String;

    .line 324423
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->g:Ljava/lang/String;

    return-object v0
.end method

.method private q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324468
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 324469
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 324470
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->i:Z

    return v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324465
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324466
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->j:Ljava/lang/String;

    .line 324467
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 324444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324445
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 324446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 324447
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 324448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 324449
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 324450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 324451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 324452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 324453
    const/16 v8, 0xa

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 324454
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 324455
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 324456
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 324457
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 324458
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->q()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 324459
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 324460
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 324461
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 324462
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 324463
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324464
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 324478
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324479
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 324481
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 324482
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    .line 324483
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->h:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 324484
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324485
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324443
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 324440
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 324441
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->i:Z

    .line 324442
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 324439
    const v0, -0x68a4f698

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324436
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324437
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->f:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->f:Ljava/util/List;

    .line 324438
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324433
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->h:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324434
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->h:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->h:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 324435
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->h:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324430
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324431
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k:Ljava/lang/String;

    .line 324432
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324427
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324428
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->l:Ljava/lang/String;

    .line 324429
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324424
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324425
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->m:Ljava/lang/String;

    .line 324426
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;->m:Ljava/lang/String;

    return-object v0
.end method
