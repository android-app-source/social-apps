.class public final Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLDate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323096
    const-class v0, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323095
    const-class v0, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 323075
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323076
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 323089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->a()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 323091
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 323092
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 323093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323094
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 323081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->a()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->a()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDate;

    .line 323084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->a()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 323085
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;

    .line 323086
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->e:Lcom/facebook/graphql/model/GraphQLDate;

    .line 323087
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323088
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLDate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323078
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->e:Lcom/facebook/graphql/model/GraphQLDate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323079
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->e:Lcom/facebook/graphql/model/GraphQLDate;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLDate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->e:Lcom/facebook/graphql/model/GraphQLDate;

    .line 323080
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;->e:Lcom/facebook/graphql/model/GraphQLDate;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 323077
    const v0, -0x48185087

    return v0
.end method
