.class public final Lcom/facebook/graphql/model/GraphQLVect2;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/1f8;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVect2$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVect2$Serializer;
.end annotation


# instance fields
.field public e:D

.field public f:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 291018
    const-class v0, Lcom/facebook/graphql/model/GraphQLVect2$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 291017
    const-class v0, Lcom/facebook/graphql/model/GraphQLVect2$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 290991
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 290992
    return-void
.end method

.method public constructor <init>(LX/4ZN;)V
    .locals 2

    .prologue
    .line 291013
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 291014
    iget-wide v0, p1, LX/4ZN;->b:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVect2;->e:D

    .line 291015
    iget-wide v0, p1, LX/4ZN;->c:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVect2;->f:D

    .line 291016
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 291010
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 291011
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 291012
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVect2;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 291004
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 291005
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 291006
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 291007
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 291008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 291009
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 291001
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 291002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 291003
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 290997
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 290998
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVect2;->e:D

    .line 290999
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVect2;->f:D

    .line 291000
    return-void
.end method

.method public final b()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290994
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290995
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290996
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVect2;->f:D

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 290993
    const v0, 0x4eb50b2

    return v0
.end method
