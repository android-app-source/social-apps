.class public final Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/1jz;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301083
    const-class v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301086
    const-class v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 301084
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301085
    return-void
.end method

.method public constructor <init>(LX/4Vo;)V
    .locals 1

    .prologue
    .line 301077
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301078
    iget v0, p1, LX/4Vo;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->e:I

    .line 301079
    iget v0, p1, LX/4Vo;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->f:I

    .line 301080
    iget v0, p1, LX/4Vo;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->g:I

    .line 301081
    iget-object v0, p1, LX/4Vo;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->h:Ljava/util/List;

    .line 301082
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 301074
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301075
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301076
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 301087
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 301089
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 301090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->a()I

    move-result v1

    invoke-virtual {p1, v3, v1, v3}, LX/186;->a(III)V

    .line 301091
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->b()I

    move-result v2

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 301092
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v2

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 301093
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 301094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301095
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 301066
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 301068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 301069
    if-eqz v1, :cond_0

    .line 301070
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    .line 301071
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->h:Ljava/util/List;

    .line 301072
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301073
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 301061
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 301062
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->e:I

    .line 301063
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->f:I

    .line 301064
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->g:I

    .line 301065
    return-void
.end method

.method public final b()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301058
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301059
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301060
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->f:I

    return v0
.end method

.method public final c()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301051
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301052
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301053
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->g:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 301057
    const v0, 0x5d308c64

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301054
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301055
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->h:Ljava/util/List;

    .line 301056
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
