.class public final Lcom/facebook/graphql/model/GraphQLStoryAttachment;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/16g;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryAttachment$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryAttachment$Serializer;
.end annotation


# instance fields
.field private A:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAttachmentProperty;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLMedia;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 189369
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 189367
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 189372
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 189373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A:LX/0x2;

    .line 189374
    return-void
.end method

.method public constructor <init>(LX/39x;)V
    .locals 1

    .prologue
    .line 189375
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 189376
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A:LX/0x2;

    .line 189377
    iget-object v0, p1, LX/39x;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->e:Ljava/util/List;

    .line 189378
    iget-object v0, p1, LX/39x;->c:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->f:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 189379
    iget-object v0, p1, LX/39x;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->g:Ljava/util/List;

    .line 189380
    iget-object v0, p1, LX/39x;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->h:Ljava/lang/String;

    .line 189381
    iget-object v0, p1, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189382
    iget-object v0, p1, LX/39x;->g:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->j:Lcom/facebook/graphql/model/GraphQLNode;

    .line 189383
    iget-boolean v0, p1, LX/39x;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k:Z

    .line 189384
    iget-boolean v0, p1, LX/39x;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l:Z

    .line 189385
    iget-object v0, p1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 189386
    iget-object v0, p1, LX/39x;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n:Ljava/lang/String;

    .line 189387
    iget-object v0, p1, LX/39x;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o:Ljava/lang/String;

    .line 189388
    iget-object v0, p1, LX/39x;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z:Ljava/lang/String;

    .line 189389
    iget-object v0, p1, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189390
    iget-object v0, p1, LX/39x;->o:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q:Ljava/util/List;

    .line 189391
    iget-object v0, p1, LX/39x;->p:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r:Ljava/util/List;

    .line 189392
    iget-object v0, p1, LX/39x;->q:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s:Ljava/util/List;

    .line 189393
    iget-object v0, p1, LX/39x;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t:Ljava/lang/String;

    .line 189394
    iget-object v0, p1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u:Lcom/facebook/graphql/model/GraphQLNode;

    .line 189395
    iget-object v0, p1, LX/39x;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v:Ljava/lang/String;

    .line 189396
    iget-object v0, p1, LX/39x;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189397
    iget-object v0, p1, LX/39x;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w:Ljava/lang/String;

    .line 189398
    iget-object v0, p1, LX/39x;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x:Ljava/lang/String;

    .line 189399
    iget-object v0, p1, LX/39x;->x:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A:LX/0x2;

    .line 189400
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189401
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189402
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v:Ljava/lang/String;

    .line 189403
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189404
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189405
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w:Ljava/lang/String;

    .line 189406
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189407
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189408
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x:Ljava/lang/String;

    .line 189409
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189410
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189411
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189412
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189413
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189414
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z:Ljava/lang/String;

    .line 189415
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 189416
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A:LX/0x2;

    if-nez v0, :cond_0

    .line 189417
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A:LX/0x2;

    .line 189418
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 23

    .prologue
    .line 189419
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 189420
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 189421
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 189422
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 189423
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 189424
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 189425
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 189426
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 189427
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 189428
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 189429
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 189430
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 189431
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->d(Ljava/util/List;)I

    move-result v13

    .line 189432
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v14

    .line 189433
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 189434
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 189435
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 189436
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 189437
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 189438
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 189439
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->E()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 189440
    const/16 v22, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 189441
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 189442
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 189443
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 189444
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 189445
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 189446
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 189447
    const/4 v2, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 189448
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 189449
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 189450
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 189451
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 189452
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 189453
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 189454
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 189455
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 189456
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 189457
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 189458
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 189459
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 189460
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 189461
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 189462
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 189463
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 189464
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189465
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189466
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->e:Ljava/util/List;

    .line 189467
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 189468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 189469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 189470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 189471
    if-eqz v1, :cond_b

    .line 189472
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189473
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->e:Ljava/util/List;

    move-object v1, v0

    .line 189474
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 189476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 189477
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189478
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->f:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 189479
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 189480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 189481
    if-eqz v2, :cond_1

    .line 189482
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189483
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->g:Ljava/util/List;

    move-object v1, v0

    .line 189484
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 189485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 189487
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189488
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189489
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 189490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 189491
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 189492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189493
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->j:Lcom/facebook/graphql/model/GraphQLNode;

    .line 189494
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 189495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 189496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 189497
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189498
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 189499
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 189500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189501
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 189502
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189503
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189504
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 189505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 189506
    if-eqz v2, :cond_6

    .line 189507
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189508
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q:Ljava/util/List;

    move-object v1, v0

    .line 189509
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 189510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 189511
    if-eqz v2, :cond_7

    .line 189512
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189513
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s:Ljava/util/List;

    move-object v1, v0

    .line 189514
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 189515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 189516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 189517
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189518
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u:Lcom/facebook/graphql/model/GraphQLNode;

    .line 189519
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 189520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 189522
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189523
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189524
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 189525
    if-nez v1, :cond_a

    :goto_1
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_1

    :cond_b
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 189526
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 189527
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k:Z

    .line 189528
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l:Z

    .line 189529
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189530
    invoke-static {}, LX/1fz;->c()LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 189531
    const/4 v0, 0x0

    .line 189532
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v1, :cond_1

    .line 189533
    :cond_0
    :goto_0
    move v0, v0

    .line 189534
    return v0

    .line 189535
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 189536
    if-ne p0, p1, :cond_2

    .line 189537
    const/4 v0, 0x1

    goto :goto_0

    .line 189538
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 189539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 189368
    const v0, -0x4b900828

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 189370
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    move v0, v0

    .line 189371
    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189319
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->f:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189320
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->f:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->f:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 189321
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->f:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAttachmentProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189322
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189323
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->g:Ljava/util/List;

    .line 189324
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189325
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189326
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->h:Ljava/lang/String;

    .line 189327
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189328
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189329
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189330
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189331
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->j:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189332
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->j:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->j:Lcom/facebook/graphql/model/GraphQLNode;

    .line 189333
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->j:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 189334
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 189335
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 189336
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k:Z

    return v0
.end method

.method public final q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 189337
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 189338
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 189339
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l:Z

    return v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189340
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189341
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 189342
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m:Lcom/facebook/graphql/model/GraphQLMedia;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189343
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189344
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n:Ljava/lang/String;

    .line 189345
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189346
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189347
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o:Ljava/lang/String;

    .line 189348
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189349
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189350
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 189351
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final v()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189352
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189353
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q:Ljava/util/List;

    .line 189354
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final w()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189355
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189356
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r:Ljava/util/List;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r:Ljava/util/List;

    .line 189357
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final x()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189358
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189359
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s:Ljava/util/List;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s:Ljava/util/List;

    .line 189360
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->s:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189361
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189362
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t:Ljava/lang/String;

    .line 189363
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189364
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189365
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u:Lcom/facebook/graphql/model/GraphQLNode;

    .line 189366
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method
