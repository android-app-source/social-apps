.class public final Lcom/facebook/graphql/model/GraphQLUser;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLUser$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLUser$Serializer;
.end annotation


# instance fields
.field public A:J

.field public B:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLEventsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public M:Lcom/facebook/graphql/enums/GraphQLGender;

.field public N:Z

.field public O:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Z

.field public S:Z

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:Z

.field public X:Z

.field public Y:Z

.field public Z:Z

.field public aA:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/model/GraphQLProfileBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:J

.field public aN:Z

.field public aO:Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:J

.field public aR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public aU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Z

.field public ab:Z

.field public ac:Z

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:Z

.field public ak:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

.field public am:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLContact;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:J

.field public ap:D

.field public aq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:I

.field public as:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bB:D

.field public bC:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bD:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public bE:Lcom/facebook/graphql/model/GraphQLDate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:Z

.field public bG:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:I

.field public bI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bJ:Z

.field public bK:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bL:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bN:Z

.field public bO:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public bP:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

.field public bQ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bR:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bS:Z

.field public bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bU:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bV:Z

.field public bW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bX:I

.field public bY:Z

.field public bZ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ba:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLName;",
            ">;"
        }
    .end annotation
.end field

.field public be:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public bf:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bj:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:I

.field public bl:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bm:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Z

.field public bp:Z

.field public bq:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public br:Z

.field public bs:Z

.field public bt:Z

.field public bu:Z

.field public bv:I

.field public bw:I

.field public bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:D

.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBylineFragment;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:D

.field public y:Lcom/facebook/graphql/model/GraphQLContact;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 223885
    const-class v0, Lcom/facebook/graphql/model/GraphQLUser$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 223886
    const-class v0, Lcom/facebook/graphql/model/GraphQLUser$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 223887
    const/16 v0, 0x9e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 223888
    return-void
.end method

.method public constructor <init>(LX/33O;)V
    .locals 2

    .prologue
    .line 223889
    const/16 v0, 0x9e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 223890
    iget-object v0, p1, LX/33O;->b:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223891
    iget-object v0, p1, LX/33O;->c:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 223892
    iget-object v0, p1, LX/33O;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223893
    iget-object v0, p1, LX/33O;->e:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->h:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 223894
    iget-object v0, p1, LX/33O;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->i:Ljava/lang/String;

    .line 223895
    iget-object v0, p1, LX/33O;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->j:Ljava/lang/String;

    .line 223896
    iget v0, p1, LX/33O;->h:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bH:I

    .line 223897
    iget-object v0, p1, LX/33O;->i:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223898
    iget-object v0, p1, LX/33O;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223899
    iget-object v0, p1, LX/33O;->k:Lcom/facebook/graphql/model/GraphQLDate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bE:Lcom/facebook/graphql/model/GraphQLDate;

    .line 223900
    iget-object v0, p1, LX/33O;->l:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->m:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 223901
    iget-object v0, p1, LX/33O;->m:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 223902
    iget-object v0, p1, LX/33O;->n:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->o:Ljava/util/List;

    .line 223903
    iget-boolean v0, p1, LX/33O;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->p:Z

    .line 223904
    iget-boolean v0, p1, LX/33O;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->q:Z

    .line 223905
    iget-boolean v0, p1, LX/33O;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->r:Z

    .line 223906
    iget-boolean v0, p1, LX/33O;->r:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->s:Z

    .line 223907
    iget-boolean v0, p1, LX/33O;->s:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->t:Z

    .line 223908
    iget-boolean v0, p1, LX/33O;->t:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->u:Z

    .line 223909
    iget-boolean v0, p1, LX/33O;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->v:Z

    .line 223910
    iget-boolean v0, p1, LX/33O;->v:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->w:Z

    .line 223911
    iget-wide v0, p1, LX/33O;->w:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->x:D

    .line 223912
    iget-object v0, p1, LX/33O;->x:Lcom/facebook/graphql/model/GraphQLContact;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->y:Lcom/facebook/graphql/model/GraphQLContact;

    .line 223913
    iget-object v0, p1, LX/33O;->y:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 223914
    iget-wide v0, p1, LX/33O;->z:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->A:J

    .line 223915
    iget v0, p1, LX/33O;->A:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bX:I

    .line 223916
    iget-object v0, p1, LX/33O;->B:Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bG:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 223917
    iget-object v0, p1, LX/33O;->C:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->B:Lcom/facebook/graphql/model/GraphQLPage;

    .line 223918
    iget-object v0, p1, LX/33O;->D:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223919
    iget-boolean v0, p1, LX/33O;->E:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bS:Z

    .line 223920
    iget-object v0, p1, LX/33O;->F:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->C:Ljava/util/List;

    .line 223921
    iget-object v0, p1, LX/33O;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223922
    iget-object v0, p1, LX/33O;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->E:Ljava/lang/String;

    .line 223923
    iget-object v0, p1, LX/33O;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bI:Ljava/lang/String;

    .line 223924
    iget-object v0, p1, LX/33O;->J:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->F:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 223925
    iget-object v0, p1, LX/33O;->K:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223926
    iget-object v0, p1, LX/33O;->L:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->H:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 223927
    iget-object v0, p1, LX/33O;->M:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->I:Ljava/lang/String;

    .line 223928
    iget-object v0, p1, LX/33O;->N:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 223929
    iget-object v0, p1, LX/33O;->O:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->K:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 223930
    iget-object v0, p1, LX/33O;->P:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->L:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 223931
    iget-object v0, p1, LX/33O;->Q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223932
    iget-object v0, p1, LX/33O;->R:Lcom/facebook/graphql/enums/GraphQLGender;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->M:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 223933
    iget-boolean v0, p1, LX/33O;->S:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->N:Z

    .line 223934
    iget-object v0, p1, LX/33O;->T:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->O:Lcom/facebook/graphql/model/GraphQLPage;

    .line 223935
    iget-object v0, p1, LX/33O;->U:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->P:Ljava/lang/String;

    .line 223936
    iget-object v0, p1, LX/33O;->V:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223937
    iget-object v0, p1, LX/33O;->W:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bM:Ljava/lang/String;

    .line 223938
    iget-object v0, p1, LX/33O;->X:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bD:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 223939
    iget-boolean v0, p1, LX/33O;->Y:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->R:Z

    .line 223940
    iget-boolean v0, p1, LX/33O;->Z:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->S:Z

    .line 223941
    iget-boolean v0, p1, LX/33O;->aa:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bN:Z

    .line 223942
    iget-boolean v0, p1, LX/33O;->ab:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->T:Z

    .line 223943
    iget-boolean v0, p1, LX/33O;->ac:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->U:Z

    .line 223944
    iget-boolean v0, p1, LX/33O;->ad:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->V:Z

    .line 223945
    iget-boolean v0, p1, LX/33O;->ae:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->W:Z

    .line 223946
    iget-boolean v0, p1, LX/33O;->af:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->X:Z

    .line 223947
    iget-boolean v0, p1, LX/33O;->ag:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bF:Z

    .line 223948
    iget-boolean v0, p1, LX/33O;->ah:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Y:Z

    .line 223949
    iget-boolean v0, p1, LX/33O;->ai:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Z:Z

    .line 223950
    iget-boolean v0, p1, LX/33O;->aj:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aa:Z

    .line 223951
    iget-boolean v0, p1, LX/33O;->ak:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ab:Z

    .line 223952
    iget-boolean v0, p1, LX/33O;->al:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ac:Z

    .line 223953
    iget-boolean v0, p1, LX/33O;->am:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ad:Z

    .line 223954
    iget-boolean v0, p1, LX/33O;->an:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ae:Z

    .line 223955
    iget-boolean v0, p1, LX/33O;->ao:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->af:Z

    .line 223956
    iget-boolean v0, p1, LX/33O;->ap:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ag:Z

    .line 223957
    iget-boolean v0, p1, LX/33O;->aq:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ah:Z

    .line 223958
    iget-boolean v0, p1, LX/33O;->ar:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ai:Z

    .line 223959
    iget-boolean v0, p1, LX/33O;->as:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aj:Z

    .line 223960
    iget-object v0, p1, LX/33O;->at:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ak:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 223961
    iget-object v0, p1, LX/33O;->au:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->al:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 223962
    iget-boolean v0, p1, LX/33O;->av:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bJ:Z

    .line 223963
    iget-object v0, p1, LX/33O;->aw:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bP:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 223964
    iget-object v0, p1, LX/33O;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223965
    iget-object v0, p1, LX/33O;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223966
    iget-object v0, p1, LX/33O;->az:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bC:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 223967
    iget-object v0, p1, LX/33O;->aA:Lcom/facebook/graphql/model/GraphQLContact;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->an:Lcom/facebook/graphql/model/GraphQLContact;

    .line 223968
    iget-wide v0, p1, LX/33O;->aB:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ao:J

    .line 223969
    iget-wide v0, p1, LX/33O;->aC:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ap:D

    .line 223970
    iget-object v0, p1, LX/33O;->aD:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bK:Lcom/facebook/graphql/model/GraphQLUser;

    .line 223971
    iget-boolean v0, p1, LX/33O;->aE:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bV:Z

    .line 223972
    iget-object v0, p1, LX/33O;->aF:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aq:Ljava/lang/String;

    .line 223973
    iget v0, p1, LX/33O;->aG:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ar:I

    .line 223974
    iget-object v0, p1, LX/33O;->aH:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->as:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 223975
    iget-object v0, p1, LX/33O;->aI:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->at:Ljava/lang/String;

    .line 223976
    iget-object v0, p1, LX/33O;->aJ:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->au:Ljava/util/List;

    .line 223977
    iget-object v0, p1, LX/33O;->aK:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->av:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 223978
    iget-object v0, p1, LX/33O;->aL:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 223979
    iget-object v0, p1, LX/33O;->aM:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223980
    iget-object v0, p1, LX/33O;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223981
    iget-object v0, p1, LX/33O;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223982
    iget-object v0, p1, LX/33O;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223983
    iget-object v0, p1, LX/33O;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223984
    iget-object v0, p1, LX/33O;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223985
    iget-object v0, p1, LX/33O;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223986
    iget-object v0, p1, LX/33O;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223987
    iget-object v0, p1, LX/33O;->aU:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223988
    iget-object v0, p1, LX/33O;->aV:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aG:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 223989
    iget-object v0, p1, LX/33O;->aW:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aH:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 223990
    iget-object v0, p1, LX/33O;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223991
    iget-object v0, p1, LX/33O;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223992
    iget-object v0, p1, LX/33O;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223993
    iget-object v0, p1, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223994
    iget-wide v0, p1, LX/33O;->bb:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aM:J

    .line 223995
    iget-boolean v0, p1, LX/33O;->bc:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aN:Z

    .line 223996
    iget-object v0, p1, LX/33O;->bd:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aO:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 223997
    iget-object v0, p1, LX/33O;->be:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223998
    iget-object v0, p1, LX/33O;->bf:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bL:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 223999
    iget-object v0, p1, LX/33O;->bg:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bW:Ljava/lang/String;

    .line 224000
    iget-object v0, p1, LX/33O;->bh:Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aP:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 224001
    iget-wide v0, p1, LX/33O;->bi:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aQ:J

    .line 224002
    iget-object v0, p1, LX/33O;->bj:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aR:Ljava/lang/String;

    .line 224003
    iget-object v0, p1, LX/33O;->bk:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aS:Ljava/lang/String;

    .line 224004
    iget-object v0, p1, LX/33O;->bl:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aT:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 224005
    iget-object v0, p1, LX/33O;->bm:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aU:Ljava/lang/String;

    .line 224006
    iget-object v0, p1, LX/33O;->bn:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aV:Ljava/lang/String;

    .line 224007
    iget-object v0, p1, LX/33O;->bo:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aW:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 224008
    iget-object v0, p1, LX/33O;->bp:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224009
    iget-object v0, p1, LX/33O;->bq:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224010
    iget-object v0, p1, LX/33O;->br:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224011
    iget-object v0, p1, LX/33O;->bs:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224012
    iget-object v0, p1, LX/33O;->bt:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bb:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 224013
    iget-object v0, p1, LX/33O;->bu:Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bc:Lcom/facebook/graphql/model/GraphQLName;

    .line 224014
    iget-object v0, p1, LX/33O;->bv:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bd:Ljava/util/List;

    .line 224015
    iget-object v0, p1, LX/33O;->bw:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->be:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 224016
    iget-object v0, p1, LX/33O;->bx:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224017
    iget-object v0, p1, LX/33O;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224018
    iget-object v0, p1, LX/33O;->bz:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bh:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 224019
    iget-object v0, p1, LX/33O;->bA:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bi:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    .line 224020
    iget-object v0, p1, LX/33O;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bj:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224021
    iget v0, p1, LX/33O;->bC:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bk:I

    .line 224022
    iget-object v0, p1, LX/33O;->bD:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bl:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 224023
    iget-object v0, p1, LX/33O;->bE:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bm:Ljava/lang/String;

    .line 224024
    iget-object v0, p1, LX/33O;->bF:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bn:Ljava/lang/String;

    .line 224025
    iget-boolean v0, p1, LX/33O;->bG:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bo:Z

    .line 224026
    iget-boolean v0, p1, LX/33O;->bH:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bp:Z

    .line 224027
    iget-object v0, p1, LX/33O;->bI:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bU:Lcom/facebook/graphql/model/GraphQLNode;

    .line 224028
    iget-object v0, p1, LX/33O;->bJ:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bq:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 224029
    iget-boolean v0, p1, LX/33O;->bK:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->br:Z

    .line 224030
    iget-boolean v0, p1, LX/33O;->bL:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bs:Z

    .line 224031
    iget-boolean v0, p1, LX/33O;->bM:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bt:Z

    .line 224032
    iget-boolean v0, p1, LX/33O;->bN:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bu:Z

    .line 224033
    iget v0, p1, LX/33O;->bO:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bv:I

    .line 224034
    iget v0, p1, LX/33O;->bP:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bw:I

    .line 224035
    iget-object v0, p1, LX/33O;->bQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224036
    iget-object v0, p1, LX/33O;->bR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224037
    iget-wide v0, p1, LX/33O;->bS:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bz:D

    .line 224038
    iget-boolean v0, p1, LX/33O;->bT:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bY:Z

    .line 224039
    iget-object v0, p1, LX/33O;->bU:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bO:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 224040
    iget-object v0, p1, LX/33O;->bV:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bA:Ljava/util/List;

    .line 224041
    iget-wide v0, p1, LX/33O;->bW:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bB:D

    .line 224042
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    .line 224043
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->L:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 224044
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 224045
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 224046
    if-eqz v0, :cond_0

    .line 224047
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 224048
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 3

    .prologue
    .line 224049
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->bO:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 224050
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 224051
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 224052
    if-eqz v0, :cond_0

    .line 224053
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x91

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 224054
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 3

    .prologue
    .line 224055
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->aT:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 224056
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 224057
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 224058
    if-eqz v0, :cond_0

    .line 224059
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x60

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 224060
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 3

    .prologue
    .line 224061
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->be:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 224062
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 224063
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 224064
    if-eqz v0, :cond_0

    .line 224065
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x6b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 224066
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 224067
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->bn:Ljava/lang/String;

    .line 224068
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 224069
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 224070
    if-eqz v0, :cond_0

    .line 224071
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x74

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 224072
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 224073
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->s:Z

    .line 224074
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 224075
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 224076
    if-eqz v0, :cond_0

    .line 224077
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 224078
    :cond_0
    return-void
.end method

.method private aA()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223882
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223883
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223884
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->A:J

    return-wide v0
.end method

.method private aB()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224082
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->B:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224083
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->B:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->B:Lcom/facebook/graphql/model/GraphQLPage;

    .line 224084
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->B:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private aC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224085
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224086
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224087
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aD()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224088
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224089
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->E:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->E:Ljava/lang/String;

    .line 224090
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->E:Ljava/lang/String;

    return-object v0
.end method

.method private aE()Lcom/facebook/graphql/model/GraphQLEventsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224091
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->F:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224092
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->F:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->F:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 224093
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->F:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    return-object v0
.end method

.method private aF()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224094
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224095
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->I:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->I:Ljava/lang/String;

    .line 224096
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->I:Ljava/lang/String;

    return-object v0
.end method

.method private aG()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224097
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224098
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 224099
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    return-object v0
.end method

.method private aH()Lcom/facebook/graphql/enums/GraphQLGender;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->M:Lcom/facebook/graphql/enums/GraphQLGender;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->M:Lcom/facebook/graphql/enums/GraphQLGender;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGender;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGender;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->M:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 224102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->M:Lcom/facebook/graphql/enums/GraphQLGender;

    return-object v0
.end method

.method private aI()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224103
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224104
    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224105
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->N:Z

    return v0
.end method

.method private aJ()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224106
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->O:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224107
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->O:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->O:Lcom/facebook/graphql/model/GraphQLPage;

    .line 224108
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->O:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private aK()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224109
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224110
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224111
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aL()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223849
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223850
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223851
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->R:Z

    return v0
.end method

.method private aM()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223816
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223817
    const/4 v0, 0x5

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223818
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->T:Z

    return v0
.end method

.method private aN()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223819
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223820
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223821
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->U:Z

    return v0
.end method

.method private aO()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 223822
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223823
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223824
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->V:Z

    return v0
.end method

.method private aP()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223825
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223826
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223827
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->W:Z

    return v0
.end method

.method private aQ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223828
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223829
    const/4 v0, 0x5

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223830
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->X:Z

    return v0
.end method

.method private aR()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223831
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223832
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223833
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Z:Z

    return v0
.end method

.method private aS()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223834
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223835
    const/4 v0, 0x6

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223836
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aa:Z

    return v0
.end method

.method private aT()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223837
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223838
    const/4 v0, 0x6

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223839
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ab:Z

    return v0
.end method

.method private aU()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223840
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223841
    const/4 v0, 0x6

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223842
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ad:Z

    return v0
.end method

.method private aV()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223843
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223844
    const/4 v0, 0x7

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223845
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ai:Z

    return v0
.end method

.method private aW()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223846
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ak:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223847
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ak:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ak:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 223848
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ak:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    return-object v0
.end method

.method private aX()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223813
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->al:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223814
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->al:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->al:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 223815
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->al:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    return-object v0
.end method

.method private aY()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->am:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->am:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->am:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aZ()Lcom/facebook/graphql/model/GraphQLContact;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->an:Lcom/facebook/graphql/model/GraphQLContact;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->an:Lcom/facebook/graphql/model/GraphQLContact;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLContact;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLContact;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->an:Lcom/facebook/graphql/model/GraphQLContact;

    .line 223857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->an:Lcom/facebook/graphql/model/GraphQLContact;

    return-object v0
.end method

.method private al()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223858
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223859
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->e:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223860
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->e:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private am()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223861
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223862
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223863
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private an()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223864
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->h:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223865
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->h:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->h:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 223866
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->h:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    return-object v0
.end method

.method private ao()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223867
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223868
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->i:Ljava/lang/String;

    .line 223869
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->i:Ljava/lang/String;

    return-object v0
.end method

.method private ap()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223870
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223871
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->j:Ljava/lang/String;

    .line 223872
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->j:Ljava/lang/String;

    return-object v0
.end method

.method private aq()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223873
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223874
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223875
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ar()Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223876
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->m:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223877
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->m:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->m:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 223878
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->m:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    return-object v0
.end method

.method private as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223879
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223880
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 223881
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private at()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224178
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224179
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224180
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->p:Z

    return v0
.end method

.method private au()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224184
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224185
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224186
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->q:Z

    return v0
.end method

.method private av()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224187
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224188
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224189
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->r:Z

    return v0
.end method

.method private aw()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224190
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224191
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224192
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->t:Z

    return v0
.end method

.method private ax()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 224193
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224194
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224195
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->v:Z

    return v0
.end method

.method private ay()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224196
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224197
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224198
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->w:Z

    return v0
.end method

.method private az()Lcom/facebook/graphql/model/GraphQLContact;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224199
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->y:Lcom/facebook/graphql/model/GraphQLContact;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224200
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->y:Lcom/facebook/graphql/model/GraphQLContact;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLContact;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLContact;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->y:Lcom/facebook/graphql/model/GraphQLContact;

    .line 224201
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->y:Lcom/facebook/graphql/model/GraphQLContact;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 224202
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->t:Z

    .line 224203
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 224204
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 224205
    if-eqz v0, :cond_0

    .line 224206
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 224207
    :cond_0
    return-void
.end method

.method private bA()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224208
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224209
    const/16 v0, 0xe

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224210
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bo:Z

    return v0
.end method

.method private bB()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224211
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224212
    const/16 v0, 0xe

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224213
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bp:Z

    return v0
.end method

.method private bC()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224214
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bq:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224215
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bq:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x77

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bq:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 224216
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bq:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private bD()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224217
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224218
    const/16 v0, 0xf

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224219
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->br:Z

    return v0
.end method

.method private bE()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224223
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224224
    const/16 v0, 0xf

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224225
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bs:Z

    return v0
.end method

.method private bF()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224247
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224248
    const/16 v0, 0xf

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224249
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bt:Z

    return v0
.end method

.method private bG()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224244
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224245
    const/16 v0, 0xf

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224246
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bu:Z

    return v0
.end method

.method private bH()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224241
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224242
    const/16 v0, 0xf

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224243
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bv:I

    return v0
.end method

.method private bI()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224238
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224239
    const/16 v0, 0xf

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224240
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bw:I

    return v0
.end method

.method private bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224235
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224236
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x7e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224237
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224232
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224233
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x7f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224234
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bL()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224229
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224230
    const/16 v0, 0x10

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224231
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bB:D

    return-wide v0
.end method

.method private bM()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bC:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bC:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    const/16 v1, 0x83

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bC:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 224228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bC:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    return-object v0
.end method

.method private bN()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224181
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bD:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224182
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bD:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    const/16 v1, 0x84

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bD:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 224183
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bD:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    return-object v0
.end method

.method private bO()Lcom/facebook/graphql/model/GraphQLDate;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224220
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bE:Lcom/facebook/graphql/model/GraphQLDate;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224221
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bE:Lcom/facebook/graphql/model/GraphQLDate;

    const/16 v1, 0x85

    const-class v2, Lcom/facebook/graphql/model/GraphQLDate;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDate;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bE:Lcom/facebook/graphql/model/GraphQLDate;

    .line 224222
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bE:Lcom/facebook/graphql/model/GraphQLDate;

    return-object v0
.end method

.method private bP()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224115
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224116
    const/16 v0, 0x10

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224117
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bF:Z

    return v0
.end method

.method private bQ()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224118
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bG:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224119
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bG:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x87

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bG:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 224120
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bG:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method private bR()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224121
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224122
    const/16 v0, 0x11

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224123
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bH:I

    return v0
.end method

.method private bS()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224124
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224125
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bI:Ljava/lang/String;

    const/16 v1, 0x89

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bI:Ljava/lang/String;

    .line 224126
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bI:Ljava/lang/String;

    return-object v0
.end method

.method private bT()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224127
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bK:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224128
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bK:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0x8b

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bK:Lcom/facebook/graphql/model/GraphQLUser;

    .line 224129
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bK:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method private bU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224130
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bL:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224131
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bL:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    const/16 v1, 0x8c

    const-class v2, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bL:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 224132
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bL:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    return-object v0
.end method

.method private bV()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224133
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bM:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224134
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bM:Ljava/lang/String;

    const/16 v1, 0x8f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bM:Ljava/lang/String;

    .line 224135
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bM:Ljava/lang/String;

    return-object v0
.end method

.method private bW()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224136
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bR:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224137
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bR:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x94

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224138
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bR:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bX()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224139
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224140
    const/16 v0, 0x12

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224141
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bS:Z

    return v0
.end method

.method private bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224142
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224143
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x96

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 224144
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bZ()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224148
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bU:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224149
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bU:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x97

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bU:Lcom/facebook/graphql/model/GraphQLNode;

    .line 224150
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bU:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private ba()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224145
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224146
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224147
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ao:J

    return-wide v0
.end method

.method private bb()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224112
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224113
    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224114
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ap:D

    return-wide v0
.end method

.method private bc()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224151
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aq:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224152
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aq:Ljava/lang/String;

    const/16 v1, 0x42

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aq:Ljava/lang/String;

    .line 224153
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aq:Ljava/lang/String;

    return-object v0
.end method

.method private bd()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224154
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224155
    const/16 v0, 0x8

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224156
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ar:I

    return v0
.end method

.method private be()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224157
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->av:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224158
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->av:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    const/16 v1, 0x47

    const-class v2, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->av:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 224159
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->av:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    return-object v0
.end method

.method private bf()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224160
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224161
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x49

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224162
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bg()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224163
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224164
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224165
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bh()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224166
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224167
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224168
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bi()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224169
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224170
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224171
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bj()Lcom/facebook/graphql/model/GraphQLProfileBadge;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224172
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aG:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224173
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aG:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfileBadge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileBadge;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aG:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 224174
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aG:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    return-object v0
.end method

.method private bk()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224079
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224080
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224081
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bl()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224175
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224176
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x55

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 224177
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bm()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222868
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222869
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x56

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222870
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bn()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222874
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222875
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222876
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aM:J

    return-wide v0
.end method

.method private bo()Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222871
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aO:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222872
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aO:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfileVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aO:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 222873
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aO:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    return-object v0
.end method

.method private bp()Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222877
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aP:Lcom/facebook/graphql/model/GraphQLEvent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222878
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aP:Lcom/facebook/graphql/model/GraphQLEvent;

    const/16 v1, 0x5c

    const-class v2, Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aP:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 222879
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aP:Lcom/facebook/graphql/model/GraphQLEvent;

    return-object v0
.end method

.method private bq()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222880
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222881
    const/16 v0, 0xb

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222882
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aQ:J

    return-wide v0
.end method

.method private br()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222883
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aR:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222884
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aR:Ljava/lang/String;

    const/16 v1, 0x5e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aR:Ljava/lang/String;

    .line 222885
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aR:Ljava/lang/String;

    return-object v0
.end method

.method private bs()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222886
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aS:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222887
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aS:Ljava/lang/String;

    const/16 v1, 0x5f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aS:Ljava/lang/String;

    .line 222888
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aS:Ljava/lang/String;

    return-object v0
.end method

.method private bt()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222889
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aU:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222890
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aU:Ljava/lang/String;

    const/16 v1, 0x61

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aU:Ljava/lang/String;

    .line 222891
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aU:Ljava/lang/String;

    return-object v0
.end method

.method private bu()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222892
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aW:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222893
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aW:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    const/16 v1, 0x63

    const-class v2, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aW:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 222894
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aW:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    return-object v0
.end method

.method private bv()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222895
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222896
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x65

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222897
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bw()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222865
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222866
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x66

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222867
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bx()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222904
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222905
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x67

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222906
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private by()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222856
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bd:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222857
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bd:Ljava/util/List;

    const/16 v1, 0x6a

    const-class v2, Lcom/facebook/graphql/model/GraphQLName;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bd:Ljava/util/List;

    .line 222858
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bd:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bz()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222907
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bj:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222908
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bj:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x70

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bj:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222909
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bj:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 222910
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->u:Z

    .line 222911
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222912
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222913
    if-eqz v0, :cond_0

    .line 222914
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222915
    :cond_0
    return-void
.end method

.method private ca()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222916
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222917
    const/16 v0, 0x13

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222918
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bV:Z

    return v0
.end method

.method private cb()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222919
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bW:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222920
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bW:Ljava/lang/String;

    const/16 v1, 0x99

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bW:Ljava/lang/String;

    .line 222921
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bW:Ljava/lang/String;

    return-object v0
.end method

.method private cc()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222922
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222923
    const/16 v0, 0x13

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222924
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bX:I

    return v0
.end method

.method private cd()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222925
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222926
    const/16 v0, 0x13

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222927
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bY:Z

    return v0
.end method

.method private ce()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bZ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bZ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bZ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 222931
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->X:Z

    .line 222932
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222933
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222934
    if-eqz v0, :cond_0

    .line 222935
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2f

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222936
    :cond_0
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 222937
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->ad:Z

    .line 222938
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222939
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222940
    if-eqz v0, :cond_0

    .line 222941
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222942
    :cond_0
    return-void
.end method

.method private f(Z)V
    .locals 3

    .prologue
    .line 222898
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->ae:Z

    .line 222899
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222900
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222901
    if-eqz v0, :cond_0

    .line 222902
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x36

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222903
    :cond_0
    return-void
.end method

.method private g(Z)V
    .locals 3

    .prologue
    .line 222826
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->bs:Z

    .line 222827
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222828
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222829
    if-eqz v0, :cond_0

    .line 222830
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x79

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222831
    :cond_0
    return-void
.end method

.method private h(Z)V
    .locals 3

    .prologue
    .line 222820
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->bt:Z

    .line 222821
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222822
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222823
    if-eqz v0, :cond_0

    .line 222824
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x7a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222825
    :cond_0
    return-void
.end method

.method private i(Z)V
    .locals 3

    .prologue
    .line 222814
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->bJ:Z

    .line 222815
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222816
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222817
    if-eqz v0, :cond_0

    .line 222818
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222819
    :cond_0
    return-void
.end method

.method private j(Z)V
    .locals 3

    .prologue
    .line 222808
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLUser;->bN:Z

    .line 222809
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 222810
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 222811
    if-eqz v0, :cond_0

    .line 222812
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x90

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 222813
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222805
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222806
    const/4 v0, 0x6

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222807
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->af:Z

    return v0
.end method

.method public final B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222802
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222803
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222804
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ag:Z

    return v0
.end method

.method public final C()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222787
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222788
    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222789
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ah:Z

    return v0
.end method

.method public final D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222790
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222791
    const/4 v0, 0x7

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222792
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aj:Z

    return v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222796
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->as:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222797
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->as:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    const/16 v1, 0x44

    const-class v2, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->as:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 222798
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->as:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222793
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->at:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222794
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->at:Ljava/lang/String;

    const/16 v1, 0x45

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->at:Ljava/lang/String;

    .line 222795
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->at:Ljava/lang/String;

    return-object v0
.end method

.method public final G()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222832
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->au:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222833
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->au:Ljava/util/List;

    const/16 v1, 0x46

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->au:Ljava/util/List;

    .line 222834
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->au:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222835
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222836
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 222837
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222838
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222839
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222840
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222841
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->az:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222842
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->az:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222843
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->az:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222844
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222845
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222846
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222847
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222848
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222849
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222850
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222851
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x51

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222852
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222853
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aH:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222854
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aH:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x53

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aH:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 222855
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aH:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222799
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222800
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x57

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222801
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final P()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222859
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222860
    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222861
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aN:Z

    return v0
.end method

.method public final Q()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222862
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aT:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222863
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aT:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aT:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 222864
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aT:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method public final R()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223641
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aV:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223642
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aV:Ljava/lang/String;

    const/16 v1, 0x62

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aV:Ljava/lang/String;

    .line 223643
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aV:Ljava/lang/String;

    return-object v0
.end method

.method public final S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223011
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223012
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x64

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223013
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223014
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bb:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223015
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bb:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x68

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bb:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 223016
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bb:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLName;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223017
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bc:Lcom/facebook/graphql/model/GraphQLName;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223018
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bc:Lcom/facebook/graphql/model/GraphQLName;

    const/16 v1, 0x69

    const-class v2, Lcom/facebook/graphql/model/GraphQLName;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bc:Lcom/facebook/graphql/model/GraphQLName;

    .line 223019
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bc:Lcom/facebook/graphql/model/GraphQLName;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223020
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->be:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223021
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->be:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x6b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->be:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 223022
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->be:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223023
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223024
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x6c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223025
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223026
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223027
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x6d

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223028
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final Y()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223029
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bh:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223030
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bh:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    const/16 v1, 0x6e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bh:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 223031
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bh:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223032
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bi:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223033
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bi:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    const/16 v1, 0x6f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bi:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    .line 223034
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bi:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 92

    .prologue
    .line 223035
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 223036
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 223037
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 223038
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 223039
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->an()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 223040
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ao()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 223041
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ap()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 223042
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 223043
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 223044
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ar()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 223045
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 223046
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->l()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 223047
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->az()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 223048
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->p()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 223049
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aB()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 223050
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->q()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v16

    .line 223051
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 223052
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aD()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 223053
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aE()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 223054
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 223055
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 223056
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aF()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 223057
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aG()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 223058
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->t()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 223059
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aJ()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 223060
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 223061
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 223062
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aW()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 223063
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 223064
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aZ()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 223065
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bc()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 223066
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->E()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 223067
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 223068
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->G()LX/0Px;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v34

    .line 223069
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->be()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 223070
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 223071
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 223072
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 223073
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 223074
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 223075
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 223076
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 223077
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 223078
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bi()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 223079
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 223080
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bj()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 223081
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 223082
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bk()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 223083
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 223084
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bm()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 223085
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 223086
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bo()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 223087
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bp()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 223088
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->br()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v54

    .line 223089
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bs()Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v55

    .line 223090
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bt()Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v56

    .line 223091
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->R()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v57

    .line 223092
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bu()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v58

    .line 223093
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 223094
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 223095
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 223096
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bx()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 223097
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 223098
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->U()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 223099
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->by()LX/0Px;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v65

    .line 223100
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 223101
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->X()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 223102
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->Y()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v68

    .line 223103
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 223104
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v70

    .line 223105
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ab()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v71

    .line 223106
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ac()Ljava/lang/String;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v72

    .line 223107
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ad()Ljava/lang/String;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v73

    .line 223108
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bC()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v74

    .line 223109
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v75

    .line 223110
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 223111
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->af()LX/0Px;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v77

    .line 223112
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bM()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v78

    .line 223113
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bO()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 223114
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bQ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v80

    .line 223115
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bS()Ljava/lang/String;

    move-result-object v81

    move-object/from16 v0, p1

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v81

    .line 223116
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bT()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v82

    move-object/from16 v0, p1

    move-object/from16 v1, v82

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v82

    .line 223117
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v83

    move-object/from16 v0, p1

    move-object/from16 v1, v83

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v83

    .line 223118
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bV()Ljava/lang/String;

    move-result-object v84

    move-object/from16 v0, p1

    move-object/from16 v1, v84

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v84

    .line 223119
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v85

    move-object/from16 v0, p1

    move-object/from16 v1, v85

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v85

    .line 223120
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v86

    move-object/from16 v0, p1

    move-object/from16 v1, v86

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v86

    .line 223121
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v87

    move-object/from16 v0, p1

    move-object/from16 v1, v87

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v87

    .line 223122
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bZ()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v88

    move-object/from16 v0, p1

    move-object/from16 v1, v88

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v88

    .line 223123
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->cb()Ljava/lang/String;

    move-result-object v89

    move-object/from16 v0, p1

    move-object/from16 v1, v89

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v89

    .line 223124
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ce()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v90

    move-object/from16 v0, p1

    move-object/from16 v1, v90

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v90

    .line 223125
    const/16 v91, 0x9d

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 223126
    const/16 v91, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 223127
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 223128
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 223129
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 223130
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 223131
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 223132
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 223133
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 223134
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 223135
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 223136
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 223137
    const/16 v2, 0xc

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->at()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223138
    const/16 v2, 0xd

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->au()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223139
    const/16 v2, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->av()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223140
    const/16 v2, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->m()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223141
    const/16 v2, 0x10

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aw()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223142
    const/16 v2, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->n()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223143
    const/16 v2, 0x12

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ax()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223144
    const/16 v2, 0x13

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ay()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223145
    const/16 v3, 0x14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->o()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 223146
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 223147
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 223148
    const/16 v3, 0x17

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aA()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 223149
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 223150
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223151
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223152
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223153
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223154
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223155
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223156
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223157
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223158
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223159
    const/16 v3, 0x23

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223160
    const/16 v3, 0x24

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aH()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223161
    const/16 v2, 0x25

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aI()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223162
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223163
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223164
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223165
    const/16 v2, 0x29

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aL()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223166
    const/16 v2, 0x2a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223167
    const/16 v2, 0x2b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aM()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223168
    const/16 v2, 0x2c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aN()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223169
    const/16 v2, 0x2d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aO()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223170
    const/16 v2, 0x2e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aP()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223171
    const/16 v2, 0x2f

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aQ()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223172
    const/16 v2, 0x30

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223173
    const/16 v2, 0x31

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aR()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223174
    const/16 v2, 0x32

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aS()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223175
    const/16 v2, 0x33

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aT()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223176
    const/16 v2, 0x34

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223177
    const/16 v2, 0x35

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aU()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223178
    const/16 v2, 0x36

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223179
    const/16 v2, 0x37

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223180
    const/16 v2, 0x38

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->B()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223181
    const/16 v2, 0x39

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->C()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223182
    const/16 v2, 0x3a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aV()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223183
    const/16 v2, 0x3b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->D()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223184
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223185
    const/16 v3, 0x3d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aX()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223186
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223187
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223188
    const/16 v3, 0x40

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ba()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 223189
    const/16 v3, 0x41

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bb()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 223190
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223191
    const/16 v2, 0x43

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bd()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 223192
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223193
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223194
    const/16 v2, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223195
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223196
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223197
    const/16 v2, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223198
    const/16 v2, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223199
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223200
    const/16 v2, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223201
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223202
    const/16 v2, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223203
    const/16 v2, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223204
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223205
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223206
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223207
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223208
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223209
    const/16 v2, 0x55

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223210
    const/16 v2, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223211
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223212
    const/16 v3, 0x58

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bn()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 223213
    const/16 v2, 0x59

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->P()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223214
    const/16 v2, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223215
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223216
    const/16 v3, 0x5d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bq()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 223217
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223218
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223219
    const/16 v3, 0x60

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->Q()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223220
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223221
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223222
    const/16 v2, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223223
    const/16 v2, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223224
    const/16 v2, 0x65

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223225
    const/16 v2, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223226
    const/16 v2, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223227
    const/16 v2, 0x68

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223228
    const/16 v2, 0x69

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223229
    const/16 v2, 0x6a

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223230
    const/16 v3, 0x6b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->V()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223231
    const/16 v2, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223232
    const/16 v2, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223233
    const/16 v2, 0x6e

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223234
    const/16 v2, 0x6f

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223235
    const/16 v2, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223236
    const/16 v2, 0x71

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aa()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 223237
    const/16 v2, 0x72

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223238
    const/16 v2, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223239
    const/16 v2, 0x74

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223240
    const/16 v2, 0x75

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bA()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223241
    const/16 v2, 0x76

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bB()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223242
    const/16 v2, 0x77

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223243
    const/16 v2, 0x78

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bD()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223244
    const/16 v2, 0x79

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bE()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223245
    const/16 v2, 0x7a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bF()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223246
    const/16 v2, 0x7b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bG()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223247
    const/16 v2, 0x7c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bH()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 223248
    const/16 v2, 0x7d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bI()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 223249
    const/16 v2, 0x7e

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223250
    const/16 v2, 0x7f

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223251
    const/16 v3, 0x80

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ae()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 223252
    const/16 v2, 0x81

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223253
    const/16 v3, 0x82

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bL()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 223254
    const/16 v2, 0x83

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223255
    const/16 v3, 0x84

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bN()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223256
    const/16 v2, 0x85

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223257
    const/16 v2, 0x86

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bP()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223258
    const/16 v2, 0x87

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223259
    const/16 v2, 0x88

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bR()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 223260
    const/16 v2, 0x89

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223261
    const/16 v2, 0x8a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ag()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223262
    const/16 v2, 0x8b

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223263
    const/16 v2, 0x8c

    move-object/from16 v0, p1

    move/from16 v1, v83

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223264
    const/16 v2, 0x8f

    move-object/from16 v0, p1

    move/from16 v1, v84

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223265
    const/16 v2, 0x90

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ah()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223266
    const/16 v3, 0x91

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ai()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223267
    const/16 v3, 0x92

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aj()Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 223268
    const/16 v2, 0x93

    move-object/from16 v0, p1

    move/from16 v1, v85

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223269
    const/16 v2, 0x94

    move-object/from16 v0, p1

    move/from16 v1, v86

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223270
    const/16 v2, 0x95

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bX()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223271
    const/16 v2, 0x96

    move-object/from16 v0, p1

    move/from16 v1, v87

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223272
    const/16 v2, 0x97

    move-object/from16 v0, p1

    move/from16 v1, v88

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223273
    const/16 v2, 0x98

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ca()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223274
    const/16 v2, 0x99

    move-object/from16 v0, p1

    move/from16 v1, v89

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223275
    const/16 v2, 0x9a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->cc()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 223276
    const/16 v2, 0x9b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->cd()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 223277
    const/16 v2, 0x9c

    move-object/from16 v0, p1

    move/from16 v1, v90

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 223278
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 223279
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 223280
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    goto/16 :goto_0

    .line 223281
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aH()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v2

    goto/16 :goto_1

    .line 223282
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aX()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v2

    goto/16 :goto_2

    .line 223283
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->Q()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    goto/16 :goto_3

    .line 223284
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->V()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    goto/16 :goto_4

    .line 223285
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->bN()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v2

    goto/16 :goto_5

    .line 223286
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->ai()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto/16 :goto_6

    .line 223287
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLUser;->aj()Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    move-result-object v2

    goto/16 :goto_7
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 223288
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 223289
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223290
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223291
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->al()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 223292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223293
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223294
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 223295
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 223296
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 223297
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223298
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 223299
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 223300
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223301
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 223302
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223303
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223304
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->an()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 223305
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->an()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 223306
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->an()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 223307
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223308
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->h:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 223309
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 223310
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223311
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 223312
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223313
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223314
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 223315
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 223317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223318
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223319
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bO()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 223320
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bO()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDate;

    .line 223321
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bO()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 223322
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223323
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bE:Lcom/facebook/graphql/model/GraphQLDate;

    .line 223324
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ar()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 223325
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ar()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 223326
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ar()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 223327
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223328
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->m:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 223329
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 223330
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 223331
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 223332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223333
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 223334
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 223335
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 223336
    if-eqz v2, :cond_9

    .line 223337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223338
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLUser;->o:Ljava/util/List;

    move-object v1, v0

    .line 223339
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->az()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 223340
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->az()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLContact;

    .line 223341
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->az()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 223342
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223343
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->y:Lcom/facebook/graphql/model/GraphQLContact;

    .line 223344
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->p()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 223345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->p()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 223346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->p()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 223347
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223348
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 223349
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bQ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 223350
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bQ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 223351
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bQ()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 223352
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223353
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bG:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 223354
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aB()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 223355
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aB()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 223356
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aB()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 223357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223358
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->B:Lcom/facebook/graphql/model/GraphQLPage;

    .line 223359
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ce()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 223360
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ce()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223361
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ce()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 223362
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223363
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223364
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 223365
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223366
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 223367
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223368
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223369
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aE()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 223370
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aE()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 223371
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aE()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 223372
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223373
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->F:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 223374
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 223375
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223376
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 223377
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223378
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223379
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 223380
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 223381
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 223382
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223383
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->H:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 223384
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aG()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 223385
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aG()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 223386
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aG()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 223387
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223388
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 223389
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->t()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 223390
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->t()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 223391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->t()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 223392
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223393
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->K:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 223394
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 223395
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223396
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 223397
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223398
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223399
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aJ()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 223400
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aJ()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 223401
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aJ()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 223402
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223403
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->O:Lcom/facebook/graphql/model/GraphQLPage;

    .line 223404
    :cond_16
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 223405
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223406
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 223407
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223408
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223409
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aW()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 223410
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aW()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 223411
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aW()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 223412
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223413
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->ak:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 223414
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 223415
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223416
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 223417
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223418
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223419
    :cond_19
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 223420
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223421
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 223422
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223423
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223424
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bM()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 223425
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bM()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 223426
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bM()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 223427
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223428
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bC:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 223429
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aZ()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 223430
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aZ()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLContact;

    .line 223431
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aZ()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 223432
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223433
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->an:Lcom/facebook/graphql/model/GraphQLContact;

    .line 223434
    :cond_1c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bT()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 223435
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bT()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223436
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bT()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 223437
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223438
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bK:Lcom/facebook/graphql/model/GraphQLUser;

    .line 223439
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->E()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 223440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->E()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 223441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->E()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 223442
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223443
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->as:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 223444
    :cond_1e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->be()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 223445
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->be()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 223446
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->be()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 223447
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223448
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->av:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 223449
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 223450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 223451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->H()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 223452
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223453
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 223454
    :cond_20
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 223455
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223456
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 223457
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223458
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223459
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 223460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223461
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 223462
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223463
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223464
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 223465
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 223467
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223468
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223469
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 223470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 223472
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223473
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223474
    :cond_24
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 223475
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223476
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 223477
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223478
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223479
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 223480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223481
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 223482
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223483
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223484
    :cond_26
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 223485
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223486
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 223487
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223488
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223489
    :cond_27
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bi()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 223490
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bi()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223491
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bi()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 223492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223493
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223494
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 223495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 223497
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223498
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223499
    :cond_29
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bj()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 223500
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bj()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 223501
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bj()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 223502
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223503
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aG:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 223504
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 223505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 223506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 223507
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223508
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aH:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 223509
    :cond_2b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bk()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 223510
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bk()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223511
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bk()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 223512
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223513
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223514
    :cond_2c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 223515
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223516
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 223517
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223518
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223519
    :cond_2d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bm()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 223520
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bm()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223521
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bm()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 223522
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223523
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223524
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 223525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 223527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223528
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223529
    :cond_2f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bo()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 223530
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bo()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 223531
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bo()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 223532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223533
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aO:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 223534
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 223535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 223537
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223538
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223539
    :cond_31
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 223540
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 223541
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 223542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223543
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bL:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 223544
    :cond_32
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bp()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 223545
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bp()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 223546
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bp()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 223547
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223548
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aP:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 223549
    :cond_33
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bu()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 223550
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bu()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 223551
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bu()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_34

    .line 223552
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223553
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aW:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 223554
    :cond_34
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 223555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->S()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_35

    .line 223557
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223558
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223559
    :cond_35
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 223560
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223561
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 223562
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223563
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223564
    :cond_36
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 223565
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223566
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 223567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223568
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->aZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223569
    :cond_37
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bx()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 223570
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bx()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223571
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bx()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_38

    .line 223572
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223573
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223574
    :cond_38
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 223575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 223576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_39

    .line 223577
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223578
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bb:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 223579
    :cond_39
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->U()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 223580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->U()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    .line 223581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->U()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v2

    if-eq v2, v0, :cond_3a

    .line 223582
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223583
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bc:Lcom/facebook/graphql/model/GraphQLName;

    .line 223584
    :cond_3a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->by()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 223585
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->by()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 223586
    if-eqz v2, :cond_3b

    .line 223587
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223588
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLUser;->bd:Ljava/util/List;

    move-object v1, v0

    .line 223589
    :cond_3b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 223590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3c

    .line 223592
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223593
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223594
    :cond_3c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->X()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 223595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->X()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->X()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3d

    .line 223597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223598
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223599
    :cond_3d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Y()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    if-eqz v0, :cond_3e

    .line 223600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Y()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 223601
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Y()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3e

    .line 223602
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223603
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bh:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 223604
    :cond_3e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_3f

    .line 223605
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    .line 223606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3f

    .line 223607
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223608
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bi:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    .line 223609
    :cond_3f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 223610
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 223611
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bz()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_40

    .line 223612
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223613
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bj:Lcom/facebook/graphql/model/GraphQLImage;

    .line 223614
    :cond_40
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ab()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 223615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ab()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 223616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ab()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_41

    .line 223617
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223618
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bl:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 223619
    :cond_41
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bZ()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 223620
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bZ()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 223621
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bZ()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_42

    .line 223622
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223623
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bU:Lcom/facebook/graphql/model/GraphQLNode;

    .line 223624
    :cond_42
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bC()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_43

    .line 223625
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bC()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 223626
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bC()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_43

    .line 223627
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223628
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bq:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 223629
    :cond_43
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_44

    .line 223630
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223631
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bJ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_44

    .line 223632
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223633
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223634
    :cond_44
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_45

    .line 223635
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223636
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_45

    .line 223637
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLUser;

    .line 223638
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLUser;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 223639
    :cond_45
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 223640
    if-nez v1, :cond_46

    :goto_0
    return-object p0

    :cond_46
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 223010
    new-instance v0, LX/4ZM;

    invoke-direct {v0, p1}, LX/4ZM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 223645
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 223646
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->p:Z

    .line 223647
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->q:Z

    .line 223648
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->r:Z

    .line 223649
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->s:Z

    .line 223650
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->t:Z

    .line 223651
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->u:Z

    .line 223652
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->v:Z

    .line 223653
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->w:Z

    .line 223654
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->x:D

    .line 223655
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->A:J

    .line 223656
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->N:Z

    .line 223657
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->R:Z

    .line 223658
    const/16 v0, 0x2a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->S:Z

    .line 223659
    const/16 v0, 0x2b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->T:Z

    .line 223660
    const/16 v0, 0x2c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->U:Z

    .line 223661
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->V:Z

    .line 223662
    const/16 v0, 0x2e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->W:Z

    .line 223663
    const/16 v0, 0x2f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->X:Z

    .line 223664
    const/16 v0, 0x30

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Y:Z

    .line 223665
    const/16 v0, 0x31

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Z:Z

    .line 223666
    const/16 v0, 0x32

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aa:Z

    .line 223667
    const/16 v0, 0x33

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ab:Z

    .line 223668
    const/16 v0, 0x34

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ac:Z

    .line 223669
    const/16 v0, 0x35

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ad:Z

    .line 223670
    const/16 v0, 0x36

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ae:Z

    .line 223671
    const/16 v0, 0x37

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->af:Z

    .line 223672
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ag:Z

    .line 223673
    const/16 v0, 0x39

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ah:Z

    .line 223674
    const/16 v0, 0x3a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ai:Z

    .line 223675
    const/16 v0, 0x3b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aj:Z

    .line 223676
    const/16 v0, 0x40

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ao:J

    .line 223677
    const/16 v0, 0x41

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ap:D

    .line 223678
    const/16 v0, 0x43

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ar:I

    .line 223679
    const/16 v0, 0x58

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aM:J

    .line 223680
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aN:Z

    .line 223681
    const/16 v0, 0x5d

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->aQ:J

    .line 223682
    const/16 v0, 0x71

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bk:I

    .line 223683
    const/16 v0, 0x75

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bo:Z

    .line 223684
    const/16 v0, 0x76

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bp:Z

    .line 223685
    const/16 v0, 0x78

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->br:Z

    .line 223686
    const/16 v0, 0x79

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bs:Z

    .line 223687
    const/16 v0, 0x7a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bt:Z

    .line 223688
    const/16 v0, 0x7b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bu:Z

    .line 223689
    const/16 v0, 0x7c

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bv:I

    .line 223690
    const/16 v0, 0x7d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bw:I

    .line 223691
    const/16 v0, 0x80

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bz:D

    .line 223692
    const/16 v0, 0x82

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bB:D

    .line 223693
    const/16 v0, 0x86

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bF:Z

    .line 223694
    const/16 v0, 0x88

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bH:I

    .line 223695
    const/16 v0, 0x8a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bJ:Z

    .line 223696
    const/16 v0, 0x90

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bN:Z

    .line 223697
    const/16 v0, 0x95

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bS:Z

    .line 223698
    const/16 v0, 0x98

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bV:Z

    .line 223699
    const/16 v0, 0x9a

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bX:I

    .line 223700
    const/16 v0, 0x9b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bY:Z

    .line 223701
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 223702
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223703
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223705
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    .line 223706
    :goto_0
    return-void

    .line 223707
    :cond_0
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223708
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aw()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223710
    const/16 v0, 0x10

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 223711
    :cond_1
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223714
    const/16 v0, 0x11

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 223715
    :cond_2
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 223716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223717
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223718
    const/16 v0, 0x23

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 223719
    :cond_3
    const-string v0, "is_connect_with_facebook_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 223720
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ah()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223722
    const/16 v0, 0x90

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 223723
    :cond_4
    const-string v0, "is_messenger_cymk_hidden"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 223724
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aQ()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223726
    const/16 v0, 0x2f

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223727
    :cond_5
    const-string v0, "is_pymm_hidden"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 223728
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->aU()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223730
    const/16 v0, 0x35

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223731
    :cond_6
    const-string v0, "is_pysf_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 223732
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->z()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223734
    const/16 v0, 0x36

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223735
    :cond_7
    const-string v0, "local_is_pymk_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 223736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ag()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223738
    const/16 v0, 0x8a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223739
    :cond_8
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 223740
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->Q()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223742
    const/16 v0, 0x60

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223743
    :cond_9
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 223744
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->V()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223745
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223746
    const/16 v0, 0x6b

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223747
    :cond_a
    const-string v0, "username"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 223748
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ad()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223749
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223750
    const/16 v0, 0x74

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223751
    :cond_b
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 223752
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bE()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223753
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223754
    const/16 v0, 0x79

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223755
    :cond_c
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 223756
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLUser;->bF()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223757
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223758
    const/16 v0, 0x7a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223759
    :cond_d
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 223760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->ai()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 223761
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 223762
    const/16 v0, 0x91

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 223763
    :cond_e
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 223764
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223765
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->a(Z)V

    .line 223766
    :cond_0
    :goto_0
    return-void

    .line 223767
    :cond_1
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223768
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->b(Z)V

    goto :goto_0

    .line 223769
    :cond_2
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 223770
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->c(Z)V

    goto :goto_0

    .line 223771
    :cond_3
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 223772
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLUser;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 223773
    :cond_4
    const-string v0, "is_connect_with_facebook_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 223774
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->j(Z)V

    goto :goto_0

    .line 223775
    :cond_5
    const-string v0, "is_messenger_cymk_hidden"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 223776
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->d(Z)V

    goto :goto_0

    .line 223777
    :cond_6
    const-string v0, "is_pymm_hidden"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 223778
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->e(Z)V

    goto :goto_0

    .line 223779
    :cond_7
    const-string v0, "is_pysf_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 223780
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->f(Z)V

    goto :goto_0

    .line 223781
    :cond_8
    const-string v0, "local_is_pymk_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 223782
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->i(Z)V

    goto/16 :goto_0

    .line 223783
    :cond_9
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 223784
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLUser;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto/16 :goto_0

    .line 223785
    :cond_a
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 223786
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLUser;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto/16 :goto_0

    .line 223787
    :cond_b
    const-string v0, "username"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 223788
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLUser;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 223789
    :cond_c
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 223790
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->g(Z)V

    goto/16 :goto_0

    .line 223791
    :cond_d
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 223792
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLUser;->h(Z)V

    goto/16 :goto_0

    .line 223793
    :cond_e
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223794
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLUser;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto/16 :goto_0
.end method

.method public final aa()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223795
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223796
    const/16 v0, 0xe

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223797
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bk:I

    return v0
.end method

.method public final ab()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223798
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bl:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223799
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bl:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    const/16 v1, 0x72

    const-class v2, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bl:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 223800
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bl:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    return-object v0
.end method

.method public final ac()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223801
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bm:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223802
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bm:Ljava/lang/String;

    const/16 v1, 0x73

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bm:Ljava/lang/String;

    .line 223803
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bm:Ljava/lang/String;

    return-object v0
.end method

.method public final ad()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223804
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bn:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223805
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bn:Ljava/lang/String;

    const/16 v1, 0x74

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bn:Ljava/lang/String;

    .line 223806
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bn:Ljava/lang/String;

    return-object v0
.end method

.method public final ae()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223807
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223808
    const/16 v0, 0x10

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223809
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bz:D

    return-wide v0
.end method

.method public final af()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223810
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bA:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 223811
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bA:Ljava/util/List;

    const/16 v1, 0x81

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bA:Ljava/util/List;

    .line 223812
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bA:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final ag()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222977
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222978
    const/16 v0, 0x11

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222979
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bJ:Z

    return v0
.end method

.method public final ah()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222946
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222947
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222948
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bN:Z

    return v0
.end method

.method public final ai()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222949
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bO:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222950
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bO:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x91

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bO:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 222951
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bO:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222952
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bP:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222953
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bP:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    const/16 v1, 0x92

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bP:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    .line 222954
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bP:Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    return-object v0
.end method

.method public final ak()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222955
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222956
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x93

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222957
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 222958
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222959
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222960
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreetAddress;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 222961
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->f:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222962
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222963
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 222964
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBylineFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222965
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222966
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->o:Ljava/util/List;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLBylineFragment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->o:Ljava/util/List;

    .line 222967
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222968
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222969
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222970
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->s:Z

    return v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222971
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222972
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222973
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->u:Z

    return v0
.end method

.method public final o()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222974
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222975
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 222976
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->x:D

    return-wide v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222943
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 222945
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->z:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method public final q()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222980
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->C:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222981
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->C:Ljava/util/List;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->C:Ljava/util/List;

    .line 222982
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->C:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222983
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222984
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 222985
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222986
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->H:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222987
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->H:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->H:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 222988
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->H:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222989
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->K:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222990
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->K:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->K:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 222991
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->K:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222992
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->L:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222993
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->L:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->L:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 222994
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->L:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222995
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 222996
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->P:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->P:Ljava/lang/String;

    .line 222997
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 222998
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 222999
    const/4 v0, 0x5

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223000
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->S:Z

    return v0
.end method

.method public final x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223001
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223002
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223003
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->Y:Z

    return v0
.end method

.method public final y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 223004
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223005
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223006
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ac:Z

    return v0
.end method

.method public final z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x6

    .line 223007
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 223008
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 223009
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLUser;->ae:Z

    return v0
.end method
