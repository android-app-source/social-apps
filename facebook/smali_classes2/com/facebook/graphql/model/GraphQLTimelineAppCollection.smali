.class public final Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1oP;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppCollection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppCollection$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation
.end field

.field public s:Z

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319622
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 319621
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 319619
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 319620
    return-void
.end method

.method public constructor <init>(LX/4Z9;)V
    .locals 1

    .prologue
    .line 319599
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 319600
    iget-object v0, p1, LX/4Z9;->b:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319601
    iget-object v0, p1, LX/4Z9;->c:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319602
    iget-object v0, p1, LX/4Z9;->d:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 319603
    iget-object v0, p1, LX/4Z9;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 319604
    iget-object v0, p1, LX/4Z9;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->i:Ljava/lang/String;

    .line 319605
    iget-object v0, p1, LX/4Z9;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j:Ljava/lang/String;

    .line 319606
    iget-object v0, p1, LX/4Z9;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k:Ljava/lang/String;

    .line 319607
    iget-object v0, p1, LX/4Z9;->i:Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 319608
    iget-object v0, p1, LX/4Z9;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m:Ljava/lang/String;

    .line 319609
    iget-object v0, p1, LX/4Z9;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 319610
    iget-object v0, p1, LX/4Z9;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 319611
    iget-object v0, p1, LX/4Z9;->m:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319612
    iget-object v0, p1, LX/4Z9;->n:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->q:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    .line 319613
    iget-object v0, p1, LX/4Z9;->o:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->r:Ljava/util/List;

    .line 319614
    iget-boolean v0, p1, LX/4Z9;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->s:Z

    .line 319615
    iget-object v0, p1, LX/4Z9;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t:Ljava/lang/String;

    .line 319616
    iget-object v0, p1, LX/4Z9;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u:Ljava/lang/String;

    .line 319617
    iget-object v0, p1, LX/4Z9;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->v:Ljava/lang/String;

    .line 319618
    return-void
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319596
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319597
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 319598
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319593
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319594
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 319595
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319590
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319591
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->i:Ljava/lang/String;

    .line 319592
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->i:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319587
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319588
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j:Ljava/lang/String;

    .line 319589
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLMediaSet;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319452
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l:Lcom/facebook/graphql/model/GraphQLMediaSet;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319453
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l:Lcom/facebook/graphql/model/GraphQLMediaSet;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 319454
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l:Lcom/facebook/graphql/model/GraphQLMediaSet;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319584
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319585
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 319586
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319581
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319582
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319583
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    return-object v0
.end method

.method private v()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319578
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319579
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->r:Ljava/util/List;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->r:Ljava/util/List;

    .line 319580
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319575
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 319576
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 319577
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->s:Z

    return v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319572
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319573
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t:Ljava/lang/String;

    .line 319574
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t:Ljava/lang/String;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319569
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319570
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->v:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->v:Ljava/lang/String;

    .line 319571
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->v:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 319510
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319511
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 319512
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 319513
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 319514
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 319515
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->q()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 319516
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->r()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 319517
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 319518
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->s()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 319519
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->c()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 319520
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 319521
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 319522
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 319523
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 319524
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->v()LX/0Px;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->d(Ljava/util/List;)I

    move-result v15

    .line 319525
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->x()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 319526
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 319527
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->y()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 319528
    const/16 v19, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 319529
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 319530
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 319531
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 319532
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 319533
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 319534
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 319535
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 319536
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 319537
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 319538
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 319539
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 319540
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 319541
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 319542
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 319543
    const/16 v2, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 319544
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319545
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319546
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319547
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319548
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 319455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319456
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 319457
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->j()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 319459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319460
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319461
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 319462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 319464
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319465
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319466
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 319467
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 319468
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 319469
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319470
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 319471
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 319472
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 319473
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 319474
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319475
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 319476
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->s()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 319477
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->s()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 319478
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->s()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 319479
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319480
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 319481
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 319482
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 319483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 319484
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319485
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 319486
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 319487
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 319488
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 319489
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319490
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 319491
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 319492
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319493
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 319494
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319495
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->p:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319496
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 319497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    .line 319498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 319499
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 319500
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->q:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    .line 319501
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319502
    if-nez v1, :cond_9

    :goto_0
    return-object p0

    :cond_9
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 319504
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 319505
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->s:Z

    .line 319506
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319507
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319508
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k:Ljava/lang/String;

    .line 319509
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319549
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319550
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m:Ljava/lang/String;

    .line 319551
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()LX/1oQ;
    .locals 1
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 319553
    const v0, -0x57fc1342

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319554
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319555
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319556
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319557
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319558
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    .line 319559
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionMembershipStateInfo;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319560
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319561
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 319562
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319563
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->q:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319564
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->q:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->q:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    .line 319565
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->q:Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319566
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319567
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u:Ljava/lang/String;

    .line 319568
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->u:Ljava/lang/String;

    return-object v0
.end method
