.class public final Lcom/facebook/graphql/model/GraphQLImage;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/0jS;
.implements LX/1Fb;
.implements LX/0jT;
.implements LX/1Fd;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLImage$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLImage$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:D

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field private m:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 224319
    const-class v0, Lcom/facebook/graphql/model/GraphQLImage$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 224318
    const-class v0, Lcom/facebook/graphql/model/GraphQLImage$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 224315
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 224316
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->m:LX/0x2;

    .line 224317
    return-void
.end method

.method public constructor <init>(LX/2dc;)V
    .locals 2

    .prologue
    .line 224303
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 224304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->m:LX/0x2;

    .line 224305
    iget-object v0, p1, LX/2dc;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->j:Ljava/lang/String;

    .line 224306
    iget v0, p1, LX/2dc;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->e:I

    .line 224307
    iget-boolean v0, p1, LX/2dc;->d:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->l:Z

    .line 224308
    iget-object v0, p1, LX/2dc;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->k:Ljava/lang/String;

    .line 224309
    iget-object v0, p1, LX/2dc;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->f:Ljava/lang/String;

    .line 224310
    iget-wide v0, p1, LX/2dc;->g:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->g:D

    .line 224311
    iget-object v0, p1, LX/2dc;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->h:Ljava/lang/String;

    .line 224312
    iget v0, p1, LX/2dc;->i:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->i:I

    .line 224313
    iget-object v0, p1, LX/2dc;->j:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->m:LX/0x2;

    .line 224314
    return-void
.end method


# virtual methods
.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 224300
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->m:LX/0x2;

    if-nez v0, :cond_0

    .line 224301
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->m:LX/0x2;

    .line 224302
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->m:LX/0x2;

    return-object v0
.end method

.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 224297
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224298
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224299
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 224266
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 224267
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 224268
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 224269
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 224270
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 224271
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 224272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-virtual {p1, v9, v1, v9}, LX/186;->a(III)V

    .line 224273
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 224274
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->k()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 224275
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 224276
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 224277
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 224278
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 224279
    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->n()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 224280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 224281
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 224294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 224295
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 224296
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 224288
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 224289
    invoke-virtual {p1, p2, v4, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->e:I

    .line 224290
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->g:D

    .line 224291
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->i:I

    .line 224292
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->l:Z

    .line 224293
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224285
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224286
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->h:Ljava/lang/String;

    .line 224287
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224282
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224283
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224284
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->i:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224263
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224264
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->f:Ljava/lang/String;

    .line 224265
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 224262
    const v0, 0x437b93b

    return v0
.end method

.method public final k()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224259
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224260
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224261
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->g:D

    return-wide v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224256
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224257
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->j:Ljava/lang/String;

    .line 224258
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224253
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 224254
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->k:Ljava/lang/String;

    .line 224255
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 224250
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 224251
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 224252
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLImage;->l:Z

    return v0
.end method
