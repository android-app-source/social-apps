.class public final Lcom/facebook/graphql/model/GraphQLViewer;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLViewer$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLViewer$Serializer;
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:I

.field public J:Z

.field public K:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

.field public N:Z

.field public O:Z

.field public P:Z

.field public Q:I

.field public R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLAudienceInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLCustomizedStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Z

.field public u:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLMegaphone;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLGreetingCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTaggableActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 159156
    const-class v0, Lcom/facebook/graphql/model/GraphQLViewer$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 159106
    const-class v0, Lcom/facebook/graphql/model/GraphQLViewer$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 159107
    const/16 v0, 0x2f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 159108
    return-void
.end method

.method public constructor <init>(LX/2sK;)V
    .locals 1

    .prologue
    .line 159109
    const/16 v0, 0x2f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 159110
    iget-object v0, p1, LX/2sK;->b:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->e:Lcom/facebook/graphql/model/GraphQLUser;

    .line 159111
    iget-object v0, p1, LX/2sK;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 159112
    iget-object v0, p1, LX/2sK;->d:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->g:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    .line 159113
    iget-object v0, p1, LX/2sK;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->T:Ljava/lang/String;

    .line 159114
    iget-boolean v0, p1, LX/2sK;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->O:Z

    .line 159115
    iget-object v0, p1, LX/2sK;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    .line 159116
    iget-object v0, p1, LX/2sK;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->R:Ljava/util/List;

    .line 159117
    iget-object v0, p1, LX/2sK;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->S:Ljava/util/List;

    .line 159118
    iget-object v0, p1, LX/2sK;->j:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->i:Lcom/facebook/graphql/model/GraphQLPage;

    .line 159119
    iget-object v0, p1, LX/2sK;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->j:Ljava/lang/String;

    .line 159120
    iget-object v0, p1, LX/2sK;->l:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->k:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 159121
    iget-object v0, p1, LX/2sK;->m:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->l:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    .line 159122
    iget v0, p1, LX/2sK;->n:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->Q:I

    .line 159123
    iget-object v0, p1, LX/2sK;->o:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->m:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    .line 159124
    iget v0, p1, LX/2sK;->p:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->n:I

    .line 159125
    iget v0, p1, LX/2sK;->q:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->I:I

    .line 159126
    iget-object v0, p1, LX/2sK;->r:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->o:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 159127
    iget-object v0, p1, LX/2sK;->s:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->p:Lcom/facebook/graphql/model/GraphQLPage;

    .line 159128
    iget-boolean v0, p1, LX/2sK;->t:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->q:Z

    .line 159129
    iget-boolean v0, p1, LX/2sK;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->P:Z

    .line 159130
    iget-object v0, p1, LX/2sK;->v:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->r:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    .line 159131
    iget-boolean v0, p1, LX/2sK;->w:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->s:Z

    .line 159132
    iget-boolean v0, p1, LX/2sK;->x:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->J:Z

    .line 159133
    iget-boolean v0, p1, LX/2sK;->y:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->N:Z

    .line 159134
    iget-boolean v0, p1, LX/2sK;->z:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->t:Z

    .line 159135
    iget-object v0, p1, LX/2sK;->A:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->u:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    .line 159136
    iget-object v0, p1, LX/2sK;->B:Lcom/facebook/graphql/model/GraphQLMegaphone;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->v:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 159137
    iget-object v0, p1, LX/2sK;->C:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->M:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    .line 159138
    iget-object v0, p1, LX/2sK;->D:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->w:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 159139
    iget-object v0, p1, LX/2sK;->E:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->x:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 159140
    iget-object v0, p1, LX/2sK;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->y:Ljava/lang/String;

    .line 159141
    iget-object v0, p1, LX/2sK;->G:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->K:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    .line 159142
    iget-object v0, p1, LX/2sK;->H:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->L:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    .line 159143
    iget-object v0, p1, LX/2sK;->I:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->z:Ljava/util/List;

    .line 159144
    iget v0, p1, LX/2sK;->J:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->A:I

    .line 159145
    iget v0, p1, LX/2sK;->K:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->B:I

    .line 159146
    iget v0, p1, LX/2sK;->L:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->C:I

    .line 159147
    iget v0, p1, LX/2sK;->M:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->D:I

    .line 159148
    iget v0, p1, LX/2sK;->N:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->E:I

    .line 159149
    iget-object v0, p1, LX/2sK;->O:Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->F:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 159150
    iget-object v0, p1, LX/2sK;->P:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->G:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 159151
    iget-object v0, p1, LX/2sK;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->H:Ljava/lang/String;

    .line 159152
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLMegaphone;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->v:Lcom/facebook/graphql/model/GraphQLMegaphone;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->v:Lcom/facebook/graphql/model/GraphQLMegaphone;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMegaphone;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->v:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 159155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->v:Lcom/facebook/graphql/model/GraphQLMegaphone;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLGreetingCard;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159157
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->x:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159158
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->x:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->x:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 159159
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->x:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    return-object v0
.end method

.method private C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159160
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159161
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->y:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->y:Ljava/lang/String;

    .line 159162
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->y:Ljava/lang/String;

    return-object v0
.end method

.method private D()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTaggableActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159163
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->z:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159164
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->z:Ljava/util/List;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->z:Ljava/util/List;

    .line 159165
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private E()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159166
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159167
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159168
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->A:I

    return v0
.end method

.method private F()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 159169
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159170
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159171
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->B:I

    return v0
.end method

.method private G()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159172
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159173
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159174
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->C:I

    return v0
.end method

.method private H()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159208
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159209
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159210
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->D:I

    return v0
.end method

.method private I()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159175
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159176
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159177
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->E:I

    return v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLMediaSet;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159205
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->F:Lcom/facebook/graphql/model/GraphQLMediaSet;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159206
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->F:Lcom/facebook/graphql/model/GraphQLMediaSet;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->F:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 159207
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->F:Lcom/facebook/graphql/model/GraphQLMediaSet;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159202
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->G:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159203
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->G:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->G:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 159204
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->G:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159199
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159200
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->H:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->H:Ljava/lang/String;

    .line 159201
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->H:Ljava/lang/String;

    return-object v0
.end method

.method private M()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159196
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159197
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159198
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->I:I

    return v0
.end method

.method private N()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159193
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159194
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159195
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->J:Z

    return v0
.end method

.method private O()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159190
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->K:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159191
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->K:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->K:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    .line 159192
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->K:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    return-object v0
.end method

.method private P()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159187
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->L:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159188
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->L:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->L:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    .line 159189
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->L:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    return-object v0
.end method

.method private Q()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->M:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->M:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->M:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    .line 159186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->M:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    return-object v0
.end method

.method private R()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159181
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159182
    const/4 v0, 0x4

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159183
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->N:Z

    return v0
.end method

.method private S()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159178
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159179
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159180
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->O:Z

    return v0
.end method

.method private T()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159100
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159101
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159102
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->P:Z

    return v0
.end method

.method private U()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 159103
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 159104
    const/4 v0, 0x5

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 159105
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->Q:I

    return v0
.end method

.method private V()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158841
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->R:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158842
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->R:Ljava/util/List;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->R:Ljava/util/List;

    .line 158843
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->R:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private W()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158871
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->S:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158872
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->S:Ljava/util/List;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->S:Ljava/util/List;

    .line 158873
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->S:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private X()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158868
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->T:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158869
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->T:Ljava/lang/String;

    const/16 v1, 0x2d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->T:Ljava/lang/String;

    .line 158870
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->T:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158865
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->e:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158866
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->e:Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->e:Lcom/facebook/graphql/model/GraphQLUser;

    .line 158867
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->e:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158862
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->f:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158863
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->f:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 158864
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->f:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158859
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->i:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158860
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->i:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->i:Lcom/facebook/graphql/model/GraphQLPage;

    .line 158861
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->i:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158856
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158857
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->j:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->j:Ljava/lang/String;

    .line 158858
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->j:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158853
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->m:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158854
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->m:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->m:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    .line 158855
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->m:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    return-object v0
.end method

.method private s()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 158850
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 158851
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 158852
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->n:I

    return v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158847
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->o:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158848
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->o:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->o:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 158849
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->o:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158844
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->p:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158845
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->p:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->p:Lcom/facebook/graphql/model/GraphQLPage;

    .line 158846
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->p:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 158838
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 158839
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 158840
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->q:Z

    return v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158875
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->r:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158876
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->r:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->r:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    .line 158877
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->r:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    return-object v0
.end method

.method private x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 158878
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 158879
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 158880
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->s:Z

    return v0
.end method

.method private y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 158881
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 158882
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 158883
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->t:Z

    return v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 158884
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->u:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 158885
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->u:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->u:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    .line 158886
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->u:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 29

    .prologue
    .line 158887
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 158888
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 158889
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 158890
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->a()Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 158891
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 158892
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 158893
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->q()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 158894
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->k()Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 158895
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 158896
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->r()Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 158897
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->t()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 158898
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->u()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 158899
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->w()Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 158900
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->z()Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 158901
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->A()Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 158902
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 158903
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->B()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 158904
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->C()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 158905
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->D()LX/0Px;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v19

    .line 158906
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->J()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 158907
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->K()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 158908
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->L()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 158909
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->O()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 158910
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->P()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 158911
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->V()LX/0Px;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v25

    .line 158912
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->W()LX/0Px;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v26

    .line 158913
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->X()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 158914
    const/16 v28, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 158915
    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 158916
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 158917
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 158918
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 158919
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 158920
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 158921
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 158922
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 158923
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 158924
    const/16 v2, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->s()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158925
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 158926
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 158927
    const/16 v2, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 158928
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 158929
    const/16 v2, 0x11

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 158930
    const/16 v2, 0x12

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 158931
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 158932
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 158933
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158934
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158935
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158936
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158937
    const/16 v2, 0x1a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->E()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158938
    const/16 v2, 0x1b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->F()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158939
    const/16 v2, 0x1c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->G()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158940
    const/16 v2, 0x1d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->H()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158941
    const/16 v2, 0x1e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->I()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158942
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158943
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158944
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158945
    const/16 v2, 0x22

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->M()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158946
    const/16 v2, 0x23

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->N()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 158947
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158948
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158949
    const/16 v3, 0x26

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->Q()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 158950
    const/16 v2, 0x27

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->R()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 158951
    const/16 v2, 0x28

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->S()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 158952
    const/16 v2, 0x29

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->T()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 158953
    const/16 v2, 0x2a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->U()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 158954
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158955
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158956
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 158957
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 158958
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 158959
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLViewer;->Q()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 158960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 158961
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158962
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 158963
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 158964
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 158965
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->e:Lcom/facebook/graphql/model/GraphQLUser;

    .line 158966
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 158967
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 158968
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 158969
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 158970
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 158971
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->a()Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 158972
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->a()Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    .line 158973
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->a()Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 158974
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 158975
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->g:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    .line 158976
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 158977
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    .line 158978
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 158979
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 158980
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    .line 158981
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->W()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 158982
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->W()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 158983
    if-eqz v2, :cond_4

    .line 158984
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 158985
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLViewer;->S:Ljava/util/List;

    move-object v1, v0

    .line 158986
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 158987
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 158988
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->p()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 158989
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 158990
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->i:Lcom/facebook/graphql/model/GraphQLPage;

    .line 158991
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->k()Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 158992
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->k()Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 158993
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->k()Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 158994
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 158995
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->k:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 158996
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 158997
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    .line 158998
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 158999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159000
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->l:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    .line 159001
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->r()Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 159002
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->r()Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    .line 159003
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->r()Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 159004
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159005
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->m:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;

    .line 159006
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->t()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 159007
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->t()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 159008
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->t()Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 159009
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159010
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->o:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;

    .line 159011
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->u()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 159012
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->u()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 159013
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->u()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 159014
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159015
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->p:Lcom/facebook/graphql/model/GraphQLPage;

    .line 159016
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->w()Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 159017
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->w()Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    .line 159018
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->w()Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 159019
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159020
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->r:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;

    .line 159021
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->z()Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 159022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->z()Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    .line 159023
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->z()Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 159024
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159025
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->u:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    .line 159026
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->A()Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 159027
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->A()Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 159028
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->A()Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 159029
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159030
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->v:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 159031
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 159032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 159033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 159034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159035
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->w:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 159036
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->B()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 159037
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->B()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 159038
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->B()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 159039
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159040
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->x:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 159041
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->O()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 159042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->O()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    .line 159043
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->O()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 159044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159045
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->K:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;

    .line 159046
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->P()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 159047
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->P()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    .line 159048
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->P()Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 159049
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159050
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->L:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;

    .line 159051
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->D()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 159052
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->D()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 159053
    if-eqz v2, :cond_12

    .line 159054
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159055
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLViewer;->z:Ljava/util/List;

    move-object v1, v0

    .line 159056
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->J()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 159057
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->J()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 159058
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->J()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 159059
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159060
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->F:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 159061
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->K()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 159062
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->K()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 159063
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLViewer;->K()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 159064
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLViewer;

    .line 159065
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLViewer;->G:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 159066
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 159067
    if-nez v1, :cond_15

    :goto_0
    return-object p0

    :cond_15
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLAudienceInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159068
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->g:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159069
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->g:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->g:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    .line 159070
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->g:Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159071
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 159072
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->n:I

    .line 159073
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->q:Z

    .line 159074
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->s:Z

    .line 159075
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->t:Z

    .line 159076
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->A:I

    .line 159077
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->B:I

    .line 159078
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->C:I

    .line 159079
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->D:I

    .line 159080
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->E:I

    .line 159081
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->I:I

    .line 159082
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->J:Z

    .line 159083
    const/16 v0, 0x27

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->N:Z

    .line 159084
    const/16 v0, 0x28

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->O:Z

    .line 159085
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->P:Z

    .line 159086
    const/16 v0, 0x2a

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->Q:I

    .line 159087
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 158874
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159088
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159089
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    .line 159090
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLCustomizedStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159091
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->k:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159092
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->k:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->k:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 159093
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->k:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159094
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->l:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159095
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->l:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->l:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    .line 159096
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->l:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159097
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->w:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 159098
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->w:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->w:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 159099
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLViewer;->w:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    return-object v0
.end method
