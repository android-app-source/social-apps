.class public abstract Lcom/facebook/graphql/model/BaseFeedUnit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 187760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 187764
    const/4 v0, 0x0

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187763
    const/4 v0, 0x0

    return-object v0
.end method

.method public F_()J
    .locals 2

    .prologue
    .line 187762
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 187761
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 187759
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "flattenToBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 187758
    return-object p0
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 187765
    return-void
.end method

.method public final a(LX/15i;I)V
    .locals 2

    .prologue
    .line 187757
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "initFromMutableFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 2

    .prologue
    .line 187756
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "initFromFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187754
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 187755
    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 187748
    const/4 v0, -0x1

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187753
    const/4 v0, 0x0

    return-object v0
.end method

.method public final o_()I
    .locals 2

    .prologue
    .line 187752
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getPositionInMutableFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public q_()LX/15i;
    .locals 2

    .prologue
    .line 187751
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getMutableFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 187749
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/BaseFeedUnit;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 187750
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot clone "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
