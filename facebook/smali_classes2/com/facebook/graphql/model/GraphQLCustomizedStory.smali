.class public final Lcom/facebook/graphql/model/GraphQLCustomizedStory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements LX/16e;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.implements LX/0jR;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLCustomizedStory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLCustomizedStory$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:J

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:I

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public y:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250300
    const-class v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250192
    const-class v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 250193
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 250194
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x2dc9932c

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 250195
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->K:LX/0x2;

    .line 250196
    return-void
.end method

.method public constructor <init>(LX/3j1;)V
    .locals 2

    .prologue
    .line 250197
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 250198
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x2dc9932c

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 250199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->K:LX/0x2;

    .line 250200
    iget-object v0, p1, LX/3j1;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->f:Ljava/util/List;

    .line 250201
    iget-object v0, p1, LX/3j1;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g:Ljava/util/List;

    .line 250202
    iget-object v0, p1, LX/3j1;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 250203
    iget-object v0, p1, LX/3j1;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->i:Ljava/util/List;

    .line 250204
    iget-object v0, p1, LX/3j1;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->j:Ljava/lang/String;

    .line 250205
    iget-wide v0, p1, LX/3j1;->g:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->k:J

    .line 250206
    iget-object v0, p1, LX/3j1;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->l:Ljava/lang/String;

    .line 250207
    iget-object v0, p1, LX/3j1;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 250208
    iget-object v0, p1, LX/3j1;->j:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 250209
    iget-wide v0, p1, LX/3j1;->k:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->o:J

    .line 250210
    iget-object v0, p1, LX/3j1;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p:Ljava/lang/String;

    .line 250211
    iget-object v0, p1, LX/3j1;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q:Ljava/lang/String;

    .line 250212
    iget-object v0, p1, LX/3j1;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->r:Ljava/lang/String;

    .line 250213
    iget-object v0, p1, LX/3j1;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->s:Ljava/lang/String;

    .line 250214
    iget v0, p1, LX/3j1;->p:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->t:I

    .line 250215
    iget-object v0, p1, LX/3j1;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250216
    iget-object v0, p1, LX/3j1;->r:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 250217
    iget-object v0, p1, LX/3j1;->s:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250218
    iget-object v0, p1, LX/3j1;->t:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 250219
    iget-object v0, p1, LX/3j1;->u:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 250220
    iget-object v0, p1, LX/3j1;->v:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 250221
    iget-object v0, p1, LX/3j1;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z:Ljava/lang/String;

    .line 250222
    iget-object v0, p1, LX/3j1;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250223
    iget-object v0, p1, LX/3j1;->y:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->B:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 250224
    iget-object v0, p1, LX/3j1;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250225
    iget-object v0, p1, LX/3j1;->A:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D:Ljava/util/List;

    .line 250226
    iget-object v0, p1, LX/3j1;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250227
    iget-object v0, p1, LX/3j1;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250228
    iget-object v0, p1, LX/3j1;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250229
    iget-object v0, p1, LX/3j1;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H:Ljava/lang/String;

    .line 250230
    iget-object v0, p1, LX/3j1;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I:Ljava/lang/String;

    .line 250231
    iget-object v0, p1, LX/3j1;->G:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->K:LX/0x2;

    .line 250232
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250233
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250234
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 250235
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private B()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 250236
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 250237
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 250238
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->k:J

    return-wide v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250239
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250240
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 250241
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250242
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250243
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 250244
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    return-object v0
.end method

.method private E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250245
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250246
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->r:Ljava/lang/String;

    .line 250247
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->r:Ljava/lang/String;

    return-object v0
.end method

.method private F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250248
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250249
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->s:Ljava/lang/String;

    .line 250250
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->s:Ljava/lang/String;

    return-object v0
.end method

.method private G()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 250251
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 250252
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 250253
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->t:I

    return v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250254
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250255
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250256
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250257
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250258
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 250259
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 250260
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250261
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 250262
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method private K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250266
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250267
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z:Ljava/lang/String;

    .line 250268
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z:Ljava/lang/String;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250304
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250305
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250306
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250301
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250302
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250303
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private N()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250183
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250184
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D:Ljava/util/List;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D:Ljava/util/List;

    .line 250185
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250297
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250298
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250299
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250294
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250295
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250296
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private Q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250291
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250292
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I:Ljava/lang/String;

    .line 250293
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 250285
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->t:I

    .line 250286
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 250287
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 250288
    if-eqz v0, :cond_0

    .line 250289
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 250290
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 250279
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->r:Ljava/lang/String;

    .line 250280
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 250281
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 250282
    if-eqz v0, :cond_0

    .line 250283
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 250284
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 250273
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->s:Ljava/lang/String;

    .line 250274
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 250275
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 250276
    if-eqz v0, :cond_0

    .line 250277
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 250278
    :cond_0
    return-void
.end method

.method private z()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250263
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250264
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g:Ljava/util/List;

    .line 250265
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250270
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250271
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p:Ljava/lang/String;

    .line 250272
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 250269
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250186
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250187
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->l:Ljava/lang/String;

    .line 250188
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 250189
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 250190
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 250191
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->o:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 250149
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 250146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->K:LX/0x2;

    if-nez v0, :cond_0

    .line 250147
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->K:LX/0x2;

    .line 250148
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->K:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 250145
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 30

    .prologue
    .line 250082
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250083
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 250084
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 250085
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 250086
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 250087
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 250088
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 250089
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 250090
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 250091
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 250092
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->t()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 250093
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 250094
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 250095
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 250096
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 250097
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 250098
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 250099
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->K()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 250100
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 250101
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 250102
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->M()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 250103
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->N()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->d(Ljava/util/List;)I

    move-result v23

    .line 250104
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 250105
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 250106
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 250107
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->c()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 250108
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->Q()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 250109
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 250110
    const/16 v7, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 250111
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 250112
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 250113
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 250114
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 250115
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 250116
    const/4 v3, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->B()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 250117
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 250118
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 250119
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 250120
    const/16 v3, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 250121
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 250122
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 250123
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 250124
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 250125
    const/16 v2, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 250126
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 250127
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250128
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250129
    const/16 v3, 0x13

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 250130
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250131
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250132
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250133
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250134
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250135
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250136
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250137
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250138
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250139
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250140
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250141
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250142
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250143
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 250144
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 249994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 249995
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 249996
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->p()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 249997
    if-eqz v1, :cond_0

    .line 249998
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 249999
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->f:Ljava/util/List;

    .line 250000
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 250001
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->z()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 250002
    if-eqz v1, :cond_1

    .line 250003
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250004
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 250005
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 250006
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 250007
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 250008
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250009
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 250010
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 250011
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 250012
    if-eqz v2, :cond_3

    .line 250013
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250014
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->i:Ljava/util/List;

    move-object v1, v0

    .line 250015
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 250016
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 250017
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 250018
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250019
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 250020
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 250021
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 250022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->D()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 250023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250024
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->n:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 250025
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 250026
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250027
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 250028
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250029
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250030
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 250031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 250032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 250033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250034
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 250035
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 250036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 250038
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250039
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250040
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 250041
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 250042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 250043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250044
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 250045
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 250046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 250047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 250048
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250049
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 250050
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 250051
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250052
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 250053
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250054
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250055
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 250056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 250057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 250058
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250059
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->B:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 250060
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->M()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 250061
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->M()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250062
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->M()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 250063
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250064
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250065
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 250066
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250067
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 250068
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250069
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250070
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 250071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250072
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 250073
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250074
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250075
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 250076
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250077
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 250078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 250079
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250080
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250081
    if-nez v1, :cond_11

    :goto_0
    return-object p0

    :cond_11
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 249955
    new-instance v0, LX/4W4;

    invoke-direct {v0, p1}, LX/4W4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249993
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 249991
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->o:J

    .line 249992
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 249986
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 249987
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->k:J

    .line 249988
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->o:J

    .line 249989
    const/16 v0, 0xf

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->t:I

    .line 249990
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 249972
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249973
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->E()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 249974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 249975
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    .line 249976
    :goto_0
    return-void

    .line 249977
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249978
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 249979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 249980
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 249981
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 249982
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->G()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 249983
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 249984
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 249985
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 249965
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249966
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->a(Ljava/lang/String;)V

    .line 249967
    :cond_0
    :goto_0
    return-void

    .line 249968
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 249969
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 249970
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249971
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249964
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249961
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249962
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H:Ljava/lang/String;

    .line 249963
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249956
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 249958
    :goto_0
    return-object v0

    .line 249959
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 249960
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 250150
    const v0, -0x2dc9932c

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250151
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250152
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->j:Ljava/lang/String;

    .line 250153
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250154
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->t()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 250156
    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250157
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250158
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->f:Ljava/util/List;

    .line 250159
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250160
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250161
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->i:Ljava/util/List;

    .line 250162
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 250163
    invoke-static {p0}, LX/1w8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 250164
    invoke-static {p0}, LX/1w8;->a(LX/16e;)Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q:Ljava/lang/String;

    .line 250167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 250170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250171
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->B:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250172
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->B:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->B:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 250173
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->B:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250174
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250175
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250176
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250177
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250178
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250179
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250180
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250181
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 250182
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    return-object v0
.end method
