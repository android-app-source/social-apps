.class public Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jQ;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedUnitEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedUnitEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:D

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 123167
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 123132
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 123133
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 123134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m:LX/0x2;

    .line 123135
    return-void
.end method

.method public constructor <init>(LX/1u8;)V
    .locals 2

    .prologue
    .line 123136
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 123137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m:LX/0x2;

    .line 123138
    iget-object v0, p1, LX/1u8;->b:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 123139
    iget-object v0, p1, LX/1u8;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->f:Ljava/lang/String;

    .line 123140
    iget-object v0, p1, LX/1u8;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g:Ljava/lang/String;

    .line 123141
    iget-boolean v0, p1, LX/1u8;->e:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->h:Z

    .line 123142
    iget-object v0, p1, LX/1u8;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->i:Ljava/lang/String;

    .line 123143
    iget-object v0, p1, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->j:Lcom/facebook/graphql/model/FeedUnit;

    .line 123144
    iget-wide v0, p1, LX/1u8;->h:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k:D

    .line 123145
    iget-object v0, p1, LX/1u8;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l:Ljava/lang/String;

    .line 123146
    iget-object v0, p1, LX/1u8;->j:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m:LX/0x2;

    .line 123147
    return-void
.end method


# virtual methods
.method public final J_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123148
    invoke-static {p0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 123149
    if-nez v0, :cond_0

    .line 123150
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    .line 123151
    :cond_0
    return-object v0
.end method

.method public final K_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123152
    invoke-static {p0}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 123153
    if-nez v0, :cond_0

    .line 123154
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 123155
    :cond_0
    return-object v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 123156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m:LX/0x2;

    if-nez v0, :cond_0

    .line 123157
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m:LX/0x2;

    .line 123158
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 123178
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 123179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 123180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 123181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 123182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    sget-object v4, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v4

    .line 123183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 123184
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 123185
    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-ne v0, v7, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 123186
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 123187
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 123188
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 123189
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 123190
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 123191
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 123192
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 123193
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 123194
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 123195
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 123159
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 123160
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 123162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 123163
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 123164
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->j:Lcom/facebook/graphql/model/FeedUnit;

    .line 123165
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 123166
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(D)V
    .locals 1

    .prologue
    .line 123125
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k:D

    .line 123126
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 123168
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 123169
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->h:Z

    .line 123170
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k:D

    .line 123171
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)V
    .locals 0

    .prologue
    .line 123172
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 123173
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123174
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->j:Lcom/facebook/graphql/model/FeedUnit;

    .line 123175
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123176
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->f:Ljava/lang/String;

    .line 123177
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 123127
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->h:Z

    .line 123128
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 123129
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 123130
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g:Ljava/lang/String;

    .line 123131
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123083
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g:Ljava/lang/String;

    .line 123084
    return-void
.end method

.method public c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 123085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123086
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->i:Ljava/lang/String;

    .line 123087
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 123088
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 123089
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l:Ljava/lang/String;

    .line 123090
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123091
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l:Ljava/lang/String;

    .line 123092
    return-void
.end method

.method public final e()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 123093
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 123094
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 123095
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k:D

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 123096
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-nez v0, :cond_0

    .line 123097
    const/4 v0, 0x0

    .line 123098
    :goto_0
    move v0, v0

    .line 123099
    return v0

    .line 123100
    :cond_0
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 123101
    invoke-static {p0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 123102
    const v0, 0x16e4793f

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 123103
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 123104
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->f:Ljava/lang/String;

    .line 123105
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 123106
    invoke-static {p0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 123107
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 123108
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123109
    invoke-static {p0}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 123110
    if-nez v0, :cond_0

    .line 123111
    const-string v0, "0"

    .line 123112
    :cond_0
    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 123113
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 123114
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->i:Ljava/lang/String;

    .line 123115
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 123116
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 123117
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 123118
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->h:Z

    return v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLBumpReason;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 123119
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 123120
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 123121
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 123122
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->j:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 123123
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->j:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x5

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->j:Lcom/facebook/graphql/model/FeedUnit;

    .line 123124
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->j:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method
