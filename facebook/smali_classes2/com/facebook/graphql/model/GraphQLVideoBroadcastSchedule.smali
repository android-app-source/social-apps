.class public final Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:J

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:J

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:J

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321260
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321261
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321262
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321263
    return-void
.end method

.method public constructor <init>(LX/4ZO;)V
    .locals 2

    .prologue
    .line 321264
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321265
    iget-object v0, p1, LX/4ZO;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j:Ljava/lang/String;

    .line 321266
    iget-object v0, p1, LX/4ZO;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z:Ljava/lang/String;

    .line 321267
    iget-object v0, p1, LX/4ZO;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k:Ljava/lang/String;

    .line 321268
    iget-wide v0, p1, LX/4ZO;->e:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y:J

    .line 321269
    iget-object v0, p1, LX/4ZO;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l:Ljava/lang/String;

    .line 321270
    iget-object v0, p1, LX/4ZO;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A:Ljava/lang/String;

    .line 321271
    iget-wide v0, p1, LX/4ZO;->h:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->e:J

    .line 321272
    iget-object v0, p1, LX/4ZO;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m:Ljava/lang/String;

    .line 321273
    iget-object v0, p1, LX/4ZO;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x:Ljava/lang/String;

    .line 321274
    iget-object v0, p1, LX/4ZO;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->f:Ljava/lang/String;

    .line 321275
    iget-boolean v0, p1, LX/4ZO;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w:Z

    .line 321276
    iget-boolean v0, p1, LX/4ZO;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n:Z

    .line 321277
    iget-boolean v0, p1, LX/4ZO;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->g:Z

    .line 321278
    iget-wide v0, p1, LX/4ZO;->o:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->h:J

    .line 321279
    iget-object v0, p1, LX/4ZO;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o:Ljava/lang/String;

    .line 321280
    iget-object v0, p1, LX/4ZO;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p:Ljava/lang/String;

    .line 321281
    iget-object v0, p1, LX/4ZO;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q:Ljava/lang/String;

    .line 321282
    iget-object v0, p1, LX/4ZO;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B:Ljava/lang/String;

    .line 321283
    iget-wide v0, p1, LX/4ZO;->t:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->r:J

    .line 321284
    iget-object v0, p1, LX/4ZO;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s:Ljava/lang/String;

    .line 321285
    iget-object v0, p1, LX/4ZO;->v:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321286
    iget-object v0, p1, LX/4ZO;->w:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321287
    iget-object v0, p1, LX/4ZO;->x:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321288
    iget-wide v0, p1, LX/4ZO;->y:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->i:J

    .line 321289
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 321290
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->i:J

    .line 321291
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 321292
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 321293
    if-eqz v0, :cond_0

    .line 321294
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 321295
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 321296
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o:Ljava/lang/String;

    .line 321297
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 321298
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 321299
    if-eqz v0, :cond_0

    .line 321300
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 321301
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 321302
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->g:Z

    .line 321303
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 321304
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 321305
    if-eqz v0, :cond_0

    .line 321306
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 321307
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 321308
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p:Ljava/lang/String;

    .line 321309
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 321310
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 321311
    if-eqz v0, :cond_0

    .line 321312
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 321313
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 321314
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w:Z

    .line 321315
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 321316
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 321317
    if-eqz v0, :cond_0

    .line 321318
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 321319
    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 321320
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q:Ljava/lang/String;

    .line 321321
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 321322
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 321323
    if-eqz v0, :cond_0

    .line 321324
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 321325
    :cond_0
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 321409
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x:Ljava/lang/String;

    .line 321410
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 321411
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 321412
    if-eqz v0, :cond_0

    .line 321413
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 321414
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321326
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321327
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321328
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321329
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321330
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321331
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w:Z

    return v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321332
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321333
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x:Ljava/lang/String;

    .line 321334
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final D()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321335
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321336
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321337
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y:J

    return-wide v0
.end method

.method public final E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321338
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321339
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z:Ljava/lang/String;

    .line 321340
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321341
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321342
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A:Ljava/lang/String;

    .line 321343
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321344
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321345
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B:Ljava/lang/String;

    .line 321346
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 24

    .prologue
    .line 321347
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321348
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 321349
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 321350
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 321351
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 321352
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->r()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 321353
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 321354
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 321355
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 321356
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 321357
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 321358
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 321359
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 321360
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->C()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 321361
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->E()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 321362
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->F()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 321363
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->G()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 321364
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 321365
    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 321366
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 321367
    const/4 v2, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 321368
    const/4 v3, 0x4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 321369
    const/4 v3, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 321370
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 321371
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 321372
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 321373
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 321374
    const/16 v2, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 321375
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 321376
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 321377
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 321378
    const/16 v3, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 321379
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321380
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321381
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321382
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321383
    const/16 v2, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 321384
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321385
    const/16 v3, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->D()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 321386
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321387
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321388
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 321389
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321390
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 321391
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321392
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 321393
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 321394
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 321395
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 321396
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321397
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 321398
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 321399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 321400
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 321401
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321402
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 321403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 321404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 321405
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 321406
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321407
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321408
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 321258
    new-instance v0, LX/4ZP;

    invoke-direct {v0, p1}, LX/4ZP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321259
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 321154
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 321155
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->e:J

    .line 321156
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->g:Z

    .line 321157
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->h:J

    .line 321158
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->i:J

    .line 321159
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n:Z

    .line 321160
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->r:J

    .line 321161
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w:Z

    .line 321162
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y:J

    .line 321163
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 321195
    const-string v0, "formatted_start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 321197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 321198
    const/16 v0, 0x14

    iput v0, p2, LX/18L;->c:I

    .line 321199
    :goto_0
    return-void

    .line 321200
    :cond_0
    const-string v0, "is_rescheduled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 321202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 321203
    const/16 v0, 0x13

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 321204
    :cond_1
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 321205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 321206
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 321207
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 321208
    :cond_2
    const-string v0, "rescheduled_endscreen_body"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 321209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 321210
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 321211
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 321212
    :cond_3
    const-string v0, "rescheduled_endscreen_title"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 321213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 321214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 321215
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 321216
    :cond_4
    const-string v0, "rescheduled_heading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 321217
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 321218
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 321219
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 321220
    :cond_5
    const-string v0, "start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 321221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 321222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 321223
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 321224
    :cond_6
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 321180
    const-string v0, "formatted_start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321181
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->d(Ljava/lang/String;)V

    .line 321182
    :cond_0
    :goto_0
    return-void

    .line 321183
    :cond_1
    const-string v0, "is_rescheduled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 321184
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->b(Z)V

    goto :goto_0

    .line 321185
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 321186
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->a(Z)V

    goto :goto_0

    .line 321187
    :cond_3
    const-string v0, "rescheduled_endscreen_body"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 321188
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 321189
    :cond_4
    const-string v0, "rescheduled_endscreen_title"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 321190
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 321191
    :cond_5
    const-string v0, "rescheduled_heading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 321192
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 321193
    :cond_6
    const-string v0, "start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321194
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->a(J)V

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 321179
    const v0, 0x56118b5d    # 4.0006937E13f

    return v0
.end method

.method public final j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321176
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321177
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321178
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->e:J

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321173
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321174
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->f:Ljava/lang/String;

    .line 321175
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321170
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321171
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321172
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->g:Z

    return v0
.end method

.method public final m()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321167
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321168
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321169
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->h:J

    return-wide v0
.end method

.method public final n()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321164
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321165
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321166
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->i:J

    return-wide v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321151
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321152
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j:Ljava/lang/String;

    .line 321153
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321225
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321226
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k:Ljava/lang/String;

    .line 321227
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321228
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321229
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l:Ljava/lang/String;

    .line 321230
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321231
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321232
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m:Ljava/lang/String;

    .line 321233
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321234
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321235
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321236
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321237
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321238
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o:Ljava/lang/String;

    .line 321239
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321240
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321241
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p:Ljava/lang/String;

    .line 321242
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321243
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321244
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q:Ljava/lang/String;

    .line 321245
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321246
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321247
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321248
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->r:J

    return-wide v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321249
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321250
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s:Ljava/lang/String;

    .line 321251
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321252
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321253
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321254
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321255
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321256
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 321257
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method
