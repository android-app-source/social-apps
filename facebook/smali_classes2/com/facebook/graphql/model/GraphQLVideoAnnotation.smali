.class public final Lcom/facebook/graphql/model/GraphQLVideoAnnotation;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoAnnotation$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoAnnotation$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321134
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321133
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321131
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321132
    return-void
.end method

.method private m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 321128
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321129
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321130
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->e:I

    return v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321125
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321126
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->h:Ljava/lang/String;

    .line 321127
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->h:Ljava/lang/String;

    return-object v0
.end method

.method private o()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321122
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321123
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321124
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->k:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 321135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321136
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 321137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 321138
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 321139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 321140
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 321141
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 321142
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->m()I

    move-result v5

    invoke-virtual {p1, v6, v5, v6}, LX/186;->a(III)V

    .line 321143
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 321144
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 321145
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 321146
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 321147
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 321148
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->o()I

    move-result v1

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 321149
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321150
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 321119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321121
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321102
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321103
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->f:Ljava/lang/String;

    .line 321104
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 321115
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 321116
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->e:I

    .line 321117
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->k:I

    .line 321118
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 321114
    const v0, -0x4ec28a16

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321111
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321112
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->g:Ljava/lang/String;

    .line 321113
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321108
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321109
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->i:Ljava/lang/String;

    .line 321110
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->j:Ljava/lang/String;

    .line 321107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->j:Ljava/lang/String;

    return-object v0
.end method
