.class public final Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSportsDataMatchData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSportsDataMatchData$Serializer;
.end annotation


# instance fields
.field public A:J

.field public B:I

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:I

.field public E:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:J

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Z

.field public M:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Z

.field public s:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:I

.field public u:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:I

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326854
    const-class v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326851
    const-class v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 326852
    const/16 v0, 0x25

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 326853
    return-void
.end method

.method private A()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326855
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326856
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326857
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->q:I

    return v0
.end method

.method private B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326858
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326859
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326860
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->r:Z

    return v0
.end method

.method private C()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326861
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326862
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326863
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->t:I

    return v0
.end method

.method private D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326864
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326865
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->u:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->u:Ljava/lang/String;

    .line 326866
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->u:Ljava/lang/String;

    return-object v0
.end method

.method private E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326867
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326868
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->v:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->v:Ljava/lang/String;

    .line 326869
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->v:Ljava/lang/String;

    return-object v0
.end method

.method private F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326870
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326871
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x:Ljava/lang/String;

    .line 326872
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x:Ljava/lang/String;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326873
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->y:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326874
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->y:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->y:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326875
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->y:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private H()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326876
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326877
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326878
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->z:I

    return v0
.end method

.method private I()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326879
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326880
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326881
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->B:I

    return v0
.end method

.method private J()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326924
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326925
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->C:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->C:Ljava/lang/String;

    .line 326926
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->C:Ljava/lang/String;

    return-object v0
.end method

.method private K()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326882
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326883
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326884
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->D:I

    return v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326921
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->E:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326922
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->E:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->E:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 326923
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->E:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326918
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->F:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326919
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->F:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326920
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->F:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326915
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326916
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G:Ljava/lang/String;

    .line 326917
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G:Ljava/lang/String;

    return-object v0
.end method

.method private O()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326912
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326913
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326914
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->J:J

    return-wide v0
.end method

.method private P()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326909
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326910
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->K:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->K:Ljava/lang/String;

    .line 326911
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->K:Ljava/lang/String;

    return-object v0
.end method

.method private Q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326906
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326907
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326908
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->L:Z

    return v0
.end method

.method private R()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326903
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326904
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326905
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 326897
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->g:I

    .line 326898
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 326899
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 326900
    if-eqz v0, :cond_0

    .line 326901
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 326902
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 326891
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->I:Ljava/lang/String;

    .line 326892
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 326893
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 326894
    if-eqz v0, :cond_0

    .line 326895
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 326896
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 326885
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->L:Z

    .line 326886
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 326887
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 326888
    if-eqz v0, :cond_0

    .line 326889
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 326890
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 326839
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j:I

    .line 326840
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 326841
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 326842
    if-eqz v0, :cond_0

    .line 326843
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 326844
    :cond_0
    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 326845
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->t:I

    .line 326846
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 326847
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 326848
    if-eqz v0, :cond_0

    .line 326849
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 326850
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 326753
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->w:I

    .line 326754
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 326755
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 326756
    if-eqz v0, :cond_0

    .line 326757
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 326758
    :cond_0
    return-void
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326750
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->e:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326751
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->e:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326752
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->e:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private s()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326747
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326748
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326749
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->g:I

    return v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326744
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326745
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->h:Ljava/lang/String;

    .line 326746
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->h:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326741
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326742
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->i:Ljava/lang/String;

    .line 326743
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->i:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326738
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326739
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->l:Ljava/lang/String;

    .line 326740
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->l:Ljava/lang/String;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326735
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326736
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    .line 326737
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326732
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326733
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    .line 326734
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    return-object v0
.end method

.method private y()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326729
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326730
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326731
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->o:I

    return v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326726
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326727
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->p:Ljava/lang/String;

    .line 326728
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 25

    .prologue
    .line 326665
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326666
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 326667
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 326668
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 326669
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 326670
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->l()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 326671
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->v()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 326672
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->w()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 326673
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 326674
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->z()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 326675
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 326676
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->D()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 326677
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->E()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 326678
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->F()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 326679
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 326680
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->J()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 326681
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 326682
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 326683
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->N()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 326684
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->p()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 326685
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->q()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 326686
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->P()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 326687
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 326688
    const/16 v24, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 326689
    const/16 v24, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 326690
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 326691
    const/4 v2, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->s()I

    move-result v3

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, LX/186;->a(III)V

    .line 326692
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 326693
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 326694
    const/4 v2, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->k()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326695
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 326696
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 326697
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 326698
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 326699
    const/16 v2, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->y()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326700
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 326701
    const/16 v2, 0xd

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->A()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326702
    const/16 v2, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->B()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 326703
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 326704
    const/16 v2, 0x10

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->C()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326705
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 326706
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 326707
    const/16 v2, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326708
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 326709
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 326710
    const/16 v2, 0x16

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->H()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326711
    const/16 v3, 0x17

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->o()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 326712
    const/16 v2, 0x18

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->I()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326713
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326714
    const/16 v2, 0x1a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->K()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326715
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326716
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326717
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326718
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326719
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326720
    const/16 v3, 0x20

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->O()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 326721
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326722
    const/16 v2, 0x22

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->Q()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 326723
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326724
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326725
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 326617
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326618
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326619
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 326620
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 326621
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326622
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326623
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 326624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 326625
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 326626
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326627
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326628
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->w()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 326629
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->w()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    .line 326630
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->w()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 326631
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326632
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFactsConnection;

    .line 326633
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 326634
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    .line 326635
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->x()Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 326636
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326637
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n:Lcom/facebook/graphql/model/GraphQLSportsDataMatchToFanFavoriteConnection;

    .line 326638
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 326639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 326640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 326641
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326642
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->s:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326643
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 326644
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 326645
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 326646
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326647
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->y:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326648
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 326649
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 326650
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 326651
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326652
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->E:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 326653
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 326654
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 326655
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 326656
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326657
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326658
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 326659
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 326660
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 326661
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 326662
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->M:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326663
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326664
    if-nez v1, :cond_9

    :goto_0
    return-object p0

    :cond_9
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 326616
    new-instance v0, LX/4Yn;

    invoke-direct {v0, p1}, LX/4Yn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326759
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 326760
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 326761
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->g:I

    .line 326762
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j:I

    .line 326763
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->o:I

    .line 326764
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->q:I

    .line 326765
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->r:Z

    .line 326766
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->t:I

    .line 326767
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->w:I

    .line 326768
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->z:I

    .line 326769
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->A:J

    .line 326770
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->B:I

    .line 326771
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->D:I

    .line 326772
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->J:J

    .line 326773
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->L:Z

    .line 326774
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 326775
    const-string v0, "away_team_fan_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326776
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 326777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 326778
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 326779
    :goto_0
    return-void

    .line 326780
    :cond_0
    const-string v0, "away_team_score"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326781
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 326782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 326783
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 326784
    :cond_1
    const-string v0, "home_team_fan_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326785
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 326786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 326787
    const/16 v0, 0x10

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 326788
    :cond_2
    const-string v0, "home_team_score"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 326789
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 326790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 326791
    const/16 v0, 0x13

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 326792
    :cond_3
    const-string v0, "status_text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326793
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 326794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 326795
    const/16 v0, 0x1f

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 326796
    :cond_4
    const-string v0, "viewer_can_vote_fan_favorite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 326797
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->Q()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 326798
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 326799
    const/16 v0, 0x22

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 326800
    :cond_5
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 326801
    const-string v0, "away_team_fan_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326802
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->a(I)V

    .line 326803
    :cond_0
    :goto_0
    return-void

    .line 326804
    :cond_1
    const-string v0, "away_team_score"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326805
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->b(I)V

    goto :goto_0

    .line 326806
    :cond_2
    const-string v0, "home_team_fan_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 326807
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->c(I)V

    goto :goto_0

    .line 326808
    :cond_3
    const-string v0, "home_team_score"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326809
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->d(I)V

    goto :goto_0

    .line 326810
    :cond_4
    const-string v0, "status_text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 326811
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 326812
    :cond_5
    const-string v0, "viewer_can_vote_fan_favorite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326813
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->a(Z)V

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 326814
    const v0, 0x31509926

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326815
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->f:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326816
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->f:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326817
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->f:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326818
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326819
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326820
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j:I

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326821
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326822
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->k:Ljava/lang/String;

    .line 326823
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326824
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->s:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326825
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->s:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->s:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326826
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->s:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326827
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326828
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326829
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->w:I

    return v0
.end method

.method public final o()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326836
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326837
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326838
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->A:J

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326830
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326831
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->H:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->H:Ljava/lang/String;

    .line 326832
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->I:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->I:Ljava/lang/String;

    .line 326835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->I:Ljava/lang/String;

    return-object v0
.end method
