.class public final Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250427
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250407
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 250408
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 250409
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x2022d481

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 250410
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->q:LX/0x2;

    .line 250411
    return-void
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250412
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250413
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250414
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250415
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250416
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n:Ljava/lang/String;

    .line 250417
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250418
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250419
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250420
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 250421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250422
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250423
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g:Ljava/lang/String;

    .line 250424
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 250459
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 250460
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 250461
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->k:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 250425
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 250426
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 250428
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->q:LX/0x2;

    if-nez v0, :cond_0

    .line 250429
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->q:LX/0x2;

    .line 250430
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->q:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 250431
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 250432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 250433
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 250434
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 250436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 250437
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 250438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 250439
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 250440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->s()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 250441
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 250442
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->w()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 250443
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 250444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 250445
    const/16 v5, 0xb

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 250446
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 250447
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 250448
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 250449
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 250450
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 250451
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 250452
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 250453
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 250454
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 250455
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 250456
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 250457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250458
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 250383
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 250385
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 250386
    if-eqz v1, :cond_4

    .line 250387
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    .line 250388
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->h:Ljava/util/List;

    move-object v1, v0

    .line 250389
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250390
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 250392
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    .line 250393
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250394
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 250395
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250396
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 250397
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    .line 250398
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250399
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 250400
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250401
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 250402
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    .line 250403
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250404
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250405
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 250406
    new-instance v0, LX/4WE;

    invoke-direct {v0, p1}, LX/4WE;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 250346
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->k:J

    .line 250347
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 250348
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 250349
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->k:J

    .line 250350
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 250351
    invoke-virtual {p2}, LX/18L;->a()V

    .line 250352
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 250353
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250354
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250380
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250381
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->p:Ljava/lang/String;

    .line 250382
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250356
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 250357
    :goto_0
    return-object v0

    .line 250358
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 250359
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 250360
    const v0, -0x2022d481

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250361
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250362
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->f:Ljava/lang/String;

    .line 250363
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250364
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->s()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 250365
    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250366
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250367
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->h:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->h:Ljava/util/List;

    .line 250368
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250369
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250370
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250371
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 250372
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 250373
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250374
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250375
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->j:Ljava/lang/String;

    .line 250376
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250377
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250378
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->l:Ljava/lang/String;

    .line 250379
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method
