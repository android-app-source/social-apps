.class public final Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements LX/16e;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I

.field public n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250648
    const-class v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 250647
    const-class v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 250640
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 250641
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x49792cfc

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 250642
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->s:LX/0x2;

    .line 250643
    return-void
.end method

.method public constructor <init>(LX/4Vx;)V
    .locals 2

    .prologue
    .line 250649
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 250650
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x49792cfc

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 250651
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->s:LX/0x2;

    .line 250652
    iget-object v0, p1, LX/4Vx;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->f:Ljava/lang/String;

    .line 250653
    iget-object v0, p1, LX/4Vx;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g:Ljava/lang/String;

    .line 250654
    iget-wide v0, p1, LX/4Vx;->d:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->h:J

    .line 250655
    iget-object v0, p1, LX/4Vx;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->i:Ljava/lang/String;

    .line 250656
    iget-object v0, p1, LX/4Vx;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->j:Ljava/util/List;

    .line 250657
    iget-object v0, p1, LX/4Vx;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->k:Ljava/lang/String;

    .line 250658
    iget-object v0, p1, LX/4Vx;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->r:Ljava/util/List;

    .line 250659
    iget-object v0, p1, LX/4Vx;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->l:Ljava/lang/String;

    .line 250660
    iget v0, p1, LX/4Vx;->j:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->m:I

    .line 250661
    iget-object v0, p1, LX/4Vx;->k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250662
    iget-object v0, p1, LX/4Vx;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->o:Ljava/lang/String;

    .line 250663
    iget-object v0, p1, LX/4Vx;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250664
    iget-object v0, p1, LX/4Vx;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->q:Ljava/lang/String;

    .line 250665
    iget-object v0, p1, LX/4Vx;->o:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->s:LX/0x2;

    .line 250666
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 250667
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->m:I

    .line 250668
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 250669
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 250670
    if-eqz v0, :cond_0

    .line 250671
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 250672
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 250673
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->k:Ljava/lang/String;

    .line 250674
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 250675
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 250676
    if-eqz v0, :cond_0

    .line 250677
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 250678
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 250679
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->l:Ljava/lang/String;

    .line 250680
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 250681
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 250682
    if-eqz v0, :cond_0

    .line 250683
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 250684
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250685
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250686
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->r:Ljava/util/List;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->r:Ljava/util/List;

    .line 250687
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250688
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250689
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->i:Ljava/lang/String;

    .line 250690
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 250691
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250692
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250693
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g:Ljava/lang/String;

    .line 250694
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 250695
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 250696
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 250697
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->h:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 250698
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 250699
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 250700
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->s:LX/0x2;

    if-nez v0, :cond_0

    .line 250701
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->s:LX/0x2;

    .line 250702
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->s:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 250703
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 250704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 250705
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 250706
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250707
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 250708
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 250709
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->C_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 250710
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 250711
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->u()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 250712
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->v()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 250713
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 250714
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->y()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 250715
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 250716
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 250717
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->A()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v16

    .line 250718
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 250719
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 250720
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 250721
    const/4 v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 250722
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 250723
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 250724
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 250725
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 250726
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->w()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 250727
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 250728
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 250729
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 250730
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 250731
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 250732
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250733
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 250734
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250735
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 250736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 250737
    if-eqz v1, :cond_3

    .line 250738
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 250739
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->j:Ljava/util/List;

    move-object v1, v0

    .line 250740
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250742
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 250743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 250744
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250745
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 250746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250747
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 250748
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 250749
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250750
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 250751
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 250752
    new-instance v0, LX/4Vy;

    invoke-direct {v0, p1}, LX/4Vy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 250645
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->h:J

    .line 250646
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 250574
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 250575
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->h:J

    .line 250576
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->m:I

    .line 250577
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 250578
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250579
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 250580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 250581
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 250582
    :goto_0
    return-void

    .line 250583
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 250585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 250586
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 250587
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->w()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 250589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 250590
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 250591
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 250592
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250593
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->a(Ljava/lang/String;)V

    .line 250594
    :cond_0
    :goto_0
    return-void

    .line 250595
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250596
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 250597
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250598
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250599
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250600
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250601
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->q:Ljava/lang/String;

    .line 250602
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 250605
    :goto_0
    return-object v0

    .line 250606
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 250607
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 250608
    const v0, 0x49792cfc

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250609
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250610
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->f:Ljava/lang/String;

    .line 250611
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250612
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250613
    const/4 v0, 0x0

    move-object v0, v0

    .line 250614
    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 250615
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 250616
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 250617
    invoke-static {p0}, LX/1w8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 250618
    invoke-static {p0}, LX/1w8;->a(LX/16e;)Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250619
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250620
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->j:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->j:Ljava/util/List;

    .line 250621
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250622
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250623
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->k:Ljava/lang/String;

    .line 250624
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250625
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250626
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->l:Ljava/lang/String;

    .line 250627
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final w()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 250628
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 250629
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 250630
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->m:I

    return v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250631
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250632
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250633
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250634
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250635
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->o:Ljava/lang/String;

    .line 250636
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250637
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250638
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250639
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
