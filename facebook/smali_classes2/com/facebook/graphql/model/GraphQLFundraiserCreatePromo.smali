.class public final Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320838
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320837
    const-class v0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320835
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320836
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320832
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320833
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->e:Ljava/lang/String;

    .line 320834
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320829
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320830
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->f:Ljava/lang/String;

    .line 320831
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->f:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320826
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->g:Lcom/facebook/graphql/model/GraphQLCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320827
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->g:Lcom/facebook/graphql/model/GraphQLCharity;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->g:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 320828
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->g:Lcom/facebook/graphql/model/GraphQLCharity;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320823
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320824
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->h:Ljava/lang/String;

    .line 320825
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->h:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320820
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320821
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->j:Ljava/lang/String;

    .line 320822
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320839
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320840
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320841
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->k:J

    return-wide v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320817
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320818
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->l:Ljava/lang/String;

    .line 320819
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320814
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320815
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m:Ljava/lang/String;

    .line 320816
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 320793
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320794
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 320795
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 320796
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 320797
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 320798
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 320799
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 320800
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 320801
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 320802
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 320803
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 320804
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 320805
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 320806
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 320807
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 320808
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 320809
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->p()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 320810
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 320811
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 320812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320813
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320785
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320786
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320787
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCharity;

    .line 320788
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->m()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320789
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    .line 320790
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->g:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 320791
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320792
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320784
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 320781
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320782
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->k:J

    .line 320783
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320780
    const v0, -0x78228b8

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->i:Ljava/lang/String;

    .line 320779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->i:Ljava/lang/String;

    return-object v0
.end method
