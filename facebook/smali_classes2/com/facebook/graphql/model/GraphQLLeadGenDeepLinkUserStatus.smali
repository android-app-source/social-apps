.class public final Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320445
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320444
    const-class v0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320442
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320443
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 320436
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->e:Z

    .line 320437
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 320438
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 320439
    if-eqz v0, :cond_0

    .line 320440
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 320441
    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320433
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320434
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->f:Ljava/lang/String;

    .line 320435
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320430
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320431
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->g:Ljava/lang/String;

    .line 320432
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320427
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320428
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->h:Ljava/lang/String;

    .line 320429
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320446
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320447
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->i:Ljava/lang/String;

    .line 320448
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 320414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320415
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 320416
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 320417
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 320418
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 320419
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 320420
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->j()Z

    move-result v5

    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 320421
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 320422
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 320423
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 320424
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 320425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320426
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 320411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320412
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320413
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 320410
    new-instance v0, LX/4X4;

    invoke-direct {v0, p1}, LX/4X4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320409
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 320406
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320407
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->e:Z

    .line 320408
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 320400
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320401
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 320402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 320403
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 320404
    :goto_0
    return-void

    .line 320405
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 320397
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320398
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->a(Z)V

    .line 320399
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320396
    const v0, 0x2df91497

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320393
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320394
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320395
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->e:Z

    return v0
.end method
