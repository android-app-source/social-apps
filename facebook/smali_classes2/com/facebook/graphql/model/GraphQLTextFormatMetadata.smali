.class public final Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTextFormatMetadata$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTextFormatMetadata$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 263566
    const-class v0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 263565
    const-class v0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 263563
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 263564
    return-void
.end method

.method public constructor <init>(LX/4Z8;)V
    .locals 1

    .prologue
    .line 263551
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 263552
    iget-object v0, p1, LX/4Z8;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->e:Ljava/lang/String;

    .line 263553
    iget-object v0, p1, LX/4Z8;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l:Ljava/lang/String;

    .line 263554
    iget-object v0, p1, LX/4Z8;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m:Ljava/lang/String;

    .line 263555
    iget-object v0, p1, LX/4Z8;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263556
    iget-object v0, p1, LX/4Z8;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k:Ljava/lang/String;

    .line 263557
    iget-object v0, p1, LX/4Z8;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->f:Ljava/lang/String;

    .line 263558
    iget-object v0, p1, LX/4Z8;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->g:Ljava/lang/String;

    .line 263559
    iget-object v0, p1, LX/4Z8;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->h:Ljava/lang/String;

    .line 263560
    iget-object v0, p1, LX/4Z8;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n:Ljava/lang/String;

    .line 263561
    iget-object v0, p1, LX/4Z8;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->i:Ljava/lang/String;

    .line 263562
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 263524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 263525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 263526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 263527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 263528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 263529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 263530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 263531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 263532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 263533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 263534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 263535
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 263536
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 263537
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 263538
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 263539
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 263540
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 263541
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 263542
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 263543
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 263544
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 263545
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 263546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 263547
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 263516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 263517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 263519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 263520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 263521
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263522
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 263523
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263513
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->e:Ljava/lang/String;

    .line 263515
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 263512
    const v0, 0x6e7daaf3

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263548
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263549
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->f:Ljava/lang/String;

    .line 263550
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263509
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263510
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->g:Ljava/lang/String;

    .line 263511
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263506
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263507
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->h:Ljava/lang/String;

    .line 263508
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263503
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263504
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->i:Ljava/lang/String;

    .line 263505
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263500
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263501
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 263502
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263488
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263489
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k:Ljava/lang/String;

    .line 263490
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263497
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263498
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l:Ljava/lang/String;

    .line 263499
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263494
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263495
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m:Ljava/lang/String;

    .line 263496
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263491
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 263492
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n:Ljava/lang/String;

    .line 263493
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n:Ljava/lang/String;

    return-object v0
.end method
