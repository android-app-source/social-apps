.class public final Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

.field public k:J

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 200046
    const-class v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 200047
    const-class v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 200048
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 200049
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x51fc7735

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 200050
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->q:LX/0x2;

    .line 200051
    return-void
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200052
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200053
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n:Ljava/lang/String;

    .line 200054
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 200055
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200056
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200057
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->h:Ljava/lang/String;

    .line 200058
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 200112
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 200113
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 200114
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->k:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 200059
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 200060
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->q:LX/0x2;

    if-nez v0, :cond_0

    .line 200061
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->q:LX/0x2;

    .line 200062
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->q:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 200063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 200064
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    .line 200065
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 200066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 200067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 200068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 200069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 200070
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->s()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 200071
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 200072
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 200073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 200074
    const/16 v1, 0xb

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 200075
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 200076
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->k()J

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 200077
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 200078
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 200079
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 200080
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->F_()J

    move-result-wide v2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 200081
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 200082
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 200083
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 200084
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 200085
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 200086
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 200087
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 200088
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 200089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 200090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200092
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 200093
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 200094
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200095
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 200096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200097
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 200098
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 200099
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200100
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 200101
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->s()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 200102
    if-eqz v2, :cond_2

    .line 200103
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 200104
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->m:Ljava/util/List;

    move-object v1, v0

    .line 200105
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 200106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 200108
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 200109
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200110
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 200111
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200006
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 200040
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->k:J

    .line 200041
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 200042
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 200043
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->g:J

    .line 200044
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->k:J

    .line 200045
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200005
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200007
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200008
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->p:Ljava/lang/String;

    .line 200009
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200010
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200011
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 200012
    :goto_0
    return-object v0

    .line 200013
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 200014
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 200015
    const v0, -0x51fc7735

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200016
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200017
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->f:Ljava/lang/String;

    .line 200018
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 200019
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 200020
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 200021
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->g:J

    return-wide v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200022
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200023
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200024
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 200025
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200026
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    .line 200027
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 200028
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 200029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->s()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 200030
    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200031
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200032
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200033
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200034
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200035
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->m:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->m:Ljava/util/List;

    .line 200036
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200037
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200038
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 200039
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
