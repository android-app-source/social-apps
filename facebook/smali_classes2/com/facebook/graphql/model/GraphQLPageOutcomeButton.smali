.class public final Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPageOutcomeButton$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPageOutcomeButton$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320776
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320775
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320773
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320774
    return-void
.end method

.method private j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320770
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320771
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 320772
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320755
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320756
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 320757
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320767
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320768
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->g:Ljava/lang/String;

    .line 320769
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320764
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320765
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->h:Ljava/lang/String;

    .line 320766
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320761
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->i:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320762
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->i:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->i:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    .line 320763
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->i:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320758
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320759
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j:Ljava/lang/String;

    .line 320760
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 320739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320740
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 320741
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 320742
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 320743
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 320744
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 320745
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 320746
    const/4 v5, 0x2

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->k()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-ne v0, v6, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 320747
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 320748
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 320749
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->n()Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 320750
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 320751
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320752
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 320753
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->k()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    goto :goto_0

    .line 320754
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->n()Lcom/facebook/graphql/enums/GraphQLPageOutcomeButtonRenderType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320731
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320732
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320733
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 320734
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320735
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;

    .line 320736
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->e:Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    .line 320737
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320738
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320730
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPageOutcomeButton;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320729
    const v0, -0x258d3d4b

    return v0
.end method
