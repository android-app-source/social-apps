.class public final Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLContactRecommendationField$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLContactRecommendationField$Serializer;
.end annotation


# instance fields
.field public e:J

.field public f:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

.field public l:I

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320986
    const-class v0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320962
    const-class v0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320963
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320964
    return-void
.end method

.method private n()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320965
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320966
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320967
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->e:J

    return-wide v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->f:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->f:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 320970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->f:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320971
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320972
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 320973
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320974
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->h:Ljava/lang/String;

    .line 320976
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->h:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320977
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320978
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320979
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320980
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320981
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->j:Ljava/lang/String;

    .line 320982
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->j:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320983
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320984
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    .line 320985
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    return-object v0
.end method

.method private u()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320956
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320957
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m:Ljava/util/List;

    .line 320958
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320959
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320960
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320961
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320842
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320843
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320844
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320848
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320849
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r:Lcom/facebook/graphql/model/GraphQLStory;

    .line 320850
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320845
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320846
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->s:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->s:Ljava/lang/String;

    .line 320847
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->s:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 320851
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320852
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 320853
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 320854
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->q()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 320855
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 320856
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->s()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 320857
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->u()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 320858
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 320859
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 320860
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 320861
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 320862
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 320863
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->y()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 320864
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 320865
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 320866
    const/4 v3, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->n()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 320867
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 320868
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 320869
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 320870
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 320871
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 320872
    const/4 v3, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->t()Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 320873
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->j()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 320874
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 320875
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 320876
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 320877
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 320878
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 320879
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 320880
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 320881
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 320882
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320883
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 320884
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->t()Lcom/facebook/graphql/enums/GraphQLContactFieldLabelType;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320885
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320886
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320887
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 320888
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320889
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320890
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->f:Lcom/facebook/graphql/model/GraphQLActor;

    .line 320891
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 320892
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 320893
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 320894
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320895
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 320896
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 320897
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 320898
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 320899
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320900
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320901
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 320902
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->u()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 320903
    if-eqz v2, :cond_3

    .line 320904
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320905
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m:Ljava/util/List;

    move-object v1, v0

    .line 320906
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 320907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 320908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 320909
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320910
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 320911
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 320912
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 320913
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 320914
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320915
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320916
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 320917
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 320918
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 320919
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320920
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320921
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 320922
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 320923
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 320924
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320925
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->q:Lcom/facebook/graphql/model/GraphQLActor;

    .line 320926
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 320927
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 320928
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 320929
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320930
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->r:Lcom/facebook/graphql/model/GraphQLStory;

    .line 320931
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 320932
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320933
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 320934
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 320935
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320936
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320937
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    :cond_a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320938
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 320939
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320940
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->e:J

    .line 320941
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l:I

    .line 320942
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320943
    const v0, -0x7d2175f

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320944
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320945
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320946
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l:I

    return v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320947
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320948
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 320949
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->n:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320950
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->q:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320951
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->q:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->q:Lcom/facebook/graphql/model/GraphQLActor;

    .line 320952
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->q:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320953
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320954
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320955
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
