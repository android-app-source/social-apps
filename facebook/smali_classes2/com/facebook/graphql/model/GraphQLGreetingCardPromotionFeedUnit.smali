.class public final Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements LX/16e;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.implements LX/0jR;
.implements LX/16g;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I

.field public n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLGreetingCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251060
    const-class v0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 251061
    const-class v0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 251062
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 251063
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x193455fc

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 251064
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t:LX/0x2;

    .line 251065
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251066
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251067
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q:Ljava/lang/String;

    .line 251068
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 250981
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->m:I

    .line 250982
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 250983
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 250984
    if-eqz v0, :cond_0

    .line 250985
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 250986
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 251069
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->k:Ljava/lang/String;

    .line 251070
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251071
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251072
    if-eqz v0, :cond_0

    .line 251073
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 251074
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 251075
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->l:Ljava/lang/String;

    .line 251076
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 251077
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 251078
    if-eqz v0, :cond_0

    .line 251079
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 251080
    :cond_0
    return-void
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251081
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251082
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251083
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251084
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251085
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->k:Ljava/lang/String;

    .line 251086
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251087
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251088
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->l:Ljava/lang/String;

    .line 251089
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method private z()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251090
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 251091
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 251092
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->m:I

    return v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251093
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251094
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->j:Ljava/lang/String;

    .line 251095
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 251096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251097
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 251098
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->h:Ljava/lang/String;

    .line 251099
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 251100
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 251101
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 251102
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->i:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 251103
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 251104
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t:LX/0x2;

    if-nez v0, :cond_0

    .line 251105
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t:LX/0x2;

    .line 251106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 251107
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 250997
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 250998
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 250999
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 251000
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->E_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 251001
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->C_()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 251002
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->w()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 251003
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->y()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 251004
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 251005
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 251006
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 251007
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->A()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 251008
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 251009
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 251010
    const/16 v5, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 251011
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 251012
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 251013
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 251014
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 251015
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 251016
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 251017
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 251018
    const/4 v2, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->z()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 251019
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 251020
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 251021
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 251022
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 251023
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 251024
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 251025
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251026
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 251027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 251028
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251029
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251030
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 251031
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 251032
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251033
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 251034
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 251036
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 251037
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 251038
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 251039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 251040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 251041
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 251042
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 251043
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 251044
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 251045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 251046
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 251047
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 251048
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 251049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 251051
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 251052
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251053
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 251054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 251056
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 251057
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 251058
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 251059
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 250930
    new-instance v0, LX/4Wl;

    invoke-direct {v0, p1}, LX/4Wl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250931
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 250932
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->i:J

    .line 250933
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 250934
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 250935
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->i:J

    .line 250936
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->m:I

    .line 250937
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 250938
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250939
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 250940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 250941
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 250942
    :goto_0
    return-void

    .line 250943
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250944
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 250945
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 250946
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 250947
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250948
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->z()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 250949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 250950
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 250951
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 250952
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250953
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->a(Ljava/lang/String;)V

    .line 250954
    :cond_0
    :goto_0
    return-void

    .line 250955
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250956
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 250957
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250958
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250959
    new-instance v0, LX/162;

    sget-object p0, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, p0}, LX/162;-><init>(LX/0mC;)V

    .line 250960
    move-object v0, v0

    .line 250961
    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250962
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250963
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 250964
    :goto_0
    return-object v0

    .line 250965
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 250966
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 250967
    const v0, 0x193455fc

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->f:Ljava/lang/String;

    .line 250970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250971
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250972
    const/4 v0, 0x0

    move-object v0, v0

    .line 250973
    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLGreetingCard;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250974
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    .line 250976
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250977
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250978
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 250979
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 250980
    invoke-static {p0}, LX/1w8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 250987
    invoke-static {p0}, LX/1w8;->a(LX/16e;)Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250988
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250989
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250990
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250991
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250992
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 250993
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250994
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 250995
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 250996
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method
