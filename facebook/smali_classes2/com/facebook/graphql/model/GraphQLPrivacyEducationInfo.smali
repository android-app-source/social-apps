.class public final Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 330808
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 330775
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 330806
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 330807
    return-void
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330803
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 330804
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 330805
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 330789
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 330790
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 330791
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 330792
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->m()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 330793
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 330794
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 330795
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 330796
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 330797
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 330798
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 330799
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 330800
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 330801
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 330802
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 330809
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 330810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    .line 330812
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 330813
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 330814
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->e:Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    .line 330815
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 330816
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    .line 330817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 330818
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 330819
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->f:Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    .line 330820
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->m()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 330821
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->m()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 330822
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->m()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 330823
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 330824
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 330825
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 330826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    .line 330827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 330828
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 330829
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->h:Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    .line 330830
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 330831
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    .line 330832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 330833
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 330834
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->i:Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    .line 330835
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 330836
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330786
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->e:Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 330787
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->e:Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->e:Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    .line 330788
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->e:Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 330785
    const v0, 0x462969ee

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330782
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->f:Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 330783
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->f:Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->f:Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    .line 330784
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->f:Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->h:Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 330780
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->h:Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->h:Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    .line 330781
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->h:Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330776
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->i:Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 330777
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->i:Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->i:Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    .line 330778
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->i:Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    return-object v0
.end method
