.class public final Lcom/facebook/graphql/model/GraphQLOffer;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLOffer$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLOffer$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOfferView;",
            ">;"
        }
    .end annotation
.end field

.field public u:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320707
    const-class v0, Lcom/facebook/graphql/model/GraphQLOffer$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320704
    const-class v0, Lcom/facebook/graphql/model/GraphQLOffer$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320705
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320706
    return-void
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 320695
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320696
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320697
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->e:I

    return v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320708
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320709
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->f:Ljava/lang/String;

    .line 320710
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->f:Ljava/lang/String;

    return-object v0
.end method

.method private n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320711
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320712
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320713
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->g:Z

    return v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320714
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320715
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->h:Ljava/lang/String;

    .line 320716
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->h:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320717
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320718
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->i:Ljava/lang/String;

    .line 320719
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->i:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320720
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->k:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320721
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->k:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->k:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320722
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->k:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320723
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320724
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->l:Ljava/lang/String;

    .line 320725
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->l:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320726
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320727
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->m:Ljava/lang/String;

    .line 320728
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->m:Ljava/lang/String;

    return-object v0
.end method

.method private t()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 320698
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320699
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320700
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->n:J

    return-wide v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320701
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320702
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->o:Ljava/lang/String;

    .line 320703
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->o:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320614
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320615
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->p:Ljava/lang/String;

    .line 320616
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->p:Ljava/lang/String;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320617
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->q:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320618
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->q:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->q:Lcom/facebook/graphql/model/GraphQLActor;

    .line 320619
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->q:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320620
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320621
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->r:Ljava/lang/String;

    .line 320622
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->r:Ljava/lang/String;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320623
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320624
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->s:Ljava/lang/String;

    .line 320625
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->s:Ljava/lang/String;

    return-object v0
.end method

.method private z()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOfferView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320626
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320627
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->t:Ljava/util/List;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLOfferView;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->t:Ljava/util/List;

    .line 320628
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->t:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 320629
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320630
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->m()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 320631
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 320632
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->p()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 320633
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->j()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 320634
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->q()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 320635
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->r()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 320636
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->s()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 320637
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->u()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 320638
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->v()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 320639
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->w()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 320640
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->x()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 320641
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->y()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 320642
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->z()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v16

    .line 320643
    const/16 v17, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 320644
    const/16 v17, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->l()I

    move-result v18

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 320645
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 320646
    const/4 v4, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->n()Z

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 320647
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 320648
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 320649
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 320650
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 320651
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 320652
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 320653
    const/16 v5, 0x9

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->t()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 320654
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 320655
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 320656
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 320657
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 320658
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 320659
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 320660
    const/16 v4, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOffer;->k()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 320661
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320662
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320663
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320664
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->q()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320665
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->q()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320666
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->q()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320667
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOffer;

    .line 320668
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOffer;->k:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320669
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->w()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 320670
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->w()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 320671
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->w()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 320672
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOffer;

    .line 320673
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOffer;->q:Lcom/facebook/graphql/model/GraphQLActor;

    .line 320674
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->z()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 320675
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->z()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 320676
    if-eqz v2, :cond_2

    .line 320677
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOffer;

    .line 320678
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOffer;->t:Ljava/util/List;

    move-object v1, v0

    .line 320679
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320680
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320681
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOffer;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 320682
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320683
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->e:I

    .line 320684
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->g:Z

    .line 320685
    const/16 v0, 0x9

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->n:J

    .line 320686
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->u:Z

    .line 320687
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320688
    const v0, 0x4892a3c

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320689
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320690
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->j:Ljava/lang/String;

    .line 320691
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320692
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320693
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320694
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOffer;->u:Z

    return v0
.end method
