.class public final Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306175
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306174
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 306176
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306177
    return-void
.end method

.method public constructor <init>(LX/4YF;)V
    .locals 1

    .prologue
    .line 306141
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306142
    iget-object v0, p1, LX/4YF;->b:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 306143
    iget-object v0, p1, LX/4YF;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->f:Ljava/lang/String;

    .line 306144
    iget-object v0, p1, LX/4YF;->d:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    .line 306145
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 306156
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306157
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 306158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 306159
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 306160
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 306161
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 306162
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->k()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 306163
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306164
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 306165
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->k()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 306166
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 306169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 306170
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 306171
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 306172
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306173
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->e:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->e:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->e:Lcom/facebook/graphql/model/GraphQLPage;

    .line 306155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->e:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 306152
    const v0, 0x33319599

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306149
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306150
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->f:Ljava/lang/String;

    .line 306151
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 306146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306147
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    .line 306148
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;->g:Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    return-object v0
.end method
