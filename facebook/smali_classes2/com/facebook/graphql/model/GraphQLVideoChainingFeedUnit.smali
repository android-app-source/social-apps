.class public final Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16m;
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/17w;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293583
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 293584
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 293585
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 293586
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x542975e

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 293587
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r:LX/0x2;

    .line 293588
    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293589
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293590
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->i:Ljava/lang/String;

    .line 293591
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293592
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293593
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->k:Ljava/lang/String;

    .line 293594
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293595
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293596
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293597
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293612
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293613
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293614
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293598
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293599
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->p:Ljava/lang/String;

    .line 293600
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 293601
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293602
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293603
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->g:Ljava/lang/String;

    .line 293604
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 293605
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 293606
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 293607
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 293608
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 293609
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r:LX/0x2;

    if-nez v0, :cond_0

    .line 293610
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r:LX/0x2;

    .line 293611
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 293554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 293555
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 293556
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293557
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 293558
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 293559
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 293560
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 293561
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->u()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 293562
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 293563
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 293564
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 293565
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 293566
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->x()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 293567
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 293568
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 293569
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 293570
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 293571
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 293572
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 293573
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 293574
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 293575
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 293576
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 293577
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 293578
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 293579
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 293580
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 293581
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293582
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 293495
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 293496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 293498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 293499
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    .line 293500
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 293501
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 293502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    .line 293503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 293504
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    .line 293505
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    .line 293506
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 293507
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293508
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 293509
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    .line 293510
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293511
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 293512
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293513
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 293514
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    .line 293515
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293516
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 293517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 293519
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;

    .line 293520
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293521
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 293522
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293523
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 293524
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->h:J

    .line 293525
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 293526
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 293527
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->h:J

    .line 293528
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293529
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293530
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293531
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o:Ljava/lang/String;

    .line 293532
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 293535
    :goto_0
    return-object v0

    .line 293536
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 293537
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 293538
    const v0, 0x542975e

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293539
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293540
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->f:Ljava/lang/String;

    .line 293541
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 293542
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293543
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293544
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLVideo;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 293545
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->j:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293546
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293547
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    .line 293548
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 293549
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Ljava/util/List;
    .locals 1

    .prologue
    .line 293550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 293551
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 293552
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 293553
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
