.class public final Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:J

.field public g:Z

.field private h:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255564
    const-class v0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255563
    const-class v0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 255559
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 255560
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x735b1b91

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 255561
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->h:LX/0x2;

    .line 255562
    return-void
.end method

.method public constructor <init>(LX/4WP;)V
    .locals 2

    .prologue
    .line 255552
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 255553
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x735b1b91

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 255554
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->h:LX/0x2;

    .line 255555
    iget-wide v0, p1, LX/4WP;->b:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->f:J

    .line 255556
    iget-boolean v0, p1, LX/4WP;->c:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->g:Z

    .line 255557
    iget-object v0, p1, LX/4WP;->d:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->h:LX/0x2;

    .line 255558
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 255551
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255550
    const/4 v0, 0x0

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 255547
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 255548
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 255549
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->f:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 255518
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->h:LX/0x2;

    if-nez v0, :cond_0

    .line 255519
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->h:LX/0x2;

    .line 255520
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->h:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 255541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255542
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 255543
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 255544
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->k()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 255545
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255546
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 255538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255540
    return-object p0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 255536
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->f:J

    .line 255537
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 255532
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 255533
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->f:J

    .line 255534
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->g:Z

    .line 255535
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 255528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 255529
    :goto_0
    return-object v0

    .line 255530
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 255531
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 255526
    const v0, -0x735b1b91

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255524
    const/4 v0, 0x0

    move-object v0, v0

    .line 255525
    return-object v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 255521
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 255522
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 255523
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->g:Z

    return v0
.end method
