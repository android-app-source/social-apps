.class public final Lcom/facebook/graphql/model/GraphQLOpenGraphObject;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLOpenGraphObject$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLOpenGraphObject$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAudio;",
            ">;"
        }
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Z

.field public M:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation
.end field

.field public u:I

.field public v:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphObject;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325158
    const-class v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325159
    const-class v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 325160
    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325161
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325165
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325166
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325167
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->r:Z

    return v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->s:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->s:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->s:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 325170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->s:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method private D()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325171
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325172
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t:Ljava/util/List;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t:Ljava/util/List;

    .line 325173
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private E()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 325174
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325175
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325176
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->u:I

    return v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325177
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325178
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 325179
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325180
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325181
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 325182
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325183
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325184
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325185
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private I()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325186
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325187
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->B:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->B:Ljava/lang/String;

    .line 325188
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->B:Ljava/lang/String;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325189
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325190
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 325191
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325195
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->E:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325196
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->E:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325197
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->E:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325192
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325193
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325194
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325225
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325226
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325227
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private N()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325222
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325223
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325224
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private O()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325219
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->I:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325220
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->I:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325221
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->I:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private P()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325216
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->J:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325217
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->J:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->J:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 325218
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->J:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private Q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325213
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325214
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325215
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private R()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325210
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325211
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325212
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->L:Z

    return v0
.end method

.method private S()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325207
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325208
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 325209
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    return-object v0
.end method

.method private T()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325204
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325205
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 325206
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private U()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325201
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325202
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325203
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private V()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325198
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325199
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 325200
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    return-object v0
.end method

.method private W()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325155
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325156
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 325157
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method private X()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325152
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325153
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S:Ljava/util/List;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S:Ljava/util/List;

    .line 325154
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private Y()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325110
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325111
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T:Ljava/util/List;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T:Ljava/util/List;

    .line 325112
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private Z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325107
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325108
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325109
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aa()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325104
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325105
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325106
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325101
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325102
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325103
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324853
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324854
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->i:Ljava/lang/String;

    .line 324855
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->i:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325098
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325099
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->j:Ljava/lang/String;

    .line 325100
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->j:Ljava/lang/String;

    return-object v0
.end method

.method private w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325095
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325096
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325097
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k:J

    return-wide v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325092
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->l:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325093
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->l:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325094
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->l:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325089
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325090
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 325091
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325086
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325087
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 325088
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 38

    .prologue
    .line 325000
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325001
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->j()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 325002
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 325003
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 325004
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 325005
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->v()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 325006
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 325007
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 325008
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 325009
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 325010
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 325011
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 325012
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v14

    .line 325013
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v15

    .line 325014
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 325015
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 325016
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 325017
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 325018
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->I()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 325019
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->r()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v21

    .line 325020
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 325021
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 325022
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 325023
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 325024
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 325025
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 325026
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 325027
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 325028
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 325029
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 325030
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 325031
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->s()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 325032
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->X()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v34

    .line 325033
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Y()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v35

    .line 325034
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 325035
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 325036
    const/16 v7, 0x30

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 325037
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 325038
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 325039
    const/4 v2, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->l()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 325040
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 325041
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 325042
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 325043
    const/4 v3, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->w()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 325044
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 325045
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 325046
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 325047
    const/16 v2, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->m()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 325048
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 325049
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 325050
    const/16 v2, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->B()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 325051
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 325052
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 325053
    const/16 v2, 0x12

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->E()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 325054
    const/16 v3, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->o()Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 325055
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 325056
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325057
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325058
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325059
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325060
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325061
    const/16 v3, 0x1a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->J()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 325062
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325063
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325064
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325065
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325066
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325067
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325068
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325069
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325070
    const/16 v2, 0x23

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->R()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 325071
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325072
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325073
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325074
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325075
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325076
    const/16 v3, 0x29

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->W()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 325077
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325078
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325079
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325080
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 325081
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325082
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 325083
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->o()Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v2

    goto/16 :goto_0

    .line 325084
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->J()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    goto/16 :goto_1

    .line 325085
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->W()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 324857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324858
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324859
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 324860
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 324861
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324862
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 324863
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 324864
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324865
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 324866
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324867
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324868
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 324869
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324870
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 324871
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324872
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->l:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324873
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 324874
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 324875
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 324876
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324877
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->m:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 324878
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 324879
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324880
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 324881
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324882
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324883
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 324884
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324885
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 324886
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324887
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324888
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 324889
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 324890
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->C()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 324891
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324892
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->s:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 324893
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 324894
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 324895
    if-eqz v2, :cond_7

    .line 324896
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324897
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->t:Ljava/util/List;

    move-object v1, v0

    .line 324898
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 324899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 324900
    if-eqz v2, :cond_8

    .line 324901
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324902
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->w:Ljava/util/List;

    move-object v1, v0

    .line 324903
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 324904
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 324905
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 324906
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324907
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->y:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 324908
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 324909
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 324910
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 324911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324912
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->z:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 324913
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 324914
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324915
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 324916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324917
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324918
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 324919
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->r()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 324920
    if-eqz v2, :cond_c

    .line 324921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324922
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D:Ljava/util/List;

    move-object v1, v0

    .line 324923
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 324924
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324925
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 324926
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324927
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324928
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 324929
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324930
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 324931
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324932
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324933
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 324934
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324935
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 324936
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324937
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324938
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 324939
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324940
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 324941
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324942
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324943
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 324944
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324945
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 324946
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324947
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324948
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 324949
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324950
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 324951
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324952
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324953
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 324954
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 324955
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 324956
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324957
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->J:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 324958
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 324959
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324960
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 324961
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324962
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324963
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 324964
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324965
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 324966
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324967
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324968
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 324969
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 324970
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 324971
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324972
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->M:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 324973
    :cond_16
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 324974
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 324975
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 324976
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324977
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->N:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 324978
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 324979
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 324980
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 324981
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324982
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 324983
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 324984
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 324985
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->V()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 324986
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324987
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->P:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 324988
    :cond_19
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->X()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 324989
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->X()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 324990
    if-eqz v2, :cond_1a

    .line 324991
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324992
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->S:Ljava/util/List;

    move-object v1, v0

    .line 324993
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 324994
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 324995
    if-eqz v2, :cond_1b

    .line 324996
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 324997
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->T:Ljava/util/List;

    move-object v1, v0

    .line 324998
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324999
    if-nez v1, :cond_1c

    :goto_0
    return-object p0

    :cond_1c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 325113
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 325114
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->g:Z

    .line 325115
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k:J

    .line 325116
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->o:Z

    .line 325117
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->r:Z

    .line 325118
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->u:I

    .line 325119
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->L:Z

    .line 325120
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 325121
    const v0, -0x4dba1a9d

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325122
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325123
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->e:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->e:Ljava/util/List;

    .line 325124
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325125
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325126
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 325127
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325128
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325129
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325130
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->g:Z

    return v0
.end method

.method public final m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 325131
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325132
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325133
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->o:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325134
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325135
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p:Ljava/lang/String;

    .line 325136
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLMusicType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325137
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->v:Lcom/facebook/graphql/enums/GraphQLMusicType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325138
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->v:Lcom/facebook/graphql/enums/GraphQLMusicType;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMusicType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->v:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 325139
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->v:Lcom/facebook/graphql/enums/GraphQLMusicType;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphObject;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 325140
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->w:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325141
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->w:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->w:Ljava/util/List;

    .line 325142
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->w:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325143
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325144
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x:Ljava/lang/String;

    .line 325145
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAudio;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325146
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325147
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D:Ljava/util/List;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLAudio;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D:Ljava/util/List;

    .line 325148
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->D:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325149
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325150
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q:Ljava/lang/String;

    .line 325151
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->Q:Ljava/lang/String;

    return-object v0
.end method
