.class public final Lcom/facebook/graphql/model/GraphQLGroup;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLGroup$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLGroup$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public D:Z

.field public E:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:Z

.field public L:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

.field public M:J

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

.field public S:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public U:I

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Z

.field public aB:I

.field public aC:I

.field public aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

.field public aH:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;",
            ">;"
        }
    .end annotation
.end field

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public aK:J

.field public aL:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

.field public aM:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

.field public aN:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public aO:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

.field public aP:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

.field public aQ:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

.field public aV:Z

.field public aW:J

.field public aX:Z

.field public aY:Z

.field public aZ:Z

.field public aa:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Z

.field public al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public ao:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:I

.field public ar:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Z

.field public at:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Z

.field public av:Z

.field public aw:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Z

.field public ay:Z

.field public az:Z

.field public ba:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Z

.field public bc:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public bd:Z

.field public be:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:Z

.field public bg:Z

.field public bh:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:Z

.field public bj:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Z

.field public bl:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bm:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bq:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public br:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bv:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupPurpose;",
            ">;"
        }
    .end annotation
.end field

.field public bw:Lcom/facebook/graphql/model/GraphQLGroupPurpose;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Z

.field public e:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:J

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 304558
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroup$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 304559
    const-class v0, Lcom/facebook/graphql/model/GraphQLGroup$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 304560
    const/16 v0, 0x86

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 304561
    return-void
.end method

.method public constructor <init>(LX/4Wm;)V
    .locals 2

    .prologue
    .line 304562
    const/16 v0, 0x86

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 304563
    iget-object v0, p1, LX/4Wm;->b:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304564
    iget-object v0, p1, LX/4Wm;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304565
    iget-wide v0, p1, LX/4Wm;->d:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aW:J

    .line 304566
    iget-object v0, p1, LX/4Wm;->e:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304567
    iget-object v0, p1, LX/4Wm;->f:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304568
    iget-boolean v0, p1, LX/4Wm;->g:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->h:Z

    .line 304569
    iget-boolean v0, p1, LX/4Wm;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->i:Z

    .line 304570
    iget-boolean v0, p1, LX/4Wm;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aX:Z

    .line 304571
    iget-boolean v0, p1, LX/4Wm;->j:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->j:Z

    .line 304572
    iget-boolean v0, p1, LX/4Wm;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bi:Z

    .line 304573
    iget-boolean v0, p1, LX/4Wm;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->k:Z

    .line 304574
    iget-boolean v0, p1, LX/4Wm;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bf:Z

    .line 304575
    iget-boolean v0, p1, LX/4Wm;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->l:Z

    .line 304576
    iget-boolean v0, p1, LX/4Wm;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aY:Z

    .line 304577
    iget-boolean v0, p1, LX/4Wm;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aV:Z

    .line 304578
    iget-boolean v0, p1, LX/4Wm;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bk:Z

    .line 304579
    iget-boolean v0, p1, LX/4Wm;->r:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bg:Z

    .line 304580
    iget-object v0, p1, LX/4Wm;->s:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bj:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304581
    iget-object v0, p1, LX/4Wm;->t:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 304582
    iget-object v0, p1, LX/4Wm;->u:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aT:Lcom/facebook/graphql/model/GraphQLPage;

    .line 304583
    iget-object v0, p1, LX/4Wm;->v:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304584
    iget-object v0, p1, LX/4Wm;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->o:Ljava/lang/String;

    .line 304585
    iget-wide v0, p1, LX/4Wm;->x:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->p:J

    .line 304586
    iget-object v0, p1, LX/4Wm;->y:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304587
    iget-object v0, p1, LX/4Wm;->z:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bl:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304588
    iget-object v0, p1, LX/4Wm;->A:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bm:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304589
    iget-object v0, p1, LX/4Wm;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->r:Ljava/lang/String;

    .line 304590
    iget-object v0, p1, LX/4Wm;->C:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->s:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304591
    iget-object v0, p1, LX/4Wm;->D:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->t:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 304592
    iget-object v0, p1, LX/4Wm;->E:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->u:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304593
    iget-object v0, p1, LX/4Wm;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->v:Ljava/lang/String;

    .line 304594
    iget-object v0, p1, LX/4Wm;->G:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304595
    iget-object v0, p1, LX/4Wm;->H:Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->x:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 304596
    iget-object v0, p1, LX/4Wm;->I:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bn:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304597
    iget-object v0, p1, LX/4Wm;->J:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->y:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 304598
    iget v0, p1, LX/4Wm;->K:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->z:I

    .line 304599
    iget-object v0, p1, LX/4Wm;->L:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 304600
    iget-object v0, p1, LX/4Wm;->M:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304601
    iget-object v0, p1, LX/4Wm;->N:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->br:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 304602
    iget-object v0, p1, LX/4Wm;->O:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->C:Ljava/util/List;

    .line 304603
    iget-boolean v0, p1, LX/4Wm;->P:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->D:Z

    .line 304604
    iget-boolean v0, p1, LX/4Wm;->Q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->E:Z

    .line 304605
    iget-object v0, p1, LX/4Wm;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->F:Ljava/lang/String;

    .line 304606
    iget-object v0, p1, LX/4Wm;->S:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304607
    iget-object v0, p1, LX/4Wm;->T:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bo:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304608
    iget-boolean v0, p1, LX/4Wm;->U:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bd:Z

    .line 304609
    iget-boolean v0, p1, LX/4Wm;->V:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aZ:Z

    .line 304610
    iget-boolean v0, p1, LX/4Wm;->W:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->H:Z

    .line 304611
    iget-boolean v0, p1, LX/4Wm;->X:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->I:Z

    .line 304612
    iget-boolean v0, p1, LX/4Wm;->Y:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->J:Z

    .line 304613
    iget-boolean v0, p1, LX/4Wm;->Z:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->K:Z

    .line 304614
    iget-object v0, p1, LX/4Wm;->aa:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->L:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 304615
    iget-wide v0, p1, LX/4Wm;->ab:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->M:J

    .line 304616
    iget-object v0, p1, LX/4Wm;->ac:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->N:Ljava/lang/String;

    .line 304617
    iget-object v0, p1, LX/4Wm;->ad:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->O:Ljava/util/List;

    .line 304618
    iget-object v0, p1, LX/4Wm;->ae:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bp:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304619
    iget-object v0, p1, LX/4Wm;->af:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->P:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304620
    iget-object v0, p1, LX/4Wm;->ag:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304621
    iget-object v0, p1, LX/4Wm;->ah:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bv:Ljava/util/List;

    .line 304622
    iget-object v0, p1, LX/4Wm;->ai:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->R:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 304623
    iget-object v0, p1, LX/4Wm;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->S:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 304624
    iget-object v0, p1, LX/4Wm;->ak:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304625
    iget-object v0, p1, LX/4Wm;->al:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 304626
    iget v0, p1, LX/4Wm;->am:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->U:I

    .line 304627
    iget-object v0, p1, LX/4Wm;->an:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304628
    iget-object v0, p1, LX/4Wm;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304629
    iget-object v0, p1, LX/4Wm;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->be:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304630
    iget-object v0, p1, LX/4Wm;->aq:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304631
    iget-object v0, p1, LX/4Wm;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304632
    iget-object v0, p1, LX/4Wm;->as:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304633
    iget-object v0, p1, LX/4Wm;->at:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aa:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304634
    iget-object v0, p1, LX/4Wm;->au:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304635
    iget-object v0, p1, LX/4Wm;->av:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304636
    iget-object v0, p1, LX/4Wm;->aw:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304637
    iget-object v0, p1, LX/4Wm;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304638
    iget-boolean v0, p1, LX/4Wm;->ay:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->af:Z

    .line 304639
    iget-object v0, p1, LX/4Wm;->az:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304640
    iget-boolean v0, p1, LX/4Wm;->aA:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ag:Z

    .line 304641
    iget-boolean v0, p1, LX/4Wm;->aB:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ah:Z

    .line 304642
    iget-boolean v0, p1, LX/4Wm;->aC:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ai:Z

    .line 304643
    iget-object v0, p1, LX/4Wm;->aD:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aj:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304644
    iget-boolean v0, p1, LX/4Wm;->aE:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ak:Z

    .line 304645
    iget-boolean v0, p1, LX/4Wm;->aF:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->by:Z

    .line 304646
    iget-object v0, p1, LX/4Wm;->aG:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304647
    iget-object v0, p1, LX/4Wm;->aH:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->am:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 304648
    iget-object v0, p1, LX/4Wm;->aI:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bt:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304649
    iget-object v0, p1, LX/4Wm;->aJ:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->an:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 304650
    iget-object v0, p1, LX/4Wm;->aK:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bw:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 304651
    iget-object v0, p1, LX/4Wm;->aL:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304652
    iget-object v0, p1, LX/4Wm;->aM:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ap:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 304653
    iget v0, p1, LX/4Wm;->aN:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aq:I

    .line 304654
    iget-object v0, p1, LX/4Wm;->aO:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ar:Ljava/lang/String;

    .line 304655
    iget-boolean v0, p1, LX/4Wm;->aP:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->as:Z

    .line 304656
    iget-object v0, p1, LX/4Wm;->aQ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->at:Ljava/lang/String;

    .line 304657
    iget-boolean v0, p1, LX/4Wm;->aR:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->au:Z

    .line 304658
    iget-boolean v0, p1, LX/4Wm;->aS:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->av:Z

    .line 304659
    iget-object v0, p1, LX/4Wm;->aT:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bs:Lcom/facebook/graphql/model/GraphQLNode;

    .line 304660
    iget-object v0, p1, LX/4Wm;->aU:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aw:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 304661
    iget-boolean v0, p1, LX/4Wm;->aV:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ax:Z

    .line 304662
    iget-boolean v0, p1, LX/4Wm;->aW:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ay:Z

    .line 304663
    iget-boolean v0, p1, LX/4Wm;->aX:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->az:Z

    .line 304664
    iget-boolean v0, p1, LX/4Wm;->aY:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aA:Z

    .line 304665
    iget v0, p1, LX/4Wm;->aZ:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aB:I

    .line 304666
    iget v0, p1, LX/4Wm;->ba:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aC:I

    .line 304667
    iget-object v0, p1, LX/4Wm;->bb:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304668
    iget-object v0, p1, LX/4Wm;->bc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304669
    iget-object v0, p1, LX/4Wm;->bd:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    .line 304670
    iget-object v0, p1, LX/4Wm;->be:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aG:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 304671
    iget-object v0, p1, LX/4Wm;->bf:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aH:Ljava/util/List;

    .line 304672
    iget-object v0, p1, LX/4Wm;->bg:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ba:Ljava/lang/String;

    .line 304673
    iget-boolean v0, p1, LX/4Wm;->bh:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bb:Z

    .line 304674
    iget-object v0, p1, LX/4Wm;->bi:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aI:Ljava/lang/String;

    .line 304675
    iget-object v0, p1, LX/4Wm;->bj:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bc:Ljava/util/List;

    .line 304676
    iget-object v0, p1, LX/4Wm;->bk:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aJ:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 304677
    iget-wide v0, p1, LX/4Wm;->bl:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aK:J

    .line 304678
    iget-object v0, p1, LX/4Wm;->bm:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aL:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    .line 304679
    iget-object v0, p1, LX/4Wm;->bn:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aU:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    .line 304680
    iget-object v0, p1, LX/4Wm;->bo:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aM:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 304681
    iget-object v0, p1, LX/4Wm;->bp:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aN:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 304682
    iget-object v0, p1, LX/4Wm;->bq:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aO:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 304683
    iget-object v0, p1, LX/4Wm;->br:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aP:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 304684
    iget-object v0, p1, LX/4Wm;->bs:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aQ:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 304685
    iget-object v0, p1, LX/4Wm;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304686
    iget-object v0, p1, LX/4Wm;->bu:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bq:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304687
    iget-object v0, p1, LX/4Wm;->bv:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304688
    return-void
.end method

.method private V()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304689
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304690
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304691
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method private W()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304692
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304693
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304694
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private X()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304695
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304696
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304697
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private Y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304698
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304699
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304700
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->h:Z

    return v0
.end method

.method private Z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304701
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304702
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304703
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->i:Z

    return v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 304552
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aq:I

    .line 304553
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304554
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304555
    if-eqz v0, :cond_0

    .line 304556
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x45

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 304557
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 304707
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aW:J

    .line 304708
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304709
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304710
    if-eqz v0, :cond_0

    .line 304711
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 304712
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V
    .locals 3

    .prologue
    .line 304713
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->L:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 304714
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304715
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304716
    if-eqz v0, :cond_0

    .line 304717
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 304718
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 3

    .prologue
    .line 304719
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aJ:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 304720
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304721
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304722
    if-eqz v0, :cond_0

    .line 304723
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x58

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 304724
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V
    .locals 3

    .prologue
    .line 304725
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->R:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 304726
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304727
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304728
    if-eqz v0, :cond_0

    .line 304729
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 304730
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V
    .locals 3

    .prologue
    .line 304731
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aN:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 304732
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304733
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304734
    if-eqz v0, :cond_0

    .line 304735
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 304736
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V
    .locals 3

    .prologue
    .line 304737
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aP:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 304738
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304739
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304740
    if-eqz v0, :cond_0

    .line 304741
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 304742
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 3

    .prologue
    .line 304743
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aQ:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 304744
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304745
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304746
    if-eqz v0, :cond_0

    .line 304747
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 304748
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 3

    .prologue
    .line 304749
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->an:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 304750
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304751
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304752
    if-eqz v0, :cond_0

    .line 304753
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 304754
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 304519
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->o:Ljava/lang/String;

    .line 304520
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304521
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304522
    if-eqz v0, :cond_0

    .line 304523
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 304524
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 304489
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->j:Z

    .line 304490
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304491
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304492
    if-eqz v0, :cond_0

    .line 304493
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 304494
    :cond_0
    return-void
.end method

.method private aA()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304495
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304496
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304497
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aB()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304498
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304499
    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304500
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ag:Z

    return v0
.end method

.method private aC()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304501
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304502
    const/4 v0, 0x7

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304503
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ah:Z

    return v0
.end method

.method private aD()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304504
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304505
    const/4 v0, 0x7

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304506
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ai:Z

    return v0
.end method

.method private aE()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304507
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aj:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304508
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aj:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aj:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304509
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aj:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private aF()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304510
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304511
    const/4 v0, 0x7

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304512
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ak:Z

    return v0
.end method

.method private aG()Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304513
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ap:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ap:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ap:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 304515
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ap:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    return-object v0
.end method

.method private aH()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304516
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304517
    const/16 v0, 0x8

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304518
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->as:Z

    return v0
.end method

.method private aI()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304486
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304487
    const/16 v0, 0x9

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304488
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->au:Z

    return v0
.end method

.method private aJ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304525
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304526
    const/16 v0, 0x9

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304527
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->av:Z

    return v0
.end method

.method private aK()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304528
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aw:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304529
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aw:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aw:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 304530
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aw:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private aL()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304531
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304532
    const/16 v0, 0x9

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304533
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ax:Z

    return v0
.end method

.method private aM()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304534
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304535
    const/16 v0, 0x9

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304536
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ay:Z

    return v0
.end method

.method private aN()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304537
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304538
    const/16 v0, 0x9

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304539
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->az:Z

    return v0
.end method

.method private aO()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304540
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304541
    const/16 v0, 0x9

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304542
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aA:Z

    return v0
.end method

.method private aP()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304543
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304544
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304545
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aB:I

    return v0
.end method

.method private aQ()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303904
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303905
    const/16 v0, 0xa

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303906
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aC:I

    return v0
.end method

.method private aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304546
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304547
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304548
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304549
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304550
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x53

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304551
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private aT()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304818
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304819
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    .line 304820
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private aU()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304785
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aG:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304786
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aG:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/16 v1, 0x55

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aG:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 304787
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aG:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method private aV()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304824
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aH:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304825
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aH:Ljava/util/List;

    const/16 v1, 0x56

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aH:Ljava/util/List;

    .line 304826
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aH:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private aW()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304827
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304828
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aI:Ljava/lang/String;

    const/16 v1, 0x57

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aI:Ljava/lang/String;

    .line 304829
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aI:Ljava/lang/String;

    return-object v0
.end method

.method private aX()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304830
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304831
    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304832
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aK:J

    return-wide v0
.end method

.method private aY()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aL:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aL:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aL:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    .line 304835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aL:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    return-object v0
.end method

.method private aZ()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304836
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aM:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304837
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aM:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const/16 v1, 0x5b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aM:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 304838
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aM:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method

.method private aa()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304839
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304840
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304841
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->j:Z

    return v0
.end method

.method private ab()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304842
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304843
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304844
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->p:J

    return-wide v0
.end method

.method private ac()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304845
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304846
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->r:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->r:Ljava/lang/String;

    .line 304847
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->r:Ljava/lang/String;

    return-object v0
.end method

.method private ad()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304848
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->s:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304849
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->s:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->s:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304850
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->s:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private ae()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304851
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->u:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304852
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->u:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->u:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304853
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->u:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private af()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304854
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304855
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->v:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->v:Ljava/lang/String;

    .line 304856
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->v:Ljava/lang/String;

    return-object v0
.end method

.method private ag()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304857
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->w:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304858
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->w:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304859
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->w:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304860
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304861
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 304862
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    return-object v0
.end method

.method private ai()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304863
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->B:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304864
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->B:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304865
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->B:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aj()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304866
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->C:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304867
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->C:Ljava/util/List;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->C:Ljava/util/List;

    .line 304868
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->C:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ak()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304869
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304870
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304871
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->D:Z

    return v0
.end method

.method private al()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 304821
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304822
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304823
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->E:Z

    return v0
.end method

.method private am()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304872
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304873
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304874
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private an()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304758
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304759
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304760
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->H:Z

    return v0
.end method

.method private ao()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304761
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304762
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304763
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->J:Z

    return v0
.end method

.method private ap()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304764
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304765
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304766
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->K:Z

    return v0
.end method

.method private aq()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304767
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->L:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304768
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->L:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->L:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 304769
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->L:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    return-object v0
.end method

.method private ar()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304770
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304771
    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304772
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->M:J

    return-wide v0
.end method

.method private as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304773
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304774
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304775
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method private at()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304776
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->R:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304777
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->R:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->R:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 304778
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->R:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    return-object v0
.end method

.method private au()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304780
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 304781
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method private av()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 304782
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304783
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304784
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->U:I

    return v0
.end method

.method private aw()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304755
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304756
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304757
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ax()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304788
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aa:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304789
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aa:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aa:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304790
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aa:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private ay()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304791
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304792
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304793
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private az()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304794
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304795
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304796
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 304797
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->N:Ljava/lang/String;

    .line 304798
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304799
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304800
    if-eqz v0, :cond_0

    .line 304801
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x26

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 304802
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 304803
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->D:Z

    .line 304804
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 304805
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 304806
    if-eqz v0, :cond_0

    .line 304807
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 304808
    :cond_0
    return-void
.end method

.method private bA()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupPurpose;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304809
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bv:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304810
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bv:Ljava/util/List;

    const/16 v1, 0x81

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bv:Ljava/util/List;

    .line 304811
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bv:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bB()Lcom/facebook/graphql/model/GraphQLGroupPurpose;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304812
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bw:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304813
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bw:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    const/16 v1, 0x82

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bw:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 304814
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bw:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    return-object v0
.end method

.method private bC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304704
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 304705
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x83

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304706
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bD()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 304815
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 304816
    const/16 v0, 0x10

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 304817
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->by:Z

    return v0
.end method

.method private ba()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303653
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aN:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303654
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aN:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const/16 v1, 0x5c

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aN:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 303655
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aN:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    return-object v0
.end method

.method private bb()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303659
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aO:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303660
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aO:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    const/16 v1, 0x5d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aO:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 303661
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aO:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    return-object v0
.end method

.method private bc()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303662
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aP:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303663
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aP:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aP:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 303664
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aP:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    return-object v0
.end method

.method private bd()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303665
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aQ:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303666
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aQ:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aQ:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 303667
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aQ:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method private be()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303668
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303669
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303670
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bf()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aT:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aT:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x62

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aT:Lcom/facebook/graphql/model/GraphQLPage;

    .line 303673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aT:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method private bg()Lcom/facebook/graphql/enums/GraphQLGroupPendingState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303674
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aU:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303675
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aU:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    const/16 v1, 0x63

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPendingState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aU:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    .line 303676
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aU:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    return-object v0
.end method

.method private bh()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303677
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303678
    const/16 v0, 0xc

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303679
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aV:Z

    return v0
.end method

.method private bi()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303680
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303681
    const/16 v0, 0xc

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303682
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aW:J

    return-wide v0
.end method

.method private bj()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303683
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303684
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303685
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aX:Z

    return v0
.end method

.method private bk()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303656
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303657
    const/16 v0, 0xd

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303658
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aY:Z

    return v0
.end method

.method private bl()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303689
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ba:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303690
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ba:Ljava/lang/String;

    const/16 v1, 0x6c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ba:Ljava/lang/String;

    .line 303691
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ba:Ljava/lang/String;

    return-object v0
.end method

.method private bm()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303692
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303693
    const/16 v0, 0xd

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303694
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bb:Z

    return v0
.end method

.method private bn()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303695
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bc:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303696
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bc:Ljava/util/List;

    const/16 v1, 0x6e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bc:Ljava/util/List;

    .line 303697
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bc:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bo()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303698
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303699
    const/16 v0, 0xe

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303700
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bf:Z

    return v0
.end method

.method private bp()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303701
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303702
    const/16 v0, 0xe

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303703
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bg:Z

    return v0
.end method

.method private bq()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303704
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303705
    const/16 v0, 0xe

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303706
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bi:Z

    return v0
.end method

.method private br()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303707
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bj:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303708
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bj:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x75

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bj:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 303709
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bj:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method private bs()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303710
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bl:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303711
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bl:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x77

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bl:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 303712
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bl:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method private bt()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303686
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bm:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303687
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bm:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    const/16 v1, 0x78

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bm:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 303688
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bm:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    return-object v0
.end method

.method private bu()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303587
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bo:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303588
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bo:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    const/16 v1, 0x7a

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bo:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 303589
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bo:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    return-object v0
.end method

.method private bv()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303593
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bp:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303594
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bp:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    const/16 v1, 0x7b

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bp:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 303595
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bp:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    return-object v0
.end method

.method private bw()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303596
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bq:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303597
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bq:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    const/16 v1, 0x7c

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bq:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 303598
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bq:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    return-object v0
.end method

.method private bx()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303599
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bs:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303600
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bs:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0x7e

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bs:Lcom/facebook/graphql/model/GraphQLNode;

    .line 303601
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bs:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method private by()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303602
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bt:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303603
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bt:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x7f

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bt:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 303604
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bt:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method private bz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303605
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303606
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x80

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 303607
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 303608
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ag:Z

    .line 303609
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 303610
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 303611
    if-eqz v0, :cond_0

    .line 303612
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 303613
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 303614
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ah:Z

    .line 303615
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 303616
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 303617
    if-eqz v0, :cond_0

    .line 303618
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 303619
    :cond_0
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 303620
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ay:Z

    .line 303621
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 303622
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 303623
    if-eqz v0, :cond_0

    .line 303624
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x4d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 303625
    :cond_0
    return-void
.end method

.method private f(Z)V
    .locals 3

    .prologue
    .line 303626
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLGroup;->az:Z

    .line 303627
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 303628
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 303629
    if-eqz v0, :cond_0

    .line 303630
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x4e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 303631
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303632
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->W:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303633
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->W:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303634
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->W:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303635
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->X:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303636
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->X:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303637
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->X:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final C()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303638
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303639
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303640
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303641
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303642
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303643
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final E()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303644
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303645
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303646
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->af:Z

    return v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303647
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303648
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 303649
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303650
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->am:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303651
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->am:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->am:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 303652
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->am:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303590
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->an:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303591
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->an:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->an:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 303592
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->an:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final J()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303901
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303902
    const/16 v0, 0x8

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303903
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aq:I

    return v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303907
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ar:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303908
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ar:Ljava/lang/String;

    const/16 v1, 0x46

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ar:Ljava/lang/String;

    .line 303909
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ar:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303910
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->at:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303911
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->at:Ljava/lang/String;

    const/16 v1, 0x48

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->at:Ljava/lang/String;

    .line 303912
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->at:Ljava/lang/String;

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303913
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aJ:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303914
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aJ:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x58

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aJ:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 303915
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aJ:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303916
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303917
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 303918
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final O()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303919
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303920
    const/16 v0, 0xd

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303921
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aZ:Z

    return v0
.end method

.method public final P()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303922
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303923
    const/16 v0, 0xd

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303924
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bd:Z

    return v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303925
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->be:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303926
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->be:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x70

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->be:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303927
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->be:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303898
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303899
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303900
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final S()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303931
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303932
    const/16 v0, 0xe

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303933
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bk:Z

    return v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303934
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bn:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303935
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bn:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    const/16 v1, 0x79

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bn:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 303936
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bn:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303937
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->br:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303938
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->br:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    const/16 v1, 0x7d

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->br:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 303939
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->br:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 71

    .prologue
    .line 303940
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 303941
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->V()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 303942
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->W()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 303943
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 303944
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 303945
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->n()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 303946
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 303947
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ac()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 303948
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ad()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 303949
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 303950
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ae()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 303951
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->af()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 303952
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 303953
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 303954
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 303955
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 303956
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 303957
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aj()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v19

    .line 303958
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 303959
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 303960
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 303961
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->w()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v23

    .line 303962
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->x()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 303963
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 303964
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->y()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 303965
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 303966
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 303967
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 303968
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 303969
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 303970
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ax()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 303971
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 303972
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->az()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 303973
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 303974
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 303975
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aE()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 303976
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 303977
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->G()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 303978
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 303979
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aG()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 303980
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->K()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v42

    .line 303981
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->L()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 303982
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aK()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 303983
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 303984
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 303985
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aT()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 303986
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aV()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->d(Ljava/util/List;)I

    move-result v48

    .line 303987
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aW()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 303988
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 303989
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 303990
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bf()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 303991
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bl()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 303992
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bn()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v54

    .line 303993
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 303994
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 303995
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->br()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 303996
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bs()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v58

    .line 303997
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bt()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 303998
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 303999
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bu()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 304000
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bv()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 304001
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bw()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 304002
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 304003
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bx()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 304004
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->by()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 304005
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 304006
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bA()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v68

    .line 304007
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bB()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 304008
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v70

    .line 304009
    const/16 v7, 0x85

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 304010
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 304011
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 304012
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 304013
    const/4 v2, 0x4

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->Y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304014
    const/4 v2, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->Z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304015
    const/4 v2, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aa()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304016
    const/4 v2, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->j()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304017
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->k()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304018
    const/16 v3, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304019
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 304020
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 304021
    const/16 v3, 0xd

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ab()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 304022
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 304023
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 304024
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 304025
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 304026
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 304027
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 304028
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 304029
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 304030
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304031
    const/16 v2, 0x18

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->s()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 304032
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304033
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304034
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304035
    const/16 v2, 0x1c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ak()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304036
    const/16 v2, 0x1d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->al()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304037
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304038
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304039
    const/16 v2, 0x20

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->an()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304040
    const/16 v2, 0x21

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->u()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304041
    const/16 v2, 0x22

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ao()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304042
    const/16 v2, 0x23

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ap()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304043
    const/16 v3, 0x24

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aq()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304044
    const/16 v3, 0x25

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ar()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 304045
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304046
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304047
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304048
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304049
    const/16 v3, 0x2a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->at()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304050
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304051
    const/16 v3, 0x2c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->au()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304052
    const/16 v2, 0x2d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->av()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 304053
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304054
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304055
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304056
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304057
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304058
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304059
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304060
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304061
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304062
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304063
    const/16 v2, 0x38

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->E()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304064
    const/16 v2, 0x39

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aB()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304065
    const/16 v2, 0x3a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aC()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304066
    const/16 v2, 0x3b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aD()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304067
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304068
    const/16 v2, 0x3d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aF()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304069
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304070
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304071
    const/16 v3, 0x40

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->H()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304072
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304073
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304074
    const/16 v2, 0x45

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->J()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 304075
    const/16 v2, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304076
    const/16 v2, 0x47

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aH()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304077
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304078
    const/16 v2, 0x49

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aI()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304079
    const/16 v2, 0x4a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aJ()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304080
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304081
    const/16 v2, 0x4c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aL()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304082
    const/16 v2, 0x4d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aM()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304083
    const/16 v2, 0x4e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aN()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304084
    const/16 v2, 0x4f

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aO()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304085
    const/16 v2, 0x50

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aP()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 304086
    const/16 v2, 0x51

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aQ()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 304087
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304088
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304089
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304090
    const/16 v3, 0x55

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aU()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304091
    const/16 v2, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304092
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304093
    const/16 v3, 0x58

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304094
    const/16 v3, 0x59

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aX()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 304095
    const/16 v3, 0x5a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aY()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304096
    const/16 v3, 0x5b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aZ()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-ne v2, v4, :cond_8

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304097
    const/16 v3, 0x5c

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ba()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    if-ne v2, v4, :cond_9

    const/4 v2, 0x0

    :goto_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304098
    const/16 v3, 0x5d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bb()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    if-ne v2, v4, :cond_a

    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304099
    const/16 v3, 0x5e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bc()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    if-ne v2, v4, :cond_b

    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304100
    const/16 v3, 0x5f

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bd()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v2, v4, :cond_c

    const/4 v2, 0x0

    :goto_c
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304101
    const/16 v2, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304102
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304103
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304104
    const/16 v3, 0x63

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bg()Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupPendingState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    if-ne v2, v4, :cond_d

    const/4 v2, 0x0

    :goto_d
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 304105
    const/16 v2, 0x64

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bh()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304106
    const/16 v3, 0x66

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bi()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 304107
    const/16 v2, 0x68

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bj()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304108
    const/16 v2, 0x6a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bk()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304109
    const/16 v2, 0x6b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->O()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304110
    const/16 v2, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304111
    const/16 v2, 0x6d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bm()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304112
    const/16 v2, 0x6e

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304113
    const/16 v2, 0x6f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->P()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304114
    const/16 v2, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304115
    const/16 v2, 0x71

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bo()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304116
    const/16 v2, 0x72

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bp()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304117
    const/16 v2, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304118
    const/16 v2, 0x74

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bq()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304119
    const/16 v2, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304120
    const/16 v2, 0x76

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->S()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304121
    const/16 v2, 0x77

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304122
    const/16 v2, 0x78

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304123
    const/16 v2, 0x79

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304124
    const/16 v2, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304125
    const/16 v2, 0x7b

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304126
    const/16 v2, 0x7c

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304127
    const/16 v2, 0x7d

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304128
    const/16 v2, 0x7e

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304129
    const/16 v2, 0x7f

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304130
    const/16 v2, 0x80

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304131
    const/16 v2, 0x81

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304132
    const/16 v2, 0x82

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304133
    const/16 v2, 0x83

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 304134
    const/16 v2, 0x84

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bD()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 304135
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 304136
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 304137
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    goto/16 :goto_0

    .line 304138
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aq()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v2

    goto/16 :goto_1

    .line 304139
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->at()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v2

    goto/16 :goto_2

    .line 304140
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->au()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    goto/16 :goto_3

    .line 304141
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->H()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    goto/16 :goto_4

    .line 304142
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aU()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    goto/16 :goto_5

    .line 304143
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    goto/16 :goto_6

    .line 304144
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aY()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v2

    goto/16 :goto_7

    .line 304145
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aZ()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v2

    goto/16 :goto_8

    .line 304146
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ba()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v2

    goto/16 :goto_9

    .line 304147
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bb()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v2

    goto/16 :goto_a

    .line 304148
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bc()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v2

    goto/16 :goto_b

    .line 304149
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bd()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    goto/16 :goto_c

    .line 304150
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bg()Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    move-result-object v2

    goto/16 :goto_d
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 304151
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 304152
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->V()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 304153
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->V()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304154
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->V()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 304155
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304156
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->e:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304157
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 304158
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304159
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 304160
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304161
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304162
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->W()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 304163
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->W()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304164
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->W()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 304165
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304166
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->f:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304167
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 304168
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304169
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 304170
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304171
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304172
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->br()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 304173
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->br()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304174
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->br()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 304175
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304176
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bj:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304177
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bf()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 304178
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bf()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 304179
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bf()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 304180
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304181
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aT:Lcom/facebook/graphql/model/GraphQLPage;

    .line 304182
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 304183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 304185
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304186
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304187
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 304188
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304189
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 304190
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304191
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304192
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bs()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 304193
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bs()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304194
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bs()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 304195
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304196
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bl:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304197
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bt()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 304198
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bt()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304199
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bt()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 304200
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304201
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bm:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304202
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ad()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 304203
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ad()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304204
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ad()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 304205
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304206
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->s:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304207
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 304208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 304209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 304210
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304211
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->t:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 304212
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ae()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 304213
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ae()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304214
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ae()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 304215
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304216
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->u:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304217
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 304218
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304219
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 304220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304221
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304222
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 304223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 304224
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 304225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304226
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->x:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 304227
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 304228
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304229
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 304230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304231
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bn:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304232
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 304233
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 304234
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 304235
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304236
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->y:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 304237
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 304238
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 304239
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 304240
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304241
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 304242
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 304243
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304244
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 304245
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304246
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304247
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 304248
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 304249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 304250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304251
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->br:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 304252
    :cond_13
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aj()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 304253
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aj()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 304254
    if-eqz v2, :cond_14

    .line 304255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304256
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroup;->C:Ljava/util/List;

    move-object v1, v0

    .line 304257
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 304258
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304259
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 304260
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304261
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304262
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bu()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 304263
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bu()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304264
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bu()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 304265
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304266
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bo:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304267
    :cond_16
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bv()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 304268
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bv()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304269
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bv()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 304270
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304271
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bp:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304272
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->x()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 304273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->x()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304274
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->x()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 304275
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304276
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->P:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304277
    :cond_18
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 304278
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304279
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->as()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 304280
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304281
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->Q:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304282
    :cond_19
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bA()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 304283
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bA()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 304284
    if-eqz v2, :cond_1a

    .line 304285
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304286
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroup;->bv:Ljava/util/List;

    move-object v1, v0

    .line 304287
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->y()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 304288
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->y()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 304289
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->y()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 304290
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304291
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->S:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 304292
    :cond_1b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 304293
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304294
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 304295
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304296
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304297
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 304298
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304299
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 304300
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304301
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304302
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 304303
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304304
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 304305
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304306
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304307
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 304308
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304309
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 304310
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304311
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->be:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304312
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 304313
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304314
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 304315
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304316
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304317
    :cond_20
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 304318
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304319
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 304320
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304321
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304322
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 304323
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304324
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 304325
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304326
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304327
    :cond_22
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ax()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 304328
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ax()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304329
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ax()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 304330
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304331
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aa:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 304332
    :cond_23
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 304333
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304334
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 304335
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304336
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304337
    :cond_24
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->az()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 304338
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->az()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304339
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->az()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 304340
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304341
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304342
    :cond_25
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 304343
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304344
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 304345
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304346
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304347
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 304348
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304349
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 304350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304351
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304352
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 304353
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304354
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 304355
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304356
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bh:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304357
    :cond_28
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aE()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 304358
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aE()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304359
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aE()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 304360
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304361
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aj:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 304362
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 304363
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304364
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 304365
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304366
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304367
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->G()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 304368
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->G()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 304369
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->G()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 304370
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304371
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->am:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 304372
    :cond_2b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->by()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 304373
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->by()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304374
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->by()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 304375
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304376
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bt:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304377
    :cond_2c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bB()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 304378
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bB()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 304379
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bB()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 304380
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304381
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bw:Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    .line 304382
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 304383
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 304385
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304386
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->ao:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304387
    :cond_2e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aG()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 304388
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aG()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 304389
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aG()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 304390
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304391
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->ap:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 304392
    :cond_2f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bx()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 304393
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bx()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 304394
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bx()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 304395
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304396
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bs:Lcom/facebook/graphql/model/GraphQLNode;

    .line 304397
    :cond_30
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aK()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 304398
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aK()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 304399
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aK()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 304400
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304401
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aw:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 304402
    :cond_31
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 304403
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304404
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 304405
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304406
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304407
    :cond_32
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 304408
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304409
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aS()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 304410
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304411
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304412
    :cond_33
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aT()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 304413
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aT()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 304414
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aT()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_34

    .line 304415
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304416
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aF:Lcom/facebook/graphql/model/GraphQLActor;

    .line 304417
    :cond_34
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bn()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 304418
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bn()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 304419
    if-eqz v2, :cond_35

    .line 304420
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304421
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLGroup;->bc:Ljava/util/List;

    move-object v1, v0

    .line 304422
    :cond_35
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 304423
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 304425
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304426
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 304427
    :cond_36
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bw()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 304428
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bw()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304429
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bw()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 304430
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304431
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->bq:Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    .line 304432
    :cond_37
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 304433
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 304434
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->be()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_38

    .line 304435
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 304436
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLGroup;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 304437
    :cond_38
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 304438
    if-nez v1, :cond_39

    :goto_0
    return-object p0

    :cond_39
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 304439
    new-instance v0, LX/4Wn;

    invoke-direct {v0, p1}, LX/4Wn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 304441
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 304442
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->h:Z

    .line 304443
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->i:Z

    .line 304444
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->j:Z

    .line 304445
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->k:Z

    .line 304446
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->l:Z

    .line 304447
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->p:J

    .line 304448
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->z:I

    .line 304449
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->D:Z

    .line 304450
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->E:Z

    .line 304451
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->H:Z

    .line 304452
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->I:Z

    .line 304453
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->J:Z

    .line 304454
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->K:Z

    .line 304455
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->M:J

    .line 304456
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->U:I

    .line 304457
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->af:Z

    .line 304458
    const/16 v0, 0x39

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ag:Z

    .line 304459
    const/16 v0, 0x3a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ah:Z

    .line 304460
    const/16 v0, 0x3b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ai:Z

    .line 304461
    const/16 v0, 0x3d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ak:Z

    .line 304462
    const/16 v0, 0x45

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aq:I

    .line 304463
    const/16 v0, 0x47

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->as:Z

    .line 304464
    const/16 v0, 0x49

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->au:Z

    .line 304465
    const/16 v0, 0x4a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->av:Z

    .line 304466
    const/16 v0, 0x4c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ax:Z

    .line 304467
    const/16 v0, 0x4d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->ay:Z

    .line 304468
    const/16 v0, 0x4e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->az:Z

    .line 304469
    const/16 v0, 0x4f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aA:Z

    .line 304470
    const/16 v0, 0x50

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aB:I

    .line 304471
    const/16 v0, 0x51

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aC:I

    .line 304472
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aK:J

    .line 304473
    const/16 v0, 0x64

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aV:Z

    .line 304474
    const/16 v0, 0x66

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aW:J

    .line 304475
    const/16 v0, 0x68

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aX:Z

    .line 304476
    const/16 v0, 0x6a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aY:Z

    .line 304477
    const/16 v0, 0x6b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->aZ:Z

    .line 304478
    const/16 v0, 0x6d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bb:Z

    .line 304479
    const/16 v0, 0x6f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bd:Z

    .line 304480
    const/16 v0, 0x71

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bf:Z

    .line 304481
    const/16 v0, 0x72

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bg:Z

    .line 304482
    const/16 v0, 0x74

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bi:Z

    .line 304483
    const/16 v0, 0x76

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->bk:Z

    .line 304484
    const/16 v0, 0x84

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->by:Z

    .line 304485
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 303789
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303790
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bi()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303792
    const/16 v0, 0x66

    iput v0, p2, LX/18L;->c:I

    .line 303793
    :goto_0
    return-void

    .line 303794
    :cond_0
    const-string v0, "can_viewer_claim_adminship"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303795
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aa()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303796
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303797
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 303798
    :cond_1
    const-string v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 303799
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303801
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 303802
    :cond_2
    const-string v0, "group_owner_authored_stories.available_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 303803
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v0

    .line 303804
    if-eqz v0, :cond_12

    .line 303805
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303806
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303807
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 303808
    :cond_3
    const-string v0, "group_owner_authored_stories.total_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 303809
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v0

    .line 303810
    if-eqz v0, :cond_12

    .line 303811
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303812
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303813
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 303814
    :cond_4
    const-string v0, "has_viewer_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 303815
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ak()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303816
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303817
    const/16 v0, 0x1c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303818
    :cond_5
    const-string v0, "join_approval_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 303819
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aq()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303820
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303821
    const/16 v0, 0x24

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303822
    :cond_6
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 303823
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303825
    const/16 v0, 0x26

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303826
    :cond_7
    const-string v0, "post_permission_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 303827
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->at()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303828
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303829
    const/16 v0, 0x2a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303830
    :cond_8
    const-string v0, "requires_admin_membership_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 303831
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aB()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303832
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303833
    const/16 v0, 0x39

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303834
    :cond_9
    const-string v0, "requires_post_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 303835
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aC()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303837
    const/16 v0, 0x3a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303838
    :cond_a
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 303839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->H()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303840
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303841
    const/16 v0, 0x40

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303842
    :cond_b
    const-string v0, "unread_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 303843
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->J()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303845
    const/16 v0, 0x45

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303846
    :cond_c
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 303847
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aM()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303848
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303849
    const/16 v0, 0x4d

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303850
    :cond_d
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 303851
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->aN()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303852
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303853
    const/16 v0, 0x4e

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303854
    :cond_e
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 303855
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303857
    const/16 v0, 0x58

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303858
    :cond_f
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 303859
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ba()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303860
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303861
    const/16 v0, 0x5c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303862
    :cond_10
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 303863
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bc()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303865
    const/16 v0, 0x5e

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303866
    :cond_11
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 303867
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->bd()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 303868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 303869
    const/16 v0, 0x5f

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 303870
    :cond_12
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 303716
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303717
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/graphql/model/GraphQLGroup;->a(J)V

    .line 303718
    :cond_0
    :goto_0
    return-void

    .line 303719
    :cond_1
    const-string v0, "can_viewer_claim_adminship"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 303720
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Z)V

    goto :goto_0

    .line 303721
    :cond_2
    const-string v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 303722
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 303723
    :cond_3
    const-string v0, "group_owner_authored_stories.available_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 303724
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v0

    .line 303725
    if-eqz v0, :cond_0

    .line 303726
    if-eqz p3, :cond_4

    .line 303727
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 303728
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->a(I)V

    .line 303729
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    goto :goto_0

    .line 303730
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->a(I)V

    goto :goto_0

    .line 303731
    :cond_5
    const-string v0, "group_owner_authored_stories.total_for_sale_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 303732
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->ah()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v0

    .line 303733
    if-eqz v0, :cond_0

    .line 303734
    if-eqz p3, :cond_6

    .line 303735
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 303736
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->b(I)V

    .line 303737
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->A:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    goto :goto_0

    .line 303738
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;->b(I)V

    goto :goto_0

    .line 303739
    :cond_7
    const-string v0, "has_viewer_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 303740
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroup;->b(Z)V

    goto/16 :goto_0

    .line 303741
    :cond_8
    const-string v0, "join_approval_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 303742
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V

    goto/16 :goto_0

    .line 303743
    :cond_9
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 303744
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 303745
    :cond_a
    const-string v0, "post_permission_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 303746
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V

    goto/16 :goto_0

    .line 303747
    :cond_b
    const-string v0, "requires_admin_membership_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 303748
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroup;->c(Z)V

    goto/16 :goto_0

    .line 303749
    :cond_c
    const-string v0, "requires_post_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 303750
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroup;->d(Z)V

    goto/16 :goto_0

    .line 303751
    :cond_d
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 303752
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto/16 :goto_0

    .line 303753
    :cond_e
    const-string v0, "unread_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 303754
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroup;->a(I)V

    goto/16 :goto_0

    .line 303755
    :cond_f
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 303756
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroup;->e(Z)V

    goto/16 :goto_0

    .line 303757
    :cond_10
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 303758
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLGroup;->f(Z)V

    goto/16 :goto_0

    .line 303759
    :cond_11
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 303760
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto/16 :goto_0

    .line 303761
    :cond_12
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 303762
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V

    goto/16 :goto_0

    .line 303763
    :cond_13
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 303764
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V

    goto/16 :goto_0

    .line 303765
    :cond_14
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303766
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLGroup;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto/16 :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 303767
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303768
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303769
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303770
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 303771
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303772
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303773
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->l:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303774
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303775
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 303776
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 303779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->n:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303780
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->o:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->o:Ljava/lang/String;

    .line 303782
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303783
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303784
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303785
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303786
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->t:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303787
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->t:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->t:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 303788
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->t:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLMediaSet;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303713
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->x:Lcom/facebook/graphql/model/GraphQLMediaSet;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303714
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->x:Lcom/facebook/graphql/model/GraphQLMediaSet;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSet;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSet;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->x:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 303715
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->x:Lcom/facebook/graphql/model/GraphQLMediaSet;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303871
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->y:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303872
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->y:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->y:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 303873
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->y:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    return-object v0
.end method

.method public final s()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 303874
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303875
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303876
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->z:I

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303877
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303878
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->F:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->F:Ljava/lang/String;

    .line 303879
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 303880
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 303881
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 303882
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->I:Z

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303883
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->N:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303884
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->N:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->N:Ljava/lang/String;

    .line 303885
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->N:Ljava/lang/String;

    return-object v0
.end method

.method public final w()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303886
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->O:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303887
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->O:Ljava/util/List;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->O:Ljava/util/List;

    .line 303888
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->O:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303889
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->P:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303890
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->P:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->P:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 303891
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->P:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303892
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->S:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303893
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->S:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->S:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 303894
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->S:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303895
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->V:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 303896
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->V:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 303897
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLGroup;->V:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method
