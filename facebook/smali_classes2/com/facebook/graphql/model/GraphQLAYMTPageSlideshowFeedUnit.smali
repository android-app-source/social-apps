.class public final Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:I

.field private s:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 249886
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 249887
    const-class v0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 249888
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 249889
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x1b7352de

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 249890
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s:LX/0x2;

    .line 249891
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 249892
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->r:I

    .line 249893
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 249894
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 249895
    if-eqz v0, :cond_0

    .line 249896
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 249897
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 249898
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p:Ljava/lang/String;

    .line 249899
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 249900
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 249901
    if-eqz v0, :cond_0

    .line 249902
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 249903
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 249904
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q:Ljava/lang/String;

    .line 249905
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 249906
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 249907
    if-eqz v0, :cond_0

    .line 249908
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 249909
    :cond_0
    return-void
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249910
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249911
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 249912
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249913
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249914
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->m:Ljava/lang/String;

    .line 249915
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249916
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249917
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 249918
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249919
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249920
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p:Ljava/lang/String;

    .line 249921
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249922
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249923
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q:Ljava/lang/String;

    .line 249924
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method private v()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 249925
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 249926
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 249927
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->r:I

    return v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->i:Ljava/lang/String;

    .line 249930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 249931
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249932
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249933
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g:Ljava/lang/String;

    .line 249934
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 249882
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 249883
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 249884
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->h:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 249885
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 249779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s:LX/0x2;

    if-nez v0, :cond_0

    .line 249780
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s:LX/0x2;

    .line 249781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 249782
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 249783
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 249784
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 249785
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 249786
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->C_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 249787
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 249788
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 249789
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 249790
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->r()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 249791
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 249792
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 249793
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 249794
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->u()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 249795
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 249796
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 249797
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 249798
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 249799
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 249800
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 249801
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 249802
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 249803
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 249804
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 249805
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 249806
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 249807
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 249808
    const/16 v2, 0xd

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->v()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 249809
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 249810
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 249811
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 249812
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 249813
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 249814
    if-eqz v1, :cond_4

    .line 249815
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    .line 249816
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->j:Ljava/util/List;

    move-object v1, v0

    .line 249817
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249818
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 249819
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 249820
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    .line 249821
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 249822
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 249823
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 249824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 249825
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    .line 249826
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 249827
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 249828
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 249829
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 249830
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    .line 249831
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 249832
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 249833
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 249834
    new-instance v0, LX/4Vl;

    invoke-direct {v0, p1}, LX/4Vl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249835
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 249836
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->h:J

    .line 249837
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 249838
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 249839
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->h:J

    .line 249840
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->r:I

    .line 249841
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 249842
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249843
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 249844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 249845
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    .line 249846
    :goto_0
    return-void

    .line 249847
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249848
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 249849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 249850
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 249851
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 249852
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 249853
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 249854
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 249855
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 249856
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249857
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->a(Ljava/lang/String;)V

    .line 249858
    :cond_0
    :goto_0
    return-void

    .line 249859
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 249860
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 249861
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249862
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249863
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249864
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o:Ljava/lang/String;

    .line 249865
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249866
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 249868
    :goto_0
    return-object v0

    .line 249869
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 249870
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 249871
    const v0, -0x1b7352de

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249872
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249873
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->f:Ljava/lang/String;

    .line 249874
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249875
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249876
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249877
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->j:Ljava/util/List;

    .line 249878
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 249879
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 249880
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 249881
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->l:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method
