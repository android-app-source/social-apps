.class public final Lcom/facebook/graphql/model/GraphQLComment;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16m;
.implements LX/16n;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLComment$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLComment$Serializer;
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Z

.field public G:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public I:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:I

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Z

.field public e:I

.field public f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLComment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:J

.field public p:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public v:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 269986
    const-class v0, Lcom/facebook/graphql/model/GraphQLComment$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 269983
    const-class v0, Lcom/facebook/graphql/model/GraphQLComment$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 269984
    const/16 v0, 0x25

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 269985
    return-void
.end method

.method public constructor <init>(LX/4Vu;)V
    .locals 2

    .prologue
    .line 269987
    const/16 v0, 0x25

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 269988
    iget v0, p1, LX/4Vu;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->e:I

    .line 269989
    iget-object v0, p1, LX/4Vu;->c:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 269990
    iget-object v0, p1, LX/4Vu;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->g:Ljava/util/List;

    .line 269991
    iget-object v0, p1, LX/4Vu;->e:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->h:Lcom/facebook/graphql/model/GraphQLActor;

    .line 269992
    iget-object v0, p1, LX/4Vu;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269993
    iget-object v0, p1, LX/4Vu;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269994
    iget-boolean v0, p1, LX/4Vu;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->H:Z

    .line 269995
    iget-boolean v0, p1, LX/4Vu;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->k:Z

    .line 269996
    iget-boolean v0, p1, LX/4Vu;->j:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->l:Z

    .line 269997
    iget-boolean v0, p1, LX/4Vu;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->L:Z

    .line 269998
    iget-object v0, p1, LX/4Vu;->l:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 269999
    iget-object v0, p1, LX/4Vu;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->n:Ljava/lang/String;

    .line 270000
    iget-wide v0, p1, LX/4Vu;->n:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->o:J

    .line 270001
    iget-object v0, p1, LX/4Vu;->o:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->p:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 270002
    iget-object v0, p1, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 270003
    iget-object v0, p1, LX/4Vu;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->r:Ljava/lang/String;

    .line 270004
    iget-object v0, p1, LX/4Vu;->r:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->s:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    .line 270005
    iget-boolean v0, p1, LX/4Vu;->s:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->t:Z

    .line 270006
    iget-boolean v0, p1, LX/4Vu;->t:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->u:Z

    .line 270007
    iget-boolean v0, p1, LX/4Vu;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->v:Z

    .line 270008
    iget-object v0, p1, LX/4Vu;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->G:Ljava/lang/String;

    .line 270009
    iget v0, p1, LX/4Vu;->w:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->J:I

    .line 270010
    iget-object v0, p1, LX/4Vu;->x:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->w:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 270011
    iget-object v0, p1, LX/4Vu;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 270012
    iget-object v0, p1, LX/4Vu;->z:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->y:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    .line 270013
    iget-object v0, p1, LX/4Vu;->A:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->I:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 270014
    iget-object v0, p1, LX/4Vu;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->z:Ljava/lang/String;

    .line 270015
    iget v0, p1, LX/4Vu;->C:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->A:I

    .line 270016
    iget-object v0, p1, LX/4Vu;->D:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->K:Ljava/lang/String;

    .line 270017
    iget v0, p1, LX/4Vu;->E:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->B:I

    .line 270018
    iget-object v0, p1, LX/4Vu;->F:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->C:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 270019
    iget-object v0, p1, LX/4Vu;->G:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 270020
    iget-object v0, p1, LX/4Vu;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->E:Ljava/lang/String;

    .line 270021
    iget-boolean v0, p1, LX/4Vu;->I:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->F:Z

    .line 270022
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270023
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->s:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270024
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->s:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->s:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    .line 270025
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->s:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    return-object v0
.end method

.method public final B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 270026
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270027
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270028
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->t:Z

    return v0
.end method

.method public final C()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 270029
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270030
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270031
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->u:Z

    return v0
.end method

.method public final D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 270032
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270033
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270034
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->v:Z

    return v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270035
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->w:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270036
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->w:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->w:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 270037
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->w:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270038
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270039
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 270040
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270041
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->y:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270042
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->y:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->y:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    .line 270043
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->y:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270044
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270045
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->z:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->z:Ljava/lang/String;

    .line 270046
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final I()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 270047
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270048
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270049
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->A:I

    return v0
.end method

.method public final J()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 270050
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270051
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270052
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->B:I

    return v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270053
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->C:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270054
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->C:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->C:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 270055
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->C:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270056
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270057
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 270058
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270059
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270060
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->E:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->E:Ljava/lang/String;

    .line 270061
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 270062
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270063
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270064
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->F:Z

    return v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270065
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270066
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->G:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->G:Ljava/lang/String;

    .line 270067
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 270068
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270069
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270070
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->H:Z

    return v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270071
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->I:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270072
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->I:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->I:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 270073
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->I:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    return-object v0
.end method

.method public final R()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 270074
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 270075
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 270076
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->J:I

    return v0
.end method

.method public final S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 270077
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 270078
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->K:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->K:Ljava/lang/String;

    .line 270079
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final T()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 269921
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 269922
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 269923
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->L:Z

    return v0
.end method

.method public final a(LX/186;)I
    .locals 28

    .prologue
    .line 269924
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 269925
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 269926
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 269927
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 269928
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 269929
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 269930
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 269931
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->w()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 269932
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 269933
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 269934
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 269935
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 269936
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->E()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 269937
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 269938
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 269939
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 269940
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 269941
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 269942
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->M()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 269943
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->O()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 269944
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->Q()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 269945
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->S()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 269946
    const/16 v25, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 269947
    const/16 v25, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->n()I

    move-result v26

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 269948
    const/16 v25, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 269949
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 269950
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 269951
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 269952
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 269953
    const/4 v4, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->t()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269954
    const/16 v4, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->u()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269955
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 269956
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 269957
    const/16 v5, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->x()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 269958
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 269959
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 269960
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 269961
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 269962
    const/16 v4, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->B()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269963
    const/16 v4, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->C()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269964
    const/16 v4, 0x12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->D()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269965
    const/16 v4, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 269966
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269967
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269968
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269969
    const/16 v4, 0x17

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->I()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 269970
    const/16 v4, 0x18

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->J()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 269971
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269972
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269973
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269974
    const/16 v4, 0x1c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->N()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269975
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269976
    const/16 v4, 0x1e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->P()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269977
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269978
    const/16 v4, 0x20

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->R()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 269979
    const/16 v4, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 269980
    const/16 v4, 0x23

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLComment;->T()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 269981
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 269982
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 269804
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 269805
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 269806
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 269807
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 269808
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269809
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 269810
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 269811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 269812
    if-eqz v2, :cond_1

    .line 269813
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269814
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLComment;->g:Ljava/util/List;

    move-object v1, v0

    .line 269815
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 269816
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 269817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 269818
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269819
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->h:Lcom/facebook/graphql/model/GraphQLActor;

    .line 269820
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 269821
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269822
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 269823
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269824
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269825
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 269826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 269828
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269829
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269830
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 269831
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 269833
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269834
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 269835
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 269836
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 269837
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 269838
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269839
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->p:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 269840
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 269841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 269842
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 269843
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269844
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 269845
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 269846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    .line 269847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 269848
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269849
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->s:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    .line 269850
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->E()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 269851
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->E()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 269852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->E()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 269853
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269854
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->w:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 269855
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 269856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269857
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 269858
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269859
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269860
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 269861
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    .line 269862
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 269863
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269864
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->y:Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    .line 269865
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->Q()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 269866
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->Q()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 269867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->Q()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 269868
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269869
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->I:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 269870
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 269871
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 269872
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->K()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 269873
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269874
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->C:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 269875
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 269876
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269877
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->L()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 269878
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269879
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLComment;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269880
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 269881
    if-nez v1, :cond_f

    :goto_0
    return-object p0

    :cond_f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269803
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 269788
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 269789
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->e:I

    .line 269790
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->k:Z

    .line 269791
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->l:Z

    .line 269792
    const/16 v0, 0xb

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->o:J

    .line 269793
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->t:Z

    .line 269794
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->u:Z

    .line 269795
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->v:Z

    .line 269796
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->A:I

    .line 269797
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->B:I

    .line 269798
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->F:Z

    .line 269799
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->H:Z

    .line 269800
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->J:I

    .line 269801
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->L:Z

    .line 269802
    return-void
.end method

.method public final e()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269785
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269786
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 269787
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 269770
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_3

    .line 269771
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 269772
    if-ne p0, p1, :cond_0

    .line 269773
    const/4 v0, 0x1

    .line 269774
    :goto_0
    move v0, v0

    .line 269775
    return v0

    .line 269776
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 269777
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 269778
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 269779
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4Zt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269780
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/4Zt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 269781
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 269782
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 269783
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 269784
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 269769
    const v0, -0x642179c1

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269767
    const/4 v0, 0x0

    move-object v0, v0

    .line 269768
    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 269762
    const/4 v0, 0x0

    .line 269763
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 269764
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/4Zt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 269765
    :cond_0
    move v0, v0

    .line 269766
    return v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 269761
    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 269759
    invoke-static {p0}, LX/2v3;->d(LX/16n;)I

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 269758
    invoke-static {p0}, LX/2v3;->e(LX/16n;)I

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 269757
    invoke-static {p0}, LX/2v3;->c(LX/16n;)Z

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 269882
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 269883
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 269884
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->e:I

    return v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269885
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->f:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269886
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 269887
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269888
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269889
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->g:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->g:Ljava/util/List;

    .line 269890
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269891
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->h:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269892
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->h:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->h:Lcom/facebook/graphql/model/GraphQLActor;

    .line 269893
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->h:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269894
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269895
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269896
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269897
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269898
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 269899
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 269900
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 269901
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 269902
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->k:Z

    return v0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 269903
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 269904
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 269905
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->l:Z

    return v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269906
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->m:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269907
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->m:Lcom/facebook/graphql/model/GraphQLComment;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 269908
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->m:Lcom/facebook/graphql/model/GraphQLComment;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269909
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269910
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->n:Ljava/lang/String;

    .line 269911
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final x()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 269918
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 269919
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 269920
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->o:J

    return-wide v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269912
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->p:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269913
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->p:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->p:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 269914
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->p:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269915
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 269916
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->r:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->r:Ljava/lang/String;

    .line 269917
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLComment;->r:Ljava/lang/String;

    return-object v0
.end method
