.class public final Lcom/facebook/graphql/model/GraphQLImageAtRange;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLImageAtRange$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLImageAtRange$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLEntityWithImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301146
    const-class v0, Lcom/facebook/graphql/model/GraphQLImageAtRange$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301176
    const-class v0, Lcom/facebook/graphql/model/GraphQLImageAtRange$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 301174
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301175
    return-void
.end method

.method public constructor <init>(LX/4Wv;)V
    .locals 1

    .prologue
    .line 301169
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301170
    iget-object v0, p1, LX/4Wv;->b:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    .line 301171
    iget v0, p1, LX/4Wv;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->f:I

    .line 301172
    iget v0, p1, LX/4Wv;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->g:I

    .line 301173
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 301161
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 301163
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 301164
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 301165
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 301166
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 301167
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301168
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 301177
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    .line 301180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 301181
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImageAtRange;

    .line 301182
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLImageAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    .line 301183
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301184
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301158
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301159
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    .line 301160
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->e:Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 301154
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 301155
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->f:I

    .line 301156
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->g:I

    .line 301157
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 301153
    const v0, 0x353fb0f

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301150
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301151
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301152
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->f:I

    return v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301147
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 301148
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 301149
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLImageAtRange;->g:I

    return v0
.end method
