.class public final Lcom/facebook/graphql/model/GraphQLStorySet;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements Lcom/facebook/graphql/model/Sponsorable;
.implements LX/17w;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStorySet$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStorySet$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:J

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 199917
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySet$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 199912
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySet$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 199913
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 199914
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x6a3d0f4d

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 199915
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->y:LX/0x2;

    .line 199916
    return-void
.end method

.method public constructor <init>(LX/4Yw;)V
    .locals 2

    .prologue
    .line 199918
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 199919
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x6a3d0f4d

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 199920
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->y:LX/0x2;

    .line 199921
    iget-object v0, p1, LX/4Yw;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->f:Ljava/util/List;

    .line 199922
    iget-object v0, p1, LX/4Yw;->c:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->g:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 199923
    iget-object v0, p1, LX/4Yw;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->h:Ljava/lang/String;

    .line 199924
    iget-object v0, p1, LX/4Yw;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->i:Ljava/util/List;

    .line 199925
    iget-object v0, p1, LX/4Yw;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->j:Ljava/lang/String;

    .line 199926
    iget-object v0, p1, LX/4Yw;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->k:Ljava/lang/String;

    .line 199927
    iget-wide v0, p1, LX/4Yw;->h:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->l:J

    .line 199928
    iget-object v0, p1, LX/4Yw;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->m:Ljava/lang/String;

    .line 199929
    iget-object v0, p1, LX/4Yw;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->n:Ljava/lang/String;

    .line 199930
    iget-object v0, p1, LX/4Yw;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->o:Ljava/lang/String;

    .line 199931
    iget-object v0, p1, LX/4Yw;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->p:Ljava/lang/String;

    .line 199932
    iget v0, p1, LX/4Yw;->m:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->q:I

    .line 199933
    iget-object v0, p1, LX/4Yw;->n:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 199934
    iget-object v0, p1, LX/4Yw;->o:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 199935
    iget-object v0, p1, LX/4Yw;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->t:Ljava/lang/String;

    .line 199936
    iget-object v0, p1, LX/4Yw;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199937
    iget-object v0, p1, LX/4Yw;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199938
    iget-object v0, p1, LX/4Yw;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->w:Ljava/lang/String;

    .line 199939
    iget-object v0, p1, LX/4Yw;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->x:Ljava/lang/String;

    .line 199940
    iget-object v0, p1, LX/4Yw;->u:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->y:LX/0x2;

    .line 199941
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 199942
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->q:I

    .line 199943
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 199944
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 199945
    if-eqz v0, :cond_0

    .line 199946
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 199947
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 199948
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->o:Ljava/lang/String;

    .line 199949
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 199950
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 199951
    if-eqz v0, :cond_0

    .line 199952
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 199953
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 199954
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->p:Ljava/lang/String;

    .line 199955
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 199956
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 199957
    if-eqz v0, :cond_0

    .line 199958
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 199959
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199960
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199961
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->m:Ljava/lang/String;

    .line 199962
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199963
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199964
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->n:Ljava/lang/String;

    .line 199965
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199966
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199967
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->o:Ljava/lang/String;

    .line 199968
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199969
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199970
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->p:Ljava/lang/String;

    .line 199971
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 199972
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 199976
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 199977
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 199978
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->q:I

    return v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199973
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199974
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->j:Ljava/lang/String;

    .line 199975
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 200002
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 200003
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 200004
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 199999
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 200000
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 200001
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->l:J

    return-wide v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199996
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->s:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199997
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->s:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 199998
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->s:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199993
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199994
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->t:Ljava/lang/String;

    .line 199995
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 199992
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199989
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199990
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199991
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 199988
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199985
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199986
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199987
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199982
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199983
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->x:Ljava/lang/String;

    .line 199984
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 199979
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->y:LX/0x2;

    if-nez v0, :cond_0

    .line 199980
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->y:LX/0x2;

    .line 199981
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->y:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 199909
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 199910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 199911
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 199762
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 199763
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->w()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 199764
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 199765
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 199766
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->d(Ljava/util/List;)I

    move-result v5

    .line 199767
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->E_()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 199768
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->z()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 199769
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->A()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 199770
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->B()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 199771
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->C()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 199772
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->D()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 199773
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->F()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 199774
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 199775
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->H()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 199776
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 199777
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 199778
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->c()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 199779
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->K()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 199780
    const/16 v19, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 199781
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 199782
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 199783
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 199784
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 199785
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 199786
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 199787
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 199788
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 199789
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 199790
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 199791
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 199792
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->E()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 199793
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 199794
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 199795
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 199796
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 199797
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 199798
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 199799
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 199800
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 199801
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 199840
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 199841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->w()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 199842
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->w()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 199843
    if-eqz v1, :cond_6

    .line 199844
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 199845
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;->f:Ljava/util/List;

    move-object v1, v0

    .line 199846
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 199848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 199849
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 199850
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;->g:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 199851
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->F()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 199852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->F()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 199853
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->F()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 199854
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 199855
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;->r:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 199856
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 199857
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 199858
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 199859
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 199860
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 199861
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 199862
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199863
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 199864
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 199865
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;->u:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199866
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 199867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199868
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 199869
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 199870
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 199871
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 199872
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 199839
    new-instance v0, LX/4Yx;

    invoke-direct {v0, p1}, LX/4Yx;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 199836
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->l:J

    .line 199837
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 199832
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 199833
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->l:J

    .line 199834
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->q:I

    .line 199835
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 199818
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 199820
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 199821
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    .line 199822
    :goto_0
    return-void

    .line 199823
    :cond_0
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 199825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 199826
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 199827
    :cond_1
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199828
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 199829
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 199830
    const/16 v0, 0xc

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 199831
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 199811
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199812
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStorySet;->a(Ljava/lang/String;)V

    .line 199813
    :cond_0
    :goto_0
    return-void

    .line 199814
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199815
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStorySet;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 199816
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199817
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->a(I)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199808
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199809
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->w:Ljava/lang/String;

    .line 199810
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199803
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199804
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 199805
    :goto_0
    return-object v0

    .line 199806
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 199807
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 199802
    const v0, 0x6a3d0f4d

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199759
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199760
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->h:Ljava/lang/String;

    .line 199761
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 199876
    invoke-static {p0}, LX/39w;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 199877
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->C_()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 199878
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->A()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 199879
    invoke-static {p0}, LX/2dv;->a(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v0

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199880
    const/4 v0, 0x0

    move-object v0, v0

    .line 199881
    return-object v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 199882
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->u()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 199883
    invoke-static {p0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 199884
    invoke-static {p0}, LX/1w8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 199885
    invoke-static {p0}, LX/39w;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 199886
    if-eqz v0, :cond_0

    .line 199887
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    .line 199888
    :goto_0
    move-object v0, v0

    .line 199889
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199890
    invoke-static {p0}, LX/1mc;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 1

    .prologue
    .line 199891
    invoke-static {p0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 5

    .prologue
    .line 199892
    const/4 v1, 0x0

    .line 199893
    invoke-static {p0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 199894
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199895
    const/4 v0, 0x1

    .line 199896
    :goto_1
    move v0, v0

    .line 199897
    return v0

    .line 199898
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 199899
    goto :goto_1
.end method

.method public final w()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199900
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199901
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->f:Ljava/util/List;

    .line 199902
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199873
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->g:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199874
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->g:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->g:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 199875
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->g:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    return-object v0
.end method

.method public final y()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199903
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199904
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->i:Ljava/util/List;

    .line 199905
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199906
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 199907
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->k:Ljava/lang/String;

    .line 199908
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;->k:Ljava/lang/String;

    return-object v0
.end method
