.class public final Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306266
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306265
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 306263
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306264
    return-void
.end method

.method public constructor <init>(LX/4Y2;)V
    .locals 1

    .prologue
    .line 306231
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306232
    iget-object v0, p1, LX/4Y2;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->e:Ljava/util/List;

    .line 306233
    iget-object v0, p1, LX/4Y2;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->f:Ljava/util/List;

    .line 306234
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 306255
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306256
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 306257
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 306258
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 306259
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 306260
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 306261
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306262
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306252
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306253
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoTagsEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->e:Ljava/util/List;

    .line 306254
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 306239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306240
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 306241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 306242
    if-eqz v1, :cond_0

    .line 306243
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 306244
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->e:Ljava/util/List;

    .line 306245
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 306246
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 306247
    if-eqz v1, :cond_1

    .line 306248
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 306249
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->f:Ljava/util/List;

    .line 306250
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306251
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 306238
    const v0, 0x69d1689

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306235
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306236
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->f:Ljava/util/List;

    .line 306237
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
