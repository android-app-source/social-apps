.class public final Lcom/facebook/graphql/model/GraphQLExternalUrl;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLExternalUrl$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLExternalUrl$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLVideoShare;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public E:Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLIcon;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLMedia;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324128
    const-class v0, Lcom/facebook/graphql/model/GraphQLExternalUrl$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324129
    const-class v0, Lcom/facebook/graphql/model/GraphQLExternalUrl$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 324130
    const/16 v0, 0x23

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 324131
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324132
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324133
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    .line 324134
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324135
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324136
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324137
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324138
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324139
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324140
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324141
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324142
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324143
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324144
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324145
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324146
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324147
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324148
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z:Ljava/lang/String;

    .line 324149
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z:Ljava/lang/String;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLVideoShare;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B:Lcom/facebook/graphql/model/GraphQLVideoShare;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B:Lcom/facebook/graphql/model/GraphQLVideoShare;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoShare;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoShare;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B:Lcom/facebook/graphql/model/GraphQLVideoShare;

    .line 324152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B:Lcom/facebook/graphql/model/GraphQLVideoShare;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 324155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 324158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324160
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 324161
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    return-object v0
.end method

.method private K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324162
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324163
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->F:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->F:Ljava/lang/String;

    .line 324164
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->F:Ljava/lang/String;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324165
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G:Lcom/facebook/graphql/model/GraphQLIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324166
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G:Lcom/facebook/graphql/model/GraphQLIcon;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 324167
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G:Lcom/facebook/graphql/model/GraphQLIcon;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323922
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->e:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323923
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->e:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->e:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    .line 323924
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->e:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324168
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->f:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->f:Ljava/util/List;

    .line 324170
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324125
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->g:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324126
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->g:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->g:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 324127
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->g:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method private r()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324171
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 324172
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 324173
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->i:J

    return-wide v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323919
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323920
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    .line 323921
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323925
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323926
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l:Ljava/lang/String;

    .line 323927
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l:Ljava/lang/String;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n:Lcom/facebook/graphql/model/GraphQLMedia;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 323930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n:Lcom/facebook/graphql/model/GraphQLMedia;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323931
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323932
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    .line 323933
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323934
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323935
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->p:Ljava/lang/String;

    .line 323936
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->p:Ljava/lang/String;

    return-object v0
.end method

.method private x()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323937
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323938
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q:Ljava/util/List;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q:Ljava/util/List;

    .line 323939
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323940
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323941
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    .line 323942
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323943
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 323945
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 31

    .prologue
    .line 323946
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323947
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 323948
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->p()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 323949
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 323950
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 323951
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 323952
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->k()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 323953
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->t()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 323954
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 323955
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 323956
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 323957
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->w()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 323958
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->x()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/util/List;)I

    move-result v15

    .line 323959
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 323960
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 323961
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 323962
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 323963
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 323964
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 323965
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 323966
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 323967
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->F()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 323968
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 323969
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G()Lcom/facebook/graphql/model/GraphQLVideoShare;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 323970
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->H()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 323971
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->J()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 323972
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->K()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 323973
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->L()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 323974
    const/16 v6, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 323975
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v2}, LX/186;->b(II)V

    .line 323976
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 323977
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 323978
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 323979
    const/4 v3, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->r()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 323980
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 323981
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 323982
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 323983
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 323984
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 323985
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 323986
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 323987
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 323988
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323989
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323990
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323991
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323992
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323993
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323994
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323995
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323996
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323997
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323998
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 323999
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 324000
    const/16 v3, 0x1d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->I()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 324001
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 324002
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 324003
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 324004
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324005
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 324006
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->I()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 324007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324008
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324009
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    .line 324010
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 324011
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324012
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->e:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    .line 324013
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 324014
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 324015
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->q()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 324016
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324017
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->g:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 324018
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 324019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 324021
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324022
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->h:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324023
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 324024
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    .line 324025
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 324026
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324027
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->j:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    .line 324028
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->H()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 324029
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->H()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 324030
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->H()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 324031
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324032
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 324033
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->L()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 324034
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->L()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLIcon;

    .line 324035
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->L()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 324036
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324037
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 324038
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 324039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 324040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->l()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 324041
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324042
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 324043
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 324044
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 324045
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 324046
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324047
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 324048
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 324049
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    .line 324050
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 324051
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324052
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->o:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    .line 324053
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 324054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 324055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 324056
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324057
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->r:Lcom/facebook/graphql/model/GraphQLNode;

    .line 324058
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 324059
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    .line 324060
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 324061
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324062
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->s:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    .line 324063
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 324064
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324065
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 324066
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324067
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324068
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 324069
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    .line 324070
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 324071
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324072
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->u:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    .line 324073
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->J()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 324074
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->J()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 324075
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->J()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 324076
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324077
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 324078
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 324079
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324080
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 324081
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324082
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324083
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 324084
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324085
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->C()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 324086
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324087
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324088
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 324089
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324090
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 324091
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324092
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324093
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 324094
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324095
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 324096
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324097
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324098
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G()Lcom/facebook/graphql/model/GraphQLVideoShare;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 324099
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G()Lcom/facebook/graphql/model/GraphQLVideoShare;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoShare;

    .line 324100
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->G()Lcom/facebook/graphql/model/GraphQLVideoShare;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 324101
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324102
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLExternalUrl;->B:Lcom/facebook/graphql/model/GraphQLVideoShare;

    .line 324103
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324104
    if-nez v1, :cond_13

    :goto_0
    return-object p0

    :cond_13
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324105
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 324106
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 324107
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->i:J

    .line 324108
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 324109
    const v0, 0x1eaef984

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324110
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->h:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324111
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->h:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExternalUrl;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->h:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 324112
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->h:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324113
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324114
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->k:Ljava/lang/String;

    .line 324115
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324116
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324117
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantArticle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 324118
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324119
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->r:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324120
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->r:Lcom/facebook/graphql/model/GraphQLNode;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->r:Lcom/facebook/graphql/model/GraphQLNode;

    .line 324121
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->r:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324122
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324123
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A:Ljava/lang/String;

    .line 324124
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLExternalUrl;->A:Ljava/lang/String;

    return-object v0
.end method
