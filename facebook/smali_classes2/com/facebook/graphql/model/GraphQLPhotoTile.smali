.class public final Lcom/facebook/graphql/model/GraphQLPhotoTile;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTile$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPhotoTile$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327058
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTile$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327057
    const-class v0, Lcom/facebook/graphql/model/GraphQLPhotoTile$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 327055
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327056
    return-void
.end method

.method public constructor <init>(LX/4Y4;)V
    .locals 1

    .prologue
    .line 327048
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327049
    iget v0, p1, LX/4Y4;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->e:I

    .line 327050
    iget v0, p1, LX/4Y4;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->f:I

    .line 327051
    iget v0, p1, LX/4Y4;->d:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->g:I

    .line 327052
    iget v0, p1, LX/4Y4;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->h:I

    .line 327053
    iget-object v0, p1, LX/4Y4;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->i:Ljava/lang/String;

    .line 327054
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 327045
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 327046
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 327047
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 327035
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 327037
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 327038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->a()I

    move-result v1

    invoke-virtual {p1, v3, v1, v3}, LX/186;->a(III)V

    .line 327039
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->j()I

    move-result v2

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 327040
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->k()I

    move-result v2

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 327041
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->l()I

    move-result v2

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 327042
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 327043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327044
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 327059
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327060
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327061
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 327029
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 327030
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->e:I

    .line 327031
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->f:I

    .line 327032
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->g:I

    .line 327033
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->h:I

    .line 327034
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 327028
    const v0, 0x44e6bbe0

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327025
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 327026
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 327027
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->f:I

    return v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327022
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 327023
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 327024
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->g:I

    return v0
.end method

.method public final l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327019
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 327020
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 327021
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->h:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327016
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327017
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->i:Ljava/lang/String;

    .line 327018
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPhotoTile;->i:Ljava/lang/String;

    return-object v0
.end method
