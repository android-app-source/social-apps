.class public final Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNewsFeedConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNewsFeedConnection$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 307120
    const-class v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 307119
    const-class v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 307117
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 307118
    return-void
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307114
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307115
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/FeedUnit;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->h:Ljava/util/List;

    .line 307116
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 307098
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 307099
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 307100
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 307101
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->n()LX/0Px;

    move-result-object v2

    sget-object v3, LX/16Z;->a:LX/16Z;

    .line 307102
    const/4 v4, 0x1

    invoke-virtual {p1, v2, v3, v4}, LX/186;->a(Ljava/util/List;LX/16a;Z)I

    move-result v4

    move v2, v4

    .line 307103
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 307104
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->m()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 307105
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 307106
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 307107
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 307108
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->k()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 307109
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 307110
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 307111
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 307112
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 307113
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 307075
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 307076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 307077
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 307078
    if-eqz v1, :cond_0

    .line 307079
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 307080
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->f:Ljava/util/List;

    .line 307081
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 307082
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 307083
    if-eqz v1, :cond_1

    .line 307084
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 307085
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->h:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 307086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 307087
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 307088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 307089
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 307090
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->i:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 307091
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->m()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 307092
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->m()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 307093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->m()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 307094
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 307095
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 307096
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 307097
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307121
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307122
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->e:Ljava/lang/String;

    .line 307123
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 307072
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 307073
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->g:Z

    .line 307074
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 307071
    const v0, 0x472f78ef

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307068
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307069
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->f:Ljava/util/List;

    .line 307070
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 307065
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 307066
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 307067
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->g:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307062
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->i:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307063
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->i:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->i:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 307064
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->i:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 307059
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 307060
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    .line 307061
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j:Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    return-object v0
.end method
