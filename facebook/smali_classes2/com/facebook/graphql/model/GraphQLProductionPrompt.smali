.class public final Lcom/facebook/graphql/model/GraphQLProductionPrompt;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLProductionPrompt$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLProductionPrompt$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

.field public j:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/enums/GraphQLPromptType;

.field public m:D

.field public n:Lcom/facebook/graphql/model/GraphQLEventTimeRange;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320216
    const-class v0, Lcom/facebook/graphql/model/GraphQLProductionPrompt$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320217
    const-class v0, Lcom/facebook/graphql/model/GraphQLProductionPrompt$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320218
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320219
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 320220
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->e:Ljava/lang/String;

    .line 320221
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 320222
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 320223
    if-eqz v0, :cond_0

    .line 320224
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 320225
    :cond_0
    return-void
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p:Ljava/lang/String;

    .line 320228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x0

    .line 320229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320230
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 320231
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 320232
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 320233
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 320234
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->s()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 320235
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 320236
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->w()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 320237
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->u()Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 320238
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->v()Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 320239
    const/16 v5, 0xe

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 320240
    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 320241
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->k()I

    move-result v5

    invoke-virtual {p1, v0, v5, v11}, LX/186;->a(III)V

    .line 320242
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->l()Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v0

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    if-ne v0, v11, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v5, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 320243
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 320244
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n()Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    if-ne v0, v5, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 320245
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 320246
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 320247
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    if-ne v2, v3, :cond_2

    :goto_2
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 320248
    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->r()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 320249
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 320250
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 320251
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 320252
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 320253
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 320254
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320255
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 320256
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->l()Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v0

    goto :goto_0

    .line 320257
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n()Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    move-result-object v0

    goto :goto_1

    .line 320258
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320259
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320260
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320261
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320262
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320263
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    .line 320264
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320265
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 320266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 320267
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 320268
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    .line 320269
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320270
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->u()Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 320271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->u()Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    .line 320272
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->u()Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 320273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    .line 320274
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q:Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    .line 320275
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 320276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320277
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 320278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    .line 320279
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320280
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->v()Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 320281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->v()Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    .line 320282
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->v()Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 320283
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    .line 320284
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->r:Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    .line 320285
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->s()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 320286
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->s()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 320287
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->s()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 320288
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    .line 320289
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 320290
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320291
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 320292
    new-instance v0, LX/4YM;

    invoke-direct {v0, p1}, LX/4YM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320293
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 320294
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320295
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->f:I

    .line 320296
    const/16 v0, 0x8

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->m:D

    .line 320297
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 320298
    const-string v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320299
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 320300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 320301
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 320302
    :goto_0
    return-void

    .line 320303
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 320212
    const-string v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320213
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->a(Ljava/lang/String;)V

    .line 320214
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320215
    const v0, -0x75d3b463

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320173
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320174
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->e:Ljava/lang/String;

    .line 320175
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320179
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320180
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320181
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->f:I

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLPromptConfidence;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320182
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->g:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320183
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->g:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->g:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    .line 320184
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->g:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320176
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320177
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320178
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLPromptFeedType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320185
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->i:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320186
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->i:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->i:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    .line 320187
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->i:Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320188
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320189
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j:Lcom/facebook/graphql/model/GraphQLImage;

    .line 320190
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320191
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320192
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 320193
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLPromptType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320194
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->l:Lcom/facebook/graphql/enums/GraphQLPromptType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320195
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->l:Lcom/facebook/graphql/enums/GraphQLPromptType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPromptType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromptType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->l:Lcom/facebook/graphql/enums/GraphQLPromptType;

    .line 320196
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->l:Lcom/facebook/graphql/enums/GraphQLPromptType;

    return-object v0
.end method

.method public final r()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320197
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 320198
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 320199
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->m:D

    return-wide v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLEventTimeRange;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320200
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320201
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 320202
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320203
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320204
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o:Ljava/lang/String;

    .line 320205
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320206
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q:Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320207
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q:Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q:Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    .line 320208
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q:Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320209
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->r:Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320210
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->r:Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->r:Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    .line 320211
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->r:Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    return-object v0
.end method
