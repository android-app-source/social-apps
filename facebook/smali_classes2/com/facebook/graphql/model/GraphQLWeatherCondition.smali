.class public final Lcom/facebook/graphql/model/GraphQLWeatherCondition;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLWeatherCondition$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLWeatherCondition$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327358
    const-class v0, Lcom/facebook/graphql/model/GraphQLWeatherCondition$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327357
    const-class v0, Lcom/facebook/graphql/model/GraphQLWeatherCondition$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 327355
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327356
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327352
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->h:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327353
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->h:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->h:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 327354
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->h:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 327340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327341
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 327342
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 327343
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 327344
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 327345
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 327346
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 327347
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 327348
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->l()Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 327349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327350
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 327351
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->l()Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 327317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327318
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327319
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 327320
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 327321
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLWeatherCondition;

    .line 327322
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327323
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 327324
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuantity;

    .line 327325
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->k()Lcom/facebook/graphql/model/GraphQLQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 327326
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLWeatherCondition;

    .line 327327
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    .line 327328
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327329
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327337
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327338
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->e:Ljava/lang/String;

    .line 327339
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 327336
    const v0, -0x503eb459

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327333
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327334
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 327335
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327330
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327331
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    .line 327332
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLWeatherCondition;->g:Lcom/facebook/graphql/model/GraphQLQuantity;

    return-object v0
.end method
