.class public final Lcom/facebook/graphql/model/GraphQLVideo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16n;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLVideo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLVideo$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Z

.field public C:Z

.field public D:I

.field public E:I

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:I

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:I

.field public W:I

.field public X:I

.field public Y:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;",
            ">;"
        }
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:I

.field public aC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:I

.field public aE:I

.field public aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:I

.field public aL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:I

.field public aN:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Z

.field public aV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Z

.field public ab:Z

.field public ac:Z

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:Z

.field public ak:Z

.field public al:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public ap:I

.field public aq:I

.field public ar:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public au:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:I

.field public bB:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bD:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public bE:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public bF:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public bG:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public bH:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bI:I

.field public bJ:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bK:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

.field public bL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bN:I

.field public bO:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bP:Z

.field public bQ:D

.field public bR:D

.field public bS:Z

.field public bT:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bU:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public bV:I

.field public bW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ba:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Z

.field public bc:Z

.field public bd:Z

.field public be:Z

.field public bf:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public bg:D

.field public bh:D

.field public bi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:I

.field public bm:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bq:Z

.field public br:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bu:I

.field public bv:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bw:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bx:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public by:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ca:I

.field public cb:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cc:I

.field public cd:I

.field public ce:Z

.field public cf:I

.field public cg:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:I

.field public m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:J

.field public v:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 289903
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 289904
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 289905
    const/16 v0, 0xa5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 289906
    return-void
.end method

.method public constructor <init>(LX/2oI;)V
    .locals 2

    .prologue
    .line 289907
    const/16 v0, 0xa5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 289908
    iget-object v0, p1, LX/2oI;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->e:Ljava/util/List;

    .line 289909
    iget-object v0, p1, LX/2oI;->c:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289910
    iget-object v0, p1, LX/2oI;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289911
    iget v0, p1, LX/2oI;->e:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->h:I

    .line 289912
    iget-object v0, p1, LX/2oI;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->i:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 289913
    iget-object v0, p1, LX/2oI;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->j:Ljava/lang/String;

    .line 289914
    iget-object v0, p1, LX/2oI;->h:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bK:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 289915
    iget-wide v0, p1, LX/2oI;->i:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->k:J

    .line 289916
    iget v0, p1, LX/2oI;->j:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->l:I

    .line 289917
    iget-object v0, p1, LX/2oI;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 289918
    iget-boolean v0, p1, LX/2oI;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->n:Z

    .line 289919
    iget-boolean v0, p1, LX/2oI;->m:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->o:Z

    .line 289920
    iget-boolean v0, p1, LX/2oI;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->p:Z

    .line 289921
    iget-boolean v0, p1, LX/2oI;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->q:Z

    .line 289922
    iget-object v0, p1, LX/2oI;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->r:Ljava/lang/String;

    .line 289923
    iget-object v0, p1, LX/2oI;->q:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bO:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 289924
    iget-object v0, p1, LX/2oI;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->s:Ljava/lang/String;

    .line 289925
    iget-object v0, p1, LX/2oI;->s:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->t:Ljava/util/List;

    .line 289926
    iget-wide v0, p1, LX/2oI;->t:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->u:J

    .line 289927
    iget-object v0, p1, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 289928
    iget-object v0, p1, LX/2oI;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->w:Ljava/lang/String;

    .line 289929
    iget-object v0, p1, LX/2oI;->w:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bJ:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 289930
    iget-boolean v0, p1, LX/2oI;->x:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bP:Z

    .line 289931
    iget-object v0, p1, LX/2oI;->y:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->x:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 289932
    iget-object v0, p1, LX/2oI;->z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289933
    iget-object v0, p1, LX/2oI;->A:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 289934
    iget-wide v0, p1, LX/2oI;->B:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bQ:D

    .line 289935
    iget-object v0, p1, LX/2oI;->C:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cg:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    .line 289936
    iget-object v0, p1, LX/2oI;->D:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->A:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 289937
    iget-boolean v0, p1, LX/2oI;->E:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->B:Z

    .line 289938
    iget-boolean v0, p1, LX/2oI;->F:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->C:Z

    .line 289939
    iget v0, p1, LX/2oI;->G:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->D:I

    .line 289940
    iget v0, p1, LX/2oI;->H:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->E:I

    .line 289941
    iget-object v0, p1, LX/2oI;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bW:Ljava/lang/String;

    .line 289942
    iget-object v0, p1, LX/2oI;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bX:Ljava/lang/String;

    .line 289943
    iget-object v0, p1, LX/2oI;->K:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->F:Ljava/lang/String;

    .line 289944
    iget-object v0, p1, LX/2oI;->L:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->G:Ljava/lang/String;

    .line 289945
    iget v0, p1, LX/2oI;->M:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->H:I

    .line 289946
    iget-object v0, p1, LX/2oI;->N:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->I:Ljava/lang/String;

    .line 289947
    iget-object v0, p1, LX/2oI;->O:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289948
    iget-object v0, p1, LX/2oI;->P:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289949
    iget-object v0, p1, LX/2oI;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289950
    iget-object v0, p1, LX/2oI;->R:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289951
    iget-object v0, p1, LX/2oI;->S:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289952
    iget-object v0, p1, LX/2oI;->T:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289953
    iget-object v0, p1, LX/2oI;->U:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289954
    iget-object v0, p1, LX/2oI;->V:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289955
    iget-object v0, p1, LX/2oI;->W:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289956
    iget-object v0, p1, LX/2oI;->X:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289957
    iget-object v0, p1, LX/2oI;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289958
    iget-object v0, p1, LX/2oI;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289959
    iget v0, p1, LX/2oI;->aa:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->V:I

    .line 289960
    iget v0, p1, LX/2oI;->ab:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->W:I

    .line 289961
    iget v0, p1, LX/2oI;->ac:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->X:I

    .line 289962
    iget-object v0, p1, LX/2oI;->ad:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Y:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 289963
    iget-object v0, p1, LX/2oI;->ae:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Z:Ljava/util/List;

    .line 289964
    iget-boolean v0, p1, LX/2oI;->af:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aa:Z

    .line 289965
    iget-boolean v0, p1, LX/2oI;->ag:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ab:Z

    .line 289966
    iget-boolean v0, p1, LX/2oI;->ah:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ac:Z

    .line 289967
    iget-boolean v0, p1, LX/2oI;->ai:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ad:Z

    .line 289968
    iget-boolean v0, p1, LX/2oI;->aj:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bS:Z

    .line 289969
    iget-boolean v0, p1, LX/2oI;->ak:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ae:Z

    .line 289970
    iget-boolean v0, p1, LX/2oI;->al:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->af:Z

    .line 289971
    iget-boolean v0, p1, LX/2oI;->am:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ag:Z

    .line 289972
    iget-boolean v0, p1, LX/2oI;->an:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ce:Z

    .line 289973
    iget-boolean v0, p1, LX/2oI;->ao:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ah:Z

    .line 289974
    iget-boolean v0, p1, LX/2oI;->ap:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ai:Z

    .line 289975
    iget-boolean v0, p1, LX/2oI;->aq:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aj:Z

    .line 289976
    iget-boolean v0, p1, LX/2oI;->ar:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ak:Z

    .line 289977
    iget-object v0, p1, LX/2oI;->as:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->al:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289978
    iget-object v0, p1, LX/2oI;->at:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289979
    iget-object v0, p1, LX/2oI;->au:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289980
    iget v0, p1, LX/2oI;->av:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bU:I

    .line 289981
    iget v0, p1, LX/2oI;->aw:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bV:I

    .line 289982
    iget v0, p1, LX/2oI;->ax:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ao:I

    .line 289983
    iget v0, p1, LX/2oI;->ay:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ap:I

    .line 289984
    iget v0, p1, LX/2oI;->az:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aq:I

    .line 289985
    iget-object v0, p1, LX/2oI;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289986
    iget v0, p1, LX/2oI;->aB:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cd:I

    .line 289987
    iget-object v0, p1, LX/2oI;->aC:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 289988
    iget-object v0, p1, LX/2oI;->aD:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->at:Ljava/util/List;

    .line 289989
    iget-object v0, p1, LX/2oI;->aE:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->au:Ljava/lang/String;

    .line 289990
    iget-object v0, p1, LX/2oI;->aF:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->av:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289991
    iget-object v0, p1, LX/2oI;->aG:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aw:Ljava/lang/String;

    .line 289992
    iget-object v0, p1, LX/2oI;->aH:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ax:Ljava/lang/String;

    .line 289993
    iget-object v0, p1, LX/2oI;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289994
    iget-object v0, p1, LX/2oI;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289995
    iget v0, p1, LX/2oI;->aK:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bN:I

    .line 289996
    iget-wide v0, p1, LX/2oI;->aL:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bR:D

    .line 289997
    iget v0, p1, LX/2oI;->aM:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cc:I

    .line 289998
    iget-object v0, p1, LX/2oI;->aN:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    .line 289999
    iget v0, p1, LX/2oI;->aO:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ca:I

    .line 290000
    iget v0, p1, LX/2oI;->aP:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aB:I

    .line 290001
    iget-object v0, p1, LX/2oI;->aQ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aC:Ljava/lang/String;

    .line 290002
    iget v0, p1, LX/2oI;->aR:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aD:I

    .line 290003
    iget v0, p1, LX/2oI;->aS:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aE:I

    .line 290004
    iget-object v0, p1, LX/2oI;->aT:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aF:Ljava/lang/String;

    .line 290005
    iget-object v0, p1, LX/2oI;->aU:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aG:Ljava/lang/String;

    .line 290006
    iget-object v0, p1, LX/2oI;->aV:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aH:Ljava/lang/String;

    .line 290007
    iget-object v0, p1, LX/2oI;->aW:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aI:Ljava/lang/String;

    .line 290008
    iget-object v0, p1, LX/2oI;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290009
    iget v0, p1, LX/2oI;->aY:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aK:I

    .line 290010
    iget-object v0, p1, LX/2oI;->aZ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aL:Ljava/lang/String;

    .line 290011
    iget v0, p1, LX/2oI;->ba:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aM:I

    .line 290012
    iget-object v0, p1, LX/2oI;->bb:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290013
    iget-object v0, p1, LX/2oI;->bc:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290014
    iget-object v0, p1, LX/2oI;->bd:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290015
    iget-object v0, p1, LX/2oI;->be:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290016
    iget-object v0, p1, LX/2oI;->bf:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290017
    iget-object v0, p1, LX/2oI;->bg:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290018
    iget-object v0, p1, LX/2oI;->bh:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aS:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 290019
    iget-object v0, p1, LX/2oI;->bi:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290020
    iget-boolean v0, p1, LX/2oI;->bj:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aU:Z

    .line 290021
    iget-object v0, p1, LX/2oI;->bk:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aV:Ljava/lang/String;

    .line 290022
    iget-object v0, p1, LX/2oI;->bl:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aW:Ljava/lang/String;

    .line 290023
    iget-object v0, p1, LX/2oI;->bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290024
    iget-object v0, p1, LX/2oI;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290025
    iget-object v0, p1, LX/2oI;->bo:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290026
    iget-object v0, p1, LX/2oI;->bp:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290027
    iget-object v0, p1, LX/2oI;->bq:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ba:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 290028
    iget-object v0, p1, LX/2oI;->br:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bY:Ljava/lang/String;

    .line 290029
    iget-boolean v0, p1, LX/2oI;->bs:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bb:Z

    .line 290030
    iget-boolean v0, p1, LX/2oI;->bt:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bc:Z

    .line 290031
    iget-boolean v0, p1, LX/2oI;->bu:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bd:Z

    .line 290032
    iget-boolean v0, p1, LX/2oI;->bv:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->be:Z

    .line 290033
    iget-boolean v0, p1, LX/2oI;->bw:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bf:Z

    .line 290034
    iget-wide v0, p1, LX/2oI;->bx:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bg:D

    .line 290035
    iget-wide v0, p1, LX/2oI;->by:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bh:D

    .line 290036
    iget-object v0, p1, LX/2oI;->bz:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bi:Ljava/lang/String;

    .line 290037
    iget-object v0, p1, LX/2oI;->bA:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bj:Ljava/lang/String;

    .line 290038
    iget-object v0, p1, LX/2oI;->bB:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bk:Ljava/lang/String;

    .line 290039
    iget v0, p1, LX/2oI;->bC:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bl:I

    .line 290040
    iget-object v0, p1, LX/2oI;->bD:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bZ:Ljava/lang/String;

    .line 290041
    iget-object v0, p1, LX/2oI;->bE:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bm:Lcom/facebook/graphql/model/GraphQLPage;

    .line 290042
    iget-object v0, p1, LX/2oI;->bF:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290043
    iget-object v0, p1, LX/2oI;->bG:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bo:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 290044
    iget-object v0, p1, LX/2oI;->bH:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bp:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 290045
    iget-boolean v0, p1, LX/2oI;->bI:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bq:Z

    .line 290046
    iget-object v0, p1, LX/2oI;->bJ:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->br:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290047
    iget-object v0, p1, LX/2oI;->bK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290048
    iget-object v0, p1, LX/2oI;->bL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290049
    iget v0, p1, LX/2oI;->bM:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bu:I

    .line 290050
    iget-object v0, p1, LX/2oI;->bN:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bv:Ljava/lang/String;

    .line 290051
    iget-object v0, p1, LX/2oI;->bO:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290052
    iget-object v0, p1, LX/2oI;->bP:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bx:Ljava/util/List;

    .line 290053
    iget-object v0, p1, LX/2oI;->bQ:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->by:Ljava/lang/String;

    .line 290054
    iget-object v0, p1, LX/2oI;->bR:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bz:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 290055
    iget v0, p1, LX/2oI;->bS:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bA:I

    .line 290056
    iget-object v0, p1, LX/2oI;->bT:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290057
    iget-object v0, p1, LX/2oI;->bU:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bC:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 290058
    iget-object v0, p1, LX/2oI;->bV:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bD:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 290059
    iget-object v0, p1, LX/2oI;->bW:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bL:Ljava/lang/String;

    .line 290060
    iget-object v0, p1, LX/2oI;->bX:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bM:Ljava/lang/String;

    .line 290061
    iget v0, p1, LX/2oI;->bY:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cf:I

    .line 290062
    iget-object v0, p1, LX/2oI;->bZ:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 290063
    iget-object v0, p1, LX/2oI;->ca:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bF:Ljava/util/List;

    .line 290064
    iget-object v0, p1, LX/2oI;->cb:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bG:Ljava/util/List;

    .line 290065
    iget-object v0, p1, LX/2oI;->cc:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290066
    iget v0, p1, LX/2oI;->cd:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bI:I

    .line 290067
    return-void
.end method

.method private bA()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290068
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->y:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290069
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->y:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290070
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->y:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bB()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290071
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290072
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->F:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->F:Ljava/lang/String;

    .line 290073
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->F:Ljava/lang/String;

    return-object v0
.end method

.method private bC()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290074
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->T:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290075
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->T:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290076
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->T:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bD()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290077
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290078
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290079
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ad:Z

    return v0
.end method

.method private bE()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290080
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->at:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290081
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->at:Ljava/util/List;

    const/16 v1, 0x44

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->at:Ljava/util/List;

    .line 290082
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->at:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bF()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289897
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ax:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289898
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ax:Ljava/lang/String;

    const/16 v1, 0x48

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ax:Ljava/lang/String;

    .line 289899
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ax:Ljava/lang/String;

    return-object v0
.end method

.method private bG()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290086
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aG:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290087
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aG:Ljava/lang/String;

    const/16 v1, 0x52

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aG:Ljava/lang/String;

    .line 290088
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aG:Ljava/lang/String;

    return-object v0
.end method

.method private bH()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290089
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aH:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290090
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aH:Ljava/lang/String;

    const/16 v1, 0x53

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aH:Ljava/lang/String;

    .line 290091
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aH:Ljava/lang/String;

    return-object v0
.end method

.method private bI()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290092
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290093
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290094
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aM:I

    return v0
.end method

.method private bJ()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290095
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290096
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x59

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290097
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bK()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290098
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290099
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290100
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bL()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290101
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290102
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290103
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bM()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290104
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290105
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290106
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bN()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290107
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290108
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290109
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bO()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290110
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aS:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290111
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aS:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aS:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 290112
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aS:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private bP()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290113
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290114
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290115
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bQ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289864
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289865
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289866
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aU:Z

    return v0
.end method

.method private bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289834
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289835
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x66

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 289836
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bS()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289837
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ba:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289838
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ba:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    const/16 v1, 0x67

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ba:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 289839
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ba:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    return-object v0
.end method

.method private bT()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289840
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bk:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289841
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bk:Ljava/lang/String;

    const/16 v1, 0x71

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bk:Ljava/lang/String;

    .line 289842
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bk:Ljava/lang/String;

    return-object v0
.end method

.method private bU()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289843
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->br:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289844
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->br:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x78

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->br:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289845
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->br:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289846
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289847
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x79

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 289848
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bW()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289849
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289850
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x7a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 289851
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private bX()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->by:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->by:Ljava/lang/String;

    const/16 v1, 0x7f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->by:Ljava/lang/String;

    .line 289854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->by:Ljava/lang/String;

    return-object v0
.end method

.method private bY()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x85

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 289857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method private bZ()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289858
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bF:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289859
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bF:Ljava/util/List;

    const/16 v1, 0x86

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bF:Ljava/util/List;

    .line 289860
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bF:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bu()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289861
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289862
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->e:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->e:Ljava/util/List;

    .line 289863
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bv()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289831
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289832
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289833
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private bw()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289867
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289868
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289869
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->n:Z

    return v0
.end method

.method private bx()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289870
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289871
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->s:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->s:Ljava/lang/String;

    .line 289872
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->s:Ljava/lang/String;

    return-object v0
.end method

.method private by()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289873
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289874
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->t:Ljava/util/List;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->t:Ljava/util/List;

    .line 289875
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->t:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private bz()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289876
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289877
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->w:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->w:Ljava/lang/String;

    .line 289878
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->w:Ljava/lang/String;

    return-object v0
.end method

.method private ca()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289879
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bG:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289880
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bG:Ljava/util/List;

    const/16 v1, 0x87

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bG:Ljava/util/List;

    .line 289881
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bG:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private cb()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289882
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bJ:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289883
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bJ:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    const/16 v1, 0x8a

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bJ:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 289884
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bJ:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    return-object v0
.end method

.method private cc()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289885
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bL:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289886
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bL:Ljava/lang/String;

    const/16 v1, 0x8c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bL:Ljava/lang/String;

    .line 289887
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bL:Ljava/lang/String;

    return-object v0
.end method

.method private cd()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289888
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bM:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289889
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bM:Ljava/lang/String;

    const/16 v1, 0x8d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bM:Ljava/lang/String;

    .line 289890
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bM:Ljava/lang/String;

    return-object v0
.end method

.method private ce()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289891
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289892
    const/16 v0, 0x11

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289893
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bN:I

    return v0
.end method

.method private cf()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289894
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bT:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289895
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bT:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x96

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289896
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bT:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private cg()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 290813
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290814
    const/16 v0, 0x12

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290815
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bU:I

    return v0
.end method

.method private ch()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290149
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290150
    const/16 v0, 0x13

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290151
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bV:I

    return v0
.end method

.method private ci()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290816
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290817
    const/16 v0, 0x13

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290818
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ca:I

    return v0
.end method

.method private cj()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290819
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cb:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290820
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cb:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290821
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cb:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ck()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290822
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290823
    const/16 v0, 0x13

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290824
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cc:I

    return v0
.end method

.method private cl()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290825
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290826
    const/16 v0, 0x14

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290827
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cf:I

    return v0
.end method

.method private cm()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290828
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cg:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290829
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cg:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    const/16 v1, 0xa3

    const-class v2, Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cg:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    .line 290830
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cg:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290831
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290832
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290833
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->B:Z

    return v0
.end method

.method public final B()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290834
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290835
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290836
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->C:Z

    return v0
.end method

.method public final C()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290837
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290838
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290839
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->D:I

    return v0
.end method

.method public final D()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 290840
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290841
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290842
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->E:I

    return v0
.end method

.method public final E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290870
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290871
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->G:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->G:Ljava/lang/String;

    .line 290872
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final F()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290843
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290844
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290845
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->H:I

    return v0
.end method

.method public final G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290867
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290868
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->I:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->I:Ljava/lang/String;

    .line 290869
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290864
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->J:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290865
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->J:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290866
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->J:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290861
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->K:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290862
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->K:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290863
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->K:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290858
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->L:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290859
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->L:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290860
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->L:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->M:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->M:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->M:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->N:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->N:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->N:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290849
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->O:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290850
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->O:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290851
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->O:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290810
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->P:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290811
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->P:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290812
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->P:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290846
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290847
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290848
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final P()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290119
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->R:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290120
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->R:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290121
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->R:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290122
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->S:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290123
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->S:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290124
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->S:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290125
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->U:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290126
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->U:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290127
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->U:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final S()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290128
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290129
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290130
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->V:I

    return v0
.end method

.method public final T()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 290131
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290132
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290133
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->W:I

    return v0
.end method

.method public final U()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290134
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290135
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290136
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->X:I

    return v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290137
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Y:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290138
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Y:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Y:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 290139
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Y:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    return-object v0
.end method

.method public final W()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290140
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Z:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290141
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Z:Ljava/util/List;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Z:Ljava/util/List;

    .line 290142
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->Z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final X()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290143
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290144
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290145
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aa:Z

    return v0
.end method

.method public final Y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290146
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290147
    const/4 v0, 0x6

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290148
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ab:Z

    return v0
.end method

.method public final Z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290116
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290117
    const/4 v0, 0x6

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290118
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ac:Z

    return v0
.end method

.method public final a(LX/186;)I
    .locals 98

    .prologue
    .line 290152
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 290153
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bu()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 290154
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 290155
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 290156
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 290157
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 290158
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->v()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 290159
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bx()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 290160
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->by()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 290161
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 290162
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bz()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 290163
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->y()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 290164
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 290165
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 290166
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 290167
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bB()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 290168
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->E()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 290169
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 290170
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 290171
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 290172
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 290173
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 290174
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 290175
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 290176
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 290177
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 290178
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 290179
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 290180
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 290181
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 290182
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->V()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 290183
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->W()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v33

    .line 290184
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 290185
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 290186
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 290187
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 290188
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 290189
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bE()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/util/List;)I

    move-result v39

    .line 290190
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ap()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 290191
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 290192
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ar()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v42

    .line 290193
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bF()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 290194
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 290195
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 290196
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->au()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 290197
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aw()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v47

    .line 290198
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 290199
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bG()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 290200
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bH()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v50

    .line 290201
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aA()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v51

    .line 290202
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aB()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 290203
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aD()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 290204
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 290205
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 290206
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 290207
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 290208
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v58

    .line 290209
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bO()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 290210
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bP()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 290211
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aE()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v61

    .line 290212
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aF()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v62

    .line 290213
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 290214
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 290215
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 290216
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bS()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 290217
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aP()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v67

    .line 290218
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aQ()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v68

    .line 290219
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bT()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v69

    .line 290220
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aS()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v70

    .line 290221
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aT()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v71

    .line 290222
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aU()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v72

    .line 290223
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aV()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 290224
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v74

    .line 290225
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v75

    .line 290226
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bW()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 290227
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aY()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v77

    .line 290228
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v78

    .line 290229
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v79

    .line 290230
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bX()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v80

    .line 290231
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v81

    .line 290232
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bd()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v82

    .line 290233
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->be()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v83

    .line 290234
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bZ()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v84

    .line 290235
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ca()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v85

    .line 290236
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v86

    .line 290237
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cb()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v87

    .line 290238
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cc()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v88

    .line 290239
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cd()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v89

    .line 290240
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bj()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v90

    .line 290241
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v91

    .line 290242
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bo()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v92

    .line 290243
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bp()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v93

    .line 290244
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bq()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v94

    .line 290245
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->br()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v95

    .line 290246
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v96

    .line 290247
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cm()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v97

    .line 290248
    const/16 v7, 0xa4

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 290249
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 290250
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 290251
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 290252
    const/4 v2, 0x4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->m()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290253
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 290254
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 290255
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->p()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 290256
    const/16 v2, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->q()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290257
    const/16 v3, 0x9

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 290258
    const/16 v2, 0xa

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bw()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290259
    const/16 v2, 0xb

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->s()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290260
    const/16 v2, 0xc

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290261
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->u()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290262
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 290263
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 290264
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 290265
    const/16 v3, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->w()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 290266
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 290267
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 290268
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 290269
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 290270
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 290271
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290272
    const/16 v2, 0x18

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290273
    const/16 v2, 0x19

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->B()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290274
    const/16 v2, 0x1a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->C()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290275
    const/16 v2, 0x1b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->D()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290276
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290277
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290278
    const/16 v2, 0x1e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290279
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290280
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290281
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290282
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290283
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290284
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290285
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290286
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290287
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290288
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290289
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290290
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290291
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290292
    const/16 v2, 0x2c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->S()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290293
    const/16 v2, 0x2d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->T()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290294
    const/16 v2, 0x2e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->U()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290295
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290296
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290297
    const/16 v2, 0x31

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->X()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290298
    const/16 v2, 0x32

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290299
    const/16 v2, 0x33

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290300
    const/16 v2, 0x34

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bD()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290301
    const/16 v2, 0x35

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290302
    const/16 v2, 0x36

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ab()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290303
    const/16 v2, 0x37

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ac()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290304
    const/16 v2, 0x38

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ad()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290305
    const/16 v2, 0x39

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ae()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290306
    const/16 v2, 0x3a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290307
    const/16 v2, 0x3b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290308
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290309
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290310
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290311
    const/16 v2, 0x3f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ak()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290312
    const/16 v2, 0x40

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->al()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290313
    const/16 v2, 0x41

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->am()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290314
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290315
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290316
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290317
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290318
    const/16 v2, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290319
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290320
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290321
    const/16 v2, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290322
    const/16 v2, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290323
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290324
    const/16 v2, 0x4c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->av()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290325
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290326
    const/16 v2, 0x4f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ax()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290327
    const/16 v2, 0x50

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290328
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290329
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290330
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290331
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290332
    const/16 v2, 0x55

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290333
    const/16 v2, 0x56

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aC()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290334
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290335
    const/16 v2, 0x58

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bI()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290336
    const/16 v2, 0x59

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290337
    const/16 v2, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290338
    const/16 v2, 0x5b

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290339
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290340
    const/16 v2, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290341
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290342
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290343
    const/16 v2, 0x60

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bQ()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290344
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290345
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290346
    const/16 v2, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290347
    const/16 v2, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290348
    const/16 v2, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290349
    const/16 v2, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290350
    const/16 v2, 0x68

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aI()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290351
    const/16 v2, 0x69

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aJ()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290352
    const/16 v2, 0x6a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aK()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290353
    const/16 v2, 0x6b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aL()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290354
    const/16 v2, 0x6c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aM()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290355
    const/16 v3, 0x6d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aN()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 290356
    const/16 v3, 0x6e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aO()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 290357
    const/16 v2, 0x6f

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290358
    const/16 v2, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290359
    const/16 v2, 0x71

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290360
    const/16 v2, 0x72

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aR()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290361
    const/16 v2, 0x73

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290362
    const/16 v2, 0x74

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290363
    const/16 v2, 0x75

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290364
    const/16 v2, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290365
    const/16 v2, 0x77

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aW()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290366
    const/16 v2, 0x78

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290367
    const/16 v2, 0x79

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290368
    const/16 v2, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290369
    const/16 v2, 0x7b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aX()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290370
    const/16 v2, 0x7c

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290371
    const/16 v2, 0x7d

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290372
    const/16 v2, 0x7e

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290373
    const/16 v2, 0x7f

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290374
    const/16 v2, 0x80

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290375
    const/16 v2, 0x81

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bc()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290376
    const/16 v2, 0x82

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290377
    const/16 v2, 0x83

    move-object/from16 v0, p1

    move/from16 v1, v83

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290378
    const/16 v3, 0x84

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 290379
    const/16 v3, 0x85

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bY()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 290380
    const/16 v2, 0x86

    move-object/from16 v0, p1

    move/from16 v1, v84

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290381
    const/16 v2, 0x87

    move-object/from16 v0, p1

    move/from16 v1, v85

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290382
    const/16 v2, 0x88

    move-object/from16 v0, p1

    move/from16 v1, v86

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290383
    const/16 v2, 0x89

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290384
    const/16 v2, 0x8a

    move-object/from16 v0, p1

    move/from16 v1, v87

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290385
    const/16 v3, 0x8b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bi()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 290386
    const/16 v2, 0x8c

    move-object/from16 v0, p1

    move/from16 v1, v88

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290387
    const/16 v2, 0x8d

    move-object/from16 v0, p1

    move/from16 v1, v89

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290388
    const/16 v2, 0x8e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ce()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290389
    const/16 v2, 0x91

    move-object/from16 v0, p1

    move/from16 v1, v90

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290390
    const/16 v2, 0x92

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bk()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290391
    const/16 v3, 0x93

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bl()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 290392
    const/16 v3, 0x94

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bm()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 290393
    const/16 v2, 0x95

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bn()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290394
    const/16 v2, 0x96

    move-object/from16 v0, p1

    move/from16 v1, v91

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290395
    const/16 v2, 0x97

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cg()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290396
    const/16 v2, 0x98

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ch()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290397
    const/16 v2, 0x99

    move-object/from16 v0, p1

    move/from16 v1, v92

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290398
    const/16 v2, 0x9a

    move-object/from16 v0, p1

    move/from16 v1, v93

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290399
    const/16 v2, 0x9b

    move-object/from16 v0, p1

    move/from16 v1, v94

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290400
    const/16 v2, 0x9c

    move-object/from16 v0, p1

    move/from16 v1, v95

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290401
    const/16 v2, 0x9d

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ci()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290402
    const/16 v2, 0x9e

    move-object/from16 v0, p1

    move/from16 v1, v96

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290403
    const/16 v2, 0x9f

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ck()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290404
    const/16 v2, 0xa0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bs()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290405
    const/16 v2, 0xa1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bt()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 290406
    const/16 v2, 0xa2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cl()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 290407
    const/16 v2, 0xa3

    move-object/from16 v0, p1

    move/from16 v1, v97

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 290408
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 290409
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 290410
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    goto/16 :goto_0

    .line 290411
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v2

    goto/16 :goto_1

    .line 290412
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bY()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto/16 :goto_2

    .line 290413
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bi()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 290414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 290415
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bu()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3f

    .line 290416
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bu()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 290417
    if-eqz v1, :cond_3f

    .line 290418
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290419
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLVideo;->e:Ljava/util/List;

    move-object v1, v0

    .line 290420
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 290421
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290422
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bv()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 290423
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290424
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290425
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 290426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290427
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 290428
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290429
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290430
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 290431
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 290432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->n()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 290433
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290434
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->i:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 290435
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bj()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 290436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bj()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 290437
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bj()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 290438
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290439
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bO:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 290440
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 290441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 290442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 290443
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290444
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 290445
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cb()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 290446
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cb()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 290447
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cb()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 290448
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290449
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bJ:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharity;

    .line 290450
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->y()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 290451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->y()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 290452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->y()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 290453
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290454
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->x:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 290455
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 290456
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290457
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 290458
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290459
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290460
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 290461
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 290462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 290463
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290464
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 290465
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cm()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 290466
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cm()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    .line 290467
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cm()Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 290468
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290469
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->cg:Lcom/facebook/graphql/model/GraphQLFundraiserToCharity;

    .line 290470
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 290471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 290472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 290473
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290474
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->A:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 290475
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 290476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 290478
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290479
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290480
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 290481
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290482
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 290483
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290484
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290485
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 290486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290487
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 290488
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290489
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290490
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 290491
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 290493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290494
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290495
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 290496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 290498
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290499
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290500
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 290501
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 290503
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290504
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290505
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 290506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 290508
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290509
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290510
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 290511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 290513
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290514
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->Q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290515
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 290516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 290518
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290519
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->R:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290520
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 290521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290522
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 290523
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290524
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290525
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 290526
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290527
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 290528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290529
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290530
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 290531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 290533
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290534
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290535
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->V()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 290536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->V()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 290537
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->V()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 290538
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290539
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->Y:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 290540
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->W()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 290541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->W()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 290542
    if-eqz v2, :cond_18

    .line 290543
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290544
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLVideo;->Z:Ljava/util/List;

    move-object v1, v0

    .line 290545
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 290546
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ah()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 290548
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290549
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->al:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290550
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 290551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 290553
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290554
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290555
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 290556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 290558
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290559
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290560
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 290561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 290563
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290564
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290565
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 290566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 290568
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290569
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290570
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 290571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aq()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 290573
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290574
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->av:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290575
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 290576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 290578
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290579
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290580
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 290581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 290583
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290584
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290585
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->au()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 290586
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->au()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 290587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->au()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 290588
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290589
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    .line 290590
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aB()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 290591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aB()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290592
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aB()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 290593
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290594
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290595
    :cond_22
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 290596
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290597
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 290598
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290599
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290600
    :cond_23
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 290601
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290602
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 290603
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290604
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aO:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290605
    :cond_24
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 290606
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290607
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cf()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 290608
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290609
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290610
    :cond_25
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 290611
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290612
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bL()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 290613
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290614
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290615
    :cond_26
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 290616
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290617
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 290618
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290619
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290620
    :cond_27
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 290621
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290622
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 290623
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290624
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290625
    :cond_28
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bO()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 290626
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bO()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 290627
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bO()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 290628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290629
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aS:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 290630
    :cond_29
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bP()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 290631
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bP()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290632
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bP()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 290633
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290634
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aT:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290635
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 290636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290637
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 290638
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290639
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290640
    :cond_2b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 290641
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290642
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->cj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 290643
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290644
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->cb:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290645
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 290646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290647
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aH()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 290648
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290649
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290650
    :cond_2d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 290651
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290652
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 290653
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290654
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->aZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290655
    :cond_2e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bS()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 290656
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bS()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 290657
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bS()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 290658
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290659
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->ba:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 290660
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aS()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 290661
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aS()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 290662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aS()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 290663
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290664
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bm:Lcom/facebook/graphql/model/GraphQLPage;

    .line 290665
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aT()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 290666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aT()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290667
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aT()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 290668
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290669
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290670
    :cond_31
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aU()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 290671
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aU()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 290672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aU()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 290673
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290674
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bo:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 290675
    :cond_32
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aV()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 290676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aV()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 290677
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aV()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 290678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290679
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bp:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 290680
    :cond_33
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 290681
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290682
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_34

    .line 290683
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290684
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->br:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290685
    :cond_34
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 290686
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290687
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_35

    .line 290688
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290689
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290690
    :cond_35
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bW()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 290691
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bW()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290692
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bW()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 290693
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290694
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bt:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 290695
    :cond_36
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 290696
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290697
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 290698
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290699
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290700
    :cond_37
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 290701
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 290702
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v2

    if-eq v2, v0, :cond_38

    .line 290703
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290704
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bz:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 290705
    :cond_38
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bd()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 290706
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bd()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290707
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bd()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_39

    .line 290708
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290709
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290710
    :cond_39
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->be()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 290711
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->be()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 290712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->be()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v2

    if-eq v2, v0, :cond_3a

    .line 290713
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290714
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bC:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 290715
    :cond_3a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bZ()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 290716
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bZ()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 290717
    if-eqz v2, :cond_3b

    .line 290718
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290719
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLVideo;->bF:Ljava/util/List;

    move-object v1, v0

    .line 290720
    :cond_3b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ca()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 290721
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ca()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 290722
    if-eqz v2, :cond_3c

    .line 290723
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290724
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLVideo;->bG:Ljava/util/List;

    move-object v1, v0

    .line 290725
    :cond_3c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 290726
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 290727
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->bg()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3d

    .line 290728
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 290729
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLVideo;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290730
    :cond_3d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 290731
    if-nez v1, :cond_3e

    :goto_1
    return-object p0

    :cond_3e
    move-object p0, v1

    goto :goto_1

    :cond_3f
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290732
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 290733
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 290734
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->h:I

    .line 290735
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->k:J

    .line 290736
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->l:I

    .line 290737
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->n:Z

    .line 290738
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->o:Z

    .line 290739
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->p:Z

    .line 290740
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->q:Z

    .line 290741
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->u:J

    .line 290742
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->B:Z

    .line 290743
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->C:Z

    .line 290744
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->D:I

    .line 290745
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->E:I

    .line 290746
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->H:I

    .line 290747
    const/16 v0, 0x2c

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->V:I

    .line 290748
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->W:I

    .line 290749
    const/16 v0, 0x2e

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->X:I

    .line 290750
    const/16 v0, 0x31

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aa:Z

    .line 290751
    const/16 v0, 0x32

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ab:Z

    .line 290752
    const/16 v0, 0x33

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ac:Z

    .line 290753
    const/16 v0, 0x34

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ad:Z

    .line 290754
    const/16 v0, 0x35

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ae:Z

    .line 290755
    const/16 v0, 0x36

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->af:Z

    .line 290756
    const/16 v0, 0x37

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ag:Z

    .line 290757
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ah:Z

    .line 290758
    const/16 v0, 0x39

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ai:Z

    .line 290759
    const/16 v0, 0x3a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aj:Z

    .line 290760
    const/16 v0, 0x3b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ak:Z

    .line 290761
    const/16 v0, 0x3f

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ao:I

    .line 290762
    const/16 v0, 0x40

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ap:I

    .line 290763
    const/16 v0, 0x41

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aq:I

    .line 290764
    const/16 v0, 0x4c

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aB:I

    .line 290765
    const/16 v0, 0x4f

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aD:I

    .line 290766
    const/16 v0, 0x50

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aE:I

    .line 290767
    const/16 v0, 0x56

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aK:I

    .line 290768
    const/16 v0, 0x58

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aM:I

    .line 290769
    const/16 v0, 0x60

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aU:Z

    .line 290770
    const/16 v0, 0x68

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bb:Z

    .line 290771
    const/16 v0, 0x69

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bc:Z

    .line 290772
    const/16 v0, 0x6a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bd:Z

    .line 290773
    const/16 v0, 0x6b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->be:Z

    .line 290774
    const/16 v0, 0x6c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bf:Z

    .line 290775
    const/16 v0, 0x6d

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bg:D

    .line 290776
    const/16 v0, 0x6e

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bh:D

    .line 290777
    const/16 v0, 0x72

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bl:I

    .line 290778
    const/16 v0, 0x77

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bq:Z

    .line 290779
    const/16 v0, 0x7b

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bu:I

    .line 290780
    const/16 v0, 0x81

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bA:I

    .line 290781
    const/16 v0, 0x89

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bI:I

    .line 290782
    const/16 v0, 0x8e

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bN:I

    .line 290783
    const/16 v0, 0x92

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bP:Z

    .line 290784
    const/16 v0, 0x93

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bQ:D

    .line 290785
    const/16 v0, 0x94

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bR:D

    .line 290786
    const/16 v0, 0x95

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bS:Z

    .line 290787
    const/16 v0, 0x97

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bU:I

    .line 290788
    const/16 v0, 0x98

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bV:I

    .line 290789
    const/16 v0, 0x9d

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ca:I

    .line 290790
    const/16 v0, 0x9f

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cc:I

    .line 290791
    const/16 v0, 0xa0

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cd:I

    .line 290792
    const/16 v0, 0xa1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ce:Z

    .line 290793
    const/16 v0, 0xa2

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cf:I

    .line 290794
    return-void
.end method

.method public final aA()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290795
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290796
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aI:Ljava/lang/String;

    const/16 v1, 0x54

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aI:Ljava/lang/String;

    .line 290797
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aI:Ljava/lang/String;

    return-object v0
.end method

.method public final aB()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290798
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290799
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x55

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 290800
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aC()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 290801
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 290802
    const/16 v0, 0xa

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 290803
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aK:I

    return v0
.end method

.method public final aD()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290804
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aL:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290805
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aL:Ljava/lang/String;

    const/16 v1, 0x57

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aL:Ljava/lang/String;

    .line 290806
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aL:Ljava/lang/String;

    return-object v0
.end method

.method public final aE()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290807
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aV:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290808
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aV:Ljava/lang/String;

    const/16 v1, 0x61

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aV:Ljava/lang/String;

    .line 290809
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aV:Ljava/lang/String;

    return-object v0
.end method

.method public final aF()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290083
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aW:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 290084
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aW:Ljava/lang/String;

    const/16 v1, 0x62

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aW:Ljava/lang/String;

    .line 290085
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aW:Ljava/lang/String;

    return-object v0
.end method

.method public final aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289900
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289901
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x63

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 289902
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aX:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aH()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289593
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289594
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x64

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289595
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aY:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aI()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289647
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289648
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289649
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bb:Z

    return v0
.end method

.method public final aJ()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289650
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289651
    const/16 v0, 0xd

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289652
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bc:Z

    return v0
.end method

.method public final aK()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289653
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289654
    const/16 v0, 0xd

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289655
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bd:Z

    return v0
.end method

.method public final aL()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289656
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289657
    const/16 v0, 0xd

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289658
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->be:Z

    return v0
.end method

.method public final aM()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 289659
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289660
    const/16 v0, 0xd

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289661
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bf:Z

    return v0
.end method

.method public final aN()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289662
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289663
    const/16 v0, 0xd

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289664
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bg:D

    return-wide v0
.end method

.method public final aO()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289665
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289666
    const/16 v0, 0xd

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289667
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bh:D

    return-wide v0
.end method

.method public final aP()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289668
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bi:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289669
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bi:Ljava/lang/String;

    const/16 v1, 0x6f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bi:Ljava/lang/String;

    .line 289670
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bi:Ljava/lang/String;

    return-object v0
.end method

.method public final aQ()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bj:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bj:Ljava/lang/String;

    const/16 v1, 0x70

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bj:Ljava/lang/String;

    .line 289673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bj:Ljava/lang/String;

    return-object v0
.end method

.method public final aR()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289674
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289675
    const/16 v0, 0xe

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289676
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bl:I

    return v0
.end method

.method public final aS()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289644
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bm:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289645
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bm:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x73

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bm:Lcom/facebook/graphql/model/GraphQLPage;

    .line 289646
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bm:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final aT()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289680
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289681
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x74

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289682
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aU()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289683
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bo:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289684
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bo:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x75

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bo:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 289685
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bo:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method public final aV()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289686
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bp:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289687
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bp:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x76

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bp:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 289688
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bp:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method public final aW()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289689
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289690
    const/16 v0, 0xe

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289691
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bq:Z

    return v0
.end method

.method public final aX()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289692
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289693
    const/16 v0, 0xf

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289694
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bu:I

    return v0
.end method

.method public final aY()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289695
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bv:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289696
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bv:Ljava/lang/String;

    const/16 v1, 0x7c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bv:Ljava/lang/String;

    .line 289697
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bv:Ljava/lang/String;

    return-object v0
.end method

.method public final aZ()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289698
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289699
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x7d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289700
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bw:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aa()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289701
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289702
    const/4 v0, 0x6

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289703
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ae:Z

    return v0
.end method

.method public final ab()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x6

    .line 289704
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289705
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289706
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->af:Z

    return v0
.end method

.method public final ac()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289707
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289708
    const/4 v0, 0x6

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289709
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ag:Z

    return v0
.end method

.method public final ad()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289677
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289678
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289679
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ah:Z

    return v0
.end method

.method public final ae()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289584
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289585
    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289586
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ai:Z

    return v0
.end method

.method public final af()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289608
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289609
    const/4 v0, 0x7

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289610
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aj:Z

    return v0
.end method

.method public final ag()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289605
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289606
    const/4 v0, 0x7

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289607
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ak:Z

    return v0
.end method

.method public final ah()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289602
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->al:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289603
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->al:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->al:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289604
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->al:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289599
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->am:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289600
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->am:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289601
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->am:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289596
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->an:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289597
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->an:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289598
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->an:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ak()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x7

    .line 289590
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289591
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289592
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ao:I

    return v0
.end method

.method public final al()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289587
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289588
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289589
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ap:I

    return v0
.end method

.method public final am()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289581
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289582
    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289583
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aq:I

    return v0
.end method

.method public final an()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289611
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289612
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289613
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ar:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ao()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289614
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289615
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 289616
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final ap()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289617
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->au:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289618
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->au:Ljava/lang/String;

    const/16 v1, 0x45

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->au:Ljava/lang/String;

    .line 289619
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->au:Ljava/lang/String;

    return-object v0
.end method

.method public final aq()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289620
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->av:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289621
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->av:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->av:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289622
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->av:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final ar()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289623
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aw:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289624
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aw:Ljava/lang/String;

    const/16 v1, 0x47

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aw:Ljava/lang/String;

    .line 289625
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aw:Ljava/lang/String;

    return-object v0
.end method

.method public final as()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289626
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289627
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x49

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289628
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final at()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289629
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->az:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289630
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->az:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->az:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289631
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->az:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final au()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289632
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289633
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    .line 289634
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aA:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final av()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289635
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289636
    const/16 v0, 0x9

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289637
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aB:I

    return v0
.end method

.method public final aw()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289638
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aC:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289639
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aC:Ljava/lang/String;

    const/16 v1, 0x4d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aC:Ljava/lang/String;

    .line 289640
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aC:Ljava/lang/String;

    return-object v0
.end method

.method public final ax()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289641
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289642
    const/16 v0, 0x9

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289643
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aD:I

    return v0
.end method

.method public final ay()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289801
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289802
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289803
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aE:I

    return v0
.end method

.method public final az()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289771
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aF:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289772
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aF:Ljava/lang/String;

    const/16 v1, 0x51

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aF:Ljava/lang/String;

    .line 289773
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->aF:Ljava/lang/String;

    return-object v0
.end method

.method public final ba()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289774
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bx:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289775
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bx:Ljava/util/List;

    const/16 v1, 0x7e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bx:Ljava/util/List;

    .line 289776
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bx:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bb()Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bz:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bz:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    const/16 v1, 0x80

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bz:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 289779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bz:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    return-object v0
.end method

.method public final bc()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289780
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289781
    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289782
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bA:I

    return v0
.end method

.method public final bd()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289783
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289784
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x82

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289785
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bB:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final be()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289786
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bC:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289787
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bC:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    const/16 v1, 0x83

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bC:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 289788
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bC:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    return-object v0
.end method

.method public final bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289789
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bD:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289790
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bD:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const/16 v1, 0x84

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bD:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 289791
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bD:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    return-object v0
.end method

.method public final bg()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289792
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289793
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x88

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289794
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bH:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final bh()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289795
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289796
    const/16 v0, 0x11

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289797
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bI:I

    return v0
.end method

.method public final bi()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289798
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bK:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289799
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bK:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    const/16 v1, 0x8b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bK:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 289800
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bK:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    return-object v0
.end method

.method public final bj()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289768
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bO:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289769
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bO:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    const/16 v1, 0x91

    const-class v2, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bO:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 289770
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bO:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    return-object v0
.end method

.method public final bk()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289804
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289805
    const/16 v0, 0x12

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289806
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bP:Z

    return v0
.end method

.method public final bl()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289807
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289808
    const/16 v0, 0x12

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289809
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bQ:D

    return-wide v0
.end method

.method public final bm()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289765
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289766
    const/16 v0, 0x12

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289767
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bR:D

    return-wide v0
.end method

.method public final bn()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289810
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289811
    const/16 v0, 0x12

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289812
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bS:Z

    return v0
.end method

.method public final bo()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289813
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bW:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289814
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bW:Ljava/lang/String;

    const/16 v1, 0x99

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bW:Ljava/lang/String;

    .line 289815
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bW:Ljava/lang/String;

    return-object v0
.end method

.method public final bp()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289816
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bX:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289817
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bX:Ljava/lang/String;

    const/16 v1, 0x9a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bX:Ljava/lang/String;

    .line 289818
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bX:Ljava/lang/String;

    return-object v0
.end method

.method public final bq()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289819
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bY:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289820
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bY:Ljava/lang/String;

    const/16 v1, 0x9b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bY:Ljava/lang/String;

    .line 289821
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bY:Ljava/lang/String;

    return-object v0
.end method

.method public final br()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289822
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289823
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bZ:Ljava/lang/String;

    const/16 v1, 0x9c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bZ:Ljava/lang/String;

    .line 289824
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->bZ:Ljava/lang/String;

    return-object v0
.end method

.method public final bs()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289825
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289826
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289827
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->cd:I

    return v0
.end method

.method public final bt()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289828
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289829
    const/16 v0, 0x14

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289830
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->ce:Z

    return v0
.end method

.method public final e()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289736
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289737
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 289738
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->z:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 289713
    const v0, 0x4ed245b

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 289715
    return-object v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289716
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289717
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 289718
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 289719
    invoke-static {p0}, LX/2v3;->d(LX/16n;)I

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 289720
    invoke-static {p0}, LX/2v3;->e(LX/16n;)I

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289721
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289722
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289723
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->h:I

    return v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289724
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->i:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289725
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->i:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->i:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 289726
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->i:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289727
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289728
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->j:Ljava/lang/String;

    .line 289729
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289730
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289731
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289732
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->k:J

    return-wide v0
.end method

.method public final q()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289733
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289734
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289735
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->l:I

    return v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289710
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289711
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 289712
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->m:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    return-object v0
.end method

.method public final s()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289739
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289740
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289741
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->o:Z

    return v0
.end method

.method public final t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289742
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289743
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289744
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->p:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289745
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    move-object v0, v0

    .line 289746
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289747
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289748
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289749
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->q:Z

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289750
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289751
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->r:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->r:Ljava/lang/String;

    .line 289752
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 289753
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 289754
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 289755
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->u:J

    return-wide v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289756
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->v:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289757
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->v:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 289758
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->v:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289759
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->x:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289760
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->x:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->x:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 289761
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->x:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289762
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->A:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 289763
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->A:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->A:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 289764
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLVideo;->A:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    return-object v0
.end method
