.class public final Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:I

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 254136
    const-class v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 254137
    const-class v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 254138
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 254139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x4c06fe62

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 254140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r:LX/0x2;

    .line 254141
    return-void
.end method

.method public constructor <init>(LX/4Yb;)V
    .locals 2

    .prologue
    .line 254142
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 254143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x4c06fe62

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 254144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r:LX/0x2;

    .line 254145
    iget-object v0, p1, LX/4Yb;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 254146
    iget-object v0, p1, LX/4Yb;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g:Ljava/lang/String;

    .line 254147
    iget-object v0, p1, LX/4Yb;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->h:Ljava/lang/String;

    .line 254148
    iget-wide v0, p1, LX/4Yb;->e:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->i:J

    .line 254149
    iget v0, p1, LX/4Yb;->f:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->j:I

    .line 254150
    iget-object v0, p1, LX/4Yb;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k:Ljava/lang/String;

    .line 254151
    iget-object v0, p1, LX/4Yb;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->q:Ljava/util/List;

    .line 254152
    iget-object v0, p1, LX/4Yb;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->l:Ljava/lang/String;

    .line 254153
    iget-object v0, p1, LX/4Yb;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254154
    iget-object v0, p1, LX/4Yb;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254155
    iget-object v0, p1, LX/4Yb;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->o:Ljava/lang/String;

    .line 254156
    iget-object v0, p1, LX/4Yb;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->p:Ljava/lang/String;

    .line 254157
    iget-object v0, p1, LX/4Yb;->n:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r:LX/0x2;

    .line 254158
    return-void
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 254159
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254160
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254161
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->h:Ljava/lang/String;

    .line 254162
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 254163
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 254164
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 254165
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->i:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 254166
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 254167
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r:LX/0x2;

    if-nez v0, :cond_0

    .line 254168
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r:LX/0x2;

    .line 254169
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 254170
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 254171
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 254172
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254173
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 254174
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 254175
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->E_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 254176
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 254177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 254178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 254179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 254180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 254181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 254182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->w()LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v12

    .line 254183
    const/16 v3, 0xd

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 254184
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 254185
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 254186
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 254187
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 254188
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->o()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 254189
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 254190
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 254191
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 254192
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 254193
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 254194
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 254195
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 254196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254197
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 254198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 254201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 254202
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    .line 254203
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 254204
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 254205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 254207
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    .line 254208
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254209
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 254210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254211
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 254212
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    .line 254213
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254214
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254215
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 254217
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->i:J

    .line 254218
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 254131
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 254132
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->i:J

    .line 254133
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->j:I

    .line 254134
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254135
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254092
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254093
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->o:Ljava/lang/String;

    .line 254094
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 254097
    :goto_0
    return-object v0

    .line 254098
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 254099
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 254100
    const v0, -0x4c06fe62

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254101
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254102
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g:Ljava/lang/String;

    .line 254103
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1

    .prologue
    .line 254104
    invoke-static {p0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 254107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->f:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    return-object v0
.end method

.method public final o()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 254108
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 254109
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 254110
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->j:I

    return v0
.end method

.method public final synthetic p()Ljava/util/List;
    .locals 1

    .prologue
    .line 254111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 254112
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254113
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254114
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k:Ljava/lang/String;

    .line 254115
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254116
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254117
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->l:Ljava/lang/String;

    .line 254118
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254119
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254120
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254121
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254122
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254123
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254124
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254125
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254126
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->p:Ljava/lang/String;

    .line 254127
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final w()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254128
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254129
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->q:Ljava/util/List;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->q:Ljava/util/List;

    .line 254130
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
