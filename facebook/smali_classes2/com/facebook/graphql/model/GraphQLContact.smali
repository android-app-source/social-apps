.class public final Lcom/facebook/graphql/model/GraphQLContact;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLContact$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLContact$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public D:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306839
    const-class v0, Lcom/facebook/graphql/model/GraphQLContact$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306840
    const-class v0, Lcom/facebook/graphql/model/GraphQLContact$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 306841
    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306842
    return-void
.end method

.method private A()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 306843
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 306844
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 306845
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->v:Z

    return v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306846
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->w:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306847
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->w:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->w:Lcom/facebook/graphql/model/GraphQLActor;

    .line 306848
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->w:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306849
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->x:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306850
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->x:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306851
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->x:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306852
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->y:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306853
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->y:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->y:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 306854
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->y:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLName;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306855
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->z:Lcom/facebook/graphql/model/GraphQLName;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306856
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->z:Lcom/facebook/graphql/model/GraphQLName;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLName;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->z:Lcom/facebook/graphql/model/GraphQLName;

    .line 306857
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->z:Lcom/facebook/graphql/model/GraphQLName;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306858
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->A:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306859
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->A:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306860
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->A:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private G()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306888
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306889
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->B:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->B:Ljava/lang/String;

    .line 306890
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->B:Ljava/lang/String;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 306861
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->C:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306862
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->C:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->C:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 306863
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->C:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306864
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->D:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306865
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->D:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306866
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->D:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306867
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->E:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306868
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->E:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306869
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->E:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 306870
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLContact;->l:Z

    .line 306871
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 306872
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 306873
    if-eqz v0, :cond_0

    .line 306874
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 306875
    :cond_0
    return-void
.end method

.method private j()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 306876
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 306877
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 306878
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->e:J

    return-wide v0
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306879
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->f:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306880
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->f:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306881
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->f:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306882
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306883
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->g:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306884
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->g:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306885
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306886
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->h:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->h:Ljava/lang/String;

    .line 306887
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306833
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306834
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306835
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306836
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306837
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->j:Ljava/lang/String;

    .line 306838
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306630
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306631
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306632
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 306634
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 306635
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 306636
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->l:Z

    return v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306637
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306638
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->m:Ljava/lang/String;

    .line 306639
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLName;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306640
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->n:Lcom/facebook/graphql/model/GraphQLName;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306641
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->n:Lcom/facebook/graphql/model/GraphQLName;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLName;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->n:Lcom/facebook/graphql/model/GraphQLName;

    .line 306642
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->n:Lcom/facebook/graphql/model/GraphQLName;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306643
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->o:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306644
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->o:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306645
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->o:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306646
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306647
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306648
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306649
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->q:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306650
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->q:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306651
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->q:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306652
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306653
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306654
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306655
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->s:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306656
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->s:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306657
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->s:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306658
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306659
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 306660
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306661
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->u:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306662
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->u:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306663
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->u:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 31

    .prologue
    .line 306664
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306665
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 306666
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 306667
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->m()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 306668
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 306669
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 306670
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 306671
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->r()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 306672
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->s()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 306673
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 306674
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 306675
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 306676
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 306677
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 306678
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 306679
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 306680
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 306681
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 306682
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->D()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 306683
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->E()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 306684
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 306685
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->G()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 306686
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 306687
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 306688
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 306689
    const/4 v3, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 306690
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 306691
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 306692
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 306693
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 306694
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 306695
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 306696
    const/16 v2, 0x8

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->q()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 306697
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 306698
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 306699
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306700
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306701
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306702
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306703
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306704
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306705
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306706
    const/16 v2, 0x12

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->A()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 306707
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306708
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306709
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306710
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306711
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306712
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306713
    const/16 v3, 0x1b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->H()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 306714
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306715
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 306716
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306717
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 306718
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLContact;->H()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 306719
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306720
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306721
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306722
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 306723
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306724
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->f:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306725
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 306726
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306727
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 306728
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306729
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306730
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 306731
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306732
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 306733
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306734
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306735
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 306736
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306737
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 306738
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306739
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306740
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->s()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 306741
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->s()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    .line 306742
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->s()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 306743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306744
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->n:Lcom/facebook/graphql/model/GraphQLName;

    .line 306745
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 306746
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306747
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 306748
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306749
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->o:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306750
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 306751
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306752
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 306753
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306754
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306755
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 306756
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306757
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 306758
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306759
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306760
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 306761
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306762
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 306763
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306764
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->q:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306765
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 306766
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306767
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 306768
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306769
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306770
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 306771
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306772
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->x()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 306773
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306774
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->s:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306775
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 306776
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 306777
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->y()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 306778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306779
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->t:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 306780
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 306781
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306782
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 306783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306784
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306785
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 306786
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306787
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 306788
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306789
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->E:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306790
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 306791
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 306792
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 306793
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306794
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->w:Lcom/facebook/graphql/model/GraphQLActor;

    .line 306795
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 306796
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306797
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 306798
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306799
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306800
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->D()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 306801
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->D()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 306802
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->D()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 306803
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306804
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->y:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 306805
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->E()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 306806
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->E()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLName;

    .line 306807
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->E()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 306808
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306809
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->z:Lcom/facebook/graphql/model/GraphQLName;

    .line 306810
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 306811
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 306812
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 306813
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLContact;

    .line 306814
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLContact;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 306815
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306816
    if-nez v1, :cond_13

    :goto_0
    return-object p0

    :cond_13
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 306817
    new-instance v0, LX/4Vz;

    invoke-direct {v0, p1}, LX/4Vz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306818
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 306819
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 306820
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->e:J

    .line 306821
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->l:Z

    .line 306822
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLContact;->v:Z

    .line 306823
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 306824
    const-string v0, "is_on_viewer_contact_list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306825
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLContact;->q()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 306826
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 306827
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 306828
    :goto_0
    return-void

    .line 306829
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 306830
    const-string v0, "is_on_viewer_contact_list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306831
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLContact;->a(Z)V

    .line 306832
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 306633
    const v0, -0x64104400

    return v0
.end method
