.class public final Lcom/facebook/graphql/model/GraphQLMediaSet;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLMediaSet$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLMediaSet$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306345
    const-class v0, Lcom/facebook/graphql/model/GraphQLMediaSet$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 306344
    const-class v0, Lcom/facebook/graphql/model/GraphQLMediaSet$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 306342
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 306343
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306336
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 306337
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 306338
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 306339
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 306340
    const/4 v0, 0x0

    .line 306341
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306333
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306334
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->f:Ljava/lang/String;

    .line 306335
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->f:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306330
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->h:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306331
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->h:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->h:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306332
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->h:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306327
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306328
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306329
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 306289
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306290
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/String;)I

    move-result v0

    .line 306291
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 306292
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 306293
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->m()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 306294
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 306295
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 306296
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 306297
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 306298
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 306299
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 306300
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 306301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306302
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 306303
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 306309
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 306310
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306311
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306312
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 306313
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 306314
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMediaSet;->g:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306315
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->m()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 306316
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->m()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306317
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->m()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 306318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 306319
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMediaSet;->h:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306320
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 306321
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306322
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 306323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 306324
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLMediaSet;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 306325
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 306326
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306308
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 306307
    const v0, -0x31d68202

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 306304
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->g:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 306305
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->g:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->g:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 306306
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLMediaSet;->g:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    return-object v0
.end method
