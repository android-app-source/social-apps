.class public final Lcom/facebook/graphql/model/GraphQLEntity;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/171;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEntity$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEntity$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:I

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLRedirectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:D

.field public P:D

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:I

.field public U:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:I

.field public Z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoEncoding;",
            ">;"
        }
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:J

.field public ac:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public p:I

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:I

.field public u:I

.field public v:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 188678
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntity$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 188578
    const-class v0, Lcom/facebook/graphql/model/GraphQLEntity$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 188579
    const/16 v0, 0x36

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 188580
    return-void
.end method

.method public constructor <init>(LX/170;)V
    .locals 2

    .prologue
    .line 188581
    const/16 v0, 0x36

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 188582
    iget-object v0, p1, LX/170;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->f:Ljava/util/List;

    .line 188583
    iget-object v0, p1, LX/170;->c:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 188584
    iget-object v0, p1, LX/170;->d:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 188585
    iget-object v0, p1, LX/170;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->i:Ljava/lang/String;

    .line 188586
    iget-object v0, p1, LX/170;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->aa:Ljava/lang/String;

    .line 188587
    iget-boolean v0, p1, LX/170;->g:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->j:Z

    .line 188588
    iget-boolean v0, p1, LX/170;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->k:Z

    .line 188589
    iget-object v0, p1, LX/170;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->l:Ljava/lang/String;

    .line 188590
    iget-object v0, p1, LX/170;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->m:Ljava/lang/String;

    .line 188591
    iget-object v0, p1, LX/170;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->n:Ljava/lang/String;

    .line 188592
    iget-object v0, p1, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ae:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 188593
    iget-object v0, p1, LX/170;->m:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->o:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 188594
    iget v0, p1, LX/170;->n:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->p:I

    .line 188595
    iget-object v0, p1, LX/170;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->q:Ljava/lang/String;

    .line 188596
    iget-object v0, p1, LX/170;->p:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188597
    iget v0, p1, LX/170;->q:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->s:I

    .line 188598
    iget v0, p1, LX/170;->r:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->t:I

    .line 188599
    iget v0, p1, LX/170;->s:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->u:I

    .line 188600
    iget-object v0, p1, LX/170;->t:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188601
    iget-boolean v0, p1, LX/170;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->w:Z

    .line 188602
    iget-boolean v0, p1, LX/170;->v:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->x:Z

    .line 188603
    iget-boolean v0, p1, LX/170;->w:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->y:Z

    .line 188604
    iget-boolean v0, p1, LX/170;->x:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->z:Z

    .line 188605
    iget-object v0, p1, LX/170;->y:Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ac:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 188606
    iget-object v0, p1, LX/170;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 188607
    iget-object v0, p1, LX/170;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->B:Ljava/lang/String;

    .line 188608
    iget-object v0, p1, LX/170;->B:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ad:Lcom/facebook/graphql/model/GraphQLActor;

    .line 188609
    iget-object v0, p1, LX/170;->C:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 188610
    iget-object v0, p1, LX/170;->D:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Z:Ljava/util/List;

    .line 188611
    iget v0, p1, LX/170;->E:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->D:I

    .line 188612
    iget-object v0, p1, LX/170;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->E:Ljava/lang/String;

    .line 188613
    iget-object v0, p1, LX/170;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->F:Ljava/lang/String;

    .line 188614
    iget-object v0, p1, LX/170;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->G:Ljava/lang/String;

    .line 188615
    iget-object v0, p1, LX/170;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->H:Ljava/lang/String;

    .line 188616
    iget-object v0, p1, LX/170;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->I:Ljava/lang/String;

    .line 188617
    iget-object v0, p1, LX/170;->K:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188618
    iget-object v0, p1, LX/170;->L:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188619
    iget-object v0, p1, LX/170;->M:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->L:Ljava/lang/String;

    .line 188620
    iget-object v0, p1, LX/170;->N:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->M:Ljava/util/List;

    .line 188621
    iget-object v0, p1, LX/170;->O:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188622
    iget-wide v0, p1, LX/170;->P:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->O:D

    .line 188623
    iget-wide v0, p1, LX/170;->Q:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->P:D

    .line 188624
    iget-object v0, p1, LX/170;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Q:Ljava/lang/String;

    .line 188625
    iget-object v0, p1, LX/170;->S:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->R:Ljava/lang/String;

    .line 188626
    iget-object v0, p1, LX/170;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->S:Ljava/lang/String;

    .line 188627
    iget v0, p1, LX/170;->U:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->T:I

    .line 188628
    iget-wide v0, p1, LX/170;->V:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ab:J

    .line 188629
    iget-object v0, p1, LX/170;->W:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->U:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 188630
    iget-object v0, p1, LX/170;->X:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->V:Ljava/lang/String;

    .line 188631
    iget-object v0, p1, LX/170;->Y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->W:Ljava/lang/String;

    .line 188632
    iget-object v0, p1, LX/170;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188633
    iget v0, p1, LX/170;->aa:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Y:I

    .line 188634
    iget-object v0, p1, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 188635
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188636
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188637
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->m:Ljava/lang/String;

    .line 188638
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->m:Ljava/lang/String;

    return-object v0
.end method

.method private B()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188639
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188640
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188641
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->p:I

    return v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188642
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188643
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188644
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private D()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188645
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188646
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188647
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->s:I

    return v0
.end method

.method private E()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188648
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188649
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188650
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->t:I

    return v0
.end method

.method private F()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188651
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188652
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188653
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->u:I

    return v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188654
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->v:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188655
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->v:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188656
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->v:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private H()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188657
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188658
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188659
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->x:Z

    return v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188660
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188661
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 188662
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private J()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188663
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188664
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188665
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->D:I

    return v0
.end method

.method private K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188666
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188667
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->E:Ljava/lang/String;

    .line 188668
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->E:Ljava/lang/String;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188709
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188710
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->F:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->F:Ljava/lang/String;

    .line 188711
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->F:Ljava/lang/String;

    return-object v0
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188669
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188670
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->G:Ljava/lang/String;

    .line 188671
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->G:Ljava/lang/String;

    return-object v0
.end method

.method private N()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188706
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188707
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->H:Ljava/lang/String;

    .line 188708
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->H:Ljava/lang/String;

    return-object v0
.end method

.method private O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188703
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188704
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->I:Ljava/lang/String;

    .line 188705
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->I:Ljava/lang/String;

    return-object v0
.end method

.method private P()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188700
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188701
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188702
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private Q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188697
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->L:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188698
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->L:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->L:Ljava/lang/String;

    .line 188699
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->L:Ljava/lang/String;

    return-object v0
.end method

.method private R()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188694
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188695
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188696
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method private S()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x4

    .line 188691
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188692
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188693
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->O:D

    return-wide v0
.end method

.method private T()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188688
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188689
    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188690
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->P:D

    return-wide v0
.end method

.method private U()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188685
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188686
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Q:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Q:Ljava/lang/String;

    .line 188687
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Q:Ljava/lang/String;

    return-object v0
.end method

.method private V()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188682
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->R:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188683
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->R:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->R:Ljava/lang/String;

    .line 188684
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->R:Ljava/lang/String;

    return-object v0
.end method

.method private W()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188679
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->S:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188680
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->S:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->S:Ljava/lang/String;

    .line 188681
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->S:Ljava/lang/String;

    return-object v0
.end method

.method private X()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188675
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188676
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188677
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->T:I

    return v0
.end method

.method private Y()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188672
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->U:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188673
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->U:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->U:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 188674
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->U:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method private Z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188572
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->X:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188573
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->X:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188574
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->X:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aa()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188575
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188576
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188577
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Y:I

    return v0
.end method

.method private ab()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoEncoding;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188524
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Z:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188525
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Z:Ljava/util/List;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Z:Ljava/util/List;

    .line 188526
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ac()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188521
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188522
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188523
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ab:J

    return-wide v0
.end method

.method private ad()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188518
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ac:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188519
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ac:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ac:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 188520
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ac:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188515
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188516
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->i:Ljava/lang/String;

    .line 188517
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->i:Ljava/lang/String;

    return-object v0
.end method

.method private x()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188569
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188570
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188571
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->j:Z

    return v0
.end method

.method private y()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188512
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188513
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188514
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->k:Z

    return v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188509
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188510
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->l:Ljava/lang/String;

    .line 188511
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 37

    .prologue
    .line 188303
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 188304
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 188305
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 188306
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 188307
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 188308
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->w()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 188309
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->z()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 188310
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->A()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 188311
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->m()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 188312
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 188313
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 188314
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 188315
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 188316
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 188317
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 188318
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->K()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 188319
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->L()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 188320
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->M()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 188321
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->N()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 188322
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->O()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 188323
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 188324
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 188325
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->Q()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 188326
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v24

    .line 188327
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->R()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 188328
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->U()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 188329
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->V()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 188330
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->W()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 188331
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 188332
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 188333
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 188334
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ab()LX/0Px;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v32

    .line 188335
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->u()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 188336
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ad()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 188337
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->v()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 188338
    const/16 v36, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 188339
    const/16 v36, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 188340
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 188341
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 188342
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 188343
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 188344
    const/4 v2, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->x()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 188345
    const/4 v2, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->y()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 188346
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 188347
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 188348
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 188349
    const/16 v3, 0xa

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 188350
    const/16 v2, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->B()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 188351
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 188352
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 188353
    const/16 v2, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->D()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 188354
    const/16 v2, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->E()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 188355
    const/16 v2, 0x10

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->F()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 188356
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 188357
    const/16 v2, 0x12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->o()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 188358
    const/16 v2, 0x13

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->H()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 188359
    const/16 v2, 0x14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->p()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 188360
    const/16 v2, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->q()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 188361
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 188362
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 188363
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 188364
    const/16 v2, 0x19

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->J()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 188365
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188366
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188367
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188368
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188369
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188370
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188371
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188372
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188373
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188374
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188375
    const/16 v3, 0x24

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->S()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 188376
    const/16 v3, 0x25

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->T()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 188377
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188378
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188379
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188380
    const/16 v2, 0x29

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->X()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 188381
    const/16 v3, 0x2a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->Y()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 188382
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188383
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188384
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188385
    const/16 v2, 0x2e

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->aa()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 188386
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188387
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188388
    const/16 v3, 0x31

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ac()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 188389
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188390
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 188391
    const/16 v3, 0x34

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 188392
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 188393
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 188394
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 188395
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    goto/16 :goto_1

    .line 188396
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->Y()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    goto/16 :goto_2

    .line 188397
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v2

    goto :goto_3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 188433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 188434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 188436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 188437
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188438
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 188439
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 188440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 188441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 188442
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188443
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 188444
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 188445
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 188446
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 188447
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188448
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188449
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 188450
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 188451
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 188452
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188453
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188454
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ad()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 188455
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ad()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 188456
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ad()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 188457
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188458
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->ac:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 188459
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 188460
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 188461
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 188462
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188463
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->A:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 188464
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->v()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 188465
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->v()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 188466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->v()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 188467
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188468
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->ad:Lcom/facebook/graphql/model/GraphQLActor;

    .line 188469
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 188470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 188471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 188472
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188473
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 188474
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ab()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 188475
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->ab()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 188476
    if-eqz v2, :cond_8

    .line 188477
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188478
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEntity;->Z:Ljava/util/List;

    move-object v1, v0

    .line 188479
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 188480
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 188481
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 188482
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188483
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188484
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 188485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 188486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 188487
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188488
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188489
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 188490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 188491
    if-eqz v2, :cond_b

    .line 188492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188493
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEntity;->M:Ljava/util/List;

    move-object v1, v0

    .line 188494
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->R()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 188495
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->R()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188496
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->R()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 188497
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188498
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->N:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188499
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 188500
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 188501
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 188502
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 188503
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEntity;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188504
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 188505
    if-nez v1, :cond_e

    :goto_0
    return-object p0

    :cond_e
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 188414
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 188415
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->j:Z

    .line 188416
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->k:Z

    .line 188417
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->p:I

    .line 188418
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->s:I

    .line 188419
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->t:I

    .line 188420
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->u:I

    .line 188421
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->w:Z

    .line 188422
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->x:Z

    .line 188423
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->y:Z

    .line 188424
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->z:Z

    .line 188425
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->D:I

    .line 188426
    const/16 v0, 0x24

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->O:D

    .line 188427
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->P:D

    .line 188428
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->T:I

    .line 188429
    const/16 v0, 0x2e

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->Y:I

    .line 188430
    const/16 v0, 0x31

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ab:J

    .line 188431
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188408
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 188409
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 188410
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLEntity;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 188411
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 188412
    const/4 v0, 0x0

    .line 188413
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188405
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188406
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->f:Ljava/util/List;

    .line 188407
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188402
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ae:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188403
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ae:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ae:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 188404
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ae:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188399
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188400
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->q:Ljava/lang/String;

    .line 188401
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 188398
    const v0, 0x7c02d003

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188527
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188528
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->W:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->W:Ljava/lang/String;

    .line 188529
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188506
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188507
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 188508
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188530
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188531
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 188532
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->h:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188533
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188534
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->n:Ljava/lang/String;

    .line 188535
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188536
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->o:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188537
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->o:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->o:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 188538
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->o:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 188539
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188540
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188541
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->w:Z

    return v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188542
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188543
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188544
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->y:Z

    return v0
.end method

.method public final q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 188545
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 188546
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 188547
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->z:Z

    return v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188548
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->C:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188549
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->C:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 188550
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->C:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188551
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->K:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188552
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->K:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 188553
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->K:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final t()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLRedirectionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188554
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->M:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188555
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->M:Ljava/util/List;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->M:Ljava/util/List;

    .line 188556
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->M:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188557
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->aa:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188558
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->aa:Ljava/lang/String;

    const/16 v1, 0x30

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->aa:Ljava/lang/String;

    .line 188559
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188560
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ad:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188561
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ad:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ad:Lcom/facebook/graphql/model/GraphQLActor;

    .line 188562
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->ad:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188563
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188564
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->B:Ljava/lang/String;

    .line 188565
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final w_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188566
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->V:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 188567
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->V:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->V:Ljava/lang/String;

    .line 188568
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEntity;->V:Ljava/lang/String;

    return-object v0
.end method
