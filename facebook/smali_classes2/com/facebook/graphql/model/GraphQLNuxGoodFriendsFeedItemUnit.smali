.class public final Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255358
    const-class v0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 255357
    const-class v0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 255318
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 255319
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x38669a7c

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 255320
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->i:LX/0x2;

    .line 255321
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255354
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255355
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->h:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->h:Ljava/lang/String;

    .line 255356
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 255353
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255350
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 255351
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->f:Ljava/lang/String;

    .line 255352
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 255347
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 255348
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 255349
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->g:J

    return-wide v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 255344
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->i:LX/0x2;

    if-nez v0, :cond_0

    .line 255345
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->i:LX/0x2;

    .line 255346
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->i:LX/0x2;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 255335
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255336
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->E_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 255337
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 255338
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 255339
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 255340
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 255341
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 255342
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255343
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 255332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 255333
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 255334
    return-object p0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 255330
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->g:J

    .line 255331
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 255327
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 255328
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->g:J

    .line 255329
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255322
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 255323
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 255324
    :goto_0
    return-object v0

    .line 255325
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 255326
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 255317
    const v0, 0x38669a7c

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255315
    const/4 v0, 0x0

    move-object v0, v0

    .line 255316
    return-object v0
.end method
