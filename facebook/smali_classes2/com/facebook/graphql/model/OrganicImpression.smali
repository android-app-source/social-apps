.class public Lcom/facebook/graphql/model/OrganicImpression;
.super Lcom/facebook/graphql/model/BaseImpression;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/graphql/model/OrganicImpression;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Lcom/facebook/graphql/model/OrganicImpression;


# instance fields
.field private o:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field private p:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203787
    new-instance v0, Lcom/facebook/graphql/model/OrganicImpression;

    invoke-direct {v0, v1, v1}, Lcom/facebook/graphql/model/OrganicImpression;-><init>(ZZ)V

    sput-object v0, Lcom/facebook/graphql/model/OrganicImpression;->n:Lcom/facebook/graphql/model/OrganicImpression;

    .line 203788
    new-instance v0, LX/184;

    invoke-direct {v0}, LX/184;-><init>()V

    sput-object v0, Lcom/facebook/graphql/model/OrganicImpression;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 203783
    invoke-direct {p0, p1}, Lcom/facebook/graphql/model/BaseImpression;-><init>(Landroid/os/Parcel;)V

    .line 203784
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->o:Z

    .line 203785
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->p:Z

    .line 203786
    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 0

    .prologue
    .line 203751
    invoke-direct {p0}, Lcom/facebook/graphql/model/BaseImpression;-><init>()V

    .line 203752
    iput-boolean p1, p0, Lcom/facebook/graphql/model/OrganicImpression;->o:Z

    .line 203753
    iput-boolean p2, p0, Lcom/facebook/graphql/model/OrganicImpression;->p:Z

    .line 203754
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/OrganicImpression;
    .locals 6

    .prologue
    .line 203766
    invoke-static {p0}, LX/0x1;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/OrganicImpression;

    move-result-object v0

    .line 203767
    if-nez v0, :cond_3

    .line 203768
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 203769
    if-eqz p0, :cond_1

    .line 203770
    invoke-static {p0}, LX/185;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 203771
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 203772
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2}, LX/185;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 203773
    :cond_0
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 203774
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    move v1, v0

    .line 203775
    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203776
    invoke-static {v0}, LX/185;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 203777
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 203778
    :cond_2
    if-eqz v0, :cond_4

    new-instance v0, Lcom/facebook/graphql/model/OrganicImpression;

    invoke-direct {v0, v5, v5}, Lcom/facebook/graphql/model/OrganicImpression;-><init>(ZZ)V

    :goto_1
    move-object v0, v0

    .line 203779
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v1

    .line 203780
    iput-object v0, v1, LX/0x2;->s:Lcom/facebook/graphql/model/OrganicImpression;

    .line 203781
    :cond_3
    return-object v0

    :cond_4
    sget-object v0, Lcom/facebook/graphql/model/OrganicImpression;->n:Lcom/facebook/graphql/model/OrganicImpression;

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 203765
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 203782
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 203764
    const/4 v0, 0x1

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 203763
    iget-boolean v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 203759
    const/16 v0, 0x32

    iput v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->j:I

    .line 203760
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->k:I

    .line 203761
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->o:Z

    .line 203762
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 203755
    invoke-super {p0, p1, p2}, Lcom/facebook/graphql/model/BaseImpression;->writeToParcel(Landroid/os/Parcel;I)V

    .line 203756
    iget-boolean v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->o:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 203757
    iget-boolean v0, p0, Lcom/facebook/graphql/model/OrganicImpression;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 203758
    return-void
.end method
