.class public final Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLInstantArticleVersion$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLInstantArticleVersion$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLDocumentElement;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public n:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:J

.field public q:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

.field public r:J

.field public s:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324384
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 324385
    const-class v0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 324386
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 324387
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324388
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->v:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324389
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->v:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->v:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324390
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->v:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324391
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->w:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324392
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->w:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->w:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324393
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->w:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324394
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->x:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324395
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->x:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->x:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324396
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->x:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324397
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->y:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324398
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->y:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->y:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324399
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->y:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324400
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324401
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324402
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private l()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324403
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 324404
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 324405
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->f:I

    return v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324406
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324407
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324408
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLDocumentElement;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324409
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->h:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324410
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->h:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->h:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 324411
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->h:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    return-object v0
.end method

.method private o()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324412
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 324413
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 324414
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->i:J

    return-wide v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324378
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324379
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 324380
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324415
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324416
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 324417
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324381
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324382
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 324383
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324418
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324419
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    .line 324420
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324241
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324242
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->o:Ljava/lang/String;

    .line 324243
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324244
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 324245
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 324246
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->p:J

    return-wide v0
.end method

.method private v()Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324247
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324248
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    .line 324249
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    return-object v0
.end method

.method private w()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324250
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 324251
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 324252
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->r:J

    return-wide v0
.end method

.method private x()Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 324253
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->s:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324254
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->s:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->s:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    .line 324255
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->s:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324256
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324257
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->t:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->t:Ljava/lang/String;

    .line 324258
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->t:Ljava/lang/String;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324259
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->u:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324260
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->u:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->u:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324261
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->u:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 324262
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324263
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 324264
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 324265
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 324266
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 324267
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->p()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 324268
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 324269
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 324270
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->y()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 324271
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 324272
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->A()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 324273
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->B()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 324274
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->C()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 324275
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->D()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 324276
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->E()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 324277
    const/16 v5, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 324278
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 324279
    const/4 v2, 0x2

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->l()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, LX/186;->a(III)V

    .line 324280
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 324281
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 324282
    const/4 v3, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->o()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 324283
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 324284
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 324285
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 324286
    const/16 v3, 0x9

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->r()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 324287
    const/16 v3, 0xa

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->s()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 324288
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 324289
    const/16 v3, 0xc

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->u()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 324290
    const/16 v3, 0xd

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->v()Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 324291
    const/16 v3, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->w()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 324292
    const/16 v3, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->x()Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 324293
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 324294
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 324295
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 324296
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 324297
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 324298
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 324299
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 324300
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324301
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 324302
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->r()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    goto/16 :goto_0

    .line 324303
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->s()Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v2

    goto/16 :goto_1

    .line 324304
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->v()Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    move-result-object v2

    goto :goto_2

    .line 324305
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->x()Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    move-result-object v2

    goto :goto_3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 324306
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 324307
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324308
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324309
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 324310
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324311
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 324312
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 324313
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324314
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 324315
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324316
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->u:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324317
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 324318
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 324319
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->n()Lcom/facebook/graphql/model/GraphQLDocumentElement;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 324320
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324321
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->h:Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 324322
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->A()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 324323
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->A()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324324
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->A()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 324325
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324326
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->v:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324327
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->B()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 324328
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->B()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324329
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->B()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 324330
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324331
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->w:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324332
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->C()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 324333
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->C()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324334
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->C()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 324335
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324336
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->x:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324337
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 324338
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 324339
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 324340
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324341
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 324342
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->D()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 324343
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->D()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324344
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->D()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 324345
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324346
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->y:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324347
    :cond_7
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->E()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 324348
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->E()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324349
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->E()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 324350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324351
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->z:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 324352
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->p()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 324353
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->p()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 324354
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->p()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 324355
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324356
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 324357
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 324358
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 324359
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->q()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 324360
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 324361
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 324362
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 324363
    if-nez v1, :cond_b

    :goto_0
    return-object p0

    :cond_b
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324364
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 324365
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 324366
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->f:I

    .line 324367
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->i:J

    .line 324368
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->p:J

    .line 324369
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->r:J

    .line 324370
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 324371
    const v0, 0x60826b23

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324372
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324373
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->e:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->e:Ljava/lang/String;

    .line 324374
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 324375
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 324376
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j:Lcom/facebook/graphql/model/GraphQLProfile;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 324377
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method
