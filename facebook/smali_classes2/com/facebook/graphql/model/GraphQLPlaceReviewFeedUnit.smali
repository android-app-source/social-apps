.class public final Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 253971
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 253975
    const-class v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 253976
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 253977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x383fb37f

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 253978
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n:LX/0x2;

    .line 253979
    return-void
.end method

.method public constructor <init>(LX/4YD;)V
    .locals 2

    .prologue
    .line 253980
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 253981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x383fb37f

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 253982
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n:LX/0x2;

    .line 253983
    iget-object v0, p1, LX/4YD;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->f:Ljava/lang/String;

    .line 253984
    iget-object v0, p1, LX/4YD;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g:Ljava/lang/String;

    .line 253985
    iget-wide v0, p1, LX/4YD;->d:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->h:J

    .line 253986
    iget-object v0, p1, LX/4YD;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->i:Ljava/util/List;

    .line 253987
    iget-object v0, p1, LX/4YD;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->m:Ljava/util/List;

    .line 253988
    iget-object v0, p1, LX/4YD;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->j:Ljava/lang/String;

    .line 253989
    iget-object v0, p1, LX/4YD;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253990
    iget-object v0, p1, LX/4YD;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->l:Ljava/lang/String;

    .line 253991
    iget-object v0, p1, LX/4YD;->j:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n:LX/0x2;

    .line 253992
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253993
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->m:Ljava/util/List;

    .line 253994
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 253995
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 253996
    if-eqz v0, :cond_0

    .line 253997
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    .line 253998
    if-eqz p1, :cond_1

    .line 253999
    new-instance v3, LX/186;

    const/16 p0, 0x80

    invoke-direct {v3, p0}, LX/186;-><init>(I)V

    .line 254000
    const/4 p0, 0x0

    invoke-virtual {v3, p1, p0}, LX/186;->b(Ljava/util/List;Z)I

    move-result p0

    .line 254001
    invoke-virtual {v3, p0}, LX/186;->d(I)V

    .line 254002
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    move-object v3, v3

    .line 254003
    :goto_0
    invoke-virtual {v0, v1, v2, v3}, LX/15i;->a(II[B)V

    .line 254004
    :cond_0
    return-void

    .line 254005
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 254006
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 254007
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 254008
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g:Ljava/lang/String;

    .line 254009
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 254010
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 254011
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 254012
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->h:J

    return-wide v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 254013
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 254014
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n:LX/0x2;

    if-nez v0, :cond_0

    .line 254015
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n:LX/0x2;

    .line 254016
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n:LX/0x2;

    return-object v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 254017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 254018
    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 254019
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 254021
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->E_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 254022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 254023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 254024
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 254025
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 254026
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->r()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 254027
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 254028
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 254029
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 254030
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 254031
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 254032
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 254033
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 254034
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 254035
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 254036
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254037
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 254038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 254039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 254040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 254041
    if-eqz v1, :cond_2

    .line 254042
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 254043
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->i:Ljava/util/List;

    move-object v1, v0

    .line 254044
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 254047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 254048
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 254049
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 254050
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 254051
    new-instance v0, LX/4YE;

    invoke-direct {v0, p1}, LX/4YE;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253972
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 253973
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->h:J

    .line 253974
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 253935
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 253936
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->h:J

    .line 253937
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 253938
    invoke-virtual {p2}, LX/18L;->a()V

    .line 253939
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 253940
    const-string v0, "local_valid_items"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253941
    check-cast p2, LX/0Px;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->a(LX/0Px;)V

    .line 253942
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 253943
    return-void
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253944
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253945
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253946
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->l:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->l:Ljava/lang/String;

    .line 253947
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253948
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 253949
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 253950
    :goto_0
    return-object v0

    .line 253951
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 253952
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 253953
    const v0, -0x383fb37f

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253954
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253955
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->f:Ljava/lang/String;

    .line 253956
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253957
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253958
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->i:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->i:Ljava/util/List;

    .line 253959
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253960
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253961
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->j:Ljava/lang/String;

    .line 253962
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253963
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253964
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 253965
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 253966
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 253967
    invoke-static {p0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 253969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->m:Ljava/util/List;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->m:Ljava/util/List;

    .line 253970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
