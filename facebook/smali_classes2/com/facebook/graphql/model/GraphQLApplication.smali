.class public final Lcom/facebook/graphql/model/GraphQLApplication;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLApplication$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLApplication$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Z

.field public F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

.field public e:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:D
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 260751
    const-class v0, Lcom/facebook/graphql/model/GraphQLApplication$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 260677
    const-class v0, Lcom/facebook/graphql/model/GraphQLApplication$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 260678
    const/16 v0, 0x2b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 260679
    return-void
.end method

.method public constructor <init>(LX/4Vr;)V
    .locals 2

    .prologue
    .line 260680
    const/16 v0, 0x2b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 260681
    iget-object v0, p1, LX/4Vr;->b:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->e:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    .line 260682
    iget-object v0, p1, LX/4Vr;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->f:Ljava/lang/String;

    .line 260683
    iget-object v0, p1, LX/4Vr;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->g:Ljava/util/List;

    .line 260684
    iget-object v0, p1, LX/4Vr;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->h:Ljava/util/List;

    .line 260685
    iget-object v0, p1, LX/4Vr;->f:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260686
    iget-object v0, p1, LX/4Vr;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260687
    iget-wide v0, p1, LX/4Vr;->h:D

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->k:D

    .line 260688
    iget-object v0, p1, LX/4Vr;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->l:Ljava/lang/String;

    .line 260689
    iget-object v0, p1, LX/4Vr;->j:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260690
    iget-object v0, p1, LX/4Vr;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260691
    iget-object v0, p1, LX/4Vr;->l:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->Q:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    .line 260692
    iget-object v0, p1, LX/4Vr;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->o:Ljava/lang/String;

    .line 260693
    iget-object v0, p1, LX/4Vr;->n:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260694
    iget-object v0, p1, LX/4Vr;->o:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->N:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    .line 260695
    iget-object v0, p1, LX/4Vr;->p:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->q:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    .line 260696
    iget-boolean v0, p1, LX/4Vr;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->r:Z

    .line 260697
    iget-object v0, p1, LX/4Vr;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->s:Ljava/lang/String;

    .line 260698
    iget-object v0, p1, LX/4Vr;->s:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->t:Ljava/util/List;

    .line 260699
    iget-object v0, p1, LX/4Vr;->t:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->u:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    .line 260700
    iget-object v0, p1, LX/4Vr;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->M:Ljava/lang/String;

    .line 260701
    iget-object v0, p1, LX/4Vr;->v:Lcom/facebook/graphql/model/GraphQLRating;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->v:Lcom/facebook/graphql/model/GraphQLRating;

    .line 260702
    iget-object v0, p1, LX/4Vr;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->w:Ljava/lang/String;

    .line 260703
    iget-object v0, p1, LX/4Vr;->x:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260704
    iget-object v0, p1, LX/4Vr;->y:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260705
    iget-object v0, p1, LX/4Vr;->z:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260706
    iget-object v0, p1, LX/4Vr;->A:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260707
    iget-object v0, p1, LX/4Vr;->B:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260708
    iget-object v0, p1, LX/4Vr;->C:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260709
    iget-object v0, p1, LX/4Vr;->D:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->C:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 260710
    iget-object v0, p1, LX/4Vr;->E:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260711
    iget-boolean v0, p1, LX/4Vr;->F:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->E:Z

    .line 260712
    iget-object v0, p1, LX/4Vr;->G:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260713
    iget-object v0, p1, LX/4Vr;->H:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260714
    iget-object v0, p1, LX/4Vr;->I:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260715
    iget-object v0, p1, LX/4Vr;->J:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->H:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 260716
    iget-object v0, p1, LX/4Vr;->K:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260717
    iget-object v0, p1, LX/4Vr;->L:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->J:Ljava/lang/String;

    .line 260718
    iget-object v0, p1, LX/4Vr;->M:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->K:Ljava/lang/String;

    .line 260719
    iget-object v0, p1, LX/4Vr;->N:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->L:Ljava/lang/String;

    .line 260720
    return-void
.end method

.method private A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260721
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260722
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260723
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260724
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260725
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260726
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260727
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->q:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260728
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->q:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->q:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    .line 260729
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->q:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    return-object v0
.end method

.method private D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260730
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260731
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260732
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->r:Z

    return v0
.end method

.method private E()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260733
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260734
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->t:Ljava/util/List;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->t:Ljava/util/List;

    .line 260735
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->t:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private F()Lcom/facebook/graphql/model/GraphQLRating;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260736
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->v:Lcom/facebook/graphql/model/GraphQLRating;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260737
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->v:Lcom/facebook/graphql/model/GraphQLRating;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLRating;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRating;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->v:Lcom/facebook/graphql/model/GraphQLRating;

    .line 260738
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->v:Lcom/facebook/graphql/model/GraphQLRating;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260739
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->x:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260740
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->x:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260741
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->x:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260742
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->y:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260743
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->y:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260744
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->y:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260745
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260746
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260747
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260748
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->A:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260749
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->A:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260750
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->A:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private K()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260752
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->B:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260753
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->B:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260754
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->B:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private L()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260755
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->C:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260756
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->C:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->C:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 260757
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->C:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private M()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260758
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->D:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260759
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->D:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260760
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->D:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private N()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 260761
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260762
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260763
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->E:Z

    return v0
.end method

.method private O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260764
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260765
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260766
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private P()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260767
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->H:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260768
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->H:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->H:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 260769
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->H:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private Q()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260770
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->I:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260771
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->I:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260772
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->I:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private R()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260773
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260774
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->K:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->K:Ljava/lang/String;

    .line 260775
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->K:Ljava/lang/String;

    return-object v0
.end method

.method private S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260671
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->L:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260672
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->L:Ljava/lang/String;

    const/16 v1, 0x23

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->L:Ljava/lang/String;

    .line 260673
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->L:Ljava/lang/String;

    return-object v0
.end method

.method private T()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260674
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->O:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260675
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->O:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260676
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->O:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private U()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260411
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->P:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260412
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->P:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260413
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->P:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private V()Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 260556
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->Q:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260557
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->Q:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->Q:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    .line 260558
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->Q:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260553
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260554
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->f:Ljava/lang/String;

    .line 260555
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->f:Ljava/lang/String;

    return-object v0
.end method

.method private t()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260550
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260551
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->g:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->g:Ljava/util/List;

    .line 260552
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private u()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260547
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260548
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->h:Ljava/util/List;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->h:Ljava/util/List;

    .line 260549
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260544
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->i:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260545
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->i:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260546
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->i:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260541
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260542
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260543
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private x()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 260538
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 260539
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 260540
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->k:D

    return-wide v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260535
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260536
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->l:Ljava/lang/String;

    .line 260537
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->l:Ljava/lang/String;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260532
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->m:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260533
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->m:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260534
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->m:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 38

    .prologue
    .line 260592
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260593
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 260594
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->s()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 260595
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->t()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 260596
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->u()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 260597
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 260598
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 260599
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->y()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 260600
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 260601
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 260602
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 260603
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 260604
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->C()Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 260605
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 260606
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->E()LX/0Px;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/util/List;)I

    move-result v15

    .line 260607
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->m()Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 260608
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->F()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 260609
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->n()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 260610
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 260611
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 260612
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 260613
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 260614
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 260615
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 260616
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 260617
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 260618
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 260619
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->P()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 260620
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 260621
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->p()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 260622
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->R()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 260623
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->S()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 260624
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->q()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 260625
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->r()Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 260626
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 260627
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 260628
    const/16 v37, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 260629
    const/16 v37, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 260630
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 260631
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 260632
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 260633
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 260634
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 260635
    const/4 v3, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->x()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 260636
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 260637
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 260638
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 260639
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 260640
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 260641
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 260642
    const/16 v2, 0xe

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->D()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 260643
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 260644
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 260645
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260646
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260647
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260648
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260649
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260650
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260651
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260652
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260653
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260654
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260655
    const/16 v2, 0x1b

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->N()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 260656
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260657
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260658
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260659
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260660
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260661
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260662
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260663
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260664
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260665
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260666
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 260667
    const/16 v3, 0x29

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->V()Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 260668
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 260669
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 260670
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLApplication;->V()Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 260414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 260415
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260416
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    .line 260417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 260418
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260419
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->e:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    .line 260420
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 260421
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260422
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 260423
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260424
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->i:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260425
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 260426
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260427
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 260428
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260429
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260430
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 260431
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260432
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 260433
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260434
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->m:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260435
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 260436
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260437
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->A()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 260438
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260439
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260440
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 260441
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260442
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 260443
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260444
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260445
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->r()Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 260446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->r()Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    .line 260447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->r()Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 260448
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260449
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->N:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    .line 260450
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->C()Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 260451
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->C()Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    .line 260452
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->C()Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 260453
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260454
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->q:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    .line 260455
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->m()Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 260456
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->m()Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    .line 260457
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->m()Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 260458
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260459
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->u:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    .line 260460
    :cond_8
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->F()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 260461
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->F()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRating;

    .line 260462
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->F()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 260463
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260464
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->v:Lcom/facebook/graphql/model/GraphQLRating;

    .line 260465
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 260466
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260467
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 260468
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260469
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260470
    :cond_a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 260471
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260472
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 260473
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260474
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260475
    :cond_b
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 260476
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260477
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 260478
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260479
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260480
    :cond_c
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 260481
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260482
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 260483
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260484
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260485
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 260486
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260487
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 260488
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260489
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260490
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 260491
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260492
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->K()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 260493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260494
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->B:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260495
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 260496
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 260497
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->L()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 260498
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260499
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->C:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 260500
    :cond_10
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 260501
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260502
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 260503
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260504
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260505
    :cond_11
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 260506
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260507
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 260508
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260509
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260510
    :cond_12
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 260511
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260512
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 260513
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260514
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 260515
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 260516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 260518
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260519
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260520
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->P()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 260521
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->P()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 260522
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->P()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 260523
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260524
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->H:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 260525
    :cond_15
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 260526
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 260527
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 260528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 260529
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLApplication;->I:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260530
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 260531
    if-nez v1, :cond_17

    :goto_0
    return-object p0

    :cond_17
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 260559
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 260560
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->k:D

    .line 260561
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->r:Z

    .line 260562
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->E:Z

    .line 260563
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 260564
    const v0, -0x3ff252d0

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260565
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->e:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260566
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->e:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->e:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    .line 260567
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->e:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260568
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260569
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->o:Ljava/lang/String;

    .line 260570
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260571
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260572
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->s:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->s:Ljava/lang/String;

    .line 260573
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLMobileStoreObject;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260574
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->u:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260575
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->u:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->u:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    .line 260576
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->u:Lcom/facebook/graphql/model/GraphQLMobileStoreObject;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260577
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260578
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->w:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->w:Ljava/lang/String;

    .line 260579
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260580
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260581
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 260582
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260583
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260584
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->J:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->J:Ljava/lang/String;

    .line 260585
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260586
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260587
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->M:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->M:Ljava/lang/String;

    .line 260588
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260589
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->N:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 260590
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->N:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->N:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    .line 260591
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLApplication;->N:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    return-object v0
.end method
