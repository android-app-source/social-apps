.class public final Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection$Serializer;
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327015
    const-class v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327014
    const-class v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 327012
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327013
    return-void
.end method

.method public constructor <init>(LX/4Xh;)V
    .locals 1

    .prologue
    .line 327008
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327009
    iget-object v0, p1, LX/4Xh;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->e:Ljava/util/List;

    .line 327010
    iget-object v0, p1, LX/4Xh;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 327011
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 326980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326981
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 326982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 326983
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 326984
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 326985
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 326986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326987
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327005
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327006
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->e:Ljava/util/List;

    .line 327007
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 326992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326993
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 326994
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 326995
    if-eqz v1, :cond_2

    .line 326996
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    .line 326997
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->e:Ljava/util/List;

    move-object v1, v0

    .line 326998
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326999
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 327000
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 327001
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    .line 327002
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 327003
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327004
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 326991
    const v0, 0x5f784305

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326988
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326989
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 326990
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->f:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method
