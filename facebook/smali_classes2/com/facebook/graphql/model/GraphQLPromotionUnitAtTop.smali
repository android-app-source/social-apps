.class public final Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 329488
    const-class v0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 329469
    const-class v0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 329489
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 329490
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 329477
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 329478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 329479
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 329480
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->a()Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 329481
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 329482
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 329483
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 329484
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->a()Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 329485
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 329486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 329487
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 329474
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->e:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329475
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->e:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->e:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    .line 329476
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->e:Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 329473
    const v0, 0x74a9dbbb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 329470
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 329471
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->f:Ljava/lang/String;

    .line 329472
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;->f:Ljava/lang/String;

    return-object v0
.end method
