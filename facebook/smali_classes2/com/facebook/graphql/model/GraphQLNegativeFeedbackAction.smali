.class public final Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327566
    const-class v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 327546
    const-class v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 327547
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327548
    return-void
.end method

.method public constructor <init>(LX/4XQ;)V
    .locals 1

    .prologue
    .line 327549
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 327550
    iget-boolean v0, p1, LX/4XQ;->b:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->e:Z

    .line 327551
    iget-object v0, p1, LX/4XQ;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327552
    iget-object v0, p1, LX/4XQ;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327553
    iget-object v0, p1, LX/4XQ;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327554
    iget-object v0, p1, LX/4XQ;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->h:Ljava/lang/String;

    .line 327555
    iget-object v0, p1, LX/4XQ;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->i:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 327556
    iget-object v0, p1, LX/4XQ;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327557
    iget-object v0, p1, LX/4XQ;->i:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 327558
    iget-object v0, p1, LX/4XQ;->j:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    .line 327559
    iget-object v0, p1, LX/4XQ;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327560
    iget-object v0, p1, LX/4XQ;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327561
    iget-object v0, p1, LX/4XQ;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o:Ljava/lang/String;

    .line 327562
    return-void
.end method

.method private m()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327563
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 327564
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 327565
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->e:Z

    return v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327537
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327538
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327539
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327567
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327568
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327569
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327570
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327571
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->h:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->h:Ljava/lang/String;

    .line 327572
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->h:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327573
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327574
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 327575
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327576
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327577
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327578
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327540
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327541
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o:Ljava/lang/String;

    .line 327542
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o:Ljava/lang/String;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327543
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327544
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327545
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 327454
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327455
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 327456
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 327457
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 327458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 327459
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 327460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 327461
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 327462
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->s()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 327463
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 327464
    const/16 v10, 0xf

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 327465
    const/4 v10, 0x1

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->m()Z

    move-result v11

    invoke-virtual {p1, v10, v11}, LX/186;->a(IZ)V

    .line 327466
    const/4 v10, 0x2

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 327467
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 327468
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 327469
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v0, v3, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 327470
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 327471
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 327472
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    if-ne v2, v3, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 327473
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 327474
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 327475
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 327476
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 327477
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327478
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 327479
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    goto :goto_0

    .line 327480
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 327481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 327482
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327483
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327484
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 327485
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 327486
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327487
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 327488
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327489
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 327490
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 327491
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327492
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 327493
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327494
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 327495
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 327496
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327497
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 327498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 327500
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 327501
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327502
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 327503
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 327504
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 327505
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 327506
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 327507
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 327508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 327510
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 327511
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327512
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 327513
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327514
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 327515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    .line 327516
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327517
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 327518
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327519
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 327520
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 327521
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->e:Z

    .line 327522
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327523
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->i:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327524
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->i:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->i:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 327525
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->i:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 1
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327526
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 327527
    const v0, 0x270961d0

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327528
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327529
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327530
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 327531
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327532
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    .line 327533
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327534
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 327535
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 327536
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method
