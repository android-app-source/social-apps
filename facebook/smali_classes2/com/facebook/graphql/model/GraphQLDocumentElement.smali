.class public final Lcom/facebook/graphql/model/GraphQLDocumentElement;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLDocumentElement$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLDocumentElement$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

.field public C:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

.field public D:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

.field public E:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field public k:I

.field public l:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public m:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public r:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

.field public u:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public v:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field public w:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

.field public x:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320068
    const-class v0, Lcom/facebook/graphql/model/GraphQLDocumentElement$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 320069
    const-class v0, Lcom/facebook/graphql/model/GraphQLDocumentElement$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320070
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 320071
    return-void
.end method

.method private A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320072
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320073
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 320074
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320075
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->w:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320076
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->w:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->w:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    .line 320077
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->w:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320078
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->x:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320079
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->x:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->x:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320080
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->x:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320081
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->y:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320082
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->y:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->y:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320083
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->y:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method private E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320084
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->z:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320085
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->z:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->z:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 320086
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->z:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method private F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320087
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320088
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->A:Ljava/lang/String;

    .line 320089
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->A:Ljava/lang/String;

    return-object v0
.end method

.method private G()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320090
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->B:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320091
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->B:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->B:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 320092
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->B:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    return-object v0
.end method

.method private H()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320093
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320094
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 320095
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    return-object v0
.end method

.method private I()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320096
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320097
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 320098
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    return-object v0
.end method

.method private J()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320099
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->E:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320100
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->E:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->E:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 320101
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->E:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    return-object v0
.end method

.method private K()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320102
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320103
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->F:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->F:Ljava/lang/String;

    .line 320104
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->F:Ljava/lang/String;

    return-object v0
.end method

.method private L()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320105
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320106
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->G:Ljava/lang/String;

    .line 320107
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->G:Ljava/lang/String;

    return-object v0
.end method

.method private M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320108
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320109
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->H:Ljava/lang/String;

    .line 320110
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->H:Ljava/lang/String;

    return-object v0
.end method

.method private N()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320111
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->I:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320112
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->I:Ljava/util/List;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->I:Ljava/util/List;

    .line 320113
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->I:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private O()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320114
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->J:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320115
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->J:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->J:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 320116
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->J:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    return-object v0
.end method

.method private P()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320117
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->K:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320118
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->K:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->K:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    .line 320119
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->K:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320059
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 320060
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 320061
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 320062
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 320063
    const/4 v0, 0x0

    .line 320064
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 320065
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 320066
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 320067
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319901
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319902
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->g:Ljava/lang/String;

    .line 319903
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->g:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319907
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319908
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->h:Ljava/lang/String;

    .line 319909
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->h:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319910
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319911
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->i:Ljava/lang/String;

    .line 319912
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->i:Ljava/lang/String;

    return-object v0
.end method

.method private p()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319913
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 319914
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 319915
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->j:I

    return v0
.end method

.method private q()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319916
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 319917
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 319918
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->k:I

    return v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319919
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->l:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319920
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->l:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->l:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 319921
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->l:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319922
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->m:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319923
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->m:Lcom/facebook/graphql/model/GraphQLVideo;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->m:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 319924
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->m:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method

.method private t()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 319925
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 319926
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 319927
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->n:Z

    return v0
.end method

.method private u()Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319928
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->o:Lcom/facebook/graphql/model/GraphQLEvent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319929
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->o:Lcom/facebook/graphql/model/GraphQLEvent;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->o:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 319930
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->o:Lcom/facebook/graphql/model/GraphQLEvent;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319931
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319932
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 319933
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319934
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->q:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319935
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->q:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->q:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 319936
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->q:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319937
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319938
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->r:Ljava/lang/String;

    .line 319939
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->r:Ljava/lang/String;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319940
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->t:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319941
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->t:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->t:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    .line 319942
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->t:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 319943
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319944
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 319945
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 319946
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 319947
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 319948
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->m()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 319949
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->n()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 319950
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 319951
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 319952
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 319953
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 319954
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->x()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 319955
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->j()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 319956
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 319957
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 319958
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->F()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 319959
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->K()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 319960
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->L()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 319961
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->M()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 319962
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->N()LX/0Px;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v17

    .line 319963
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->O()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 319964
    const/16 v19, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 319965
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 319966
    const/16 v19, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->l()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v2

    sget-object v20, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-object/from16 v0, v20

    if-ne v2, v0, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319967
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 319968
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 319969
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 319970
    const/4 v2, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->p()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 319971
    const/4 v2, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->q()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 319972
    const/4 v3, 0x7

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->r()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319973
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 319974
    const/16 v2, 0x9

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->t()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 319975
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 319976
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 319977
    const/16 v3, 0xc

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->w()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319978
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 319979
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 319980
    const/16 v3, 0xf

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->y()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319981
    const/16 v3, 0x10

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->z()Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319982
    const/16 v3, 0x11

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319983
    const/16 v3, 0x12

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319984
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 319985
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 319986
    const/16 v3, 0x15

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v2, v4, :cond_8

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319987
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 319988
    const/16 v3, 0x17

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->G()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    if-ne v2, v4, :cond_9

    const/4 v2, 0x0

    :goto_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319989
    const/16 v3, 0x18

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->H()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    if-ne v2, v4, :cond_a

    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319990
    const/16 v3, 0x19

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->I()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    if-ne v2, v4, :cond_b

    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319991
    const/16 v3, 0x1a

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->J()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v2, v4, :cond_c

    const/4 v2, 0x0

    :goto_c
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319992
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 319993
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 319994
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319995
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319996
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 319997
    const/16 v3, 0x20

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->P()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    if-ne v2, v4, :cond_d

    const/4 v2, 0x0

    :goto_d
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 319998
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 319999
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 320000
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 320001
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->l()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v2

    goto/16 :goto_1

    .line 320002
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->r()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v2

    goto/16 :goto_2

    .line 320003
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->w()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    goto/16 :goto_3

    .line 320004
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->y()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v2

    goto/16 :goto_4

    .line 320005
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->z()Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-result-object v2

    goto/16 :goto_5

    .line 320006
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v2

    goto/16 :goto_6

    .line 320007
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v2

    goto/16 :goto_7

    .line 320008
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    goto/16 :goto_8

    .line 320009
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->G()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v2

    goto/16 :goto_9

    .line 320010
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->H()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v2

    goto/16 :goto_a

    .line 320011
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->I()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v2

    goto/16 :goto_b

    .line 320012
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->J()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v2

    goto/16 :goto_c

    .line 320013
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->P()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object v2

    goto :goto_d
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 320014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 320015
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->N()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 320016
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->N()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 320017
    if-eqz v1, :cond_7

    .line 320018
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 320019
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->I:Ljava/util/List;

    move-object v1, v0

    .line 320020
    :goto_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->O()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320021
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->O()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 320022
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->O()Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 320023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 320024
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;->J:Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    .line 320025
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 320026
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 320027
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 320028
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 320029
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;->m:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 320030
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 320031
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEvent;

    .line 320032
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->u()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 320033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 320034
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;->o:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 320035
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 320036
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 320037
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->v()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 320038
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 320039
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 320040
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 320041
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320042
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->C()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 320043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 320044
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;->x:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320045
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 320046
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320047
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->D()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 320048
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;

    .line 320049
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLDocumentElement;->y:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 320050
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 320051
    if-nez v1, :cond_6

    :goto_1
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_1

    :cond_7
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 320052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLDocumentElement;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 320053
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 320054
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->j:I

    .line 320055
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->k:I

    .line 320056
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->n:Z

    .line 320057
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 320058
    const v0, 0x1c343941

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319904
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 319905
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s:Ljava/lang/String;

    .line 319906
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLDocumentElement;->s:Ljava/lang/String;

    return-object v0
.end method
