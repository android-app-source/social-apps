.class public final Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262850
    const-class v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 262849
    const-class v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 262847
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 262848
    return-void
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262844
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262845
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262846
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262838
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262839
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262840
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->f:Z

    return v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262835
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262836
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->h:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->h:Ljava/lang/String;

    .line 262837
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262832
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262833
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262834
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262829
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262830
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262831
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262841
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262842
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262843
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262826
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 262827
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->l:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->l:Ljava/lang/String;

    .line 262828
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262823
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262824
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262825
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->m:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 262804
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262805
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 262806
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 262807
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 262808
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 262809
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 262810
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 262811
    const/16 v6, 0xc

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 262812
    const/4 v6, 0x3

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 262813
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->l()Z

    move-result v6

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 262814
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->j()Z

    move-result v6

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 262815
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 262816
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 262817
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 262818
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 262819
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 262820
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->r()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 262821
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262822
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 262781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 262782
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262783
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262784
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 262785
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 262786
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262787
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262788
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262789
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 262790
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 262791
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->i:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262792
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 262793
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262794
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 262795
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 262796
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262797
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 262798
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262799
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 262800
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 262801
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 262802
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 262803
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262771
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 262776
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 262777
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->f:Z

    .line 262778
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->g:Z

    .line 262779
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->m:Z

    .line 262780
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 262775
    const v0, 0x2f9344b6

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 262772
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 262773
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 262774
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->g:Z

    return v0
.end method
