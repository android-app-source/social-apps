.class public final Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

.field public R:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation
.end field

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation
.end field

.field public z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 265416
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 265417
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 265418
    const/16 v0, 0x2c

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 265419
    return-void
.end method

.method public constructor <init>(LX/4Yt;)V
    .locals 1

    .prologue
    .line 265420
    const/16 v0, 0x2c

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 265421
    iget-object v0, p1, LX/4Yt;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H:Ljava/util/List;

    .line 265422
    iget-boolean v0, p1, LX/4Yt;->c:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->f:Z

    .line 265423
    iget-boolean v0, p1, LX/4Yt;->d:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->g:Z

    .line 265424
    iget-object v0, p1, LX/4Yt;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265425
    iget-object v0, p1, LX/4Yt;->f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->i:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 265426
    iget-object v0, p1, LX/4Yt;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 265427
    iget-object v0, p1, LX/4Yt;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265428
    iget-object v0, p1, LX/4Yt;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P:Ljava/lang/String;

    .line 265429
    iget-object v0, p1, LX/4Yt;->j:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    .line 265430
    iget-object v0, p1, LX/4Yt;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->j:Ljava/lang/String;

    .line 265431
    iget-object v0, p1, LX/4Yt;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k:Ljava/lang/String;

    .line 265432
    iget-object v0, p1, LX/4Yt;->m:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    .line 265433
    iget-object v0, p1, LX/4Yt;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m:Ljava/lang/String;

    .line 265434
    iget-object v0, p1, LX/4Yt;->o:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 265435
    iget-object v0, p1, LX/4Yt;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->n:Ljava/lang/String;

    .line 265436
    iget-object v0, p1, LX/4Yt;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->T:Ljava/lang/String;

    .line 265437
    iget-object v0, p1, LX/4Yt;->r:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 265438
    iget-object v0, p1, LX/4Yt;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D:Ljava/lang/String;

    .line 265439
    iget-object v0, p1, LX/4Yt;->t:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I:Ljava/util/List;

    .line 265440
    iget-object v0, p1, LX/4Yt;->u:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 265441
    iget-object v0, p1, LX/4Yt;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->E:Ljava/lang/String;

    .line 265442
    iget-object v0, p1, LX/4Yt;->w:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 265443
    iget-object v0, p1, LX/4Yt;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K:Ljava/lang/String;

    .line 265444
    iget-object v0, p1, LX/4Yt;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L:Ljava/lang/String;

    .line 265445
    iget-object v0, p1, LX/4Yt;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->o:Ljava/lang/String;

    .line 265446
    iget-object v0, p1, LX/4Yt;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p:Ljava/lang/String;

    .line 265447
    iget-object v0, p1, LX/4Yt;->B:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q:Ljava/util/List;

    .line 265448
    iget v0, p1, LX/4Yt;->C:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->r:I

    .line 265449
    iget v0, p1, LX/4Yt;->D:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->s:I

    .line 265450
    iget v0, p1, LX/4Yt;->E:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->t:I

    .line 265451
    iget v0, p1, LX/4Yt;->F:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u:I

    .line 265452
    iget-object v0, p1, LX/4Yt;->G:Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 265453
    iget-object v0, p1, LX/4Yt;->H:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 265454
    iget-object v0, p1, LX/4Yt;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x:Ljava/lang/String;

    .line 265455
    iget-object v0, p1, LX/4Yt;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 265456
    iget-object v0, p1, LX/4Yt;->K:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y:Ljava/util/List;

    .line 265457
    iget-object v0, p1, LX/4Yt;->L:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G:Lcom/facebook/graphql/model/GraphQLStory;

    .line 265458
    iget-object v0, p1, LX/4Yt;->M:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 265459
    iget-boolean v0, p1, LX/4Yt;->N:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z:Z

    .line 265460
    iget-object v0, p1, LX/4Yt;->O:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A:Ljava/lang/String;

    .line 265461
    iget-object v0, p1, LX/4Yt;->P:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 265462
    iget-object v0, p1, LX/4Yt;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->B:Ljava/lang/String;

    .line 265463
    iget-object v0, p1, LX/4Yt;->R:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 265464
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265465
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265466
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 265467
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265468
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265469
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x:Ljava/lang/String;

    .line 265470
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final C()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265471
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265472
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y:Ljava/util/List;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y:Ljava/util/List;

    .line 265473
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265474
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 265475
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 265476
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z:Z

    return v0
.end method

.method public final E()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265477
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265478
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A:Ljava/lang/String;

    .line 265479
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265480
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265481
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->B:Ljava/lang/String;

    .line 265482
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265483
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265484
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 265485
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265486
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265487
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D:Ljava/lang/String;

    .line 265488
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265489
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265490
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->E:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->E:Ljava/lang/String;

    .line 265491
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265492
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265493
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265494
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265495
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265496
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G:Lcom/facebook/graphql/model/GraphQLStory;

    .line 265497
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final L()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265498
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265499
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H:Ljava/util/List;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H:Ljava/util/List;

    .line 265500
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final M()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265501
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265502
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I:Ljava/util/List;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I:Ljava/util/List;

    .line 265503
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265504
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265505
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J:Lcom/facebook/graphql/model/GraphQLApplication;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 265506
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265507
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265508
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K:Ljava/lang/String;

    .line 265509
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265510
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265511
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L:Ljava/lang/String;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L:Ljava/lang/String;

    .line 265512
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265513
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 265515
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265516
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265517
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 265518
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    return-object v0
.end method

.method public final S()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265519
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265520
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 265521
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    return-object v0
.end method

.method public final T()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265522
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265523
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P:Ljava/lang/String;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P:Ljava/lang/String;

    .line 265524
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265410
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265411
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    .line 265412
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265413
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265414
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 265415
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265167
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265168
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 265169
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final X()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265173
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->T:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265174
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->T:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->T:Ljava/lang/String;

    .line 265175
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final Y()Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265176
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265177
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 265178
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 37

    .prologue
    .line 265179
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 265180
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/String;)I

    move-result v2

    .line 265181
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 265182
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 265183
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 265184
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 265185
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 265186
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->r()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 265187
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->s()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 265188
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->t()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 265189
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u()LX/0Px;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 265190
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 265191
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 265192
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->B()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 265193
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C()LX/0Px;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v15

    .line 265194
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->E()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 265195
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 265196
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 265197
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 265198
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 265199
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 265200
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 265201
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L()LX/0Px;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v23

    .line 265202
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M()LX/0Px;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v24

    .line 265203
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 265204
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 265205
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 265206
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 265207
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 265208
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 265209
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->T()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 265210
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 265211
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->W()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 265212
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->X()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 265213
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 265214
    const/16 v36, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 265215
    const/16 v36, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 265216
    const/4 v2, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->j()Z

    move-result v36

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 265217
    const/4 v2, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k()Z

    move-result v36

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 265218
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 265219
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 265220
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 265221
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 265222
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 265223
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 265224
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 265225
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 265226
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 265227
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 265228
    const/16 v2, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 265229
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 265230
    const/16 v2, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 265231
    const/16 v2, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 265232
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 265233
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 265234
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 265235
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 265236
    const/16 v2, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 265237
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265238
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265239
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265240
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265241
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265242
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265243
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265244
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265245
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265246
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265247
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265248
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265249
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265250
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265251
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265252
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265253
    const/16 v3, 0x26

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U()Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 265254
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265255
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265256
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265257
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 265258
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 265259
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 265260
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 265261
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v2

    goto/16 :goto_1

    .line 265262
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U()Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 265263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 265264
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 265265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 265267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265268
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265269
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 265270
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 265271
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 265272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265273
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->i:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 265274
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 265275
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 265276
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 265277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265278
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 265279
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 265280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 265282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265283
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265284
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 265285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroup;

    .line 265286
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 265287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265288
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 265289
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 265290
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 265291
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 265292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265293
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 265294
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 265295
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 265296
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 265297
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265298
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 265299
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->W()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 265300
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->W()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 265301
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->W()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 265302
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265303
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 265304
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 265305
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 265306
    if-eqz v2, :cond_8

    .line 265307
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265308
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q:Ljava/util/List;

    move-object v1, v0

    .line 265309
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 265310
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 265311
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 265312
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265313
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 265314
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 265315
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 265316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 265317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265318
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 265319
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 265320
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 265321
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 265322
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265323
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 265324
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 265325
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 265326
    if-eqz v2, :cond_c

    .line 265327
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265328
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y:Ljava/util/List;

    move-object v1, v0

    .line 265329
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 265330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265331
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 265332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265333
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G:Lcom/facebook/graphql/model/GraphQLStory;

    .line 265334
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 265335
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 265336
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 265337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265338
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 265339
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 265340
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 265341
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 265342
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 265343
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 265344
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 265345
    if-nez v1, :cond_10

    :goto_0
    return-object p0

    :cond_10
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265346
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 265347
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 265348
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 265349
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-nez v0, :cond_1

    .line 265350
    const/4 v0, 0x0

    .line 265351
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265352
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 265353
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->f:Z

    .line 265354
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->g:Z

    .line 265355
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->r:I

    .line 265356
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->s:I

    .line 265357
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->t:I

    .line 265358
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u:I

    .line 265359
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z:Z

    .line 265360
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 265361
    const v0, -0x4a6acef9

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265362
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 265363
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 265364
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->f:Z

    return v0
.end method

.method public final k()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265365
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 265366
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 265367
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->g:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265368
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265369
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 265370
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265371
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->i:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265372
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->i:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->i:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 265373
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->i:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265374
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265375
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->j:Ljava/lang/String;

    .line 265376
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265377
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265378
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k:Ljava/lang/String;

    .line 265379
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265380
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265381
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    .line 265382
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265170
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265171
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m:Ljava/lang/String;

    .line 265172
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265383
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265384
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->n:Ljava/lang/String;

    .line 265385
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265386
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265387
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->o:Ljava/lang/String;

    .line 265388
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265389
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265390
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p:Ljava/lang/String;

    .line 265391
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265392
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265393
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q:Ljava/util/List;

    .line 265394
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final v()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265395
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 265396
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 265397
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->r:I

    return v0
.end method

.method public final w()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265398
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 265399
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 265400
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->s:I

    return v0
.end method

.method public final x()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265401
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 265402
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 265403
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->t:I

    return v0
.end method

.method public final y()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 265404
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 265405
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 265406
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u:I

    return v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLLocation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265407
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 265408
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v:Lcom/facebook/graphql/model/GraphQLLocation;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 265409
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v:Lcom/facebook/graphql/model/GraphQLLocation;

    return-object v0
.end method
