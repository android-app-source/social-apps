.class public final Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 189186
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 189185
    const-class v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 189183
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 189184
    return-void
.end method

.method public constructor <init>(LX/3dO;)V
    .locals 1

    .prologue
    .line 189177
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 189178
    iget v0, p1, LX/3dO;->b:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->e:I

    .line 189179
    iget-object v0, p1, LX/3dO;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->f:Ljava/util/List;

    .line 189180
    iget-object v0, p1, LX/3dO;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->h:Ljava/util/List;

    .line 189181
    iget-object v0, p1, LX/3dO;->e:Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 189182
    return-void
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189126
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189127
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->h:Ljava/util/List;

    .line 189128
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 189163
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 189164
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 189165
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 189166
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 189167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 189168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 189169
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->l()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 189170
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 189171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 189172
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 189173
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 189174
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 189175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 189176
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 189145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 189146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 189147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 189148
    if-eqz v1, :cond_0

    .line 189149
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 189150
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->f:Ljava/util/List;

    .line 189151
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 189152
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 189153
    if-eqz v1, :cond_1

    .line 189154
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 189155
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->h:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 189156
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 189157
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 189158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 189159
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 189160
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 189161
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 189162
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 189139
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->e:I

    .line 189140
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 189141
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 189142
    if-eqz v0, :cond_0

    .line 189143
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 189144
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 189136
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 189137
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->e:I

    .line 189138
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 189135
    const v0, 0xf50222f

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189132
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189133
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->f:Ljava/util/List;

    .line 189134
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189129
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 189130
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 189131
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method
