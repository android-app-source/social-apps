.class public final Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTopReactionsEdge$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTopReactionsEdge$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321889
    const-class v0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 321888
    const-class v0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321869
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321870
    return-void
.end method

.method public constructor <init>(LX/4ZJ;)V
    .locals 1

    .prologue
    .line 321884
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 321885
    iget-object v0, p1, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->e:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321886
    iget v0, p1, LX/4ZJ;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->f:I

    .line 321887
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 321877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321878
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 321879
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 321880
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 321881
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 321882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321883
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 321890
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 321891
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 321892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 321894
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    .line 321895
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->e:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321896
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 321897
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321874
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->e:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 321875
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->e:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->e:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 321876
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->e:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 321871
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 321872
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->f:I

    .line 321873
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 321868
    const v0, -0x667f32ee

    return v0
.end method

.method public final j()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 321865
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 321866
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 321867
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->f:I

    return v0
.end method
