.class public final Lcom/facebook/graphql/model/GraphQLPostTranslatability;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPostTranslatability$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPostTranslatability$Serializer;
.end annotation


# instance fields
.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLTranslation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 264433
    const-class v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 264383
    const-class v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 264431
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 264432
    return-void
.end method

.method private o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264428
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264429
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264430
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 264410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 264411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 264412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 264413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 264414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 264415
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 264416
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 264417
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 264418
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 264419
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 264420
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 264421
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 264422
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 264423
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 264424
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 264425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 264426
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 264427
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 264397
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 264398
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 264399
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264400
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 264401
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 264402
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 264403
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 264404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 264405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 264406
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 264407
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->i:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 264408
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 264409
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264434
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264435
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->e:Ljava/lang/String;

    .line 264436
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 264396
    const v0, -0x643bd98d

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264393
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264394
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->f:Ljava/lang/String;

    .line 264395
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264390
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264391
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->g:Ljava/lang/String;

    .line 264392
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264387
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264388
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->h:Ljava/lang/String;

    .line 264389
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/model/GraphQLTranslation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 264384
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->i:Lcom/facebook/graphql/model/GraphQLTranslation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264385
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->i:Lcom/facebook/graphql/model/GraphQLTranslation;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLTranslation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTranslation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->i:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 264386
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->i:Lcom/facebook/graphql/model/GraphQLTranslation;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 264380
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->j:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 264381
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->j:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->j:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 264382
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->j:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    return-object v0
.end method
