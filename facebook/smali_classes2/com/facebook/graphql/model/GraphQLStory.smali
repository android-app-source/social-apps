.class public final Lcom/facebook/graphql/model/GraphQLStory;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/16m;
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16n;
.implements LX/16o;
.implements LX/16d;
.implements LX/16e;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
.implements Lcom/facebook/graphql/model/Sponsorable;
.implements LX/16g;
.implements LX/16q;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLStory$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLStory$Serializer;
.end annotation


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field

.field public I:J

.field public J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Z

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLIcon;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLStoryInsights;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Z

.field public U:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Z

.field public W:Z

.field public X:Z

.field public Y:Z

.field public Z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLTranslation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation
.end field

.field public aO:Z

.field public aP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Z

.field public aR:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Z

.field public aV:Lcom/facebook/graphql/model/GraphQLFeedBackendData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public aX:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aY:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:I

.field public ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLBoostedComponent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLSticker;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public aq:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:J

.field public ax:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;",
            ">;"
        }
    .end annotation
.end field

.field public ay:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public az:I

.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphAction;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLBackdatedTime;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public w:Z

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 186795
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 186796
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 186797
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 186798
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x4c808d5

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 186799
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aY:LX/0x2;

    .line 186800
    return-void
.end method

.method public constructor <init>(LX/23u;)V
    .locals 2

    .prologue
    .line 186801
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 186802
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x4c808d5

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 186803
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aY:LX/0x2;

    .line 186804
    iget-object v0, p1, LX/23u;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->f:Ljava/util/List;

    .line 186805
    iget-object v0, p1, LX/23u;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->g:Ljava/util/List;

    .line 186806
    iget-object v0, p1, LX/23u;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->h:Ljava/util/List;

    .line 186807
    iget-object v0, p1, LX/23u;->e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->i:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 186808
    iget-object v0, p1, LX/23u;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->j:Ljava/util/List;

    .line 186809
    iget-object v0, p1, LX/23u;->g:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 186810
    iget-object v0, p1, LX/23u;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 186811
    iget-object v0, p1, LX/23u;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->m:Ljava/util/List;

    .line 186812
    iget-object v0, p1, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->n:Lcom/facebook/graphql/model/GraphQLStory;

    .line 186813
    iget-object v0, p1, LX/23u;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->o:Ljava/util/List;

    .line 186814
    iget-object v0, p1, LX/23u;->l:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->p:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 186815
    iget-object v0, p1, LX/23u;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->q:Ljava/lang/String;

    .line 186816
    iget-boolean v0, p1, LX/23u;->n:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->r:Z

    .line 186817
    iget-boolean v0, p1, LX/23u;->o:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->s:Z

    .line 186818
    iget-boolean v0, p1, LX/23u;->p:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->t:Z

    .line 186819
    iget-boolean v0, p1, LX/23u;->q:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->u:Z

    .line 186820
    iget-boolean v0, p1, LX/23u;->r:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->v:Z

    .line 186821
    iget-boolean v0, p1, LX/23u;->s:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->w:Z

    .line 186822
    iget-object v0, p1, LX/23u;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->x:Ljava/lang/String;

    .line 186823
    iget-object v0, p1, LX/23u;->u:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 186824
    iget-wide v0, p1, LX/23u;->v:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->z:J

    .line 186825
    iget-object v0, p1, LX/23u;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->A:Ljava/lang/String;

    .line 186826
    iget-object v0, p1, LX/23u;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186827
    iget-object v0, p1, LX/23u;->y:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aW:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 186828
    iget-object v0, p1, LX/23u;->z:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->C:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 186829
    iget-object v0, p1, LX/23u;->A:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->D:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 186830
    iget-object v0, p1, LX/23u;->B:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aV:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    .line 186831
    iget-object v0, p1, LX/23u;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->E:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 186832
    iget-object v0, p1, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 186833
    iget-object v0, p1, LX/23u;->E:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->G:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 186834
    iget-object v0, p1, LX/23u;->F:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->H:Ljava/util/List;

    .line 186835
    iget-wide v0, p1, LX/23u;->G:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->I:J

    .line 186836
    iget-object v0, p1, LX/23u;->H:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 186837
    iget-object v0, p1, LX/23u;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 186838
    iget-boolean v0, p1, LX/23u;->J:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->L:Z

    .line 186839
    iget-object v0, p1, LX/23u;->K:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->M:Ljava/lang/String;

    .line 186840
    iget-object v0, p1, LX/23u;->L:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->N:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    .line 186841
    iget-object v0, p1, LX/23u;->M:Lcom/facebook/graphql/model/GraphQLIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->O:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 186842
    iget-object v0, p1, LX/23u;->N:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->P:Ljava/lang/String;

    .line 186843
    iget-object v0, p1, LX/23u;->O:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Q:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 186844
    iget-object v0, p1, LX/23u;->P:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->R:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 186845
    iget-object v0, p1, LX/23u;->Q:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->S:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 186846
    iget-boolean v0, p1, LX/23u;->R:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aQ:Z

    .line 186847
    iget-boolean v0, p1, LX/23u;->S:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->T:Z

    .line 186848
    iget-object v0, p1, LX/23u;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->U:Ljava/lang/String;

    .line 186849
    iget-boolean v0, p1, LX/23u;->U:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aU:Z

    .line 186850
    iget-object v0, p1, LX/23u;->V:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aX:Lcom/facebook/graphql/model/FeedUnit;

    .line 186851
    iget-boolean v0, p1, LX/23u;->W:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->V:Z

    .line 186852
    iget-boolean v0, p1, LX/23u;->X:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->W:Z

    .line 186853
    iget-boolean v0, p1, LX/23u;->Y:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->X:Z

    .line 186854
    iget-boolean v0, p1, LX/23u;->Z:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Y:Z

    .line 186855
    iget-object v0, p1, LX/23u;->aa:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Z:Ljava/lang/String;

    .line 186856
    iget-object v0, p1, LX/23u;->ab:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aa:Ljava/lang/String;

    .line 186857
    iget v0, p1, LX/23u;->ac:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ab:I

    .line 186858
    iget-object v0, p1, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186859
    iget-object v0, p1, LX/23u;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186860
    iget-object v0, p1, LX/23u;->af:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aR:Ljava/util/List;

    .line 186861
    iget-object v0, p1, LX/23u;->ag:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ae:Ljava/util/List;

    .line 186862
    iget-object v0, p1, LX/23u;->ah:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->af:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 186863
    iget-object v0, p1, LX/23u;->ai:Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ag:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 186864
    iget-object v0, p1, LX/23u;->aj:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ah:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 186865
    iget-object v0, p1, LX/23u;->ak:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ai:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 186866
    iget-object v0, p1, LX/23u;->al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 186867
    iget-object v0, p1, LX/23u;->am:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ak:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 186868
    iget-object v0, p1, LX/23u;->an:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->al:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    .line 186869
    iget-object v0, p1, LX/23u;->ao:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aS:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 186870
    iget-object v0, p1, LX/23u;->ap:Lcom/facebook/graphql/model/GraphQLSticker;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->am:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 186871
    iget-object v0, p1, LX/23u;->aq:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 186872
    iget-object v0, p1, LX/23u;->ar:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ao:Ljava/util/List;

    .line 186873
    iget-object v0, p1, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ap:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 186874
    iget-object v0, p1, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aq:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 186875
    iget-object v0, p1, LX/23u;->au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186876
    iget-object v0, p1, LX/23u;->av:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->as:Ljava/lang/String;

    .line 186877
    iget-object v0, p1, LX/23u;->aw:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->at:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 186878
    iget-object v0, p1, LX/23u;->ax:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->au:Ljava/util/List;

    .line 186879
    iget-object v0, p1, LX/23u;->ay:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->av:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 186880
    iget-wide v0, p1, LX/23u;->az:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aw:J

    .line 186881
    iget-object v0, p1, LX/23u;->aA:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ax:Ljava/util/List;

    .line 186882
    iget-object v0, p1, LX/23u;->aB:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ay:Ljava/util/List;

    .line 186883
    iget v0, p1, LX/23u;->aC:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->az:I

    .line 186884
    iget-object v0, p1, LX/23u;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186885
    iget-object v0, p1, LX/23u;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186886
    iget-object v0, p1, LX/23u;->aF:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aC:Lcom/facebook/graphql/model/GraphQLStory;

    .line 186887
    iget-object v0, p1, LX/23u;->aG:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 186888
    iget-object v0, p1, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186889
    iget-object v0, p1, LX/23u;->aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186890
    iget-object v0, p1, LX/23u;->aJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186891
    iget-object v0, p1, LX/23u;->aK:Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aG:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 186892
    iget-object v0, p1, LX/23u;->aL:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aH:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    .line 186893
    iget-object v0, p1, LX/23u;->aM:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aI:Ljava/lang/String;

    .line 186894
    iget-object v0, p1, LX/23u;->aN:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 186895
    iget-object v0, p1, LX/23u;->aO:Lcom/facebook/graphql/model/GraphQLTranslation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aK:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 186896
    iget-object v0, p1, LX/23u;->aP:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aL:Ljava/lang/String;

    .line 186897
    iget-object v0, p1, LX/23u;->aQ:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aM:Lcom/facebook/graphql/model/GraphQLActor;

    .line 186898
    iget-object v0, p1, LX/23u;->aR:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aN:Ljava/util/List;

    .line 186899
    iget-boolean v0, p1, LX/23u;->aS:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aO:Z

    .line 186900
    iget-object v0, p1, LX/23u;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 186901
    iget-object v0, p1, LX/23u;->aU:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aY:LX/0x2;

    .line 186902
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 186903
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->ab:I

    .line 186904
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186905
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186906
    if-eqz v0, :cond_0

    .line 186907
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x31

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 186908
    :cond_0
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186909
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->o:Ljava/util/List;

    .line 186910
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186911
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186912
    if-eqz v0, :cond_0

    .line 186913
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 186914
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V
    .locals 3

    .prologue
    .line 186915
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->ap:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 186916
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186917
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186918
    if-eqz v0, :cond_0

    .line 186919
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/Enum;)V

    .line 186920
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186921
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aX:Lcom/facebook/graphql/model/FeedUnit;

    .line 186922
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186923
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186924
    if-eqz v0, :cond_0

    .line 186925
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x62

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 186926
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyScope;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186786
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 186787
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186788
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186789
    if-eqz v0, :cond_0

    .line 186790
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 186791
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186930
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 186931
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186932
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186933
    if-eqz v0, :cond_0

    .line 186934
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 186935
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186936
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186937
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186938
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186939
    if-eqz v0, :cond_0

    .line 186940
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 186941
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186942
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->Z:Ljava/lang/String;

    .line 186943
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186944
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186945
    if-eqz v0, :cond_0

    .line 186946
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2f

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 186947
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 186948
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->V:Z

    .line 186949
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186950
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186951
    if-eqz v0, :cond_0

    .line 186952
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 186953
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186954
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186955
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186956
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186957
    if-eqz v0, :cond_0

    .line 186958
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x4e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 186959
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186960
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aa:Ljava/lang/String;

    .line 186961
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186962
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186963
    if-eqz v0, :cond_0

    .line 186964
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x30

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 186965
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 186966
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->W:Z

    .line 186967
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186968
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186969
    if-eqz v0, :cond_0

    .line 186970
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 186971
    :cond_0
    return-void
.end method

.method private c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186972
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186973
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186974
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186975
    if-eqz v0, :cond_0

    .line 186976
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x50

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 186977
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 186757
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->X:Z

    .line 186758
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186759
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186760
    if-eqz v0, :cond_0

    .line 186761
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 186762
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 186724
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->Y:Z

    .line 186725
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186726
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186727
    if-eqz v0, :cond_0

    .line 186728
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 186729
    :cond_0
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 186730
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aU:Z

    .line 186731
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 186732
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 186733
    if-eqz v0, :cond_0

    .line 186734
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 186735
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 186736
    invoke-interface {p0}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 186737
    if-eqz v0, :cond_0

    .line 186738
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object p0

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :goto_0
    move v0, p0

    .line 186739
    :goto_1
    move v0, v0

    .line 186740
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->a()I

    move-result p0

    goto :goto_0
.end method

.method public final B()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186741
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186742
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->f:Ljava/util/List;

    .line 186743
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final C()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186744
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186745
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->g:Ljava/util/List;

    .line 186746
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186747
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186748
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->M:Ljava/lang/String;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->M:Ljava/lang/String;

    .line 186749
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final D()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186750
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186751
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->h:Ljava/util/List;

    .line 186752
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 186753
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186754
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->i:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186755
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->i:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->i:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 186756
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->i:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186721
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186722
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->A:Ljava/lang/String;

    .line 186723
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final F()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186763
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186764
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->j:Ljava/util/List;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->j:Ljava/util/List;

    .line 186765
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186766
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186767
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186768
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->I:J

    return-wide v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186927
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->k:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186928
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->k:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 186929
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->k:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLApplication;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186769
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186770
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 186771
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    return-object v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 186772
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final I()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186773
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186774
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->m:Ljava/util/List;

    .line 186775
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final I_()I
    .locals 1

    .prologue
    .line 186776
    invoke-static {p0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result v0

    return v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186777
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->n:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186778
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->n:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->n:Lcom/facebook/graphql/model/GraphQLStory;

    .line 186779
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->n:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final K()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186780
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186781
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->o:Ljava/util/List;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->o:Ljava/util/List;

    .line 186782
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->o:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186783
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->p:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186784
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->p:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->p:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 186785
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->p:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    return-object v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 187362
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aY:LX/0x2;

    if-nez v0, :cond_0

    .line 187363
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aY:LX/0x2;

    .line 187364
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aY:LX/0x2;

    return-object v0
.end method

.method public final M()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 187456
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 187457
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 187458
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->r:Z

    return v0
.end method

.method public final M_()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 8

    .prologue
    .line 187442
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187443
    if-eqz p0, :cond_4

    .line 187444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    or-int/lit8 v3, v0, 0x0

    .line 187445
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    or-int/2addr v0, v3

    .line 187446
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 187447
    invoke-static {p0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    move v3, v0

    .line 187448
    :goto_2
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187449
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_3
    or-int/2addr v3, v0

    .line 187450
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_0
    move v0, v2

    .line 187451
    goto :goto_0

    :cond_1
    move v0, v2

    .line 187452
    goto :goto_1

    :cond_2
    move v0, v2

    .line 187453
    goto :goto_3

    :cond_3
    move v2, v3

    .line 187454
    :cond_4
    :goto_4
    if-eqz v2, :cond_5

    new-instance v0, Lcom/facebook/graphql/model/SponsoredImpression;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/SponsoredImpression;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    :goto_5
    move-object v0, v0

    .line 187455
    return-object v0

    :cond_5
    sget-object v0, Lcom/facebook/graphql/model/SponsoredImpression;->n:Lcom/facebook/graphql/model/SponsoredImpression;

    goto :goto_5

    :cond_6
    move v2, v0

    goto :goto_4
.end method

.method public final N()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 187637
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 187638
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 187639
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->s:Z

    return v0
.end method

.method public final O()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 187640
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 187641
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 187642
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->t:Z

    return v0
.end method

.method public final P()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 187643
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 187644
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 187645
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->u:Z

    return v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 187646
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final Q()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 187647
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 187648
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 187649
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->v:Z

    return v0
.end method

.method public final Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 187650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v0, v0

    .line 187651
    return-object v0
.end method

.method public final R()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 187670
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 187671
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 187672
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->w:Z

    return v0
.end method

.method public final S()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187652
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187653
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->x:Ljava/lang/String;

    .line 187654
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final T()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187673
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->y:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187674
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->y:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 187675
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->y:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final U()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 187667
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 187668
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 187669
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->z:J

    return-wide v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187676
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187677
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187678
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187664
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->C:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187665
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->C:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->C:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 187666
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->C:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    return-object v0
.end method

.method public final X()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187661
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->D:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187662
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->D:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->D:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 187663
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->D:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187658
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->E:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187659
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->E:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->E:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 187660
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->E:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187655
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->G:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187656
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->G:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->G:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 187657
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->G:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 80

    .prologue
    .line 187459
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 187460
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 187461
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->C()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 187462
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 187463
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 187464
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->F()LX/0Px;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 187465
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 187466
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 187467
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->I()LX/0Px;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 187468
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 187469
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 187470
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 187471
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 187472
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 187473
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->T()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 187474
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 187475
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 187476
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 187477
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 187478
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 187479
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 187480
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 187481
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aa()LX/0Px;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v25

    .line 187482
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ab()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 187483
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 187484
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->C_()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 187485
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 187486
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 187487
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 187488
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 187489
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 187490
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 187491
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 187492
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aq()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 187493
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ar()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 187494
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 187495
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 187496
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->av()LX/0Px;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v40

    .line 187497
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 187498
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aw()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 187499
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ax()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 187500
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 187501
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 187502
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 187503
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 187504
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 187505
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 187506
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aE()LX/0Px;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v50

    .line 187507
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 187508
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 187509
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aI()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v53

    .line 187510
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 187511
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aK()LX/0Px;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v55

    .line 187512
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 187513
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aN()LX/0Px;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v57

    .line 187514
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v58

    .line 187515
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 187516
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 187517
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v61

    .line 187518
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v62

    .line 187519
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 187520
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 187521
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v65

    .line 187522
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 187523
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v67

    .line 187524
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v68

    .line 187525
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aZ()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 187526
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v70

    .line 187527
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bb()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v71

    .line 187528
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bc()LX/0Px;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-virtual {v0, v1}, LX/186;->d(Ljava/util/List;)I

    move-result v72

    .line 187529
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 187530
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bg()LX/0Px;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v74

    .line 187531
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v75

    .line 187532
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v76

    .line 187533
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bk()Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v77

    .line 187534
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bm()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v78

    sget-object v79, LX/16Z;->a:LX/16Z;

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    move-object/from16 v2, v79

    invoke-virtual {v0, v1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v78

    .line 187535
    const/16 v79, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 187536
    const/16 v79, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 187537
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 187538
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 187539
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 187540
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 187541
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 187542
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 187543
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 187544
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 187545
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 187546
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 187547
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 187548
    const/16 v4, 0xd

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->M()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187549
    const/16 v4, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->N()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187550
    const/16 v4, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187551
    const/16 v4, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->P()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187552
    const/16 v4, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->Q()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187553
    const/16 v4, 0x12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->R()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187554
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187555
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187556
    const/16 v5, 0x15

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 187557
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187558
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187559
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187560
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187561
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187562
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187563
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187564
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187565
    const/16 v5, 0x1e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 187566
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187567
    const/16 v4, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187568
    const/16 v4, 0x21

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ad()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187569
    const/16 v4, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187570
    const/16 v4, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187571
    const/16 v4, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187572
    const/16 v4, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187573
    const/16 v4, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187574
    const/16 v4, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187575
    const/16 v4, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187576
    const/16 v4, 0x29

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187577
    const/16 v4, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187578
    const/16 v4, 0x2b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->am()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187579
    const/16 v4, 0x2c

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->an()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187580
    const/16 v4, 0x2d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ao()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187581
    const/16 v4, 0x2e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->ap()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187582
    const/16 v4, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187583
    const/16 v4, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187584
    const/16 v4, 0x31

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->as()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 187585
    const/16 v4, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187586
    const/16 v4, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187587
    const/16 v4, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187588
    const/16 v4, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187589
    const/16 v4, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187590
    const/16 v4, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187591
    const/16 v4, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187592
    const/16 v4, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187593
    const/16 v4, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187594
    const/16 v4, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187595
    const/16 v4, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187596
    const/16 v4, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187597
    const/16 v4, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187598
    const/16 v5, 0x40

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v4, v6, :cond_0

    const/4 v4, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 187599
    const/16 v4, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187600
    const/16 v4, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187601
    const/16 v4, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187602
    const/16 v4, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187603
    const/16 v4, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187604
    const/16 v4, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187605
    const/16 v5, 0x47

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 187606
    const/16 v4, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187607
    const/16 v4, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187608
    const/16 v4, 0x4a

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, LX/186;->a(III)V

    .line 187609
    const/16 v4, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187610
    const/16 v4, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187611
    const/16 v4, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187612
    const/16 v4, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187613
    const/16 v4, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187614
    const/16 v4, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187615
    const/16 v4, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187616
    const/16 v4, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187617
    const/16 v4, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187618
    const/16 v4, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187619
    const/16 v4, 0x55

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187620
    const/16 v4, 0x56

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187621
    const/16 v4, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187622
    const/16 v4, 0x58

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187623
    const/16 v4, 0x59

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bd()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187624
    const/16 v4, 0x5a

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187625
    const/16 v4, 0x5b

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bf()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187626
    const/16 v4, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187627
    const/16 v4, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187628
    const/16 v4, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187629
    const/16 v4, 0x5f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bj()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 187630
    const/16 v4, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187631
    const/16 v5, 0x61

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bl()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    if-ne v4, v6, :cond_1

    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 187632
    const/16 v4, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 187633
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 187634
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    return v4

    .line 187635
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    goto/16 :goto_0

    .line 187636
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLStory;->bl()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v4

    goto :goto_1
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 186981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 186982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 186983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 186984
    if-eqz v1, :cond_0

    .line 186985
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 186986
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->f:Ljava/util/List;

    .line 186987
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->C()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 186988
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->C()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 186989
    if-eqz v1, :cond_1

    .line 186990
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 186991
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->g:Ljava/util/List;

    .line 186992
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 186993
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 186994
    if-eqz v1, :cond_2

    .line 186995
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 186996
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->h:Ljava/util/List;

    :cond_2
    move-object v1, v0

    .line 186997
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 186998
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 186999
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 187000
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187001
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->i:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 187002
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 187003
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 187004
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 187005
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187006
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->k:Lcom/facebook/graphql/model/GraphQLImage;

    .line 187007
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 187008
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLApplication;

    .line 187009
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 187010
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187011
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->l:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 187012
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->I()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 187013
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->I()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 187014
    if-eqz v2, :cond_6

    .line 187015
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187016
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->m:Ljava/util/List;

    move-object v1, v0

    .line 187017
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 187018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 187020
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187021
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->n:Lcom/facebook/graphql/model/GraphQLStory;

    .line 187022
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 187023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 187024
    if-eqz v2, :cond_8

    .line 187025
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187026
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->o:Ljava/util/List;

    move-object v1, v0

    .line 187027
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 187028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 187029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 187030
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187031
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->p:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 187032
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->T()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 187033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->T()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187034
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->T()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 187035
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187036
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 187037
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 187038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 187040
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187041
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->B:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187042
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 187043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 187044
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 187045
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187046
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->C:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 187047
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 187048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 187049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 187050
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187051
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->D:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 187052
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bk()Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 187053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bk()Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    .line 187054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bk()Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 187055
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187056
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aV:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    .line 187057
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 187058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 187059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 187060
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187061
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->E:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 187062
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 187063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 187064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 187065
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187066
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 187067
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 187068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 187069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 187070
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187071
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->G:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 187072
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aa()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 187073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aa()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 187074
    if-eqz v2, :cond_12

    .line 187075
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187076
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->H:Ljava/util/List;

    move-object v1, v0

    .line 187077
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ab()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 187078
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ab()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 187079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ab()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 187080
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187081
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 187082
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 187083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 187084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 187085
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187086
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 187087
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 187088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    .line 187089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 187090
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187091
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->N:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    .line 187092
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 187093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLIcon;

    .line 187094
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 187095
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187096
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->O:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 187097
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 187098
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 187099
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 187100
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187101
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->Q:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 187102
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 187103
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 187104
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 187105
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187106
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->R:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 187107
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 187108
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 187109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 187110
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187111
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->S:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 187112
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bm()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 187113
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bm()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 187114
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bm()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 187115
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187116
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aX:Lcom/facebook/graphql/model/FeedUnit;

    .line 187117
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 187118
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187119
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 187120
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187121
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187122
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 187123
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187124
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 187125
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187126
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187127
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bg()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 187128
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bg()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 187129
    if-eqz v2, :cond_1d

    .line 187130
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187131
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->aR:Ljava/util/List;

    move-object v1, v0

    .line 187132
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->av()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 187133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->av()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 187134
    if-eqz v2, :cond_1e

    .line 187135
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187136
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->ae:Ljava/util/List;

    move-object v1, v0

    .line 187137
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 187138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 187139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 187140
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187141
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->af:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 187142
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aw()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 187143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aw()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    .line 187144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aw()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 187145
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187146
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->ag:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 187147
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ax()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 187148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ax()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 187149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ax()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 187150
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187151
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->ah:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 187152
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 187153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 187154
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 187155
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187156
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->ai:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 187157
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 187158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 187159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 187160
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187161
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 187162
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 187163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 187164
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 187165
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187166
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->ak:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 187167
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 187168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    .line 187169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 187170
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187171
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->al:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    .line 187172
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 187173
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 187174
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 187175
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187176
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aS:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 187177
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 187178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSticker;

    .line 187179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 187180
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187181
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->am:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 187182
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 187183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 187184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 187185
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187186
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 187187
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aE()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 187188
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aE()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 187189
    if-eqz v2, :cond_29

    .line 187190
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187191
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->ao:Ljava/util/List;

    move-object v1, v0

    .line 187192
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 187193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 187194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 187195
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187196
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aq:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 187197
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 187198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 187200
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187201
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187202
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 187203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 187204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 187205
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187206
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->at:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 187207
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aK()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 187208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aK()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 187209
    if-eqz v2, :cond_2d

    .line 187210
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187211
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLStory;->au:Ljava/util/List;

    move-object v1, v0

    .line 187212
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 187213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 187214
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 187215
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187216
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->av:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 187217
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 187218
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 187220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187221
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187222
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 187223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187224
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_30

    .line 187225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187226
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187227
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 187228
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187229
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_31

    .line 187230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187231
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aC:Lcom/facebook/graphql/model/GraphQLStory;

    .line 187232
    :cond_31
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 187233
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 187234
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v2

    if-eq v2, v0, :cond_32

    .line 187235
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187236
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 187237
    :cond_32
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 187238
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187239
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_33

    .line 187240
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187241
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187242
    :cond_33
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 187243
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187244
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_34

    .line 187245
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187246
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187247
    :cond_34
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 187248
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_35

    .line 187250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187251
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187252
    :cond_35
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 187253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 187254
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eq v2, v0, :cond_36

    .line 187255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187256
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aG:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 187257
    :cond_36
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 187258
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    .line 187259
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v2

    if-eq v2, v0, :cond_37

    .line 187260
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187261
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aH:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    .line 187262
    :cond_37
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 187263
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 187264
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v2

    if-eq v2, v0, :cond_38

    .line 187265
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187266
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 187267
    :cond_38
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aZ()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 187268
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aZ()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 187269
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aZ()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v2

    if-eq v2, v0, :cond_39

    .line 187270
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187271
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aK:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 187272
    :cond_39
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bb()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 187273
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bb()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 187274
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bb()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_3a

    .line 187275
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187276
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aM:Lcom/facebook/graphql/model/GraphQLActor;

    .line 187277
    :cond_3a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 187278
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 187279
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3b

    .line 187280
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 187281
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLStory;->aP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 187282
    :cond_3b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 187283
    if-nez v1, :cond_3c

    :goto_0
    return-object p0

    :cond_3c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 187284
    new-instance v0, LX/4Yr;

    invoke-direct {v0, p1}, LX/4Yr;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 187286
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->I:J

    .line 187287
    return-void
.end method

.method public final a(LX/0x2;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 187288
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLStory;->aY:LX/0x2;

    .line 187289
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 187290
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 187291
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->r:Z

    .line 187292
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->s:Z

    .line 187293
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->t:Z

    .line 187294
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->u:Z

    .line 187295
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->v:Z

    .line 187296
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->w:Z

    .line 187297
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->z:J

    .line 187298
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->I:J

    .line 187299
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->L:Z

    .line 187300
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->T:Z

    .line 187301
    const/16 v0, 0x2b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->V:Z

    .line 187302
    const/16 v0, 0x2c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->W:Z

    .line 187303
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->X:Z

    .line 187304
    const/16 v0, 0x2e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Y:Z

    .line 187305
    const/16 v0, 0x31

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ab:I

    .line 187306
    const/16 v0, 0x47

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aw:J

    .line 187307
    const/16 v0, 0x4a

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->az:I

    .line 187308
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aO:Z

    .line 187309
    const/16 v0, 0x5b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aQ:Z

    .line 187310
    const/16 v0, 0x5f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aU:Z

    .line 187311
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 187312
    const-string v0, "local_edit_pending"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187313
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bj()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187315
    const/16 v0, 0x5f

    iput v0, p2, LX/18L;->c:I

    .line 187316
    :goto_0
    return-void

    .line 187317
    :cond_0
    const-string v0, "local_group_did_approve"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187318
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->am()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187319
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187320
    const/16 v0, 0x2b

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 187321
    :cond_1
    const-string v0, "local_group_did_ignore_report"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187322
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->an()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187323
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187324
    const/16 v0, 0x2c

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 187325
    :cond_2
    const-string v0, "local_group_did_pin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187326
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ao()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187327
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187328
    const/16 v0, 0x2d

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 187329
    :cond_3
    const-string v0, "local_group_did_unpin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 187330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ap()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187331
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187332
    const/16 v0, 0x2e

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 187333
    :cond_4
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 187334
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aq()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187335
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187336
    const/16 v0, 0x2f

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 187337
    :cond_5
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 187338
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ar()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187339
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187340
    const/16 v0, 0x30

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 187341
    :cond_6
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 187342
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->as()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187343
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187344
    const/16 v0, 0x31

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 187345
    :cond_7
    const-string v0, "privacy_scope.type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 187346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    .line 187347
    if-eqz v0, :cond_a

    .line 187348
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187349
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187350
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 187351
    :cond_8
    const-string v0, "save_info.viewer_save_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 187352
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 187353
    if-eqz v0, :cond_a

    .line 187354
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187355
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187356
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 187357
    :cond_9
    const-string v0, "seen_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 187358
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 187359
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 187360
    const/16 v0, 0x40

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 187361
    :cond_a
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 187400
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187401
    check-cast p2, LX/0Px;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0Px;)V

    .line 187402
    :cond_0
    :goto_0
    return-void

    .line 187403
    :cond_1
    const-string v0, "title"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187404
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 187405
    :cond_2
    const-string v0, "titleFromRenderLocation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187406
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 187407
    :cond_3
    const-string v0, "local_follow_up_feed_unit"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 187408
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0

    .line 187409
    :cond_4
    const-string v0, "privacy_scope"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 187410
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)V

    goto :goto_0

    .line 187411
    :cond_5
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 187412
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 187413
    :cond_6
    const-string v0, "text_format_metadata"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187414
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 187365
    const-string v0, "local_edit_pending"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187366
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStory;->e(Z)V

    .line 187367
    :cond_0
    :goto_0
    return-void

    .line 187368
    :cond_1
    const-string v0, "local_group_did_approve"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187369
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStory;->a(Z)V

    goto :goto_0

    .line 187370
    :cond_2
    const-string v0, "local_group_did_ignore_report"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187371
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStory;->b(Z)V

    goto :goto_0

    .line 187372
    :cond_3
    const-string v0, "local_group_did_pin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 187373
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStory;->c(Z)V

    goto :goto_0

    .line 187374
    :cond_4
    const-string v0, "local_group_did_unpin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 187375
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStory;->d(Z)V

    goto :goto_0

    .line 187376
    :cond_5
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 187377
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 187378
    :cond_6
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 187379
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 187380
    :cond_7
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 187381
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLStory;->a(I)V

    goto :goto_0

    .line 187382
    :cond_8
    const-string v0, "privacy_scope.type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 187383
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    .line 187384
    if-eqz v0, :cond_0

    .line 187385
    if-eqz p3, :cond_9

    .line 187386
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 187387
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->a(Ljava/lang/String;)V

    .line 187388
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    goto/16 :goto_0

    .line 187389
    :cond_9
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 187390
    :cond_a
    const-string v0, "save_info.viewer_save_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 187391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 187392
    if-eqz v0, :cond_0

    .line 187393
    if-eqz p3, :cond_b

    .line 187394
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 187395
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 187396
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    goto/16 :goto_0

    .line 187397
    :cond_b
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto/16 :goto_0

    .line 187398
    :cond_c
    const-string v0, "seen_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187399
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLStory;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    goto/16 :goto_0
.end method

.method public final aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186978
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ak:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186979
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ak:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ak:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 186980
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ak:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    return-object v0
.end method

.method public final aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187415
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->al:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187416
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->al:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->al:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    .line 187417
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->al:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    return-object v0
.end method

.method public final aC()Lcom/facebook/graphql/model/GraphQLSticker;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187418
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->am:Lcom/facebook/graphql/model/GraphQLSticker;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187419
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->am:Lcom/facebook/graphql/model/GraphQLSticker;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/graphql/model/GraphQLSticker;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSticker;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->am:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 187420
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->am:Lcom/facebook/graphql/model/GraphQLSticker;

    return-object v0
.end method

.method public final aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187421
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187422
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 187423
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->an:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    return-object v0
.end method

.method public final aE()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187424
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ao:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187425
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ao:Ljava/util/List;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ao:Ljava/util/List;

    .line 187426
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ao:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 187427
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ap:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187428
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ap:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/16 v1, 0x40

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ap:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 187429
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ap:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method public final aG()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187430
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aq:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187431
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aq:Lcom/facebook/graphql/model/GraphQLEntity;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aq:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 187432
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aq:Lcom/facebook/graphql/model/GraphQLEntity;

    return-object v0
.end method

.method public final aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187433
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187434
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 187435
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ar:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aI()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187436
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->as:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187437
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->as:Ljava/lang/String;

    const/16 v1, 0x43

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->as:Ljava/lang/String;

    .line 187438
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->as:Ljava/lang/String;

    return-object v0
.end method

.method public final aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187439
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->at:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 187440
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->at:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    const/16 v1, 0x44

    const-class v2, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->at:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 187441
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->at:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    return-object v0
.end method

.method public final aK()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186522
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->au:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186523
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->au:Ljava/util/List;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->au:Ljava/util/List;

    .line 186524
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->au:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186792
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->av:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186793
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->av:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeader;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->av:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 186794
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->av:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    return-object v0
.end method

.method public final aM()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186528
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186529
    const/16 v0, 0x8

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186530
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aw:J

    return-wide v0
.end method

.method public final aN()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186531
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ax:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186532
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ax:Ljava/util/List;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ax:Ljava/util/List;

    .line 186533
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ax:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aO()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186534
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ay:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186535
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ay:Ljava/util/List;

    const/16 v1, 0x49

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ay:Ljava/util/List;

    .line 186536
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ay:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aP()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186537
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186538
    const/16 v0, 0x9

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186539
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->az:I

    return v0
.end method

.method public final aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186540
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186541
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186542
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186543
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186544
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x4c

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186545
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aS()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186546
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aC:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186547
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aC:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aC:Lcom/facebook/graphql/model/GraphQLStory;

    .line 186548
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aC:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186549
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186550
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186551
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186552
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186553
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186554
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186525
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186526
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186527
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final aW()Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186558
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aG:Lcom/facebook/graphql/model/GraphQLProfile;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186559
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aG:Lcom/facebook/graphql/model/GraphQLProfile;

    const/16 v1, 0x51

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aG:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 186560
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aG:Lcom/facebook/graphql/model/GraphQLProfile;

    return-object v0
.end method

.method public final aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186561
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aH:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186562
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aH:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    const/16 v1, 0x52

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aH:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    .line 186563
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aH:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    return-object v0
.end method

.method public final aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186564
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186565
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 186566
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    return-object v0
.end method

.method public final aZ()Lcom/facebook/graphql/model/GraphQLTranslation;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186567
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aK:Lcom/facebook/graphql/model/GraphQLTranslation;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186568
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aK:Lcom/facebook/graphql/model/GraphQLTranslation;

    const/16 v1, 0x55

    const-class v2, Lcom/facebook/graphql/model/GraphQLTranslation;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTranslation;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aK:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 186569
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aK:Lcom/facebook/graphql/model/GraphQLTranslation;

    return-object v0
.end method

.method public final aa()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186570
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->H:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186571
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->H:Ljava/util/List;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->H:Ljava/util/List;

    .line 186572
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->H:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final ab()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186573
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186574
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 186575
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->J:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    return-object v0
.end method

.method public final ac()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186576
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186577
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 186578
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final ad()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186579
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186580
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186581
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->L:Z

    return v0
.end method

.method public final ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186555
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->N:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186556
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->N:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->N:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    .line 186557
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->N:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    return-object v0
.end method

.method public final af()Lcom/facebook/graphql/model/GraphQLIcon;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186468
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->O:Lcom/facebook/graphql/model/GraphQLIcon;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186469
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->O:Lcom/facebook/graphql/model/GraphQLIcon;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLIcon;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLIcon;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->O:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 186470
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->O:Lcom/facebook/graphql/model/GraphQLIcon;

    return-object v0
.end method

.method public final ag()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186474
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->P:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186475
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->P:Ljava/lang/String;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->P:Ljava/lang/String;

    .line 186476
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final ah()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186477
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Q:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186478
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Q:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Q:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 186479
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Q:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186480
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->R:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186481
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->R:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->R:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 186482
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->R:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    return-object v0
.end method

.method public final aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186483
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->S:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186484
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->S:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryInsights;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryInsights;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->S:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 186485
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->S:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    return-object v0
.end method

.method public final ak()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186486
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186487
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186488
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->T:Z

    return v0
.end method

.method public final al()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186489
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186490
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->U:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->U:Ljava/lang/String;

    .line 186491
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final am()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186492
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186493
    const/4 v0, 0x5

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186494
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->V:Z

    return v0
.end method

.method public final an()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186495
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186496
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186497
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->W:Z

    return v0
.end method

.method public final ao()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 186498
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186499
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186500
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->X:Z

    return v0
.end method

.method public final ap()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186501
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186502
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186503
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Y:Z

    return v0
.end method

.method public final aq()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186504
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Z:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186505
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Z:Ljava/lang/String;

    const/16 v1, 0x2f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Z:Ljava/lang/String;

    .line 186506
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->Z:Ljava/lang/String;

    return-object v0
.end method

.method public final ar()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186507
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aa:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186508
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aa:Ljava/lang/String;

    const/16 v1, 0x30

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aa:Ljava/lang/String;

    .line 186509
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final as()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186510
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186511
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186512
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ab:I

    return v0
.end method

.method public final at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186513
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186514
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186515
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ac:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186516
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186517
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 186518
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final av()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186519
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ae:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186520
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ae:Ljava/util/List;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ae:Ljava/util/List;

    .line 186521
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ae:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final aw()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186471
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ag:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186472
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ag:Lcom/facebook/graphql/model/GraphQLPlace;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlace;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ag:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 186473
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ag:Lcom/facebook/graphql/model/GraphQLPlace;

    return-object v0
.end method

.method public final ax()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186691
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ah:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186692
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ah:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ah:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 186693
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ah:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    return-object v0
.end method

.method public final ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186666
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ai:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186667
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ai:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ai:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 186668
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->ai:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    return-object v0
.end method

.method public final az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186669
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186670
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    const/16 v1, 0x39

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 186671
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aj:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    return-object v0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186672
    invoke-static {}, LX/1fz;->d()LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final ba()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186673
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aL:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186674
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aL:Ljava/lang/String;

    const/16 v1, 0x56

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aL:Ljava/lang/String;

    .line 186675
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aL:Ljava/lang/String;

    return-object v0
.end method

.method public final bb()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186676
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aM:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186677
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aM:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x57

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aM:Lcom/facebook/graphql/model/GraphQLActor;

    .line 186678
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aM:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final bc()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186679
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aN:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186680
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aN:Ljava/util/List;

    const/16 v1, 0x58

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aN:Ljava/util/List;

    .line 186681
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aN:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bd()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186682
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186683
    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186684
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aO:Z

    return v0
.end method

.method public final be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186685
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186686
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    const/16 v1, 0x5a

    const-class v2, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 186687
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    return-object v0
.end method

.method public final bf()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186688
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186689
    const/16 v0, 0xb

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186690
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aQ:Z

    return v0
.end method

.method public final bg()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186663
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aR:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186664
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aR:Ljava/util/List;

    const/16 v1, 0x5c

    const-class v2, Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aR:Ljava/util/List;

    .line 186665
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aR:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186694
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aS:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186695
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aS:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    const/16 v1, 0x5d

    const-class v2, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aS:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 186696
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aS:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    return-object v0
.end method

.method public final bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186697
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186698
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 186699
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aT:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    return-object v0
.end method

.method public final bj()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186700
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 186701
    const/16 v0, 0xb

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 186702
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aU:Z

    return v0
.end method

.method public final bk()Lcom/facebook/graphql/model/GraphQLFeedBackendData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186703
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aV:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186704
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aV:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aV:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    .line 186705
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aV:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    return-object v0
.end method

.method public final bl()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 186706
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aW:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186707
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aW:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aW:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 186708
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aW:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    return-object v0
.end method

.method public final bm()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186709
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aX:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186710
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aX:Lcom/facebook/graphql/model/FeedUnit;

    const/16 v1, 0x62

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aX:Lcom/facebook/graphql/model/FeedUnit;

    .line 186711
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aX:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186712
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aI:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186713
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aI:Ljava/lang/String;

    const/16 v1, 0x53

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aI:Ljava/lang/String;

    .line 186714
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->aI:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186715
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 186716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 186717
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 186718
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 186719
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 186720
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186597
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186598
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 186599
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->F:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 186583
    const v0, 0x4c808d5

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186584
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186585
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->q:Ljava/lang/String;

    .line 186586
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 186587
    invoke-static {p0}, LX/2v3;->d(LX/16n;)I

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 186588
    invoke-static {p0}, LX/2v3;->e(LX/16n;)I

    move-result v0

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186589
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 186590
    invoke-static {p0}, LX/2dv;->a(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v0

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 186592
    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1

    .prologue
    .line 186593
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 186594
    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 186595
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 186596
    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 186582
    invoke-static {p0}, LX/1w8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 186600
    invoke-static {p0}, LX/1w8;->a(LX/16e;)Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186601
    invoke-static {p0}, LX/18M;->a(LX/16o;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 186602
    const/4 v3, 0x0

    const/16 v8, 0x2c

    .line 186603
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 186604
    const/16 v1, 0x5b

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 186605
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186606
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 186607
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 186608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186609
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 186610
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 186611
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186612
    const-string v1, ", ProfilePics: ["

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186613
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_2

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 186614
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 186615
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186616
    const-string v1, "|"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186617
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 186618
    :cond_2
    const-string v1, "]"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186619
    :cond_3
    invoke-static {p0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 186620
    if-eqz v1, :cond_9

    .line 186621
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186622
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 186623
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 186624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186625
    :goto_2
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 186626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v1

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 186627
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 186628
    const-string v1, ", attachment 0:"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186629
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186630
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 186631
    const-string v1, ", shareable: { id: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 186633
    const-string v1, ", __typename: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186634
    :cond_5
    const-string v1, "}"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186635
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 186636
    const-string v1, ", legacyApiStoryId: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186637
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->C_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 186638
    const-string v1, ", hideableToken: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->C_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186639
    :cond_8
    const/16 v1, 0x5d

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 186640
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 186641
    return-object v0

    .line 186642
    :cond_9
    const-string v1, "NULL"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186643
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 186644
    :cond_a
    const-string v1, "NULL"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2
.end method

.method public final u()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    .line 186646
    if-eqz v0, :cond_0

    .line 186647
    :goto_0
    move-object v0, v0

    .line 186648
    return-object v0

    .line 186649
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 186650
    goto :goto_0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 186651
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 186652
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 186653
    invoke-interface {p0}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 186654
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 186655
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186656
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->af:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 186657
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->af:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->af:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 186658
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLStory;->af:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 186659
    invoke-interface {p0}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 186660
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 186661
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 186662
    invoke-static {p0}, LX/2v3;->c(LX/16n;)Z

    move-result v0

    return v0
.end method
