.class public final Lcom/facebook/graphql/model/GraphQLFeedback;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jR;
.implements LX/0jS;
.implements LX/1VU;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedback$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLFeedback$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Z

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedbackReaction;",
            ">;"
        }
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:I

.field public W:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Z

.field public aa:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:I

.field public ad:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ae:Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

.field private af:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public t:Z

.field public u:J

.field public v:Lcom/facebook/graphql/model/GraphQLCommentersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 261919
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 261918
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 261915
    const/16 v0, 0x37

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 261916
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->af:LX/0x2;

    .line 261917
    return-void
.end method

.method public constructor <init>(LX/3dM;)V
    .locals 2

    .prologue
    .line 261857
    const/16 v0, 0x37

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 261858
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->af:LX/0x2;

    .line 261859
    iget-object v0, p1, LX/3dM;->b:LX/15i;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->c:LX/15i;

    .line 261860
    iget v0, p1, LX/3dM;->c:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->d:I

    .line 261861
    iget-boolean v0, p1, LX/3dM;->d:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Z:Z

    .line 261862
    iget-boolean v0, p1, LX/3dM;->e:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->e:Z

    .line 261863
    iget-boolean v0, p1, LX/3dM;->f:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->f:Z

    .line 261864
    iget-boolean v0, p1, LX/3dM;->g:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->g:Z

    .line 261865
    iget-boolean v0, p1, LX/3dM;->h:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->h:Z

    .line 261866
    iget-boolean v0, p1, LX/3dM;->i:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->i:Z

    .line 261867
    iget-boolean v0, p1, LX/3dM;->j:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->j:Z

    .line 261868
    iget-boolean v0, p1, LX/3dM;->k:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->k:Z

    .line 261869
    iget-boolean v0, p1, LX/3dM;->l:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->l:Z

    .line 261870
    iget-object v0, p1, LX/3dM;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261871
    iget-object v0, p1, LX/3dM;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 261872
    iget-object v0, p1, LX/3dM;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261873
    iget-object v0, p1, LX/3dM;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->p:Ljava/lang/String;

    .line 261874
    iget v0, p1, LX/3dM;->q:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ac:I

    .line 261875
    iget-object v0, p1, LX/3dM;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->q:Ljava/lang/String;

    .line 261876
    iget-boolean v0, p1, LX/3dM;->s:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->r:Z

    .line 261877
    iget-boolean v0, p1, LX/3dM;->t:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->s:Z

    .line 261878
    iget-boolean v0, p1, LX/3dM;->u:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->t:Z

    .line 261879
    iget-wide v0, p1, LX/3dM;->v:J

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->u:J

    .line 261880
    iget-object v0, p1, LX/3dM;->w:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->v:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261881
    iget-boolean v0, p1, LX/3dM;->x:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->w:Z

    .line 261882
    iget-object v0, p1, LX/3dM;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->x:Ljava/lang/String;

    .line 261883
    iget-object v0, p1, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->y:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 261884
    iget-object v0, p1, LX/3dM;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->z:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261885
    iget-object v0, p1, LX/3dM;->B:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261886
    iget-boolean v0, p1, LX/3dM;->C:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->B:Z

    .line 261887
    iget-object v0, p1, LX/3dM;->D:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->C:Ljava/lang/String;

    .line 261888
    iget-object v0, p1, LX/3dM;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261889
    iget-object v0, p1, LX/3dM;->F:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 261890
    iget-object v0, p1, LX/3dM;->G:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ad:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 261891
    iget-object v0, p1, LX/3dM;->H:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 261892
    iget-object v0, p1, LX/3dM;->I:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->G:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 261893
    iget-object v0, p1, LX/3dM;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->H:Ljava/lang/String;

    .line 261894
    iget-object v0, p1, LX/3dM;->K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 261895
    iget-object v0, p1, LX/3dM;->L:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 261896
    iget-boolean v0, p1, LX/3dM;->M:Z

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->K:Z

    .line 261897
    iget-object v0, p1, LX/3dM;->N:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->L:Ljava/util/List;

    .line 261898
    iget-object v0, p1, LX/3dM;->O:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 261899
    iget-object v0, p1, LX/3dM;->P:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 261900
    iget-object v0, p1, LX/3dM;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->O:Ljava/lang/String;

    .line 261901
    iget-object v0, p1, LX/3dM;->R:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->P:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    .line 261902
    iget-object v0, p1, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Q:Lcom/facebook/graphql/model/GraphQLPage;

    .line 261903
    iget-object v0, p1, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->R:Lcom/facebook/graphql/model/GraphQLUser;

    .line 261904
    iget-object v0, p1, LX/3dM;->U:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->aa:Lcom/facebook/graphql/model/GraphQLUser;

    .line 261905
    iget-object v0, p1, LX/3dM;->V:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->S:Ljava/lang/String;

    .line 261906
    iget-object v0, p1, LX/3dM;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261907
    iget-object v0, p1, LX/3dM;->X:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 261908
    iget v0, p1, LX/3dM;->Y:I

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->V:I

    .line 261909
    iget-object v0, p1, LX/3dM;->Z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->W:Ljava/lang/String;

    .line 261910
    iget-object v0, p1, LX/3dM;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->X:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261911
    iget-object v0, p1, LX/3dM;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    .line 261912
    iget-object v0, p1, LX/3dM;->ac:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Y:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    .line 261913
    iget-object v0, p1, LX/3dM;->ad:LX/0x2;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->af:LX/0x2;

    .line 261914
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 261851
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->V:I

    .line 261852
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261853
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261854
    if-eqz v0, :cond_0

    .line 261855
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 261856
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedbackReaction;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 261845
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 261846
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261847
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261848
    if-eqz v0, :cond_0

    .line 261849
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 261850
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 261839
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 261840
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261841
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261842
    if-eqz v0, :cond_0

    .line 261843
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 261844
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 261833
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 261834
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261835
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261836
    if-eqz v0, :cond_0

    .line 261837
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 261838
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 261827
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 261828
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261829
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261830
    if-eqz v0, :cond_0

    .line 261831
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 261832
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 261773
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 261774
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261775
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261776
    if-eqz v0, :cond_0

    .line 261777
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x25

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 261778
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 261818
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->f:Z

    .line 261819
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261820
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261821
    if-eqz v0, :cond_0

    .line 261822
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 261823
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 261812
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->j:Z

    .line 261813
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261814
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261815
    if-eqz v0, :cond_0

    .line 261816
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 261817
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 261806
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->s:Z

    .line 261807
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261808
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261809
    if-eqz v0, :cond_0

    .line 261810
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 261811
    :cond_0
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 261800
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->w:Z

    .line 261801
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261802
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261803
    if-eqz v0, :cond_0

    .line 261804
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 261805
    :cond_0
    return-void
.end method

.method private f(Z)V
    .locals 3

    .prologue
    .line 261794
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->B:Z

    .line 261795
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261796
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261797
    if-eqz v0, :cond_0

    .line 261798
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 261799
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261791
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->y:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261792
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->y:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->y:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 261793
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->y:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261788
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->z:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261789
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->z:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->z:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261790
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->z:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    return-object v0
.end method

.method public final C()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261785
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261786
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261787
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    return-object v0
.end method

.method public final D()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261782
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261783
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261784
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->B:Z

    return v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261779
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261780
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261781
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261977
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261978
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 261979
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261920
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261921
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 261922
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261974
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->G:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261975
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->G:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->G:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 261976
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->G:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261971
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261972
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->H:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->H:Ljava/lang/String;

    .line 261973
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261968
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261969
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 261970
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261965
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261966
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 261967
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    return-object v0
.end method

.method public final L()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 261962
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261963
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261964
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->K:Z

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 261959
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->af:LX/0x2;

    if-nez v0, :cond_0

    .line 261960
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->af:LX/0x2;

    .line 261961
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->af:LX/0x2;

    return-object v0
.end method

.method public final M()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedbackReaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261956
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->L:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261957
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->L:Ljava/util/List;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->L:Ljava/util/List;

    .line 261958
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->L:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261953
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261954
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 261955
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261950
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261951
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 261952
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261947
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261948
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->O:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->O:Ljava/lang/String;

    .line 261949
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final Q()Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261944
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->P:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261945
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->P:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->P:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    .line 261946
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->P:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261941
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Q:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261942
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Q:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Q:Lcom/facebook/graphql/model/GraphQLPage;

    .line 261943
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Q:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final S()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261938
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->R:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261939
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->R:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->R:Lcom/facebook/graphql/model/GraphQLUser;

    .line 261940
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->R:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final T()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261935
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->S:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261936
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->S:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->S:Ljava/lang/String;

    .line 261937
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->S:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261932
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261933
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261934
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final V()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261929
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261930
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 261931
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    return-object v0
.end method

.method public final W()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 261714
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261715
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261716
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->V:I

    return v0
.end method

.method public final X()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261926
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261927
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->W:Ljava/lang/String;

    const/16 v1, 0x2e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->W:Ljava/lang/String;

    .line 261928
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261824
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->X:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261825
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->X:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->X:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261826
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->X:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261923
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Y:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261924
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Y:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Y:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    .line 261925
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Y:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 38

    .prologue
    .line 261317
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 261318
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->q()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 261319
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 261320
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 261321
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->t()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 261322
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 261323
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 261324
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 261325
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 261326
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->B()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 261327
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->C()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 261328
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 261329
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 261330
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 261331
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 261332
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 261333
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->I()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 261334
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 261335
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 261336
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v21

    .line 261337
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 261338
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 261339
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->P()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 261340
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Q()Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 261341
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 261342
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 261343
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->T()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 261344
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->U()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 261345
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->V()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 261346
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->X()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 261347
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 261348
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Z()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 261349
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ab()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 261350
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 261351
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 261352
    const/16 v7, 0x36

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 261353
    const/4 v7, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261354
    const/4 v7, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261355
    const/4 v7, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->d()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261356
    const/4 v7, 0x4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->n()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261357
    const/4 v7, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->e()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261358
    const/4 v7, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261359
    const/4 v7, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261360
    const/16 v7, 0x8

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->p()Z

    move-result v37

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 261361
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 261362
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 261363
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 261364
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 261365
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 261366
    const/16 v2, 0xe

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->v()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261367
    const/16 v2, 0xf

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261368
    const/16 v2, 0x10

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261369
    const/16 v3, 0x11

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->x()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 261370
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 261371
    const/16 v2, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->z()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261372
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 261373
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 261374
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 261375
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 261376
    const/16 v2, 0x18

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->D()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261377
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 261378
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 261379
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 261380
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261381
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261382
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261383
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261384
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261385
    const/16 v2, 0x22

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->L()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261386
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261387
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261388
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261389
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261390
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261391
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261392
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261393
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261394
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261395
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261396
    const/16 v2, 0x2d

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 261397
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261398
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261399
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261400
    const/16 v2, 0x31

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->aa()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 261401
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261402
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261403
    const/16 v2, 0x34

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ad()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 261404
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 261405
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 261406
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 261581
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 261582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->q()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 261583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->q()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->q()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 261585
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261586
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261587
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 261588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 261589
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 261590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261591
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 261592
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 261593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 261595
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261596
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261597
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 261598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261599
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 261600
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261601
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->v:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261602
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 261603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 261604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 261605
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261606
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->y:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 261607
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->B()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 261608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->B()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261609
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->B()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 261610
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261611
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->z:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261612
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->C()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 261613
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->C()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->C()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 261615
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261616
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 261617
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 261618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 261620
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261621
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261622
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 261623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 261624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 261625
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261626
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 261627
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 261628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 261629
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 261630
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261631
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->ad:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 261632
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 261633
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 261634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 261635
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261636
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 261637
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 261638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 261639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 261640
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261641
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->G:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 261642
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 261643
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 261644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 261645
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261646
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 261647
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 261648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 261649
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 261650
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261651
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 261652
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 261653
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 261654
    if-eqz v2, :cond_e

    .line 261655
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261656
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLFeedback;->L:Ljava/util/List;

    move-object v1, v0

    .line 261657
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 261658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 261659
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 261660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261661
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 261662
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 261663
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 261664
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 261665
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261666
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->N:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 261667
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Q()Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 261668
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Q()Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    .line 261669
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Q()Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 261670
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261671
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->P:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    .line 261672
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 261673
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 261674
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 261675
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261676
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->Q:Lcom/facebook/graphql/model/GraphQLPage;

    .line 261677
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 261678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 261679
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 261680
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261681
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->R:Lcom/facebook/graphql/model/GraphQLUser;

    .line 261682
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ab()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 261683
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ab()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 261684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ab()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 261685
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261686
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->aa:Lcom/facebook/graphql/model/GraphQLUser;

    .line 261687
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->U()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 261688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->U()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261689
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->U()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 261690
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261691
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->T:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261692
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->V()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 261693
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->V()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 261694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->V()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 261695
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261696
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->U:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 261697
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 261698
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261699
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 261700
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261701
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->X:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261702
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 261703
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    .line 261704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 261705
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261706
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    .line 261707
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Z()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 261708
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Z()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    .line 261709
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Z()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 261710
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 261711
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLFeedback;->Y:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    .line 261712
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 261713
    if-nez v1, :cond_1a

    :goto_0
    return-object p0

    :cond_1a
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 261580
    new-instance v0, LX/4WJ;

    invoke-direct {v0, p1}, LX/4WJ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261579
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 261577
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->u:J

    .line 261578
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 261557
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 261558
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->e:Z

    .line 261559
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->f:Z

    .line 261560
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->g:Z

    .line 261561
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->h:Z

    .line 261562
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->i:Z

    .line 261563
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->j:Z

    .line 261564
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->k:Z

    .line 261565
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->l:Z

    .line 261566
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->r:Z

    .line 261567
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->s:Z

    .line 261568
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->t:Z

    .line 261569
    const/16 v0, 0x11

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->u:J

    .line 261570
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->w:Z

    .line 261571
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->B:Z

    .line 261572
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->K:Z

    .line 261573
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->V:I

    .line 261574
    const/16 v0, 0x31

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Z:Z

    .line 261575
    const/16 v0, 0x34

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ac:I

    .line 261576
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 261489
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261491
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261492
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 261493
    :goto_0
    return-void

    .line 261494
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261497
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 261498
    :cond_1
    const-string v0, "comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    .line 261500
    if-eqz v0, :cond_c

    .line 261501
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261502
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261503
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 261504
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261506
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261507
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 261508
    :cond_3
    const-string v0, "have_comments_been_disabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 261509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->z()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261510
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261511
    const/16 v0, 0x13

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 261512
    :cond_4
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 261513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->D()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261514
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261515
    const/16 v0, 0x18

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261516
    :cond_5
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 261517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    .line 261518
    if-eqz v0, :cond_c

    .line 261519
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261520
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261521
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261522
    :cond_6
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 261523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    .line 261524
    if-eqz v0, :cond_c

    .line 261525
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261526
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261527
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261528
    :cond_7
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 261529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    .line 261530
    if-eqz v0, :cond_c

    .line 261531
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261532
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261533
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261534
    :cond_8
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 261535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    .line 261536
    if-eqz v0, :cond_c

    .line 261537
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261538
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261539
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261540
    :cond_9
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 261541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    .line 261542
    if-eqz v0, :cond_c

    .line 261543
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261544
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261545
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261546
    :cond_a
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 261547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    .line 261548
    if-eqz v0, :cond_c

    .line 261549
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261550
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261551
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261552
    :cond_b
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 261553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 261554
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 261555
    const/16 v0, 0x2d

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 261556
    :cond_c
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 261478
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261479
    check-cast p2, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)V

    .line 261480
    :cond_0
    :goto_0
    return-void

    .line 261481
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261482
    check-cast p2, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)V

    goto :goto_0

    .line 261483
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261484
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)V

    goto :goto_0

    .line 261485
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 261486
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)V

    goto :goto_0

    .line 261487
    :cond_4
    const-string v0, "viewer_feedback_reaction"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261488
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(Lcom/facebook/graphql/model/GraphQLFeedbackReaction;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 261409
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261410
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->b(Z)V

    .line 261411
    :cond_0
    :goto_0
    return-void

    .line 261412
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261413
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c(Z)V

    goto :goto_0

    .line 261414
    :cond_2
    const-string v0, "comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 261415
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    .line 261416
    if-eqz v0, :cond_0

    .line 261417
    if-eqz p3, :cond_3

    .line 261418
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 261419
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->a(I)V

    .line 261420
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    goto :goto_0

    .line 261421
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->a(I)V

    goto :goto_0

    .line 261422
    :cond_4
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 261423
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->d(Z)V

    goto :goto_0

    .line 261424
    :cond_5
    const-string v0, "have_comments_been_disabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 261425
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->e(Z)V

    goto :goto_0

    .line 261426
    :cond_6
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 261427
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->f(Z)V

    goto :goto_0

    .line 261428
    :cond_7
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 261429
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    .line 261430
    if-eqz v0, :cond_0

    .line 261431
    if-eqz p3, :cond_8

    .line 261432
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 261433
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a(I)V

    .line 261434
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->E:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    goto/16 :goto_0

    .line 261435
    :cond_8
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a(I)V

    goto/16 :goto_0

    .line 261436
    :cond_9
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 261437
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    .line 261438
    if-eqz v0, :cond_0

    .line 261439
    if-eqz p3, :cond_a

    .line 261440
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 261441
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a(I)V

    .line 261442
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->F:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    goto/16 :goto_0

    .line 261443
    :cond_a
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a(I)V

    goto/16 :goto_0

    .line 261444
    :cond_b
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 261445
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    .line 261446
    if-eqz v0, :cond_0

    .line 261447
    if-eqz p3, :cond_c

    .line 261448
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 261449
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;->a(I)V

    .line 261450
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->I:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    goto/16 :goto_0

    .line 261451
    :cond_c
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;->a(I)V

    goto/16 :goto_0

    .line 261452
    :cond_d
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 261453
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    .line 261454
    if-eqz v0, :cond_0

    .line 261455
    if-eqz p3, :cond_e

    .line 261456
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 261457
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->a(I)V

    .line 261458
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->J:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    goto/16 :goto_0

    .line 261459
    :cond_e
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->a(I)V

    goto/16 :goto_0

    .line 261460
    :cond_f
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 261461
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    .line 261462
    if-eqz v0, :cond_0

    .line 261463
    if-eqz p3, :cond_10

    .line 261464
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 261465
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a(I)V

    .line 261466
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    goto/16 :goto_0

    .line 261467
    :cond_10
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a(I)V

    goto/16 :goto_0

    .line 261468
    :cond_11
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 261469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    .line 261470
    if-eqz v0, :cond_0

    .line 261471
    if-eqz p3, :cond_12

    .line 261472
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 261473
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b(I)V

    .line 261474
    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->M:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    goto/16 :goto_0

    .line 261475
    :cond_12
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b(I)V

    goto/16 :goto_0

    .line 261476
    :cond_13
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261477
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(I)V

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 261407
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->t:Z

    .line 261408
    return-void
.end method

.method public final aa()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261283
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261284
    const/4 v0, 0x6

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261285
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->Z:Z

    return v0
.end method

.method public final ab()Lcom/facebook/graphql/model/GraphQLUser;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261289
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->aa:Lcom/facebook/graphql/model/GraphQLUser;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261290
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->aa:Lcom/facebook/graphql/model/GraphQLUser;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->aa:Lcom/facebook/graphql/model/GraphQLUser;

    .line 261291
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->aa:Lcom/facebook/graphql/model/GraphQLUser;

    return-object v0
.end method

.method public final ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261292
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261293
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    .line 261294
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    return-object v0
.end method

.method public final ad()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261295
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261296
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261297
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ac:I

    return v0
.end method

.method public final ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261298
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ad:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261299
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ad:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ad:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 261300
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ad:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    return-object v0
.end method

.method public final af()Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;
    .locals 3

    .prologue
    .line 261301
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ae:Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    if-nez v0, :cond_0

    .line 261302
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 261303
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 261304
    if-eqz v0, :cond_1

    .line 261305
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    invoke-virtual {v0, v1, p0, v2}, LX/15i;->a(ILcom/facebook/flatbuffers/Flattenable;Ljava/lang/Class;)Lcom/facebook/graphql/model/extras/BaseExtra;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ae:Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    .line 261306
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ae:Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    return-object v0

    .line 261307
    :cond_1
    new-instance v0, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->ae:Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261308
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261309
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261310
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261311
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261312
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261313
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261314
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261315
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261316
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->g:Z

    return v0
.end method

.method public final e()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261286
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261287
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261288
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 261749
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261770
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261771
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->x:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->x:Ljava/lang/String;

    .line 261772
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261767
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261768
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->C:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->C:Ljava/lang/String;

    .line 261769
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic l()LX/17A;
    .locals 1
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261766
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()LX/172;
    .locals 1
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261765
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261762
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261763
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261764
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->h:Z

    return v0
.end method

.method public final o()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261759
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261760
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261761
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->k:Z

    return v0
.end method

.method public final p()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261756
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261757
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261758
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->l:Z

    return v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLCommentersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261753
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261754
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261755
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261750
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261751
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 261752
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    return-object v0
.end method

.method public final r_()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 261717
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261718
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261719
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->j:Z

    return v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261746
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261747
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 261748
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final s_()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 261743
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261744
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261745
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->s:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261740
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261741
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->p:Ljava/lang/String;

    .line 261742
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 261738
    const-class v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "LikeCount"

    invoke-static {p0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "CommentCount"

    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "doesViewerLike"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 261739
    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261735
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261736
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->q:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->q:Ljava/lang/String;

    .line 261737
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261732
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261733
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261734
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->r:Z

    return v0
.end method

.method public final w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261729
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261730
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261731
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->t:Z

    return v0
.end method

.method public final x()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261726
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261727
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261728
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->u:J

    return-wide v0
.end method

.method public final y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261723
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->v:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 261724
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->v:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->v:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 261725
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->v:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    return-object v0
.end method

.method public final z()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 261720
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 261721
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 261722
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLFeedback;->w:Z

    return v0
.end method
