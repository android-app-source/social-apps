.class public final Lcom/facebook/graphql/model/GraphQLPrivacyOption;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1oQ;
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;
.implements LX/1oS;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyOption$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivacyOption$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325757
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 325758
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 325759
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325760
    return-void
.end method

.method public constructor <init>(LX/4YH;)V
    .locals 1

    .prologue
    .line 325761
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 325762
    iget-object v0, p1, LX/4YH;->b:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 325763
    iget-object v0, p1, LX/4YH;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->f:Ljava/util/List;

    .line 325764
    iget-object v0, p1, LX/4YH;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->g:Ljava/lang/String;

    .line 325765
    iget-object v0, p1, LX/4YH;->e:Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325766
    iget-object v0, p1, LX/4YH;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->i:Ljava/lang/String;

    .line 325767
    iget-object v0, p1, LX/4YH;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j:Ljava/util/List;

    .line 325768
    iget-object v0, p1, LX/4YH;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->k:Ljava/lang/String;

    .line 325769
    iget-object v0, p1, LX/4YH;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l:Ljava/lang/String;

    .line 325770
    iget-object v0, p1, LX/4YH;->j:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    .line 325771
    iget-object v0, p1, LX/4YH;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n:Ljava/util/List;

    .line 325772
    iget-object v0, p1, LX/4YH;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o:Ljava/lang/String;

    .line 325773
    iget-object v0, p1, LX/4YH;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p:Ljava/lang/String;

    .line 325774
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 325775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325776
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 325777
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 325778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 325779
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 325780
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 325781
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 325782
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 325783
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 325784
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->d(Ljava/util/List;)I

    move-result v9

    .line 325785
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 325786
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 325787
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 325788
    const/4 v12, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v0

    sget-object v13, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v13, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v12, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 325789
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 325790
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 325791
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 325792
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 325793
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 325794
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 325795
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 325796
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 325797
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 325798
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 325799
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 325800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325801
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 325802
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 325803
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325804
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 325805
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 325806
    if-eqz v1, :cond_4

    .line 325807
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 325808
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->f:Ljava/util/List;

    move-object v1, v0

    .line 325809
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 325810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 325812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 325813
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325814
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 325815
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 325816
    if-eqz v2, :cond_1

    .line 325817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 325818
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j:Ljava/util/List;

    move-object v1, v0

    .line 325819
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 325820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    .line 325821
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 325822
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 325823
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    .line 325824
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 325825
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/1Fd;
    .locals 1
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325828
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325829
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->k:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->k:Ljava/lang/String;

    .line 325830
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325751
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325752
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l:Ljava/lang/String;

    .line 325753
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325754
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325755
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 325756
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 325750
    const v0, -0x7646fe03    # -4.4539E-33f

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325747
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325748
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j:Ljava/util/List;

    .line 325749
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic k()LX/2cq;
    .locals 1
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325743
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325744
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n:Ljava/util/List;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n:Ljava/util/List;

    .line 325745
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325740
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325741
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o:Ljava/lang/String;

    .line 325742
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325737
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325738
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325739
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325734
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325735
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->i:Ljava/lang/String;

    .line 325736
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325731
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325732
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    .line 325733
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325722
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325723
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p:Ljava/lang/String;

    .line 325724
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final x_()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325728
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325729
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->f:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->f:Ljava/util/List;

    .line 325730
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final y_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325725
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325726
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->g:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->g:Ljava/lang/String;

    .line 325727
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->g:Ljava/lang/String;

    return-object v0
.end method
