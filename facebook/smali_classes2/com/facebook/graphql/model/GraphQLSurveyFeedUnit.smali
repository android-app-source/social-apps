.class public final Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/model/FeedUnit;
.implements LX/16d;
.implements Lcom/facebook/graphql/model/HideableUnit;
.implements LX/0jR;
.implements Lcom/facebook/graphql/model/Sponsorable;
.implements LX/16g;
.implements LX/16h;
.implements LX/0jS;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field private y:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201149
    const-class v0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 201150
    const-class v0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 201151
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 201152
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x46f2ee24

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 201153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->y:LX/0x2;

    .line 201154
    return-void
.end method

.method private A()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201155
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201156
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q:Ljava/lang/String;

    .line 201157
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method private B()Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201158
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201159
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201160
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    return-object v0
.end method

.method private C()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201161
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201162
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u:Ljava/lang/String;

    .line 201163
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u:Ljava/lang/String;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201164
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201165
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201166
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 201167
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->l:I

    .line 201168
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201169
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201170
    if-eqz v0, :cond_0

    .line 201171
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 201172
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 201173
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->j:Ljava/lang/String;

    .line 201174
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201175
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201176
    if-eqz v0, :cond_0

    .line 201177
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 201178
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 201179
    iput-boolean p1, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->x:Z

    .line 201180
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201181
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201182
    if-eqz v0, :cond_0

    .line 201183
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 201184
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 201188
    iput-object p1, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->k:Ljava/lang/String;

    .line 201189
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 201190
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 201191
    if-eqz v0, :cond_0

    .line 201192
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 201193
    :cond_0
    return-void
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201185
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201186
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->j:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->j:Ljava/lang/String;

    .line 201187
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->j:Ljava/lang/String;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201214
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201215
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->k:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->k:Ljava/lang/String;

    .line 201216
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->k:Ljava/lang/String;

    return-object v0
.end method

.method private y()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201211
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201212
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201213
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->l:I

    return v0
.end method

.method private z()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201208
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201209
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->m:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->m:Ljava/lang/String;

    .line 201210
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final C_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201205
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201206
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->i:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->i:Ljava/lang/String;

    .line 201207
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 201204
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final E_()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201201
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201202
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g:Ljava/lang/String;

    .line 201203
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final F_()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 201198
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201199
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201200
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->h:J

    return-wide v0
.end method

.method public final H_()I
    .locals 1

    .prologue
    .line 201197
    invoke-static {p0}, LX/18K;->b(Lcom/facebook/graphql/model/HideableUnit;)I

    move-result v0

    return v0
.end method

.method public final L_()LX/0x2;
    .locals 1

    .prologue
    .line 201194
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->y:LX/0x2;

    if-nez v0, :cond_0

    .line 201195
    new-instance v0, LX/0x2;

    invoke-direct {v0}, LX/0x2;-><init>()V

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->y:LX/0x2;

    .line 201196
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->y:LX/0x2;

    return-object v0
.end method

.method public final P_()Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 1

    .prologue
    .line 201109
    invoke-static {p0}, LX/18K;->a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 201110
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201111
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 201112
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->E_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 201113
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->C_()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 201114
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->w()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 201115
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->x()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 201116
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->z()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 201117
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 201118
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 201119
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 201120
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->A()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 201121
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->r()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 201122
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 201123
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 201124
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->C()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 201125
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 201126
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->c()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 201127
    const/16 v4, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 201128
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 201129
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 201130
    const/4 v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 201131
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 201132
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 201133
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 201134
    const/4 v2, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->y()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 201135
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 201136
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 201137
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 201138
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 201139
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 201140
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201141
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201142
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201143
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201144
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201145
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 201146
    const/16 v2, 0x12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 201147
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201148
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 201023
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 201024
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201025
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201026
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 201027
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 201028
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201029
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 201030
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 201031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 201032
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 201033
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLActor;

    .line 201034
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 201035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    .line 201036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 201037
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 201038
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    .line 201039
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 201040
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201041
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 201042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 201043
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201044
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 201045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 201047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 201048
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201049
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 201050
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201051
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 201052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 201053
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201054
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 201055
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 201064
    new-instance v0, LX/4Z2;

    invoke-direct {v0, p1}, LX/4Z2;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 201061
    iput-wide p1, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->h:J

    .line 201062
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 201056
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 201057
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->h:J

    .line 201058
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->l:I

    .line 201059
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->x:Z

    .line 201060
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 201065
    const-string v0, "local_is_completed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201067
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201068
    const/16 v0, 0x12

    iput v0, p2, LX/18L;->c:I

    .line 201069
    :goto_0
    return-void

    .line 201070
    :cond_0
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201071
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201073
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 201074
    :cond_1
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201075
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201077
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 201078
    :cond_2
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201079
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->y()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 201080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 201081
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 201082
    :cond_3
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 201014
    const-string v0, "local_is_completed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201015
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->a(Z)V

    .line 201016
    :cond_0
    :goto_0
    return-void

    .line 201017
    :cond_1
    const-string v0, "local_last_negative_feedback_action_type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201018
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 201019
    :cond_2
    const-string v0, "local_story_visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201020
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 201021
    :cond_3
    const-string v0, "local_story_visible_height"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201022
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->a(I)V

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201013
    invoke-static {p0}, LX/1fz;->a(LX/16h;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201010
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->w:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201011
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->w:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->w:Ljava/lang/String;

    .line 201012
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201005
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201006
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 201007
    :goto_0
    return-object v0

    .line 201008
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 201009
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 201004
    const v0, -0x46f2ee24

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201083
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201084
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->f:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->f:Ljava/lang/String;

    .line 201085
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201086
    invoke-static {p0}, LX/18K;->a(LX/16d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 201087
    invoke-static {p0}, LX/2dv;->a(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v0

    return v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201088
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201089
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSponsoredData;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 201090
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->n:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201091
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201092
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLActor;

    .line 201093
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201094
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201095
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    .line 201096
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->p:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201097
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201098
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->r:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->r:Ljava/lang/String;

    .line 201099
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201100
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 201101
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 201102
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/model/SponsoredImpression;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201103
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/model/SponsoredImpression;->a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    move-object v0, v0

    .line 201104
    return-object v0
.end method

.method public final u()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 201105
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 201106
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 201107
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->x:Z

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 201108
    invoke-static {p0}, LX/18M;->a(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v0

    return v0
.end method
