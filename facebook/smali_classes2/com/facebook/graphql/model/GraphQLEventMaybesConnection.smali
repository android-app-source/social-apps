.class public final Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLEventMaybesConnection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLEventMaybesConnection$Serializer;
.end annotation


# instance fields
.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I

.field public i:I

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323616
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 323615
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 323613
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 323614
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 323610
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323611
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323612
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method private m()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323607
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323608
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323609
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->i:I

    return v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323604
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323605
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j:Ljava/util/List;

    .line 323606
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 323601
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323602
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323603
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 323552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 323554
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 323555
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->n()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 323556
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 323557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->a()I

    move-result v3

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 323558
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 323559
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 323560
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->k()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323561
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->m()I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 323562
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 323563
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323564
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 323583
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 323584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323585
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323586
    if-eqz v1, :cond_0

    .line 323587
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 323588
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->f:Ljava/util/List;

    .line 323589
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 323590
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 323591
    if-eqz v1, :cond_1

    .line 323592
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 323593
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 323594
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 323595
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323596
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 323597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 323598
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 323599
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 323600
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 323577
    iput p1, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->e:I

    .line 323578
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 323579
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 323580
    if-eqz v0, :cond_0

    .line 323581
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 323582
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323572
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 323573
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->e:I

    .line 323574
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->h:I

    .line 323575
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->i:I

    .line 323576
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 323571
    const v0, 0x7b1bfce3

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323568
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 323569
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->f:Ljava/util/List;

    .line 323570
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 323565
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 323566
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 323567
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->h:I

    return v0
.end method
