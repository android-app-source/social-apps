.class public final Lcom/facebook/graphql/model/GraphQLTimelineAppSection;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppSection$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLTimelineAppSection$Serializer;
.end annotation


# instance fields
.field public e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301311
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 301323
    const-class v0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 301321
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 301322
    return-void
.end method

.method private k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301318
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301319
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301320
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301315
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301316
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301317
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301312
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301313
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301314
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301214
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->h:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301215
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->h:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 301216
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->h:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301308
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301309
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j:Ljava/lang/String;

    .line 301310
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 301305
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301306
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 301307
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301302
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301303
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l:Ljava/lang/String;

    .line 301304
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l:Ljava/lang/String;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301324
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301325
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301326
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301299
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301300
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301301
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301296
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301297
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301298
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301293
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301294
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->p:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->p:Ljava/lang/String;

    .line 301295
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->p:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301290
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301291
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->q:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->q:Ljava/lang/String;

    .line 301292
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 301260
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301261
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 301262
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 301263
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 301264
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 301265
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 301266
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 301267
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 301268
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 301269
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 301270
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 301271
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->u()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 301272
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->v()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 301273
    const/16 v12, 0xe

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 301274
    const/4 v12, 0x1

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 301275
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 301276
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 301277
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 301278
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 301279
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 301280
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->p()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 301281
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 301282
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 301283
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 301284
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 301285
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 301286
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 301287
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301288
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 301289
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->p()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 301222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 301223
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301224
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301225
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 301226
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 301227
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->e:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301228
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 301229
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301230
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 301231
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 301232
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->f:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301233
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 301234
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301235
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 301236
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 301237
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->g:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 301238
    :cond_2
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 301239
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 301240
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 301241
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 301242
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->h:Lcom/facebook/graphql/model/GraphQLImage;

    .line 301243
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 301244
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301245
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 301246
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 301247
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301248
    :cond_4
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 301249
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301250
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 301251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 301252
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301253
    :cond_5
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 301254
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301255
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 301256
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    .line 301257
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 301258
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 301259
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 301220
    const v0, 0x6dd6f4c5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301217
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 301218
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->i:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->i:Ljava/lang/String;

    .line 301219
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->i:Ljava/lang/String;

    return-object v0
.end method
