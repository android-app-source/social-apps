.class public final Lcom/facebook/graphql/model/GraphQLProductItem;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLProductItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLProductItem$Serializer;
.end annotation


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:D

.field public I:D

.field public J:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Z

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Z

.field public ab:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLProductImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductImage;",
            ">;"
        }
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductImage;",
            ">;"
        }
    .end annotation
.end field

.field public ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

.field public h:Z

.field public i:I

.field public j:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

.field public k:J

.field public l:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public w:Z

.field public x:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326252
    const-class v0, Lcom/facebook/graphql/model/GraphQLProductItem$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 326159
    const-class v0, Lcom/facebook/graphql/model/GraphQLProductItem$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 326250
    const/16 v0, 0x3f

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 326251
    return-void
.end method

.method private U()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326247
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326248
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->f:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->f:Ljava/lang/String;

    .line 326249
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method private V()I
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326244
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326245
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326246
    :cond_0
    iget v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->i:I

    return v0
.end method

.method private W()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326241
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->p:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326242
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->p:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326243
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->p:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private X()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326238
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326239
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326240
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private Y()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326235
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->v:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326236
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->v:Ljava/util/List;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->v:Ljava/util/List;

    .line 326237
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->v:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private Z()Lcom/facebook/graphql/enums/GraphQLProductAvailability;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326205
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->x:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326206
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->x:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProductAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->x:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    .line 326207
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->x:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    return-object v0
.end method

.method private aa()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326229
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326230
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->y:Ljava/util/List;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->y:Ljava/util/List;

    .line 326231
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ab()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326226
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->A:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326227
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->A:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326228
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->A:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ac()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326223
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->E:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326224
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->E:Ljava/util/List;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->E:Ljava/util/List;

    .line 326225
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->E:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ad()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326220
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->F:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326221
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->F:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326222
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->F:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ae()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326217
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->G:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326218
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->G:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326219
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->G:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private af()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326214
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->N:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326215
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->N:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326216
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->N:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ag()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326211
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->R:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326212
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->R:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->R:Ljava/lang/String;

    .line 326213
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->R:Ljava/lang/String;

    return-object v0
.end method

.method private ah()Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326208
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->U:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326209
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->U:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->U:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 326210
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->U:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    return-object v0
.end method

.method private ai()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326256
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->V:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326257
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->V:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326258
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->V:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private aj()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326253
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->X:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326254
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->X:Ljava/util/List;

    const/16 v1, 0x2f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->X:Ljava/util/List;

    .line 326255
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->X:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private ak()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326295
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326296
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326297
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private al()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326292
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ab:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326293
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ab:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ab:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 326294
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ab:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    return-object v0
.end method

.method private am()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326289
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326290
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326291
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private an()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326286
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326287
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326288
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method private ao()LX/0Px;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326283
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->aj:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326284
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->aj:Ljava/util/List;

    const/16 v1, 0x3d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->aj:Ljava/util/List;

    .line 326285
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->aj:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326280
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->D:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326281
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->D:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->D:Lcom/facebook/graphql/model/GraphQLStory;

    .line 326282
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->D:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final B()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326298
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326299
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326300
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->H:D

    return-wide v0
.end method

.method public final C()D
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326277
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326278
    const/4 v0, 0x3

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326279
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->I:D

    return-wide v0
.end method

.method public final D()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326274
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->J:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326275
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->J:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326276
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->J:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final E()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326271
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->K:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326272
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->K:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326273
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->K:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final F()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326268
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->L:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326269
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->L:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326270
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->L:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326265
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->M:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326266
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->M:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326267
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->M:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326262
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->O:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326263
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->O:Lcom/facebook/graphql/model/GraphQLPhoto;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->O:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 326264
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->O:Lcom/facebook/graphql/model/GraphQLPhoto;

    return-object v0
.end method

.method public final I()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326232
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->P:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326233
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->P:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326234
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->P:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final J()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326259
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326260
    const/4 v0, 0x4

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326261
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Q:Z

    return v0
.end method

.method public final K()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326156
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->S:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326157
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->S:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->S:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 326158
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->S:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326153
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->T:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326154
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->T:Lcom/facebook/graphql/model/GraphQLActor;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->T:Lcom/facebook/graphql/model/GraphQLActor;

    .line 326155
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->T:Lcom/facebook/graphql/model/GraphQLActor;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326150
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326151
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->W:Ljava/lang/String;

    const/16 v1, 0x2e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->W:Ljava/lang/String;

    .line 326152
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326147
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Y:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326148
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Y:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Y:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 326149
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Y:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final O()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326144
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326145
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326146
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->aa:Z

    return v0
.end method

.method public final P()Lcom/facebook/graphql/model/GraphQLProductImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326141
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ac:Lcom/facebook/graphql/model/GraphQLProductImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326142
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ac:Lcom/facebook/graphql/model/GraphQLProductImage;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ac:Lcom/facebook/graphql/model/GraphQLProductImage;

    .line 326143
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ac:Lcom/facebook/graphql/model/GraphQLProductImage;

    return-object v0
.end method

.method public final Q()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326138
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ad:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326139
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ad:Ljava/util/List;

    const/16 v1, 0x37

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ad:Ljava/util/List;

    .line 326140
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ad:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final R()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325831
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ae:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 325832
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ae:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    const/16 v1, 0x38

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ae:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 325833
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ae:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    return-object v0
.end method

.method public final S()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326132
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->af:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326133
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->af:Ljava/util/List;

    const/16 v1, 0x39

    const-class v2, Lcom/facebook/graphql/model/GraphQLProductImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->af:Ljava/util/List;

    .line 326134
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->af:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final T()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326129
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ag:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326130
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ag:Ljava/lang/String;

    const/16 v1, 0x3a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ag:Ljava/lang/String;

    .line 326131
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->ag:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 52

    .prologue
    .line 326018
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 326019
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->U()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 326020
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->o()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 326021
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->p()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 326022
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->q()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 326023
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->r()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 326024
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 326025
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->s()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 326026
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 326027
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 326028
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 326029
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 326030
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Y()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v18

    .line 326031
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->aa()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v19

    .line 326032
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->x()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 326033
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 326034
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->y()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 326035
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 326036
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->A()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 326037
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ac()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v25

    .line 326038
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 326039
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 326040
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 326041
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 326042
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 326043
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 326044
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 326045
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->H()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 326046
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 326047
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ag()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 326048
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->K()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 326049
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->L()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 326050
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ah()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 326051
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 326052
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->M()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 326053
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->aj()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v41

    .line 326054
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 326055
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->al()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 326056
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 326057
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Q()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v45

    .line 326058
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->R()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 326059
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->S()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v47

    .line 326060
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->T()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 326061
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 326062
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 326063
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ao()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v51

    .line 326064
    const/16 v3, 0x3e

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 326065
    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->j()Z

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 326066
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 326067
    const/4 v3, 0x3

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->k()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 326068
    const/4 v2, 0x4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->l()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 326069
    const/4 v2, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->V()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 326070
    const/4 v3, 0x6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 326071
    const/4 v3, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->n()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 326072
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 326073
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 326074
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 326075
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 326076
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 326077
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 326078
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 326079
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 326080
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326081
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326082
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326083
    const/16 v2, 0x13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->w()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 326084
    const/16 v3, 0x14

    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Z()Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProductAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 326085
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326086
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326087
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326088
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326089
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326090
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326091
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326092
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326093
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326094
    const/16 v3, 0x1e

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->B()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 326095
    const/16 v3, 0x1f

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->C()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 326096
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326097
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326098
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326099
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326100
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326101
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326102
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326103
    const/16 v2, 0x27

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->J()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 326104
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326105
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326106
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326107
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326108
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326109
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326110
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326111
    const/16 v3, 0x30

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->N()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 326112
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326113
    const/16 v2, 0x34

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->O()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 326114
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326115
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326116
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326117
    const/16 v2, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326118
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326119
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326120
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326121
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326122
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 326123
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326124
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2

    .line 326125
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->k()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v2

    goto/16 :goto_0

    .line 326126
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v2

    goto/16 :goto_1

    .line 326127
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Z()Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    move-result-object v2

    goto/16 :goto_2

    .line 326128
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->N()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 325850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 325851
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->o()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 325852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->o()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 325853
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->o()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 325854
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325855
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->l:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 325856
    :cond_0
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->al()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 325857
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->al()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 325858
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->al()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 325859
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325860
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->ab:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 325861
    :cond_1
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 325862
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325863
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 325864
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325865
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325866
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->s()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 325867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->s()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 325868
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->s()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 325869
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325870
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 325871
    :cond_3
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 325872
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325873
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 325874
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325875
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325876
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 325877
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325878
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 325879
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325880
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325881
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 325882
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325883
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 325884
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325885
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325886
    :cond_6
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 325887
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 325888
    if-eqz v2, :cond_7

    .line 325889
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325890
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLProductItem;->v:Ljava/util/List;

    move-object v1, v0

    .line 325891
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->x()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 325892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->x()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 325893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->x()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 325894
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325895
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->z:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 325896
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 325897
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductImage;

    .line 325898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 325899
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325900
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->ac:Lcom/facebook/graphql/model/GraphQLProductImage;

    .line 325901
    :cond_9
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 325902
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325903
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 325904
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325905
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->A:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325906
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 325907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->Q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 325908
    if-eqz v2, :cond_b

    .line 325909
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325910
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLProductItem;->ad:Ljava/util/List;

    move-object v1, v0

    .line 325911
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 325912
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 325913
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 325914
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325915
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 325916
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->A()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 325917
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->A()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 325918
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->A()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 325919
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325920
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->D:Lcom/facebook/graphql/model/GraphQLStory;

    .line 325921
    :cond_d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ac()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 325922
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ac()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 325923
    if-eqz v2, :cond_e

    .line 325924
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325925
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLProductItem;->E:Ljava/util/List;

    move-object v1, v0

    .line 325926
    :cond_e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 325927
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325928
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 325929
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325930
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325931
    :cond_f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 325932
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325933
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 325934
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325935
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->G:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325936
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->S()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 325937
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->S()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 325938
    if-eqz v2, :cond_11

    .line 325939
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325940
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/graphql/model/GraphQLProductItem;->af:Ljava/util/List;

    move-object v1, v0

    .line 325941
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->R()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 325942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->R()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 325943
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->R()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 325944
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325945
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->ae:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 325946
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 325947
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325948
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 325949
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325950
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325951
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 325952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325953
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 325954
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325955
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325956
    :cond_14
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 325957
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325958
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->am()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 325959
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325960
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->ah:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325961
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 325962
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325963
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 325964
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325965
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325966
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 325967
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 325969
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325970
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325971
    :cond_17
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 325972
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325973
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->af()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 325974
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325975
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->N:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325976
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->H()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 325977
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->H()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 325978
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->H()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 325979
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325980
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->O:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 325981
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 325982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 325984
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325985
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325986
    :cond_1a
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 325987
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 325988
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->an()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 325989
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325990
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 325991
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->K()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 325992
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->K()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 325993
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->K()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 325994
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 325995
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->S:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 325996
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->L()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 325997
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->L()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 325998
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->L()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 325999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 326000
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->T:Lcom/facebook/graphql/model/GraphQLActor;

    .line 326001
    :cond_1d
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 326002
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 326003
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ak()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 326004
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 326005
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326006
    :cond_1e
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ah()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 326007
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ah()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 326008
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ah()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 326009
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 326010
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->U:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 326011
    :cond_1f
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 326012
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 326013
    invoke-direct {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 326014
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 326015
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLProductItem;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326016
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 326017
    if-nez v1, :cond_21

    :goto_0
    return-object p0

    :cond_21
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325849
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 325838
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 325839
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->e:Z

    .line 325840
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->h:Z

    .line 325841
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->i:I

    .line 325842
    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->k:J

    .line 325843
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->w:Z

    .line 325844
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->H:D

    .line 325845
    const/16 v0, 0x1f

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->I:D

    .line 325846
    const/16 v0, 0x27

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->Q:Z

    .line 325847
    const/16 v0, 0x34

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->aa:Z

    .line 325848
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 325837
    const v0, 0xa7c5482

    return v0
.end method

.method public final j()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 325834
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 325835
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 325836
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->e:Z

    return v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326135
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->g:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326136
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->g:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->g:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 326137
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->g:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    return-object v0
.end method

.method public final l()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326160
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326161
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326162
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->h:Z

    return v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326163
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->j:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326164
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->j:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->j:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    .line 326165
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->j:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    return-object v0
.end method

.method public final n()J
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326166
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326167
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326168
    :cond_0
    iget-wide v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->k:J

    return-wide v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326169
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->l:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326170
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->l:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->l:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 326171
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->l:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326172
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326173
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->m:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->m:Ljava/lang/String;

    .line 326174
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326175
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326176
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->n:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->n:Ljava/lang/String;

    .line 326177
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326178
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326179
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->o:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->o:Ljava/lang/String;

    .line 326180
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326181
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326182
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 326183
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->q:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326184
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326185
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->s:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->s:Ljava/lang/String;

    .line 326186
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326187
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->t:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326188
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->t:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->t:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326189
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->t:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326190
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->u:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326191
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->u:Lcom/facebook/graphql/model/GraphQLImage;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 326192
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->u:Lcom/facebook/graphql/model/GraphQLImage;

    return-object v0
.end method

.method public final w()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 326193
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 326194
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 326195
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->w:Z

    return v0
.end method

.method public final x()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326196
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->z:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326197
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->z:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->z:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 326198
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->z:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326199
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326200
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->B:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->B:Ljava/lang/String;

    .line 326201
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 326202
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->C:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 326203
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->C:Lcom/facebook/graphql/model/GraphQLPage;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 326204
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLProductItem;->C:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method
