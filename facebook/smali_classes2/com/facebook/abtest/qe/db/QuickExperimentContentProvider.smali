.class public Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;
.super LX/0Ov;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0ce;

.field private c:LX/3yV;

.field private d:LX/0PL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55928
    const-class v0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;

    sput-object v0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55927
    invoke-direct {p0}, LX/0Ov;-><init>()V

    return-void
.end method

.method private a(LX/0ce;LX/3yV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 55908
    iput-object p1, p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->b:LX/0ce;

    .line 55909
    iput-object p2, p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->c:LX/3yV;

    .line 55910
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;

    invoke-static {v1}, LX/0ce;->a(LX/0QB;)LX/0ce;

    move-result-object v0

    check-cast v0, LX/0ce;

    new-instance p1, LX/3yV;

    invoke-static {v1}, LX/2VQ;->a(LX/0QB;)LX/2VQ;

    move-result-object v2

    check-cast v2, LX/2VQ;

    invoke-direct {p1, v2}, LX/3yV;-><init>(LX/2VQ;)V

    move-object v1, p1

    check-cast v1, LX/3yV;

    invoke-direct {p0, v0, v1}, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->a(LX/0ce;LX/3yV;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55926
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55925
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55918
    const-string v0, "QuickExperimentContentProvider.doQuery"

    const v1, 0x76eacbd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 55919
    :try_start_0
    iget-object v0, p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->d:LX/0PL;

    invoke-virtual {v0, p1}, LX/0PL;->a(Landroid/net/Uri;)LX/2Rg;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 55920
    invoke-virtual/range {v0 .. v5}, LX/2Rg;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 55921
    const v1, -0x2e1870d9

    invoke-static {v1}, LX/02m;->b(I)J

    .line 55922
    return-object v0

    .line 55923
    :catchall_0
    move-exception v0

    const v1, 0x7a9e522e

    invoke-static {v1}, LX/02m;->b(I)J

    .line 55924
    throw v0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 55917
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55916
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 55911
    invoke-super {p0}, LX/0Ov;->a()V

    .line 55912
    const-class v0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;

    invoke-static {v0, p0}, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 55913
    new-instance v0, LX/0PL;

    invoke-direct {v0}, LX/0PL;-><init>()V

    iput-object v0, p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->d:LX/0PL;

    .line 55914
    iget-object v0, p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->d:LX/0PL;

    iget-object v1, p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->b:LX/0ce;

    iget-object v1, v1, LX/0ce;->a:Ljava/lang/String;

    const-string v2, "metainfo"

    iget-object v3, p0, Lcom/facebook/abtest/qe/db/QuickExperimentContentProvider;->c:LX/3yV;

    invoke-virtual {v0, v1, v2, v3}, LX/0PL;->a(Ljava/lang/String;Ljava/lang/String;LX/2Rg;)V

    .line 55915
    return-void
.end method
