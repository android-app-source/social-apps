.class public Lcom/facebook/abtest/qe/analytics/QuickExperimentAnalyticsEvent;
.super Lcom/facebook/analytics/HoneyExperimentEvent;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/1gX;Ljava/lang/String;LX/0lF;)V
    .locals 2
    .param p5    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 294416
    const-string v0, "quick_experiment_event"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/HoneyExperimentEvent;-><init>(Ljava/lang/String;)V

    .line 294417
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "__qe__"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 294418
    iput-object v0, p0, Lcom/facebook/analytics/HoneyExperimentEvent;->c:Ljava/lang/String;

    .line 294419
    const-string v0, "group"

    invoke-virtual {p0, v0, p2}, Lcom/facebook/analytics/HoneyExperimentEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyExperimentEvent;

    .line 294420
    const-string v0, "event"

    invoke-virtual {p3}, LX/1gX;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/HoneyExperimentEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyExperimentEvent;

    .line 294421
    const-string v0, "context"

    invoke-virtual {p0, v0, p4}, Lcom/facebook/analytics/HoneyExperimentEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyExperimentEvent;

    .line 294422
    if-eqz p5, :cond_0

    .line 294423
    const-string v0, "extras"

    invoke-virtual {p0, v0, p5}, Lcom/facebook/analytics/HoneyExperimentEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/HoneyExperimentEvent;

    .line 294424
    :cond_0
    return-void
.end method
