.class public Lcom/facebook/abtest/qe/QuickExperimentINeedInit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:Lcom/facebook/abtest/qe/QuickExperimentINeedInit;


# instance fields
.field private final a:LX/0d6;

.field private final b:LX/0ae;


# direct methods
.method public constructor <init>(LX/0d6;LX/0ae;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 89012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89013
    iput-object p1, p0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->a:LX/0d6;

    .line 89014
    iput-object p2, p0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->b:LX/0ae;

    .line 89015
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/abtest/qe/QuickExperimentINeedInit;
    .locals 5

    .prologue
    .line 89016
    sget-object v0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->c:Lcom/facebook/abtest/qe/QuickExperimentINeedInit;

    if-nez v0, :cond_1

    .line 89017
    const-class v1, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;

    monitor-enter v1

    .line 89018
    :try_start_0
    sget-object v0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->c:Lcom/facebook/abtest/qe/QuickExperimentINeedInit;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 89019
    if-eqz v2, :cond_0

    .line 89020
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 89021
    new-instance p0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;

    .line 89022
    invoke-static {v0}, LX/0ca;->a(LX/0QB;)LX/0ca;

    move-result-object v3

    check-cast v3, LX/0cb;

    invoke-static {v3}, LX/0d4;->a(LX/0cb;)LX/0d6;

    move-result-object v3

    move-object v3, v3

    .line 89023
    check-cast v3, LX/0d6;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ae;

    invoke-direct {p0, v3, v4}, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;-><init>(LX/0d6;LX/0ae;)V

    .line 89024
    move-object v0, p0

    .line 89025
    sput-object v0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->c:Lcom/facebook/abtest/qe/QuickExperimentINeedInit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89026
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 89027
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 89028
    :cond_1
    sget-object v0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->c:Lcom/facebook/abtest/qe/QuickExperimentINeedInit;

    return-object v0

    .line 89029
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 89030
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 89031
    iget-object v0, p0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->a:LX/0d6;

    invoke-interface {v0}, LX/0d6;->a()V

    .line 89032
    iget-object v0, p0, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->b:LX/0ae;

    invoke-interface {v0}, LX/0ae;->a()V

    .line 89033
    return-void
.end method
