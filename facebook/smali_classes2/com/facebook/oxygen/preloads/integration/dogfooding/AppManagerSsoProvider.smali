.class public Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;
.super LX/0Om;
.source ""


# instance fields
.field public a:LX/2Q4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserTrustedTester;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55842
    invoke-direct {p0}, LX/0Om;-><init>()V

    .line 55843
    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55839
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "uid"

    aput-object v2, v1, v3

    const-string v2, "access_token"

    aput-object v2, v1, v4

    const-string v2, "failure_reason"

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 55840
    new-array v1, v6, [Ljava/lang/Object;

    aput-object v7, v1, v3

    aput-object v7, v1, v4

    aput-object p0, v1, v5

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 55841
    return-object v0
.end method

.method private static a(Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;LX/2Q4;LX/0Or;LX/0Or;LX/0Or;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;",
            "LX/2Q4;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55838
    iput-object p1, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->a:LX/2Q4;

    iput-object p2, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->e:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;

    invoke-static {v5}, LX/2Q4;->b(LX/0QB;)LX/2Q4;

    move-result-object v1

    check-cast v1, LX/2Q4;

    const/16 v2, 0x19e

    invoke-static {v5, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x2fd

    invoke-static {v5, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2ff

    invoke-static {v5, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {v0 .. v5}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->a(Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;LX/2Q4;LX/0Or;LX/0Or;LX/0Or;LX/03V;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55844
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 55837
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 55820
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 55821
    if-nez v0, :cond_0

    .line 55822
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->e:LX/03V;

    const-string v1, "AppManagerSsoProvider__USER_NOT_LOGGED_IN"

    const-string v2, "Not logged in."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55823
    const-string v0, "User not logged in."

    invoke-static {v0}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 55824
    :goto_0
    return-object v0

    .line 55825
    :cond_0
    iget-object v1, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_1

    .line 55826
    iget-object v1, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->e:LX/03V;

    const-string v2, "AppManagerSsoProvider__USER_NOT_LOGGED_IN"

    .line 55827
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 55828
    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55829
    const-string v0, "User is not an employee."

    invoke-static {v0}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 55830
    :cond_1
    new-instance v1, Landroid/database/MatrixCursor;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "uid"

    aput-object v3, v2, v4

    const-string v3, "access_token"

    aput-object v3, v2, v5

    const-string v3, "failure_reason"

    aput-object v3, v2, v6

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 55831
    new-array v2, v7, [Ljava/lang/Object;

    .line 55832
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v3

    .line 55833
    aput-object v3, v2, v4

    .line 55834
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v3

    .line 55835
    aput-object v0, v2, v5

    const/4 v0, 0x0

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object v0, v1

    .line 55836
    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 55819
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55818
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 55815
    invoke-virtual {p0}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 55816
    const-class v0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;

    invoke-static {v0, p0}, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->a(Ljava/lang/Class;LX/02k;)V

    .line 55817
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 55809
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 55810
    :goto_0
    return v2

    .line 55811
    :cond_0
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/dogfooding/AppManagerSsoProvider;->a:LX/2Q4;

    .line 55812
    iget-object v1, v0, LX/2Q4;->b:LX/2QK;

    invoke-virtual {v1}, LX/2QK;->a()V

    .line 55813
    iget-object v1, v0, LX/2Q4;->a:LX/2Q5;

    invoke-virtual {v1}, LX/2Q5;->a()LX/Jut;

    .line 55814
    goto :goto_0
.end method
