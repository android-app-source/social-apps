.class public abstract Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Nt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Props:",
        "Ljava/lang/Object;",
        "State:",
        "Ljava/lang/Object;",
        "Environment::",
        "LX/1PW;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Nt",
        "<TProps;TState;TEnvironment;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 237881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TEnvironment;>;TProps;TEnvironment;)TState;"
        }
    .end annotation

    .prologue
    .line 237875
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProps;TState;TEnvironment;)V"
        }
    .end annotation

    .prologue
    .line 237879
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V

    .line 237880
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .annotation build Lcom/facebook/infer/annotation/NoAllocation;
    .end annotation

    .annotation build Lcom/facebook/infer/annotation/PerformanceCritical;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProps;TState;TEnvironment;TV;)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6d5f310b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 237878
    const/16 v1, 0x1f

    const v2, -0x38b510ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProps;TState;TEnvironment;)V"
        }
    .end annotation

    .prologue
    .line 237877
    return-void
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0
    .annotation build Lcom/facebook/infer/annotation/NoAllocation;
    .end annotation

    .annotation build Lcom/facebook/infer/annotation/PerformanceCritical;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProps;TState;TEnvironment;TV;)V"
        }
    .end annotation

    .prologue
    .line 237876
    return-void
.end method
