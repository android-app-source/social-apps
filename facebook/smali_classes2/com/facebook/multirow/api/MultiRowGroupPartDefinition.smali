.class public interface abstract Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Props:",
        "Ljava/lang/Object;",
        "State:",
        "Ljava/lang/Object;",
        "Environment::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
        "<TProps;TEnvironment;>;"
    }
.end annotation


# virtual methods
.method public abstract a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TEnvironment;>;TProps;TEnvironment;)TState;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProps;TState;TEnvironment;",
            "Lcom/facebook/multirow/api/RowViewData;",
            ")V"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProps;TState;TEnvironment;",
            "Lcom/facebook/multirow/api/RowViewData;",
            ")V"
        }
    .end annotation
.end method
