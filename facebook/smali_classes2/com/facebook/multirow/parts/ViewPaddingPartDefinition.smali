.class public Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1ds;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 286661
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 286662
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;
    .locals 3

    .prologue
    .line 286663
    const-class v1, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    monitor-enter v1

    .line 286664
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 286665
    sput-object v2, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 286666
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286667
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 286668
    new-instance v0, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;-><init>()V

    .line 286669
    move-object v0, v0

    .line 286670
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 286671
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286672
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 286673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x21566a5c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 286674
    check-cast p1, LX/1ds;

    const/high16 p3, -0x80000000

    .line 286675
    iget v1, p1, LX/1ds;->c:I

    if-ne v1, p3, :cond_0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    :goto_0
    iget v2, p1, LX/1ds;->a:I

    if-ne v2, p3, :cond_1

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    :goto_1
    iget p0, p1, LX/1ds;->d:I

    if-ne p0, p3, :cond_2

    invoke-virtual {p4}, Landroid/view/View;->getPaddingRight()I

    move-result p0

    :goto_2
    iget p2, p1, LX/1ds;->b:I

    if-ne p2, p3, :cond_3

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result p2

    :goto_3
    invoke-static {p4, v1, v2, p0, p2}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 286676
    const/16 v1, 0x1f

    const v2, 0x279731c1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 286677
    :cond_0
    iget v1, p1, LX/1ds;->c:I

    goto :goto_0

    :cond_1
    iget v2, p1, LX/1ds;->a:I

    goto :goto_1

    :cond_2
    iget p0, p1, LX/1ds;->d:I

    goto :goto_2

    :cond_3
    iget p2, p1, LX/1ds;->b:I

    goto :goto_3
.end method
