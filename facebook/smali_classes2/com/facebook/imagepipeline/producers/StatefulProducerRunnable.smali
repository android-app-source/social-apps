.class public abstract Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;
.super Lcom/facebook/common/executors/StatefulRunnable;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/common/executors/StatefulRunnable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final b:LX/1cd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:LX/1BV;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "LX/1BV;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282393
    invoke-direct {p0}, Lcom/facebook/common/executors/StatefulRunnable;-><init>()V

    .line 282394
    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->b:LX/1cd;

    .line 282395
    iput-object p2, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    .line 282396
    iput-object p3, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->d:Ljava/lang/String;

    .line 282397
    iput-object p4, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    .line 282398
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282399
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 282400
    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    iget-object v4, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    invoke-interface {v0, v4}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282401
    const/4 v0, 0x0

    move-object v0, v0

    .line 282402
    :goto_0
    invoke-interface {v1, v2, v3, p1, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 282403
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->b:LX/1cd;

    invoke-virtual {v0, p1}, LX/1cd;->b(Ljava/lang/Throwable;)V

    .line 282404
    return-void

    .line 282405
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 282406
    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    iget-object v4, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    invoke-interface {v0, v4}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v2, v3, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 282407
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->b:LX/1cd;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 282408
    return-void

    .line 282409
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    .line 282410
    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->c:LX/1BV;

    iget-object v4, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->e:Ljava/lang/String;

    invoke-interface {v0, v4}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282411
    const/4 v0, 0x0

    move-object v0, v0

    .line 282412
    :goto_0
    invoke-interface {v1, v2, v3, v0}, LX/1BV;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 282413
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->b:LX/1cd;

    invoke-virtual {v0}, LX/1cd;->b()V

    .line 282414
    return-void

    .line 282415
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282416
    const/4 v0, 0x0

    return-object v0
.end method
