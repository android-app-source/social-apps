.class public Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;
.super LX/1Gj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Gj",
        "<",
        "LX/1ur;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/1Gk;

.field private final c:LX/00H;

.field private final d:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final e:LX/1Gl;

.field private final f:LX/1Gm;

.field private final g:LX/1Go;

.field private final h:LX/0oz;

.field private final i:LX/13t;

.field public final j:LX/0So;

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Iq;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Zb;LX/1Gk;LX/00H;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gl;LX/1Gm;LX/1Go;LX/0oz;LX/13t;LX/0So;Ljava/util/Set;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/1Gk;",
            "LX/00H;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/1Gl;",
            "LX/1Gm;",
            "LX/1Go;",
            "LX/0oz;",
            "LX/13t;",
            "LX/0So;",
            "Ljava/util/Set",
            "<",
            "LX/1Iq;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225929
    invoke-direct {p0}, LX/1Gj;-><init>()V

    .line 225930
    iput-object p1, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->a:LX/0Zb;

    .line 225931
    iput-object p2, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->b:LX/1Gk;

    .line 225932
    iput-object p3, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->c:LX/00H;

    .line 225933
    iput-object p4, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->d:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 225934
    iput-object p5, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->e:LX/1Gl;

    .line 225935
    iput-object p6, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->f:LX/1Gm;

    .line 225936
    iput-object p7, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->g:LX/1Go;

    .line 225937
    iput-object p8, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->h:LX/0oz;

    .line 225938
    iput-object p9, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->i:LX/13t;

    .line 225939
    iput-object p10, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->j:LX/0So;

    .line 225940
    iput-object p11, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->k:Ljava/util/Set;

    .line 225941
    iput-object p12, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->l:LX/0ad;

    .line 225942
    return-void
.end method

.method private a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;LX/1ur;LX/1ut;)LX/1j2;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "LX/1ur;",
            "Lcom/facebook/imagepipeline/producers/NetworkFetcher$Callback;",
            ")",
            "LX/1j2",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225918
    new-instance v2, LX/1uv;

    iget-object v3, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->c:LX/00H;

    invoke-virtual {v3}, LX/00H;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/1uv;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 225919
    new-instance v11, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v3

    invoke-direct {v11, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 225920
    const-string v3, "Referer"

    invoke-virtual {v2}, LX/1uv;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11, v3, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 225921
    const-string v2, "X-FB-Connection-Type"

    iget-object v3, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->i:LX/13t;

    invoke-virtual {v3}, LX/13t;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v11, v2, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 225922
    invoke-interface {v11}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 225923
    iget-object v2, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->f:LX/1Gm;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Gm;->b(Ljava/lang/String;)V

    .line 225924
    new-instance v12, LX/1uw;

    iget-object v2, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->f:LX/1Gm;

    invoke-direct {v12, p1, v2}, LX/1uw;-><init>(Landroid/net/Uri;LX/1Gm;)V

    .line 225925
    new-instance v4, LX/1ux;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {v4, p0, v0, v1}, LX/1ux;-><init>(Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;LX/1ur;LX/1ut;)V

    .line 225926
    new-instance v2, LX/1uz;

    const-string v5, "image"

    iget-object v6, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->f:LX/1Gm;

    iget-object v7, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->a:LX/0Zb;

    iget-object v8, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->b:LX/1Gk;

    iget-object v9, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->e:LX/1Gl;

    iget-object v10, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->g:LX/1Go;

    move-object v3, p1

    invoke-direct/range {v2 .. v10}, LX/1uz;-><init>(Landroid/net/Uri;LX/1uy;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;)V

    .line 225927
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v3

    const-string v4, "image"

    invoke-virtual {v3, v4}, LX/15E;->a(Ljava/lang/String;)LX/15E;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/15E;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/15E;

    move-result-object v3

    const-string v4, "MediaDownloader"

    invoke-virtual {v3, v4}, LX/15E;->b(Ljava/lang/String;)LX/15E;

    move-result-object v3

    invoke-virtual {v3, v11}, LX/15E;->a(Lorg/apache/http/client/methods/HttpUriRequest;)LX/15E;

    move-result-object v3

    invoke-virtual {v3, v12}, LX/15E;->a(Lorg/apache/http/client/RedirectHandler;)LX/15E;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/15E;->a(Lorg/apache/http/client/ResponseHandler;)LX/15E;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, LX/15E;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/15E;

    move-result-object v2

    sget-object v3, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v2, v3}, LX/15E;->a(LX/14P;)LX/15E;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/15E;->a(Z)LX/15E;

    move-result-object v2

    .line 225928
    iget-object v3, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->d:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v2}, LX/15E;->a()LX/15D;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v2

    return-object v2
.end method

.method public static a(LX/1bc;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 3

    .prologue
    .line 225912
    sget-object v0, LX/1uu;->a:[I

    invoke-virtual {p0}, LX/1bc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 225913
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized priority: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225914
    :pswitch_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 225915
    :goto_0
    return-object v0

    .line 225916
    :pswitch_1
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    .line 225917
    :pswitch_2
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;
    .locals 3

    .prologue
    .line 225902
    sget-object v0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->m:Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    if-nez v0, :cond_1

    .line 225903
    const-class v1, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    monitor-enter v1

    .line 225904
    :try_start_0
    sget-object v0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->m:Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225905
    if-eqz v2, :cond_0

    .line 225906
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->b(LX/0QB;)Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    move-result-object v0

    sput-object v0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->m:Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225907
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225908
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225909
    :cond_1
    sget-object v0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->m:Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    return-object v0

    .line 225910
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;
    .locals 14

    .prologue
    .line 225943
    new-instance v0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v2

    check-cast v2, LX/1Gk;

    const-class v3, LX/00H;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/00H;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v4

    check-cast v4, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v5

    check-cast v5, LX/1Gl;

    invoke-static {p0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {p0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v7

    check-cast v7, LX/1Go;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v8

    check-cast v8, LX/0oz;

    invoke-static {p0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v9

    check-cast v9, LX/13t;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    .line 225944
    new-instance v11, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v12

    new-instance v13, LX/1Gp;

    invoke-direct {v13, p0}, LX/1Gp;-><init>(LX/0QB;)V

    invoke-direct {v11, v12, v13}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v11, v11

    .line 225945
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;-><init>(LX/0Zb;LX/1Gk;LX/00H;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gl;LX/1Gm;LX/1Go;LX/0oz;LX/13t;LX/0So;Ljava/util/Set;LX/0ad;)V

    .line 225946
    return-object v0
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)LX/1us;
    .locals 4

    .prologue
    .line 225901
    new-instance v0, LX/1ur;

    iget-object v1, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->j:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, LX/1ur;-><init>(LX/1cd;LX/1cW;J)V

    return-object v0
.end method

.method public final a(LX/1us;I)V
    .locals 8

    .prologue
    .line 225889
    check-cast p1, LX/1ur;

    .line 225890
    iget-object v0, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->j:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 225891
    iput-wide v0, p1, LX/1ur;->d:J

    .line 225892
    iget-object v0, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Iq;

    .line 225893
    iget-object v1, p1, LX/1us;->b:LX/1cW;

    move-object v1, v1

    .line 225894
    iget-object v2, v1, LX/1cW;->a:LX/1bf;

    move-object v1, v2

    .line 225895
    iget-object v2, p1, LX/1us;->b:LX/1cW;

    move-object v2, v2

    .line 225896
    iget-object v3, v2, LX/1cW;->d:Ljava/lang/Object;

    move-object v2, v3

    .line 225897
    check-cast v2, Lcom/facebook/common/callercontext/CallerContext;

    .line 225898
    iget-object v3, p1, LX/1us;->b:LX/1cW;

    move-object v3, v3

    .line 225899
    invoke-virtual {v3}, LX/1cW;->f()Z

    move-result v4

    invoke-virtual {p1}, LX/1ur;->l()Z

    move-result v5

    move v3, p2

    invoke-interface/range {v0 .. v5}, LX/1Iq;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;IZZ)V

    goto :goto_0

    .line 225900
    :cond_0
    return-void
.end method

.method public final a(LX/1us;LX/1ut;)V
    .locals 7

    .prologue
    .line 225875
    check-cast p1, LX/1ur;

    .line 225876
    iget-object v0, p1, LX/1us;->b:LX/1cW;

    move-object v6, v0

    .line 225877
    iget-object v0, v6, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 225878
    :try_start_0
    iget-object v1, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 225879
    iget-object v0, v6, LX/1cW;->d:Ljava/lang/Object;

    move-object v2, v0

    .line 225880
    check-cast v2, Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6}, LX/1cW;->g()LX/1bc;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->a(LX/1bc;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v3

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;LX/1ur;LX/1ut;)LX/1j2;

    move-result-object v0

    .line 225881
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v1

    .line 225882
    new-instance v2, LX/1v2;

    invoke-direct {v2, p0, p2}, LX/1v2;-><init>(Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;LX/1ut;)V

    .line 225883
    sget-object v3, LX/1fo;->a:LX/1fo;

    move-object v3, v3

    .line 225884
    invoke-static {v1, v2, v3}, LX/1v3;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 225885
    new-instance v1, LX/1v4;

    invoke-direct {v1, p0, p1, v0, v6}, LX/1v4;-><init>(Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;LX/1ur;LX/1j2;LX/1cW;)V

    invoke-virtual {v6, v1}, LX/1cW;->a(LX/1cg;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 225886
    :goto_0
    return-void

    .line 225887
    :catch_0
    move-exception v0

    .line 225888
    invoke-virtual {p2, v0}, LX/1ut;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(LX/1us;)Z
    .locals 2

    .prologue
    .line 225873
    iget-object v0, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->h:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 225874
    sget-object v1, LX/0p3;->POOR:LX/0p3;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/0p3;->MODERATE:LX/0p3;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1us;I)Ljava/util/Map;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 225851
    check-cast p1, LX/1ur;

    .line 225852
    iget-wide v10, p1, LX/1ur;->c:J

    move-wide v0, v10

    .line 225853
    iget-wide v10, p1, LX/1ur;->b:J

    move-wide v2, v10

    .line 225854
    sub-long/2addr v0, v2

    .line 225855
    iget-wide v10, p1, LX/1ur;->e:J

    move-wide v2, v10

    .line 225856
    iget-wide v10, p1, LX/1ur;->b:J

    move-wide v4, v10

    .line 225857
    sub-long/2addr v2, v4

    .line 225858
    iget-object v4, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->h:LX/0oz;

    invoke-virtual {v4}, LX/0oz;->f()D

    move-result-wide v4

    .line 225859
    iget-object v6, p1, LX/1ur;->a:LX/2ur;

    move-object v6, v6

    .line 225860
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v7

    .line 225861
    const-string v8, "responseLatency"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 225862
    const-string v0, "result_content_length"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 225863
    const-string v0, "rtt_ms"

    iget-object v1, p0, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->h:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->k()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 225864
    const-string v0, "average_bandwidth_kbit"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 225865
    const-string v0, "dropped_bytes"

    .line 225866
    iget v10, p1, LX/1ur;->f:I

    int-to-long v10, v10

    move-wide v4, v10

    .line 225867
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 225868
    invoke-virtual {p1}, LX/1ur;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225869
    const-string v0, "cancellation_time_ms"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 225870
    :cond_0
    sget-object v0, LX/2ur;->NOT_IN_GK:LX/2ur;

    if-eq v6, v0, :cond_1

    .line 225871
    const-string v0, "cdnHeaderResponse"

    invoke-virtual {v6}, LX/2ur;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 225872
    :cond_1
    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
