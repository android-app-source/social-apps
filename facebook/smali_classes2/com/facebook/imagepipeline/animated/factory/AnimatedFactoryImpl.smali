.class public Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private a:LX/1G9;

.field private b:LX/1bw;

.field private c:LX/1c3;

.field private d:LX/1GA;

.field private e:LX/1Ft;

.field private f:LX/1FZ;


# direct methods
.method public constructor <init>(LX/1FZ;LX/1Ft;)V
    .locals 0
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 224852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224853
    iput-object p1, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->f:LX/1FZ;

    .line 224854
    iput-object p2, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->e:LX/1Ft;

    .line 224855
    return-void
.end method

.method private static a(LX/1G9;LX/1c2;LX/1bw;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/res/Resources;)LX/1c3;
    .locals 6

    .prologue
    .line 224851
    new-instance v0, LX/1c3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/1c3;-><init>(LX/1G9;LX/1c2;LX/1bw;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/res/Resources;)V

    return-object v0
.end method

.method private a(LX/1bv;Landroid/app/ActivityManager;LX/1bw;LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;Landroid/content/res/Resources;)LX/1c3;
    .locals 6

    .prologue
    .line 224849
    new-instance v0, LX/1c1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/1c1;-><init>(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;LX/1bv;Landroid/app/ActivityManager;LX/1bw;LX/0So;)V

    .line 224850
    invoke-static {p4, v0, p3, p5, p7}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a(LX/1G9;LX/1c2;LX/1bw;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/res/Resources;)LX/1c3;

    move-result-object v0

    return-object v0
.end method

.method private b()LX/1G9;
    .locals 1

    .prologue
    .line 224846
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a:LX/1G9;

    if-nez v0, :cond_0

    .line 224847
    new-instance v0, LX/1bx;

    invoke-direct {v0, p0}, LX/1bx;-><init>(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a:LX/1G9;

    .line 224848
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a:LX/1G9;

    return-object v0
.end method

.method public static c(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)LX/1bw;
    .locals 1

    .prologue
    .line 224843
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->b:LX/1bw;

    if-nez v0, :cond_0

    .line 224844
    new-instance v0, LX/1bw;

    invoke-direct {v0}, LX/1bw;-><init>()V

    iput-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->b:LX/1bw;

    .line 224845
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->b:LX/1bw;

    return-object v0
.end method

.method private d()LX/1GA;
    .locals 3

    .prologue
    .line 224831
    new-instance v0, LX/1G8;

    invoke-direct {v0, p0}, LX/1G8;-><init>(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)V

    .line 224832
    new-instance v1, LX/1GA;

    iget-object v2, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->f:LX/1FZ;

    invoke-direct {v1, v0, v2}, LX/1GA;-><init>(LX/1G9;LX/1FZ;)V

    return-object v1
.end method


# virtual methods
.method public final a()LX/1GA;
    .locals 1

    .prologue
    .line 224840
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->d:LX/1GA;

    if-nez v0, :cond_0

    .line 224841
    invoke-direct {p0}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->d()LX/1GA;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->d:LX/1GA;

    .line 224842
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->d:LX/1GA;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)LX/1c3;
    .locals 8

    .prologue
    .line 224833
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->c:LX/1c3;

    if-nez v0, :cond_0

    .line 224834
    new-instance v1, LX/1bt;

    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->e:LX/1Ft;

    invoke-interface {v0}, LX/1Ft;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1bt;-><init>(Ljava/util/concurrent/Executor;)V

    .line 224835
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    .line 224836
    invoke-static {p0}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->c(Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;)LX/1bw;

    move-result-object v3

    invoke-direct {p0}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->b()LX/1G9;

    move-result-object v4

    invoke-static {}, LX/1by;->b()LX/1by;

    move-result-object v5

    .line 224837
    sget-object v0, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v6, v0

    .line 224838
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a(LX/1bv;Landroid/app/ActivityManager;LX/1bw;LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;Landroid/content/res/Resources;)LX/1c3;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->c:LX/1c3;

    .line 224839
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->c:LX/1c3;

    return-object v0
.end method
