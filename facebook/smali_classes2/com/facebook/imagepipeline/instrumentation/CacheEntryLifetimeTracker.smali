.class public Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:J

.field private static final b:LX/0Tn;

.field private static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static final e:LX/0Tn;

.field private static final f:LX/0Tn;

.field private static final g:LX/0Tn;

.field private static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field private static volatile y:Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;


# instance fields
.field private final u:LX/0Zb;

.field public final v:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final w:LX/0SG;

.field public final x:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 225665
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->a:J

    .line 225666
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "cache_entry_lifetime"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 225667
    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->b:LX/0Tn;

    const-string v1, "tracking_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 225668
    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "resource_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    .line 225669
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "calling_class"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->e:LX/0Tn;

    .line 225670
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "analytics_tag"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->f:LX/0Tn;

    .line 225671
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "size_bytes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->g:LX/0Tn;

    .line 225672
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "write_unix_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->h:LX/0Tn;

    .line 225673
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "eviction_unix_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->i:LX/0Tn;

    .line 225674
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "eviction_reason"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->j:LX/0Tn;

    .line 225675
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "evicted_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->k:LX/0Tn;

    .line 225676
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "evicted_size"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->l:LX/0Tn;

    .line 225677
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "written_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->m:LX/0Tn;

    .line 225678
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "written_size"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->n:LX/0Tn;

    .line 225679
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "random_write_calling_class"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->o:LX/0Tn;

    .line 225680
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "random_write_analytics_tag"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->p:LX/0Tn;

    .line 225681
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "random_write_size_bytes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->q:LX/0Tn;

    .line 225682
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "random_evict_calling_class"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->r:LX/0Tn;

    .line 225683
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "random_evict_analytics_tag"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->s:LX/0Tn;

    .line 225684
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    const-string v1, "random_evict_size_bytes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->t:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;Ljava/util/Random;)V
    .locals 0
    .param p4    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225660
    iput-object p1, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->u:LX/0Zb;

    .line 225661
    iput-object p2, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 225662
    iput-object p3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->w:LX/0SG;

    .line 225663
    iput-object p4, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->x:Ljava/util/Random;

    .line 225664
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;
    .locals 7

    .prologue
    .line 225646
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->y:Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    if-nez v0, :cond_1

    .line 225647
    const-class v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    monitor-enter v1

    .line 225648
    :try_start_0
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->y:Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225649
    if-eqz v2, :cond_0

    .line 225650
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225651
    new-instance p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v6

    check-cast v6, Ljava/util/Random;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;-><init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;Ljava/util/Random;)V

    .line 225652
    move-object v0, p0

    .line 225653
    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->y:Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225654
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225655
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225656
    :cond_1
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->y:Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;

    return-object v0

    .line 225657
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225658
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x0

    .line 225622
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->u:LX/0Zb;

    const-string v2, "cache_entry_lifetime"

    invoke-interface {v1, v2, v0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 225623
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 225624
    const-string v2, "calling_class"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->e:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225625
    const-string v2, "analytics_tag"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->f:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225626
    const-string v2, "size_bytes"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->g:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225627
    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->q:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225628
    const-string v2, "random_write_calling_class"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->o:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225629
    const-string v2, "random_write_analytics_tag"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->p:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225630
    const-string v2, "random_write_size_bytes"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->q:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225631
    :cond_0
    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->t:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 225632
    const-string v2, "random_evict_calling_class"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->r:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225633
    const-string v2, "random_evict_analytics_tag"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->s:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225634
    const-string v2, "random_evict_size_bytes"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->t:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225635
    :cond_1
    const-string v2, "evicted_count"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->k:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225636
    const-string v2, "evicted_size"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->l:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225637
    const-string v2, "written_count"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->m:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225638
    const-string v2, "written_size"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->n:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225639
    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->i:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 225640
    const-string v2, "eviction_reason"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->j:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225641
    const-string v2, "eviction_time"

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->i:LX/0Tn;

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->h:LX/0Tn;

    invoke-interface {v3, v6, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 225642
    :cond_2
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 225643
    :cond_3
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->c:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 225644
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 225645
    return-void
.end method

.method private h(LX/1gC;)V
    .locals 4

    .prologue
    .line 225613
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 225614
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->x:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    rem-int/lit8 v0, v0, 0x64

    if-eqz v0, :cond_1

    .line 225615
    :goto_1
    return-void

    .line 225616
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 225617
    :cond_1
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->m(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 225618
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->e:LX/0Tn;

    .line 225619
    iget-object v3, v0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 225620
    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->f:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->h:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->w:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->g:LX/0Tn;

    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 225621
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    goto :goto_1
.end method

.method public static m(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2

    .prologue
    .line 225606
    invoke-interface {p0}, LX/1gC;->a()LX/1bh;

    move-result-object v0

    .line 225607
    instance-of v1, v0, LX/1ec;

    if-eqz v1, :cond_0

    .line 225608
    check-cast v0, LX/1ec;

    .line 225609
    iget-object v1, v0, LX/1ec;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 225610
    instance-of v1, v0, Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v1, :cond_0

    .line 225611
    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 225612
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 225562
    return-void
.end method

.method public final a(LX/1gC;)V
    .locals 0

    .prologue
    .line 225605
    return-void
.end method

.method public final b(LX/1gC;)V
    .locals 0

    .prologue
    .line 225604
    return-void
.end method

.method public final c(LX/1gC;)V
    .locals 0

    .prologue
    .line 225603
    return-void
.end method

.method public final d(LX/1gC;)V
    .locals 14

    .prologue
    .line 225584
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 225585
    :cond_0
    :goto_0
    return-void

    .line 225586
    :cond_1
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 225587
    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->h(LX/1gC;)V

    goto :goto_0

    .line 225588
    :cond_2
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->w:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->h:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 225589
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->b()V

    .line 225590
    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->h(LX/1gC;)V

    goto :goto_0

    .line 225591
    :cond_3
    const-wide/16 v12, 0x0

    .line 225592
    iget-object v6, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v6

    invoke-static {v6}, LX/0PB;->checkState(Z)V

    .line 225593
    iget-object v6, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->m:LX/0Tn;

    iget-object v8, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->m:LX/0Tn;

    invoke-interface {v8, v9, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    invoke-interface {v6, v7, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    sget-object v7, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->n:LX/0Tn;

    iget-object v8, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->n:LX/0Tn;

    invoke-interface {v8, v9, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v10

    add-long/2addr v8, v10

    invoke-interface {v6, v7, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 225594
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->q:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 225595
    iget-object v6, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v6

    invoke-static {v6}, LX/0PB;->checkState(Z)V

    .line 225596
    iget-object v6, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->x:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextInt()I

    move-result v6

    rem-int/lit16 v6, v6, 0xbb8

    if-eqz v6, :cond_4

    .line 225597
    :goto_1
    goto/16 :goto_0

    .line 225598
    :cond_4
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->m(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    .line 225599
    iget-object v7, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v8, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->o:LX/0Tn;

    .line 225600
    iget-object v9, v6, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v9, v9

    .line 225601
    invoke-interface {v7, v8, v9}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v7

    sget-object v8, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->p:LX/0Tn;

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v7, v8, v6}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v6

    sget-object v7, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->q:LX/0Tn;

    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 225602
    iget-object v6, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->q:LX/0Tn;

    invoke-interface {v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v6

    invoke-static {v6}, LX/0PB;->checkState(Z)V

    goto :goto_1
.end method

.method public final e(LX/1gC;)V
    .locals 0

    .prologue
    .line 225583
    return-void
.end method

.method public final f(LX/1gC;)V
    .locals 0

    .prologue
    .line 225582
    return-void
.end method

.method public final g(LX/1gC;)V
    .locals 11

    .prologue
    .line 225563
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 225564
    :cond_0
    :goto_0
    return-void

    .line 225565
    :cond_1
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225566
    invoke-interface {p1}, LX/1gC;->g()LX/37E;

    move-result-object v0

    .line 225567
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 225568
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->j:LX/0Tn;

    invoke-virtual {v0}, LX/37E;->name()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->i:LX/0Tn;

    iget-object v5, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->w:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 225569
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->b()V

    goto :goto_0

    .line 225570
    :cond_2
    const-wide/16 v9, 0x0

    .line 225571
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 225572
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->k:LX/0Tn;

    iget-object v5, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->k:LX/0Tn;

    invoke-interface {v5, v6, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->l:LX/0Tn;

    iget-object v5, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->l:LX/0Tn;

    invoke-interface {v5, v6, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v7

    add-long/2addr v5, v7

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 225573
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->t:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 225574
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->d:LX/0Tn;

    invoke-interface {v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 225575
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->x:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    rem-int/lit16 v3, v3, 0xbb8

    if-eqz v3, :cond_3

    .line 225576
    :goto_1
    goto/16 :goto_0

    .line 225577
    :cond_3
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->m(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 225578
    iget-object v4, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->r:LX/0Tn;

    .line 225579
    iget-object v6, v3, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v6, v6

    .line 225580
    invoke-interface {v4, v5, v6}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v4

    sget-object v5, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->s:LX/0Tn;

    invoke-virtual {v3}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->t:LX/0Tn;

    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 225581
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/imagepipeline/instrumentation/CacheEntryLifetimeTracker;->t:LX/0Tn;

    invoke-interface {v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkState(Z)V

    goto :goto_1
.end method
