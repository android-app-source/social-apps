.class public Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1BU;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/1Ie;

.field private static volatile k:Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;


# instance fields
.field public final a:LX/1If;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1If",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/11i;

.field private final f:LX/0So;

.field private final g:Lcom/facebook/common/perftest/PerfTestConfig;

.field public final h:LX/0Zm;

.field private final i:LX/0kb;

.field public final j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 229117
    const-string v0, "cancelled"

    const-string v1, "true"

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->b:LX/0P1;

    .line 229118
    const-string v0, "failed"

    const-string v1, "false"

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->c:LX/0P1;

    .line 229119
    new-instance v0, LX/1Ie;

    invoke-direct {v0}, LX/1Ie;-><init>()V

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    return-void
.end method

.method public constructor <init>(LX/11i;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Zm;LX/0So;LX/0W3;LX/0kb;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229108
    new-instance v0, LX/1If;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-direct {v0, v1}, LX/1If;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    move-object v0, v0

    .line 229109
    iput-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a:LX/1If;

    .line 229110
    iput-object p1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    .line 229111
    iput-object p2, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->g:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 229112
    iput-object p3, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->h:LX/0Zm;

    .line 229113
    iput-object p4, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    .line 229114
    iput-object p6, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->i:LX/0kb;

    .line 229115
    sget-wide v0, LX/0X5;->fl:J

    const/16 v2, 0x97

    invoke-interface {p5, v0, v1, v2}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->j:I

    .line 229116
    return-void
.end method

.method private static a(Ljava/util/Map;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229106
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;
    .locals 10

    .prologue
    .line 229093
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->k:Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;

    if-nez v0, :cond_1

    .line 229094
    const-class v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;

    monitor-enter v1

    .line 229095
    :try_start_0
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->k:Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 229096
    if-eqz v2, :cond_0

    .line 229097
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 229098
    new-instance v3, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v5

    check-cast v5, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v6

    check-cast v6, LX/0Zm;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v9

    check-cast v9, LX/0kb;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;-><init>(LX/11i;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Zm;LX/0So;LX/0W3;LX/0kb;)V

    .line 229099
    move-object v0, v3

    .line 229100
    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->k:Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229101
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 229102
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 229103
    :cond_1
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->k:Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;

    return-object v0

    .line 229104
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 229105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/11o;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 229080
    const-string v0, "request_lifetime"

    invoke-interface {p1, v0}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a:LX/1If;

    .line 229081
    iget-object v6, v0, LX/1If;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/atomic/AtomicLong;

    .line 229082
    if-nez v6, :cond_1

    const-wide/16 v6, 0x0

    :goto_0
    move-wide v0, v6

    .line 229083
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 229084
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a:LX/1If;

    const-wide/16 v8, 0x0

    .line 229085
    iget-object v6, v0, LX/1If;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/atomic/AtomicLong;

    .line 229086
    if-nez v6, :cond_2

    .line 229087
    :goto_1
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 229088
    :cond_0
    return-void

    :cond_1
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    goto :goto_0

    .line 229089
    :cond_2
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    .line 229090
    cmp-long v7, v10, v8

    if-eqz v7, :cond_3

    invoke-virtual {v6, v10, v11, v8, v9}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 229091
    :cond_3
    iget-object v7, v0, LX/1If;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, p2, v6}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 229092
    goto :goto_1
.end method

.method private c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 229074
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->o:Z

    move v0, v0

    .line 229075
    if-nez v0, :cond_0

    .line 229076
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->h:LX/0Zm;

    invoke-virtual {v0}, LX/0Zm;->c()Z

    move-result v0

    move v0, v0

    .line 229077
    if-eqz v0, :cond_1

    .line 229078
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->j:I

    rem-int/2addr v0, v1

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 229079
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1bf;Ljava/lang/Object;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 229062
    invoke-direct {p0, p3}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 229063
    :cond_0
    :goto_0
    return-void

    .line 229064
    :cond_1
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 229065
    instance-of v0, p2, Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/facebook/common/callercontext/CallerContext;

    .line 229066
    :goto_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "analyticsTag"

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "callerContextClass"

    .line 229067
    iget-object v2, p2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 229068
    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "callerContextTag"

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "isPrefetch"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "networkType"

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->i:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "networkSubtype"

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->i:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "moduleAnalyticsTag"

    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    .line 229069
    :goto_2
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object v2, p3

    invoke-interface/range {v0 .. v5}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    .line 229070
    const-string v1, "request_lifetime"

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x1a96d99e

    move-object v2, v7

    move-object v3, v7

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229071
    if-nez p4, :cond_0

    .line 229072
    const-string v1, "grey_box_time"

    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0xe26af5a

    move-object v2, v7

    move-object v3, v7

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto/16 :goto_0

    .line 229073
    :cond_2
    sget-object p2, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    goto/16 :goto_1

    :cond_3
    move-object v3, v7

    goto :goto_2
.end method

.method public final a(LX/1bf;Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 229053
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v8

    .line 229054
    if-eqz v8, :cond_1

    .line 229055
    const-string v0, "grey_box_time"

    invoke-interface {v8, v0}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229056
    const-string v0, "grey_box_time"

    const v1, -0x3e756567

    invoke-static {v8, v0, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 229057
    :cond_0
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 229058
    const-string v0, "failed"

    const-string v1, "true"

    const-string v2, "exception"

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cause"

    invoke-virtual {p3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    .line 229059
    :goto_0
    const-string v1, "request_lifetime"

    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x2ab1f7e0

    move-object v0, v8

    move-object v2, v7

    invoke-static/range {v0 .. v6}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229060
    invoke-direct {p0, v8, p2}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(LX/11o;Ljava/lang/String;)V

    .line 229061
    :cond_1
    return-void

    :cond_2
    move-object v3, v7

    goto :goto_0
.end method

.method public final a(LX/1bf;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 229002
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 229003
    if-eqz v0, :cond_1

    .line 229004
    const-string v1, "grey_box_time"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229005
    const-string v1, "grey_box_time"

    const v2, -0xfa757e5

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 229006
    :cond_0
    const-string v1, "request_lifetime"

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->c:LX/0P1;

    iget-object v4, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x5e387657

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229007
    invoke-direct {p0, v0, p2}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(LX/11o;Ljava/lang/String;)V

    .line 229008
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 229046
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 229047
    if-eqz v0, :cond_1

    .line 229048
    const-string v1, "grey_box_time"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229049
    const-string v1, "grey_box_time"

    const v2, 0x12d53237

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 229050
    :cond_0
    const-string v1, "request_lifetime"

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->b:LX/0P1;

    iget-object v4, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x2ca28c96

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229051
    invoke-direct {p0, v0, p1}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(LX/11o;Ljava/lang/String;)V

    .line 229052
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 229040
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 229041
    if-eqz v0, :cond_0

    .line 229042
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x4348fa5e

    move-object v1, p2

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229043
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a:LX/1If;

    .line 229044
    const-wide/16 v7, 0x1

    invoke-static {v0, p1, v7, v8}, LX/1If;->a(LX/1If;Ljava/lang/Object;J)J

    .line 229045
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 229034
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 229035
    if-eqz v0, :cond_0

    .line 229036
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 229037
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229038
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x3d57baca

    invoke-static {v0, v1, v2}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 229039
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V
    .locals 7
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 229024
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 229025
    if-eqz v0, :cond_0

    .line 229026
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 229027
    if-nez p4, :cond_1

    .line 229028
    const-string v1, "cause"

    invoke-virtual {p3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    .line 229029
    :goto_0
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x3d9ef060

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229030
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a:LX/1If;

    invoke-virtual {v1, p1}, LX/1If;->c(Ljava/lang/Object;)J

    .line 229031
    invoke-direct {p0, v0, p1}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(LX/11o;Ljava/lang/String;)V

    .line 229032
    :cond_0
    return-void

    .line 229033
    :cond_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v1

    const-string v3, "cause"

    invoke-virtual {p3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    goto :goto_0

    :cond_2
    move-object v3, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229017
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 229018
    if-eqz v0, :cond_0

    .line 229019
    invoke-static {p3}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    .line 229020
    const/4 v2, 0x0

    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x64dc1e62

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229021
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a:LX/1If;

    invoke-virtual {v1, p1}, LX/1If;->c(Ljava/lang/Object;)J

    .line 229022
    invoke-direct {p0, v0, p1}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(LX/11o;Ljava/lang/String;)V

    .line 229023
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229010
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->e:LX/11i;

    sget-object v1, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->d:LX/1Ie;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 229011
    if-eqz v0, :cond_0

    .line 229012
    invoke-static {p3}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    .line 229013
    const/4 v2, 0x0

    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    const v6, -0x3d2affbf

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 229014
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a:LX/1If;

    invoke-virtual {v1, p1}, LX/1If;->c(Ljava/lang/Object;)J

    .line 229015
    invoke-direct {p0, v0, p1}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->a(LX/11o;Ljava/lang/String;)V

    .line 229016
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 229009
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/imagepipeline/instrumentation/RequestPerfLoggingListener;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
