.class public Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0kb;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213091
    iput-object p1, p0, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->a:LX/0Zb;

    .line 213092
    iput-object p2, p0, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->b:LX/0kb;

    .line 213093
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;
    .locals 3

    .prologue
    .line 213094
    new-instance v2, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v1

    check-cast v1, LX/0kb;

    invoke-direct {v2, v0, v1}, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;-><init>(LX/0Zb;LX/0kb;)V

    .line 213095
    return-object v2
.end method


# virtual methods
.method public final a(LX/2xo;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 213096
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->a:LX/0Zb;

    const-string v3, "fresco_percent_photos_rendered"

    invoke-interface {v0, v3, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 213097
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213098
    :goto_0
    return-void

    .line 213099
    :cond_0
    invoke-virtual {p1}, LX/2xo;->a()I

    move-result v4

    .line 213100
    const-string v5, "rendered"

    if-ne v4, v1, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {v3, v5, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 213101
    const-string v5, "not_rendered"

    const/4 v0, 0x2

    if-ne v4, v0, :cond_8

    move v0, v1

    :goto_2
    invoke-virtual {v3, v5, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 213102
    const-string v0, "ignored"

    const/4 v5, 0x3

    if-ne v4, v5, :cond_9

    :goto_3
    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 213103
    const-string v0, "network_type"

    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213104
    const-string v0, "network_subtype"

    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213105
    iget-object v0, p1, LX/2xo;->n:Landroid/util/Pair;

    move-object v0, v0

    .line 213106
    if-eqz v0, :cond_1

    .line 213107
    const-string v1, "bitmap_size_width"

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 213108
    const-string v1, "bitmap_size_height"

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v3, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 213109
    :cond_1
    iget-object v0, p1, LX/2xo;->j:Ljava/lang/Boolean;

    move-object v0, v0

    .line 213110
    if-eqz v0, :cond_2

    .line 213111
    const-string v0, "bitmap_cache_hit"

    .line 213112
    iget-object v1, p1, LX/2xo;->j:Ljava/lang/Boolean;

    move-object v1, v1

    .line 213113
    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 213114
    :cond_2
    iget-object v0, p1, LX/2xo;->k:Ljava/lang/Boolean;

    move-object v0, v0

    .line 213115
    if-eqz v0, :cond_3

    .line 213116
    const-string v0, "encoded_cache_hit"

    .line 213117
    iget-object v1, p1, LX/2xo;->k:Ljava/lang/Boolean;

    move-object v1, v1

    .line 213118
    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 213119
    :cond_3
    iget-object v0, p1, LX/2xo;->l:Ljava/lang/Boolean;

    move-object v0, v0

    .line 213120
    if-eqz v0, :cond_4

    .line 213121
    const-string v0, "disk_cache_hit"

    .line 213122
    iget-object v1, p1, LX/2xo;->l:Ljava/lang/Boolean;

    move-object v1, v1

    .line 213123
    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 213124
    :cond_4
    const-string v0, "failure_message"

    .line 213125
    iget-object v1, p1, LX/2xo;->i:Ljava/lang/String;

    move-object v1, v1

    .line 213126
    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213127
    iget-object v0, p1, LX/2xo;->m:Landroid/net/Uri;

    move-object v0, v0

    .line 213128
    if-eqz v0, :cond_5

    .line 213129
    const-string v1, "uri_scheme"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213130
    :cond_5
    iget-object v0, p1, LX/2xo;->h:Ljava/lang/Object;

    move-object v0, v0

    .line 213131
    instance-of v0, v0, Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v0, :cond_6

    .line 213132
    iget-object v0, p1, LX/2xo;->h:Ljava/lang/Object;

    move-object v0, v0

    .line 213133
    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 213134
    const-string v1, "analytics_tag"

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213135
    const-string v1, "calling_class"

    .line 213136
    iget-object v2, v0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 213137
    invoke-virtual {v3, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213138
    const-string v1, "feature_tag"

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213139
    const-string v1, "module_tag"

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 213140
    :cond_6
    invoke-virtual {v3}, LX/0oG;->d()V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 213141
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 213142
    goto/16 :goto_2

    :cond_9
    move v1, v2

    .line 213143
    goto/16 :goto_3
.end method
