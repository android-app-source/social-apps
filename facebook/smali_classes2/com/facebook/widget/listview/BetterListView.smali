.class public Lcom/facebook/widget/listview/BetterListView;
.super Landroid/widget/ListView;
.source ""

# interfaces
.implements LX/0h0;
.implements LX/1Zi;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/2hv;

.field private c:LX/2hz;

.field private d:Landroid/view/MotionEvent;

.field private e:LX/0Sy;

.field private f:LX/0So;

.field private g:Landroid/widget/AbsListView$OnScrollListener;

.field private h:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field private i:Ljava/lang/Runnable;

.field public j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:LX/2hy;

.field private p:Z

.field private q:LX/1OB;

.field public r:LX/2i5;

.field private s:J

.field private t:LX/2hu;

.field private u:I

.field public v:Z

.field private w:Z

.field private x:Z

.field public y:LX/2i6;

.field public z:LX/2i9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 275161
    const-class v0, Lcom/facebook/widget/listview/BetterListView;

    sput-object v0, Lcom/facebook/widget/listview/BetterListView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 275155
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 275156
    iput v1, p0, Lcom/facebook/widget/listview/BetterListView;->j:I

    .line 275157
    iput-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->k:Z

    .line 275158
    iput-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->p:Z

    .line 275159
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 275160
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 275149
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 275150
    iput v0, p0, Lcom/facebook/widget/listview/BetterListView;->j:I

    .line 275151
    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->k:Z

    .line 275152
    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->p:Z

    .line 275153
    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 275154
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 275143
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 275144
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/listview/BetterListView;->j:I

    .line 275145
    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->k:Z

    .line 275146
    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->p:Z

    .line 275147
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 275148
    return-void
.end method

.method private static a(Landroid/widget/ListAdapter;)LX/2ht;
    .locals 3

    .prologue
    .line 275135
    if-nez p0, :cond_0

    .line 275136
    const/4 v0, 0x0

    .line 275137
    :goto_0
    return-object v0

    .line 275138
    :cond_0
    instance-of v0, p0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v0, :cond_2

    .line 275139
    check-cast p0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 275140
    :goto_1
    instance-of v1, v0, LX/2ht;

    if-nez v1, :cond_1

    .line 275141
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " must implement StickyHeaderAdapter"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 275142
    :cond_1
    check-cast v0, LX/2ht;

    goto :goto_0

    :cond_2
    move-object v0, p0

    goto :goto_1
.end method

.method private a(LX/0Sy;LX/0So;LX/2hu;LX/2hv;)V
    .locals 0
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedAwakeTimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 275130
    iput-object p1, p0, Lcom/facebook/widget/listview/BetterListView;->e:LX/0Sy;

    .line 275131
    iput-object p2, p0, Lcom/facebook/widget/listview/BetterListView;->f:LX/0So;

    .line 275132
    iput-object p3, p0, Lcom/facebook/widget/listview/BetterListView;->t:LX/2hu;

    .line 275133
    iput-object p4, p0, Lcom/facebook/widget/listview/BetterListView;->b:LX/2hv;

    .line 275134
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 275118
    const-class v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-static {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 275119
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->b:LX/2hv;

    invoke-super {p0, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 275120
    new-instance v0, LX/2hw;

    invoke-direct {v0, p0}, LX/2hw;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    .line 275121
    new-instance v0, Lcom/facebook/widget/listview/BetterListView$2;

    invoke-direct {v0, p0}, Lcom/facebook/widget/listview/BetterListView$2;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->i:Ljava/lang/Runnable;

    .line 275122
    new-instance v0, LX/2hx;

    invoke-direct {v0, p0}, LX/2hx;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->h:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 275123
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->q:LX/1OB;

    .line 275124
    sget-object v0, LX/03r;->BetterListView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 275125
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->w:Z

    .line 275126
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 275127
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 275128
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->setNestedScrollingEnabled(Z)V

    .line 275129
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/listview/BetterListView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/widget/listview/BetterListView;

    invoke-static {v3}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v0

    check-cast v0, LX/0Sy;

    invoke-static {v3}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    const-class v2, LX/2hu;

    invoke-interface {v3, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/2hu;

    invoke-static {v3}, LX/2hv;->a(LX/0QB;)LX/2hv;

    move-result-object v3

    check-cast v3, LX/2hv;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0Sy;LX/0So;LX/2hu;LX/2hv;)V

    return-void
.end method

.method private b(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1    # Landroid/widget/ListAdapter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 275052
    if-nez p1, :cond_1

    .line 275053
    :cond_0
    :goto_0
    return-void

    .line 275054
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->o:LX/2hy;

    if-nez v0, :cond_0

    .line 275055
    new-instance v0, LX/2hy;

    invoke-direct {v0, p0}, LX/2hy;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->o:LX/2hy;

    .line 275056
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->o:LX/2hy;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/widget/listview/BetterListView;I)V
    .locals 4

    .prologue
    .line 275045
    iget v0, p0, Lcom/facebook/widget/listview/BetterListView;->j:I

    if-eq p1, v0, :cond_0

    .line 275046
    iput p1, p0, Lcom/facebook/widget/listview/BetterListView;->j:I

    .line 275047
    if-nez p1, :cond_1

    .line 275048
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->e:LX/0Sy;

    invoke-virtual {v0, p0}, LX/0Sy;->b(Landroid/view/View;)V

    .line 275049
    :cond_0
    :goto_0
    return-void

    .line 275050
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->e:LX/0Sy;

    invoke-virtual {v0, p0}, LX/0Sy;->a(Landroid/view/View;)V

    .line 275051
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/widget/listview/BetterListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private c(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 275044
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1    # Landroid/widget/ListAdapter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 275040
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->o:LX/2hy;

    if-eqz v0, :cond_0

    .line 275041
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->o:LX/2hy;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 275042
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->o:LX/2hy;

    .line 275043
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/widget/listview/BetterListView;)V
    .locals 2

    .prologue
    .line 275038
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/widget/listview/BetterListView;->s:J

    .line 275039
    return-void
.end method

.method public static f(Lcom/facebook/widget/listview/BetterListView;)V
    .locals 6

    .prologue
    .line 275030
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 275031
    iget v2, p0, Lcom/facebook/widget/listview/BetterListView;->j:I

    if-eqz v2, :cond_0

    .line 275032
    iget-wide v2, p0, Lcom/facebook/widget/listview/BetterListView;->s:J

    const-wide/16 v4, 0xbb8

    add-long/2addr v2, v4

    .line 275033
    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    .line 275034
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->b(Lcom/facebook/widget/listview/BetterListView;I)V

    .line 275035
    :cond_0
    :goto_0
    return-void

    .line 275036
    :cond_1
    sub-long v0, v2, v0

    .line 275037
    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->i:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/widget/listview/BetterListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static g(Lcom/facebook/widget/listview/BetterListView;)V
    .locals 6

    .prologue
    .line 275021
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 275022
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->c:LX/2hz;

    iget-object v1, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/facebook/widget/listview/BetterListView;->getNextEstimatedDrawTime()J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, LX/2hz;->a(Landroid/view/MotionEvent;IJ)Landroid/view/MotionEvent;

    move-result-object v0

    .line 275023
    iget-object v1, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 275024
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    .line 275025
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->n:Z

    .line 275026
    invoke-super {p0, v0}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 275027
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->n:Z

    .line 275028
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 275029
    :cond_0
    return-void
.end method

.method private getListViewLayoutModeDebugString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 275243
    const/4 v0, -0x1

    .line 275244
    :try_start_0
    sget-object v1, LX/2i0;->a:Ljava/lang/reflect/Field;

    if-nez v1, :cond_0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275245
    :goto_0
    move v0, v0

    .line 275246
    packed-switch v0, :pswitch_data_0

    .line 275247
    const-string v1, "unknown"

    :goto_1
    move-object v0, v1

    .line 275248
    return-object v0

    .line 275249
    :cond_0
    :try_start_1
    sget-object v1, LX/2i0;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0

    .line 275250
    :catch_0
    goto :goto_0

    .line 275251
    :pswitch_0
    const-string v1, "LAYOUT_NORMAL"

    goto :goto_1

    .line 275252
    :pswitch_1
    const-string v1, "LAYOUT_FORCE_TOP"

    goto :goto_1

    .line 275253
    :pswitch_2
    const-string v1, "LAYOUT_SET_SELECTION"

    goto :goto_1

    .line 275254
    :pswitch_3
    const-string v1, "LAYOUT_FORCE_BOTTOM"

    goto :goto_1

    .line 275255
    :pswitch_4
    const-string v1, "LAYOUT_SPECIFIC"

    goto :goto_1

    .line 275256
    :pswitch_5
    const-string v1, "LAYOUT_SYNC"

    goto :goto_1

    .line 275257
    :pswitch_6
    const-string v1, "LAYOUT_MOVE_SELECTION"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private getNextEstimatedDrawTime()J
    .locals 2

    .prologue
    .line 275162
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private getOffsetsOfVisibleItemsWhenInSync()LX/0P1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275229
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->v:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 275230
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 275231
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v1

    .line 275232
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v3

    .line 275233
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    move v0, v1

    .line 275234
    :goto_0
    if-gt v0, v3, :cond_1

    .line 275235
    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 275236
    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v6

    .line 275237
    const-wide/high16 v8, -0x8000000000000000L

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 275238
    sub-int v5, v0, v1

    invoke-virtual {p0, v5}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 275239
    if-eqz v5, :cond_0

    .line 275240
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275241
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275242
    :cond_1
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private getOffsetsOfVisibleItemsWhenNotInSync()LX/0P1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 275212
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->v:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 275213
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 275214
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 275215
    invoke-virtual {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 275216
    if-eqz v3, :cond_1

    .line 275217
    move-object v0, v3

    :goto_2
    if-eqz v0, :cond_0

    instance-of v4, v0, LX/2i1;

    if-eqz v4, :cond_0

    .line 275218
    check-cast v0, LX/2i1;

    invoke-interface {v0}, LX/2i1;->getWrappedView()Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 275219
    :cond_0
    instance-of v4, v0, LX/2i2;

    if-eqz v4, :cond_4

    .line 275220
    check-cast v0, LX/2i2;

    invoke-interface {v0}, LX/2i2;->a()Ljava/lang/Object;

    move-result-object v0

    .line 275221
    :goto_3
    move-object v0, v0

    .line 275222
    if-eqz v0, :cond_1

    instance-of v4, v0, LX/2i3;

    if-eqz v4, :cond_1

    .line 275223
    check-cast v0, LX/2i3;

    invoke-interface {v0}, LX/2i3;->a()J

    move-result-wide v4

    .line 275224
    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    .line 275225
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275226
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 275227
    goto :goto_0

    .line 275228
    :cond_3
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 275204
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 275205
    if-nez v1, :cond_1

    .line 275206
    :cond_0
    :goto_0
    return-object v0

    .line 275207
    :cond_1
    if-ltz p1, :cond_0

    .line 275208
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v1, p1

    .line 275209
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getFooterViewsCount()I

    move-result v3

    sub-int/2addr v2, v3

    .line 275210
    if-ge v1, v2, :cond_0

    .line 275211
    invoke-direct {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->c(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0fu;)V
    .locals 1

    .prologue
    .line 275202
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->q:LX/1OB;

    invoke-virtual {v0, p1}, LX/1OB;->a(LX/0fu;)V

    .line 275203
    return-void
.end method

.method public final a(LX/2i4;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 275189
    invoke-virtual {p0, v5}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 275190
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 275191
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v1

    .line 275192
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    neg-int v3, v2

    .line 275193
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 275194
    :goto_2
    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v4

    .line 275195
    :goto_3
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v5

    :cond_0
    move-object v0, p1

    .line 275196
    invoke-virtual/range {v0 .. v5}, LX/2i4;->a(IIIII)V

    .line 275197
    return-void

    :cond_1
    move v1, v5

    .line 275198
    goto :goto_0

    :cond_2
    move v3, v5

    .line 275199
    goto :goto_1

    :cond_3
    move v2, v5

    .line 275200
    goto :goto_2

    :cond_4
    move v4, v5

    .line 275201
    goto :goto_3
.end method

.method public final a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 275187
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->b:LX/2hv;

    invoke-virtual {v0, p1}, LX/2hv;->b(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 275188
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 275186
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public addHeaderView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 275184
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 275185
    return-void
.end method

.method public addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 275180
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 275181
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Phones up until 4.4 may crash if addHeaderView is called after setAdapter.  Keep the header permanently added and use visibility instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275182
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 275183
    return-void
.end method

.method public final asViewGroup()Landroid/view/ViewGroup;
    .locals 0

    .prologue
    .line 275179
    return-object p0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 275177
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->setOverScrollMode(I)V

    .line 275178
    return-void
.end method

.method public final b(LX/2i4;)V
    .locals 2

    .prologue
    .line 274944
    iget v0, p1, LX/2i4;->a:I

    move v0, v0

    .line 274945
    iget v1, p1, LX/2i4;->c:I

    move v1, v1

    .line 274946
    neg-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setSelectionFromTop(II)V

    .line 274947
    return-void
.end method

.method public final b(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 275174
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->b:LX/2hv;

    .line 275175
    iget-object p0, v0, LX/2hv;->a:LX/01J;

    invoke-virtual {p0, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275176
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 275172
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->q:LX/1OB;

    invoke-virtual {v0}, LX/1OB;->b()V

    .line 275173
    return-void
.end method

.method public final canScrollVertically(I)Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ImprovedNewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 275166
    if-gez p1, :cond_2

    .line 275167
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getClipToPadding()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 275168
    :goto_0
    invoke-virtual {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-ge v2, v0, :cond_0

    const/4 v1, 0x1

    .line 275169
    :cond_0
    :goto_1
    return v1

    .line 275170
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingTop()I

    move-result v0

    goto :goto_0

    .line 275171
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ListView;->canScrollVertically(I)Z

    move-result v1

    goto :goto_1
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 275057
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 275058
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    if-eqz v0, :cond_0

    .line 275059
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v10, 0x41200000    # 10.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 275060
    iget-object v1, v0, LX/2i5;->c:LX/2ht;

    if-nez v1, :cond_1

    .line 275061
    :cond_0
    :goto_0
    return-void

    .line 275062
    :cond_1
    iget-object v1, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v3

    .line 275063
    if-ltz v3, :cond_0

    iget-object v1, v0, LX/2i5;->c:LX/2ht;

    invoke-interface {v1}, LX/2ht;->getCount()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 275064
    iget-object v1, v0, LX/2i5;->c:LX/2ht;

    invoke-interface {v1, v3}, LX/2ht;->o_(I)I

    move-result v4

    .line 275065
    const/4 v1, 0x0

    .line 275066
    iget v5, v0, LX/2i5;->f:I

    if-ne v5, v4, :cond_3

    .line 275067
    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    .line 275068
    :goto_1
    iget-object v4, v0, LX/2i5;->c:LX/2ht;

    iget-object v5, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-interface {v4, v3, v1, v5}, LX/2ht;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, LX/2i5;->e:Landroid/view/View;

    .line 275069
    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 275070
    iget-object v1, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 275071
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_2

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v4, 0x400000

    and-int/2addr v1, v4

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    iget-object v4, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getLayoutDirection()I

    move-result v4

    if-eq v1, v4, :cond_2

    .line 275072
    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    iget-object v4, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getLayoutDirection()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setLayoutDirection(I)V

    .line 275073
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v9

    .line 275074
    iget-object v1, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getWidth()I

    move-result v1

    iget-object v4, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v1, v4

    iget-object v4, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v1, v4

    .line 275075
    iget-object v4, v0, LX/2i5;->c:LX/2ht;

    invoke-interface {v4, v3}, LX/2ht;->e(I)I

    move-result v4

    .line 275076
    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 275077
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 275078
    iget-object v7, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v7, v5, v6}, Landroid/view/View;->measure(II)V

    .line 275079
    iget-object v5, v0, LX/2i5;->e:Landroid/view/View;

    iget-object v6, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v6}, Lcom/facebook/widget/listview/BetterListView;->getPaddingLeft()I

    move-result v6

    iget-object v7, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v7}, Lcom/facebook/widget/listview/BetterListView;->getPaddingTop()I

    move-result v7

    invoke-virtual {v5, v6, v7, v1, v4}, Landroid/view/View;->layout(IIII)V

    .line 275080
    iget-object v1, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 275081
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0

    .line 275082
    :cond_3
    iput v4, v0, LX/2i5;->f:I

    goto/16 :goto_1

    .line 275083
    :cond_4
    const/4 v5, 0x0

    const/4 v1, -0x1

    .line 275084
    iget-object v6, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v6, v5}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 275085
    if-nez v6, :cond_b

    .line 275086
    :cond_5
    :goto_2
    move v5, v1

    .line 275087
    const/4 v1, -0x1

    if-eq v5, v1, :cond_6

    iget-object v1, v0, LX/2i5;->c:LX/2ht;

    invoke-interface {v1}, LX/2ht;->getCount()I

    move-result v1

    if-lt v5, v1, :cond_7

    .line 275088
    :cond_6
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0

    .line 275089
    :cond_7
    const/4 v1, 0x0

    .line 275090
    if-ltz v5, :cond_a

    iget-object v6, v0, LX/2i5;->c:LX/2ht;

    invoke-interface {v6, v5}, LX/2ht;->f(I)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 275091
    sub-int v1, v5, v3

    .line 275092
    iget-object v3, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3, v1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 275093
    if-nez v1, :cond_8

    .line 275094
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0

    .line 275095
    :cond_8
    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v3, v1

    .line 275096
    sub-int v1, v3, v4

    .line 275097
    int-to-float v3, v3

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 275098
    mul-float/2addr v3, v3

    .line 275099
    mul-float/2addr v3, v10

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v10

    move v7, v3

    .line 275100
    :goto_3
    iget-object v3, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3}, Lcom/facebook/widget/listview/BetterListView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    int-to-float v1, v1

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 275101
    iget-object v1, v0, LX/2i5;->b:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->reset()V

    .line 275102
    iget-object v1, v0, LX/2i5;->b:Landroid/graphics/Paint;

    iget-object v3, v0, LX/2i5;->c:LX/2ht;

    invoke-interface {v3}, LX/2ht;->d()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 275103
    iget-object v1, v0, LX/2i5;->b:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 275104
    iget-object v1, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getWidth()I

    move-result v1

    int-to-float v4, v1

    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v5, v1

    iget-object v6, v0, LX/2i5;->b:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 275105
    cmpl-float v1, v7, v8

    if-eqz v1, :cond_9

    .line 275106
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 275107
    iget-object v3, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 275108
    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v2, v7

    float-to-int v2, v2

    const/16 v3, 0x1f

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    .line 275109
    :cond_9
    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 275110
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0

    :cond_a
    move v7, v8

    goto :goto_3

    .line 275111
    :cond_b
    iget-object v7, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v7}, Lcom/facebook/widget/listview/BetterListView;->getPaddingTop()I

    move-result v7

    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v6

    float-to-int v6, v6

    add-int/2addr v6, v7

    .line 275112
    iget-object v7, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v7}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v7

    .line 275113
    :goto_4
    if-ge v5, v7, :cond_5

    .line 275114
    iget-object v11, v0, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v11, v5}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v11

    add-int/2addr v6, v11

    .line 275115
    if-lt v6, v4, :cond_c

    .line 275116
    add-int v1, v3, v5

    goto/16 :goto_2

    .line 275117
    :cond_c
    add-int/lit8 v5, v5, 0x1

    goto :goto_4
.end method

.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275163
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->p:Z

    if-eqz v0, :cond_0

    .line 275164
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 275165
    :cond_0
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274854
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->p:Z

    if-eqz v0, :cond_0

    .line 274855
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 274856
    :cond_0
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    .line 274919
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 274920
    iget-object v3, v0, LX/2i5;->e:Landroid/view/View;

    if-nez v3, :cond_3

    .line 274921
    :cond_0
    :goto_0
    move v0, v1

    .line 274922
    if-eqz v0, :cond_1

    .line 274923
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->invalidate()V

    .line 274924
    const/4 v0, 0x1

    .line 274925
    :goto_1
    return v0

    .line 274926
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_2

    .line 274927
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    invoke-interface {v0}, LX/2i6;->a()V

    .line 274928
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 274929
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 274930
    if-nez v3, :cond_4

    .line 274931
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 274932
    iget-object v5, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 274933
    invoke-virtual {v4, v1, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 274934
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 274935
    iput-boolean v2, v0, LX/2i5;->d:Z

    .line 274936
    :cond_4
    iget-boolean v4, v0, LX/2i5;->d:Z

    if-eqz v4, :cond_0

    .line 274937
    if-nez v3, :cond_7

    move v1, v2

    .line 274938
    :cond_5
    :goto_2
    if-eqz v1, :cond_6

    .line 274939
    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 274940
    :cond_6
    iget-object v1, v0, LX/2i5;->e:Landroid/view/View;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 274941
    :cond_7
    if-eq v3, v2, :cond_8

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    .line 274942
    :cond_8
    iput-boolean v1, v0, LX/2i5;->d:Z

    move v1, v2

    .line 274943
    goto :goto_2
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 274916
    invoke-super {p0, p1}, Landroid/widget/ListView;->draw(Landroid/graphics/Canvas;)V

    .line 274917
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->q:LX/1OB;

    invoke-virtual {v0}, LX/1OB;->a()V

    .line 274918
    return-void
.end method

.method public getClipToPadding()Z
    .locals 2

    .prologue
    .line 274913
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 274914
    invoke-super {p0}, Landroid/widget/ListView;->getClipToPadding()Z

    move-result v0

    .line 274915
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->k:Z

    goto :goto_0
.end method

.method public getCurrentScrollState()I
    .locals 1

    .prologue
    .line 274912
    iget v0, p0, Lcom/facebook/widget/listview/BetterListView;->j:I

    return v0
.end method

.method public getOffsetsOfVisibleItems()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274909
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->v:Z

    if-eqz v0, :cond_0

    .line 274910
    invoke-direct {p0}, Lcom/facebook/widget/listview/BetterListView;->getOffsetsOfVisibleItemsWhenInSync()LX/0P1;

    move-result-object v0

    .line 274911
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/listview/BetterListView;->getOffsetsOfVisibleItemsWhenNotInSync()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public getOnScrollListenerProxy()LX/2hv;
    .locals 1

    .prologue
    .line 274908
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->b:LX/2hv;

    return-object v0
.end method

.method public getScrollPosition()LX/2i7;
    .locals 4

    .prologue
    .line 274893
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v0

    .line 274894
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v1

    .line 274895
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->isStackFromBottom()Z

    move-result v2

    .line 274896
    iget v3, p0, Lcom/facebook/widget/listview/BetterListView;->u:I

    .line 274897
    if-eqz v2, :cond_2

    .line 274898
    if-eqz v3, :cond_0

    add-int/lit8 v2, v3, -0x1

    if-ne v1, v2, :cond_1

    .line 274899
    :cond_0
    sget-object v0, LX/2i7;->BOTTOM:LX/2i7;

    .line 274900
    :goto_0
    return-object v0

    .line 274901
    :cond_1
    if-nez v0, :cond_5

    .line 274902
    sget-object v0, LX/2i7;->TOP:LX/2i7;

    goto :goto_0

    .line 274903
    :cond_2
    if-eqz v3, :cond_3

    if-nez v0, :cond_4

    .line 274904
    :cond_3
    sget-object v0, LX/2i7;->TOP:LX/2i7;

    goto :goto_0

    .line 274905
    :cond_4
    add-int/lit8 v0, v3, -0x1

    if-ne v1, v0, :cond_5

    .line 274906
    sget-object v0, LX/2i7;->BOTTOM:LX/2i7;

    goto :goto_0

    .line 274907
    :cond_5
    sget-object v0, LX/2i7;->MIDDLE:LX/2i7;

    goto :goto_0
.end method

.method public getScrollState()LX/2i8;
    .locals 3

    .prologue
    .line 274890
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getScrollPosition()LX/2i7;

    move-result-object v0

    .line 274891
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getOffsetsOfVisibleItems()LX/0P1;

    move-result-object v1

    .line 274892
    new-instance v2, LX/2i8;

    invoke-direct {v2, v0, v1}, LX/2i8;-><init>(LX/2i7;LX/0P1;)V

    return-object v2
.end method

.method public getStickyHeader()LX/2i5;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 274889
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    return-object v0
.end method

.method public final handleDataChanged()V
    .locals 1

    .prologue
    .line 274885
    invoke-super {p0}, Landroid/widget/ListView;->handleDataChanged()V

    .line 274886
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274887
    invoke-direct {p0}, Lcom/facebook/widget/listview/BetterListView;->getListViewLayoutModeDebugString()Ljava/lang/String;

    .line 274888
    :cond_0
    return-void
.end method

.method public isAtBottom()Z
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 274884
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/listview/BetterListView;->u:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getHeight()I

    move-result v1

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAttachedToWindow()Z
    .locals 1

    .prologue
    .line 274875
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->l:Z

    return v0
.end method

.method public final layoutChildren()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 274862
    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274863
    invoke-direct {p0}, Lcom/facebook/widget/listview/BetterListView;->getListViewLayoutModeDebugString()Ljava/lang/String;

    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->x:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 274864
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getScrollPosition()LX/2i7;

    move-result-object v0

    .line 274865
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 274866
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getCount()I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/listview/BetterListView;->u:I

    .line 274867
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->v:Z

    .line 274868
    iget-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->x:Z

    if-eqz v1, :cond_2

    .line 274869
    sget-object v1, LX/2i7;->BOTTOM:LX/2i7;

    if-ne v0, v1, :cond_1

    .line 274870
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->setSelection(I)V

    .line 274871
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->x:Z

    .line 274872
    :cond_2
    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 274873
    invoke-direct {p0}, Lcom/facebook/widget/listview/BetterListView;->getListViewLayoutModeDebugString()Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getScrollPosition()LX/2i7;

    .line 274874
    :cond_3
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6f89cfa0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 274857
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->h:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 274858
    invoke-super {p0}, Landroid/widget/ListView;->onAttachedToWindow()V

    .line 274859
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->l:Z

    .line 274860
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->b(Landroid/widget/ListAdapter;)V

    .line 274861
    const/16 v1, 0x2d

    const v2, -0x49f3b569

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2c

    const v1, 0x7c78e8e5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 274876
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/listview/BetterListView;->m:Z

    .line 274877
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->h:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 274878
    invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V

    .line 274879
    iget-object v1, p0, Lcom/facebook/widget/listview/BetterListView;->e:LX/0Sy;

    invoke-virtual {v1, p0}, LX/0Sy;->b(Landroid/view/View;)V

    .line 274880
    iput-boolean v3, p0, Lcom/facebook/widget/listview/BetterListView;->l:Z

    .line 274881
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/widget/listview/BetterListView;->c(Landroid/widget/ListAdapter;)V

    .line 274882
    iput-boolean v3, p0, Lcom/facebook/widget/listview/BetterListView;->m:Z

    .line 274883
    const/16 v1, 0x2d

    const v2, 0x1cc36ebd

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 274978
    const/4 v0, 0x0

    .line 274979
    iget-object v1, p0, Lcom/facebook/widget/listview/BetterListView;->z:LX/2i9;

    if-eqz v1, :cond_0

    .line 274980
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->z:LX/2i9;

    invoke-interface {v0, p1}, LX/2i9;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 274981
    :cond_0
    if-nez v0, :cond_1

    .line 274982
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 274983
    :cond_1
    return v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x604d2317

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 275019
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    .line 275020
    const/16 v1, 0x2d

    const v2, 0x54429b40

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x6ad0d1dc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 275001
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-gt v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/facebook/widget/listview/BetterListView;->n:Z

    if-eqz v2, :cond_1

    .line 275002
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x37c56981

    invoke-static {v4, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 275003
    :goto_0
    return v0

    .line 275004
    :cond_1
    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->c:LX/2hz;

    if-nez v2, :cond_2

    .line 275005
    new-instance v2, LX/2hz;

    invoke-direct {v2}, LX/2hz;-><init>()V

    iput-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->c:LX/2hz;

    .line 275006
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 275007
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->isClickable()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    :goto_1
    const v2, 0x6332d809

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 275008
    :cond_5
    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->c:LX/2hz;

    invoke-virtual {v2, p1}, LX/2hz;->a(Landroid/view/MotionEvent;)V

    .line 275009
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 275010
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    if-eqz v0, :cond_6

    .line 275011
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 275012
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    .line 275013
    :cond_6
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x5ec90ac3

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 275014
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    if-eqz v2, :cond_7

    .line 275015
    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 275016
    :cond_7
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->d:Landroid/view/MotionEvent;

    .line 275017
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->invalidate()V

    .line 275018
    const v2, -0x75d70d85

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final performItemClick(Landroid/view/View;IJ)Z
    .locals 1

    .prologue
    .line 274998
    iget-boolean v0, p0, Lcom/facebook/widget/listview/BetterListView;->m:Z

    if-nez v0, :cond_0

    .line 274999
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    .line 275000
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final restoreHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274996
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 274997
    return-void
.end method

.method public final saveHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274994
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 274995
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1    # Landroid/widget/Adapter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 274993
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1    # Landroid/widget/ListAdapter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 274984
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 274985
    if-eq v0, p1, :cond_0

    .line 274986
    invoke-direct {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->c(Landroid/widget/ListAdapter;)V

    .line 274987
    invoke-direct {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->b(Landroid/widget/ListAdapter;)V

    .line 274988
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    if-eqz v0, :cond_1

    .line 274989
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    invoke-static {p1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/ListAdapter;)LX/2ht;

    move-result-object v1

    .line 274990
    iput-object v1, v0, LX/2i5;->c:LX/2ht;

    .line 274991
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 274992
    return-void
.end method

.method public setBroadcastInteractionChanges(Z)V
    .locals 1

    .prologue
    .line 274948
    if-eqz p1, :cond_0

    .line 274949
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 274950
    :goto_0
    return-void

    .line 274951
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->g:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->b(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0
.end method

.method public setClipToPadding(Z)V
    .locals 0

    .prologue
    .line 274975
    iput-boolean p1, p0, Lcom/facebook/widget/listview/BetterListView;->k:Z

    .line 274976
    invoke-super {p0, p1}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 274977
    return-void
.end method

.method public setInterceptTouchEventListener(LX/2i9;)V
    .locals 0

    .prologue
    .line 274973
    iput-object p1, p0, Lcom/facebook/widget/listview/BetterListView;->z:LX/2i9;

    .line 274974
    return-void
.end method

.method public setOnDrawListenerTo(LX/0fu;)V
    .locals 1

    .prologue
    .line 274971
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->q:LX/1OB;

    invoke-virtual {v0, p1}, LX/1OB;->b(LX/0fu;)V

    .line 274972
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView$OnScrollListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 274968
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->b:LX/2hv;

    .line 274969
    iput-object p1, v0, LX/2hv;->b:Landroid/widget/AbsListView$OnScrollListener;

    .line 274970
    return-void
.end method

.method public setOnScrollListenerLogging(I)V
    .locals 5

    .prologue
    .line 274963
    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->t:LX/2hu;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/listview/BetterListView;->b:LX/2hv;

    .line 274964
    new-instance p1, LX/2iA;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v4

    check-cast v4, Ljava/util/Random;

    invoke-direct {p1, v3, v4, v1, v2}, LX/2iA;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/util/Random;Ljava/lang/Integer;Landroid/widget/AbsListView$OnScrollListener;)V

    .line 274965
    move-object v0, p1

    .line 274966
    invoke-super {p0, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 274967
    return-void
.end method

.method public setOnTouchDownListener(LX/2i6;)V
    .locals 0

    .prologue
    .line 274961
    iput-object p1, p0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    .line 274962
    return-void
.end method

.method public setSaveFromParentEnabledCompat(Z)V
    .locals 0

    .prologue
    .line 274959
    iput-boolean p1, p0, Lcom/facebook/widget/listview/BetterListView;->p:Z

    .line 274960
    return-void
.end method

.method public setStickyHeaderEnabled(Z)V
    .locals 2

    .prologue
    .line 274952
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    if-nez v0, :cond_1

    .line 274953
    new-instance v0, LX/2i5;

    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/ListAdapter;)LX/2ht;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/2i5;-><init>(Lcom/facebook/widget/listview/BetterListView;LX/2ht;)V

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    .line 274954
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->invalidate()V

    .line 274955
    :cond_0
    :goto_0
    return-void

    .line 274956
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    if-eqz v0, :cond_0

    .line 274957
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    .line 274958
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->invalidate()V

    goto :goto_0
.end method
