.class public Lcom/facebook/widget/CustomLinearLayout;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/0h0;
.implements LX/0h1;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:I

.field private h:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LX/10U;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 114587
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 114588
    iput-object v1, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    .line 114589
    iput-object v1, p0, Lcom/facebook/widget/CustomLinearLayout;->b:Ljava/lang/String;

    .line 114590
    iput-object v1, p0, Lcom/facebook/widget/CustomLinearLayout;->c:Ljava/lang/String;

    .line 114591
    iput-boolean v0, p0, Lcom/facebook/widget/CustomLinearLayout;->e:Z

    .line 114592
    iput-boolean v0, p0, Lcom/facebook/widget/CustomLinearLayout;->f:Z

    .line 114593
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114594
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 114563
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 114564
    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    .line 114565
    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->b:Ljava/lang/String;

    .line 114566
    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->c:Ljava/lang/String;

    .line 114567
    iput-boolean v1, p0, Lcom/facebook/widget/CustomLinearLayout;->e:Z

    .line 114568
    iput-boolean v1, p0, Lcom/facebook/widget/CustomLinearLayout;->f:Z

    .line 114569
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114570
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 114571
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114572
    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    .line 114573
    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->b:Ljava/lang/String;

    .line 114574
    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->c:Ljava/lang/String;

    .line 114575
    iput-boolean v1, p0, Lcom/facebook/widget/CustomLinearLayout;->e:Z

    .line 114576
    iput-boolean v1, p0, Lcom/facebook/widget/CustomLinearLayout;->f:Z

    .line 114577
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114578
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114579
    if-eqz p2, :cond_0

    .line 114580
    sget-object v0, LX/03r;->CustomLinearLayout:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 114581
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    .line 114582
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 114583
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 114584
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onMeasure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->b:Ljava/lang/String;

    .line 114585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onLayout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->c:Ljava/lang/String;

    .line 114586
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 114595
    invoke-static {p0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/10U;)V
    .locals 1

    .prologue
    .line 114552
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    if-nez v0, :cond_0

    .line 114553
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 114554
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 114555
    return-void
.end method

.method public final asViewGroup()Landroid/view/ViewGroup;
    .locals 0

    .prologue
    .line 114596
    return-object p0
.end method

.method public final attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .param p3    # Landroid/view/ViewGroup$LayoutParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 114597
    instance-of v0, p1, LX/2ea;

    if-eqz v0, :cond_0

    .line 114598
    invoke-static {p0, p1, p2}, LX/4oH;->a(LX/0h1;Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114599
    :goto_0
    return-void

    .line 114600
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 114601
    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->requestLayout()V

    goto :goto_0
.end method

.method public final b(I)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)",
            "LX/0am",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 114602
    invoke-static {p0, p1}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final detachRecyclableViewFromParent(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 114603
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->detachViewFromParent(Landroid/view/View;)V

    .line 114604
    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->requestLayout()V

    .line 114605
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 114606
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 114607
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    if-nez v0, :cond_1

    .line 114608
    :cond_0
    :goto_0
    return-void

    .line 114609
    :cond_1
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 114610
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10U;

    .line 114611
    invoke-interface {v0}, LX/10U;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 114612
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 114613
    :catch_0
    move-exception v0

    .line 114614
    iget v1, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0

    .line 114615
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->removeAll(Ljava/util/Collection;)Z

    .line 114616
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114617
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->h:Ljava/util/concurrent/CopyOnWriteArraySet;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 114618
    :catch_1
    move-exception v0

    .line 114619
    iget v1, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0
.end method

.method public dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114620
    iget-boolean v0, p0, Lcom/facebook/widget/CustomLinearLayout;->f:Z

    if-eqz v0, :cond_0

    .line 114621
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 114622
    :cond_0
    return-void
.end method

.method public dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114623
    iget-boolean v0, p0, Lcom/facebook/widget/CustomLinearLayout;->f:Z

    if-eqz v0, :cond_0

    .line 114624
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 114625
    :cond_0
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 114556
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 114557
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 114558
    iget-boolean v0, p0, Lcom/facebook/widget/CustomLinearLayout;->e:Z

    if-eqz v0, :cond_0

    .line 114559
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/CustomLinearLayout;->e:Z

    .line 114560
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 114561
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 114562
    :cond_1
    return-void
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 114548
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 114549
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114550
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 114551
    :cond_0
    return-void
.end method

.method public getInjector()LX/0QA;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 114547
    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 114528
    iget-object v2, p0, Lcom/facebook/widget/CustomLinearLayout;->c:Ljava/lang/String;

    .line 114529
    if-eqz v2, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 114530
    :goto_0
    if-eqz v1, :cond_0

    .line 114531
    const v0, 0x711a734e

    invoke-static {v2, v0}, LX/02m;->a(Ljava/lang/String;I)V

    .line 114532
    :cond_0
    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114533
    if-eqz v1, :cond_1

    .line 114534
    const v0, -0x43a90dfb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 114535
    :cond_1
    :goto_1
    return-void

    .line 114536
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 114537
    :catch_0
    move-exception v0

    .line 114538
    :try_start_1
    iget v2, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114539
    if-eqz v1, :cond_1

    .line 114540
    const v0, 0x2e414354

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 114541
    :catch_1
    move-exception v0

    .line 114542
    :try_start_2
    iget v2, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 114543
    if-eqz v1, :cond_1

    .line 114544
    const v0, -0x5c5e9f08

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 114545
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 114546
    const v1, -0x50e3023b

    invoke-static {v1}, LX/02m;->a(I)V

    :cond_3
    throw v0
.end method

.method public onMeasure(II)V
    .locals 3

    .prologue
    .line 114509
    iget-object v2, p0, Lcom/facebook/widget/CustomLinearLayout;->b:Ljava/lang/String;

    .line 114510
    if-eqz v2, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 114511
    :goto_0
    if-eqz v1, :cond_0

    .line 114512
    const v0, -0x8e02698

    invoke-static {v2, v0}, LX/02m;->a(Ljava/lang/String;I)V

    .line 114513
    :cond_0
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114514
    if-eqz v1, :cond_1

    .line 114515
    const v0, -0x1a543146

    invoke-static {v0}, LX/02m;->a(I)V

    .line 114516
    :cond_1
    :goto_1
    return-void

    .line 114517
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 114518
    :catch_0
    move-exception v0

    .line 114519
    :try_start_1
    iget v2, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114520
    if-eqz v1, :cond_1

    .line 114521
    const v0, -0x66f37418

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 114522
    :catch_1
    move-exception v0

    .line 114523
    :try_start_2
    iget v2, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 114524
    if-eqz v1, :cond_1

    .line 114525
    const v0, -0x4577f301

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 114526
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 114527
    const v1, 0x19179932

    invoke-static {v1}, LX/02m;->a(I)V

    :cond_3
    throw v0
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x722da9d2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 114506
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 114507
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/CustomLinearLayout;->e:Z

    .line 114508
    const/16 v1, 0x2d

    const v2, 0x1b176469

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final removeRecyclableViewFromParent(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 114504
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->removeDetachedView(Landroid/view/View;Z)V

    .line 114505
    return-void
.end method

.method public final restoreHierarchyState(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114500
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114501
    :goto_0
    return-void

    .line 114502
    :catch_0
    move-exception v0

    .line 114503
    iget v1, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final saveHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114498
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 114499
    return-void
.end method

.method public setContentView(I)V
    .locals 4

    .prologue
    .line 114482
    iput p1, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    .line 114483
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->a:Ljava/lang/String;

    .line 114484
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 114485
    const-string v1, "%s.setContentView(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const v0, -0x181f35e9

    invoke-static {v1, v2, v0}, LX/02m;->a(Ljava/lang/String;[Ljava/lang/Object;I)V

    .line 114486
    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 114487
    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114488
    const v0, -0x2f57f66c    # -2.25536E10f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 114489
    :goto_2
    return-void

    .line 114490
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 114491
    :cond_1
    const-string v1, "%s.setContentView"

    const v2, 0x59a743c4

    invoke-static {v1, v0, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    goto :goto_1

    .line 114492
    :catch_0
    move-exception v0

    .line 114493
    :try_start_1
    iget v1, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114494
    const v0, 0x33854f7a

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_2

    .line 114495
    :catch_1
    move-exception v0

    .line 114496
    :try_start_2
    iget v1, p0, Lcom/facebook/widget/CustomLinearLayout;->g:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 114497
    const v0, -0x4c377e6c

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_2

    :catchall_0
    move-exception v0

    const v1, 0x514483cc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 114460
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 114461
    :goto_0
    return-void

    .line 114462
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 114463
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 114464
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114465
    :cond_1
    iput-object p1, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    .line 114466
    if-eqz p1, :cond_3

    .line 114467
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setWillNotDraw(Z)V

    .line 114468
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 114469
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114470
    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 114471
    :cond_2
    :goto_1
    iput-boolean v2, p0, Lcom/facebook/widget/CustomLinearLayout;->e:Z

    .line 114472
    invoke-virtual {p0}, Lcom/facebook/widget/CustomLinearLayout;->invalidate()V

    goto :goto_0

    .line 114473
    :cond_3
    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setWillNotDraw(Z)V

    goto :goto_1
.end method

.method public setSaveFromParentEnabledCompat(Z)V
    .locals 0

    .prologue
    .line 114480
    iput-boolean p1, p0, Lcom/facebook/widget/CustomLinearLayout;->f:Z

    .line 114481
    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 114475
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114476
    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 114477
    iget-object v2, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 114478
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 114479
    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 114474
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/CustomLinearLayout;->d:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
