.class public Lcom/facebook/widget/text/BetterEditTextView;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""


# instance fields
.field private b:LX/630;

.field public c:LX/633;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/631;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Z

.field public g:Z

.field private h:Z

.field private i:Z

.field private j:LX/62z;

.field public k:LX/62y;

.field private l:LX/632;

.field private m:Z

.field private n:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 167767
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 167768
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->f:Z

    .line 167769
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->g:Z

    .line 167770
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->h:Z

    .line 167771
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    .line 167772
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 167773
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 167774
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->f:Z

    .line 167775
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->g:Z

    .line 167776
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->h:Z

    .line 167777
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    .line 167778
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 167779
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 167780
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 167781
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->f:Z

    .line 167782
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->g:Z

    .line 167783
    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->h:Z

    .line 167784
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    .line 167785
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 167786
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 167787
    if-nez p1, :cond_0

    .line 167788
    :goto_0
    return v0

    .line 167789
    :cond_0
    invoke-static {p0, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v1

    move v4, v0

    move v0, v1

    move v1, v4

    .line 167790
    :goto_1
    if-ge v1, p1, :cond_2

    .line 167791
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    .line 167792
    add-int v3, v1, v2

    if-ge v3, p1, :cond_1

    .line 167793
    add-int v0, v1, v2

    invoke-static {p0, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 167794
    :cond_1
    add-int/2addr v1, v2

    .line 167795
    goto :goto_1

    :cond_2
    move v0, v1

    .line 167796
    goto :goto_0
.end method

.method private a(Landroid/content/ClipData;)V
    .locals 2

    .prologue
    .line 167797
    if-nez p1, :cond_0

    .line 167798
    :goto_0
    return-void

    .line 167799
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 167800
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167801
    :catch_0
    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 167802
    sget-object v0, LX/03r;->BetterEditTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 167803
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 167804
    const/16 v2, 0x1

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 167805
    invoke-static {v1}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v1

    invoke-static {v2}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 167806
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->f:Z

    .line 167807
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->e:Landroid/graphics/drawable/Drawable;

    .line 167808
    iget-object v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 167809
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->b()V

    .line 167810
    :cond_0
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->h:Z

    .line 167811
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->i:Z

    .line 167812
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 167813
    return-void
.end method

.method private static a(I)Z
    .locals 2

    .prologue
    .line 167814
    const v0, 0x2000f

    and-int/2addr v0, p0

    const v1, 0x20001

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 167815
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getCompoundPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/widget/text/BetterEditTextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 167816
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 167817
    :goto_0
    return-void

    .line 167818
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 167819
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setRightDrawableVisibility(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 167820
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->f:Z

    if-nez v0, :cond_3

    .line 167821
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->e()V

    goto :goto_0

    .line 167822
    :cond_3
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->f()V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 167823
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->l:LX/632;

    if-nez v0, :cond_0

    .line 167824
    new-instance v0, LX/632;

    invoke-direct {v0, p0}, LX/632;-><init>(Lcom/facebook/widget/text/BetterEditTextView;)V

    iput-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->l:LX/632;

    .line 167825
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->l:LX/632;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 167826
    iput-boolean v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->g:Z

    .line 167827
    :cond_0
    return-void
.end method

.method private b(I)Z
    .locals 2

    .prologue
    .line 167828
    iget-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->i:Z

    if-nez v0, :cond_0

    const v0, 0x1020022

    if-ne p1, v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 167829
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->l:LX/632;

    if-eqz v0, :cond_0

    .line 167830
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->l:LX/632;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 167831
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->l:LX/632;

    .line 167832
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 167833
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    .line 167834
    iget-object v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->e:Landroid/graphics/drawable/Drawable;

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 167835
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->e:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setRightDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167836
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 167837
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setRightDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167838
    return-void
.end method

.method private g()Landroid/content/ClipData;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 167757
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 167758
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 167759
    :goto_0
    return-object v0

    .line 167760
    :cond_0
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v2

    .line 167761
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 167762
    invoke-static {v1, v3}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    move-object v0, v2

    .line 167763
    goto :goto_0
.end method

.method private setRightDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 167764
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 167765
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    const/4 v3, 0x3

    aget-object v0, v0, v3

    invoke-virtual {p0, v1, v2, p1, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 167766
    return-void
.end method

.method private setTextWithDispatchToTextInteractionListener(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 167691
    sget-object v0, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    invoke-super {p0, p1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 167692
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 167693
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setTextWithDispatchToTextInteractionListener(Ljava/lang/CharSequence;)V

    .line 167694
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 4

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v1, 0x1

    .line 167695
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getImeOptions()I

    move-result v0

    and-int/2addr v0, v2

    .line 167696
    if-ne v0, v2, :cond_1

    move v0, v1

    .line 167697
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v3

    .line 167698
    const/4 v2, 0x0

    .line 167699
    if-eqz v3, :cond_2

    .line 167700
    new-instance v2, LX/62x;

    invoke-direct {v2, p0, v3, v1}, LX/62x;-><init>(Lcom/facebook/widget/text/BetterEditTextView;Landroid/view/inputmethod/InputConnection;Z)V

    move-object v1, v2

    .line 167701
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/widget/text/BetterEditTextView;->h:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167702
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, -0x40000001    # -1.9999999f

    and-int/2addr v0, v2

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 167703
    :cond_0
    return-object v1

    .line 167704
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method

.method public final onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x741f5cd1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 167705
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->a$redex0(Lcom/facebook/widget/text/BetterEditTextView;Ljava/lang/CharSequence;)V

    .line 167706
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 167707
    const/16 v1, 0x2d

    const v2, 0x735f7bff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 167708
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->m:Z

    .line 167709
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 167710
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/widget/text/BetterEditTextView;->m:Z

    .line 167711
    return-object v0
.end method

.method public final onScrollChanged(IIII)V
    .locals 0

    .prologue
    .line 167712
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbEditText;->onScrollChanged(IIII)V

    .line 167713
    return-void
.end method

.method public final onSelectionChanged(II)V
    .locals 2

    .prologue
    .line 167714
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;->onSelectionChanged(II)V

    .line 167715
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 167716
    if-ne p1, p2, :cond_0

    if-lt p2, v0, :cond_1

    .line 167717
    :cond_0
    :goto_0
    return-void

    .line 167718
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/facebook/widget/text/BetterEditTextView;->a(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 167719
    if-eq v0, p1, :cond_0

    .line 167720
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_0
.end method

.method public final onTextContextMenuItem(I)Z
    .locals 2

    .prologue
    .line 167721
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167722
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->g()Landroid/content/ClipData;

    move-result-object v1

    .line 167723
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onTextContextMenuItem(I)Z

    move-result v0

    .line 167724
    invoke-direct {p0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->a(Landroid/content/ClipData;)V

    .line 167725
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onTextContextMenuItem(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1b15685a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 167726
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_1

    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 167727
    iget-object v2, p0, Lcom/facebook/widget/text/BetterEditTextView;->k:LX/62y;

    if-eqz v2, :cond_0

    .line 167728
    iget-object v2, p0, Lcom/facebook/widget/text/BetterEditTextView;->k:LX/62y;

    invoke-interface {v2}, LX/62y;->onClick()Z

    move-result v2

    .line 167729
    if-eqz v2, :cond_0

    .line 167730
    const v2, -0x32eeb8d8

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 167731
    :goto_0
    return v0

    .line 167732
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 167733
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x2347c3a8

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setOnCustomRightDrawableClickListener(LX/62y;)V
    .locals 0

    .prologue
    .line 167734
    iput-object p1, p0, Lcom/facebook/widget/text/BetterEditTextView;->k:LX/62y;

    .line 167735
    return-void
.end method

.method public setOnDeleteKeyListener(LX/62z;)V
    .locals 0

    .prologue
    .line 167736
    iput-object p1, p0, Lcom/facebook/widget/text/BetterEditTextView;->j:LX/62z;

    .line 167737
    return-void
.end method

.method public setOnScrollListener(LX/630;)V
    .locals 0

    .prologue
    .line 167738
    iput-object p1, p0, Lcom/facebook/widget/text/BetterEditTextView;->b:LX/630;

    .line 167739
    return-void
.end method

.method public setOnSelectionChangedListener(LX/631;)V
    .locals 0
    .param p1    # LX/631;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 167740
    iput-object p1, p0, Lcom/facebook/widget/text/BetterEditTextView;->d:LX/631;

    .line 167741
    return-void
.end method

.method public setRightDrawableVisibility(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 167742
    iput-object p1, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    .line 167743
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 167744
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/widget/text/BetterEditTextView;->a$redex0(Lcom/facebook/widget/text/BetterEditTextView;Ljava/lang/CharSequence;)V

    .line 167745
    :goto_0
    return-void

    .line 167746
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167747
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->e()V

    goto :goto_0

    .line 167748
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->f()V

    goto :goto_0
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .prologue
    .line 167749
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterEditTextView;->g:Z

    .line 167750
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 167751
    return-void
.end method

.method public setTextInteractionListener(LX/633;)V
    .locals 0
    .param p1    # LX/633;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 167752
    if-nez p1, :cond_0

    .line 167753
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->c()V

    .line 167754
    :goto_0
    iput-object p1, p0, Lcom/facebook/widget/text/BetterEditTextView;->c:LX/633;

    .line 167755
    return-void

    .line 167756
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/text/BetterEditTextView;->b()V

    goto :goto_0
.end method
