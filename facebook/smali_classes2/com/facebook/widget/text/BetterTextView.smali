.class public Lcom/facebook/widget/text/BetterTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:Ljava/lang/Object;

.field private b:Z

.field private c:Z

.field private d:I

.field private e:Z

.field public f:Z

.field private g:Z

.field private h:LX/2nR;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 168003
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 168004
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 167986
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 167987
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 167988
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 167989
    sget-object v0, LX/03r;->BetterTextView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 167990
    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/text/BetterTextView;->a(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 167991
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 167992
    return-void
.end method

.method private a(Z)I
    .locals 5

    .prologue
    .line 167993
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 167994
    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    .line 167995
    const/4 v1, 0x0

    .line 167996
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 167997
    if-nez p1, :cond_0

    .line 167998
    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineMax(I)F

    move-result v0

    .line 167999
    :goto_1
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 168000
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 168001
    :cond_0
    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v0

    goto :goto_1

    .line 168002
    :cond_1
    float-to-int v0, v2

    return v0
.end method

.method private a(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 167966
    const/16 v0, 0x3

    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->b:Z

    .line 167967
    const/16 v0, 0x4

    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->c:Z

    .line 167968
    const/16 v0, 0x5

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/text/BetterTextView;->d:I

    .line 167969
    const/16 v0, 0x7

    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->e:Z

    .line 167970
    const/16 v0, 0x0

    sget-object v1, LX/0xq;->BUILTIN:LX/0xq;

    invoke-virtual {v1}, LX/0xq;->ordinal()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 167971
    const/16 v1, 0x1

    sget-object v2, LX/0xr;->BUILTIN:LX/0xr;

    invoke-virtual {v2}, LX/0xr;->ordinal()I

    move-result v2

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 167972
    invoke-static {v0}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v0

    invoke-static {v1}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 167973
    const/16 v0, 0x2

    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167974
    new-instance v0, LX/0wJ;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wJ;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 167975
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/636;)V
    .locals 2

    .prologue
    .line 168010
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 168011
    iput-object p1, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    .line 168012
    :goto_0
    return-void

    .line 168013
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 168014
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168015
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168016
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    check-cast v0, LX/636;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168017
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168018
    iput-object v1, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(LX/636;)V
    .locals 1

    .prologue
    .line 168005
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    if-ne v0, p1, :cond_1

    .line 168006
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    .line 168007
    :cond_0
    :goto_0
    return-void

    .line 168008
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 168009
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getMaximallyWideThreshold()F
    .locals 1

    .prologue
    .line 168020
    iget v0, p0, Lcom/facebook/widget/text/BetterTextView;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public getMinimallyWide()Z
    .locals 1

    .prologue
    .line 168019
    iget-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->b:Z

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x9a2a16f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 167976
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onAttachedToWindow()V

    .line 167977
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/text/BetterTextView;->g:Z

    .line 167978
    iget-object v1, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    if-eqz v1, :cond_0

    .line 167979
    iget-object v1, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    invoke-interface {v1, p0}, LX/2nR;->a(Landroid/view/View;)V

    .line 167980
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x73d51855

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x147543d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 167981
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/widget/text/BetterTextView;->g:Z

    .line 167982
    iget-object v1, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    if-eqz v1, :cond_0

    .line 167983
    iget-object v1, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    invoke-interface {v1, p0}, LX/2nR;->b(Landroid/view/View;)V

    .line 167984
    :cond_0
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onDetachedFromWindow()V

    .line 167985
    const/16 v1, 0x2d

    const v2, -0x583ecd5b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 167892
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    instance-of v0, v0, LX/636;

    if-eqz v0, :cond_2

    .line 167893
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    check-cast v0, LX/636;

    invoke-interface {v0, p1}, LX/636;->a(Landroid/graphics/Canvas;)V

    .line 167894
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v0

    instance-of v0, v0, Landroid/text/method/SingleLineTransformationMethod;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 167895
    invoke-virtual {p0, v3}, Lcom/facebook/widget/text/BetterTextView;->bringPointIntoView(I)Z

    .line 167896
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 167897
    return-void

    .line 167898
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 167899
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 167900
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    .line 167901
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/636;

    invoke-interface {v1, p1}, LX/636;->a(Landroid/graphics/Canvas;)V

    .line 167902
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 167903
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onFinishTemporaryDetach()V

    .line 167904
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->g:Z

    .line 167905
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    if-eqz v0, :cond_0

    .line 167906
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->a(Landroid/view/View;)V

    .line 167907
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v9, -0x80000000

    const/high16 v8, 0x40000000    # 2.0f

    const/16 v2, 0x2c

    const v3, 0x6cfde705

    invoke-static {v10, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 167908
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 167909
    iget-boolean v4, p0, Lcom/facebook/widget/text/BetterTextView;->e:Z

    if-eqz v4, :cond_1

    if-eq v3, v9, :cond_0

    if-ne v3, v8, :cond_1

    .line 167910
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 167911
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getLineHeight()I

    move-result v4

    div-int/2addr v3, v4

    .line 167912
    invoke-virtual {p0, v3}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    .line 167913
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 167914
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getLineCount()I

    move-result v3

    if-le v3, v0, :cond_4

    .line 167915
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 167916
    iget v4, p0, Lcom/facebook/widget/text/BetterTextView;->d:I

    if-lez v4, :cond_5

    .line 167917
    if-ne v3, v9, :cond_5

    .line 167918
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 167919
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 167920
    invoke-direct {p0, v1}, Lcom/facebook/widget/text/BetterTextView;->a(Z)I

    move-result v6

    .line 167921
    if-ge v6, v4, :cond_5

    sub-int v6, v4, v6

    iget v7, p0, Lcom/facebook/widget/text/BetterTextView;->d:I

    if-ge v6, v7, :cond_5

    .line 167922
    if-ge v5, v4, :cond_2

    .line 167923
    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 167924
    invoke-super {p0, v1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 167925
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/facebook/widget/text/BetterTextView;->b:Z

    if-eqz v1, :cond_4

    if-eq v3, v8, :cond_3

    if-ne v3, v9, :cond_4

    :cond_3
    if-nez v0, :cond_4

    .line 167926
    iget-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->c:Z

    invoke-direct {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->a(Z)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 167927
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getMeasuredWidth()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 167928
    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 167929
    invoke-super {p0, v0, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 167930
    :cond_4
    const/16 v0, 0x2d

    const v1, 0xcb647a2

    invoke-static {v10, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 167931
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->g:Z

    .line 167932
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    if-eqz v0, :cond_0

    .line 167933
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->b(Landroid/view/View;)V

    .line 167934
    :cond_0
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onStartTemporaryDetach()V

    .line 167935
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 167936
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 167937
    invoke-static {p0, p1, p4}, LX/0zn;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->f:Z

    .line 167938
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x1

    const v3, 0x7f3a54dd

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 167939
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    instance-of v0, v0, LX/636;

    if-eqz v0, :cond_1

    .line 167940
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    check-cast v0, LX/636;

    invoke-interface {v0}, LX/636;->a()Z

    move-result v0

    .line 167941
    :goto_0
    if-nez v0, :cond_0

    .line 167942
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 167943
    :cond_0
    const v1, 0x155c6a36

    invoke-static {v1, v3}, LX/02F;->a(II)V

    return v0

    .line 167944
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 167945
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 167946
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    .line 167947
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/636;

    invoke-interface {v1}, LX/636;->a()Z

    move-result v1

    .line 167948
    if-nez v1, :cond_2

    .line 167949
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public setAttachDetachListener(LX/2nR;)V
    .locals 1

    .prologue
    .line 167950
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    if-eqz v0, :cond_0

    .line 167951
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->b(Landroid/view/View;)V

    .line 167952
    :cond_0
    iput-object p1, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    .line 167953
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/text/BetterTextView;->g:Z

    if-eqz v0, :cond_1

    .line 167954
    iget-object v0, p0, Lcom/facebook/widget/text/BetterTextView;->h:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->a(Landroid/view/View;)V

    .line 167955
    :cond_1
    return-void
.end method

.method public setMaximallyWideThreshold(I)V
    .locals 0

    .prologue
    .line 167956
    iput p1, p0, Lcom/facebook/widget/text/BetterTextView;->d:I

    .line 167957
    return-void
.end method

.method public setMinimallyWide(Z)V
    .locals 0

    .prologue
    .line 167958
    iput-boolean p1, p0, Lcom/facebook/widget/text/BetterTextView;->b:Z

    .line 167959
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->requestLayout()V

    .line 167960
    return-void
.end method

.method public final setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 167961
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 167962
    sget-object v0, LX/03r;->BetterTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 167963
    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/text/BetterTextView;->a(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 167964
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 167965
    return-void
.end method
