.class public Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/1UP;


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I


# instance fields
.field private e:Landroid/widget/ProgressBar;

.field private f:Landroid/view/ViewStub;

.field private g:LX/62S;

.field private h:Landroid/view/View;

.field private i:LX/1lD;

.field private j:LX/1lC;

.field public k:I

.field public l:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:LX/0yW;

.field private v:LX/0So;

.field private w:J

.field private final x:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 311246
    const v0, 0x7f080039

    sput v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a:I

    .line 311247
    const v0, 0x7f080040

    sput v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b:I

    .line 311248
    const v0, 0x7f020bb9

    sput v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->c:I

    .line 311249
    const v0, 0x7f020bba

    sput v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 311250
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 311251
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311252
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 311253
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311254
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 311255
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->x:Landroid/os/Handler;

    .line 311256
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 311257
    return-void
.end method

.method private a(LX/0So;LX/0yW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 311258
    iput-object p1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->v:LX/0So;

    .line 311259
    iput-object p2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->u:LX/0yW;

    .line 311260
    return-void
.end method

.method private a(LX/1lD;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x8

    const/4 v3, 0x4

    .line 311261
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v5

    .line 311262
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 311263
    :goto_0
    iget-object v2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v2, v2, LX/62S;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    .line 311264
    :goto_1
    sget-object v6, LX/1lE;->a:[I

    invoke-virtual {p1}, LX/1lD;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    move v3, v0

    move v4, v5

    .line 311265
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 311266
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewStub;

    if-nez v0, :cond_1

    .line 311267
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 311268
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    if-eqz v0, :cond_2

    .line 311269
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 311270
    :cond_2
    iput-object p1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->i:LX/1lD;

    .line 311271
    return-void

    :cond_3
    move v0, v1

    .line 311272
    goto :goto_0

    :cond_4
    move v2, v1

    .line 311273
    goto :goto_1

    .line 311274
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->i:LX/1lD;

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 311275
    goto :goto_2

    .line 311276
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    const-string v1, "notifyLoadingFailed() should be called before updating the state to ERROR"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v4

    move v4, v3

    .line 311277
    goto :goto_2

    .line 311278
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_5

    .line 311279
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    :cond_5
    move v2, v1

    move v3, v4

    move v4, v1

    .line 311280
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 311319
    const-class v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-static {v0, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 311320
    const v0, 0x7f030a3d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 311321
    const v0, 0x7f0d0446

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e:Landroid/widget/ProgressBar;

    .line 311322
    const v0, 0x7f0d0d24

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f:Landroid/view/ViewStub;

    .line 311323
    sget-object v0, LX/03r;->LoadingIndicatorView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 311324
    const/16 v0, 0x8

    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 311325
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 311326
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 311327
    iget-object v2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 311328
    const/16 v0, 0x6

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311329
    const/16 v0, 0x6

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 311330
    invoke-virtual {p0, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setContentLayout(I)V

    .line 311331
    :cond_0
    const/16 v0, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 311332
    if-nez v0, :cond_1

    .line 311333
    sget-object v0, LX/1lC;->VERTICAL:LX/1lC;

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->j:LX/1lC;

    .line 311334
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f:Landroid/view/ViewStub;

    const v2, 0x7f030a3c

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 311335
    :goto_0
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 311336
    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->m:I

    .line 311337
    invoke-direct {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getDefaultResourceId()I

    move-result v0

    .line 311338
    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->k:I

    .line 311339
    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->s:Ljava/lang/String;

    .line 311340
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->n:I

    .line 311341
    const/16 v0, 0x3

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->o:I

    .line 311342
    const/16 v0, 0x4

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->p:I

    .line 311343
    const/16 v0, 0x5

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->q:I

    .line 311344
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 311345
    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x1010036

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 311346
    const/16 v2, 0x7

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->t:I

    .line 311347
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 311348
    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 311349
    return-void

    .line 311350
    :cond_1
    sget-object v0, LX/1lC;->HORIZONTAL:LX/1lC;

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->j:LX/1lC;

    .line 311351
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f:Landroid/view/ViewStub;

    const v2, 0x7f030a3a

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;)V
    .locals 2

    .prologue
    .line 311281
    iget-object v0, p1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->b:Ljava/lang/String;

    move-object v0, v0

    .line 311282
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->r:Ljava/lang/String;

    .line 311283
    iget-object v0, p1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->c:Ljava/lang/String;

    move-object v0, v0

    .line 311284
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->s:Ljava/lang/String;

    .line 311285
    iget v0, p1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->d:I

    move v0, v0

    .line 311286
    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getDefaultResourceId()I

    move-result v0

    .line 311287
    :goto_2
    iput v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->k:I

    .line 311288
    return-void

    .line 311289
    :cond_0
    iget-object v0, p1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->b:Ljava/lang/String;

    move-object v0, v0

    .line 311290
    goto :goto_0

    .line 311291
    :cond_1
    iget-object v0, p1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->c:Ljava/lang/String;

    move-object v0, v0

    .line 311292
    goto :goto_1

    .line 311293
    :cond_2
    iget v0, p1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->d:I

    move v0, v0

    .line 311294
    goto :goto_2
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-static {v1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-static {v1}, LX/0yW;->a(LX/0QB;)LX/0yW;

    move-result-object v1

    check-cast v1, LX/0yW;

    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/0So;LX/0yW;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;LX/1DI;)V
    .locals 2

    .prologue
    .line 311295
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->i:LX/1lD;

    sget-object v1, LX/1lD;->LOADING:LX/1lD;

    if-eq v0, v1, :cond_0

    .line 311296
    :goto_0
    return-void

    .line 311297
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g()V

    .line 311298
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311299
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311300
    if-eqz p1, :cond_1

    .line 311301
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->a:Landroid/view/View;

    new-instance v1, LX/62R;

    invoke-direct {v1, p0, p1}, LX/62R;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;LX/1DI;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311302
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311303
    :goto_1
    sget-object v0, LX/1lD;->ERROR:LX/1lD;

    invoke-direct {p0, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1lD;)V

    goto :goto_0

    .line 311304
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private g()V
    .locals 4

    .prologue
    .line 311305
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    if-eqz v0, :cond_0

    .line 311306
    :goto_0
    return-void

    .line 311307
    :cond_0
    new-instance v0, LX/62S;

    iget-object v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62S;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    .line 311308
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->j:LX/1lC;

    sget-object v1, LX/1lC;->HORIZONTAL:LX/1lC;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .line 311309
    :goto_1
    iget-object v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v1, v1, LX/62S;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 311310
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->b:Landroid/widget/TextView;

    iget v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->t:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 311311
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 311312
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 311313
    :goto_2
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->j:LX/1lC;

    sget-object v1, LX/1lC;->HORIZONTAL:LX/1lC;

    if-ne v0, v1, :cond_3

    .line 311314
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->d:Landroid/widget/ImageView;

    new-instance v1, LX/1au;

    iget v2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->n:I

    iget v3, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->o:I

    invoke-direct {v1, v2, v3}, LX/1au;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 311315
    :goto_3
    iget v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->p:I

    iget v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->q:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(II)V

    goto :goto_0

    .line 311316
    :cond_1
    const/16 v0, 0x11

    goto :goto_1

    .line 311317
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->d:Landroid/widget/ImageView;

    iget v1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->k:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 311318
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->d:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->n:I

    iget v3, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->o:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3
.end method

.method private getDefaultResourceId()I
    .locals 1

    .prologue
    .line 311245
    iget v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->m:I

    if-nez v0, :cond_0

    sget v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->c:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->d:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 311241
    sget-object v0, LX/1lD;->LOADING:LX/1lD;

    invoke-direct {p0, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1lD;)V

    .line 311242
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->u:LX/0yW;

    sget-object v1, LX/1lF;->LOADING_START_TRIGGER:LX/1lF;

    invoke-virtual {v0, v1}, LX/0yW;->a(LX/1lF;)V

    .line 311243
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->v:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->w:J

    .line 311244
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 311223
    iput p1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->p:I

    .line 311224
    iput p2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->q:I

    .line 311225
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    if-eqz v0, :cond_0

    .line 311226
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->a:Landroid/view/View;

    invoke-virtual {v0, v1, p1, v1, p2}, Landroid/view/View;->setPadding(IIII)V

    .line 311227
    :cond_0
    return-void
.end method

.method public final a(LX/1DI;Ljava/lang/Runnable;)V
    .locals 8
    .param p2    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v6, 0x1f4

    const-wide/16 v2, 0x0

    .line 311214
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->u:LX/0yW;

    sget-object v1, LX/1lF;->LOADING_FAILED_TRIGGER:LX/1lF;

    invoke-virtual {v0, v1}, LX/0yW;->a(LX/1lF;)V

    .line 311215
    iget-wide v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->w:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 311216
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->v:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->w:J

    sub-long/2addr v0, v4

    .line 311217
    cmp-long v4, v0, v6

    if-gez v4, :cond_0

    .line 311218
    sub-long v0, v6, v0

    .line 311219
    :goto_0
    iput-wide v2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->w:J

    .line 311220
    :goto_1
    sget-object v2, LX/1lD;->LOADING:LX/1lD;

    invoke-direct {p0, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1lD;)V

    .line 311221
    iget-object v2, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->x:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView$1;

    invoke-direct {v3, p0, p1, p2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView$1;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;LX/1DI;Ljava/lang/Runnable;)V

    const v4, -0x45ba76d8

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 311222
    return-void

    :cond_0
    move-wide v0, v2

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method public final a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V
    .locals 1

    .prologue
    .line 311211
    invoke-direct {p0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;)V

    .line 311212
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1DI;Ljava/lang/Runnable;)V

    .line 311213
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1DI;)V
    .locals 1

    .prologue
    .line 311209
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;Ljava/lang/Runnable;)V

    .line 311210
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1DI;Ljava/lang/Runnable;)V
    .locals 0
    .param p3    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311206
    iput-object p1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->r:Ljava/lang/String;

    .line 311207
    invoke-virtual {p0, p2, p3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1DI;Ljava/lang/Runnable;)V

    .line 311208
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 311202
    sget-object v0, LX/1lD;->LOAD_FINISHED:LX/1lD;

    invoke-direct {p0, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1lD;)V

    .line 311203
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 311228
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 311229
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->g:LX/62S;

    iget-object v0, v0, LX/62S;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311230
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 311231
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->i:LX/1lD;

    sget-object v1, LX/1lD;->LOADING:LX/1lD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContentLayout(I)V
    .locals 2

    .prologue
    .line 311232
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 311233
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->removeView(Landroid/view/View;)V

    .line 311234
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    .line 311235
    iget-object v0, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->h:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->addView(Landroid/view/View;)V

    .line 311236
    return-void
.end method

.method public setErrorImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 311237
    iput-object p1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->l:Landroid/graphics/drawable/Drawable;

    .line 311238
    return-void
.end method

.method public setImage(I)V
    .locals 0

    .prologue
    .line 311239
    iput p1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->k:I

    .line 311240
    return-void
.end method

.method public setImageSize(I)V
    .locals 0

    .prologue
    .line 311204
    iput p1, p0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->m:I

    .line 311205
    return-void
.end method
