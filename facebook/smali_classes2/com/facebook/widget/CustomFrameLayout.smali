.class public Lcom/facebook/widget/CustomFrameLayout;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""

# interfaces
.implements LX/0h0;
.implements LX/0h1;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Z

.field private e:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LX/10U;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:LX/1B1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 238003
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;)V

    .line 238004
    iput-object v1, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    .line 238005
    iput-object v1, p0, Lcom/facebook/widget/CustomFrameLayout;->b:Ljava/lang/String;

    .line 238006
    iput-object v1, p0, Lcom/facebook/widget/CustomFrameLayout;->c:Ljava/lang/String;

    .line 238007
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/CustomFrameLayout;->d:Z

    .line 238008
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 238009
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 237989
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 237990
    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    .line 237991
    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->b:Ljava/lang/String;

    .line 237992
    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->c:Ljava/lang/String;

    .line 237993
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/CustomFrameLayout;->d:Z

    .line 237994
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 237995
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 237996
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 237997
    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    .line 237998
    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->b:Ljava/lang/String;

    .line 237999
    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->c:Ljava/lang/String;

    .line 238000
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/CustomFrameLayout;->d:Z

    .line 238001
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 238002
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 238010
    if-eqz p2, :cond_0

    .line 238011
    sget-object v0, LX/03r;->CustomFrameLayout:[I

    invoke-virtual {p1, p2, v0, p3, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 238012
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    .line 238013
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 238014
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 238015
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onMeasure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->b:Ljava/lang/String;

    .line 238016
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onLayout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->c:Ljava/lang/String;

    .line 238017
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 238018
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 238019
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238020
    :cond_0
    return-void

    .line 238021
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10U;

    .line 238022
    invoke-interface {v0}, LX/10U;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 238023
    iget-object v2, p0, Lcom/facebook/widget/CustomFrameLayout;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/10U;)V
    .locals 1

    .prologue
    .line 238024
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_0

    .line 238025
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 238026
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 238027
    return-void
.end method

.method public final asViewGroup()Landroid/view/ViewGroup;
    .locals 0

    .prologue
    .line 238028
    return-object p0
.end method

.method public final attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 238029
    instance-of v0, p1, LX/2eZ;

    if-eqz v0, :cond_0

    .line 238030
    invoke-static {p0, p1, p2}, LX/4oH;->a(LX/0h1;Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238031
    :goto_0
    return-void

    .line 238032
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 238033
    invoke-virtual {p0}, Lcom/facebook/widget/CustomFrameLayout;->requestLayout()V

    goto :goto_0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 238034
    invoke-static {p0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)",
            "LX/0am",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 238035
    invoke-static {p0, p1}, LX/0jc;->a(Landroid/view/View;I)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final detachRecyclableViewFromParent(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 238036
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->detachViewFromParent(Landroid/view/View;)V

    .line 238037
    invoke-virtual {p0}, Lcom/facebook/widget/CustomFrameLayout;->requestLayout()V

    .line 238038
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 238039
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->a(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    .line 238040
    :goto_0
    return-void

    .line 238041
    :catch_0
    move-exception v0

    .line 238042
    iget v1, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0

    .line 238043
    :catch_1
    move-exception v0

    .line 238044
    iget v1, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V

    goto :goto_0
.end method

.method public dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237983
    iget-boolean v0, p0, Lcom/facebook/widget/CustomFrameLayout;->d:Z

    if-eqz v0, :cond_0

    .line 237984
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 237985
    :cond_0
    return-void
.end method

.method public dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237986
    iget-boolean v0, p0, Lcom/facebook/widget/CustomFrameLayout;->d:Z

    if-eqz v0, :cond_0

    .line 237987
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 237988
    :cond_0
    return-void
.end method

.method public fe_()V
    .locals 2

    .prologue
    .line 237901
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->g:LX/1B1;

    if-eqz v0, :cond_0

    .line 237902
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->g:LX/1B1;

    .line 237903
    const/4 v1, 0x0

    move-object v1, v1

    .line 237904
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 237905
    :cond_0
    return-void
.end method

.method public ff_()V
    .locals 2

    .prologue
    .line 237906
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->g:LX/1B1;

    if-eqz v0, :cond_0

    .line 237907
    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->g:LX/1B1;

    .line 237908
    const/4 v1, 0x0

    move-object v1, v1

    .line 237909
    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 237910
    :cond_0
    return-void
.end method

.method public getEventBus()LX/0b4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0b4;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 237911
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4fb2ebaa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 237912
    invoke-super {p0}, Lcom/facebook/resources/ui/FbFrameLayout;->onAttachedToWindow()V

    .line 237913
    invoke-virtual {p0}, Lcom/facebook/widget/CustomFrameLayout;->fe_()V

    .line 237914
    const/16 v1, 0x2d

    const v2, -0x5ae7a7af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x621ee5b1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 237915
    invoke-super {p0}, Lcom/facebook/resources/ui/FbFrameLayout;->onDetachedFromWindow()V

    .line 237916
    invoke-virtual {p0}, Lcom/facebook/widget/CustomFrameLayout;->ff_()V

    .line 237917
    const/16 v1, 0x2d

    const v2, 0x73906bb7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 237918
    invoke-super {p0}, Lcom/facebook/resources/ui/FbFrameLayout;->onFinishTemporaryDetach()V

    .line 237919
    invoke-virtual {p0}, Lcom/facebook/widget/CustomFrameLayout;->fe_()V

    .line 237920
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 237921
    iget-object v2, p0, Lcom/facebook/widget/CustomFrameLayout;->c:Ljava/lang/String;

    .line 237922
    if-eqz v2, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 237923
    :goto_0
    if-eqz v1, :cond_0

    .line 237924
    const v0, 0xe28ef81

    invoke-static {v2, v0}, LX/02m;->a(Ljava/lang/String;I)V

    .line 237925
    :cond_0
    :try_start_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbFrameLayout;->onLayout(ZIIII)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237926
    if-eqz v1, :cond_1

    .line 237927
    const v0, -0x68f3adb3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 237928
    :cond_1
    :goto_1
    return-void

    .line 237929
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 237930
    :catch_0
    move-exception v0

    .line 237931
    :try_start_1
    iget v2, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237932
    if-eqz v1, :cond_1

    .line 237933
    const v0, 0x21e8396f

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 237934
    :catch_1
    move-exception v0

    .line 237935
    :try_start_2
    iget v2, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 237936
    if-eqz v1, :cond_1

    .line 237937
    const v0, -0x2f2395bc

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 237938
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 237939
    const v1, 0x54f706cf

    invoke-static {v1}, LX/02m;->a(I)V

    :cond_3
    throw v0
.end method

.method public onMeasure(II)V
    .locals 3

    .prologue
    .line 237940
    iget-object v2, p0, Lcom/facebook/widget/CustomFrameLayout;->b:Ljava/lang/String;

    .line 237941
    if-eqz v2, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 237942
    :goto_0
    if-eqz v1, :cond_0

    .line 237943
    const v0, -0x3290181a    # -2.5155952E8f

    invoke-static {v2, v0}, LX/02m;->a(Ljava/lang/String;I)V

    .line 237944
    :cond_0
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbFrameLayout;->onMeasure(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237945
    if-eqz v1, :cond_1

    .line 237946
    const v0, 0x63dc71dc

    invoke-static {v0}, LX/02m;->a(I)V

    .line 237947
    :cond_1
    :goto_1
    return-void

    .line 237948
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 237949
    :catch_0
    move-exception v0

    .line 237950
    :try_start_1
    iget v2, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237951
    if-eqz v1, :cond_1

    .line 237952
    const v0, 0x2bd69fd0

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 237953
    :catch_1
    move-exception v0

    .line 237954
    :try_start_2
    iget v2, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v2, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 237955
    if-eqz v1, :cond_1

    .line 237956
    const v0, -0x39f301a0

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 237957
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 237958
    const v1, -0x73bbca40

    invoke-static {v1}, LX/02m;->a(I)V

    :cond_3
    throw v0
.end method

.method public onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 237959
    invoke-super {p0}, Lcom/facebook/resources/ui/FbFrameLayout;->onStartTemporaryDetach()V

    .line 237960
    invoke-virtual {p0}, Lcom/facebook/widget/CustomFrameLayout;->ff_()V

    .line 237961
    return-void
.end method

.method public final removeRecyclableViewFromParent(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 237962
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbFrameLayout;->removeDetachedView(Landroid/view/View;Z)V

    .line 237963
    return-void
.end method

.method public final restoreHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237964
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 237965
    return-void
.end method

.method public final saveHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237981
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 237982
    return-void
.end method

.method public setContentView(I)V
    .locals 3

    .prologue
    .line 237966
    iput p1, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    .line 237967
    const-string v1, "%s.setContentView"

    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/CustomFrameLayout;->a:Ljava/lang/String;

    :goto_0
    const v2, 0x10c28b9a

    invoke-static {v1, v0, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 237968
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/widget/CustomFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 237969
    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237970
    const v0, 0x16a36d61

    invoke-static {v0}, LX/02m;->a(I)V

    .line 237971
    :goto_1
    return-void

    .line 237972
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 237973
    :catch_0
    move-exception v0

    .line 237974
    :try_start_1
    iget v1, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237975
    const v0, -0x59a855e7

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    .line 237976
    :catch_1
    move-exception v0

    .line 237977
    :try_start_2
    iget v1, p0, Lcom/facebook/widget/CustomFrameLayout;->f:I

    invoke-static {p0, v1, v0}, LX/2Fh;->a(Landroid/view/View;ILjava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 237978
    const v0, -0x710b4782

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const v1, 0x433e0503

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setSaveFromParentEnabledCompat(Z)V
    .locals 0

    .prologue
    .line 237979
    iput-boolean p1, p0, Lcom/facebook/widget/CustomFrameLayout;->d:Z

    .line 237980
    return-void
.end method
