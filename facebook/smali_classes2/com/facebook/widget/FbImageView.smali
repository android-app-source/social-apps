.class public Lcom/facebook/widget/FbImageView;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 159736
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 159737
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/FbImageView;->a:Z

    .line 159738
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 159739
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 159740
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/FbImageView;->a:Z

    .line 159741
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 159742
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 159743
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/FbImageView;->a:Z

    .line 159744
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x28c25c98

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 159745
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 159746
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/FbImageView;->a:Z

    .line 159747
    const/16 v1, 0x2d

    const v2, 0x33fcbc20

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x66100abf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 159748
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 159749
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/widget/FbImageView;->a:Z

    .line 159750
    const/16 v1, 0x2d

    const v2, -0x61c7ed0d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 159751
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishTemporaryDetach()V

    .line 159752
    invoke-virtual {p0}, Lcom/facebook/widget/FbImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 159753
    if-eqz v2, :cond_0

    .line 159754
    invoke-virtual {p0}, Lcom/facebook/widget/FbImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159755
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 159756
    goto :goto_0
.end method

.method public onStartTemporaryDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159757
    invoke-super {p0}, Landroid/widget/ImageView;->onStartTemporaryDetach()V

    .line 159758
    invoke-virtual {p0}, Lcom/facebook/widget/FbImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 159759
    if-eqz v0, :cond_0

    .line 159760
    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159761
    :cond_0
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159762
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 159763
    if-eqz p1, :cond_0

    .line 159764
    invoke-virtual {p0}, Lcom/facebook/widget/FbImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/FbImageView;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159765
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 159766
    goto :goto_0
.end method

.method public setImageResource(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 159767
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 159768
    invoke-virtual {p0}, Lcom/facebook/widget/FbImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 159769
    if-eqz v2, :cond_0

    .line 159770
    invoke-virtual {p0}, Lcom/facebook/widget/FbImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/FbImageView;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159771
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 159772
    goto :goto_0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 159773
    invoke-super {p0, p1}, Landroid/widget/ImageView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 159774
    if-eqz p1, :cond_0

    .line 159775
    invoke-virtual {p1, v0, v0}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 159776
    :cond_0
    return-void
.end method
