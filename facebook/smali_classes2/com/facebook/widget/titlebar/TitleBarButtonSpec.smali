.class public Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field

.field public static a:I

.field public static final b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;


# instance fields
.field public final c:I

.field public final d:Landroid/graphics/drawable/Drawable;

.field private final e:Z

.field private final f:Z

.field private final g:Ljava/lang/String;

.field public final h:I

.field public final i:Ljava/lang/String;

.field private final j:I

.field public final k:Ljava/lang/String;

.field public final l:I

.field public final m:I

.field private final n:Z

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:I

.field public final s:I

.field public t:Z

.field public u:Z

.field public v:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 168447
    sput v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a:I

    .line 168448
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    .line 168449
    iput-boolean v1, v0, LX/108;->d:Z

    .line 168450
    move-object v0, v0

    .line 168451
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 168452
    new-instance v0, LX/109;

    invoke-direct {v0}, LX/109;-><init>()V

    sput-object v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/108;)V
    .locals 1

    .prologue
    .line 168424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168425
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    .line 168426
    iget v0, p1, LX/108;->a:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    .line 168427
    iget-object v0, p1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    .line 168428
    iget-boolean v0, p1, LX/108;->c:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->e:Z

    .line 168429
    iget-boolean v0, p1, LX/108;->e:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->f:Z

    .line 168430
    iget-object v0, p1, LX/108;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->g:Ljava/lang/String;

    .line 168431
    iget v0, p1, LX/108;->i:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    .line 168432
    iget-object v0, p1, LX/108;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    .line 168433
    iget v0, p1, LX/108;->h:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->j:I

    .line 168434
    iget-object v0, p1, LX/108;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    .line 168435
    iget-boolean v0, p1, LX/108;->d:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 168436
    iget-boolean v0, p1, LX/108;->k:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    .line 168437
    iget v0, p1, LX/108;->l:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    .line 168438
    iget v0, p1, LX/108;->m:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->m:I

    .line 168439
    iget-boolean v0, p1, LX/108;->n:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->n:Z

    .line 168440
    iget v0, p1, LX/108;->o:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    .line 168441
    iget-boolean v0, p1, LX/108;->p:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->o:Z

    .line 168442
    iget-boolean v0, p1, LX/108;->q:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    .line 168443
    iget v0, p1, LX/108;->r:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    .line 168444
    iget v0, p1, LX/108;->s:I

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->s:I

    .line 168445
    iget-boolean v0, p1, LX/108;->t:Z

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->q:Z

    .line 168446
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 168401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168402
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    .line 168403
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    .line 168404
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    .line 168405
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->e:Z

    .line 168406
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->f:Z

    .line 168407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->g:Ljava/lang/String;

    .line 168408
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    .line 168409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    .line 168410
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->j:I

    .line 168411
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    .line 168412
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    .line 168413
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->m:I

    .line 168414
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->n:Z

    .line 168415
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    .line 168416
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->o:Z

    .line 168417
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    .line 168418
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    .line 168419
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->s:I

    .line 168420
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    .line 168421
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 168422
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->q:Z

    .line 168423
    return-void
.end method

.method public static a()LX/108;
    .locals 2

    .prologue
    .line 168400
    new-instance v0, LX/108;

    invoke-direct {v0}, LX/108;-><init>()V

    return-object v0
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1
    .param p0    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 168453
    if-nez p0, :cond_0

    .line 168454
    const/4 v0, 0x0

    .line 168455
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 168399
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168393
    if-ne p0, p1, :cond_1

    .line 168394
    :cond_0
    :goto_0
    return v0

    .line 168395
    :cond_1
    instance-of v2, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-nez v2, :cond_2

    move v0, v1

    .line 168396
    goto :goto_0

    .line 168397
    :cond_2
    check-cast p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 168398
    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v3}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->e:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->e:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->f:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->f:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->j:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->j:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->m:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->m:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->n:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->n:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->o:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->o:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->s:I

    iget v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->s:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->q:Z

    iget-boolean v3, p1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->q:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 168392
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-boolean v2, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->q:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 168372
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168373
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168374
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168375
    iget-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 168376
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168377
    iget-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 168378
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168379
    iget-object v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 168380
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168381
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168382
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168383
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168384
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->o:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168385
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168386
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168387
    iget v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->s:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168388
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168389
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168390
    iget-boolean v0, p0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->q:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 168391
    return-void
.end method
