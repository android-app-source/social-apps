.class public Lcom/facebook/widget/CustomViewPager;
.super Landroid/support/v4/view/ViewPager;
.source ""

# interfaces
.implements LX/0hY;
.implements LX/0h1;


# instance fields
.field public a:Z

.field private b:Z

.field private c:Z

.field public d:LX/0ha;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 116304
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 116305
    iput-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 116306
    iput-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->b:Z

    .line 116307
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->c:Z

    .line 116308
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/CustomViewPager;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116309
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 116310
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116311
    iput-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 116312
    iput-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->b:Z

    .line 116313
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->c:Z

    .line 116314
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewPager;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116315
    return-void
.end method

.method private a([Landroid/view/View;)I
    .locals 9

    .prologue
    .line 116316
    invoke-direct {p0}, Lcom/facebook/widget/CustomViewPager;->g()LX/4nu;

    move-result-object v4

    .line 116317
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getPaddingLeft()I

    move-result v1

    add-int v5, v0, v1

    .line 116318
    sget-object v0, LX/4nu;->RIGHT:LX/4nu;

    if-ne v4, v0, :cond_1

    const v0, 0x7fffffff

    .line 116319
    :goto_0
    const/4 v2, -0x1

    .line 116320
    const/4 v1, 0x0

    move v8, v1

    move v1, v2

    move v2, v0

    move v0, v8

    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 116321
    aget-object v3, p1, v0

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 116322
    sget-object v6, LX/4nt;->a:[I

    invoke-virtual {v4}, LX/4nu;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 116323
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 116324
    :cond_1
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 116325
    :pswitch_0
    if-ne v3, v5, :cond_0

    .line 116326
    :goto_3
    return v0

    .line 116327
    :pswitch_1
    if-ge v3, v5, :cond_0

    if-le v3, v2, :cond_0

    move v1, v0

    move v2, v3

    .line 116328
    goto :goto_2

    .line 116329
    :pswitch_2
    if-le v3, v5, :cond_0

    if-ge v3, v2, :cond_0

    move v1, v0

    move v2, v3

    .line 116330
    goto :goto_2

    :cond_2
    move v0, v1

    .line 116331
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 116332
    if-eqz p2, :cond_0

    .line 116333
    sget-object v0, LX/03r;->CustomViewPager:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 116334
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 116335
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/CustomViewPager;->b:Z

    .line 116336
    const/16 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/CustomViewPager;->c:Z

    .line 116337
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 116338
    :cond_0
    return-void
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 116339
    if-lez p0, :cond_0

    const v0, 0xffff

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()LX/4nu;
    .locals 4

    .prologue
    .line 116340
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 116341
    iget v1, p0, Landroid/support/v4/view/ViewPager;->c:I

    move v1, v1

    .line 116342
    iget v2, p0, Landroid/support/v4/view/ViewPager;->d:F

    move v2, v2

    .line 116343
    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 116344
    sget-object v0, LX/4nu;->CENTER:LX/4nu;

    .line 116345
    :goto_0
    return-object v0

    .line 116346
    :cond_0
    if-le v0, v1, :cond_1

    .line 116347
    sget-object v0, LX/4nu;->RIGHT:LX/4nu;

    goto :goto_0

    .line 116348
    :cond_1
    sget-object v0, LX/4nu;->LEFT:LX/4nu;

    goto :goto_0
.end method

.method private getSortedChildren()[Landroid/view/View;
    .locals 4

    .prologue
    .line 116349
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getChildCount()I

    move-result v1

    .line 116350
    new-array v2, v1, [Landroid/view/View;

    .line 116351
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 116352
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v0

    .line 116353
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116354
    :cond_0
    new-instance v0, LX/4ns;

    invoke-direct {v0, p0}, LX/4ns;-><init>(Lcom/facebook/widget/CustomViewPager;)V

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 116355
    return-object v2
.end method


# virtual methods
.method public final a(I)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 116356
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->b:Z

    if-eqz v0, :cond_1

    .line 116357
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->a(I)Z

    move-result v4

    .line 116358
    :cond_0
    :goto_0
    return v4

    .line 116359
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewPager;->b(I)Landroid/view/View;

    move-result-object v0

    .line 116360
    if-nez v0, :cond_2

    .line 116361
    const-string v0, "CustomViewPager"

    const-string v1, "arrowScroll tried to scroll when there was no current child."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 116362
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->findFocus()Landroid/view/View;

    move-result-object v3

    .line 116363
    if-ne v3, v0, :cond_4

    move-object v1, v2

    .line 116364
    :goto_1
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_7

    .line 116365
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, v1, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 116366
    if-eqz v0, :cond_8

    if-eq v0, v1, :cond_8

    .line 116367
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    :goto_2
    move v4, v0

    .line 116368
    :cond_3
    :goto_3
    if-eqz v4, :cond_0

    .line 116369
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewPager;->playSoundEffect(I)V

    goto :goto_0

    .line 116370
    :cond_4
    if-eqz v3, :cond_9

    .line 116371
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :goto_4
    instance-of v6, v1, Landroid/view/ViewGroup;

    if-eqz v6, :cond_a

    .line 116372
    if-ne v1, v0, :cond_5

    move v1, v5

    .line 116373
    :goto_5
    if-nez v1, :cond_9

    .line 116374
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 116375
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116376
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :goto_6
    instance-of v3, v1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_6

    .line 116377
    const-string v3, " => "

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116378
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_6

    .line 116379
    :cond_5
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_4

    .line 116380
    :cond_6
    const-string v1, "CustomViewPager"

    const-string v3, "arrowScroll tried to find focus based on non-child current focused view %s"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v1, v3, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    .line 116381
    goto :goto_1

    .line 116382
    :cond_7
    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_3

    .line 116383
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v4

    goto :goto_3

    :cond_8
    move v0, v4

    goto :goto_2

    :cond_9
    move-object v1, v3

    goto/16 :goto_1

    :cond_a
    move v1, v4

    goto :goto_5
.end method

.method public a(LX/31M;II)Z
    .locals 2

    .prologue
    .line 116384
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, LX/31M;->RIGHT:LX/31M;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    sget-object v0, LX/31M;->LEFT:LX/31M;

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ZIII)Z
    .locals 1

    .prologue
    .line 116385
    check-cast p1, Landroid/view/ViewGroup;

    if-lez p3, :cond_0

    sget-object v0, LX/31M;->RIGHT:LX/31M;

    :goto_0
    invoke-static {p1, v0, p4, p5}, LX/3BA;->a(Landroid/view/ViewGroup;LX/31M;II)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, LX/31M;->LEFT:LX/31M;

    goto :goto_0
.end method

.method public final attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 116386
    instance-of v0, p1, LX/2ea;

    if-eqz v0, :cond_0

    .line 116387
    invoke-static {p0, p1, p2}, LX/4oH;->a(LX/0h1;Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116388
    :goto_0
    return-void

    .line 116389
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/ViewPager;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 116390
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->requestLayout()V

    goto :goto_0
.end method

.method public final b(I)Landroid/view/View;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 116391
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    .line 116392
    if-nez v1, :cond_1

    .line 116393
    :cond_0
    :goto_0
    return-object v0

    .line 116394
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/CustomViewPager;->getSortedChildren()[Landroid/view/View;

    move-result-object v1

    .line 116395
    invoke-direct {p0, v1}, Lcom/facebook/widget/CustomViewPager;->a([Landroid/view/View;)I

    move-result v2

    .line 116396
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 116397
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    .line 116398
    if-ne v3, p1, :cond_2

    .line 116399
    aget-object v0, v1, v2

    goto :goto_0

    .line 116400
    :cond_2
    const/4 v4, 0x0

    sub-int v2, v3, v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 116401
    sub-int v2, p1, v2

    .line 116402
    if-ltz v2, :cond_0

    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 116403
    aget-object v0, v1, v2

    goto :goto_0
.end method

.method public b(IZ)V
    .locals 1

    .prologue
    .line 116299
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 116300
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 116301
    iput-boolean p2, p0, Lcom/facebook/widget/CustomViewPager;->c:Z

    .line 116302
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116303
    return-void
.end method

.method public final detachRecyclableViewFromParent(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 116404
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->detachViewFromParent(Landroid/view/View;)V

    .line 116405
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->requestLayout()V

    .line 116406
    return-void
.end method

.method public getAllowDpadPaging()Z
    .locals 1

    .prologue
    .line 116240
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->b:Z

    return v0
.end method

.method public getInitializeHeightToFirstItem()Z
    .locals 1

    .prologue
    .line 116241
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->c:Z

    return v0
.end method

.method public getIsSwipingEnabled()Z
    .locals 1

    .prologue
    .line 116298
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    return v0
.end method

.method public getMeasuredHeightOfFirstItem()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 116242
    invoke-virtual {p0, v4}, Lcom/facebook/widget/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 116243
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getMeasuredWidth()I

    move-result v2

    .line 116244
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 116245
    invoke-static {v0}, Lcom/facebook/widget/CustomViewPager;->c(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 116246
    :goto_0
    return v0

    .line 116247
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getPaddingLeft()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 116248
    const/high16 v2, -0x80000000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 116249
    invoke-virtual {v1, v0, v4}, Landroid/view/View;->measure(II)V

    .line 116250
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6999f906

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 116251
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->onAttachedToWindow()V

    .line 116252
    iget-object v1, p0, Lcom/facebook/widget/CustomViewPager;->d:LX/0ha;

    if-eqz v1, :cond_0

    .line 116253
    iget-object v1, p0, Lcom/facebook/widget/CustomViewPager;->d:LX/0ha;

    invoke-interface {v1}, LX/0ha;->a()V

    .line 116254
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x6c1b84ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x453eade

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 116255
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->onDetachedFromWindow()V

    .line 116256
    iget-object v1, p0, Lcom/facebook/widget/CustomViewPager;->d:LX/0ha;

    if-eqz v1, :cond_0

    .line 116257
    iget-object v1, p0, Lcom/facebook/widget/CustomViewPager;->d:LX/0ha;

    invoke-interface {v1}, LX/0ha;->b()V

    .line 116258
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x73746c5e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 116259
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    if-eqz v0, :cond_0

    .line 116260
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 116261
    :goto_0
    return v0

    .line 116262
    :catch_0
    move-exception v0

    .line 116263
    const-class v1, Lcom/facebook/widget/CustomViewPager;

    const-string v2, "ViewPager threw an IllegalArgumentException. "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 3

    .prologue
    .line 116265
    const-string v0, "CustomViewPager.onMeasure"

    const v1, 0x735eef7e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 116266
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->onMeasure(II)V

    .line 116267
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getChildCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 116268
    :cond_0
    const v0, -0x38079bd7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 116269
    :goto_0
    return-void

    .line 116270
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 116271
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 116272
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 116273
    invoke-static {v0}, Lcom/facebook/widget/CustomViewPager;->c(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116274
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v0, v1

    invoke-virtual {p0, v2, v0}, Lcom/facebook/widget/CustomViewPager;->setMeasuredDimension(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116275
    const v0, 0x46bdf53b

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 116276
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewPager;->getMeasuredHeightOfFirstItem()I

    move-result v0

    add-int/2addr v0, v1

    .line 116277
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 116278
    invoke-super {p0, p1, v0}, Landroid/support/v4/view/ViewPager;->onMeasure(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 116279
    const v0, -0x4b5c3bda

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x1f49437b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0xd9e7ea4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 116280
    iget-boolean v0, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    if-eqz v0, :cond_0

    .line 116281
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const v2, 0x31dd2566

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 116282
    :goto_0
    return v0

    .line 116283
    :catch_0
    move-exception v0

    .line 116284
    const-class v2, Lcom/facebook/widget/CustomViewPager;

    const-string v3, "ViewPager threw an IllegalArgumentException. "

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116285
    :cond_0
    const/4 v0, 0x0

    const v2, 0x549fd87d

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final removeRecyclableViewFromParent(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 116286
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->removeDetachedView(Landroid/view/View;Z)V

    .line 116287
    return-void
.end method

.method public final requestLayout()V
    .locals 2

    .prologue
    .line 116288
    const-string v0, "CustomViewPager.requestLayout"

    const v1, -0x57adb2a5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 116289
    const v0, -0x64b7c18a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 116290
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 116291
    return-void
.end method

.method public setAllowDpadPaging(Z)V
    .locals 0

    .prologue
    .line 116292
    iput-boolean p1, p0, Lcom/facebook/widget/CustomViewPager;->b:Z

    .line 116293
    return-void
.end method

.method public setIsSwipingEnabled(Z)V
    .locals 0

    .prologue
    .line 116294
    iput-boolean p1, p0, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 116295
    return-void
.end method

.method public setOnAttachStateChangeListener(LX/0ha;)V
    .locals 0

    .prologue
    .line 116296
    iput-object p1, p0, Lcom/facebook/widget/CustomViewPager;->d:LX/0ha;

    .line 116297
    return-void
.end method
