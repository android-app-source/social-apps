.class public Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;
.super LX/1OD;
.source ""

# interfaces
.implements LX/1Rr;
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1KR;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 247085
    invoke-direct {p0}, LX/1OD;-><init>()V

    .line 247086
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->a:Landroid/os/Handler;

    .line 247087
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b:Ljava/util/List;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 247081
    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->e:Z

    if-eqz v0, :cond_1

    .line 247082
    :cond_0
    :goto_0
    return-void

    .line 247083
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->a:Landroid/os/Handler;

    const v1, -0x52c1f44e

    invoke-static {v0, p0, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 247084
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->d:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 247079
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b()V

    .line 247080
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 247077
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b()V

    .line 247078
    return-void
.end method

.method public final a(III)V
    .locals 0

    .prologue
    .line 247075
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b()V

    .line 247076
    return-void
.end method

.method public final a(LX/1KR;)V
    .locals 2

    .prologue
    .line 247070
    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->c:Z

    if-eqz v0, :cond_0

    .line 247071
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t register observer during onChanged()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247072
    :cond_0
    if-eqz p1, :cond_1

    .line 247073
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247074
    :cond_1
    return-void
.end method

.method public final b(II)V
    .locals 0

    .prologue
    .line 247047
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b()V

    .line 247048
    return-void
.end method

.method public final b(LX/1KR;)V
    .locals 2

    .prologue
    .line 247065
    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->c:Z

    if-eqz v0, :cond_0

    .line 247066
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t unregister observer during onChanged()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247067
    :cond_0
    if-eqz p1, :cond_1

    .line 247068
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 247069
    :cond_1
    return-void
.end method

.method public final c(II)V
    .locals 0

    .prologue
    .line 247063
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b()V

    .line 247064
    return-void
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 247061
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->e:Z

    .line 247062
    return-void
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 247060
    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->e:Z

    return v0
.end method

.method public final run()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 247049
    const-string v1, "BaseHasNotifyOnceAdapterObservers.run"

    const v2, 0x5e960d9a

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 247050
    :try_start_0
    iget-boolean v1, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 247051
    const v0, -0x5a1a6425

    invoke-static {v0}, LX/02m;->a(I)V

    .line 247052
    :goto_0
    return-void

    .line 247053
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->c:Z

    move v1, v0

    .line 247054
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 247055
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1KR;

    invoke-interface {v0}, LX/1KR;->a()V

    .line 247056
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 247057
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->c:Z

    .line 247058
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247059
    const v0, -0x625d4242

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x20f57888

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
