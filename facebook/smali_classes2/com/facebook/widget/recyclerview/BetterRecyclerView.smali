.class public Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# instance fields
.field public A:I

.field private B:I

.field public C:Z

.field public D:LX/1OU;

.field private E:LX/1OY;

.field public h:LX/0Sy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:Landroid/os/Handler;

.field private final j:LX/1OB;

.field private final k:LX/1OC;

.field public final l:Landroid/view/GestureDetector;

.field public final m:Landroid/view/GestureDetector;

.field private final n:LX/1OH;

.field private final o:LX/1OH;

.field private final p:LX/1OK;

.field private final q:LX/1OK;

.field private r:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/1OT;

.field public t:LX/1OZ;

.field public u:LX/1Oa;

.field public v:LX/1OV;

.field private w:LX/1OW;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 238853
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 238854
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    .line 238855
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->j:LX/1OB;

    .line 238856
    new-instance v0, LX/1OC;

    invoke-direct {v0, p0}, LX/1OC;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->k:LX/1OC;

    .line 238857
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/1OE;

    invoke-direct {v2, p0}, LX/1OE;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iget-object v3, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->l:Landroid/view/GestureDetector;

    .line 238858
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/1OF;

    invoke-direct {v2, p0}, LX/1OF;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iget-object v3, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->m:Landroid/view/GestureDetector;

    .line 238859
    new-instance v0, LX/1OG;

    invoke-direct {v0, p0}, LX/1OG;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->n:LX/1OH;

    .line 238860
    new-instance v0, LX/1OI;

    invoke-direct {v0, p0}, LX/1OI;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o:LX/1OH;

    .line 238861
    new-instance v0, LX/1OJ;

    invoke-direct {v0, p0}, LX/1OJ;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p:LX/1OK;

    .line 238862
    new-instance v0, LX/1OL;

    invoke-direct {v0, p0}, LX/1OL;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->q:LX/1OK;

    .line 238863
    iput v4, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->A:I

    .line 238864
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o()V

    .line 238865
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 238866
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 238867
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    .line 238868
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->j:LX/1OB;

    .line 238869
    new-instance v0, LX/1OC;

    invoke-direct {v0, p0}, LX/1OC;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->k:LX/1OC;

    .line 238870
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/1OE;

    invoke-direct {v2, p0}, LX/1OE;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iget-object v3, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->l:Landroid/view/GestureDetector;

    .line 238871
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/1OF;

    invoke-direct {v2, p0}, LX/1OF;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iget-object v3, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->m:Landroid/view/GestureDetector;

    .line 238872
    new-instance v0, LX/1OG;

    invoke-direct {v0, p0}, LX/1OG;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->n:LX/1OH;

    .line 238873
    new-instance v0, LX/1OI;

    invoke-direct {v0, p0}, LX/1OI;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o:LX/1OH;

    .line 238874
    new-instance v0, LX/1OJ;

    invoke-direct {v0, p0}, LX/1OJ;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p:LX/1OK;

    .line 238875
    new-instance v0, LX/1OL;

    invoke-direct {v0, p0}, LX/1OL;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->q:LX/1OK;

    .line 238876
    iput v4, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->A:I

    .line 238877
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o()V

    .line 238878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 238879
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 238880
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    .line 238881
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->j:LX/1OB;

    .line 238882
    new-instance v0, LX/1OC;

    invoke-direct {v0, p0}, LX/1OC;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->k:LX/1OC;

    .line 238883
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/1OE;

    invoke-direct {v2, p0}, LX/1OE;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iget-object v3, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->l:Landroid/view/GestureDetector;

    .line 238884
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/1OF;

    invoke-direct {v2, p0}, LX/1OF;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iget-object v3, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->i:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->m:Landroid/view/GestureDetector;

    .line 238885
    new-instance v0, LX/1OG;

    invoke-direct {v0, p0}, LX/1OG;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->n:LX/1OH;

    .line 238886
    new-instance v0, LX/1OI;

    invoke-direct {v0, p0}, LX/1OI;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o:LX/1OH;

    .line 238887
    new-instance v0, LX/1OJ;

    invoke-direct {v0, p0}, LX/1OJ;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p:LX/1OK;

    .line 238888
    new-instance v0, LX/1OL;

    invoke-direct {v0, p0}, LX/1OL;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->q:LX/1OK;

    .line 238889
    iput v4, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->A:I

    .line 238890
    invoke-direct {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o()V

    .line 238891
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v0

    check-cast v0, LX/0Sy;

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->h:LX/0Sy;

    return-void
.end method

.method private static a(LX/1OM;)Z
    .locals 2
    .param p0    # LX/1OM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 238892
    if-nez p0, :cond_1

    .line 238893
    :cond_0
    :goto_0
    return v0

    .line 238894
    :cond_1
    instance-of v1, p0, LX/1ON;

    if-eqz v1, :cond_2

    .line 238895
    check-cast p0, LX/1ON;

    .line 238896
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 238897
    goto :goto_0

    .line 238898
    :cond_2
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private o()V
    .locals 1

    .prologue
    .line 238899
    const-class v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-static {v0, p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 238900
    invoke-super {p0}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->B:I

    .line 238901
    return-void
.end method

.method public static p(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    .line 238902
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->r:Landroid/view/View;

    if-nez v0, :cond_0

    .line 238903
    iget v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->B:I

    invoke-super {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 238904
    :goto_0
    return-void

    .line 238905
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 238906
    invoke-static {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/1OM;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 238907
    :goto_1
    iget-object v3, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->r:Landroid/view/View;

    if-eqz v0, :cond_2

    iget v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->B:I

    :goto_2
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 238908
    if-eqz v0, :cond_3

    :goto_3
    invoke-super {p0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 238909
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 238910
    goto :goto_2

    .line 238911
    :cond_3
    iget v2, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->B:I

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0fu;)V
    .locals 1

    .prologue
    .line 238912
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->j:LX/1OB;

    invoke-virtual {v0, p1}, LX/1OB;->a(LX/0fu;)V

    .line 238913
    return-void
.end method

.method public final canScrollVertically(I)Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ImprovedNewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 238949
    if-gez p1, :cond_4

    .line 238950
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getClipToPadding()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 238951
    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v2

    instance-of v2, v2, LX/1OS;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v2

    invoke-interface {v2}, LX/1OS;->I()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-ge v2, v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    .line 238952
    :cond_2
    :goto_1
    return v1

    .line 238953
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingTop()I

    move-result v0

    goto :goto_0

    .line 238954
    :cond_4
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v1

    goto :goto_1
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 238914
    const-string v0, "BetterRecyclerView.offsetChildrenVertical"

    const v1, 0x747212d0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 238915
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238916
    const v0, 0x3293dbcd

    invoke-static {v0}, LX/02m;->a(I)V

    .line 238917
    return-void

    .line 238918
    :catchall_0
    move-exception v0

    const v1, -0x7a977fa2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 238919
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 238920
    instance-of v0, v0, LX/0Vf;

    if-eqz v0, :cond_0

    .line 238921
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 238922
    check-cast v0, LX/0Vf;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238923
    const/4 v0, 0x0

    .line 238924
    :goto_0
    return v0

    .line 238925
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->s:LX/1OT;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 238926
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->s:LX/1OT;

    invoke-interface {v0}, LX/1OT;->a()V

    .line 238927
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 238929
    const-string v0, "BetterRecyclerView.draw"

    const v1, -0x5c820b6a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 238930
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238931
    :try_start_1
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->j:LX/1OB;

    invoke-virtual {v0}, LX/1OB;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238932
    const v0, 0x74606436

    invoke-static {v0}, LX/02m;->a(I)V

    .line 238933
    return-void

    .line 238934
    :catch_0
    move-exception v1

    .line 238935
    :try_start_2
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v2

    .line 238936
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 238937
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 238938
    invoke-virtual {p0, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238939
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238940
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Expected:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Children:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 238941
    :catchall_0
    move-exception v0

    const v1, 0x286a2e4e    # 1.299965E-14f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final g(II)V
    .locals 1

    .prologue
    .line 238942
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1OS;->d(II)V

    .line 238943
    return-void
.end method

.method public getBetterLayoutManager()LX/1OS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/1OS;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 238944
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    instance-of v0, v0, LX/1OS;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 238945
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1OS;

    return-object v0
.end method

.method public getClipToPadding()Z
    .locals 2

    .prologue
    .line 238946
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 238947
    invoke-super {p0}, Landroid/support/v7/widget/RecyclerView;->getClipToPadding()Z

    move-result v0

    .line 238948
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->x:Z

    goto :goto_0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 238851
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v0

    .line 238852
    invoke-interface {v0}, LX/1OS;->n()I

    move-result v0

    return v0
.end method

.method public getRecyclerListener()LX/1OU;
    .locals 1

    .prologue
    .line 238928
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->D:LX/1OU;

    return-object v0
.end method

.method public getRecyclerViewVisibility()I
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 238782
    invoke-super {p0}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v0

    return v0
.end method

.method public getVisibility()I
    .locals 1

    .prologue
    .line 238783
    iget v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->B:I

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 238784
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOverScrollMode(I)V

    .line 238785
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 238786
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->j:LX/1OB;

    invoke-virtual {v0}, LX/1OB;->b()V

    .line 238787
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 238788
    const/4 v0, 0x0

    .line 238789
    iget-object v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->v:LX/1OV;

    if-eqz v1, :cond_0

    .line 238790
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->v:LX/1OV;

    invoke-interface {v0, p1}, LX/1OV;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 238791
    :cond_0
    if-nez v0, :cond_1

    .line 238792
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 238793
    :cond_1
    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 238794
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/RecyclerView;->onLayout(ZIIII)V

    .line 238795
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->C:Z

    .line 238796
    return-void
.end method

.method public setAdapter(LX/1OM;)V
    .locals 2

    .prologue
    .line 238839
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 238840
    if-eqz v0, :cond_0

    .line 238841
    iget-object v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->k:LX/1OC;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1OD;)V

    .line 238842
    iget-object v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p:LX/1OK;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1OD;)V

    .line 238843
    iget-object v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->q:LX/1OK;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1OD;)V

    .line 238844
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 238845
    if-eqz p1, :cond_1

    .line 238846
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p:LX/1OK;

    invoke-virtual {p1, v0}, LX/1OM;->a(LX/1OD;)V

    .line 238847
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->k:LX/1OC;

    invoke-virtual {p1, v0}, LX/1OM;->a(LX/1OD;)V

    .line 238848
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->q:LX/1OK;

    invoke-virtual {p1, v0}, LX/1OM;->a(LX/1OD;)V

    .line 238849
    :cond_1
    invoke-static {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 238850
    return-void
.end method

.method public setBroadcastInteractionChanges(Z)V
    .locals 2

    .prologue
    .line 238797
    if-eqz p1, :cond_1

    .line 238798
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->w:LX/1OW;

    if-nez v0, :cond_0

    .line 238799
    new-instance v0, LX/1OW;

    invoke-direct {v0, p0}, LX/1OW;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->w:LX/1OW;

    .line 238800
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->w:LX/1OW;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 238801
    :goto_0
    return-void

    .line 238802
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->w:LX/1OW;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    goto :goto_0
.end method

.method public setClipToPadding(Z)V
    .locals 0

    .prologue
    .line 238803
    iput-boolean p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->x:Z

    .line 238804
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setClipToPadding(Z)V

    .line 238805
    return-void
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 238806
    iput-object p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->r:Landroid/view/View;

    .line 238807
    invoke-static {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 238808
    return-void
.end method

.method public setInterceptTouchEventListener(LX/1OV;)V
    .locals 0

    .prologue
    .line 238809
    iput-object p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->v:LX/1OV;

    .line 238810
    return-void
.end method

.method public setLayoutChangesListener(LX/1OY;)V
    .locals 0

    .prologue
    .line 238780
    iput-object p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->E:LX/1OY;

    .line 238781
    return-void
.end method

.method public setOnItemClickListener(LX/1OZ;)V
    .locals 1
    .param p1    # LX/1OZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 238811
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->y:Z

    if-eqz v0, :cond_0

    .line 238812
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->n:LX/1OH;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OH;)V

    .line 238813
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->y:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 238814
    iget-object v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->n:LX/1OH;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OH;)V

    .line 238815
    :cond_1
    iput-object p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->t:LX/1OZ;

    .line 238816
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->y:Z

    .line 238817
    return-void

    .line 238818
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnItemLongClickListener(LX/1Oa;)V
    .locals 2
    .param p1    # LX/1Oa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 238819
    invoke-virtual {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->isLongClickable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 238820
    invoke-virtual {p0, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setLongClickable(Z)V

    .line 238821
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->z:Z

    if-eqz v1, :cond_1

    .line 238822
    iget-object v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o:LX/1OH;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OH;)V

    .line 238823
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->z:Z

    if-nez v1, :cond_2

    if-eqz p1, :cond_2

    .line 238824
    iget-object v1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->o:LX/1OH;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OH;)V

    .line 238825
    :cond_2
    iput-object p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->u:LX/1Oa;

    .line 238826
    if-eqz p1, :cond_3

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->z:Z

    .line 238827
    return-void

    .line 238828
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnTouchDownListener(LX/1OT;)V
    .locals 0

    .prologue
    .line 238829
    iput-object p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->s:LX/1OT;

    .line 238830
    return-void
.end method

.method public setRecyclerListener(LX/1OU;)V
    .locals 0

    .prologue
    .line 238831
    iput-object p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->D:LX/1OU;

    .line 238832
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setRecyclerListener(LX/1OU;)V

    .line 238833
    return-void
.end method

.method public setSelection(I)V
    .locals 0

    .prologue
    .line 238834
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 238835
    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    .prologue
    .line 238836
    iput p1, p0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->B:I

    .line 238837
    invoke-static {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->p(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 238838
    return-void
.end method
