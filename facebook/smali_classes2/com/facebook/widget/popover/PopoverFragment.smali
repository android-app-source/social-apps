.class public abstract Lcom/facebook/widget/popover/PopoverFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field private static final m:Ljava/lang/String;


# instance fields
.field public n:LX/8tn;

.field public o:LX/1nJ;

.field public p:Landroid/view/Window;

.field private q:Landroid/graphics/drawable/Drawable;

.field public r:Landroid/view/View;

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/8tf;

.field public u:Z

.field private v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115372
    const-class v0, Lcom/facebook/widget/popover/PopoverFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/popover/PopoverFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 115373
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 115374
    new-instance v0, LX/8tf;

    invoke-direct {v0, p0}, LX/8tf;-><init>(Lcom/facebook/widget/popover/PopoverFragment;)V

    iput-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->t:LX/8tf;

    .line 115375
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->v:Z

    .line 115376
    return-void
.end method

.method public static A(Lcom/facebook/widget/popover/PopoverFragment;)V
    .locals 2

    .prologue
    .line 115377
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115378
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a00e8

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 115379
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/widget/popover/PopoverFragment;LX/0gc;Z)V
    .locals 2

    .prologue
    .line 115380
    iput-boolean p2, p0, Lcom/facebook/widget/popover/PopoverFragment;->v:Z

    .line 115381
    invoke-virtual {p1}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 115382
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 115383
    :cond_0
    :goto_0
    return-void

    .line 115384
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 115385
    const-string v0, "chromeless:content:fragment:tag"

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 115386
    iget-boolean v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->v:Z

    if-eqz v0, :cond_0

    .line 115387
    invoke-virtual {p1}, LX/0gc;->b()Z

    .line 115388
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->r()Z

    move-result v1

    .line 115389
    iput-boolean v1, v0, LX/8tn;->v:Z

    .line 115390
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->u()LX/31M;

    move-result-object v1

    .line 115391
    iput-object v1, v0, LX/8tn;->A:LX/31M;

    .line 115392
    goto :goto_1

    .line 115393
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {v0}, LX/8tn;->d()V

    .line 115394
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->o:LX/1nJ;

    invoke-virtual {v0}, LX/1nJ;->b()V

    goto :goto_0
.end method


# virtual methods
.method public S_()Z
    .locals 2

    .prologue
    .line 115395
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_back_button"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 115396
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->l()V

    .line 115397
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115398
    new-instance v0, LX/8tg;

    invoke-direct {v0, p0}, LX/8tg;-><init>(Lcom/facebook/widget/popover/PopoverFragment;)V

    .line 115399
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->x()Z

    move-result v1

    if-nez v1, :cond_0

    .line 115400
    invoke-virtual {p0, v0}, Lcom/facebook/widget/popover/PopoverFragment;->a(Landroid/app/Dialog;)V

    .line 115401
    :cond_0
    return-object v0
.end method

.method public final a(LX/0gc;Landroid/view/Window;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 115402
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/facebook/widget/popover/PopoverFragment;->a(LX/0gc;Landroid/view/Window;Landroid/view/View;Z)V

    .line 115403
    return-void
.end method

.method public final a(LX/0gc;Landroid/view/Window;Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 115404
    invoke-virtual {p1}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 115405
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 115406
    :cond_0
    :goto_0
    return-void

    .line 115407
    :cond_1
    invoke-static {p0, p1, p4}, Lcom/facebook/widget/popover/PopoverFragment;->a(Lcom/facebook/widget/popover/PopoverFragment;LX/0gc;Z)V

    .line 115408
    iput-object p3, p0, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    .line 115409
    iput-object p2, p0, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    .line 115410
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 115411
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->q:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 115412
    invoke-static {p1}, LX/8ti;->a(Landroid/app/Dialog;)V

    .line 115413
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 115414
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    if-eqz v0, :cond_1

    .line 115415
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    .line 115416
    iget-object v1, v0, LX/8tn;->o:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    const-string p0, "In order to set the footer, the footer needs to be in the layout."

    invoke-static {v1, p0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 115417
    iget-object v1, v0, LX/8tn;->o:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_2

    .line 115418
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 115419
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 115420
    :cond_0
    iget-object v1, v0, LX/8tn;->o:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 115421
    :cond_1
    return-void

    .line 115422
    :cond_2
    iget-object v1, v0, LX/8tn;->o:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0
.end method

.method public eK_()I
    .locals 1

    .prologue
    .line 115423
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0e05f9

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0e05fb

    goto :goto_0
.end method

.method public k()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 115424
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->o:LX/1nJ;

    invoke-virtual {v0}, LX/1nJ;->c()V

    .line 115425
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 115426
    if-eqz v0, :cond_0

    .line 115427
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115428
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 115429
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setScaleX(F)V

    .line 115430
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setScaleY(F)V

    .line 115431
    :cond_1
    return-void

    .line 115432
    :catch_0
    move-exception v0

    .line 115433
    sget-object v1, Lcom/facebook/widget/popover/PopoverFragment;->m:Ljava/lang/String;

    const-string v2, "Null pointer exception while trying to dismiss the popover"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final l()V
    .locals 5

    .prologue
    .line 115434
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->u:Z

    .line 115435
    iget-object v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    .line 115436
    iget-boolean v1, v0, LX/8tn;->v:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/8tn;->B:LX/31M;

    if-eqz v1, :cond_0

    .line 115437
    iget-object v1, v0, LX/8tn;->B:LX/31M;

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4}, LX/8tn;->a(LX/8tn;LX/31M;D)V

    .line 115438
    :goto_0
    return-void

    .line 115439
    :cond_0
    iget-object v1, v0, LX/8tn;->j:LX/8qU;

    invoke-virtual {v1}, LX/8qU;->d()V

    goto :goto_0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 115293
    const v0, 0x7f030feb

    return v0
.end method

.method public o()LX/8qU;
    .locals 1

    .prologue
    .line 115371
    new-instance v0, LX/8qY;

    invoke-direct {v0, p0}, LX/8qY;-><init>(Lcom/facebook/widget/popover/PopoverFragment;)V

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 115440
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 115441
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->k()V

    .line 115442
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 115288
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 115289
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 115290
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->x()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115291
    invoke-virtual {p0, v0}, Lcom/facebook/widget/popover/PopoverFragment;->a(Landroid/app/Dialog;)V

    .line 115292
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x54a6fb99    # -7.7099996E-13f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115294
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 115295
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/widget/popover/PopoverFragment;

    invoke-static {p1}, LX/1nJ;->a(LX/0QB;)LX/1nJ;

    move-result-object v4

    check-cast v4, LX/1nJ;

    const/16 v1, 0x97

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v4, p0, Lcom/facebook/widget/popover/PopoverFragment;->o:LX/1nJ;

    iput-object p1, p0, Lcom/facebook/widget/popover/PopoverFragment;->s:LX/0Ot;

    .line 115296
    const/16 v1, 0x2b

    const v2, 0x3fabe20c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x420451af

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115297
    new-instance v1, LX/8tn;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->n()I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/8tn;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->o()LX/8qU;

    move-result-object v2

    .line 115298
    iput-object v2, v1, LX/8tn;->j:LX/8qU;

    .line 115299
    move-object v1, v1

    .line 115300
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->r()Z

    move-result v2

    .line 115301
    iput-boolean v2, v1, LX/8tn;->v:Z

    .line 115302
    move-object v1, v1

    .line 115303
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->q()Z

    move-result v2

    .line 115304
    iput-boolean v2, v1, LX/8tn;->w:Z

    .line 115305
    move-object v1, v1

    .line 115306
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->x()Z

    move-result v2

    move v2, v2

    .line 115307
    iput-boolean v2, v1, LX/8tn;->x:Z

    .line 115308
    if-eqz v2, :cond_0

    .line 115309
    iget-object p1, v1, LX/8tn;->l:Landroid/graphics/drawable/Drawable;

    iget-boolean v3, v1, LX/8tn;->w:Z

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p1, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 115310
    :cond_0
    move-object v1, v1

    .line 115311
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->s()I

    move-result v2

    .line 115312
    iput v2, v1, LX/8tn;->y:I

    .line 115313
    iget-object v3, v1, LX/8tn;->b:LX/1wz;

    iget p1, v1, LX/8tn;->y:I

    .line 115314
    iput p1, v3, LX/1wz;->p:I

    .line 115315
    move-object v1, v1

    .line 115316
    iput-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    .line 115317
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->r()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115318
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->t()I

    move-result v2

    .line 115319
    iput v2, v1, LX/8tn;->z:I

    .line 115320
    move-object v1, v1

    .line 115321
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->u()LX/31M;

    move-result-object v2

    .line 115322
    iput-object v2, v1, LX/8tn;->A:LX/31M;

    .line 115323
    move-object v1, v1

    .line 115324
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->v()LX/31M;

    move-result-object v2

    .line 115325
    iput-object v2, v1, LX/8tn;->B:LX/31M;

    .line 115326
    move-object v1, v1

    .line 115327
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->w()D

    move-result-wide v2

    .line 115328
    iput-wide v2, v1, LX/8tn;->C:D

    .line 115329
    move-object v1, v1

    .line 115330
    const-wide/high16 v5, 0x3fd0000000000000L    # 0.25

    move-wide v2, v5

    .line 115331
    iput-wide v2, v1, LX/8tn;->D:D

    .line 115332
    move-object v1, v1

    .line 115333
    iget-object v2, p0, Lcom/facebook/widget/popover/PopoverFragment;->t:LX/8tf;

    .line 115334
    iput-object v2, v1, LX/8tn;->k:LX/0xi;

    .line 115335
    :cond_1
    goto :goto_1

    .line 115336
    :goto_1
    iget-boolean v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->v:Z

    if-nez v1, :cond_2

    .line 115337
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->r()Z

    move-result v2

    .line 115338
    iput-boolean v2, v1, LX/8tn;->v:Z

    .line 115339
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->u()LX/31M;

    move-result-object v2

    .line 115340
    iput-object v2, v1, LX/8tn;->A:LX/31M;

    .line 115341
    goto :goto_2

    .line 115342
    :goto_2
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {v1}, LX/8tn;->d()V

    .line 115343
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->o:LX/1nJ;

    invoke-virtual {v1}, LX/1nJ;->b()V

    .line 115344
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    const/16 v2, 0x2b

    const v3, 0x1e76c2f7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    .line 115345
    :cond_3
    const/16 v3, 0xb2

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3340bfa8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115346
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 115347
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 115348
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/popover/PopoverFragment;->q:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 115349
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    const/4 v2, 0x0

    .line 115350
    iput-object v2, v1, LX/8tn;->j:LX/8qU;

    .line 115351
    const/16 v1, 0x2b

    const v2, -0x584cc9f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x23b939c1    # 2.00822E-17f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115352
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 115353
    iget-object v1, p0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    const/4 v2, 0x0

    .line 115354
    iput-object v2, v1, LX/8tn;->k:LX/0xi;

    .line 115355
    const/16 v1, 0x2b

    const v2, 0x5a9a84e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x712dc91b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115356
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 115357
    invoke-static {p0}, Lcom/facebook/widget/popover/PopoverFragment;->A(Lcom/facebook/widget/popover/PopoverFragment;)V

    .line 115358
    const/16 v1, 0x2b

    const v2, -0x9165539

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 115359
    iget-boolean v0, p0, Lcom/facebook/widget/popover/PopoverFragment;->u:Z

    if-eqz v0, :cond_0

    .line 115360
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->k()V

    .line 115361
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 115362
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 115363
    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->x()Z

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 115364
    const/4 v0, 0x1

    return v0
.end method

.method public s()I
    .locals 2

    .prologue
    .line 115365
    sget-object v0, LX/31M;->UP:LX/31M;

    invoke-virtual {v0}, LX/31M;->flag()I

    move-result v0

    sget-object v1, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public t()I
    .locals 2

    .prologue
    .line 115366
    sget-object v0, LX/31M;->UP:LX/31M;

    invoke-virtual {v0}, LX/31M;->flag()I

    move-result v0

    sget-object v1, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public u()LX/31M;
    .locals 1

    .prologue
    .line 115367
    sget-object v0, LX/31M;->UP:LX/31M;

    return-object v0
.end method

.method public v()LX/31M;
    .locals 1

    .prologue
    .line 115368
    sget-object v0, LX/31M;->DOWN:LX/31M;

    return-object v0
.end method

.method public w()D
    .locals 2

    .prologue
    .line 115369
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    return-wide v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 115370
    const/4 v0, 0x1

    return v0
.end method
