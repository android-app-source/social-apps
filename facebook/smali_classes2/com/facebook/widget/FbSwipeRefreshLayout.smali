.class public Lcom/facebook/widget/FbSwipeRefreshLayout;
.super Landroid/support/v4/widget/SwipeRefreshLayout;
.source ""

# interfaces
.implements LX/1Nv;


# instance fields
.field private c:I

.field private d:F

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 238045
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 238046
    invoke-direct {p0, p1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->a(Landroid/content/Context;)V

    .line 238047
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 238064
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 238065
    invoke-direct {p0, p1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->a(Landroid/content/Context;)V

    .line 238066
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 238059
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/FbSwipeRefreshLayout;->c:I

    .line 238060
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0a00d1

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 238061
    const v0, 0x7f0a0048

    .line 238062
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeResource(I)V

    .line 238063
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 238051
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 238052
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 238053
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/FbSwipeRefreshLayout;->d:F

    .line 238054
    iput-boolean v0, p0, Lcom/facebook/widget/FbSwipeRefreshLayout;->e:Z

    goto :goto_0

    .line 238055
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 238056
    iget v2, p0, Lcom/facebook/widget/FbSwipeRefreshLayout;->d:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 238057
    iget-boolean v2, p0, Lcom/facebook/widget/FbSwipeRefreshLayout;->e:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/facebook/widget/FbSwipeRefreshLayout;->c:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 238058
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/FbSwipeRefreshLayout;->e:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 238048
    invoke-virtual {p0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 238049
    invoke-virtual {p0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 238050
    :cond_0
    return-void
.end method
