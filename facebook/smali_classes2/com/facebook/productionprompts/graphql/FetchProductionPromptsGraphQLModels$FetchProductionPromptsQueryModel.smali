.class public final Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x577e2a2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 310318
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 310351
    const-class v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 310349
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 310350
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 310341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 310342
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 310343
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 310344
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 310345
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 310346
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 310347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 310348
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 310328
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 310329
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310330
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    .line 310331
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 310332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    .line 310333
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    .line 310334
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 310335
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    .line 310336
    invoke-virtual {p0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 310337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    .line 310338
    iput-object v0, v1, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->f:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    .line 310339
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 310340
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 310326
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    .line 310327
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->e:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 310323
    new-instance v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-direct {v0}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;-><init>()V

    .line 310324
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 310325
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 310322
    const v0, 0x7dae79f2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 310321
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 310319
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->f:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    iput-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->f:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    .line 310320
    iget-object v0, p0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->f:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    return-object v0
.end method
