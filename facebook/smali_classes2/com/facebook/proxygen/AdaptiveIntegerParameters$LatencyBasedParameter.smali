.class public final Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final excellentValue:I

.field public final goodValue:I

.field public final moderateValue:I

.field public final poorValue:I

.field public final unknownValue:I


# direct methods
.method public constructor <init>(IIIII)V
    .locals 0

    .prologue
    .line 184169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184170
    iput p1, p0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;->excellentValue:I

    .line 184171
    iput p2, p0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;->goodValue:I

    .line 184172
    iput p3, p0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;->moderateValue:I

    .line 184173
    iput p4, p0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;->poorValue:I

    .line 184174
    iput p5, p0, Lcom/facebook/proxygen/AdaptiveIntegerParameters$LatencyBasedParameter;->unknownValue:I

    .line 184175
    return-void
.end method
