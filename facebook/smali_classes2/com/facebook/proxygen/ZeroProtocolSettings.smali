.class public Lcom/facebook/proxygen/ZeroProtocolSettings;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public aeads:[Ljava/lang/String;

.field public final cacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field public final enableTCPFastOpen:Z

.field public final enabled:Z

.field public final enforceExpiration:Z

.field public hostnamePolicy:Ljava/lang/String;

.field public final persistentCacheEnabled:Z

.field public final retryEnabled:Z

.field public final tlsFallback:I

.field public final zeroRttEnabled:Z


# direct methods
.method public constructor <init>(ZZZZLcom/facebook/proxygen/PersistentSSLCacheSettings;[Ljava/lang/String;Ljava/lang/String;ZIZ)V
    .locals 0

    .prologue
    .line 186329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186330
    iput-boolean p1, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->enabled:Z

    .line 186331
    iput-boolean p2, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->enforceExpiration:Z

    .line 186332
    iput-boolean p3, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->zeroRttEnabled:Z

    .line 186333
    iput-boolean p4, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->persistentCacheEnabled:Z

    .line 186334
    iput-object p5, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->cacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 186335
    iput-object p6, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->aeads:[Ljava/lang/String;

    .line 186336
    iput-object p7, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->hostnamePolicy:Ljava/lang/String;

    .line 186337
    iput-boolean p8, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->retryEnabled:Z

    .line 186338
    iput p9, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->tlsFallback:I

    .line 186339
    iput-boolean p10, p0, Lcom/facebook/proxygen/ZeroProtocolSettings;->enableTCPFastOpen:Z

    .line 186340
    return-void
.end method
