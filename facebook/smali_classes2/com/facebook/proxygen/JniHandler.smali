.class public Lcom/facebook/proxygen/JniHandler;
.super LX/160;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/HTTPResponseHandler;
.implements Lcom/facebook/proxygen/HTTPTransportCallback;


# instance fields
.field private mRequestHandler:LX/4iW;

.field private mResponseHandler:Lcom/facebook/proxygen/HTTPResponseHandler;

.field private mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4iW;Lcom/facebook/proxygen/HTTPResponseHandler;)V
    .locals 1

    .prologue
    .line 183953
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/proxygen/JniHandler;-><init>(LX/4iW;Lcom/facebook/proxygen/HTTPResponseHandler;Lcom/facebook/proxygen/HTTPTransportCallback;)V

    .line 183954
    return-void
.end method

.method public constructor <init>(LX/4iW;Lcom/facebook/proxygen/HTTPResponseHandler;Lcom/facebook/proxygen/HTTPTransportCallback;)V
    .locals 1
    .param p3    # Lcom/facebook/proxygen/HTTPTransportCallback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183944
    invoke-direct {p0}, LX/160;-><init>()V

    .line 183945
    invoke-static {p1}, LX/1hH;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 183946
    invoke-static {p2}, LX/1hH;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 183947
    iput-object p1, p0, Lcom/facebook/proxygen/JniHandler;->mRequestHandler:LX/4iW;

    .line 183948
    iput-object p2, p0, Lcom/facebook/proxygen/JniHandler;->mResponseHandler:Lcom/facebook/proxygen/HTTPResponseHandler;

    .line 183949
    iput-object p3, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    .line 183950
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mRequestHandler:LX/4iW;

    .line 183951
    iput-object p0, v0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 183952
    return-void
.end method

.method private native cancelNative()V
.end method

.method private native changePriorityNative(I)V
.end method

.method private native closeNative()V
.end method

.method private native sendBodyNative([BII)Z
.end method

.method private native sendEOMNative()Z
.end method

.method private native sendHeadersNative(Lorg/apache/http/client/methods/HttpUriRequest;)Z
.end method

.method private native sendRequestWithBodyAndEOMNative(Lorg/apache/http/client/methods/HttpUriRequest;[BII)Z
.end method

.method private native setEnabledCallbackFlagNative(I)V
.end method


# virtual methods
.method public bodyBytesGenerated(J)V
    .locals 1

    .prologue
    .line 183941
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183942
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0, p1, p2}, Lcom/facebook/proxygen/HTTPTransportCallback;->bodyBytesGenerated(J)V

    .line 183943
    :cond_0
    return-void
.end method

.method public bodyBytesReceived(J)V
    .locals 1

    .prologue
    .line 183938
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183939
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0, p1, p2}, Lcom/facebook/proxygen/HTTPTransportCallback;->bodyBytesReceived(J)V

    .line 183940
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 2

    .prologue
    .line 183934
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mRequestHandler:LX/4iW;

    const/4 v1, 0x0

    .line 183935
    iput-object v1, v0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 183936
    invoke-direct {p0}, Lcom/facebook/proxygen/JniHandler;->cancelNative()V

    .line 183937
    return-void
.end method

.method public changePriority(I)V
    .locals 0

    .prologue
    .line 183932
    invoke-direct {p0, p1}, Lcom/facebook/proxygen/JniHandler;->changePriorityNative(I)V

    .line 183933
    return-void
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 183955
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/proxygen/JniHandler;->closeNative()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183956
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 183957
    return-void

    .line 183958
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public firstByteFlushed()V
    .locals 1

    .prologue
    .line 183895
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183896
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0}, Lcom/facebook/proxygen/HTTPTransportCallback;->firstByteFlushed()V

    .line 183897
    :cond_0
    return-void
.end method

.method public firstHeaderByteFlushed()V
    .locals 1

    .prologue
    .line 183900
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183901
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0}, Lcom/facebook/proxygen/HTTPTransportCallback;->firstHeaderByteFlushed()V

    .line 183902
    :cond_0
    return-void
.end method

.method public getEnabledCallbackFlag()I
    .locals 1

    .prologue
    .line 183903
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183904
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0}, Lcom/facebook/proxygen/HTTPTransportCallback;->getEnabledCallbackFlag()I

    move-result v0

    .line 183905
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public headerBytesGenerated(JJ)V
    .locals 1

    .prologue
    .line 183906
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183907
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/facebook/proxygen/HTTPTransportCallback;->headerBytesGenerated(JJ)V

    .line 183908
    :cond_0
    return-void
.end method

.method public headerBytesReceived(JJ)V
    .locals 1

    .prologue
    .line 183909
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183910
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/facebook/proxygen/HTTPTransportCallback;->headerBytesReceived(JJ)V

    .line 183911
    :cond_0
    return-void
.end method

.method public lastByteAcked(J)V
    .locals 1

    .prologue
    .line 183912
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183913
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0, p1, p2}, Lcom/facebook/proxygen/HTTPTransportCallback;->lastByteAcked(J)V

    .line 183914
    :cond_0
    return-void
.end method

.method public lastByteFlushed()V
    .locals 1

    .prologue
    .line 183915
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    if-eqz v0, :cond_0

    .line 183916
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mTransportCallback:Lcom/facebook/proxygen/HTTPTransportCallback;

    invoke-interface {v0}, Lcom/facebook/proxygen/HTTPTransportCallback;->lastByteFlushed()V

    .line 183917
    :cond_0
    return-void
.end method

.method public onBody()V
    .locals 1

    .prologue
    .line 183898
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mResponseHandler:Lcom/facebook/proxygen/HTTPResponseHandler;

    invoke-interface {v0}, Lcom/facebook/proxygen/HTTPResponseHandler;->onBody()V

    .line 183899
    return-void
.end method

.method public onEOM()V
    .locals 2

    .prologue
    .line 183918
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mResponseHandler:Lcom/facebook/proxygen/HTTPResponseHandler;

    invoke-interface {v0}, Lcom/facebook/proxygen/HTTPResponseHandler;->onEOM()V

    .line 183919
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mRequestHandler:LX/4iW;

    const/4 v1, 0x0

    .line 183920
    iput-object v1, v0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 183921
    return-void
.end method

.method public onError(Lcom/facebook/proxygen/HTTPRequestError;)V
    .locals 2

    .prologue
    .line 183922
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mResponseHandler:Lcom/facebook/proxygen/HTTPResponseHandler;

    invoke-interface {v0, p1}, Lcom/facebook/proxygen/HTTPResponseHandler;->onError(Lcom/facebook/proxygen/HTTPRequestError;)V

    .line 183923
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mRequestHandler:LX/4iW;

    const/4 v1, 0x0

    .line 183924
    iput-object v1, v0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 183925
    return-void
.end method

.method public onResponse(ILjava/lang/String;[Lorg/apache/http/Header;)V
    .locals 1

    .prologue
    .line 183926
    iget-object v0, p0, Lcom/facebook/proxygen/JniHandler;->mResponseHandler:Lcom/facebook/proxygen/HTTPResponseHandler;

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/proxygen/HTTPResponseHandler;->onResponse(ILjava/lang/String;[Lorg/apache/http/Header;)V

    .line 183927
    return-void
.end method

.method public sendBody([BII)Z
    .locals 1

    .prologue
    .line 183928
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/proxygen/JniHandler;->sendBodyNative([BII)Z

    move-result v0

    return v0
.end method

.method public sendEOM()Z
    .locals 1

    .prologue
    .line 183929
    invoke-direct {p0}, Lcom/facebook/proxygen/JniHandler;->sendEOMNative()Z

    move-result v0

    return v0
.end method

.method public sendHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Z
    .locals 1

    .prologue
    .line 183930
    invoke-direct {p0, p1}, Lcom/facebook/proxygen/JniHandler;->sendHeadersNative(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    move-result v0

    return v0
.end method

.method public sendRequestWithBodyAndEom(Lorg/apache/http/client/methods/HttpUriRequest;[BII)Z
    .locals 1

    .prologue
    .line 183931
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/proxygen/JniHandler;->sendRequestWithBodyAndEOMNative(Lorg/apache/http/client/methods/HttpUriRequest;[BII)Z

    move-result v0

    return v0
.end method
