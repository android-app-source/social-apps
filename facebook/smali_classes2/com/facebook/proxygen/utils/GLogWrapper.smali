.class public Lcom/facebook/proxygen/utils/GLogWrapper;
.super LX/160;
.source ""


# direct methods
.method public constructor <init>(LX/1hF;)V
    .locals 1

    .prologue
    .line 186357
    invoke-direct {p0}, LX/160;-><init>()V

    .line 186358
    new-instance v0, Lcom/facebook/proxygen/utils/GLogHandler;

    invoke-direct {v0, p1}, Lcom/facebook/proxygen/utils/GLogHandler;-><init>(LX/1hF;)V

    invoke-direct {p0, v0}, Lcom/facebook/proxygen/utils/GLogWrapper;->init(Lcom/facebook/proxygen/utils/GLogHandler;)V

    .line 186359
    return-void
.end method

.method private native init(Lcom/facebook/proxygen/utils/GLogHandler;)V
.end method

.method private native setMinLogLevel(I)V
.end method


# virtual methods
.method public native close()V
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 186362
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/proxygen/utils/GLogWrapper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186363
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 186364
    return-void

    .line 186365
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public setMinLogLevel(LX/1hG;)V
    .locals 1

    .prologue
    .line 186360
    invoke-virtual {p1}, LX/1hG;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/proxygen/utils/GLogWrapper;->setMinLogLevel(I)V

    .line 186361
    return-void
.end method

.method public native setVLogLevel(I)V
.end method
