.class public Lcom/facebook/proxygen/utils/GLogHandler;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private handler:LX/1hF;

.field private final severities:[LX/1hG;


# direct methods
.method public constructor <init>(LX/1hF;)V
    .locals 1

    .prologue
    .line 186260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186261
    invoke-static {}, LX/1hG;->values()[LX/1hG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/proxygen/utils/GLogHandler;->severities:[LX/1hG;

    .line 186262
    iput-object p1, p0, Lcom/facebook/proxygen/utils/GLogHandler;->handler:LX/1hF;

    .line 186263
    return-void
.end method


# virtual methods
.method public log(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 186264
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/proxygen/utils/GLogHandler;->severities:[LX/1hG;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 186265
    :cond_0
    :goto_0
    return-void

    .line 186266
    :cond_1
    iget-object v0, p0, Lcom/facebook/proxygen/utils/GLogHandler;->handler:LX/1hF;

    iget-object v1, p0, Lcom/facebook/proxygen/utils/GLogHandler;->severities:[LX/1hG;

    aget-object v1, v1, p1

    invoke-interface {v0, v1, p2}, LX/1hF;->log(LX/1hG;Ljava/lang/String;)V

    goto :goto_0
.end method
