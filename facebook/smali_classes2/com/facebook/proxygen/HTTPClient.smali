.class public Lcom/facebook/proxygen/HTTPClient;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/NativeHandle;


# instance fields
.field private mActiveDomains:[Ljava/lang/String;

.field private mActiveProbeJson:Ljava/lang/String;

.field private mAdaptiveConfigInterval:I

.field private mAdaptiveConnTOParam:Lcom/facebook/proxygen/AdaptiveIntegerParameters;

.field private mAdaptivePing:Z

.field private mAdaptiveSessionTOParam:Lcom/facebook/proxygen/AdaptiveIntegerParameters;

.field private mAllowBrotli:Z

.field private mAllowGradientWeight:Z

.field private mAllowPreconnect:Z

.field private mAllowZstd:Z

.field private mAnalyticsLogger:Lcom/facebook/proxygen/AnalyticsLogger;

.field private mAsyncTCPProbeCallback:Lcom/facebook/proxygen/AsyncTCPProbeCallback;

.field private mBackupConnTimeout:I

.field private mBypassProxyDomains:Ljava/lang/String;

.field private mCAresEnabled:Z

.field private mCircularLogSinkEnabled:Z

.field private mClosed:Z

.field private mConnSampleRate:D

.field private mCrossDomainTCPConnsEnabled:Z

.field private mDNSServers:[Ljava/lang/String;

.field private mDnsCacheEnabled:Z

.field private mDnsRequestsOutstanding:I

.field private mDynamicLimitRatio:I

.field private mEnableCachingPushManager:Z

.field private mEnableTransportCallbacks:Z

.field public final mEventBase:Lcom/facebook/proxygen/EventBase;

.field private mFallbackRedirectFilter:Z

.field private mFlowControlSelectedTxnsOnly:Z

.field private mFlowControlWindow:I

.field private mGatewayPingAddress:Ljava/lang/String;

.field private mGatewayPingEnabled:Z

.field private mGatewayPingIntervalMs:I

.field private mGradientWeights:Ljava/lang/String;

.field private mHTTPSessionCacheType:Ljava/lang/String;

.field private mHappyEyeballsConnectionDelayMillis:J

.field private mHttpPushPolicy:I

.field private mIdleTimeoutForUnused:I

.field private mIdleTimeoutForUsed:I

.field private mInitialized:Z

.field private mIsHTTPSEnforced:Z

.field private mIsHappyEyeballsV4Preferred:Z

.field private mIsNetworkEventLogEnabled:Z

.field private mIsPerDomainLimitEnabled:Z

.field private mIsPerDomainLimitEnabled2G:Z

.field private mIsReplaySafeFilterEnabled:Z

.field private mIsSSLSessionCacheEnabled:Z

.field private mIsSandbox:Z

.field private mIsTLSCachedInfoEnabled:Z

.field private mIsZlibFilterEnabled:Z

.field private mLargerFlowControlWindow:Z

.field private mMaxConnectionRetryCount:I

.field private mMaxConnectionRetryCount2G:I

.field private mMaxIdleHTTPSessions:I

.field private mMaxIdleHTTPSessions2G:I

.field private mMaxIdleSPDYSessions:I

.field private mMaxIdleSPDYSessions2G:I

.field private mMaxPingRetries:I

.field private mMaxPriorityLevelForSession:I

.field private mMinDomainRefreshInterval:I

.field private mNetworkStatusMonitor:Lcom/facebook/proxygen/NetworkStatusMonitor;

.field private mNewConnTimeoutMillis:J

.field private mPersistentDNSCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field private mPersistentSSLCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field private mPersistentZstdCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field private mPingRttThreshold:I

.field private mPingTimeout:I

.field private mPreConnects:Ljava/lang/String;

.field private mPreconnectFilePath:Ljava/lang/String;

.field private mProxyAddHostHeader:Z

.field private mProxyFallbackEnabled:Z

.field private mProxyHost:Ljava/lang/String;

.field private mProxyPassword:Ljava/lang/String;

.field private mProxyPort:I

.field private mProxyUsername:Ljava/lang/String;

.field private mPushCallbacks:Lcom/facebook/proxygen/PushCallbacks;

.field private mReInitToRefreshSettings:Z

.field private mRedirectTargetWhitelist:[Ljava/lang/String;

.field private mRewriteExemptions:[Ljava/lang/String;

.field private mRewriteRules:[Lcom/facebook/proxygen/RewriteRule;

.field private mRewriteRulesHandle:J

.field private mSSLVerificationSettings:Lcom/facebook/proxygen/SSLVerificationSettings;

.field private mSecureProxyHost:Ljava/lang/String;

.field private mSecureProxyPassword:Ljava/lang/String;

.field private mSecureProxyPort:I

.field private mSecureProxyUsername:Ljava/lang/String;

.field private mSecurityAsSessionLimitHint:Z

.field private mSendPingForTcpRetransmission:Z

.field private mSessionWriteTimeoutMillis:J

.field private mSettings:J

.field private mSocketBufferExperimentEnabled:Z

.field private mSocketSendBuffer:I

.field private mStaleAnswersEnabled:Z

.field private mTLSCachedInfoSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field private mTransactionIdleTimeoutMillis:J

.field private mUpdateDNSAfterTCPReuse:Z

.field private mUseLoadBalancing:Z

.field private mUseRequestWeight:Z

.field private mUseUrlRewriteFilter:Z

.field private mUseZRRedirectFilter:Z

.field private mUserAgent:Ljava/lang/String;

.field private mUserInstalledCertificates:[[B

.field private mZeroProtocolSettings:Lcom/facebook/proxygen/ZeroProtocolSettings;


# direct methods
.method public constructor <init>(Lcom/facebook/proxygen/EventBase;)V
    .locals 7

    .prologue
    const v6, 0xd6d8

    const/4 v5, 0x6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183759
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/facebook/proxygen/HTTPClient;->mNewConnTimeoutMillis:J

    .line 183760
    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/facebook/proxygen/HTTPClient;->mSessionWriteTimeoutMillis:J

    .line 183761
    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/facebook/proxygen/HTTPClient;->mTransactionIdleTimeoutMillis:J

    .line 183762
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/proxygen/HTTPClient;->mConnSampleRate:D

    .line 183763
    const/16 v0, 0xa

    iput v0, p0, Lcom/facebook/proxygen/HTTPClient;->mAdaptiveConfigInterval:I

    .line 183764
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mUseZRRedirectFilter:Z

    .line 183765
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mLargerFlowControlWindow:Z

    .line 183766
    const v0, 0xffff

    iput v0, p0, Lcom/facebook/proxygen/HTTPClient;->mFlowControlWindow:I

    .line 183767
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mFlowControlSelectedTxnsOnly:Z

    .line 183768
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mEnableCachingPushManager:Z

    .line 183769
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mUseLoadBalancing:Z

    .line 183770
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mIsReplaySafeFilterEnabled:Z

    .line 183771
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowPreconnect:Z

    .line 183772
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mPreconnectFilePath:Ljava/lang/String;

    .line 183773
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mActiveProbeJson:Ljava/lang/String;

    .line 183774
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowBrotli:Z

    .line 183775
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowZstd:Z

    .line 183776
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingAddress:Ljava/lang/String;

    .line 183777
    iput v2, p0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingIntervalMs:I

    .line 183778
    iput v2, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxPriorityLevelForSession:I

    .line 183779
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mEventBase:Lcom/facebook/proxygen/EventBase;

    .line 183780
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mIsNetworkEventLogEnabled:Z

    .line 183781
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mIsZlibFilterEnabled:Z

    .line 183782
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mIsSSLSessionCacheEnabled:Z

    .line 183783
    const-string v0, "adv"

    iput-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mHTTPSessionCacheType:Ljava/lang/String;

    .line 183784
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mIsPerDomainLimitEnabled:Z

    .line 183785
    iput v2, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxConnectionRetryCount:I

    .line 183786
    iput v5, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleHTTPSessions:I

    .line 183787
    iput v4, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleSPDYSessions:I

    .line 183788
    iput v2, p0, Lcom/facebook/proxygen/HTTPClient;->mDynamicLimitRatio:I

    .line 183789
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mIsPerDomainLimitEnabled2G:Z

    .line 183790
    iput v2, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxConnectionRetryCount2G:I

    .line 183791
    iput v5, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleHTTPSessions2G:I

    .line 183792
    iput v4, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleSPDYSessions2G:I

    .line 183793
    iput v6, p0, Lcom/facebook/proxygen/HTTPClient;->mIdleTimeoutForUsed:I

    .line 183794
    iput v6, p0, Lcom/facebook/proxygen/HTTPClient;->mIdleTimeoutForUnused:I

    .line 183795
    iput v2, p0, Lcom/facebook/proxygen/HTTPClient;->mBackupConnTimeout:I

    .line 183796
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mIsHTTPSEnforced:Z

    .line 183797
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mIsSandbox:Z

    .line 183798
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mIsHappyEyeballsV4Preferred:Z

    .line 183799
    const-wide/16 v0, 0x32

    iput-wide v0, p0, Lcom/facebook/proxygen/HTTPClient;->mHappyEyeballsConnectionDelayMillis:J

    .line 183800
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mDnsCacheEnabled:Z

    .line 183801
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mCAresEnabled:Z

    .line 183802
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mStaleAnswersEnabled:Z

    .line 183803
    iput v3, p0, Lcom/facebook/proxygen/HTTPClient;->mDnsRequestsOutstanding:I

    .line 183804
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mCircularLogSinkEnabled:Z

    .line 183805
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mSendPingForTcpRetransmission:Z

    .line 183806
    iput v2, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxPingRetries:I

    .line 183807
    const/16 v0, 0x64

    iput v0, p0, Lcom/facebook/proxygen/HTTPClient;->mPingTimeout:I

    .line 183808
    const/16 v0, 0x96

    iput v0, p0, Lcom/facebook/proxygen/HTTPClient;->mPingRttThreshold:I

    .line 183809
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mAdaptivePing:Z

    .line 183810
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/proxygen/HTTPClient;->mRewriteRulesHandle:J

    .line 183811
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "^https?://([a-z0-9\\.-]+)*facebook\\.com"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mRedirectTargetWhitelist:[Ljava/lang/String;

    .line 183812
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mSocketBufferExperimentEnabled:Z

    .line 183813
    const v0, 0x450f00

    iput v0, p0, Lcom/facebook/proxygen/HTTPClient;->mSocketSendBuffer:I

    .line 183814
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mCrossDomainTCPConnsEnabled:Z

    .line 183815
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mUpdateDNSAfterTCPReuse:Z

    .line 183816
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mEnableTransportCallbacks:Z

    .line 183817
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mIsTLSCachedInfoEnabled:Z

    .line 183818
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mUseUrlRewriteFilter:Z

    .line 183819
    iput-boolean v3, p0, Lcom/facebook/proxygen/HTTPClient;->mFallbackRedirectFilter:Z

    .line 183820
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowBrotli:Z

    .line 183821
    sget v0, LX/1hT;->FACEBOOK:I

    iput v0, p0, Lcom/facebook/proxygen/HTTPClient;->mHttpPushPolicy:I

    .line 183822
    iput-boolean v2, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowZstd:Z

    .line 183823
    return-void
.end method

.method private native close(Lcom/facebook/proxygen/EventBase;)V
.end method

.method private native connect(Lcom/facebook/proxygen/EventBase;[Ljava/lang/String;)V
.end method

.method private native init(Lcom/facebook/proxygen/EventBase;ZZZZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;IZIIIIZIIIIIIZZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/proxygen/PersistentSSLCacheSettings;Lcom/facebook/proxygen/SSLVerificationSettings;Lcom/facebook/proxygen/ZeroProtocolSettings;[[BZJJJJ[Ljava/lang/String;Lcom/facebook/proxygen/PersistentSSLCacheSettings;ZZZILcom/facebook/proxygen/NetworkStatusMonitor;ZIIIZ[Lcom/facebook/proxygen/RewriteRule;[Ljava/lang/String;[Ljava/lang/String;Lcom/facebook/proxygen/AnalyticsLogger;DZIZLjava/lang/String;ILjava/lang/String;ZILcom/facebook/proxygen/AdaptiveIntegerParameters;ZZZIZIZZLjava/lang/String;ZILcom/facebook/proxygen/PushCallbacks;ZLcom/facebook/proxygen/AdaptiveIntegerParameters;ZZLcom/facebook/proxygen/PersistentSSLCacheSettings;ZZLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/proxygen/AsyncTCPProbeCallback;ZZLcom/facebook/proxygen/PersistentSSLCacheSettings;)V
.end method

.method private stringEquals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183755
    if-eqz p1, :cond_0

    .line 183756
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 183757
    :goto_0
    return v0

    :cond_0
    if-nez p2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native updateRewriteRules([Lcom/facebook/proxygen/RewriteRule;)V
.end method


# virtual methods
.method public declared-synchronized callNativeInit(ZZ)V
    .locals 106

    .prologue
    .line 183750
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/proxygen/HTTPClient;->mEventBase:Lcom/facebook/proxygen/EventBase;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/facebook/proxygen/HTTPClient;->mIsNetworkEventLogEnabled:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/facebook/proxygen/HTTPClient;->mIsZlibFilterEnabled:Z

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/facebook/proxygen/HTTPClient;->mIsSSLSessionCacheEnabled:Z

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/proxygen/HTTPClient;->mHTTPSessionCacheType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/proxygen/HTTPClient;->mPreConnects:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/proxygen/HTTPClient;->mActiveDomains:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/proxygen/HTTPClient;->mMinDomainRefreshInterval:I

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/facebook/proxygen/HTTPClient;->mIsPerDomainLimitEnabled:Z

    move-object/from16 v0, p0

    iget v15, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxConnectionRetryCount:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleHTTPSessions:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleSPDYSessions:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mDynamicLimitRatio:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIsPerDomainLimitEnabled2G:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxConnectionRetryCount2G:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleHTTPSessions2G:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleSPDYSessions2G:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIdleTimeoutForUsed:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIdleTimeoutForUnused:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mBackupConnTimeout:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIsHTTPSEnforced:Z

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIsSandbox:Z

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mProxyHost:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mProxyPort:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mProxyUsername:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mProxyPassword:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyHost:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyPort:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyUsername:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyPassword:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mBypassProxyDomains:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mProxyFallbackEnabled:Z

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mProxyAddHostHeader:Z

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mPersistentSSLCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSSLVerificationSettings:Lcom/facebook/proxygen/SSLVerificationSettings;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mZeroProtocolSettings:Lcom/facebook/proxygen/ZeroProtocolSettings;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mUserInstalledCertificates:[[B

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIsHappyEyeballsV4Preferred:Z

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/proxygen/HTTPClient;->mHappyEyeballsConnectionDelayMillis:J

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/proxygen/HTTPClient;->mNewConnTimeoutMillis:J

    move-wide/from16 v46, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSessionWriteTimeoutMillis:J

    move-wide/from16 v48, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/proxygen/HTTPClient;->mTransactionIdleTimeoutMillis:J

    move-wide/from16 v50, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mDNSServers:[Ljava/lang/String;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mPersistentDNSCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mDnsCacheEnabled:Z

    move/from16 v54, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mCAresEnabled:Z

    move/from16 v55, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mStaleAnswersEnabled:Z

    move/from16 v56, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mDnsRequestsOutstanding:I

    move/from16 v57, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mNetworkStatusMonitor:Lcom/facebook/proxygen/NetworkStatusMonitor;

    move-object/from16 v58, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSendPingForTcpRetransmission:Z

    move/from16 v59, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxPingRetries:I

    move/from16 v60, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mPingTimeout:I

    move/from16 v61, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mPingRttThreshold:I

    move/from16 v62, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAdaptivePing:Z

    move/from16 v63, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mRewriteRules:[Lcom/facebook/proxygen/RewriteRule;

    move-object/from16 v64, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mRewriteExemptions:[Ljava/lang/String;

    move-object/from16 v65, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mRedirectTargetWhitelist:[Ljava/lang/String;

    move-object/from16 v66, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAnalyticsLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    move-object/from16 v67, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/proxygen/HTTPClient;->mConnSampleRate:D

    move-wide/from16 v68, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSocketBufferExperimentEnabled:Z

    move/from16 v70, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSocketSendBuffer:I

    move/from16 v71, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingEnabled:Z

    move/from16 v72, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingAddress:Ljava/lang/String;

    move-object/from16 v73, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingIntervalMs:I

    move/from16 v74, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mUserAgent:Ljava/lang/String;

    move-object/from16 v75, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mCrossDomainTCPConnsEnabled:Z

    move/from16 v76, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAdaptiveConfigInterval:I

    move/from16 v77, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAdaptiveConnTOParam:Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    move-object/from16 v78, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mUseZRRedirectFilter:Z

    move/from16 v79, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mUpdateDNSAfterTCPReuse:Z

    move/from16 v80, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mLargerFlowControlWindow:Z

    move/from16 v81, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mFlowControlWindow:I

    move/from16 v82, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mFlowControlSelectedTxnsOnly:Z

    move/from16 v83, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mMaxPriorityLevelForSession:I

    move/from16 v84, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mUseRequestWeight:Z

    move/from16 v85, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAllowGradientWeight:Z

    move/from16 v86, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mGradientWeights:Ljava/lang/String;

    move-object/from16 v87, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mEnableCachingPushManager:Z

    move/from16 v88, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/proxygen/HTTPClient;->mHttpPushPolicy:I

    move/from16 v89, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mPushCallbacks:Lcom/facebook/proxygen/PushCallbacks;

    move-object/from16 v90, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mUseLoadBalancing:Z

    move/from16 v91, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAdaptiveSessionTOParam:Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    move-object/from16 v92, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIsReplaySafeFilterEnabled:Z

    move/from16 v93, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mSecurityAsSessionLimitHint:Z

    move/from16 v94, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mTLSCachedInfoSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-object/from16 v95, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mIsTLSCachedInfoEnabled:Z

    move/from16 v96, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAllowPreconnect:Z

    move/from16 v97, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mPreconnectFilePath:Ljava/lang/String;

    move-object/from16 v98, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mUseUrlRewriteFilter:Z

    move/from16 v99, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mFallbackRedirectFilter:Z

    move/from16 v100, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mActiveProbeJson:Ljava/lang/String;

    move-object/from16 v101, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAsyncTCPProbeCallback:Lcom/facebook/proxygen/AsyncTCPProbeCallback;

    move-object/from16 v102, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAllowBrotli:Z

    move/from16 v103, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/HTTPClient;->mAllowZstd:Z

    move/from16 v104, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/HTTPClient;->mPersistentZstdCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-object/from16 v105, v0

    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v3 .. v105}, Lcom/facebook/proxygen/HTTPClient;->init(Lcom/facebook/proxygen/EventBase;ZZZZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;IZIIIIZIIIIIIZZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/proxygen/PersistentSSLCacheSettings;Lcom/facebook/proxygen/SSLVerificationSettings;Lcom/facebook/proxygen/ZeroProtocolSettings;[[BZJJJJ[Ljava/lang/String;Lcom/facebook/proxygen/PersistentSSLCacheSettings;ZZZILcom/facebook/proxygen/NetworkStatusMonitor;ZIIIZ[Lcom/facebook/proxygen/RewriteRule;[Ljava/lang/String;[Ljava/lang/String;Lcom/facebook/proxygen/AnalyticsLogger;DZIZLjava/lang/String;ILjava/lang/String;ZILcom/facebook/proxygen/AdaptiveIntegerParameters;ZZZIZIZZLjava/lang/String;ZILcom/facebook/proxygen/PushCallbacks;ZLcom/facebook/proxygen/AdaptiveIntegerParameters;ZZLcom/facebook/proxygen/PersistentSSLCacheSettings;ZZLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/proxygen/AsyncTCPProbeCallback;ZZLcom/facebook/proxygen/PersistentSSLCacheSettings;)V

    .line 183751
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/proxygen/HTTPClient;->mInitialized:Z

    .line 183752
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183753
    monitor-exit p0

    return-void

    .line 183754
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public close()V
    .locals 1

    .prologue
    .line 183746
    iget-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mInitialized:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mClosed:Z

    if-nez v0, :cond_0

    .line 183747
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mEventBase:Lcom/facebook/proxygen/EventBase;

    invoke-direct {p0, v0}, Lcom/facebook/proxygen/HTTPClient;->close(Lcom/facebook/proxygen/EventBase;)V

    .line 183748
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mClosed:Z

    .line 183749
    :cond_0
    return-void
.end method

.method public connect([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 183743
    iget-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mInitialized:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mClosed:Z

    if-nez v0, :cond_0

    .line 183744
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mEventBase:Lcom/facebook/proxygen/EventBase;

    invoke-direct {p0, v0, p1}, Lcom/facebook/proxygen/HTTPClient;->connect(Lcom/facebook/proxygen/EventBase;[Ljava/lang/String;)V

    .line 183745
    :cond_0
    return-void
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 183739
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/proxygen/HTTPClient;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183740
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 183741
    return-void

    .line 183742
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getEventBase()Lcom/facebook/proxygen/EventBase;
    .locals 1

    .prologue
    .line 183738
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mEventBase:Lcom/facebook/proxygen/EventBase;

    return-object v0
.end method

.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 183703
    iget-wide v0, p0, Lcom/facebook/proxygen/HTTPClient;->mSettings:J

    return-wide v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 183734
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/proxygen/HTTPClient;->callNativeInit(ZZ)V

    .line 183735
    return-void
.end method

.method public make(Lcom/facebook/proxygen/JniHandler;Lcom/facebook/proxygen/NativeReadBuffer;Lcom/facebook/proxygen/TraceEventContext;)V
    .locals 8

    .prologue
    .line 183729
    const/4 v7, 0x0

    .line 183730
    iget-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mEnableTransportCallbacks:Z

    if-eqz v0, :cond_0

    .line 183731
    invoke-virtual {p1}, Lcom/facebook/proxygen/JniHandler;->getEnabledCallbackFlag()I

    move-result v7

    .line 183732
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/proxygen/TraceEventContext;->needPublishEvent()Z

    move-result v3

    iget-boolean v5, p0, Lcom/facebook/proxygen/HTTPClient;->mCircularLogSinkEnabled:Z

    iget-object v6, p0, Lcom/facebook/proxygen/HTTPClient;->mNetworkStatusMonitor:Lcom/facebook/proxygen/NetworkStatusMonitor;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/proxygen/HTTPClient;->make(Lcom/facebook/proxygen/JniHandler;Lcom/facebook/proxygen/NativeReadBuffer;ZLcom/facebook/proxygen/TraceEventContext;ZLcom/facebook/proxygen/NetworkStatusMonitor;I)V

    .line 183733
    return-void
.end method

.method public synchronized native declared-synchronized make(Lcom/facebook/proxygen/JniHandler;Lcom/facebook/proxygen/NativeReadBuffer;ZLcom/facebook/proxygen/TraceEventContext;ZLcom/facebook/proxygen/NetworkStatusMonitor;I)V
.end method

.method public nonBlockingInit()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 183727
    invoke-virtual {p0, v0, v0}, Lcom/facebook/proxygen/HTTPClient;->callNativeInit(ZZ)V

    .line 183728
    return-void
.end method

.method public reInitializeIfNeeded()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 183720
    iget-boolean v1, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/proxygen/HTTPClient;->mInitialized:Z

    if-eqz v1, :cond_0

    .line 183721
    invoke-virtual {p0}, Lcom/facebook/proxygen/HTTPClient;->reinit()V

    .line 183722
    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mClosed:Z

    .line 183723
    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    .line 183724
    const/4 v0, 0x1

    .line 183725
    :goto_0
    return v0

    .line 183726
    :cond_0
    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    goto :goto_0
.end method

.method public reinit()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 183718
    invoke-virtual {p0, v0, v0}, Lcom/facebook/proxygen/HTTPClient;->callNativeInit(ZZ)V

    .line 183719
    return-void
.end method

.method public setActiveDomains([Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183716
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mActiveDomains:[Ljava/lang/String;

    .line 183717
    return-object p0
.end method

.method public setActiveProbeJson(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183714
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mActiveProbeJson:Ljava/lang/String;

    .line 183715
    return-object p0
.end method

.method public setAdaptiveConfigInterval(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183712
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAdaptiveConfigInterval:I

    .line 183713
    return-object p0
.end method

.method public setAdaptiveConnTOParam(Lcom/facebook/proxygen/AdaptiveIntegerParameters;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183710
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAdaptiveConnTOParam:Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    .line 183711
    return-object p0
.end method

.method public setAdaptivePing(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183708
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAdaptivePing:Z

    .line 183709
    return-object p0
.end method

.method public setAdaptiveSessionTOParam(Lcom/facebook/proxygen/AdaptiveIntegerParameters;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183706
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAdaptiveSessionTOParam:Lcom/facebook/proxygen/AdaptiveIntegerParameters;

    .line 183707
    return-object p0
.end method

.method public setAllowBrotli(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183736
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowBrotli:Z

    .line 183737
    return-object p0
.end method

.method public setAllowGradientWeight(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183704
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowGradientWeight:Z

    .line 183705
    return-object p0
.end method

.method public setAllowPreconnect(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183854
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowPreconnect:Z

    .line 183855
    return-object p0
.end method

.method public setAllowZstd(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183883
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAllowZstd:Z

    .line 183884
    return-object p0
.end method

.method public setAnalyticsLogger(Lcom/facebook/proxygen/AnalyticsLogger;D)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183880
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAnalyticsLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    .line 183881
    iput-wide p2, p0, Lcom/facebook/proxygen/HTTPClient;->mConnSampleRate:D

    .line 183882
    return-object p0
.end method

.method public setAsyncTCPProbeCallback(Lcom/facebook/proxygen/AsyncTCPProbeCallback;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183878
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mAsyncTCPProbeCallback:Lcom/facebook/proxygen/AsyncTCPProbeCallback;

    .line 183879
    return-object p0
.end method

.method public setBackupConnTimeout(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183876
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mBackupConnTimeout:I

    .line 183877
    return-object p0
.end method

.method public setBypassProxyDomains(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183872
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mBypassProxyDomains:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/facebook/proxygen/HTTPClient;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183873
    :goto_0
    return-object p0

    .line 183874
    :cond_0
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mBypassProxyDomains:Ljava/lang/String;

    .line 183875
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    goto :goto_0
.end method

.method public setCAresEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183870
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mCAresEnabled:Z

    .line 183871
    return-object p0
.end method

.method public setCircularLogSinkEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183868
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mCircularLogSinkEnabled:Z

    .line 183869
    return-object p0
.end method

.method public setCrossDomainTCPConnsEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183866
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mCrossDomainTCPConnsEnabled:Z

    .line 183867
    return-object p0
.end method

.method public setDNSCacheEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183864
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mDnsCacheEnabled:Z

    .line 183865
    return-object p0
.end method

.method public setDNSServers([Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 1

    .prologue
    .line 183860
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mDNSServers:[Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183861
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mDNSServers:[Ljava/lang/String;

    .line 183862
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    .line 183863
    :cond_0
    return-object p0
.end method

.method public setDnsRequestsOutstanding(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183858
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mDnsRequestsOutstanding:I

    .line 183859
    return-object p0
.end method

.method public setDynamicLimitRatio(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183856
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mDynamicLimitRatio:I

    .line 183857
    return-object p0
.end method

.method public setEnableCachingPushManager(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183826
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mEnableCachingPushManager:Z

    .line 183827
    return-object p0
.end method

.method public setFallbackRedirectFilter(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183852
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mFallbackRedirectFilter:Z

    .line 183853
    return-object p0
.end method

.method public setFlowControl(ZIZ)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183848
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mLargerFlowControlWindow:Z

    .line 183849
    iput p2, p0, Lcom/facebook/proxygen/HTTPClient;->mFlowControlWindow:I

    .line 183850
    iput-boolean p3, p0, Lcom/facebook/proxygen/HTTPClient;->mFlowControlSelectedTxnsOnly:Z

    .line 183851
    return-object p0
.end method

.method public setGatewayPingAddress(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183846
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingAddress:Ljava/lang/String;

    .line 183847
    return-object p0
.end method

.method public setGatewayPingEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183844
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingEnabled:Z

    .line 183845
    return-object p0
.end method

.method public setGatewayPingIntervalMs(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183842
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mGatewayPingIntervalMs:I

    .line 183843
    return-object p0
.end method

.method public setGradientWeights(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183840
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mGradientWeights:Ljava/lang/String;

    .line 183841
    return-object p0
.end method

.method public setHTTPSEnforced(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183838
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsHTTPSEnforced:Z

    .line 183839
    return-object p0
.end method

.method public setHTTPSessionCacheType(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183836
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mHTTPSessionCacheType:Ljava/lang/String;

    .line 183837
    return-object p0
.end method

.method public setHappyEyeballsConnectionDelayMillis(J)Lcom/facebook/proxygen/HTTPClient;
    .locals 1

    .prologue
    .line 183834
    iput-wide p1, p0, Lcom/facebook/proxygen/HTTPClient;->mHappyEyeballsConnectionDelayMillis:J

    .line 183835
    return-object p0
.end method

.method public setHappyEyeballsV4Preferred(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183832
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsHappyEyeballsV4Preferred:Z

    .line 183833
    return-object p0
.end method

.method public setHttpPushPolicy(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183830
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mHttpPushPolicy:I

    .line 183831
    return-object p0
.end method

.method public setIdleTimeoutForUnused(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183828
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIdleTimeoutForUnused:I

    .line 183829
    return-object p0
.end method

.method public setIdleTimeoutForUsed(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183824
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIdleTimeoutForUsed:I

    .line 183825
    return-object p0
.end method

.method public setIsSandbox(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 1

    .prologue
    .line 183639
    iget-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mIsSandbox:Z

    if-eq v0, p1, :cond_0

    .line 183640
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    .line 183641
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsSandbox:Z

    .line 183642
    :cond_0
    return-object p0
.end method

.method public setLoadBalancing(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183637
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mUseLoadBalancing:Z

    .line 183638
    return-object p0
.end method

.method public setMaxConnectionRetryCount(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183635
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxConnectionRetryCount:I

    .line 183636
    return-object p0
.end method

.method public setMaxConnectionRetryCount2G(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183633
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxConnectionRetryCount2G:I

    .line 183634
    return-object p0
.end method

.method public setMaxIdleHTTPSessions(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183631
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleHTTPSessions:I

    .line 183632
    return-object p0
.end method

.method public setMaxIdleHTTPSessions2G(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183629
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleHTTPSessions2G:I

    .line 183630
    return-object p0
.end method

.method public setMaxIdleSPDYSessions(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183627
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleSPDYSessions:I

    .line 183628
    return-object p0
.end method

.method public setMaxIdleSPDYSessions2G(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183625
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxIdleSPDYSessions2G:I

    .line 183626
    return-object p0
.end method

.method public setMaxPingRetries(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183623
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxPingRetries:I

    .line 183624
    return-object p0
.end method

.method public setMaxPriorityLevelForSession(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183621
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMaxPriorityLevelForSession:I

    .line 183622
    return-object p0
.end method

.method public setMinDomainRefereshInterval(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183619
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mMinDomainRefreshInterval:I

    .line 183620
    return-object p0
.end method

.method public setNativeHandle(J)V
    .locals 1

    .prologue
    .line 183617
    iput-wide p1, p0, Lcom/facebook/proxygen/HTTPClient;->mSettings:J

    .line 183618
    return-void
.end method

.method public setNetworkEventLogging(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183615
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsNetworkEventLogEnabled:Z

    .line 183616
    return-object p0
.end method

.method public setNetworkStatusMonitor(Lcom/facebook/proxygen/NetworkStatusMonitor;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183613
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mNetworkStatusMonitor:Lcom/facebook/proxygen/NetworkStatusMonitor;

    .line 183614
    return-object p0
.end method

.method public setNewConnectionTimeoutMillis(J)Lcom/facebook/proxygen/HTTPClient;
    .locals 1

    .prologue
    .line 183584
    iput-wide p1, p0, Lcom/facebook/proxygen/HTTPClient;->mNewConnTimeoutMillis:J

    .line 183585
    return-object p0
.end method

.method public setPerDomainLimitEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183586
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsPerDomainLimitEnabled:Z

    .line 183587
    return-object p0
.end method

.method public setPerDomainLimitEnabled2G(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183588
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsPerDomainLimitEnabled2G:Z

    .line 183589
    return-object p0
.end method

.method public setPersistentDNSCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183590
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPersistentDNSCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 183591
    return-object p0
.end method

.method public setPersistentSSLCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183592
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPersistentSSLCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 183593
    return-object p0
.end method

.method public setPersistentZstdCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183594
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPersistentZstdCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 183595
    return-object p0
.end method

.method public setPingRttThreshold(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183596
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPingRttThreshold:I

    .line 183597
    return-object p0
.end method

.method public setPingTimeout(I)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183598
    iput p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPingTimeout:I

    .line 183599
    return-object p0
.end method

.method public setPreConnects(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183600
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPreConnects:Ljava/lang/String;

    .line 183601
    return-object p0
.end method

.method public setPreconnectFilePath(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183602
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPreconnectFilePath:Ljava/lang/String;

    .line 183603
    return-object p0
.end method

.method public setProxy(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183604
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyHost:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/facebook/proxygen/HTTPClient;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyPort:I

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyUsername:Ljava/lang/String;

    invoke-direct {p0, p3, v0}, Lcom/facebook/proxygen/HTTPClient;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyPassword:Ljava/lang/String;

    invoke-direct {p0, p4, v0}, Lcom/facebook/proxygen/HTTPClient;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183605
    :goto_0
    return-object p0

    .line 183606
    :cond_0
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyHost:Ljava/lang/String;

    .line 183607
    iput p2, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyPort:I

    .line 183608
    iput-object p3, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyUsername:Ljava/lang/String;

    .line 183609
    iput-object p4, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyPassword:Ljava/lang/String;

    .line 183610
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    goto :goto_0
.end method

.method public setProxyAddHostHeader(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183611
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyAddHostHeader:Z

    .line 183612
    return-object p0
.end method

.method public setProxyFallbackEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183671
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mProxyFallbackEnabled:Z

    .line 183672
    return-object p0
.end method

.method public setPushCallbacks(Lcom/facebook/proxygen/PushCallbacks;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183701
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mPushCallbacks:Lcom/facebook/proxygen/PushCallbacks;

    .line 183702
    return-object p0
.end method

.method public setRedirectTargetWhitelist([Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183699
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mRedirectTargetWhitelist:[Ljava/lang/String;

    .line 183700
    return-object p0
.end method

.method public setReplaySafeFilter(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183697
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsReplaySafeFilterEnabled:Z

    .line 183698
    return-object p0
.end method

.method public setRewriteExemptions([Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183695
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mRewriteExemptions:[Ljava/lang/String;

    .line 183696
    return-object p0
.end method

.method public setRewriteRules([Lcom/facebook/proxygen/RewriteRule;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183693
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mRewriteRules:[Lcom/facebook/proxygen/RewriteRule;

    .line 183694
    return-object p0
.end method

.method public setSSLSessionCache(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183645
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsSSLSessionCacheEnabled:Z

    .line 183646
    return-object p0
.end method

.method public setSSLVerificationSettings(Lcom/facebook/proxygen/SSLVerificationSettings;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183691
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mSSLVerificationSettings:Lcom/facebook/proxygen/SSLVerificationSettings;

    .line 183692
    return-object p0
.end method

.method public setSecureProxy(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183684
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyHost:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/facebook/proxygen/HTTPClient;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyPort:I

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyUsername:Ljava/lang/String;

    invoke-direct {p0, p3, v0}, Lcom/facebook/proxygen/HTTPClient;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyPassword:Ljava/lang/String;

    invoke-direct {p0, p4, v0}, Lcom/facebook/proxygen/HTTPClient;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183685
    :goto_0
    return-object p0

    .line 183686
    :cond_0
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyHost:Ljava/lang/String;

    .line 183687
    iput p2, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyPort:I

    .line 183688
    iput-object p3, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyUsername:Ljava/lang/String;

    .line 183689
    iput-object p4, p0, Lcom/facebook/proxygen/HTTPClient;->mSecureProxyPassword:Ljava/lang/String;

    .line 183690
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/proxygen/HTTPClient;->mReInitToRefreshSettings:Z

    goto :goto_0
.end method

.method public setSecurityAsSessionLimitHint(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183682
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mSecurityAsSessionLimitHint:Z

    .line 183683
    return-object p0
.end method

.method public setSendPingForTcpRetransmission(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183680
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mSendPingForTcpRetransmission:Z

    .line 183681
    return-object p0
.end method

.method public setSessionWriteTimeoutMillis(J)Lcom/facebook/proxygen/HTTPClient;
    .locals 1

    .prologue
    .line 183678
    iput-wide p1, p0, Lcom/facebook/proxygen/HTTPClient;->mSessionWriteTimeoutMillis:J

    .line 183679
    return-object p0
.end method

.method public setSocketBufferExperimentEnabled(ZI)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183675
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mSocketBufferExperimentEnabled:Z

    .line 183676
    iput p2, p0, Lcom/facebook/proxygen/HTTPClient;->mSocketSendBuffer:I

    .line 183677
    return-object p0
.end method

.method public setStaleAnswersEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183673
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mStaleAnswersEnabled:Z

    .line 183674
    return-object p0
.end method

.method public setTLSCachedInfoEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183582
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsTLSCachedInfoEnabled:Z

    .line 183583
    return-object p0
.end method

.method public setTLSCachedInfoSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183643
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mTLSCachedInfoSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 183644
    return-object p0
.end method

.method public setTransactionIdleTimeoutMillis(J)Lcom/facebook/proxygen/HTTPClient;
    .locals 1

    .prologue
    .line 183669
    iput-wide p1, p0, Lcom/facebook/proxygen/HTTPClient;->mTransactionIdleTimeoutMillis:J

    .line 183670
    return-object p0
.end method

.method public setTransportCallbackEnabled(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183667
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mEnableTransportCallbacks:Z

    .line 183668
    return-object p0
.end method

.method public setUpdateDNSAfterTCPReuse(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183665
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mUpdateDNSAfterTCPReuse:Z

    .line 183666
    return-object p0
.end method

.method public setUseRequestWeight(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183663
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mUseRequestWeight:Z

    .line 183664
    return-object p0
.end method

.method public setUseUrlRewriteFilter(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183661
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mUseUrlRewriteFilter:Z

    .line 183662
    return-object p0
.end method

.method public setUseZRRedirectFilter(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183659
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mUseZRRedirectFilter:Z

    .line 183660
    return-object p0
.end method

.method public setUserAgent(Ljava/lang/String;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183657
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mUserAgent:Ljava/lang/String;

    .line 183658
    return-object p0
.end method

.method public setUserInstalledCertificates([[B)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183655
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mUserInstalledCertificates:[[B

    .line 183656
    return-object p0
.end method

.method public setZeroProtocolSettings(Lcom/facebook/proxygen/ZeroProtocolSettings;)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183653
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mZeroProtocolSettings:Lcom/facebook/proxygen/ZeroProtocolSettings;

    .line 183654
    return-object p0
.end method

.method public setZlibFilter(Z)Lcom/facebook/proxygen/HTTPClient;
    .locals 0

    .prologue
    .line 183651
    iput-boolean p1, p0, Lcom/facebook/proxygen/HTTPClient;->mIsZlibFilterEnabled:Z

    .line 183652
    return-object p0
.end method

.method public updateUrlRewriteRules([Lcom/facebook/proxygen/RewriteRule;)V
    .locals 1

    .prologue
    .line 183647
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPClient;->mRewriteRules:[Lcom/facebook/proxygen/RewriteRule;

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183648
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPClient;->mRewriteRules:[Lcom/facebook/proxygen/RewriteRule;

    .line 183649
    invoke-direct {p0, p1}, Lcom/facebook/proxygen/HTTPClient;->updateRewriteRules([Lcom/facebook/proxygen/RewriteRule;)V

    .line 183650
    :cond_0
    return-void
.end method
