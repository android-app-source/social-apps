.class public Lcom/facebook/proxygen/AsyncTCPProbeResult;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final mHostAndPort:Ljava/lang/String;

.field public final mRTTInMs:I

.field public final mRTTStdDevInMs:I

.field public final mRegion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 183889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183890
    iput-object p1, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mHostAndPort:Ljava/lang/String;

    .line 183891
    iput-object p2, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRegion:Ljava/lang/String;

    .line 183892
    iput p3, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRTTInMs:I

    .line 183893
    iput p4, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRTTStdDevInMs:I

    .line 183894
    return-void
.end method


# virtual methods
.method public getHostAndPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183885
    iget-object v0, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mHostAndPort:Ljava/lang/String;

    return-object v0
.end method

.method public getRTT()I
    .locals 1

    .prologue
    .line 183886
    iget v0, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRTTInMs:I

    return v0
.end method

.method public getRTTStdDev()I
    .locals 1

    .prologue
    .line 183887
    iget v0, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRTTStdDevInMs:I

    return v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183888
    iget-object v0, p0, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRegion:Ljava/lang/String;

    return-object v0
.end method
