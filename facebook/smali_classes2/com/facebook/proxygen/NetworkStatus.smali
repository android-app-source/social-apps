.class public Lcom/facebook/proxygen/NetworkStatus;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mActiveReadSeconds:J

.field private final mActiveWriteSeconds:J

.field private final mClosedConn:J

.field private final mDequeuedReq:J

.field private final mDownloadBwQuality:Ljava/lang/String;

.field private final mEgressAvg:J

.field private final mEgressMax:J

.field private final mEgressWindowSize:J

.field private final mEnqueuedReq:J

.field private final mFinishedReq:J

.field private final mFlowControlHit:J

.field private final mInQueueReq:J

.field private final mInflightConn:J

.field private final mInflightReq:J

.field private final mIngressAvg:J

.field private final mIngressMax:J

.field private final mIngressWindowSize:J

.field private final mLatencyQuality:Ljava/lang/String;

.field private final mMayHaveIdledMS:J

.field private final mMayHaveNetwork:Z

.field private final mOpenedConn:J

.field private final mPriReqInflight:[J

.field private final mReadCount:J

.field private mReqBwEgressAvg:J

.field private mReqBwEgressMax:J

.field private mReqBwEgressSize:J

.field private mReqBwEgressStdDev:D

.field private mReqBwIngressAvg:J

.field private mReqBwIngressMax:J

.field private mReqBwIngressSize:J

.field private mReqBwIngressStdDev:D

.field private final mRttAvg:J

.field private final mRttMax:J

.field private final mRttStdDev:D

.field private final mUploadBwQuality:Ljava/lang/String;

.field private final mWriteCount:J


# direct methods
.method public constructor <init>(JJJJJJJJJJJDJJJDJJDJJJJJJJJJ[JJJZJIII)V
    .locals 3

    .prologue
    .line 184080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184081
    iput-wide p1, p0, Lcom/facebook/proxygen/NetworkStatus;->mIngressWindowSize:J

    .line 184082
    iput-wide p3, p0, Lcom/facebook/proxygen/NetworkStatus;->mIngressAvg:J

    .line 184083
    iput-wide p5, p0, Lcom/facebook/proxygen/NetworkStatus;->mIngressMax:J

    .line 184084
    iput-wide p7, p0, Lcom/facebook/proxygen/NetworkStatus;->mEgressWindowSize:J

    .line 184085
    iput-wide p9, p0, Lcom/facebook/proxygen/NetworkStatus;->mEgressAvg:J

    .line 184086
    iput-wide p11, p0, Lcom/facebook/proxygen/NetworkStatus;->mEgressMax:J

    .line 184087
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReadCount:J

    .line 184088
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mWriteCount:J

    .line 184089
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressSize:J

    .line 184090
    move-wide/from16 v0, p19

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressAvg:J

    .line 184091
    move-wide/from16 v0, p21

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressMax:J

    .line 184092
    move-wide/from16 v0, p23

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressStdDev:D

    .line 184093
    move-wide/from16 v0, p25

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressSize:J

    .line 184094
    move-wide/from16 v0, p27

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressAvg:J

    .line 184095
    move-wide/from16 v0, p29

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressMax:J

    .line 184096
    move-wide/from16 v0, p31

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressStdDev:D

    .line 184097
    move-wide/from16 v0, p33

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mRttAvg:J

    .line 184098
    move-wide/from16 v0, p35

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mRttMax:J

    .line 184099
    move-wide/from16 v0, p37

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mRttStdDev:D

    .line 184100
    move-wide/from16 v0, p39

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mOpenedConn:J

    .line 184101
    move-wide/from16 v0, p41

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mClosedConn:J

    .line 184102
    move-wide/from16 v0, p43

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mInflightConn:J

    .line 184103
    move-wide/from16 v0, p45

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mFlowControlHit:J

    .line 184104
    move-wide/from16 v0, p47

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mEnqueuedReq:J

    .line 184105
    move-wide/from16 v0, p49

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mDequeuedReq:J

    .line 184106
    move-wide/from16 v0, p51

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mFinishedReq:J

    .line 184107
    move-wide/from16 v0, p53

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mInflightReq:J

    .line 184108
    move-wide/from16 v0, p55

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mInQueueReq:J

    .line 184109
    move-object/from16 v0, p57

    iput-object v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mPriReqInflight:[J

    .line 184110
    move-wide/from16 v0, p58

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mActiveReadSeconds:J

    .line 184111
    move-wide/from16 v0, p60

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mActiveWriteSeconds:J

    .line 184112
    move/from16 v0, p62

    iput-boolean v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mMayHaveNetwork:Z

    .line 184113
    move-wide/from16 v0, p63

    iput-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mMayHaveIdledMS:J

    .line 184114
    move/from16 v0, p65

    invoke-direct {p0, v0}, Lcom/facebook/proxygen/NetworkStatus;->describeNetworkQuality(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/proxygen/NetworkStatus;->mLatencyQuality:Ljava/lang/String;

    .line 184115
    move/from16 v0, p66

    invoke-direct {p0, v0}, Lcom/facebook/proxygen/NetworkStatus;->describeNetworkQuality(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/proxygen/NetworkStatus;->mUploadBwQuality:Ljava/lang/String;

    .line 184116
    move/from16 v0, p67

    invoke-direct {p0, v0}, Lcom/facebook/proxygen/NetworkStatus;->describeNetworkQuality(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/proxygen/NetworkStatus;->mDownloadBwQuality:Ljava/lang/String;

    .line 184117
    return-void
.end method

.method private describeNetworkQuality(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 184118
    packed-switch p1, :pswitch_data_0

    .line 184119
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 184120
    :pswitch_0
    const-string v0, "poor"

    goto :goto_0

    .line 184121
    :pswitch_1
    const-string v0, "moderate"

    goto :goto_0

    .line 184122
    :pswitch_2
    const-string v0, "good"

    goto :goto_0

    .line 184123
    :pswitch_3
    const-string v0, "excellent"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getActiveReadSeconds()J
    .locals 2

    .prologue
    .line 184078
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mActiveReadSeconds:J

    return-wide v0
.end method

.method public getActiveWriteSeconds()J
    .locals 2

    .prologue
    .line 184124
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mActiveWriteSeconds:J

    return-wide v0
.end method

.method public getClosedConn()J
    .locals 2

    .prologue
    .line 184125
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mClosedConn:J

    return-wide v0
.end method

.method public getDequeuedReq()J
    .locals 2

    .prologue
    .line 184126
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mDequeuedReq:J

    return-wide v0
.end method

.method public getDownloadBwQuality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184127
    iget-object v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mDownloadBwQuality:Ljava/lang/String;

    return-object v0
.end method

.method public getEgressAvg()J
    .locals 2

    .prologue
    .line 184128
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mEgressAvg:J

    return-wide v0
.end method

.method public getEgressMax()J
    .locals 2

    .prologue
    .line 184129
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mEgressMax:J

    return-wide v0
.end method

.method public getEgressWindowSize()J
    .locals 2

    .prologue
    .line 184135
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mEgressWindowSize:J

    return-wide v0
.end method

.method public getEnqueuedReq()J
    .locals 2

    .prologue
    .line 184130
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mEnqueuedReq:J

    return-wide v0
.end method

.method public getFinishedReq()J
    .locals 2

    .prologue
    .line 184137
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mFinishedReq:J

    return-wide v0
.end method

.method public getFlowControlHit()J
    .locals 2

    .prologue
    .line 184136
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mFlowControlHit:J

    return-wide v0
.end method

.method public getInQueueReq()J
    .locals 2

    .prologue
    .line 184138
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mInQueueReq:J

    return-wide v0
.end method

.method public getInflightConn()J
    .locals 2

    .prologue
    .line 184134
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mInflightConn:J

    return-wide v0
.end method

.method public getInflightReq()J
    .locals 2

    .prologue
    .line 184133
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mInflightReq:J

    return-wide v0
.end method

.method public getIngressAvg()J
    .locals 2

    .prologue
    .line 184132
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mIngressAvg:J

    return-wide v0
.end method

.method public getIngressMax()J
    .locals 2

    .prologue
    .line 184131
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mIngressMax:J

    return-wide v0
.end method

.method public getIngressWindowSize()J
    .locals 2

    .prologue
    .line 184079
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mIngressWindowSize:J

    return-wide v0
.end method

.method public getLatencyQuality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184060
    iget-object v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mLatencyQuality:Ljava/lang/String;

    return-object v0
.end method

.method public getMayHaveIdledMS()J
    .locals 2

    .prologue
    .line 184059
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mMayHaveIdledMS:J

    return-wide v0
.end method

.method public getMayHaveNetwork()Z
    .locals 1

    .prologue
    .line 184077
    iget-boolean v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mMayHaveNetwork:Z

    return v0
.end method

.method public getOpenedConn()J
    .locals 2

    .prologue
    .line 184061
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mOpenedConn:J

    return-wide v0
.end method

.method public getPriReqInflight()[J
    .locals 1

    .prologue
    .line 184062
    iget-object v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mPriReqInflight:[J

    return-object v0
.end method

.method public getReadCount()J
    .locals 2

    .prologue
    .line 184063
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReadCount:J

    return-wide v0
.end method

.method public getReqBwEgressAvg()J
    .locals 2

    .prologue
    .line 184064
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressAvg:J

    return-wide v0
.end method

.method public getReqBwEgressMax()J
    .locals 2

    .prologue
    .line 184065
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressMax:J

    return-wide v0
.end method

.method public getReqBwEgressSize()J
    .locals 2

    .prologue
    .line 184066
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressSize:J

    return-wide v0
.end method

.method public getReqBwEgressStdDev()D
    .locals 2

    .prologue
    .line 184067
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwEgressStdDev:D

    return-wide v0
.end method

.method public getReqBwIngressAvg()J
    .locals 2

    .prologue
    .line 184068
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressAvg:J

    return-wide v0
.end method

.method public getReqBwIngressMax()J
    .locals 2

    .prologue
    .line 184069
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressMax:J

    return-wide v0
.end method

.method public getReqBwIngressSize()J
    .locals 2

    .prologue
    .line 184070
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressSize:J

    return-wide v0
.end method

.method public getReqBwIngressStdDev()D
    .locals 2

    .prologue
    .line 184071
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mReqBwIngressStdDev:D

    return-wide v0
.end method

.method public getRttAvg()J
    .locals 2

    .prologue
    .line 184072
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mRttAvg:J

    return-wide v0
.end method

.method public getRttMax()J
    .locals 2

    .prologue
    .line 184073
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mRttMax:J

    return-wide v0
.end method

.method public getRttStdDev()D
    .locals 2

    .prologue
    .line 184074
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mRttStdDev:D

    return-wide v0
.end method

.method public getUploadBwQuality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184075
    iget-object v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mUploadBwQuality:Ljava/lang/String;

    return-object v0
.end method

.method public getWriteCount()J
    .locals 2

    .prologue
    .line 184076
    iget-wide v0, p0, Lcom/facebook/proxygen/NetworkStatus;->mWriteCount:J

    return-wide v0
.end method
