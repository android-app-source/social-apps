.class public Lcom/facebook/proxygen/TraceEventContext;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final RAND:Ljava/util/Random;


# instance fields
.field private mObservers:[LX/4ia;

.field public mParentID:I

.field private final mSamplePolicy:LX/1iR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186376
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/facebook/proxygen/TraceEventContext;->RAND:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(LX/1iR;)V
    .locals 1

    .prologue
    .line 186374
    const/4 v0, 0x0

    new-array v0, v0, [LX/4ia;

    invoke-direct {p0, v0, p1}, Lcom/facebook/proxygen/TraceEventContext;-><init>([LX/4ia;LX/1iR;)V

    .line 186375
    return-void
.end method

.method public constructor <init>([LX/4ia;)V
    .locals 1

    .prologue
    .line 186372
    new-instance v0, LX/4id;

    invoke-direct {v0}, LX/4id;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/facebook/proxygen/TraceEventContext;-><init>([LX/4ia;LX/1iR;)V

    .line 186373
    return-void
.end method

.method public constructor <init>([LX/4ia;LX/1iR;)V
    .locals 2

    .prologue
    .line 186377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186378
    sget-object v0, Lcom/facebook/proxygen/TraceEventContext;->RAND:Ljava/util/Random;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/proxygen/TraceEventContext;->mParentID:I

    .line 186379
    iput-object p1, p0, Lcom/facebook/proxygen/TraceEventContext;->mObservers:[LX/4ia;

    .line 186380
    iput-object p2, p0, Lcom/facebook/proxygen/TraceEventContext;->mSamplePolicy:LX/1iR;

    .line 186381
    return-void
.end method


# virtual methods
.method public getParentID()I
    .locals 1

    .prologue
    .line 186371
    iget v0, p0, Lcom/facebook/proxygen/TraceEventContext;->mParentID:I

    return v0
.end method

.method public informAllObservers([Lcom/facebook/proxygen/TraceEvent;)V
    .locals 4

    .prologue
    .line 186367
    iget-object v1, p0, Lcom/facebook/proxygen/TraceEventContext;->mObservers:[LX/4ia;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 186368
    invoke-interface {v3, p1}, LX/4ia;->traceEventAvailable([Lcom/facebook/proxygen/TraceEvent;)V

    .line 186369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186370
    :cond_0
    return-void
.end method

.method public needPublishEvent()Z
    .locals 1

    .prologue
    .line 186366
    iget-object v0, p0, Lcom/facebook/proxygen/TraceEventContext;->mSamplePolicy:LX/1iR;

    invoke-interface {v0}, LX/1iR;->isSampled()Z

    move-result v0

    return v0
.end method
