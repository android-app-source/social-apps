.class public Lcom/facebook/proxygen/NetworkStatusMonitor;
.super LX/160;
.source ""


# instance fields
.field public mAnalyticsLogger:Lcom/facebook/proxygen/AnalyticsLogger;

.field private mBandwidthSplitSize:I

.field public mCacheDurationInSeconds:I

.field private mCacheStatus:Lcom/facebook/proxygen/NetworkStatus;

.field private mDNSPort:I

.field private mDNSReachabilityEnabled:Z

.field private mDNSServer:Ljava/lang/String;

.field private final mEventBase:Lcom/facebook/proxygen/EventBase;

.field private mGoodDL:I

.field private mGoodRtt:I

.field private mGoodUL:I

.field private mHostname:Ljava/lang/String;

.field private mInitialized:Z

.field private mMaxPriority:I

.field private mModerateDL:I

.field private mModerateRtt:I

.field private mModerateUL:I

.field private mPoorDL:I

.field private mPoorRtt:I

.field private mPoorUL:I

.field public mRadioMonitorEnabled:Z


# direct methods
.method public constructor <init>(Lcom/facebook/proxygen/EventBase;)V
    .locals 1

    .prologue
    .line 184023
    invoke-direct {p0}, LX/160;-><init>()V

    .line 184024
    const/4 v0, 0x7

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mMaxPriority:I

    .line 184025
    const/16 v0, 0x2710

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mBandwidthSplitSize:I

    .line 184026
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mCacheDurationInSeconds:I

    .line 184027
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorRtt:I

    .line 184028
    const/16 v0, 0x96

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateRtt:I

    .line 184029
    const/16 v0, 0x32

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodRtt:I

    .line 184030
    const/16 v0, 0x7530

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorUL:I

    .line 184031
    const v0, 0x1adb0

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateUL:I

    .line 184032
    const v0, 0x61a80

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodUL:I

    .line 184033
    const v0, 0x249f0

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorDL:I

    .line 184034
    const v0, 0x86470

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateDL:I

    .line 184035
    const v0, 0x1e8480

    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodDL:I

    .line 184036
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mDNSServer:Ljava/lang/String;

    .line 184037
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mHostname:Ljava/lang/String;

    .line 184038
    iput-object p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mEventBase:Lcom/facebook/proxygen/EventBase;

    .line 184039
    return-void
.end method

.method private native getNetworkStatusNative()V
.end method


# virtual methods
.method public native close()V
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 184040
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184041
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 184042
    return-void

    .line 184043
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public native getDownloadBandwidthQualityNative()I
.end method

.method public native getInboundHistogramTraceDataNative()[J
.end method

.method public native getLatencyQualityNative()I
.end method

.method public native getLatestObservedConnectionQualitiesNative()[Lcom/facebook/proxygen/ObservedConnQuality;
.end method

.method public getNetworkQualityString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 184044
    packed-switch p1, :pswitch_data_0

    .line 184045
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 184046
    :pswitch_0
    const-string v0, "POOR"

    goto :goto_0

    .line 184047
    :pswitch_1
    const-string v0, "MODERATE"

    goto :goto_0

    .line 184048
    :pswitch_2
    const-string v0, "GOOD"

    goto :goto_0

    .line 184049
    :pswitch_3
    const-string v0, "EXCELLENT"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getNetworkStatus()Lcom/facebook/proxygen/NetworkStatus;
    .locals 1

    .prologue
    .line 184050
    iget-boolean v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mInitialized:Z

    if-nez v0, :cond_0

    .line 184051
    const/4 v0, 0x0

    .line 184052
    :goto_0
    return-object v0

    .line 184053
    :cond_0
    invoke-direct {p0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->getNetworkStatusNative()V

    .line 184054
    iget-object v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mCacheStatus:Lcom/facebook/proxygen/NetworkStatus;

    goto :goto_0
.end method

.method public native getRadioData()Ljava/lang/Object;
.end method

.method public native getUploadBandwidthQualityNative()I
.end method

.method public init(I)V
    .locals 18

    .prologue
    .line 184056
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mEventBase:Lcom/facebook/proxygen/EventBase;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mMaxPriority:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mBandwidthSplitSize:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mCacheDurationInSeconds:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorRtt:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateRtt:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodRtt:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorUL:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateUL:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodUL:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorDL:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateDL:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodDL:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mAnalyticsLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mRadioMonitorEnabled:Z

    move/from16 v17, v0

    move-object/from16 v1, p0

    move/from16 v3, p1

    invoke-virtual/range {v1 .. v17}, Lcom/facebook/proxygen/NetworkStatusMonitor;->init(Lcom/facebook/proxygen/EventBase;IIIIIIIIIIIIILcom/facebook/proxygen/AnalyticsLogger;Z)V

    .line 184057
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mInitialized:Z

    .line 184058
    return-void
.end method

.method public native init(Lcom/facebook/proxygen/EventBase;IIIIIIIIIIIIILcom/facebook/proxygen/AnalyticsLogger;Z)V
.end method

.method public isDNSReachabilityEnabled()Z
    .locals 1

    .prologue
    .line 184055
    iget-boolean v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mDNSReachabilityEnabled:Z

    return v0
.end method

.method public setAnalyticsLogger(Lcom/facebook/proxygen/AnalyticsLogger;)V
    .locals 0

    .prologue
    .line 183995
    iput-object p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mAnalyticsLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    .line 183996
    return-void
.end method

.method public native setAppForegrounded(Z)V
.end method

.method public setBandwidthSplitSize(I)V
    .locals 0

    .prologue
    .line 183999
    iput p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mBandwidthSplitSize:I

    .line 184000
    return-void
.end method

.method public setCacheDuration(I)V
    .locals 0

    .prologue
    .line 184001
    iput p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mCacheDurationInSeconds:I

    .line 184002
    return-void
.end method

.method public setDNSPort(I)V
    .locals 0

    .prologue
    .line 183997
    iput p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mDNSPort:I

    .line 183998
    return-void
.end method

.method public setDNSReachabilityEnabled(Z)V
    .locals 0

    .prologue
    .line 184003
    iput-boolean p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mDNSReachabilityEnabled:Z

    .line 184004
    return-void
.end method

.method public setDNSServer(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184005
    iput-object p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mDNSServer:Ljava/lang/String;

    .line 184006
    return-void
.end method

.method public setHostname(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184007
    iput-object p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mHostname:Ljava/lang/String;

    .line 184008
    return-void
.end method

.method public setMaxPriority(I)V
    .locals 0

    .prologue
    .line 184009
    iput p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mMaxPriority:I

    .line 184010
    return-void
.end method

.method public native setNetworkType(II)V
.end method

.method public setQualityBenchmarks(IIIIIIIII)V
    .locals 0

    .prologue
    .line 184011
    iput p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorRtt:I

    .line 184012
    iput p2, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateRtt:I

    .line 184013
    iput p3, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodRtt:I

    .line 184014
    iput p4, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorUL:I

    .line 184015
    iput p5, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateUL:I

    .line 184016
    iput p6, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodUL:I

    .line 184017
    iput p7, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mPoorDL:I

    .line 184018
    iput p8, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mModerateDL:I

    .line 184019
    iput p9, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mGoodDL:I

    .line 184020
    return-void
.end method

.method public setRadioMonitorEnabled(Z)V
    .locals 0

    .prologue
    .line 184021
    iput-boolean p1, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mRadioMonitorEnabled:Z

    .line 184022
    return-void
.end method

.method public native startInboundHistogramTracingNative(I)V
.end method

.method public native stopInboundHistogramTracingNative()V
.end method
