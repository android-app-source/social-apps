.class public Lcom/facebook/proxygen/HTTPThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mEventBase:Lcom/facebook/proxygen/EventBase;

.field private mExHandler:LX/4iO;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 295643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEventBase()Lcom/facebook/proxygen/EventBase;
    .locals 2

    .prologue
    .line 295644
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPThread;->mEventBase:Lcom/facebook/proxygen/EventBase;

    const-string v1, "EventBase has not been created yet"

    invoke-static {v0, v1}, LX/1hH;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295645
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPThread;->mEventBase:Lcom/facebook/proxygen/EventBase;

    return-object v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 295646
    monitor-enter p0

    .line 295647
    :try_start_0
    new-instance v0, Lcom/facebook/proxygen/EventBase;

    invoke-direct {v0}, Lcom/facebook/proxygen/EventBase;-><init>()V

    iput-object v0, p0, Lcom/facebook/proxygen/HTTPThread;->mEventBase:Lcom/facebook/proxygen/EventBase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295648
    const v0, 0x3159d5d2

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 295649
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 295650
    :try_start_2
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPThread;->mEventBase:Lcom/facebook/proxygen/EventBase;

    if-eqz v0, :cond_0

    .line 295651
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPThread;->mEventBase:Lcom/facebook/proxygen/EventBase;

    invoke-virtual {v0}, Lcom/facebook/proxygen/EventBase;->loopForever()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 295652
    :cond_0
    :goto_0
    return-void

    .line 295653
    :catchall_0
    move-exception v0

    const v1, 0x24610a2b

    :try_start_3
    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    throw v0

    .line 295654
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 295655
    :catch_0
    move-exception v0

    .line 295656
    iget-object v1, p0, Lcom/facebook/proxygen/HTTPThread;->mExHandler:LX/4iO;

    if-eqz v1, :cond_1

    .line 295657
    iget-object v1, p0, Lcom/facebook/proxygen/HTTPThread;->mExHandler:LX/4iO;

    invoke-interface {v1, v0}, LX/4iO;->handle(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 295658
    :cond_1
    throw v0
.end method

.method public setExceptionHandler(LX/4iO;)Lcom/facebook/proxygen/HTTPThread;
    .locals 0

    .prologue
    .line 295659
    iput-object p1, p0, Lcom/facebook/proxygen/HTTPThread;->mExHandler:LX/4iO;

    .line 295660
    return-object p0
.end method

.method public declared-synchronized waitForInitialization()V
    .locals 1

    .prologue
    .line 295661
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/proxygen/HTTPThread;->mEventBase:Lcom/facebook/proxygen/EventBase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 295662
    const v0, -0x4dbb99d4

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 295663
    :catch_0
    goto :goto_0

    .line 295664
    :cond_0
    monitor-exit p0

    return-void

    .line 295665
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
