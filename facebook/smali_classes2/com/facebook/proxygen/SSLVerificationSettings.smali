.class public Lcom/facebook/proxygen/SSLVerificationSettings;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private enableTimestampVerification:Z

.field private enforceCertKeyLengthVerification:Z

.field private trustedReferenceTimestamp:J


# direct methods
.method public constructor <init>(ZZJ)V
    .locals 1

    .prologue
    .line 186154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186155
    iput-boolean p1, p0, Lcom/facebook/proxygen/SSLVerificationSettings;->enableTimestampVerification:Z

    .line 186156
    iput-boolean p2, p0, Lcom/facebook/proxygen/SSLVerificationSettings;->enforceCertKeyLengthVerification:Z

    .line 186157
    iput-wide p3, p0, Lcom/facebook/proxygen/SSLVerificationSettings;->trustedReferenceTimestamp:J

    .line 186158
    return-void
.end method

.method public synthetic constructor <init>(ZZJLX/4ic;)V
    .locals 1

    .prologue
    .line 186159
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/proxygen/SSLVerificationSettings;-><init>(ZZJ)V

    return-void
.end method
