.class public Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/analytics2/logger/Uploader;
.implements LX/0Ya;


# instance fields
.field public a:LX/11H;

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 132757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132758
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;

    invoke-static {v2}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v1

    check-cast v1, LX/11H;

    const/16 v0, 0x1445

    invoke-static {v2, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    iput-object v1, p0, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;->a:LX/11H;

    iput-object v2, p0, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;->b:LX/0Or;

    .line 132759
    return-void
.end method

.method private static a(LX/2We;)LX/2YR;
    .locals 4

    .prologue
    .line 132753
    new-instance v1, Ljava/io/StringWriter;

    invoke-interface {p0}, LX/2We;->a()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/io/StringWriter;-><init>(I)V

    .line 132754
    :try_start_0
    invoke-interface {p0, v1}, LX/2We;->a(Ljava/io/Writer;)V

    .line 132755
    new-instance v0, LX/2YR;

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, LX/2We;->b()Z

    move-result v3

    invoke-direct {v0, v2, v3}, LX/2YR;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132756
    invoke-virtual {v1}, Ljava/io/StringWriter;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/StringWriter;->close()V

    throw v0
.end method

.method private static a(LX/11H;LX/0e6;Ljava/lang/Object;LX/14U;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<REQUEST:",
            "Ljava/lang/Object;",
            "RESPONSE:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0e6",
            "<TREQUEST;TRESPONSE;>;TREQUEST;",
            "LX/14U;",
            ")V"
        }
    .end annotation

    .prologue
    .line 132760
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 132761
    return-void

    .line 132762
    :catch_0
    move-exception v0

    .line 132763
    throw v0

    .line 132764
    :catch_1
    move-exception v0

    .line 132765
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 132766
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 132767
    throw v1

    .line 132768
    :catch_2
    move-exception v0

    .line 132769
    invoke-static {v0}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;)V

    .line 132770
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 132771
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 132772
    throw v1
.end method


# virtual methods
.method public final a(LX/2Xd;LX/2YV;)V
    .locals 4

    .prologue
    .line 132734
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    .line 132735
    iget-object v0, p1, LX/2Xd;->b:LX/2YU;

    move-object v0, v0

    .line 132736
    sget-object v2, LX/2YU;->BOOTSTRAP:LX/2YU;

    if-ne v0, v2, :cond_0

    .line 132737
    sget-object v0, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v1, v0}, LX/14U;->a(LX/14V;)V

    .line 132738
    :cond_0
    iget-object v0, p1, LX/2Xd;->a:LX/0pD;

    move-object v0, v0

    .line 132739
    sget-object v2, LX/2YM;->a:[I

    invoke-virtual {v0}, LX/0pD;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 132740
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    move-object v0, v2

    .line 132741
    iput-object v0, v1, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 132742
    iget-object v0, p1, LX/2Xd;->c:LX/2We;

    move-object v2, v0

    .line 132743
    :try_start_0
    new-instance v3, LX/2YN;

    invoke-direct {v3, p2}, LX/2YN;-><init>(LX/2YV;)V

    .line 132744
    iget-object v0, p0, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 132745
    iget-object v0, p0, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;->a:LX/11H;

    invoke-static {v2}, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;->a(LX/2We;)LX/2YR;

    move-result-object v2

    invoke-static {v0, v3, v2, v1}, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;->a(LX/11H;LX/0e6;Ljava/lang/Object;LX/14U;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132746
    :cond_1
    :goto_1
    return-void

    .line 132747
    :catch_0
    move-exception v0

    .line 132748
    iget-object v1, p2, LX/2YV;->a:LX/2Wd;

    invoke-interface {v1}, LX/2Wd;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 132749
    iget-object v1, p2, LX/2YV;->a:LX/2Wd;

    invoke-interface {v1}, LX/2Wd;->c()V

    .line 132750
    :cond_2
    iget-object v1, p2, LX/2YV;->c:LX/2WY;

    invoke-interface {v1, v0}, LX/2WY;->a(Ljava/io/IOException;)V

    .line 132751
    goto :goto_1

    .line 132752
    :pswitch_0
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
