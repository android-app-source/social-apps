.class public Lcom/facebook/analytics2/loggermodule/Analytics2HandlerThreadFactory;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/analytics2/logger/HandlerThreadFactory;
.implements LX/0Ya;


# instance fields
.field public a:LX/0Zr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 132543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132544
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/analytics2/loggermodule/Analytics2HandlerThreadFactory;

    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    iput-object v0, p0, Lcom/facebook/analytics2/loggermodule/Analytics2HandlerThreadFactory;->a:LX/0Zr;

    .line 132545
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/HandlerThread;
    .locals 1

    .prologue
    .line 132542
    iget-object v0, p0, Lcom/facebook/analytics2/loggermodule/Analytics2HandlerThreadFactory;->a:LX/0Zr;

    invoke-virtual {v0, p1}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)Landroid/os/HandlerThread;
    .locals 2

    .prologue
    .line 132540
    invoke-static {p2}, LX/0TP;->getClosestThreadPriorityFromAndroidThreadPriority(I)LX/0TP;

    move-result-object v0

    .line 132541
    iget-object v1, p0, Lcom/facebook/analytics2/loggermodule/Analytics2HandlerThreadFactory;->a:LX/0Zr;

    invoke-virtual {v1, p1, v0}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v0

    return-object v0
.end method
