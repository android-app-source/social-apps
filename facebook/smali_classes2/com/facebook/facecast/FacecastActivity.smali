.class public Lcom/facebook/facecast/FacecastActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/1b6;
.implements LX/1b7;
.implements LX/1b8;
.implements LX/1b9;
.implements LX/1bA;
.implements LX/1bB;
.implements LX/1bC;
.implements LX/1bD;
.implements LX/1bE;
.implements LX/1bF;
.implements LX/1bG;
.implements LX/1bH;
.implements LX/1bI;
.implements LX/1bJ;
.implements LX/1bK;
.implements LX/1bL;
.implements LX/1bM;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field private static final q:Ljava/lang/String;

.field private static final r:[Ljava/lang/String;


# instance fields
.field private A:LX/AV6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private B:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private C:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private D:LX/1b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/AVh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private F:LX/Aag;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private G:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private H:LX/BSb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private I:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private J:LX/AYq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private K:LX/6Rg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private L:Lcom/facebook/facecast/protocol/FacecastNetworker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private M:LX/AVC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private N:LX/6Re;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private O:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AWK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Q:LX/AVB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private R:LX/0Zr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/Abh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private T:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private V:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private W:LX/8wR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private X:LX/AVc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Y:LX/1Nq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final Z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AWT;",
            ">;"
        }
    .end annotation
.end field

.field private aA:Z

.field private aB:LX/AVA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aC:I

.field private aD:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aE:Z

.field private aF:Z

.field private aG:Z

.field private aH:Z

.field private aI:Z

.field private aJ:Z

.field private aK:Z

.field private aL:LX/0Ab;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aM:Z

.field private aN:Ljava/util/concurrent/CountDownLatch;

.field private aO:Ljava/util/concurrent/CountDownLatch;

.field public aP:LX/AYK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aR:LX/AYE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aS:LX/AXg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aT:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aU:LX/AXY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aV:LX/AXt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aW:LX/AY1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aX:LX/AY2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aZ:LX/AXa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final aa:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private ab:Landroid/os/HandlerThread;

.field private ac:Landroid/os/HandlerThread;

.field private ad:Landroid/view/ViewGroup;

.field private ae:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:Lcom/facebook/http/protocol/ApiErrorResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:Landroid/view/Window;

.field private ai:J

.field private aj:J

.field private volatile ak:Z

.field private al:Z

.field private am:Z

.field private an:Ljava/lang/Runnable;

.field private ao:LX/2EJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ap:Lcom/facebook/facecast/model/FacecastCompositionData;

.field private aq:LX/AV1;

.field private ar:LX/AV3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:LX/AV5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private at:Ljava/lang/Runnable;

.field private au:Z

.field private av:F

.field private aw:Z

.field private ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ay:J

.field public az:Z

.field public ba:LX/AWe;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:LX/AWa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bc:LX/AYk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:LX/AWY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

.field private s:LX/AV2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/AVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/7RY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 280144
    const-class v0, Lcom/facebook/facecast/FacecastActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/FacecastActivity;->q:Ljava/lang/String;

    .line 280145
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/facecast/FacecastActivity;->r:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 280138
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 280139
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 280140
    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    .line 280141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    .line 280142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aa:Ljava/util/List;

    .line 280143
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/facecast/FacecastActivity;->aC:I

    return-void
.end method

.method private A()Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 280129
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 280130
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 280131
    sget-object v1, LX/AVE;->FAILED:LX/AVE;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/AVE;->FINISHED:LX/AVE;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/AVE;->UNINITIALIZED:LX/AVE;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/AVE;->STARTING:LX/AVE;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/AVE;->SHOW_END_SCREEN:LX/AVE;

    if-ne v0, v1, :cond_1

    :cond_0
    move v0, v2

    .line 280132
    :goto_0
    return v0

    .line 280133
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->w:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 280134
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastInterruptionLimitInSeconds:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    .line 280135
    :goto_1
    iget-wide v6, p0, Lcom/facebook/facecast/FacecastActivity;->ay:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_3

    iget-wide v6, p0, Lcom/facebook/facecast/FacecastActivity;->ay:J

    sub-long/2addr v4, v6

    cmp-long v0, v4, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    .line 280136
    :cond_2
    const-wide/32 v0, 0x7fffffff

    goto :goto_1

    :cond_3
    move v0, v2

    .line 280137
    goto :goto_0
.end method

.method public static B(Lcom/facebook/facecast/FacecastActivity;)V
    .locals 3

    .prologue
    .line 280119
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->au:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_composition_editing"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 280120
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    .line 280121
    iget-object v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 280122
    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    .line 280123
    iget-object v2, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    if-eqz v2, :cond_4

    iget-object v2, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 280124
    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    invoke-virtual {v1}, LX/AZK;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 280125
    if-nez v0, :cond_1

    .line 280126
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 280127
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-virtual {v1}, LX/AWy;->g()V

    .line 280128
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private C()V
    .locals 5

    .prologue
    .line 280115
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->az:Z

    if-eqz v0, :cond_0

    .line 280116
    :goto_0
    return-void

    .line 280117
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->az:Z

    .line 280118
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->v:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    sget-object v1, Lcom/facebook/facecast/FacecastActivity;->r:[Ljava/lang/String;

    const v2, 0x7f080ca6

    invoke-virtual {p0, v2}, Lcom/facebook/facecast/FacecastActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f080ca7

    invoke-virtual {p0, v3}, Lcom/facebook/facecast/FacecastActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/AUs;

    invoke-direct {v4, p0}, LX/AUs;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0i5;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V

    goto :goto_0
.end method

.method private D()I
    .locals 1

    .prologue
    .line 280114
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method private E()Z
    .locals 2

    .prologue
    .line 280113
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->d()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/facebook/facecast/FacecastActivity;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()Z
    .locals 2

    .prologue
    .line 280112
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()V
    .locals 4

    .prologue
    .line 280105
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ah:Landroid/view/Window;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 280106
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->K:LX/6Rg;

    .line 280107
    iget-object v1, v0, LX/6Rg;->a:LX/0ad;

    sget-short v2, LX/1v6;->w:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 280108
    if-eqz v0, :cond_0

    .line 280109
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->I()V

    .line 280110
    :goto_0
    return-void

    .line 280111
    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->H()V

    goto :goto_0
.end method

.method private H()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 280085
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    if-nez v1, :cond_1

    move v9, v0

    .line 280086
    :goto_0
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aT:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    if-nez v1, :cond_0

    .line 280087
    iget-boolean v1, p0, Lcom/facebook/facecast/FacecastActivity;->aE:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aD:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v1, :cond_2

    .line 280088
    new-instance v0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aD:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;-><init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    move-object v1, p0

    .line 280089
    :goto_1
    iput-object v0, v1, Lcom/facebook/facecast/FacecastActivity;->aT:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    .line 280090
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aT:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    .line 280091
    iput-object p0, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->r:LX/1bC;

    .line 280092
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aT:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    iget-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->ai:J

    iget-wide v4, p0, Lcom/facebook/facecast/FacecastActivity;->aj:J

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    iget-object v6, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    iget-object v7, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 280093
    iget-object v8, v0, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v8, v8

    .line 280094
    invoke-virtual/range {v1 .. v9}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;I)V

    .line 280095
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aT:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    invoke-direct {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 280096
    return-void

    .line 280097
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    .line 280098
    iget-object v2, v1, LX/AYE;->n:LX/AXm;

    .line 280099
    iget-object v1, v2, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 280100
    iget v2, v1, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->n:I

    move v1, v2

    .line 280101
    move v2, v1

    .line 280102
    move v9, v2

    .line 280103
    goto :goto_0

    .line 280104
    :cond_2
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->G:LX/0ad;

    sget-short v2, LX/1v6;->m:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, LX/AaP;

    invoke-direct {v0, p0}, LX/AaP;-><init>(Landroid/content/Context;)V

    move-object v1, p0

    goto :goto_1

    :cond_3
    new-instance v0, LX/AX9;

    invoke-direct {v0, p0}, LX/AX9;-><init>(Landroid/content/Context;)V

    move-object v1, p0

    goto :goto_1
.end method

.method private I()V
    .locals 14

    .prologue
    .line 279904
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aU:LX/AXY;

    if-nez v0, :cond_0

    .line 279905
    new-instance v0, LX/AXY;

    invoke-direct {v0, p0}, LX/AXY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aU:LX/AXY;

    .line 279906
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aU:LX/AXY;

    new-instance v1, LX/AUt;

    invoke-direct {v1, p0}, LX/AUt;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    .line 279907
    iput-object v1, v0, LX/AXY;->V:LX/AUt;

    .line 279908
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aU:LX/AXY;

    iget-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->ai:J

    iget-wide v4, p0, Lcom/facebook/facecast/FacecastActivity;->aj:J

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    iget-object v6, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    iget-object v7, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279909
    iget-object v8, v0, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v8, v8

    .line 279910
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279911
    iget-object v9, v0, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v9, v9

    .line 279912
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->d()F

    move-result v10

    iget-object v11, p0, Lcom/facebook/facecast/FacecastActivity;->aD:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    iget-boolean v12, p0, Lcom/facebook/facecast/FacecastActivity;->aE:Z

    iget-boolean v13, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    invoke-virtual/range {v1 .. v13}, LX/AXY;->a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/facecast/model/FacecastPrivacyData;FLcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;ZZ)V

    .line 279913
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aU:LX/AXY;

    invoke-direct {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279914
    return-void
.end method

.method private J()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 280071
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280072
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ac:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 280073
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->K()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method private K()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 280068
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280069
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ab:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 280070
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->n()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method private L()V
    .locals 4

    .prologue
    .line 280060
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    const/4 v1, 0x0

    .line 280061
    iput-boolean v1, v0, LX/AV1;->s:Z

    .line 280062
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->f()V

    .line 280063
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 280064
    iget-object v1, v0, LX/AY1;->l:LX/AVP;

    .line 280065
    iget-object v2, v1, LX/AVP;->b:LX/AVN;

    iget-object v3, v1, LX/AVP;->b:LX/AVN;

    const/16 p0, 0xa

    invoke-virtual {v3, p0}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 280066
    iget-object v1, v0, LX/AY1;->h:LX/3HT;

    iget-object v2, v0, LX/AY1;->k:LX/AXy;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 280067
    return-void
.end method

.method private M()V
    .locals 1

    .prologue
    .line 280054
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, p0}, LX/7RY;->a(LX/1bL;)V

    .line 280055
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, p0}, LX/7RY;->a(LX/1bJ;)V

    .line 280056
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ae:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-nez v0, :cond_0

    .line 280057
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, p0}, LX/7RY;->a(LX/1bK;)V

    .line 280058
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->p()V

    .line 280059
    :cond_0
    return-void
.end method

.method private N()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 280033
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-nez v0, :cond_0

    .line 280034
    new-instance v0, LX/AY1;

    invoke-direct {v0, p0}, LX/AY1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 280035
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 280036
    iget-object v2, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    move-object v2, v2

    .line 280037
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->au:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/facecast/FacecastPreviewView;->setSquareScreen(Z)V

    .line 280038
    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWK;

    iget-object v3, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    invoke-virtual {v3}, LX/1b4;->X()Z

    move-result v3

    .line 280039
    new-instance v4, LX/AVP;

    iget-object v7, v2, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    iget-object v8, v2, LX/AY1;->c:LX/AVR;

    iget-object v10, v2, LX/AY1;->f:LX/0So;

    move-object v5, p0

    move-object v6, p0

    move-object v9, v0

    move v11, v3

    invoke-direct/range {v4 .. v11}, LX/AVP;-><init>(LX/1bI;LX/1bM;Landroid/view/SurfaceView;LX/AVR;LX/AWK;LX/0So;Z)V

    iput-object v4, v2, LX/AY1;->l:LX/AVP;

    .line 280040
    iget-object v4, v2, LX/AY1;->l:LX/AVP;

    iget-object v5, v2, LX/AY1;->g:LX/Ac6;

    invoke-virtual {v5}, LX/Ac6;->j()Z

    move-result v5

    .line 280041
    iput-boolean v5, v4, LX/AVP;->B:Z

    .line 280042
    iget-object v4, v2, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    invoke-virtual {v4}, Lcom/facebook/facecast/FacecastPreviewView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    iget-object v5, v2, LX/AY1;->l:LX/AVP;

    invoke-interface {v4, v5}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 280043
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->K()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->J()Landroid/os/Looper;

    move-result-object v3

    .line 280044
    iget-object v4, v0, LX/AY1;->l:LX/AVP;

    .line 280045
    new-instance v5, LX/AVN;

    invoke-direct {v5, v4, v2}, LX/AVN;-><init>(LX/AVP;Landroid/os/Looper;)V

    iput-object v5, v4, LX/AVP;->b:LX/AVN;

    .line 280046
    new-instance v4, LX/AY0;

    invoke-direct {v4, v0, v3}, LX/AY0;-><init>(LX/AY1;Landroid/os/Looper;)V

    iput-object v4, v0, LX/AY1;->o:LX/AY0;

    .line 280047
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 280048
    iget-object v2, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    move-object v0, v2

    .line 280049
    iput-object p0, v0, Lcom/facebook/facecast/FacecastPreviewView;->d:LX/1b8;

    .line 280050
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;I)V

    .line 280051
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->M()V

    .line 280052
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 280053
    goto :goto_0
.end method

.method public static O(Lcom/facebook/facecast/FacecastActivity;)V
    .locals 10

    .prologue
    .line 279999
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    if-eqz v0, :cond_1

    .line 280000
    :cond_0
    :goto_0
    return-void

    .line 280001
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->al:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->ak:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->am:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 280002
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    const/4 v2, 0x0

    .line 280003
    iget-object v3, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-nez v3, :cond_6

    .line 280004
    :cond_2
    :goto_1
    move v1, v2

    .line 280005
    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 280006
    if-eqz v0, :cond_0

    .line 280007
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 280008
    iget-boolean v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->w:Z

    if-nez v1, :cond_8

    .line 280009
    invoke-static {v0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n(Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;)V

    .line 280010
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 280011
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 280012
    iget-object v1, v0, LX/AWT;->d:Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    .line 280013
    iget-object v1, v0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v2, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->requestTransparentRegion(Landroid/view/View;)V

    .line 280014
    :cond_4
    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    :cond_6
    iget-object v3, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 280015
    iget-object v0, v3, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v3, v0

    .line 280016
    if-nez v3, :cond_7

    iget-object v3, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 280017
    iget-object v0, v3, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    move-object v3, v0

    .line 280018
    if-eqz v3, :cond_2

    :cond_7
    const/4 v2, 0x1

    goto :goto_1

    .line 280019
    :cond_8
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    .line 280020
    iget-object v2, v1, LX/AYY;->x:Lcom/facebook/widget/text/BetterTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 280021
    iget-object v2, v1, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->end()V

    .line 280022
    iget-object v2, v1, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 280023
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->v:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-eqz v1, :cond_3

    .line 280024
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    iget-object v2, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->v:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    const/4 v3, 0x1

    .line 280025
    iput-object v2, v1, LX/AZK;->F:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 280026
    iget-object v4, v1, LX/AZK;->F:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v4, v4, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->firstCommercialEligibleSecs:I

    int-to-long v5, v4

    const-wide/16 v7, 0x3c

    div-long/2addr v5, v7

    long-to-int v4, v5

    .line 280027
    iget-object v5, v1, LX/AZK;->q:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, LX/AZK;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080c61

    new-array v8, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    if-nez v4, :cond_9

    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 280028
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    iget-object v2, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->v:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 280029
    iput-object v2, v1, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 280030
    invoke-static {v1}, LX/AYY;->j(LX/AYY;)V

    .line 280031
    goto :goto_3

    :cond_9
    move v3, v4

    .line 280032
    goto :goto_4
.end method

.method private P()V
    .locals 2

    .prologue
    .line 279996
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->an:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 279997
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->am:Z

    .line 279998
    return-void
.end method

.method private Q()V
    .locals 12

    .prologue
    .line 279978
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    if-nez v0, :cond_1

    .line 279979
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    const-string v1, "no_response"

    invoke-virtual {v0, v1}, LX/AVT;->c(Ljava/lang/String;)V

    .line 279980
    :cond_0
    :goto_0
    return-void

    .line 279981
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ar:LX/AV3;

    if-nez v0, :cond_0

    .line 279982
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    if-nez v0, :cond_2

    .line 279983
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->A:LX/AV6;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    .line 279984
    new-instance v2, LX/AV5;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v4

    check-cast v4, LX/AVT;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    move-object v10, v1

    move-object v11, p0

    invoke-direct/range {v2 .. v11}, LX/AV5;-><init>(LX/03V;LX/AVT;Ljava/util/concurrent/ScheduledExecutorService;LX/0tX;LX/0Sh;Landroid/os/Handler;LX/0SG;Ljava/lang/String;LX/1b7;)V

    .line 279985
    move-object v0, v2

    .line 279986
    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    .line 279987
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    .line 279988
    iget-object v2, v0, LX/AV5;->f:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 279989
    invoke-static {v0}, LX/AV5;->e(LX/AV5;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 279990
    sget-object v2, LX/AV5;->a:Ljava/lang/String;

    const-string v3, "The service has already shutdown, it cannot be started any more."

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 279991
    :cond_3
    :goto_1
    goto :goto_0

    .line 279992
    :cond_4
    invoke-static {v0}, LX/AV5;->f(LX/AV5;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 279993
    iget-object v2, v0, LX/AV5;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;

    invoke-direct {v3, v0}, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;-><init>(LX/AV5;)V

    iget v4, v0, LX/AV5;->m:I

    int-to-long v4, v4

    iget v6, v0, LX/AV5;->l:I

    int-to-long v6, v6

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v2 .. v8}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    iput-object v2, v0, LX/AV5;->n:Ljava/util/concurrent/Future;

    .line 279994
    const-string v2, "copyright_monitor_start"

    invoke-static {v0, v2}, LX/AV5;->a$redex0(LX/AV5;Ljava/lang/String;)V

    .line 279995
    const/4 v2, 0x0

    iput v2, v0, LX/AV5;->m:I

    goto :goto_1
.end method

.method public static R(Lcom/facebook/facecast/FacecastActivity;)V
    .locals 1

    .prologue
    .line 279976
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->l()V

    .line 279977
    return-void
.end method

.method private S()Z
    .locals 2

    .prologue
    .line 279973
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279974
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 279975
    sget-object v1, LX/AVE;->RECORDING:LX/AVE;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private T()V
    .locals 9

    .prologue
    .line 279961
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aa:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279962
    :cond_0
    :goto_0
    return-void

    .line 279963
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->L:Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->aa:Ljava/util/List;

    .line 279964
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v5, v3, [F

    .line 279965
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 279966
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    aput v3, v5, v4

    .line 279967
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 279968
    :cond_2
    new-instance v3, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;

    invoke-direct {v3, v1, v5}, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;-><init>(Ljava/lang/String;[F)V

    .line 279969
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 279970
    const-string v4, "video_broadcast_update_commercial_break_time_offsets_key"

    invoke-virtual {v5, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 279971
    iget-object v3, v0, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    const-string v4, "video_broadcast_update_commercial_break_time_offsets_type"

    sget-object v6, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v7, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, -0x2f542959

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    .line 279972
    goto :goto_0
.end method

.method private U()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 279915
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v0, :cond_7

    .line 279916
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWK;

    invoke-virtual {v1, v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->a(LX/AWK;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279917
    :cond_0
    :goto_0
    return-void

    .line 279918
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWK;

    .line 279919
    iget-object v2, v1, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->l:LX/1FJ;

    if-nez v2, :cond_8

    .line 279920
    const/4 v2, 0x0

    .line 279921
    :goto_1
    move v0, v2

    .line 279922
    if-nez v0, :cond_0

    .line 279923
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    if-nez v0, :cond_2

    .line 279924
    new-instance v0, LX/AWa;

    invoke-direct {v0, p0}, LX/AWa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    .line 279925
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    .line 279926
    iget-boolean v1, v0, LX/AWT;->a:Z

    move v0, v1

    .line 279927
    if-nez v0, :cond_4

    .line 279928
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;LX/AWT;)V

    .line 279929
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    if-eqz v0, :cond_3

    .line 279930
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/AWe;->setVisibility(I)V

    .line 279931
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 279932
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    const/16 v0, 0x8

    .line 279933
    iget-object v2, v1, LX/AYY;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v2

    if-ne v2, v0, :cond_9

    .line 279934
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279935
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v1, v2

    .line 279936
    iget-object v2, v0, LX/AWa;->g:Lcom/facebook/user/tiles/UserTileView;

    new-instance v3, LX/AWZ;

    invoke-direct {v3, v0}, LX/AWZ;-><init>(LX/AWa;)V

    invoke-virtual {v2, v3}, Lcom/facebook/user/tiles/UserTileView;->setOnUserTileUpdatedListener(LX/8t6;)V

    .line 279937
    iget-object v2, v0, LX/AWa;->g:Lcom/facebook/user/tiles/UserTileView;

    iget-object v3, v0, LX/AWa;->a:LX/AWf;

    invoke-virtual {v0}, LX/AWa;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0624

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v3, v1, v4, v5}, LX/AWf;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;IZ)LX/8t9;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 279938
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    .line 279939
    iget-object v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->k:LX/1FJ;

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->k:LX/1FJ;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    :goto_3
    move-object v0, v1

    .line 279940
    if-nez v0, :cond_5

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    .line 279941
    iget-boolean v2, v1, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object v1, v2

    .line 279942
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 279943
    :cond_5
    if-nez v0, :cond_6

    .line 279944
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    .line 279945
    iget-object v1, v0, LX/AWa;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, LX/AWa;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0206c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 279946
    :goto_4
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecast/FacecastActivity$10;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/FacecastActivity$10;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    const v2, 0x16f89dcc

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto/16 :goto_0

    .line 279947
    :cond_6
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    .line 279948
    iget-object v2, v1, LX/AWa;->h:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 279949
    iget-object v2, v1, LX/AWa;->h:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 279950
    goto :goto_4

    .line 279951
    :cond_7
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWK;

    invoke-virtual {v0, v1}, LX/AWK;->a(Landroid/net/Uri;)V

    .line 279952
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWK;

    invoke-virtual {v0, v1}, LX/AWK;->a(Landroid/graphics/Bitmap;)V

    .line 279953
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    if-eqz v0, :cond_0

    .line 279954
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bb:LX/AWa;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    goto/16 :goto_0

    .line 279955
    :cond_8
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/AWK;->a(Landroid/net/Uri;)V

    .line 279956
    iget-object v2, v1, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->l:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, LX/AWK;->a(Landroid/graphics/Bitmap;)V

    .line 279957
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 279958
    :cond_9
    iget-object v2, v1, LX/AYY;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 279959
    iget-object v2, v1, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 279960
    iget-object v2, v1, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_3
.end method

.method private V()V
    .locals 3

    .prologue
    .line 280309
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->V:Lcom/facebook/content/SecureContextHelper;

    .line 280310
    sget-object v1, LX/8AB;->LIVE_VIDEO:LX/8AB;

    .line 280311
    new-instance v2, LX/8AA;

    invoke-direct {v2, v1}, LX/8AA;-><init>(LX/8AB;)V

    sget-object v1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v2, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    .line 280312
    invoke-static {p0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    move-object v1, v1

    .line 280313
    const/16 v2, 0x1db3

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 280314
    return-void
.end method

.method private W()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 280181
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->p:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    if-nez v1, :cond_1

    .line 280182
    :cond_0
    :goto_0
    return v0

    .line 280183
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->p:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v1}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->c()Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v1

    .line 280184
    invoke-virtual {v1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 280185
    invoke-virtual {v1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 280186
    iget-object p0, v2, Lcom/facebook/facecast/model/FacecastCompositionData;->d:Ljava/lang/String;

    move-object v2, p0

    .line 280187
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280188
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/21D;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 280308
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/21D;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Z)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/21D;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Z)Landroid/os/Bundle;
    .locals 2
    .param p0    # LX/21D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280301
    new-instance v0, LX/AWP;

    invoke-direct {v0}, LX/AWP;-><init>()V

    .line 280302
    iput-object p1, v0, LX/AWP;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 280303
    move-object v0, v0

    .line 280304
    iput-object p0, v0, LX/AWP;->k:LX/21D;

    .line 280305
    move-object v0, v0

    .line 280306
    invoke-virtual {v0}, LX/AWP;->a()Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object v0

    .line 280307
    const/4 v1, 0x1

    invoke-static {v0, v1, p2}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZ)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZ)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 280300
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZLcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZLcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Landroid/os/Bundle;
    .locals 2
    .param p3    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280293
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280294
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 280295
    const-string v1, "extra_composition_data"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 280296
    const-string v1, "extra_composition_editing"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 280297
    const-string v1, "extra_is_posting_as_page"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 280298
    const-string v1, "extra_prompt_plugin_config"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 280299
    return-object v0
.end method

.method private a(LX/AWT;)V
    .locals 1

    .prologue
    .line 280291
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;I)V

    .line 280292
    return-void
.end method

.method private a(LX/AWT;I)V
    .locals 2

    .prologue
    .line 280288
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ad:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    invoke-virtual {p1, v0, v1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;I)V

    .line 280289
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 280290
    return-void
.end method

.method private a(LX/AWT;LX/AWT;)V
    .locals 1

    .prologue
    .line 280286
    invoke-virtual {p2}, LX/AWT;->getFirstIndexInParent()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;I)V

    .line 280287
    return-void
.end method

.method private static a(Lcom/facebook/facecast/FacecastActivity;LX/AV2;LX/AVT;Landroid/os/Handler;LX/0i4;LX/0SG;LX/AVF;LX/0kL;LX/7RY;LX/AV6;LX/Ac6;LX/1b4;LX/1b3;LX/AVh;LX/Aag;LX/0ad;LX/BSb;LX/0kb;LX/AYq;LX/6Rg;Lcom/facebook/facecast/protocol/FacecastNetworker;LX/AVC;LX/6Re;LX/0hB;LX/0Ot;LX/AVB;LX/0Zr;LX/Abh;LX/0Or;Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;Lcom/facebook/content/SecureContextHelper;LX/8wR;LX/AVc;LX/1Nq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecast/FacecastActivity;",
            "LX/AV2;",
            "LX/AVT;",
            "Landroid/os/Handler;",
            "LX/0i4;",
            "LX/0SG;",
            "LX/AVF;",
            "LX/0kL;",
            "LX/7RY;",
            "LX/AV6;",
            "LX/Ac6;",
            "LX/1b4;",
            "LX/1b3;",
            "LX/AVh;",
            "LX/Aag;",
            "LX/0ad;",
            "LX/BSb;",
            "LX/0kb;",
            "LX/AYq;",
            "LX/6Rg;",
            "Lcom/facebook/facecast/protocol/FacecastNetworker;",
            "LX/AVC;",
            "LX/6Re;",
            "LX/0hB;",
            "LX/0Ot",
            "<",
            "LX/AWK;",
            ">;",
            "LX/AVB;",
            "LX/0Zr;",
            "LX/Abh;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/8wR;",
            "LX/AVc;",
            "LX/1Nq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 280285
    iput-object p1, p0, Lcom/facebook/facecast/FacecastActivity;->s:LX/AV2;

    iput-object p2, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    iput-object p3, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    iput-object p4, p0, Lcom/facebook/facecast/FacecastActivity;->v:LX/0i4;

    iput-object p5, p0, Lcom/facebook/facecast/FacecastActivity;->w:LX/0SG;

    iput-object p6, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    iput-object p7, p0, Lcom/facebook/facecast/FacecastActivity;->y:LX/0kL;

    iput-object p8, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    iput-object p9, p0, Lcom/facebook/facecast/FacecastActivity;->A:LX/AV6;

    iput-object p10, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    iput-object p11, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    iput-object p12, p0, Lcom/facebook/facecast/FacecastActivity;->D:LX/1b3;

    iput-object p13, p0, Lcom/facebook/facecast/FacecastActivity;->E:LX/AVh;

    iput-object p14, p0, Lcom/facebook/facecast/FacecastActivity;->F:LX/Aag;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->G:LX/0ad;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->H:LX/BSb;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->I:LX/0kb;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->K:LX/6Rg;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->L:Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->M:LX/AVC;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->N:LX/6Re;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->O:LX/0hB;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Q:LX/AVB;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->R:LX/0Zr;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->S:LX/Abh;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->T:LX/0Or;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->V:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->W:LX/8wR;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->X:LX/AVc;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Y:LX/1Nq;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 36

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v35

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/facecast/FacecastActivity;

    const-class v3, LX/AV2;

    move-object/from16 v0, v35

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/AV2;

    invoke-static/range {v35 .. v35}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v4

    check-cast v4, LX/AVT;

    invoke-static/range {v35 .. v35}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    const-class v6, LX/0i4;

    move-object/from16 v0, v35

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/0i4;

    invoke-static/range {v35 .. v35}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static/range {v35 .. v35}, LX/AVF;->a(LX/0QB;)LX/AVF;

    move-result-object v8

    check-cast v8, LX/AVF;

    invoke-static/range {v35 .. v35}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static/range {v35 .. v35}, LX/AVM;->a(LX/0QB;)LX/7RY;

    move-result-object v10

    check-cast v10, LX/7RY;

    const-class v11, LX/AV6;

    move-object/from16 v0, v35

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/AV6;

    invoke-static/range {v35 .. v35}, LX/Ac6;->a(LX/0QB;)LX/Ac6;

    move-result-object v12

    check-cast v12, LX/Ac6;

    invoke-static/range {v35 .. v35}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v13

    check-cast v13, LX/1b4;

    invoke-static/range {v35 .. v35}, LX/1b3;->a(LX/0QB;)LX/1b3;

    move-result-object v14

    check-cast v14, LX/1b3;

    invoke-static/range {v35 .. v35}, LX/AVh;->a(LX/0QB;)LX/AVh;

    move-result-object v15

    check-cast v15, LX/AVh;

    invoke-static/range {v35 .. v35}, LX/Aag;->a(LX/0QB;)LX/Aag;

    move-result-object v16

    check-cast v16, LX/Aag;

    invoke-static/range {v35 .. v35}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    invoke-static/range {v35 .. v35}, LX/BSb;->a(LX/0QB;)LX/BSb;

    move-result-object v18

    check-cast v18, LX/BSb;

    invoke-static/range {v35 .. v35}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v19

    check-cast v19, LX/0kb;

    invoke-static/range {v35 .. v35}, LX/AYq;->a(LX/0QB;)LX/AYq;

    move-result-object v20

    check-cast v20, LX/AYq;

    invoke-static/range {v35 .. v35}, LX/6Rg;->a(LX/0QB;)LX/6Rg;

    move-result-object v21

    check-cast v21, LX/6Rg;

    invoke-static/range {v35 .. v35}, Lcom/facebook/facecast/protocol/FacecastNetworker;->a(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-result-object v22

    check-cast v22, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static/range {v35 .. v35}, LX/AVC;->a(LX/0QB;)LX/AVC;

    move-result-object v23

    check-cast v23, LX/AVC;

    invoke-static/range {v35 .. v35}, LX/6Re;->a(LX/0QB;)LX/6Re;

    move-result-object v24

    check-cast v24, LX/6Re;

    invoke-static/range {v35 .. v35}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v25

    check-cast v25, LX/0hB;

    const/16 v26, 0x1bda

    move-object/from16 v0, v35

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v26

    const-class v27, LX/AVB;

    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/AVB;

    invoke-static/range {v35 .. v35}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v28

    check-cast v28, LX/0Zr;

    invoke-static/range {v35 .. v35}, LX/Abh;->a(LX/0QB;)LX/Abh;

    move-result-object v29

    check-cast v29, LX/Abh;

    const/16 v30, 0x12cb

    move-object/from16 v0, v35

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {v35 .. v35}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->a(LX/0QB;)Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    move-result-object v31

    check-cast v31, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    invoke-static/range {v35 .. v35}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v32

    check-cast v32, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v35 .. v35}, LX/8wR;->a(LX/0QB;)LX/8wR;

    move-result-object v33

    check-cast v33, LX/8wR;

    invoke-static/range {v35 .. v35}, LX/AVc;->a(LX/0QB;)LX/AVc;

    move-result-object v34

    check-cast v34, LX/AVc;

    invoke-static/range {v35 .. v35}, LX/1Nq;->a(LX/0QB;)LX/1Nq;

    move-result-object v35

    check-cast v35, LX/1Nq;

    invoke-static/range {v2 .. v35}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/facecast/FacecastActivity;LX/AV2;LX/AVT;Landroid/os/Handler;LX/0i4;LX/0SG;LX/AVF;LX/0kL;LX/7RY;LX/AV6;LX/Ac6;LX/1b4;LX/1b3;LX/AVh;LX/Aag;LX/0ad;LX/BSb;LX/0kb;LX/AYq;LX/6Rg;Lcom/facebook/facecast/protocol/FacecastNetworker;LX/AVC;LX/6Re;LX/0hB;LX/0Ot;LX/AVB;LX/0Zr;LX/Abh;LX/0Or;Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;Lcom/facebook/content/SecureContextHelper;LX/8wR;LX/AVc;LX/1Nq;)V

    return-void
.end method

.method private a(ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 280267
    if-eqz p1, :cond_4

    .line 280268
    if-eqz p2, :cond_3

    .line 280269
    const v0, 0x7f0e038f

    move v0, v0

    .line 280270
    :goto_0
    move v1, v0

    .line 280271
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v0, :cond_0

    .line 280272
    const v0, 0x7f0e03ce

    .line 280273
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 280274
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 280275
    return-void

    .line 280276
    :cond_0
    if-eqz p1, :cond_2

    .line 280277
    if-eqz p2, :cond_1

    .line 280278
    const v0, 0x7f0e03d2

    goto :goto_1

    .line 280279
    :cond_1
    const v0, 0x7f0e03d1

    goto :goto_1

    .line 280280
    :cond_2
    const v0, 0x7f0e03d0

    goto :goto_1

    .line 280281
    :cond_3
    const v0, 0x7f0e038e

    move v0, v0

    .line 280282
    goto :goto_0

    .line 280283
    :cond_4
    const v0, 0x7f0e038d

    move v0, v0

    .line 280284
    goto :goto_0
.end method

.method private static a(FF)Z
    .locals 2

    .prologue
    .line 280315
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/facecast/model/FacecastCompositionData;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 280250
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280251
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->o:Ljava/lang/String;

    move-object v0, v0

    .line 280252
    if-eqz v0, :cond_0

    move v0, v1

    .line 280253
    :goto_0
    return v0

    .line 280254
    :cond_0
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v0, v0

    .line 280255
    if-eqz v0, :cond_3

    .line 280256
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v0, v0

    .line 280257
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 280258
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v0, v0

    .line 280259
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 280260
    :goto_1
    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->N:LX/6Re;

    iget-boolean v2, v2, LX/6Re;->a:Z

    iget-object v3, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-boolean v3, v3, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->allowPublicBroadcastsOnly:Z

    invoke-static {v0, v2, v3}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;ZZ)Z

    move-result v0

    .line 280261
    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-boolean v2, v2, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 280262
    :cond_1
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v0, v0

    .line 280263
    iget-object v2, v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v2

    .line 280264
    goto :goto_1

    :cond_2
    move v0, v1

    .line 280265
    goto :goto_0

    .line 280266
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-boolean v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 280249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "114000975315193"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private static a(Lcom/facebook/privacy/model/SelectablePrivacyData;ZZ)Z
    .locals 4
    .param p0    # Lcom/facebook/privacy/model/SelectablePrivacyData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 280235
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 280236
    :cond_0
    :goto_0
    return v0

    .line 280237
    :cond_1
    if-eqz p0, :cond_0

    .line 280238
    iget-object v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 280239
    if-eqz v2, :cond_0

    .line 280240
    iget-object v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 280241
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 280242
    iget-object v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 280243
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v2

    .line 280244
    if-eqz p1, :cond_3

    .line 280245
    invoke-static {v2}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 280246
    :cond_3
    if-eqz p2, :cond_4

    .line 280247
    invoke-static {v2}, Lcom/facebook/facecast/FacecastActivity;->c(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 280248
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/facecast/FacecastActivity;LX/AUv;)V
    .locals 3

    .prologue
    .line 280212
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280213
    :cond_0
    :goto_0
    return-void

    .line 280214
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ao:LX/2EJ;

    if-nez v0, :cond_0

    .line 280215
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/AUu;

    invoke-direct {v1, p0}, LX/AUu;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v1

    .line 280216
    sget-object v0, LX/AUv;->INTERNET:LX/AUv;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ag:Lcom/facebook/http/protocol/ApiErrorResult;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ag:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 280217
    iget-object v2, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    move-object v0, v2

    .line 280218
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 280219
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ag:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 280220
    iget-object v2, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    move-object v0, v2

    .line 280221
    invoke-virtual {v1, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->ag:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 280222
    iget-object v2, v1, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserMessage:Ljava/lang/String;

    move-object v1, v2

    .line 280223
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ao:LX/2EJ;

    goto :goto_0

    .line 280224
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 280225
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v0, :cond_3

    const v0, 0x7f080ca1

    :goto_1
    invoke-virtual {v1, v0}, LX/0ju;->a(I)LX/0ju;

    .line 280226
    :goto_2
    sget-object v0, LX/AUq;->c:[I

    invoke-virtual {p1}, LX/AUv;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 280227
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f080ca5

    .line 280228
    :goto_3
    invoke-virtual {v1, v0}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ao:LX/2EJ;

    goto :goto_0

    .line 280229
    :cond_3
    const v0, 0x7f080ca0

    goto :goto_1

    .line 280230
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v0, :cond_5

    const v0, 0x7f080c9f

    :goto_4
    invoke-virtual {v1, v0}, LX/0ju;->a(I)LX/0ju;

    goto :goto_2

    :cond_5
    const v0, 0x7f080c9e

    goto :goto_4

    .line 280231
    :pswitch_0
    const v0, 0x7f080ca2

    goto :goto_3

    .line 280232
    :pswitch_1
    const v0, 0x7f080ca3

    goto :goto_3

    .line 280233
    :pswitch_2
    const v0, 0x7f08119f

    goto :goto_3

    .line 280234
    :cond_6
    const v0, 0x7f080ca4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Lcom/facebook/facecast/FacecastActivity;I)V
    .locals 4

    .prologue
    .line 280206
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->p:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    if-eqz v0, :cond_0

    .line 280207
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 280208
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->p:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v1}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->d()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    const-class v2, LX/1kW;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->W()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/BMQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Ljava/lang/Boolean;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 280209
    invoke-virtual {p0, p1, v0}, Lcom/facebook/facecast/FacecastActivity;->setResult(ILandroid/content/Intent;)V

    .line 280210
    :goto_0
    return-void

    .line 280211
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/facecast/FacecastActivity;->setResult(I)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V
    .locals 1

    .prologue
    .line 280203
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 280204
    invoke-virtual {p1}, LX/AWT;->d()V

    .line 280205
    return-void
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)Z
    .locals 2

    .prologue
    .line 280202
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)Z
    .locals 2

    .prologue
    .line 280201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
    .locals 1

    .prologue
    .line 280200
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aI:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ae:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 280195
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 280196
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 280197
    sget-object v1, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    if-ne v0, v1, :cond_2

    .line 280198
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->e()V

    .line 280199
    :cond_2
    return-void
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 280189
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "new volume: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 280190
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    .line 280191
    iget-boolean v1, v0, LX/AWT;->a:Z

    move v0, v1

    .line 280192
    if-eqz v0, :cond_0

    .line 280193
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecast/FacecastActivity$7;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/facecast/FacecastActivity$7;-><init>(Lcom/facebook/facecast/FacecastActivity;D)V

    const v2, 0x2e934c0e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 280194
    :cond_0
    return-void
.end method

.method public final a(IJ)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x3
    .end annotation

    .prologue
    .line 280074
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 280075
    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    iget-wide v0, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->clientRenderingDurationMs:J

    .line 280076
    :goto_0
    iget-object v4, v2, LX/AY1;->l:LX/AVP;

    .line 280077
    iget-object v6, v4, LX/AVP;->b:LX/AVN;

    iget-object v7, v4, LX/AVP;->b:LX/AVN;

    const/4 v8, 0x5

    long-to-int v9, v0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, p1, v9, v10}, LX/AVN;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 280078
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aL:LX/0Ab;

    if-eqz v0, :cond_1

    .line 280079
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aL:LX/0Ab;

    .line 280080
    iget-object v4, v0, LX/0Ab;->a:LX/0Ad;

    iget-wide v6, v0, LX/0Ab;->f:J

    invoke-virtual {v4, v6, v7, p2, p3}, LX/0Ad;->a(JJ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 280081
    iput-wide p2, v0, LX/0Ab;->f:J

    .line 280082
    const-string v5, "live_video_frame_captured"

    const-wide/16 v8, 0x0

    move-object v4, v0

    move-wide v6, p2

    invoke-static/range {v4 .. v9}, LX/0Ab;->a(LX/0Ab;Ljava/lang/String;JJ)V

    .line 280083
    :cond_1
    return-void

    .line 280084
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)V
    .locals 13

    .prologue
    .line 280146
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 280147
    iput-wide p1, p0, Lcom/facebook/facecast/FacecastActivity;->aj:J

    .line 280148
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    if-eqz v0, :cond_3

    .line 280149
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    .line 280150
    iput-wide p1, v0, LX/AYE;->G:J

    .line 280151
    iget-object v1, v0, LX/AYE;->n:LX/AXm;

    .line 280152
    iget-object v2, v1, LX/AXm;->h:Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    invoke-virtual {v2, p1, p2}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(J)V

    .line 280153
    iget-object v2, v1, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v2, p1, p2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setTimeElapsed(J)V

    .line 280154
    iget-object v1, v0, LX/AYE;->p:LX/AYv;

    if-eqz v1, :cond_2

    .line 280155
    iget-object v1, v0, LX/AYE;->h:LX/AYs;

    .line 280156
    iput-wide p1, v1, LX/AYs;->d:J

    .line 280157
    iget-object v1, v0, LX/AYE;->i:LX/AYr;

    .line 280158
    iput-wide p1, v1, LX/AYr;->d:J

    .line 280159
    iget-object v1, v0, LX/AYE;->p:LX/AYv;

    iget-wide v3, v0, LX/AYE;->G:J

    iget-wide v5, v0, LX/AYE;->H:J

    invoke-virtual {v1, v3, v4, v5, v6}, LX/AYv;->a(JJ)LX/AZC;

    move-result-object v1

    .line 280160
    sget-object v2, LX/AZC;->ELIGIBLE:LX/AZC;

    if-ne v1, v2, :cond_2

    .line 280161
    iget-object v1, v0, LX/AYE;->c:LX/AYq;

    .line 280162
    iget-object v2, v1, LX/AYq;->b:LX/AYp;

    move-object v1, v2

    .line 280163
    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    if-ne v1, v2, :cond_2

    .line 280164
    :cond_0
    iget-object v1, v0, LX/AYE;->c:LX/AYq;

    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

    invoke-virtual {v1, v2}, LX/AYq;->a(LX/AYp;)V

    .line 280165
    iget-object v1, v0, LX/AYE;->k:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->W()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/AYE;->x:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-static {v1}, LX/AZN;->a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280166
    iget-object v1, v0, LX/AYE;->m:LX/AWl;

    iget-object v1, v1, LX/AWl;->f:LX/Aby;

    iget-object v2, v0, LX/AYE;->x:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 280167
    iget-object v3, v1, LX/Aby;->a:LX/AcX;

    iget v4, v1, LX/Aby;->O:I

    .line 280168
    iget-object v5, v3, LX/AcX;->n:LX/AeS;

    new-instance v6, LX/AfR;

    sget-object v1, LX/AfQ;->LIVE_COMMERCIAL_BREAK_ELIGIBLE:LX/AfQ;

    invoke-direct {v6, v1, v2, v4}, LX/AfR;-><init>(LX/AfQ;Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;I)V

    invoke-virtual {v5, v6}, LX/AeS;->a(LX/AeO;)V

    .line 280169
    :cond_1
    iget-object v1, v0, LX/AYE;->h:LX/AYs;

    .line 280170
    iget-object v2, v1, LX/AYs;->a:LX/0if;

    sget-object v3, LX/0ig;->C:LX/0ih;

    const-string v4, "commercial_break_eligible"

    const/4 v5, 0x0

    invoke-static {v1}, LX/AYs;->i(LX/AYs;)LX/1rQ;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 280171
    iget-object v1, v0, LX/AYE;->i:LX/AYr;

    iget-object v2, v0, LX/AYE;->n:LX/AXm;

    invoke-virtual {v2}, LX/AXm;->getCurrentViewerCount()I

    move-result v2

    .line 280172
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "commercial_break_broadcaster_eligible"

    invoke-direct {v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v9, "commercial_break_broadcaster"

    .line 280173
    iput-object v9, v8, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 280174
    move-object v8, v8

    .line 280175
    const-string v9, "broadcaster_id"

    iget-object v10, v1, LX/AYr;->b:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "video_id"

    iget-object v10, v1, LX/AYr;->c:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "time_offset_ms"

    iget-wide v10, v1, LX/AYr;->d:J

    invoke-virtual {v8, v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "concurrent_viewer_count"

    invoke-virtual {v8, v9, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 280176
    iget-object v9, v1, LX/AYr;->a:LX/0Zb;

    invoke-interface {v9, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 280177
    :cond_2
    iget-object v1, v0, LX/AYE;->m:LX/AWl;

    .line 280178
    iget-object v8, v1, LX/AWl;->f:LX/Aby;

    const-wide/16 v10, 0x3e8

    div-long v10, p1, v10

    long-to-float v9, v10

    invoke-virtual {v8, v9}, LX/Aby;->a(F)V

    .line 280179
    iget-object v8, v1, LX/AWl;->m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-virtual {v8, p1, p2}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->setTimeElapsed(J)V

    .line 280180
    :cond_3
    return-void
.end method

.method public final a(LX/AV3;)V
    .locals 2

    .prologue
    .line 279157
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279158
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 279159
    sget-object v1, LX/AVE;->RECORDING:LX/AVE;

    if-ne v0, v1, :cond_0

    .line 279160
    iput-object p1, p0, Lcom/facebook/facecast/FacecastActivity;->ar:LX/AV3;

    .line 279161
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->COPYRIGHT_VIOLATION:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 279162
    :cond_0
    return-void
.end method

.method public final a(LX/AVE;LX/AVE;)V
    .locals 18

    .prologue
    .line 279525
    sget-object v2, LX/AUq;->b:[I

    invoke-virtual/range {p2 .. p2}, LX/AVE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 279526
    :cond_0
    :goto_0
    sget-object v2, LX/AUq;->b:[I

    invoke-virtual/range {p1 .. p1}, LX/AVE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 279527
    :cond_1
    :goto_1
    return-void

    .line 279528
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279529
    sget-object v2, LX/AVE;->RECORDING:LX/AVE;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 279530
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    invoke-virtual {v2, v3}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->b(Lcom/facebook/facecast/model/FacecastCompositionData;)Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/facecast/model/FacecastCompositionData;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/facecast/FacecastActivity;->aw:Z

    .line 279532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/facecast/FacecastActivity;->aw:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a(Lcom/facebook/facecast/model/FacecastCompositionData;Ljava/lang/String;ZZ)Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279533
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-virtual {v2}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->getFundraiserModel()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aD:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 279534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-virtual {v2}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/facecast/FacecastActivity;->aE:Z

    .line 279535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-eqz v2, :cond_2

    .line 279536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v3}, LX/AVA;->d()I

    move-result v3

    invoke-static {v3}, LX/AVA;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AVT;->d(Ljava/lang/String;)V

    .line 279537
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    .line 279538
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 279539
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aa:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    .line 279540
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 279541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-virtual {v2}, LX/AYE;->g()V

    .line 279543
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    if-eqz v2, :cond_0

    .line 279544
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    invoke-virtual {v2}, LX/AV5;->b()V

    goto/16 :goto_0

    .line 279545
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aS:LX/AXg;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279546
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aS:LX/AXg;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    goto/16 :goto_0

    .line 279547
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aZ:LX/AXa;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279548
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aZ:LX/AXa;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    goto/16 :goto_0

    .line 279549
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aV:LX/AXt;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aV:LX/AXt;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    goto/16 :goto_0

    .line 279551
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    goto/16 :goto_0

    .line 279553
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aX:LX/AY2;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279554
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aX:LX/AY2;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    goto/16 :goto_0

    .line 279555
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 279556
    new-instance v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 279557
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 279558
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    const-string v5, "extra_composition_editing"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/facecast/FacecastActivity;->au:Z

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->F()Z

    move-result v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/facebook/facecast/FacecastActivity;->aH:Z

    move-object/from16 v8, p0

    invoke-virtual/range {v2 .. v8}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZZZLX/1bE;)V

    .line 279559
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    goto/16 :goto_1

    .line 279560
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 279561
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279562
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 279563
    new-instance v3, LX/7RX;

    invoke-direct {v3}, LX/7RX;-><init>()V

    .line 279564
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/facecast/FacecastActivity;->aI:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->d()LX/7Rc;

    move-result-object v2

    sget-object v4, LX/7Rc;->STREAMING_OFF:LX/7Rc;

    if-ne v2, v4, :cond_4

    .line 279565
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->h()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 279566
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/AVT;->f(Ljava/lang/String;)V

    .line 279567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/AVT;->a(Ljava/lang/String;)V

    .line 279568
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    invoke-virtual {v3, v2}, LX/7RX;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;)V

    .line 279569
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/facecast/FacecastActivity;->aG:Z

    .line 279570
    :cond_4
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->d()F

    move-result v2

    invoke-virtual {v3, v2}, LX/7RX;->a(F)V

    .line 279571
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2, v3}, LX/7RY;->a(LX/7RX;)V

    .line 279572
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->m()LX/0Ab;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aL:LX/0Ab;

    .line 279573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/AV1;->a(Z)V

    .line 279574
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    if-nez v2, :cond_6

    .line 279575
    new-instance v2, LX/AYE;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/AYE;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    .line 279576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v4

    iget-wide v4, v4, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->minBroadcastDurationSeconds:J

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v6

    iget-wide v6, v6, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->maxBroadcastDurationSeconds:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    invoke-virtual {v8}, Lcom/facebook/facecast/model/FacecastCompositionData;->c()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->E()Z

    move-result v11

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/facebook/facecast/FacecastActivity;->aH:Z

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->d()F

    move-result v14

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/facebook/facecast/FacecastActivity;->aw:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/facecast/FacecastActivity;->aD:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/AYE;->a(Ljava/lang/String;JJLcom/facebook/ipc/composer/intent/ComposerTargetData;LX/AY1;LX/AV1;ZZZFZLcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 279577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->H:LX/BSb;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/BSb;->a(Ljava/lang/String;)V

    .line 279578
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279579
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-nez v2, :cond_5

    .line 279580
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    .line 279581
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279582
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v2, :cond_6

    .line 279583
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    .line 279584
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-virtual {v2}, LX/AYE;->h()V

    .line 279585
    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->Q()V

    .line 279586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ad:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 279587
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ad:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 279588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ae:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-eqz v2, :cond_7

    .line 279589
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->W:LX/8wR;

    new-instance v3, LX/8wO;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecast/FacecastActivity;->ae:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-direct {v3, v4}, LX/8wO;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 279590
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->E:LX/AVh;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/AVh;->c(Ljava/lang/String;)V

    .line 279591
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->E:LX/AVh;

    invoke-virtual {v2}, LX/AVh;->b()V

    goto/16 :goto_1

    .line 279592
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v2, v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    .line 279593
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v5}, LX/7RY;->i()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, LX/AVT;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_3

    .line 279594
    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    .line 279595
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->c()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 279596
    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->L()V

    .line 279597
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aS:LX/AXg;

    if-nez v2, :cond_b

    .line 279598
    new-instance v2, LX/AXg;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/AXg;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aS:LX/AXg;

    .line 279599
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aS:LX/AXg;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279600
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ad:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 279601
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->ad:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    goto/16 :goto_1

    .line 279602
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->c()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 279603
    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->L()V

    .line 279604
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aZ:LX/AXa;

    if-nez v2, :cond_d

    .line 279605
    new-instance v2, LX/AXa;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/AXa;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aZ:LX/AXa;

    .line 279606
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aZ:LX/AXa;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279607
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->r()V

    .line 279608
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    if-eqz v2, :cond_1

    .line 279609
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    invoke-virtual {v2}, LX/AV5;->c()V

    goto/16 :goto_1

    .line 279610
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->S:LX/Abh;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/facecast/FacecastActivity;->aj:J

    invoke-virtual {v2, v3, v4, v5}, LX/Abh;->a(Ljava/lang/String;J)V

    .line 279611
    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->G()V

    goto/16 :goto_1

    .line 279612
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aV:LX/AXt;

    if-nez v2, :cond_e

    .line 279613
    new-instance v2, LX/AXt;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/AXt;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aV:LX/AXt;

    .line 279614
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aV:LX/AXt;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    goto/16 :goto_1

    .line 279615
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->c()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 279616
    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->L()V

    .line 279617
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    if-nez v2, :cond_10

    .line 279618
    new-instance v2, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    .line 279619
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/facecast/FacecastActivity;->ar:LX/AV3;

    invoke-virtual {v2, v3}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->setViolationText(LX/AV3;)V

    .line 279620
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->setBroadcastId(Ljava/lang/String;)V

    .line 279621
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    if-eqz v2, :cond_11

    .line 279622
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    invoke-virtual {v2}, LX/AV5;->c()V

    .line 279623
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->as:LX/AV5;

    .line 279624
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aY:Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279625
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    if-eqz v2, :cond_1

    .line 279626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-virtual {v2}, LX/AYE;->k()V

    goto/16 :goto_1

    .line 279627
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aX:LX/AY2;

    if-nez v2, :cond_12

    .line 279628
    new-instance v2, LX/AY2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/AY2;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aX:LX/AY2;

    .line 279629
    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->L()V

    .line 279630
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->aX:LX/AY2;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    goto/16 :goto_1

    .line 279631
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v2}, LX/7RY;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 279632
    invoke-direct/range {p0 .. p0}, Lcom/facebook/facecast/FacecastActivity;->L()V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_b
        :pswitch_f
    .end packed-switch
.end method

.method public final a(LX/AYp;LX/AYp;)V
    .locals 9

    .prologue
    .line 279493
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279494
    sget-object v0, LX/AUq;->a:[I

    invoke-virtual {p2}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 279495
    :cond_0
    :goto_0
    sget-object v0, LX/AUq;->a:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 279496
    :cond_1
    :goto_1
    return-void

    .line 279497
    :pswitch_0
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

    if-eq p1, v0, :cond_0

    .line 279498
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->L()V

    goto :goto_0

    .line 279499
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->H:LX/BSb;

    iget-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->aj:J

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->commercialBreakLengthMs:I

    int-to-long v4, v0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    .line 279500
    iget v6, v0, LX/AYq;->e:I

    move v6, v6

    .line 279501
    iget-object v0, v1, LX/BSb;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 279502
    :goto_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aa:Ljava/util/List;

    iget-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->aj:J

    long-to-float v1, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279503
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->T()V

    .line 279504
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v0, :cond_1

    .line 279505
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    invoke-virtual {v0}, LX/AYK;->h()V

    goto :goto_1

    .line 279506
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279507
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 279508
    sget-object v1, LX/AVE;->INTERRUPTED:LX/AVE;

    if-eq v0, v1, :cond_2

    .line 279509
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->N()V

    .line 279510
    new-instance v0, LX/7RX;

    invoke-direct {v0}, LX/7RX;-><init>()V

    .line 279511
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->d()F

    move-result v1

    .line 279512
    iput v1, v0, LX/7RX;->a:F

    .line 279513
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v1, v0}, LX/7RY;->a(LX/7RX;)V

    .line 279514
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    const/4 v1, 0x1

    .line 279515
    iput-boolean v1, v0, LX/AV1;->s:Z

    .line 279516
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v0, :cond_1

    .line 279517
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    invoke-virtual {v0}, LX/AYK;->g()V

    goto :goto_1

    .line 279518
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v1, LX/BSb;->d:LX/2ml;

    invoke-virtual {v7}, LX/2ml;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v7, v1, LX/BSb;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279519
    iget-object v7, v1, LX/BSb;->c:LX/0lC;

    invoke-virtual {v7}, LX/0lC;->e()LX/0m9;

    move-result-object v7

    .line 279520
    const-string v8, "type"

    sget-object p1, LX/BSZ;->START:LX/BSZ;

    invoke-virtual {p1}, LX/BSZ;->name()Ljava/lang/String;

    move-result-object p1

    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, v8, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 279521
    const-string v8, "commercial_break_start_time_ms"

    invoke-virtual {v7, v8, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 279522
    const-string v8, "commercial_break_length_ms"

    invoke-virtual {v7, v8, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 279523
    const-string v8, "index"

    invoke-virtual {v7, v8, v6}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 279524
    invoke-static {v1, v0, v7}, LX/BSb;->a(LX/BSb;Ljava/lang/String;LX/0m9;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 279486
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 279487
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 279488
    :cond_0
    if-eqz p1, :cond_1

    .line 279489
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, LX/AV1;->a(ILjava/lang/Object;)V

    .line 279490
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->ak:Z

    .line 279491
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecast/FacecastActivity$5;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/FacecastActivity$5;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    const v2, 0x585531f2

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 279492
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/facecast/FacecastPreviewView;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 279471
    invoke-virtual {p1}, Lcom/facebook/facecast/FacecastPreviewView;->getWidth()I

    move-result v0

    .line 279472
    invoke-virtual {p1}, Lcom/facebook/facecast/FacecastPreviewView;->getHeight()I

    move-result v1

    .line 279473
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 279474
    :cond_0
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/facebook/facecast/FacecastActivity;->av:F

    .line 279475
    :cond_1
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/facecast/FacecastActivity;->av:F

    .line 279476
    iget v0, p0, Lcom/facebook/facecast/FacecastActivity;->av:F

    invoke-static {v0, v3}, Lcom/facebook/facecast/FacecastActivity;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 279477
    iput v3, p0, Lcom/facebook/facecast/FacecastActivity;->av:F

    .line 279478
    :cond_2
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->E()Z

    move-result v0

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->F()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/FacecastActivity;->a(ZZ)V

    .line 279479
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    if-eqz v0, :cond_3

    .line 279480
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->E()Z

    move-result v1

    iget-boolean v2, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a(ZZ)V

    .line 279481
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AY1;

    .line 279482
    iget-object v1, v0, LX/AY1;->l:LX/AVP;

    .line 279483
    invoke-static {v1}, LX/AVP;->s(LX/AVP;)LX/1bI;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 279484
    iget-object v2, v1, LX/AVP;->b:LX/AVN;

    iget-object v3, v1, LX/AVP;->b:LX/AVN;

    const/16 p0, 0x10

    invoke-static {v1}, LX/AVP;->s(LX/AVP;)LX/1bI;

    move-result-object v0

    invoke-interface {v0}, LX/1bI;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v3, p0, v0}, LX/AVN;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 279485
    :cond_4
    return-void
.end method

.method public final a(Lcom/facebook/http/protocol/ApiErrorResult;Z)V
    .locals 3
    .param p1    # Lcom/facebook/http/protocol/ApiErrorResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 279462
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aI:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 279463
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    .line 279464
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 279465
    const-string v2, "facecast_event_name"

    const-string p0, "fetch_audio_live_broadcast_id_failed"

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279466
    invoke-virtual {v0, v1}, LX/AVT;->a(Ljava/util/Map;)V

    .line 279467
    :goto_0
    return-void

    .line 279468
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecast/FacecastActivity;->ag:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 279469
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->FAILED:LX/AVE;

    const-string v2, "fetch_broadcast_id_failed"

    invoke-virtual {v0, v1, v2}, LX/AVF;->a(LX/AVE;Ljava/lang/String;)V

    .line 279470
    sget-object v0, LX/AUv;->INTERNET:LX/AUv;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a$redex0(Lcom/facebook/facecast/FacecastActivity;LX/AUv;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;FF)V
    .locals 10

    .prologue
    .line 279435
    if-eqz p1, :cond_0

    .line 279436
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v7, 0x3f100000    # 0.5625f

    const/high16 v6, -0x40800000    # -1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 279437
    iput-object p1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->i:Lcom/facebook/ipc/media/MediaItem;

    .line 279438
    iget-object v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->i:Lcom/facebook/ipc/media/MediaItem;

    .line 279439
    iget-object v2, v1, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v1, v2

    .line 279440
    invoke-virtual {v1}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 279441
    iget v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    move v2, v2

    .line 279442
    if-nez v2, :cond_1

    .line 279443
    iget v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v2, v2

    .line 279444
    iget v3, v1, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v1, v3

    .line 279445
    :goto_0
    int-to-float v3, v2

    mul-float/2addr v3, v7

    int-to-float v4, v1

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 279446
    int-to-float v2, v2

    mul-float/2addr v2, v7

    .line 279447
    int-to-float v1, v1

    div-float v1, v2, v1

    div-float/2addr v1, v8

    .line 279448
    div-float v2, p3, v1

    .line 279449
    sub-float v3, p3, v5

    div-float v1, v3, v1

    .line 279450
    const/16 v3, 0x8

    new-array v3, v3, [F

    aput v6, v3, v9

    const/4 v4, 0x1

    aput v1, v3, v4

    const/4 v4, 0x2

    aput v5, v3, v4

    const/4 v4, 0x3

    aput v1, v3, v4

    const/4 v1, 0x4

    aput v6, v3, v1

    const/4 v1, 0x5

    aput v2, v3, v1

    const/4 v1, 0x6

    aput v5, v3, v1

    const/4 v1, 0x7

    aput v2, v3, v1

    iput-object v3, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->j:[F

    .line 279451
    :goto_1
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWK;

    invoke-virtual {v1, v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->a(LX/AWK;)Z

    .line 279452
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bc:LX/AYk;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    .line 279453
    return-void

    .line 279454
    :cond_1
    iget v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v2, v2

    .line 279455
    iget v3, v1, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v1, v3

    .line 279456
    goto :goto_0

    .line 279457
    :cond_2
    int-to-float v1, v1

    div-float/2addr v1, v7

    .line 279458
    int-to-float v2, v2

    div-float/2addr v1, v2

    div-float/2addr v1, v8

    .line 279459
    neg-float v2, p2

    div-float/2addr v2, v1

    .line 279460
    sub-float v3, v5, p2

    div-float v1, v3, v1

    .line 279461
    const/16 v3, 0x8

    new-array v3, v3, [F

    aput v2, v3, v9

    const/4 v4, 0x1

    aput v6, v3, v4

    const/4 v4, 0x2

    aput v1, v3, v4

    const/4 v4, 0x3

    aput v6, v3, v4

    const/4 v4, 0x4

    aput v2, v3, v4

    const/4 v2, 0x5

    aput v5, v3, v2

    const/4 v2, 0x6

    aput v1, v3, v2

    const/4 v1, 0x7

    aput v5, v3, v1

    iput-object v3, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->j:[F

    goto :goto_1
.end method

.method public final a(Lcom/facebook/video/videostreaming/LiveStreamingError;)V
    .locals 3

    .prologue
    .line 279429
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    const-string v1, "audio_recording_failed"

    .line 279430
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 279431
    const-string p0, "facecast_event_name"

    invoke-interface {v2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279432
    invoke-static {p1, v2}, LX/AVT;->a(Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 279433
    invoke-virtual {v0, v2}, LX/AVT;->a(Ljava/util/Map;)V

    .line 279434
    return-void
.end method

.method public final a(Lcom/facebook/video/videostreaming/NetworkSpeedTest;)V
    .locals 3

    .prologue
    .line 279420
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->P()V

    .line 279421
    iget-object v0, p1, Lcom/facebook/video/videostreaming/NetworkSpeedTest;->state:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    sget-object v1, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->Succeeded:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p1, Lcom/facebook/video/videostreaming/NetworkSpeedTest;->speedTestPassesThreshold:Z

    if-eqz v0, :cond_1

    .line 279422
    invoke-static {p0}, Lcom/facebook/facecast/FacecastActivity;->O(Lcom/facebook/facecast/FacecastActivity;)V

    .line 279423
    :cond_0
    :goto_0
    return-void

    .line 279424
    :cond_1
    iget-object v0, p1, Lcom/facebook/video/videostreaming/NetworkSpeedTest;->state:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    sget-object v1, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->Canceled:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    if-eq v0, v1, :cond_0

    .line 279425
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279426
    iget-object v2, v1, LX/AVF;->b:LX/AVE;

    move-object v1, v2

    .line 279427
    iget-object v1, v1, LX/AVE;->loggerName:Ljava/lang/String;

    const-string v2, "speed_test_failed"

    invoke-virtual {v0, v1, v2}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 279428
    sget-object v0, LX/AUv;->SPEEDTEST:LX/AUv;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a$redex0(Lcom/facebook/facecast/FacecastActivity;LX/AUv;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;Z)V
    .locals 6

    .prologue
    .line 279399
    if-nez p1, :cond_1

    .line 279400
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/http/protocol/ApiErrorResult;Z)V

    .line 279401
    :cond_0
    :goto_0
    return-void

    .line 279402
    :cond_1
    if-eqz p2, :cond_3

    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aI:Z

    if-eqz v0, :cond_3

    .line 279403
    iput-object p1, p0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    .line 279404
    :goto_1
    if-nez p2, :cond_0

    .line 279405
    new-instance v0, Lcom/facebook/facecast/FacecastActivity$6;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/FacecastActivity$6;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->an:Ljava/lang/Runnable;

    .line 279406
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->an:Ljava/lang/Runnable;

    iget-wide v2, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->speedTestTimeoutSeconds:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    const v4, 0x4e1e5b5e    # 6.6419699E8f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 279407
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    iget-object v1, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    .line 279408
    iput-object v1, v0, LX/AVT;->f:Ljava/lang/String;

    .line 279409
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->al:Z

    .line 279410
    iget-object v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->commercialBreakSettings:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 279411
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    if-eqz v0, :cond_2

    .line 279412
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->ax:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 279413
    iput-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->v:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 279414
    :cond_2
    invoke-static {p0}, Lcom/facebook/facecast/FacecastActivity;->O(Lcom/facebook/facecast/FacecastActivity;)V

    .line 279415
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->E:LX/AVh;

    iget-object v1, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    .line 279416
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "broadcast_id"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 279417
    const-string v3, "broadcast_ready"

    invoke-static {v0, v3, v2}, LX/AVh;->a(LX/AVh;Ljava/lang/String;LX/1rQ;)V

    .line 279418
    goto :goto_0

    .line 279419
    :cond_3
    iput-object p1, p0, Lcom/facebook/facecast/FacecastActivity;->ae:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 279396
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->FAILED:LX/AVE;

    const-string v2, "Preview error"

    new-instance v3, Lcom/facebook/video/videostreaming/LiveStreamingError;

    invoke-direct {v3, p1}, Lcom/facebook/video/videostreaming/LiveStreamingError;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v0, v1, v2, v3}, LX/AVF;->a(LX/AVE;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;)V

    .line 279397
    sget-object v0, LX/AUv;->PREVIEW_ERROR:LX/AUv;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a$redex0(Lcom/facebook/facecast/FacecastActivity;LX/AUv;)V

    .line 279398
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 279393
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->ak:Z

    .line 279394
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->b()V

    .line 279395
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 279277
    const v0, 0x7f03059f

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->setContentView(I)V

    .line 279278
    invoke-static {p0, p0}, Lcom/facebook/facecast/FacecastActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 279279
    iput-boolean v6, p0, Lcom/facebook/facecast/FacecastActivity;->aA:Z

    .line 279280
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_is_posting_as_page"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aH:Z

    .line 279281
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, LX/AWM;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aM:Z

    .line 279282
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aM:Z

    if-nez v0, :cond_0

    .line 279283
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->E:LX/AVh;

    invoke-virtual {v0}, LX/AVh;->a()V

    .line 279284
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->E:LX/AVh;

    const-string v1, "launch_live_composer"

    invoke-virtual {v0, v1}, LX/AVh;->a(Ljava/lang/String;)V

    .line 279285
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    .line 279286
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v3, 0x34b

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 279287
    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aI:Z

    .line 279288
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ah:Landroid/view/Window;

    .line 279289
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 279290
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->setRequestedOrientation(I)V

    .line 279291
    new-instance v0, LX/AVA;

    invoke-direct {v0, p0}, LX/AVA;-><init>(Landroid/app/Activity;)V

    .line 279292
    move-object v0, v0

    .line 279293
    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    .line 279294
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ah:Landroid/view/Window;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 279295
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->au:Z

    .line 279296
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->au:Z

    invoke-direct {p0, v0, v5}, Lcom/facebook/facecast/FacecastActivity;->a(ZZ)V

    .line 279297
    const v0, 0x7f0d0e99

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ad:Landroid/view/ViewGroup;

    .line 279298
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_composition_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/model/FacecastCompositionData;

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279299
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    if-nez v0, :cond_b

    .line 279300
    const-string v0, "No composition data"

    .line 279301
    new-instance v1, Lcom/facebook/facecast/model/FacecastCompositionData;

    new-instance v3, LX/AWP;

    invoke-direct {v3}, LX/AWP;-><init>()V

    invoke-direct {v1, v3}, Lcom/facebook/facecast/model/FacecastCompositionData;-><init>(LX/AWP;)V

    iput-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    move-object v1, v0

    .line 279302
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "extra_prompt_plugin_config"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    .line 279303
    if-eqz v0, :cond_9

    .line 279304
    iget-object v3, p0, Lcom/facebook/facecast/FacecastActivity;->Y:LX/1Nq;

    const-class v4, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v3, v0, v4}, LX/1Nq;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Ljava/lang/Class;)LX/88f;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->p:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    .line 279305
    :goto_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279306
    iget-object v3, v0, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v3, v3

    .line 279307
    if-eqz v3, :cond_a

    .line 279308
    iget-object v0, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 279309
    if-nez v0, :cond_1

    .line 279310
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    .line 279311
    :cond_1
    sget-object v4, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq v0, v4, :cond_2

    iget-wide v2, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 279312
    :cond_2
    :goto_3
    iget-object v3, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    .line 279313
    if-nez v0, :cond_c

    const-string v4, "null"

    :goto_4
    iput-object v4, v3, LX/AVT;->h:Ljava/lang/String;

    .line 279314
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, v2}, LX/7RY;->a(Ljava/lang/String;)V

    .line 279315
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, v5}, LX/7RY;->a(Z)V

    .line 279316
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    iget-boolean v3, p0, Lcom/facebook/facecast/FacecastActivity;->aH:Z

    invoke-virtual {v2, v3}, LX/1b4;->a(Z)Z

    move-result v2

    invoke-interface {v0, v2}, LX/7RY;->b(Z)V

    .line 279317
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->s:LX/AV2;

    const-string v2, "Facecast"

    invoke-virtual {v0, v6, v2}, LX/AV2;->a(ILjava/lang/String;)LX/AV1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    .line 279318
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    invoke-virtual {v0, p0}, LX/AV1;->a(LX/1b6;)V

    .line 279319
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 279320
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->R:LX/0Zr;

    const-string v2, "live_stream_renderer_thread"

    invoke-virtual {v0, v2}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ab:Landroid/os/HandlerThread;

    .line 279321
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ab:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 279322
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->ae()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 279323
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->R:LX/0Zr;

    const-string v2, "live_stream_camera_thread"

    invoke-virtual {v0, v2}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ac:Landroid/os/HandlerThread;

    .line 279324
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ac:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 279325
    :cond_4
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->J()Landroid/os/Looper;

    move-result-object v2

    .line 279326
    new-instance v3, LX/AUz;

    invoke-direct {v3, v0, v2}, LX/AUz;-><init>(LX/AV1;Landroid/os/Looper;)V

    iput-object v3, v0, LX/AV1;->f:Landroid/os/Handler;

    .line 279327
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    invoke-virtual {v0, p0}, LX/AVF;->a(LX/1b9;)V

    .line 279328
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v2, LX/AVE;->STARTING:LX/AVE;

    .line 279329
    sget-object v3, LX/AVE;->UNINITIALIZED:LX/AVE;

    iput-object v3, v0, LX/AVF;->b:LX/AVE;

    .line 279330
    sget-object v3, LX/AVE;->UNINITIALIZED:LX/AVE;

    iput-object v3, v0, LX/AVF;->c:LX/AVE;

    .line 279331
    invoke-virtual {v0, v2}, LX/AVF;->b(LX/AVE;)V

    .line 279332
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    if-le v0, v6, :cond_5

    .line 279333
    new-instance v0, LX/AYK;

    invoke-direct {v0, p0}, LX/AYK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    .line 279334
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    sget-object v2, LX/AYJ;->TOP_RIGHT:LX/AYJ;

    const/16 p1, 0x14

    const/16 v9, 0xc

    const/16 v8, 0xb

    const/16 v7, 0x9

    const/4 v6, 0x3

    .line 279335
    iget-object v3, v0, LX/AYK;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3}, Lcom/facebook/fbui/glyph/GlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 279336
    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 279337
    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 279338
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 279339
    invoke-virtual {v3, p1}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 279340
    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 279341
    const/16 v4, 0x15

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 279342
    sget-object v4, LX/AYI;->a:[I

    invoke-virtual {v2}, LX/AYJ;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 279343
    :goto_5
    iget-object v4, v0, LX/AYK;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279344
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    invoke-direct {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279345
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    .line 279346
    iget-object v2, v0, LX/AYK;->c:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v3, LX/AYH;

    invoke-direct {v3, v0, p0}, LX/AYH;-><init>(LX/AYK;LX/1bD;)V

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279347
    :cond_5
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->w:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->ai:J

    .line 279348
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->ay:J

    .line 279349
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279350
    iget-object v3, v2, Lcom/facebook/facecast/model/FacecastCompositionData;->k:LX/21D;

    move-object v2, v3

    .line 279351
    iput-object v2, v0, LX/AVT;->i:LX/21D;

    .line 279352
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279353
    iget-object v3, v2, Lcom/facebook/facecast/model/FacecastCompositionData;->l:Ljava/lang/String;

    move-object v2, v3

    .line 279354
    iput-object v2, v0, LX/AVT;->g:Ljava/lang/String;

    .line 279355
    new-instance v0, Lcom/facebook/facecast/FacecastActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/FacecastActivity$1;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->at:Ljava/lang/Runnable;

    .line 279356
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    invoke-virtual {v0, p0}, LX/AYq;->a(LX/1bG;)V

    .line 279357
    if-eqz v1, :cond_6

    .line 279358
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v2, LX/AVE;->FAILED:LX/AVE;

    invoke-virtual {v0, v2, v1}, LX/AVF;->a(LX/AVE;Ljava/lang/String;)V

    .line 279359
    sget-object v0, LX/AUv;->OTHER:LX/AUv;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a$redex0(Lcom/facebook/facecast/FacecastActivity;LX/AUv;)V

    .line 279360
    :cond_6
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/facecast/FacecastActivity;->av:F

    .line 279361
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    iget-boolean v1, p0, Lcom/facebook/facecast/FacecastActivity;->aH:Z

    invoke-virtual {v0, v1}, LX/1b4;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 279362
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    .line 279363
    iput-object p0, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->m:LX/1bA;

    .line 279364
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->U:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    .line 279365
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->n:Z

    .line 279366
    new-instance v1, LX/Aar;

    invoke-direct {v1}, LX/Aar;-><init>()V

    move-object v1, v1

    .line 279367
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 279368
    iget-object v2, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 279369
    new-instance v3, LX/AVa;

    invoke-direct {v3, v0, v1}, LX/AVa;-><init>(Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;LX/0zO;)V

    iget-object v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 279370
    :cond_7
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->D:LX/1b3;

    invoke-virtual {v0}, LX/1b3;->b()V

    .line 279371
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->F:LX/Aag;

    new-instance v1, LX/AUr;

    invoke-direct {v1, p0}, LX/AUr;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    .line 279372
    iput-object v1, v0, LX/Aag;->a:LX/AUr;

    .line 279373
    new-instance v2, LX/Aad;

    invoke-direct {v2}, LX/Aad;-><init>()V

    move-object v2, v2

    .line 279374
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 279375
    iget-object v3, v0, LX/Aag;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 279376
    new-instance v3, LX/Aaf;

    invoke-direct {v3, v0}, LX/Aaf;-><init>(LX/Aag;)V

    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 279377
    return-void

    .line 279378
    :cond_8
    invoke-virtual {p0, v6}, Lcom/facebook/facecast/FacecastActivity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 279379
    :cond_9
    iput-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->p:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    goto/16 :goto_2

    :cond_a
    move-object v0, v2

    goto/16 :goto_3

    :cond_b
    move-object v1, v2

    goto/16 :goto_1

    .line 279380
    :cond_c
    invoke-virtual {v0}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 279381
    :pswitch_0
    const v4, 0x7f0d0f69

    invoke-virtual {v3, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 279382
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 279383
    invoke-virtual {v3, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_5

    .line 279384
    :pswitch_1
    const v4, 0x7f0d0f69

    invoke-virtual {v3, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 279385
    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 279386
    const/16 v4, 0x15

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_5

    .line 279387
    :pswitch_2
    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 279388
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 279389
    invoke-virtual {v3, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_5

    .line 279390
    :pswitch_3
    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 279391
    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 279392
    const/16 v4, 0x15

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Lcom/facebook/video/videostreaming/LiveStreamingError;)V
    .locals 3

    .prologue
    .line 279272
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->y:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080ca8

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 279273
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->FAILED:LX/AVE;

    const-string v2, "broadcast_session_failed"

    invoke-virtual {v0, v1, v2, p1}, LX/AVF;->a(LX/AVE;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;)V

    .line 279274
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;I)V

    .line 279275
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->finish()V

    .line 279276
    return-void
.end method

.method public final b(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 279269
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->FAILED:LX/AVE;

    const-string v2, "Camera open failed"

    new-instance v3, Lcom/facebook/video/videostreaming/LiveStreamingError;

    invoke-direct {v3, p1}, Lcom/facebook/video/videostreaming/LiveStreamingError;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v0, v1, v2, v3}, LX/AVF;->a(LX/AVE;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;)V

    .line 279270
    sget-object v0, LX/AUv;->CAMERA:LX/AUv;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a$redex0(Lcom/facebook/facecast/FacecastActivity;LX/AUv;)V

    .line 279271
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 279264
    if-eqz p1, :cond_0

    .line 279265
    invoke-static {p0}, Lcom/facebook/facecast/FacecastActivity;->O(Lcom/facebook/facecast/FacecastActivity;)V

    .line 279266
    :goto_0
    return-void

    .line 279267
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->FAILED:LX/AVE;

    const-string v2, "Failed to fetch user privacy"

    invoke-virtual {v0, v1, v2}, LX/AVF;->a(LX/AVE;Ljava/lang/String;)V

    .line 279268
    sget-object v0, LX/AUv;->INTERNET:LX/AUv;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a$redex0(Lcom/facebook/facecast/FacecastActivity;LX/AUv;)V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 279209
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-nez v0, :cond_0

    .line 279210
    :goto_0
    return-void

    .line 279211
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    .line 279212
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, p1}, LX/7RY;->a(Z)V

    .line 279213
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aI:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aJ:Z

    if-nez v0, :cond_1

    .line 279214
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->j()V

    .line 279215
    iput-boolean v1, p0, Lcom/facebook/facecast/FacecastActivity;->aJ:Z

    .line 279216
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWK;

    .line 279217
    iget-object v2, v0, LX/AWK;->a:LX/AWL;

    .line 279218
    iput-boolean p1, v2, LX/AWL;->f:Z

    .line 279219
    if-eqz p1, :cond_8

    .line 279220
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279221
    iget-object v1, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    move-object v0, v1

    .line 279222
    const/high16 v1, 0x3f100000    # 0.5625f

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/FacecastPreviewView;->setAspectRatio(F)V

    .line 279223
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    if-nez v0, :cond_2

    .line 279224
    new-instance v0, LX/AWe;

    invoke-direct {v0, p0}, LX/AWe;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    .line 279225
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    .line 279226
    iget-object v1, v0, LX/AWe;->b:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/AWd;

    invoke-direct {v2, v0, p0}, LX/AWd;-><init>(LX/AWe;LX/1bB;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279227
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->ap:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 279228
    iget-object v2, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o:LX/AWy;

    if-eqz v2, :cond_c

    .line 279229
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    invoke-direct {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279230
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v0, :cond_3

    .line 279231
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    .line 279232
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->C:LX/1b4;

    .line 279233
    iget-object v1, v0, LX/1b4;->a:LX/0ad;

    sget-short v2, LX/1v6;->c:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 279234
    if-eqz v0, :cond_5

    .line 279235
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    if-nez v0, :cond_4

    .line 279236
    new-instance v0, LX/AWY;

    invoke-direct {v0, p0}, LX/AWY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    .line 279237
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    .line 279238
    iget-object v1, v0, LX/AWY;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    iget v2, v0, LX/AWY;->c:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->setPivotY(F)V

    .line 279239
    iget-object v1, v0, LX/AWY;->a:LX/3Hq;

    iget-object v2, v0, LX/AWY;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    iget v3, v0, LX/AWY;->c:I

    invoke-virtual {v1, v2, v3}, LX/3Hq;->a(Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;I)LX/AVY;

    move-result-object v1

    iput-object v1, v0, LX/AWY;->f:LX/AVY;

    .line 279240
    :cond_4
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;LX/AWT;)V

    .line 279241
    :cond_5
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-eqz v0, :cond_6

    .line 279242
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v0}, LX/AVA;->a()V

    .line 279243
    :cond_6
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->X:LX/AVc;

    const/4 v4, 0x1

    .line 279244
    iget-object v1, v0, LX/AVc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, v0, LX/AVc;->d:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 279245
    :cond_7
    :goto_2
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->U()V

    goto/16 :goto_0

    .line 279246
    :cond_8
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279247
    iget-object v2, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    move-object v2, v2

    .line 279248
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->au:Z

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Lcom/facebook/facecast/FacecastPreviewView;->setSquareScreen(Z)V

    .line 279249
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    .line 279250
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v0, :cond_9

    .line 279251
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    invoke-direct {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279252
    :cond_9
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    if-eqz v0, :cond_a

    .line 279253
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->bd:LX/AWY;

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;LX/AWT;)V

    .line 279254
    :cond_a
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-eqz v0, :cond_7

    .line 279255
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v0}, LX/AVA;->c()V

    goto :goto_2

    .line 279256
    :cond_b
    const/4 v0, 0x0

    goto :goto_3

    .line 279257
    :cond_c
    new-instance v2, LX/AWy;

    new-instance v3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->getContext()Landroid/content/Context;

    move-result-object v4

    const p1, 0x7f0e03cf

    invoke-direct {v3, v4, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v2, v3}, LX/AWy;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o:LX/AWy;

    .line 279258
    iget-object v2, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o:LX/AWy;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/AWy;->setFullScreen(Z)V

    .line 279259
    iget-object v2, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o:LX/AWy;

    .line 279260
    iget-object v3, v1, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v3, v3

    .line 279261
    invoke-virtual {v2, v3}, LX/AWy;->setComposerTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    goto/16 :goto_1

    .line 279262
    :cond_d
    iget-object v1, v0, LX/AVc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, v0, LX/AVc;->d:LX/0Tn;

    invoke-interface {v1, v2, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 279263
    new-instance v2, LX/0ju;

    iget-object v1, v0, LX/AVc;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v2, 0x7f080cff

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f080d00

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_2
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 279208
    iget v0, p0, Lcom/facebook/facecast/FacecastActivity;->av:F

    return v0
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 279194
    iput-boolean p1, p0, Lcom/facebook/facecast/FacecastActivity;->aK:Z

    .line 279195
    if-eqz p1, :cond_2

    .line 279196
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v0, :cond_0

    .line 279197
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    invoke-virtual {v0}, LX/AYK;->h()V

    .line 279198
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-eqz v0, :cond_1

    .line 279199
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v0}, LX/AVA;->d()I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/FacecastActivity;->aC:I

    .line 279200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->setRequestedOrientation(I)V

    .line 279201
    :cond_1
    :goto_0
    return-void

    .line 279202
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v0, :cond_3

    .line 279203
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    invoke-virtual {v0}, LX/AYK;->g()V

    .line 279204
    :cond_3
    iget v0, p0, Lcom/facebook/facecast/FacecastActivity;->aC:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 279205
    iget v0, p0, Lcom/facebook/facecast/FacecastActivity;->aC:I

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->setRequestedOrientation(I)V

    .line 279206
    :cond_4
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-eqz v0, :cond_1

    .line 279207
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v0}, LX/AVA;->c()V

    goto :goto_0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 279188
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 279189
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279190
    iget-object v1, v0, LX/AY1;->l:LX/AVP;

    .line 279191
    iget-object v2, v1, LX/AVP;->b:LX/AVN;

    iget-object v3, v1, LX/AVP;->b:LX/AVN;

    const/16 p0, 0x9

    invoke-virtual {v3, p0}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 279192
    iget-object v1, v0, LX/AY1;->h:LX/3HT;

    iget-object v2, v0, LX/AY1;->k:LX/AXy;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 279193
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 279183
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 279184
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279185
    iget-object v2, v0, LX/AY1;->l:LX/AVP;

    .line 279186
    iget-object p0, v2, LX/AVP;->b:LX/AVN;

    iget-object v0, v2, LX/AVP;->b:LX/AVN;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 279187
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 279163
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->L:Lcom/facebook/facecast/protocol/FacecastNetworker;

    if-eqz v0, :cond_3

    .line 279164
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 279165
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->L:Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->z()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    .line 279166
    new-instance v6, Lcom/facebook/facecast/protocol/VideoBroadcastSealRequest;

    invoke-direct {v6, v2}, Lcom/facebook/facecast/protocol/VideoBroadcastSealRequest;-><init>(Ljava/lang/String;)V

    .line 279167
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 279168
    const-string v7, "video_broadcast_seal_key"

    invoke-virtual {v8, v7, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 279169
    iget-object v6, v0, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    const-string v7, "video_broadcast_seal_type"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v10, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    const v11, 0x7cfbc858

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    .line 279170
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->k()Ljava/util/Map;

    move-result-object v5

    .line 279171
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    sget-object v2, LX/AVE;->SEAL_BROADCAST_REQUEST:LX/AVE;

    iget-object v2, v2, LX/AVE;->loggerName:Ljava/lang/String;

    const-string v3, "Seal Broadcast Request"

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 279172
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279173
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 279174
    sget-object v1, LX/AVE;->FINISHED:LX/AVE;

    if-ne v0, v1, :cond_0

    .line 279175
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->SHOW_END_SCREEN:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 279176
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    .line 279177
    iget-object v1, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v1

    .line 279178
    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    if-ne v0, v1, :cond_1

    .line 279179
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/AYp;)V

    .line 279180
    :cond_1
    return-void

    .line 279181
    :cond_2
    sget-object v0, Lcom/facebook/facecast/FacecastActivity;->q:Ljava/lang/String;

    const-string v1, "getBroadcastInitResponse() is null Unable to send seal request "

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 279182
    :cond_3
    sget-object v0, Lcom/facebook/facecast/FacecastActivity;->q:Ljava/lang/String;

    const-string v1, "mFacecastNetworker is null. Unable to send seal request"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 279154
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aN:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 279155
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aN:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 279156
    :cond_0
    return-void
.end method

.method public final iv_()I
    .locals 1

    .prologue
    .line 279759
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->D()I

    move-result v0

    return v0
.end method

.method public final iw_()V
    .locals 0

    .prologue
    .line 279901
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->P()V

    .line 279902
    invoke-static {p0}, Lcom/facebook/facecast/FacecastActivity;->O(Lcom/facebook/facecast/FacecastActivity;)V

    .line 279903
    return-void
.end method

.method public final ix_()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279893
    const/4 v0, 0x0

    .line 279894
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    if-eqz v1, :cond_0

    .line 279895
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-virtual {v0}, LX/AWT;->getLoggingInfo()Ljava/util/Map;

    move-result-object v0

    .line 279896
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    invoke-virtual {v1}, LX/AWT;->getLoggingInfo()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 279897
    if-nez v0, :cond_1

    .line 279898
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 279899
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 279900
    :cond_2
    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videostreaming/LiveStreamEncoderSurface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279892
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->o()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 279890
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->b()V

    .line 279891
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 279885
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279886
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 279887
    sget-object v1, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    if-ne v0, v1, :cond_1

    .line 279888
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->a()V

    .line 279889
    :cond_1
    return-void
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 279633
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->finish()V

    .line 279634
    return-void
.end method

.method public final o()V
    .locals 0

    .prologue
    .line 279883
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->U()V

    .line 279884
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 279851
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 279852
    const/16 v0, 0x1db3

    if-ne p1, v0, :cond_3

    .line 279853
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 279854
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 279855
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 279856
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->bc:LX/AYk;

    if-nez v1, :cond_0

    .line 279857
    new-instance v1, LX/AYk;

    invoke-direct {v1, p0}, LX/AYk;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->bc:LX/AYk;

    .line 279858
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->bc:LX/AYk;

    .line 279859
    iput-object p0, v1, LX/AYk;->h:LX/1bF;

    .line 279860
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->bc:LX/AYk;

    invoke-direct {p0, v1}, Lcom/facebook/facecast/FacecastActivity;->a(LX/AWT;)V

    .line 279861
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->bc:LX/AYk;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 279862
    iget-object v2, v1, LX/AYk;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    .line 279863
    iget-object v3, v1, LX/AYk;->c:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object p1

    int-to-float p2, v2

    const/high16 p3, 0x3f100000    # 0.5625f

    mul-float/2addr p2, p3

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p2

    invoke-virtual {v3, p1, v2, p2}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a(Ljava/lang/String;II)V

    .line 279864
    iput-object v0, v1, LX/AYk;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 279865
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    .line 279866
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 279867
    const-string v2, "facecast_event_name"

    const-string v3, "facecast_audio_pick_photo"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279868
    const-string v2, "facecast_event_extra"

    const-string v3, "success"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279869
    invoke-virtual {v0, v1}, LX/AVT;->a(Ljava/util/Map;)V

    .line 279870
    :cond_1
    :goto_0
    return-void

    .line 279871
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    .line 279872
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 279873
    const-string v2, "facecast_event_name"

    const-string v3, "facecast_audio_pick_photo"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279874
    const-string v2, "facecast_event_extra"

    const-string v3, "cancel"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279875
    invoke-virtual {v0, v1}, LX/AVT;->a(Ljava/util/Map;)V

    .line 279876
    goto :goto_0

    .line 279877
    :cond_3
    const/16 v0, 0x1db5

    if-ne p1, v0, :cond_1

    .line 279878
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    if-eqz v0, :cond_1

    .line 279879
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    .line 279880
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    invoke-virtual {v1}, LX/AZK;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 279881
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    invoke-virtual {v1}, LX/AZK;->c()V

    .line 279882
    :cond_4
    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 279843
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWT;

    .line 279844
    invoke-virtual {v0}, LX/AWT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279845
    :goto_0
    return-void

    .line 279846
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279847
    iget-object v1, v0, LX/AVF;->b:LX/AVE;

    move-object v0, v1

    .line 279848
    sget-object v1, LX/AVE;->FINISHED:LX/AVE;

    if-ne v0, v1, :cond_2

    const/4 v0, -0x1

    :goto_1
    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;I)V

    .line 279849
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 279850
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    .line 279821
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->F()Z

    move-result v0

    .line 279822
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 279823
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->F()Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 279824
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->E()Z

    move-result v0

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->F()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/FacecastActivity;->a(ZZ)V

    .line 279825
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279826
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279827
    iget-object v1, v0, LX/AY1;->l:LX/AVP;

    .line 279828
    iget-object v2, v1, LX/AVP;->b:LX/AVN;

    iget-object v3, v1, LX/AVP;->b:LX/AVN;

    const/16 v0, 0xf

    invoke-virtual {v3, v0}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 279829
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    if-eqz v0, :cond_1

    .line 279830
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aQ:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->F()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->setLandscape(Z)V

    .line 279831
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-eqz v0, :cond_2

    .line 279832
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v0}, LX/AVA;->d()I

    move-result v0

    .line 279833
    iget v1, p0, Lcom/facebook/facecast/FacecastActivity;->aC:I

    if-eq v0, v1, :cond_2

    .line 279834
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->t:LX/AVT;

    iget v2, p0, Lcom/facebook/facecast/FacecastActivity;->aC:I

    invoke-static {v2}, LX/AVA;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, LX/AVA;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 279835
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 279836
    const-string p1, "previous_orientation"

    invoke-interface {v4, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279837
    const-string p1, "orientation"

    invoke-interface {v4, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279838
    invoke-virtual {v1, v4}, LX/AVT;->a(Ljava/util/Map;)V

    .line 279839
    iput v0, p0, Lcom/facebook/facecast/FacecastActivity;->aC:I

    .line 279840
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    if-eqz v0, :cond_2

    .line 279841
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->D()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/AV1;->a(ILjava/lang/Object;)V

    .line 279842
    :cond_2
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x66052c2c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 279792
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 279793
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWT;

    .line 279794
    invoke-virtual {v0}, LX/AWT;->d()V

    goto :goto_0

    .line 279795
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aA:Z

    if-nez v0, :cond_1

    .line 279796
    const v0, 0xc0434b8

    invoke-static {v0, v1}, LX/02F;->c(II)V

    .line 279797
    :goto_1
    return-void

    .line 279798
    :cond_1
    iput-object v3, p0, Lcom/facebook/facecast/FacecastActivity;->af:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    .line 279799
    iput-object v3, p0, Lcom/facebook/facecast/FacecastActivity;->ae:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    .line 279800
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aA:Z

    .line 279801
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 279802
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 279803
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279804
    iget-object v2, v0, LX/AVF;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 279805
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->r()V

    .line 279806
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, v3}, LX/7RY;->a(LX/1bK;)V

    .line 279807
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, v3}, LX/7RY;->a(LX/1bL;)V

    .line 279808
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0, v3}, LX/7RY;->a(LX/1bJ;)V

    .line 279809
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    invoke-virtual {v0, v3}, LX/AV1;->a(LX/1b6;)V

    .line 279810
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    .line 279811
    iget-object v2, v0, LX/AYq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 279812
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    const/4 v3, 0x0

    .line 279813
    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    iput-object v2, v0, LX/AYq;->c:LX/AYp;

    .line 279814
    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    iput-object v2, v0, LX/AYq;->b:LX/AYp;

    .line 279815
    iput-boolean v3, v0, LX/AYq;->d:Z

    .line 279816
    iput v3, v0, LX/AYq;->e:I

    .line 279817
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->M:LX/AVC;

    invoke-virtual {v0}, LX/AVC;->a()V

    .line 279818
    invoke-static {}, LX/8wJ;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->stopService(Landroid/content/Intent;)Z

    .line 279819
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->E:LX/AVh;

    invoke-virtual {v0}, LX/AVh;->b()V

    .line 279820
    const v0, -0x7f761e10

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_1
.end method

.method public final onPause()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x15169d51

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 279760
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aN:Ljava/util/concurrent/CountDownLatch;

    .line 279761
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 279762
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWT;

    .line 279763
    invoke-virtual {v0}, LX/AWT;->e()V

    goto :goto_0

    .line 279764
    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279765
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->M:LX/AVC;

    iget-boolean v2, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    .line 279766
    iget-boolean v6, v0, LX/AVC;->g:Z

    if-nez v6, :cond_3

    .line 279767
    :goto_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v2, LX/AVE;->INTERRUPTED:LX/AVE;

    invoke-virtual {v0, v2}, LX/AVF;->b(LX/AVE;)V

    .line 279768
    :cond_1
    sget-object v0, LX/AUq;->a:[I

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    .line 279769
    iget-object v4, v2, LX/AYq;->b:LX/AYp;

    move-object v2, v4

    .line 279770
    invoke-virtual {v2}, LX/AYp;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 279771
    :goto_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    .line 279772
    iget-object v2, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v2

    .line 279773
    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    if-eq v0, v2, :cond_2

    .line 279774
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->f()V

    .line 279775
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    invoke-virtual {v0, v3, v5}, LX/AV1;->a(ILjava/lang/Object;)V

    .line 279776
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aN:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279777
    iput-object v5, p0, Lcom/facebook/facecast/FacecastActivity;->aN:Ljava/util/concurrent/CountDownLatch;

    .line 279778
    :goto_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->w:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->ay:J

    .line 279779
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->at:Ljava/lang/Runnable;

    invoke-static {v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 279780
    const v0, 0x719f8d

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 279781
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

    invoke-virtual {v0, v2}, LX/AYq;->a(LX/AYp;)V

    goto :goto_2

    .line 279782
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    invoke-virtual {v0, v2}, LX/AYq;->a(LX/AYp;)V

    goto :goto_2

    .line 279783
    :catch_0
    move-exception v0

    .line 279784
    :try_start_1
    sget-object v2, Lcom/facebook/facecast/FacecastActivity;->q:Ljava/lang/String;

    const-string v3, "On pause potentially stuck"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279785
    iput-object v5, p0, Lcom/facebook/facecast/FacecastActivity;->aN:Ljava/util/concurrent/CountDownLatch;

    goto :goto_3

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lcom/facebook/facecast/FacecastActivity;->aN:Ljava/util/concurrent/CountDownLatch;

    const v2, -0x5100c03c

    invoke-static {v2, v1}, LX/02F;->c(II)V

    throw v0

    .line 279786
    :cond_3
    iput-boolean v2, v0, LX/AVC;->k:Z

    .line 279787
    invoke-virtual {v0}, LX/AVC;->a()V

    .line 279788
    iget-object v6, v0, LX/AVC;->e:Landroid/os/Handler;

    iget-object v7, v0, LX/AVC;->j:Ljava/lang/Runnable;

    const-wide/16 v8, 0x2710

    const v10, -0x2a831af5

    invoke-static {v6, v7, v8, v9, v10}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 279789
    iget-object v6, v0, LX/AVC;->h:LX/AVU;

    .line 279790
    const-string v7, "facecast_schedule_pause_notif"

    invoke-static {v6, v7}, LX/AVU;->c(LX/AVU;Ljava/lang/String;)V

    .line 279791
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, -0x53f4830

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 279635
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    .line 279636
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 279637
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->M:LX/AVC;

    invoke-virtual {v0}, LX/AVC;->a()V

    .line 279638
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279639
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v2, LX/AVE;->FINISHED:LX/AVE;

    const-string v3, "interrupted_for_too_long"

    invoke-virtual {v0, v2, v3}, LX/AVF;->a(LX/AVE;Ljava/lang/String;)V

    .line 279640
    const/16 v0, 0x23

    const v2, -0x65cc3ec

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 279641
    :goto_0
    return-void

    .line 279642
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/facecast/FacecastActivity;->ay:J

    .line 279643
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWT;

    .line 279644
    invoke-virtual {v0}, LX/AWT;->f()V

    goto :goto_1

    .line 279645
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->v:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    sget-object v2, Lcom/facebook/facecast/FacecastActivity;->r:[Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 279646
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    .line 279647
    iget-object v2, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v2

    .line 279648
    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    if-eq v0, v2, :cond_2

    .line 279649
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->N()V

    .line 279650
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    .line 279651
    iget-object v2, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v2

    .line 279652
    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    if-ne v0, v2, :cond_3

    .line 279653
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->J:LX/AYq;

    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    invoke-virtual {v0, v2}, LX/AYq;->a(LX/AYp;)V

    .line 279654
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->D()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, LX/AV1;->a(ILjava/lang/Object;)V

    .line 279655
    invoke-static {p0}, Lcom/facebook/facecast/FacecastActivity;->B(Lcom/facebook/facecast/FacecastActivity;)V

    .line 279656
    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279657
    iput-object v5, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    .line 279658
    :goto_3
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/facecast/FacecastActivity;->at:Ljava/lang/Runnable;

    const v3, 0x7f144323

    invoke-static {v0, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 279659
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aF:Z

    if-eqz v0, :cond_4

    .line 279660
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->z:LX/7RY;

    invoke-interface {v0}, LX/7RY;->q()V

    .line 279661
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-eqz v0, :cond_4

    .line 279662
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    .line 279663
    iget-object v2, v0, LX/AVA;->a:Landroid/app/Activity;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 279664
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastActivity;->aK:Z

    if-eqz v0, :cond_5

    .line 279665
    invoke-virtual {p0, v6}, Lcom/facebook/facecast/FacecastActivity;->setRequestedOrientation(I)V

    .line 279666
    :cond_5
    const v0, -0x525223ef

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto/16 :goto_0

    .line 279667
    :cond_6
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 279668
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->C()V

    goto :goto_2

    .line 279669
    :catch_0
    move-exception v0

    .line 279670
    :try_start_1
    sget-object v2, Lcom/facebook/facecast/FacecastActivity;->q:Ljava/lang/String;

    const-string v3, "onResume potentially stuck"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279671
    iput-object v5, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    goto :goto_3

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lcom/facebook/facecast/FacecastActivity;->aO:Ljava/util/concurrent/CountDownLatch;

    const v2, 0x1ff893e5

    invoke-static {v2, v1}, LX/02F;->c(II)V

    throw v0
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 279754
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 279755
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279756
    iget-object v1, v0, LX/AY1;->l:LX/AVP;

    .line 279757
    iget-object v2, v1, LX/AVP;->b:LX/AVN;

    iget-object p0, v1, LX/AVP;->b:LX/AVN;

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 279758
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 5

    .prologue
    .line 279741
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 279742
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    .line 279743
    iput-object v1, v0, LX/AY1;->m:LX/AV1;

    .line 279744
    iget-object v2, v0, LX/AY1;->l:LX/AVP;

    .line 279745
    iget-object v3, v2, LX/AVP;->b:LX/AVN;

    iget-object v4, v2, LX/AVP;->b:LX/AVN;

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 279746
    iget-object v2, v0, LX/AY1;->l:LX/AVP;

    .line 279747
    iget v3, v1, LX/AV1;->o:I

    move v3, v3

    .line 279748
    iget v4, v1, LX/AV1;->p:I

    move v4, v4

    .line 279749
    invoke-virtual {v2, v3, v4}, LX/AVP;->a(II)V

    .line 279750
    iget-object v2, v0, LX/AY1;->m:LX/AV1;

    invoke-virtual {v2}, LX/AV1;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279751
    iget-object v2, v0, LX/AY1;->o:LX/AY0;

    iget-object v3, v0, LX/AY1;->o:LX/AY0;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/AY0;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AY0;->sendMessage(Landroid/os/Message;)Z

    .line 279752
    iget-object v2, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    new-instance v3, LX/AXu;

    invoke-direct {v3, v0}, LX/AXu;-><init>(LX/AY1;)V

    invoke-virtual {v2, v3}, Lcom/facebook/facecast/FacecastPreviewView;->a(Landroid/view/View$OnTouchListener;)V

    .line 279753
    :cond_0
    return-void
.end method

.method public final q_(I)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 279721
    invoke-static {p1}, LX/7Rb;->fromInteger(I)LX/7Rb;

    move-result-object v0

    .line 279722
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->I:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 279723
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    const-string v2, "lost_connection"

    invoke-virtual {v0, v1, v2}, LX/AVF;->a(LX/AVE;Ljava/lang/String;)V

    .line 279724
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->at:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, 0x1cd93f89

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 279725
    return-void

    .line 279726
    :cond_1
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/7Rb;->SHOULD_STOP_STREAMING:LX/7Rb;

    if-ne v0, v1, :cond_2

    .line 279727
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    const-string v2, "network_lagging"

    invoke-virtual {v0, v1, v2}, LX/AVF;->a(LX/AVE;Ljava/lang/String;)V

    goto :goto_0

    .line 279728
    :cond_2
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/7Rb;->WEAK:LX/7Rb;

    if-ne v0, v1, :cond_3

    .line 279729
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-virtual {v0, v3}, LX/AYE;->setWeakConnection(Z)V

    goto :goto_0

    .line 279730
    :cond_3
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->S()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, LX/7Rb;->NORMAL:LX/7Rb;

    if-ne v0, v1, :cond_4

    .line 279731
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AYE;->setWeakConnection(Z)V

    goto :goto_0

    .line 279732
    :cond_4
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279733
    iget-object v2, v1, LX/AVF;->b:LX/AVE;

    move-object v1, v2

    .line 279734
    sget-object v2, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->I:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, LX/7Rb;->NORMAL:LX/7Rb;

    if-ne v0, v1, :cond_5

    .line 279735
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->RECORDING:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    goto :goto_0

    .line 279736
    :cond_5
    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    .line 279737
    iget-object v2, v1, LX/AVF;->b:LX/AVE;

    move-object v1, v2

    .line 279738
    sget-object v2, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->I:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/7Rb;->WEAK:LX/7Rb;

    if-ne v0, v1, :cond_0

    .line 279739
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->x:LX/AVF;

    sget-object v1, LX/AVE;->RECORDING:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 279740
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    invoke-virtual {v0, v3}, LX/AYE;->setWeakConnection(Z)V

    goto :goto_0
.end method

.method public final r()V
    .locals 8

    .prologue
    .line 279703
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 279704
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    const/4 v2, 0x1

    .line 279705
    iget-object v3, v0, LX/AY1;->l:LX/AVP;

    .line 279706
    iget v4, v1, LX/AV1;->o:I

    move v4, v4

    .line 279707
    iget v5, v1, LX/AV1;->p:I

    move v5, v5

    .line 279708
    invoke-virtual {v3, v4, v5}, LX/AVP;->a(II)V

    .line 279709
    iget-object v3, v0, LX/AY1;->l:LX/AVP;

    invoke-virtual {v1}, LX/AV1;->a()I

    move-result v4

    .line 279710
    new-instance v5, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v5}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 279711
    invoke-static {v4, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 279712
    iget v5, v5, Landroid/hardware/Camera$CameraInfo;->orientation:I

    move v4, v5

    .line 279713
    iget v5, v1, LX/AV1;->e:I

    move v5, v5

    .line 279714
    if-ne v5, v2, :cond_2

    .line 279715
    :goto_0
    iget-object v6, v3, LX/AVP;->b:LX/AVN;

    iget-object v7, v3, LX/AVP;->b:LX/AVN;

    const/16 v0, 0x15

    if-eqz v2, :cond_3

    const/4 v5, 0x1

    :goto_1
    const/4 v1, 0x0

    invoke-virtual {v7, v0, v4, v5, v1}, LX/AVN;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 279716
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aP:LX/AYK;

    if-eqz v0, :cond_1

    .line 279717
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->u:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecast/FacecastActivity$4;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/FacecastActivity$4;-><init>(Lcom/facebook/facecast/FacecastActivity;)V

    const v2, 0x632fd226

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 279718
    :cond_1
    return-void

    .line 279719
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 279720
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 279701
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aq:LX/AV1;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/AV1;->a(ILjava/lang/Object;)V

    .line 279702
    return-void
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 279698
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;I)V

    .line 279699
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastActivity;->finish()V

    .line 279700
    return-void
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 279690
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    if-eqz v0, :cond_0

    .line 279691
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279692
    iget-object v1, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    move-object v1, v1

    .line 279693
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aW:LX/AY1;

    .line 279694
    iget-object p0, v0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    move-object v0, p0

    .line 279695
    invoke-virtual {v0}, Lcom/facebook/facecast/FacecastPreviewView;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/facecast/FacecastPreviewView;->setSquareScreen(Z)V

    .line 279696
    :cond_0
    return-void

    .line 279697
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 279685
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    if-eqz v0, :cond_0

    .line 279686
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/AWe;->setVisibility(I)V

    .line 279687
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-nez v0, :cond_2

    .line 279688
    :cond_1
    :goto_0
    return-void

    .line 279689
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v0}, LX/AVA;->a()V

    goto :goto_0
.end method

.method public final w()V
    .locals 2

    .prologue
    .line 279680
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    if-eqz v0, :cond_0

    .line 279681
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->ba:LX/AWe;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AWe;->setVisibility(I)V

    .line 279682
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->B:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    if-nez v0, :cond_2

    .line 279683
    :cond_1
    :goto_0
    return-void

    .line 279684
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aB:LX/AVA;

    invoke-virtual {v0}, LX/AVA;->c()V

    goto :goto_0
.end method

.method public final x()V
    .locals 0

    .prologue
    .line 279678
    invoke-direct {p0}, Lcom/facebook/facecast/FacecastActivity;->V()V

    .line 279679
    return-void
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 279672
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aD:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 279673
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    if-eqz v0, :cond_0

    .line 279674
    iget-object v0, p0, Lcom/facebook/facecast/FacecastActivity;->aR:LX/AYE;

    .line 279675
    iget-object v1, v0, LX/AYE;->m:LX/AWl;

    .line 279676
    iget-object p0, v1, LX/AWl;->f:LX/Aby;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Aby;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 279677
    :cond_0
    return-void
.end method
