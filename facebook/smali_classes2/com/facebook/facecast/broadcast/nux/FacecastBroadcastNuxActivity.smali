.class public Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final p:Ljava/lang/String;

.field private static final q:J


# instance fields
.field private A:LX/AVv;

.field private B:LX/AVd;

.field public C:Lcom/facebook/video/player/RichVideoPlayer;

.field private D:LX/AVg;

.field public E:Z

.field private F:J

.field public r:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/AVn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Ljava/lang/Class;
    .annotation runtime Lcom/facebook/facecast/launcher/FacecastLauncherActivity;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:LX/1b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/AVh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:LX/AWJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:LX/AVt;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 280316
    const-class v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->p:Ljava/lang/String;

    .line 280317
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->q:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 280430
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 280431
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->F:J

    .line 280432
    return-void
.end method

.method private static a(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;LX/03V;LX/AVn;Ljava/lang/Class;Lcom/facebook/content/SecureContextHelper;LX/1b3;LX/AVh;LX/AWJ;LX/0SG;)V
    .locals 0

    .prologue
    .line 280429
    iput-object p1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->r:LX/03V;

    iput-object p2, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    iput-object p3, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->t:Ljava/lang/Class;

    iput-object p4, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->u:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->v:LX/1b3;

    iput-object p6, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->w:LX/AVh;

    iput-object p7, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->x:LX/AWJ;

    iput-object p8, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->y:LX/0SG;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    invoke-static {v8}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v8}, LX/AVn;->b(LX/0QB;)LX/AVn;

    move-result-object v2

    check-cast v2, LX/AVn;

    invoke-static {v8}, LX/1b5;->a(LX/0QB;)Ljava/lang/Class;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/1b3;->b(LX/0QB;)LX/1b3;

    move-result-object v5

    check-cast v5, LX/1b3;

    invoke-static {v8}, LX/AVh;->b(LX/0QB;)LX/AVh;

    move-result-object v6

    check-cast v6, LX/AVh;

    invoke-static {v8}, LX/AWJ;->b(LX/0QB;)LX/AWJ;

    move-result-object v7

    check-cast v7, LX/AWJ;

    invoke-static {v8}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static/range {v0 .. v8}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->a(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;LX/03V;LX/AVn;Ljava/lang/Class;Lcom/facebook/content/SecureContextHelper;LX/1b3;LX/AVh;LX/AWJ;LX/0SG;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;Ljava/lang/String;D)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 280400
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 280401
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04D;->FACECAST_NUX:LX/04D;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 280402
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 280403
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v1

    .line 280404
    iput-object v0, v1, LX/2oE;->a:Landroid/net/Uri;

    .line 280405
    move-object v0, v1

    .line 280406
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 280407
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 280408
    move-object v0, v0

    .line 280409
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 280410
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    .line 280411
    iput-boolean v3, v0, LX/2oH;->t:Z

    .line 280412
    move-object v0, v0

    .line 280413
    iput-boolean v3, v0, LX/2oH;->g:Z

    .line 280414
    move-object v0, v0

    .line 280415
    iput v3, v0, LX/2oH;->p:I

    .line 280416
    move-object v0, v0

    .line 280417
    iput v3, v0, LX/2oH;->s:I

    .line 280418
    move-object v0, v0

    .line 280419
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 280420
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 280421
    move-object v0, v1

    .line 280422
    iput-wide p2, v0, LX/2pZ;->e:D

    .line 280423
    move-object v0, v0

    .line 280424
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 280425
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 280426
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v3, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 280427
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->w:LX/AVh;

    const-string v1, "nux_video_start"

    invoke-virtual {v0, v1}, LX/AVh;->a(Ljava/lang/String;)V

    .line 280428
    return-void
.end method

.method public static b$redex0(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V
    .locals 3

    .prologue
    .line 280391
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    .line 280392
    iget-object v1, v0, LX/AVn;->c:LX/AVt;

    if-nez v1, :cond_1

    .line 280393
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->finish()V

    .line 280394
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->t:Ljava/lang/Class;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, LX/AWM;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 280395
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->u:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 280396
    invoke-direct {p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->l()V

    .line 280397
    return-void

    .line 280398
    :cond_1
    iget-object v1, v0, LX/AVn;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AVj;

    .line 280399
    invoke-virtual {v1}, LX/AVj;->e()V

    goto :goto_0
.end method

.method private l()V
    .locals 6

    .prologue
    .line 280380
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 280381
    :cond_0
    :goto_0
    return-void

    .line 280382
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    .line 280383
    if-lez v0, :cond_2

    int-to-long v2, v0

    sget-wide v4, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->q:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 280384
    :cond_2
    if-nez v0, :cond_3

    iget-wide v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->F:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->y:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->F:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoDurationMs()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 280385
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->v:LX/1b3;

    .line 280386
    iget-object v1, v0, LX/1b3;->b:LX/0iA;

    sget-object v2, LX/3EJ;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/3EJ;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/3EJ;

    .line 280387
    if-eqz v1, :cond_4

    .line 280388
    iget-object v2, v1, LX/3EJ;->c:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    const-string v3, "4540"

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 280389
    iget-object v2, v1, LX/3EJ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/3EJ;->a:LX/0Tn;

    const/4 v0, 0x1

    invoke-interface {v2, v3, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 280390
    :cond_4
    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 280433
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 280434
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 280435
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 280436
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 280437
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->z:LX/AVt;

    .line 280438
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 280439
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->A:LX/AVv;

    .line 280440
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 280441
    return-void
.end method

.method public static n(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V
    .locals 1

    .prologue
    .line 280377
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    invoke-virtual {v0}, LX/AVn;->c()V

    .line 280378
    invoke-static {p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->b$redex0(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    .line 280379
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 280339
    const v0, 0x7f0305aa

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->setContentView(I)V

    .line 280340
    invoke-static {p0, p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 280341
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->w:LX/AVh;

    invoke-virtual {v0}, LX/AVh;->a()V

    .line 280342
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->w:LX/AVh;

    const-string v1, "launch_live_composer"

    invoke-virtual {v0, v1}, LX/AVh;->a(Ljava/lang/String;)V

    .line 280343
    const v0, 0x7f0d0f87

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    .line 280344
    new-instance v0, LX/AVg;

    invoke-direct {v0, p0}, LX/AVg;-><init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->D:LX/AVg;

    .line 280345
    new-instance v0, LX/AVd;

    invoke-direct {v0, p0}, LX/AVd;-><init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->B:LX/AVd;

    .line 280346
    new-instance v0, LX/AVt;

    invoke-direct {v0, p0}, LX/AVt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->z:LX/AVt;

    .line 280347
    new-instance v0, LX/AVv;

    invoke-direct {v0, p0}, LX/AVv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->A:LX/AVv;

    .line 280348
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->A:LX/AVv;

    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->B:LX/AVd;

    .line 280349
    iget-object v3, v0, LX/AVv;->a:Landroid/view/View;

    new-instance p1, LX/AVu;

    invoke-direct {p1, v0, v1}, LX/AVu;-><init>(LX/AVv;LX/AVd;)V

    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280350
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->z:LX/AVt;

    .line 280351
    if-nez v1, :cond_1

    .line 280352
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->B:LX/AVd;

    .line 280353
    iput-object v1, v0, LX/AVn;->i:LX/AVd;

    .line 280354
    invoke-direct {p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->m()V

    .line 280355
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->v:LX/1b3;

    invoke-virtual {v0}, LX/1b3;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 280356
    invoke-static {p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->b$redex0(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    .line 280357
    :goto_1
    return-void

    .line 280358
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->x:LX/AWJ;

    new-instance v1, LX/AVf;

    invoke-direct {v1, p0}, LX/AVf;-><init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    invoke-virtual {v0, v1, v2}, LX/AWJ;->a(LX/AVe;Z)V

    .line 280359
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->v:LX/1b3;

    invoke-virtual {v0}, LX/1b3;->b()V

    .line 280360
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->y:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->F:J

    goto :goto_1

    .line 280361
    :cond_1
    iput-object v1, v0, LX/AVn;->c:LX/AVt;

    .line 280362
    iget-object v3, v0, LX/AVn;->d:LX/AVq;

    iget-object p1, v0, LX/AVn;->c:LX/AVt;

    .line 280363
    iget-object v1, p1, LX/AVt;->a:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    move-object p1, v1

    .line 280364
    invoke-virtual {v3, p1}, LX/AVi;->a(Landroid/view/View;)V

    .line 280365
    iget-object v3, v0, LX/AVn;->e:LX/AVo;

    iget-object p1, v0, LX/AVn;->c:LX/AVt;

    .line 280366
    iget-object v1, p1, LX/AVt;->b:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    move-object p1, v1

    .line 280367
    invoke-virtual {v3, p1}, LX/AVi;->a(Landroid/view/View;)V

    .line 280368
    iget-object v3, v0, LX/AVn;->f:LX/AVp;

    iget-object p1, v0, LX/AVn;->c:LX/AVt;

    .line 280369
    iget-object v1, p1, LX/AVt;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    move-object p1, v1

    .line 280370
    invoke-virtual {v3, p1}, LX/AVi;->a(Landroid/view/View;)V

    .line 280371
    iget-object v3, v0, LX/AVn;->g:LX/AVl;

    iget-object p1, v0, LX/AVn;->c:LX/AVt;

    .line 280372
    iget-object v1, p1, LX/AVt;->d:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    move-object p1, v1

    .line 280373
    invoke-virtual {v3, p1}, LX/AVi;->a(Landroid/view/View;)V

    .line 280374
    iget-object v3, v0, LX/AVn;->h:LX/AVr;

    iget-object p1, v0, LX/AVn;->c:LX/AVt;

    .line 280375
    iget-object v0, p1, LX/AVt;->e:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    move-object p1, v0

    .line 280376
    invoke-virtual {v3, p1}, LX/AVi;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 280336
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->w:LX/AVh;

    const-string v1, "nux_video_abort"

    iget-object v2, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/AVh;->a(Ljava/lang/String;I)V

    .line 280337
    invoke-static {p0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->n(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    .line 280338
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x48407173

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 280331
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 280332
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    invoke-virtual {v1}, LX/AVn;->c()V

    .line 280333
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->D:LX/AVg;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 280334
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->n()Ljava/util/List;

    .line 280335
    const/16 v1, 0x23

    const v2, -0x37d527a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x59ea9d4a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 280326
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 280327
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->D:LX/AVg;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 280328
    iget-boolean v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->E:Z

    if-eqz v1, :cond_0

    .line 280329
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    invoke-virtual {v1}, LX/AVn;->c()V

    .line 280330
    :cond_0
    const/16 v1, 0x23

    const v2, -0x3eb20d66

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1cf797b5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 280318
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 280319
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->D:LX/AVg;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 280320
    iget-boolean v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->E:Z

    if-eqz v1, :cond_0

    .line 280321
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setVolume(F)V

    .line 280322
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_ANDROID:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 280323
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    .line 280324
    invoke-static {v1}, LX/AVn;->d(LX/AVn;)V

    .line 280325
    :cond_0
    const/16 v1, 0x23

    const v2, 0x67a0f340

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
