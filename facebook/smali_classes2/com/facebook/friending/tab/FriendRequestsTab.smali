.class public Lcom/facebook/friending/tab/FriendRequestsTab;
.super Lcom/facebook/apptab/state/TabTag;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friending/tab/FriendRequestsTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/facebook/friending/tab/FriendRequestsTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163346
    new-instance v0, Lcom/facebook/friending/tab/FriendRequestsTab;

    invoke-direct {v0}, Lcom/facebook/friending/tab/FriendRequestsTab;-><init>()V

    sput-object v0, Lcom/facebook/friending/tab/FriendRequestsTab;->l:Lcom/facebook/friending/tab/FriendRequestsTab;

    .line 163347
    new-instance v0, LX/0xd;

    invoke-direct {v0}, LX/0xd;-><init>()V

    sput-object v0, Lcom/facebook/friending/tab/FriendRequestsTab;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const v6, 0x63000e

    .line 163348
    sget-object v1, LX/0ax;->da:Ljava/lang/String;

    sget-object v2, LX/0cQ;->FRIEND_REQUESTS_FRAGMENT:LX/0cQ;

    const v3, 0x7f021074

    const/4 v4, 0x0

    const-string v5, "friend_requests"

    const v10, 0x7f080f70

    const v11, 0x7f0d0031

    move-object v0, p0

    move v7, v6

    move-object v9, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/apptab/state/TabTag;-><init>(Ljava/lang/String;LX/0cQ;IZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;II)V

    .line 163349
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163350
    const-string v0, "FriendRequests"

    return-object v0
.end method
