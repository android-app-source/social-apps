.class public Lcom/facebook/friending/jewel/FriendRequestsFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0gr;
.implements LX/0fv;
.implements LX/0fw;


# annotations
.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "android.R.id.list"
        type = .enum LX/8Cz;->LIST_VIEW:LX/8Cz;
    .end subannotation
.end annotation


# static fields
.field private static final T:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/friending/jewel/FriendRequestsFragment;",
            ">;"
        }
    .end annotation
.end field

.field public static final U:Lcom/facebook/common/callercontext/CallerContext;

.field private static final V:I


# instance fields
.field public A:LX/2do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/2ha;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/2hb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/2e3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/2dl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/2hc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0xB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0gh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/2hd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/2he;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/2hf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/0gy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/0gw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/0Or;
    .annotation runtime Lcom/facebook/friending/jewel/gk/IsFabViewEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final W:LX/2Ip;

.field private final X:LX/2hG;

.field private final Y:LX/2hI;

.field private final Z:LX/2hK;

.field public aA:I

.field public aB:Z

.field public aC:LX/2h7;

.field private aD:Z

.field public aE:Z

.field private aF:Z

.field private aG:Z

.field public aH:Z

.field public aI:Z

.field public aJ:Z

.field public aK:Z

.field private aL:Z

.field private aM:Z

.field public aN:Z

.field public aO:Z

.field public aP:Z

.field public aQ:Z

.field public aR:Z

.field private final aa:LX/2hM;

.field private final ab:LX/2hO;

.field private final ac:LX/0dN;

.field private ad:Landroid/view/View$OnClickListener;

.field public ae:Landroid/support/v4/widget/SwipeRefreshLayout;

.field public af:LX/4nS;

.field private ag:Landroid/view/View;

.field private ah:Lcom/facebook/resources/ui/FbTextView;

.field private ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public ak:LX/0g8;

.field public al:LX/2iK;

.field private am:LX/2if;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:LX/2hl;

.field private ao:LX/2hs;

.field public ap:LX/0Yb;

.field private aq:LX/0xA;

.field private ar:LX/0fx;

.field private as:Landroid/view/View;

.field private at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

.field public au:LX/B9g;

.field public av:J

.field private aw:Z

.field private ax:Z

.field public ay:Ljava/lang/String;

.field public az:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/2hQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2hR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2hS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1Kt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2hT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2hU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EzP;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/83b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/2hW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/2hX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/2dj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 113826
    const-class v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 113827
    sput-object v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->T:Ljava/lang/Class;

    const-string v1, "friend_requests"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->U:Lcom/facebook/common/callercontext/CallerContext;

    .line 113828
    const v0, 0x7f080f70

    sput v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->V:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113829
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 113830
    new-instance v0, LX/2hE;

    invoke-direct {v0, p0}, LX/2hE;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->W:LX/2Ip;

    .line 113831
    new-instance v0, LX/2hF;

    invoke-direct {v0, p0}, LX/2hF;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->X:LX/2hG;

    .line 113832
    new-instance v0, LX/2hH;

    invoke-direct {v0, p0}, LX/2hH;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Y:LX/2hI;

    .line 113833
    new-instance v0, LX/2hJ;

    invoke-direct {v0, p0}, LX/2hJ;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Z:LX/2hK;

    .line 113834
    new-instance v0, LX/2hL;

    invoke-direct {v0, p0}, LX/2hL;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aa:LX/2hM;

    .line 113835
    new-instance v0, LX/2hN;

    invoke-direct {v0, p0}, LX/2hN;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ab:LX/2hO;

    .line 113836
    new-instance v0, LX/2hP;

    invoke-direct {v0, p0}, LX/2hP;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ac:LX/0dN;

    .line 113837
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aw:Z

    .line 113838
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ax:Z

    .line 113839
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aK:Z

    .line 113840
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aL:Z

    .line 113841
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aM:Z

    .line 113842
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aN:Z

    .line 113843
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aO:Z

    .line 113844
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aP:Z

    .line 113845
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aQ:Z

    return-void
.end method

.method private A()V
    .locals 1

    .prologue
    .line 113846
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->g$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    .line 113847
    return-void
.end method

.method private D()V
    .locals 5

    .prologue
    .line 113848
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Q:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113849
    :goto_0
    return-void

    .line 113850
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->P:LX/0gy;

    sget-object v1, LX/0cQ;->FRIEND_REQUESTS_FRAGMENT:LX/0cQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    const v4, 0x7f0101e2

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0gy;->a(LX/0cQ;Landroid/content/Context;Landroid/support/v4/widget/SwipeRefreshLayout;I)V

    goto :goto_0
.end method

.method public static E(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 3

    .prologue
    .line 113851
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113852
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->t:LX/03V;

    sget-object v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->T:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "showPTRRetryToast is called outside of fragment lifecycle"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113853
    :goto_0
    return-void

    .line 113854
    :cond_0
    new-instance v0, LX/62h;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 113855
    new-instance v1, LX/Eyr;

    invoke-direct {v1, p0}, LX/Eyr;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 113856
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->q:LX/2hU;

    const/16 v2, 0x2710

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->af:LX/4nS;

    .line 113857
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->af:LX/4nS;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 113858
    iput-object v1, v0, LX/4nS;->i:Landroid/view/View;

    .line 113859
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->af:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    goto :goto_0
.end method

.method public static a(LX/2h7;ZZZZZ)Lcom/facebook/friending/jewel/FriendRequestsFragment;
    .locals 3

    .prologue
    .line 113860
    new-instance v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;-><init>()V

    .line 113861
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 113862
    const-string v2, "friending_location"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 113863
    const-string v2, "open_timeline_on_click"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113864
    const-string v2, "show_no_requests"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113865
    const-string v2, "show_friend_requests_header"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113866
    const-string v2, "fetch_on_visible"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113867
    const-string v2, "mark_nux_step_complete_on_empty_content"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113868
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 113869
    return-object v0
.end method

.method private static a(Lcom/facebook/friending/jewel/FriendRequestsFragment;LX/2hQ;LX/2hR;LX/2hS;LX/1Kt;LX/2hT;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Sh;LX/0Uo;LX/2hU;LX/0SG;LX/0Ot;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17W;LX/0Ot;LX/2hW;LX/2hX;LX/2dj;LX/2do;LX/2ha;LX/2hb;LX/2e3;LX/2dl;LX/2hc;LX/0iA;LX/0xB;LX/0Or;LX/0gh;LX/16I;LX/2hd;LX/2he;LX/0ad;LX/2hf;LX/0gy;LX/0gw;LX/1Ck;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friending/jewel/FriendRequestsFragment;",
            "LX/2hQ;",
            "LX/2hR;",
            "LX/2hS;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/2hT;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Uo;",
            "LX/2hU;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/EzP;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/17W;",
            "LX/0Ot",
            "<",
            "LX/83b;",
            ">;",
            "LX/2hW;",
            "LX/2hX;",
            "LX/2dj;",
            "LX/2do;",
            "LX/2ha;",
            "LX/2hb;",
            "LX/2e3;",
            "LX/2dl;",
            "LX/2hc;",
            "LX/0iA;",
            "LX/0xB;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0gh;",
            "LX/16I;",
            "LX/2hd;",
            "LX/2he;",
            "LX/0ad;",
            "LX/2hf;",
            "LX/0gy;",
            "LX/0gw;",
            "LX/1Ck;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113870
    iput-object p1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->i:LX/2hQ;

    iput-object p2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->j:LX/2hR;

    iput-object p3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->k:LX/2hS;

    iput-object p4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->l:LX/1Kt;

    iput-object p5, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->m:LX/2hT;

    iput-object p6, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->n:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p7, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->o:LX/0Sh;

    iput-object p8, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->p:LX/0Uo;

    iput-object p9, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->q:LX/2hU;

    iput-object p10, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->r:LX/0SG;

    iput-object p11, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->s:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->t:LX/03V;

    iput-object p13, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p14, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->v:LX/17W;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->w:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->x:LX/2hW;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->y:LX/2hX;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->B:LX/2ha;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->C:LX/2hb;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->E:LX/2dl;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->F:LX/2hc;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->G:LX/0iA;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->I:LX/0Or;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->J:LX/0gh;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->K:LX/16I;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->L:LX/2hd;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->N:LX/0ad;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->O:LX/2hf;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->P:LX/0gy;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Q:LX/0gw;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->S:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 41

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v39

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;

    const-class v3, LX/2hQ;

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2hQ;

    const-class v4, LX/2hR;

    move-object/from16 v0, v39

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2hR;

    const-class v5, LX/2hS;

    move-object/from16 v0, v39

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/2hS;

    invoke-static/range {v39 .. v39}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v6

    check-cast v6, LX/1Kt;

    const-class v7, LX/2hT;

    move-object/from16 v0, v39

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/2hT;

    invoke-static/range {v39 .. v39}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v8

    check-cast v8, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static/range {v39 .. v39}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v9

    check-cast v9, LX/0Sh;

    invoke-static/range {v39 .. v39}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v10

    check-cast v10, LX/0Uo;

    invoke-static/range {v39 .. v39}, LX/2hU;->a(LX/0QB;)LX/2hU;

    move-result-object v11

    check-cast v11, LX/2hU;

    invoke-static/range {v39 .. v39}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const/16 v13, 0x2257

    move-object/from16 v0, v39

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v39 .. v39}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {v39 .. v39}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v15

    check-cast v15, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v39 .. v39}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v16

    check-cast v16, LX/17W;

    const/16 v17, 0x226d

    move-object/from16 v0, v39

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v39 .. v39}, LX/2hW;->a(LX/0QB;)LX/2hW;

    move-result-object v18

    check-cast v18, LX/2hW;

    invoke-static/range {v39 .. v39}, LX/2hX;->a(LX/0QB;)LX/2hX;

    move-result-object v19

    check-cast v19, LX/2hX;

    invoke-static/range {v39 .. v39}, LX/2dj;->a(LX/0QB;)LX/2dj;

    move-result-object v20

    check-cast v20, LX/2dj;

    invoke-static/range {v39 .. v39}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v21

    check-cast v21, LX/2do;

    invoke-static/range {v39 .. v39}, LX/2ha;->a(LX/0QB;)LX/2ha;

    move-result-object v22

    check-cast v22, LX/2ha;

    invoke-static/range {v39 .. v39}, LX/2hb;->a(LX/0QB;)LX/2hb;

    move-result-object v23

    check-cast v23, LX/2hb;

    invoke-static/range {v39 .. v39}, LX/2e3;->a(LX/0QB;)LX/2e3;

    move-result-object v24

    check-cast v24, LX/2e3;

    invoke-static/range {v39 .. v39}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v25

    check-cast v25, LX/2dl;

    invoke-static/range {v39 .. v39}, LX/2hc;->a(LX/0QB;)LX/2hc;

    move-result-object v26

    check-cast v26, LX/2hc;

    invoke-static/range {v39 .. v39}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v27

    check-cast v27, LX/0iA;

    invoke-static/range {v39 .. v39}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v28

    check-cast v28, LX/0xB;

    const/16 v29, 0x15e7

    move-object/from16 v0, v39

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v29

    invoke-static/range {v39 .. v39}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v30

    check-cast v30, LX/0gh;

    invoke-static/range {v39 .. v39}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v31

    check-cast v31, LX/16I;

    invoke-static/range {v39 .. v39}, LX/2hd;->a(LX/0QB;)LX/2hd;

    move-result-object v32

    check-cast v32, LX/2hd;

    invoke-static/range {v39 .. v39}, LX/2he;->a(LX/0QB;)LX/2he;

    move-result-object v33

    check-cast v33, LX/2he;

    invoke-static/range {v39 .. v39}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v34

    check-cast v34, LX/0ad;

    invoke-static/range {v39 .. v39}, LX/2hf;->a(LX/0QB;)LX/2hf;

    move-result-object v35

    check-cast v35, LX/2hf;

    invoke-static/range {v39 .. v39}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v36

    check-cast v36, LX/0gy;

    invoke-static/range {v39 .. v39}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v37

    check-cast v37, LX/0gw;

    invoke-static/range {v39 .. v39}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v38

    check-cast v38, LX/1Ck;

    const/16 v40, 0x14ba

    invoke-static/range {v39 .. v40}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v39

    invoke-static/range {v2 .. v39}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->a(Lcom/facebook/friending/jewel/FriendRequestsFragment;LX/2hQ;LX/2hR;LX/2hS;LX/1Kt;LX/2hT;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Sh;LX/0Uo;LX/2hU;LX/0SG;LX/0Ot;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17W;LX/0Ot;LX/2hW;LX/2hX;LX/2dj;LX/2do;LX/2ha;LX/2hb;LX/2e3;LX/2dl;LX/2hc;LX/0iA;LX/0xB;LX/0Or;LX/0gh;LX/16I;LX/2hd;LX/2he;LX/0ad;LX/2hf;LX/0gy;LX/0gw;LX/1Ck;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 113871
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EzP;

    .line 113872
    instance-of v1, p1, Lcom/facebook/friends/model/FriendRequest;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/EzP;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113873
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->av:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v4}, LX/2dj;->h()LX/0am;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080f70

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/EzP;->a(Landroid/app/Activity;LX/2iL;Ljava/lang/String;LX/0am;LX/2h7;Ljava/lang/String;)V

    .line 113874
    :cond_0
    :goto_0
    return-void

    .line 113875
    :cond_1
    instance-of v1, p1, Lcom/facebook/friends/model/PersonYouMayKnow;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/EzP;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 113876
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->av:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v4}, LX/2dj;->i()LX/0am;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080f77

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual/range {v0 .. v7}, LX/EzP;->a(Landroid/app/Activity;LX/2iL;Ljava/lang/String;LX/0am;LX/2h7;Ljava/lang/String;I)V

    goto :goto_0

    .line 113877
    :cond_2
    instance-of v0, p1, LX/2lq;

    if-eqz v0, :cond_0

    .line 113878
    check-cast p1, LX/2lq;

    .line 113879
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 113880
    const-string v1, "timeline_friend_request_ref"

    sget-object v2, LX/5P2;->PYMK_JEWEL:LX/5P2;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 113881
    invoke-interface {p1}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 113882
    iget-wide v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->av:J

    .line 113883
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->v:LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 113884
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 2

    .prologue
    .line 113885
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ao:LX/2hs;

    new-instance v1, LX/Eyg;

    invoke-direct {v1, p0, p1}, LX/Eyg;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    invoke-virtual {v0, v1}, LX/2hs;->b(LX/0TF;)V

    .line 113886
    return-void
.end method

.method public static b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 113887
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113888
    :goto_0
    return-void

    .line 113889
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aQ:Z

    .line 113890
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Eyp;

    invoke-direct {v2, p0}, LX/Eyp;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 113891
    sget-object v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->T:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 2

    .prologue
    .line 113892
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ao:LX/2hs;

    new-instance v1, LX/Eyh;

    invoke-direct {v1, p0, p1}, LX/Eyh;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    invoke-virtual {v0, v1}, LX/2hs;->d(LX/0TF;)V

    .line 113893
    return-void
.end method

.method public static b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 113894
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 113895
    if-nez v0, :cond_1

    .line 113896
    :cond_0
    :goto_0
    return-void

    .line 113897
    :cond_1
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ag:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 113898
    if-eqz p1, :cond_0

    .line 113899
    if-eqz p2, :cond_3

    .line 113900
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ah:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 113901
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 113902
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 113903
    goto :goto_1

    .line 113904
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aQ:Z

    if-eqz v0, :cond_4

    .line 113905
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ah:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 113906
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 113907
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Eyo;

    invoke-direct {v2, p0}, LX/Eyo;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0

    .line 113908
    :cond_4
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ah:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 113909
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    goto :goto_0
.end method

.method private d(Z)V
    .locals 9

    .prologue
    .line 113910
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->e()Z

    move-result v3

    .line 113911
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->r:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 113912
    iget-object v6, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    const-string v7, "FETCH_FRIEND_REQUESTS_TASK"

    new-instance v8, LX/2il;

    invoke-direct {v8, p0}, LX/2il;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    new-instance v0, LX/2im;

    move-object v1, p0

    move v2, p1

    invoke-direct/range {v0 .. v5}, LX/2im;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZJ)V

    invoke-virtual {v6, v7, v8, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 113913
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->a()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 113914
    return-void
.end method

.method public static e(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 1

    .prologue
    .line 113915
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aI:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->l()I

    move-result v0

    .line 113916
    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113917
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 113918
    :cond_1
    return-void

    .line 113919
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->k()I

    move-result v0

    goto :goto_0
.end method

.method public static f$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 113920
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aD:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aM:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113921
    :goto_0
    return-void

    .line 113922
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->r:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ay:Ljava/lang/String;

    .line 113923
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ay:Ljava/lang/String;

    .line 113924
    iget-object v3, v0, LX/2iK;->d:LX/2iS;

    .line 113925
    iput-object v1, v3, LX/2iS;->o:Ljava/lang/String;

    .line 113926
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113927
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 113928
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aQ:Z

    .line 113929
    iput-boolean v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aR:Z

    .line 113930
    iput-boolean v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aM:Z

    .line 113931
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->j()V

    .line 113932
    invoke-direct {p0, p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->d(Z)V

    goto :goto_0
.end method

.method public static g$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 3

    .prologue
    .line 113933
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->p:LX/0Uo;

    if-nez v0, :cond_1

    .line 113934
    :cond_0
    :goto_0
    return-void

    .line 113935
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->az:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    .line 113936
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aP:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->p:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113937
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_0

    .line 113938
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->az:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 113939
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->o:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->az:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Eyq;

    invoke-direct {v2, p0}, LX/Eyq;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 113940
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    sget-object v1, LX/2h7;->JEWEL:LX/2h7;

    if-eq v0, v1, :cond_1

    .line 113941
    :cond_0
    :goto_0
    return-void

    .line 113942
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->G:LX/0iA;

    sget-object v1, LX/77s;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/13D;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13D;

    .line 113943
    if-eqz v0, :cond_0

    .line 113944
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 113945
    if-eqz v1, :cond_0

    .line 113946
    invoke-virtual {v0}, LX/13D;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/1XZ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 113947
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "qp_definition"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 113948
    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113949
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->o:LX/0Sh;

    new-instance v2, Lcom/facebook/friending/jewel/FriendRequestsFragment$31;

    invoke-direct {v2, p0, v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment$31;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 113950
    goto :goto_0

    .line 113951
    :cond_2
    invoke-virtual {v0}, LX/13D;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "1820"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113952
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->as:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 113953
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113954
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113955
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->r()V

    goto :goto_0

    .line 113956
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->O:LX/2hf;

    invoke-virtual {v0, v1}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v0

    .line 113957
    instance-of v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v1, :cond_4

    .line 113958
    check-cast v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 113959
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 113960
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 113961
    const-string v1, "ACTION_BUTTON_THEME_ARG"

    sget-object v2, LX/77w;->SPECIAL:LX/77w;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 113962
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1295

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 113963
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->p()V

    .line 113964
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->as:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 113965
    :cond_4
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->n()V

    goto/16 :goto_0
.end method

.method public static m(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 113966
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    sget-object v1, LX/2h7;->JEWEL:LX/2h7;

    if-eq v0, v1, :cond_1

    .line 113967
    :cond_0
    :goto_0
    return-void

    .line 113968
    :cond_1
    const/4 v0, 0x0

    .line 113969
    sget-object v1, LX/787;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/787;

    .line 113970
    iget-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->G:LX/0iA;

    invoke-virtual {v4, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v4

    check-cast v4, LX/13D;

    .line 113971
    if-nez v4, :cond_4

    const/4 v4, 0x0

    :goto_1
    move-object v1, v4

    .line 113972
    if-eqz v1, :cond_3

    .line 113973
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113974
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->O:LX/2hf;

    invoke-virtual {v0, v1}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v0

    .line 113975
    :cond_3
    instance-of v1, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;

    if-eqz v1, :cond_0

    .line 113976
    check-cast v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 113977
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->p()V

    .line 113978
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->as:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 113979
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f04004e

    invoke-virtual {v0, v1, v3}, LX/0hH;->a(II)LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1295

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_1
.end method

.method private n()V
    .locals 2

    .prologue
    .line 113980
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    instance-of v0, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionToastFooterFragment;

    if-nez v0, :cond_0

    .line 113981
    invoke-static {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->o(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 113982
    :goto_0
    return-void

    .line 113983
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04004f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 113984
    new-instance v1, LX/Eyf;

    invoke-direct {v1, p0}, LX/Eyf;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 113985
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->as:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static o(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 2

    .prologue
    .line 113986
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    if-eqz v0, :cond_0

    .line 113987
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 113988
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 113989
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->as:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 113990
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->p()V

    .line 113991
    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    .line 113992
    const-class v0, LX/0fH;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fH;

    .line 113993
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 113994
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 113995
    new-instance v2, Lcom/facebook/friending/jewel/FriendRequestsFragment$14;

    invoke-direct {v2, p0, v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment$14;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;LX/0fH;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 113996
    :cond_0
    return-void
.end method

.method public static r(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 113795
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aM:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113796
    :cond_0
    :goto_0
    return-void

    .line 113797
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113798
    invoke-direct {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->d(Z)V

    goto :goto_0

    .line 113799
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 113800
    if-eqz v0, :cond_b

    .line 113801
    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 113802
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aK:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_5

    .line 113803
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113804
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aI:Z

    if-eqz v0, :cond_4

    .line 113805
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->t()V

    goto :goto_0

    .line 113806
    :cond_4
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->s()V

    goto :goto_0

    .line 113807
    :cond_5
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113808
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->d()Z

    move-result v2

    .line 113809
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aI:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->l()I

    move-result v0

    .line 113810
    :goto_2
    const/16 v3, 0x64

    if-lt v0, v3, :cond_6

    const/4 v1, 0x1

    .line 113811
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aR:Z

    if-eqz v0, :cond_9

    if-eqz v2, :cond_7

    if-eqz v1, :cond_9

    .line 113812
    :cond_7
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aR:Z

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 113813
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aR:Z

    .line 113814
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    const-string v1, "FETCH_CONTACTS_SECTION_TASK"

    new-instance v2, LX/Eym;

    invoke-direct {v2, p0}, LX/Eym;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    new-instance v3, LX/Eyn;

    invoke-direct {v3, p0}, LX/Eyn;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 113815
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->a()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 113816
    goto/16 :goto_0

    .line 113817
    :cond_8
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->k()I

    move-result v0

    goto :goto_2

    .line 113818
    :cond_9
    if-eqz v2, :cond_0

    .line 113819
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aI:Z

    if-eqz v0, :cond_a

    .line 113820
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->t()V

    goto/16 :goto_0

    .line 113821
    :cond_a
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->s()V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private s()V
    .locals 5

    .prologue
    .line 113822
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->g()Z

    move-result v0

    .line 113823
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    const-string v2, "FETCH_PYMK_TASK"

    new-instance v3, LX/3Br;

    invoke-direct {v3, p0, v0}, LX/3Br;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    new-instance v4, LX/3Bs;

    invoke-direct {v4, p0, v0}, LX/3Bs;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 113824
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->a()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 113825
    return-void
.end method

.method private t()V
    .locals 5

    .prologue
    .line 113541
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->g()Z

    move-result v0

    .line 113542
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    const-string v2, "FETCH_PYMI_AND_PYMK_TASK"

    new-instance v3, LX/Eyk;

    invoke-direct {v3, p0, v0}, LX/Eyk;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    new-instance v4, LX/Eyl;

    invoke-direct {v4, p0, v0}, LX/Eyl;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 113543
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->a()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 113544
    return-void
.end method

.method public static v(Lcom/facebook/friending/jewel/FriendRequestsFragment;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/2no;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 113545
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    invoke-virtual {v1}, LX/2e3;->c()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    iget-object v2, v2, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    sget-object v3, Lcom/facebook/friending/jewel/FriendRequestsFragment;->U:Lcom/facebook/common/callercontext/CallerContext;

    .line 113546
    iget-object v4, v0, LX/2dj;->k:LX/2dk;

    invoke-static {v0}, LX/2dj;->n(LX/2dj;)Ljava/lang/String;

    move-result-object v5

    .line 113547
    const/4 v10, 0x1

    .line 113548
    if-lez v1, :cond_0

    move v9, v10

    :goto_0
    invoke-static {v9}, LX/0PB;->checkArgument(Z)V

    .line 113549
    new-instance v9, LX/84w;

    invoke-direct {v9}, LX/84w;-><init>()V

    move-object v9, v9

    .line 113550
    const-string v11, "after_param"

    invoke-virtual {v9, v11, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    const-string v12, "first_param"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    const-string v12, "location"

    iget-object v13, v2, LX/2hC;->value:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    .line 113551
    iput-boolean v10, v11, LX/0gW;->l:Z

    .line 113552
    invoke-static {v9}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v9

    sget-object v11, LX/0zS;->a:LX/0zS;

    invoke-virtual {v9, v11}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v9

    const-string v11, "REQUESTS_TAB_PYMI_QUERY_TAG"

    invoke-static {v11}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v11

    .line 113553
    iput-object v11, v9, LX/0zO;->d:Ljava/util/Set;

    .line 113554
    move-object v9, v9

    .line 113555
    iput-boolean v10, v9, LX/0zO;->p:Z

    .line 113556
    move-object v9, v9

    .line 113557
    sget-object v10, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v9, v10}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v9

    .line 113558
    iput-object v3, v9, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 113559
    move-object v9, v9

    .line 113560
    const-wide/16 v11, 0xe10

    invoke-virtual {v9, v11, v12}, LX/0zO;->a(J)LX/0zO;

    move-result-object v9

    move-object v6, v9

    .line 113561
    iget-object v7, v4, LX/2dk;->c:LX/2dl;

    const-string v8, "REQUESTS_TAB_PYMI_QUERY_TAG"

    invoke-virtual {v7, v6, v8}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 113562
    new-instance v7, LX/843;

    invoke-direct {v7, v4}, LX/843;-><init>(LX/2dk;)V

    move-object v7, v7

    .line 113563
    iget-object v8, v4, LX/2dk;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v4, v6

    .line 113564
    new-instance v5, LX/840;

    invoke-direct {v5, v0}, LX/840;-><init>(LX/2dj;)V

    iget-object v6, v0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 113565
    move-object v0, v4

    .line 113566
    return-object v0

    .line 113567
    :cond_0
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public static w(Lcom/facebook/friending/jewel/FriendRequestsFragment;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 113568
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    invoke-virtual {v1}, LX/2e3;->c()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    iget-object v2, v2, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    sget-object v3, Lcom/facebook/friending/jewel/FriendRequestsFragment;->U:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/2dj;->b(ILX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static z(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 2

    .prologue
    .line 113569
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, LX/8DG;

    if-eqz v0, :cond_0

    .line 113570
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/8DG;

    const-string v1, "people_you_may_know"

    invoke-interface {v0, v1}, LX/8DG;->c(Ljava/lang/String;)V

    .line 113571
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113572
    sget-object v0, Lcom/facebook/friending/tab/FriendRequestsTab;->l:Lcom/facebook/friending/tab/FriendRequestsTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113573
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->S:LX/0Or;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->S:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 113574
    :goto_0
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    if-eqz v3, :cond_1

    move v3, v1

    .line 113575
    :goto_1
    if-eqz v0, :cond_2

    if-nez v3, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    .line 113576
    goto :goto_0

    :cond_1
    move v3, v2

    .line 113577
    goto :goto_1

    :cond_2
    move v1, v2

    .line 113578
    goto :goto_2
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 113579
    if-eqz p1, :cond_0

    .line 113580
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->n()V

    .line 113581
    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 113582
    const v0, 0x7f080faa

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 113583
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ad:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 113584
    new-instance v0, LX/Eye;

    invoke-direct {v0, p0}, LX/Eye;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ad:Landroid/view/View$OnClickListener;

    .line 113585
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ad:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 113586
    invoke-virtual {p0}, Landroid/support/v4/app/ListFragment;->ld_()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 113587
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 113588
    invoke-virtual {p0}, Landroid/support/v4/app/ListFragment;->ld_()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 113589
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x74fb73c5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 113590
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 113591
    iput-boolean v7, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ax:Z

    .line 113592
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    if-nez v1, :cond_0

    .line 113593
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->i:LX/2hQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    .line 113594
    iget-boolean v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aI:Z

    if-eqz v4, :cond_3

    .line 113595
    new-instance v4, LX/86b;

    iget-object v5, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    invoke-virtual {v5}, LX/2e3;->c()I

    move-result v5

    iget-object v8, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->N:LX/0ad;

    sget v9, LX/2ez;->g:I

    const/4 v10, 0x5

    invoke-interface {v8, v9, v10}, LX/0ad;->a(II)I

    move-result v8

    iget-object v9, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->N:LX/0ad;

    sget v10, LX/2ez;->f:I

    const/16 p1, 0xa

    invoke-interface {v9, v10, p1}, LX/0ad;->a(II)I

    move-result v9

    invoke-direct {v4, v5, v8, v9}, LX/86b;-><init>(III)V

    .line 113596
    :goto_0
    move-object v4, v4

    .line 113597
    iget-object v5, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/2hQ;->a(Landroid/content/Context;LX/2h7;LX/86b;LX/1Ck;)LX/2iK;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 113598
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-boolean v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aK:Z

    .line 113599
    iput-boolean v2, v1, LX/2iK;->r:Z

    .line 113600
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-boolean v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aH:Z

    .line 113601
    iput-boolean v2, v1, LX/2iK;->s:Z

    .line 113602
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-boolean v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aG:Z

    .line 113603
    iput-boolean v2, v1, LX/2iK;->t:Z

    .line 113604
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 113605
    iget-object v2, v1, LX/2iK;->d:LX/2iS;

    invoke-virtual {v2}, LX/2iS;->a()V

    .line 113606
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->l:LX/1Kt;

    new-instance v2, LX/2ic;

    invoke-direct {v2, p0}, LX/2ic;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v1, v2}, LX/1Kt;->a(LX/1Ce;)V

    .line 113607
    new-instance v1, LX/2id;

    invoke-direct {v1, p0}, LX/2id;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    move-object v1, v1

    .line 113608
    iput-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ar:LX/0fx;

    .line 113609
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-interface {v1, v2}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 113610
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->m:LX/2hT;

    sget-object v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->U:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-object v5, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    const/4 v8, 0x3

    .line 113611
    iget-boolean v9, v5, LX/2e3;->c:Z

    if-nez v9, :cond_4

    .line 113612
    :goto_1
    move v5, v8

    .line 113613
    new-instance v8, LX/2if;

    invoke-static {v1}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v9

    check-cast v9, LX/1HI;

    move-object v10, v2

    move-object v11, v3

    move-object v12, v4

    move v13, v5

    invoke-direct/range {v8 .. v13}, LX/2if;-><init>(LX/1HI;Lcom/facebook/common/callercontext/CallerContext;LX/0g8;LX/2iK;I)V

    .line 113614
    move-object v1, v8

    .line 113615
    iput-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->am:LX/2if;

    .line 113616
    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aF:Z

    if-eqz v1, :cond_1

    .line 113617
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    .line 113618
    new-instance v3, LX/2ih;

    invoke-direct {v3, p0, v2}, LX/2ih;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;LX/0g8;)V

    move-object v2, v3

    .line 113619
    invoke-interface {v1, v2}, LX/0g8;->a(LX/2ii;)V

    .line 113620
    :goto_2
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-interface {v1}, LX/0g8;->c()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 113621
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-interface {v1}, LX/0g8;->c()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 113622
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-interface {v1, v6}, LX/0g8;->b(I)V

    .line 113623
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ar:LX/0fx;

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 113624
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->l:LX/1Kt;

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 113625
    new-instance v1, LX/2ik;

    invoke-direct {v1, p0}, LX/2ik;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    iput-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aq:LX/0xA;

    .line 113626
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aq:LX/0xA;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/0xA;)V

    .line 113627
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    sget-object v2, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    iput v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aA:I

    .line 113628
    iget v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aA:I

    if-lez v1, :cond_2

    .line 113629
    invoke-static {p0, v6}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->a$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    .line 113630
    :goto_3
    const v1, 0x71377bbf

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 113631
    :cond_1
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 113632
    iput-boolean v7, v1, LX/2iK;->u:Z

    .line 113633
    goto :goto_2

    .line 113634
    :cond_2
    invoke-static {p0, v6}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->f$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 113635
    :cond_4
    sget-object v9, LX/33c;->a:[I

    iget-object v10, v5, LX/2e3;->a:LX/0oz;

    invoke-virtual {v10}, LX/0oz;->c()LX/0p3;

    move-result-object v10

    invoke-virtual {v10}, LX/0p3;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    goto/16 :goto_1

    .line 113636
    :pswitch_0
    iget-object v9, v5, LX/2e3;->b:LX/0ad;

    sget v10, LX/2e4;->r:I

    invoke-interface {v9, v10, v8}, LX/0ad;->a(II)I

    move-result v8

    goto/16 :goto_1

    .line 113637
    :pswitch_1
    iget-object v9, v5, LX/2e3;->b:LX/0ad;

    sget v10, LX/2e4;->o:I

    invoke-interface {v9, v10, v8}, LX/0ad;->a(II)I

    move-result v8

    goto/16 :goto_1

    .line 113638
    :pswitch_2
    iget-object v9, v5, LX/2e3;->b:LX/0ad;

    sget v10, LX/2e4;->l:I

    invoke-interface {v9, v10, v8}, LX/0ad;->a(II)I

    move-result v8

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113639
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 113640
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aB:Z

    if-eqz v0, :cond_1

    .line 113641
    :cond_0
    :goto_0
    return-void

    .line 113642
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 113643
    :pswitch_0
    const/4 v0, 0x0

    .line 113644
    if-eqz p3, :cond_2

    .line 113645
    const-string v0, "entity_cards_visible_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113646
    :cond_2
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 113647
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 113648
    iget-object v0, v1, LX/2iK;->l:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 113649
    :goto_1
    goto :goto_0

    .line 113650
    :cond_3
    const v0, 0x347acd15

    invoke-static {v1, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 113651
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 113652
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D()V

    .line 113653
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-static {v0}, LX/62G;->a(LX/0g8;)V

    .line 113654
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x2a

    const v4, -0x2a3921ba

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 113655
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 113656
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 113657
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v0

    .line 113658
    const-string v0, "friending_location"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "friending_location"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/2h7;

    :goto_0
    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    .line 113659
    const-string v0, "open_timeline_on_click"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aF:Z

    .line 113660
    const-string v0, "show_no_requests"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aH:Z

    .line 113661
    const-string v0, "show_friend_requests_header"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aG:Z

    .line 113662
    const-string v0, "fetch_on_visible"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aD:Z

    .line 113663
    const-string v0, "mark_nux_step_complete_on_empty_content"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aE:Z

    .line 113664
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->N:LX/0ad;

    sget-short v4, LX/2ez;->m:S

    invoke-interface {v0, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aB:Z

    .line 113665
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->B:LX/2ha;

    iget-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    .line 113666
    iput-object v4, v0, LX/2ha;->b:LX/2h7;

    .line 113667
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    sget-object v4, LX/2h7;->JEWEL:LX/2h7;

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aK:Z

    .line 113668
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    sget-object v4, LX/2h7;->JEWEL:LX/2h7;

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->N:LX/0ad;

    sget-short v4, LX/2ez;->i:S

    invoke-interface {v0, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aI:Z

    .line 113669
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aI:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->N:LX/0ad;

    sget-short v4, LX/2ez;->j:S

    invoke-interface {v0, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aJ:Z

    .line 113670
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->j:LX/2hR;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 113671
    const-class v4, LX/0fH;

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbListFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0fH;

    .line 113672
    if-eqz v4, :cond_5

    invoke-interface {v4}, LX/0fH;->iC_()LX/0gc;

    move-result-object v4

    :goto_3
    move-object v4, v4

    .line 113673
    new-instance v5, LX/2hl;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    const/16 v8, 0x12cb

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    move-object v9, v2

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, LX/2hl;-><init>(LX/17W;LX/0ad;LX/0Ot;Landroid/content/Context;LX/0gc;)V

    .line 113674
    move-object v0, v5

    .line 113675
    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->an:LX/2hl;

    .line 113676
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->k:LX/2hS;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v0, v2}, LX/2hS;->a(LX/1Ck;)LX/2hs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ao:LX/2hs;

    .line 113677
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aw:Z

    .line 113678
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aL:Z

    if-nez v0, :cond_1

    .line 113679
    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aL:Z

    .line 113680
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    invoke-virtual {v0, v1}, LX/2he;->a(LX/2h7;)V

    .line 113681
    :cond_1
    const v0, -0x6af97762

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    .line 113682
    :cond_2
    sget-object v0, LX/2h7;->JEWEL:LX/2h7;

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 113683
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 113684
    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbListFragment;->iC_()LX/0gc;

    move-result-object v4

    goto :goto_3
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x16018aac

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 113685
    const v0, 0x7f0306ee

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 113686
    const v0, 0x7f0d0595

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 113687
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aB:Z

    if-nez v0, :cond_0

    .line 113688
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->W:LX/2Ip;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 113689
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ab:LX/2hO;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 113690
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->X:LX/2hG;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 113691
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Y:LX/2hI;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 113692
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Z:LX/2hK;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 113693
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aa:LX/2hM;

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 113694
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 113695
    if-eqz v0, :cond_1

    .line 113696
    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ac:LX/0dN;

    invoke-interface {v3, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 113697
    :cond_1
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->y:LX/2hX;

    .line 113698
    iput-boolean v5, v0, LX/2hY;->d:Z

    .line 113699
    const/16 v0, 0x2b

    const v3, -0x7da7ae32

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x68128603

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 113700
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    .line 113701
    iget-object v2, v1, LX/2dj;->k:LX/2dk;

    .line 113702
    iget-object v1, v2, LX/2dk;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 113703
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroy()V

    .line 113704
    const/16 v1, 0x2b

    const v2, 0x2a968e43

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x33c81ef1

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 113705
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->R:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 113706
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ax:Z

    .line 113707
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aq:LX/0xA;

    if-eqz v0, :cond_0

    .line 113708
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aq:LX/0xA;

    invoke-virtual {v0, v2}, LX/0xB;->b(LX/0xA;)V

    .line 113709
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ap:LX/0Yb;

    if-eqz v0, :cond_1

    .line 113710
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ap:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 113711
    iput-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ap:LX/0Yb;

    .line 113712
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aB:Z

    if-nez v0, :cond_2

    .line 113713
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->W:LX/2Ip;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 113714
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ab:LX/2hO;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 113715
    :cond_2
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->X:LX/2hG;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 113716
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Y:LX/2hI;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 113717
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->Z:LX/2hK;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 113718
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aa:LX/2hM;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 113719
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 113720
    if-eqz v0, :cond_3

    .line 113721
    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ac:LX/0dN;

    invoke-interface {v2, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 113722
    :cond_3
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 113723
    iget-object v2, v0, LX/2iK;->d:LX/2iS;

    invoke-virtual {v2}, LX/2iS;->b()V

    .line 113724
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->y:LX/2hX;

    const/4 v2, 0x1

    .line 113725
    iput-boolean v2, v0, LX/2hY;->d:Z

    .line 113726
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->B:LX/2ha;

    .line 113727
    const/4 v2, 0x0

    iput-object v2, v0, LX/2ha;->b:LX/2h7;

    .line 113728
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->am:LX/2if;

    if-eqz v0, :cond_4

    .line 113729
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->am:LX/2if;

    invoke-virtual {v0}, LX/1Yh;->a()V

    .line 113730
    iput-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->am:LX/2if;

    .line 113731
    :cond_4
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ah:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113732
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e()V

    .line 113733
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-interface {v0, v4}, LX/0g8;->a(LX/2ii;)V

    .line 113734
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ar:LX/0fx;

    invoke-interface {v0, v2}, LX/0g8;->c(LX/0fx;)V

    .line 113735
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->l:LX/1Kt;

    invoke-interface {v0, v2}, LX/0g8;->c(LX/0fx;)V

    .line 113736
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_5

    .line 113737
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 113738
    iput-object v4, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 113739
    :cond_5
    const-class v0, LX/0fH;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fH;

    .line 113740
    if-eqz v0, :cond_6

    .line 113741
    invoke-interface {v0, p0}, LX/0fH;->b(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 113742
    :cond_6
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroyView()V

    .line 113743
    const/16 v0, 0x2b

    const v2, -0x5322fe2e

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6945cbf6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 113744
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    invoke-virtual {v1, v2}, LX/2he;->b(LX/2h7;)V

    .line 113745
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onPause()V

    .line 113746
    const/16 v1, 0x2b

    const v2, 0x3ec7bb70

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3d30cdd3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 113747
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onResume()V

    .line 113748
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113749
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->l()V

    .line 113750
    :cond_0
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A()V

    .line 113751
    const/16 v1, 0x2b

    const v2, -0x7856b09

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7c3b648

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 113752
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onStart()V

    .line 113753
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 113754
    if-eqz v0, :cond_0

    .line 113755
    sget v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->V:I

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 113756
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 113757
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x4f548b27    # 3.56588928E9f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 113758
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 113759
    const-string v1, "jewel_popup_friend_requests"

    invoke-static {p1, v1, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 113760
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ag:Landroid/view/View;

    .line 113761
    const v0, 0x7f0d128f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ah:Lcom/facebook/resources/ui/FbTextView;

    .line 113762
    const v0, 0x7f0d1290

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ai:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 113763
    const v0, 0x7f0d1295

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->as:Landroid/view/View;

    .line 113764
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 113765
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0101e2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 113766
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ag:Landroid/view/View;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 113767
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, LX/2iG;

    invoke-direct {v1, p0}, LX/2iG;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 113768
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ah:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/2iH;

    invoke-direct {v1, p0}, LX/2iH;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113769
    new-instance v1, LX/2iI;

    invoke-virtual {p0}, Landroid/support/v4/app/ListFragment;->ld_()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    .line 113770
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    invoke-interface {v0}, LX/0g8;->k()V

    .line 113771
    sget-object v0, LX/2iJ;->BIG:LX/2iJ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iJ;->getFullSize(Landroid/content/res/Resources;)I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0d1c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v1, v0

    .line 113772
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0306eb

    invoke-virtual {p0}, Landroid/support/v4/app/ListFragment;->ld_()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 113773
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setPadding(IIII)V

    .line 113774
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ak:LX/0g8;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 113775
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->K:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v2, Lcom/facebook/friending/jewel/FriendRequestsFragment$27;

    invoke-direct {v2, p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment$27;-><init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ap:LX/0Yb;

    .line 113776
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D()V

    .line 113777
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 6

    .prologue
    .line 113778
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 113779
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->setUserVisibleHint(Z)V

    .line 113780
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 113781
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->l()V

    .line 113782
    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ax:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aM:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aD:Z

    if-eqz v1, :cond_0

    .line 113783
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->f$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    .line 113784
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aw:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aL:Z

    if-nez v1, :cond_1

    .line 113785
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aL:Z

    .line 113786
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    invoke-virtual {v1, v2}, LX/2he;->a(LX/2h7;)V

    .line 113787
    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aN:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aL:Z

    if-eqz v1, :cond_2

    .line 113788
    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    iget-object v2, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, LX/2he;->a(LX/2h7;J)V

    .line 113789
    :cond_2
    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    iget-boolean v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aL:Z

    if-eqz v0, :cond_3

    .line 113790
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    iget-object v1, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    invoke-virtual {v0, v1}, LX/2he;->b(LX/2h7;)V

    .line 113791
    :cond_3
    invoke-direct {p0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->A()V

    .line 113792
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->af:LX/4nS;

    if-eqz v0, :cond_4

    .line 113793
    iget-object v0, p0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->af:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 113794
    :cond_4
    return-void
.end method
