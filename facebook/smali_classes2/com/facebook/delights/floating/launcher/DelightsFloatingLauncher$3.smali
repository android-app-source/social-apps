.class public final Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/20s;


# direct methods
.method public constructor <init>(LX/20s;)V
    .locals 0

    .prologue
    .line 339430
    iput-object p1, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 339431
    iget-object v0, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v0, v0, LX/20s;->f:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    .line 339432
    :goto_0
    return-void

    .line 339433
    :cond_0
    iget-object v0, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v0, v0, LX/20s;->d:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    .line 339434
    iget-object v1, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v1, v1, LX/20s;->d:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    .line 339435
    iget-object v2, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v2, v2, LX/20s;->i:LX/3K3;

    .line 339436
    iget-object v4, v2, LX/3K3;->e:[F

    move-object v2, v4

    .line 339437
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 339438
    int-to-float v0, v0

    aget v1, v2, v3

    div-float/2addr v0, v1

    .line 339439
    const/4 v1, 0x1

    aget v1, v2, v1

    mul-float/2addr v0, v1

    .line 339440
    iget-object v1, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v1, v1, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float v0, v1, v0

    .line 339441
    iget-object v1, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v1, v1, LX/20s;->j:Landroid/widget/ImageView;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v2, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget v2, v2, LX/20s;->k:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v3, v0, v3, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 339442
    iget-object v0, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v0, v0, LX/20s;->m:LX/9Ug;

    invoke-virtual {v0}, LX/9Ug;->a()V

    .line 339443
    iget-object v0, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v0, v0, LX/20s;->m:LX/9Ug;

    invoke-virtual {v0}, LX/9Ug;->c()V

    .line 339444
    iget-object v0, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v0, v0, LX/20s;->m:LX/9Ug;

    iget-object v1, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v1, v1, LX/20s;->o:LX/20u;

    invoke-virtual {v0, v1}, LX/9Ug;->a(LX/20u;)V

    .line 339445
    iget-object v0, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v0, v0, LX/20s;->i:LX/3K3;

    .line 339446
    iget v1, v0, LX/3K3;->b:I

    move v0, v1

    .line 339447
    iget-object v1, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v1, v1, LX/20s;->i:LX/3K3;

    .line 339448
    iget v2, v1, LX/3K3;->a:I

    move v1, v2

    .line 339449
    div-int/2addr v0, v1

    mul-int/lit16 v0, v0, 0x3e8

    .line 339450
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iget-object v2, p0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;->a:LX/20s;

    iget-object v2, v2, LX/20s;->n:Ljava/lang/Runnable;

    int-to-long v4, v0

    const v0, 0x41596a71

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
