.class public Lcom/facebook/delights/floating/DelightsFireworks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field private static h:LX/0Xm;


# instance fields
.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/1Ac;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/1Ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210742
    const-class v0, Lcom/facebook/delights/floating/DelightsFireworks;

    sput-object v0, Lcom/facebook/delights/floating/DelightsFireworks;->a:Ljava/lang/Class;

    .line 210743
    const-string v0, "http://www.facebook.com/mobileassets/nye_2016_animation"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/delights/floating/DelightsFireworks;->b:Landroid/net/Uri;

    .line 210744
    const-string v0, "http://www.facebook.com/mobileassets/nye_2016_comment_animation"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/delights/floating/DelightsFireworks;->c:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 210745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210746
    return-void
.end method

.method private static a(Landroid/net/Uri;)LX/1bf;
    .locals 2

    .prologue
    .line 210747
    invoke-static {}, LX/4eB;->newBuilder()LX/4eC;

    move-result-object v0

    invoke-virtual {v0}, LX/4eC;->j()LX/4eC;

    move-result-object v0

    invoke-virtual {v0}, LX/4eC;->l()LX/4eB;

    move-result-object v0

    .line 210748
    invoke-static {p0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    .line 210749
    iput-object v0, v1, LX/1bX;->e:LX/1bZ;

    .line 210750
    move-object v0, v1

    .line 210751
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/delights/floating/DelightsFireworks;
    .locals 7

    .prologue
    .line 210752
    const-class v1, Lcom/facebook/delights/floating/DelightsFireworks;

    monitor-enter v1

    .line 210753
    :try_start_0
    sget-object v0, Lcom/facebook/delights/floating/DelightsFireworks;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 210754
    sput-object v2, Lcom/facebook/delights/floating/DelightsFireworks;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 210755
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210756
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 210757
    new-instance v6, Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-direct {v6}, Lcom/facebook/delights/floating/DelightsFireworks;-><init>()V

    .line 210758
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Ac;->a(LX/0QB;)LX/1Ac;

    move-result-object v4

    check-cast v4, LX/1Ac;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    const/16 p0, 0x3567

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 210759
    iput-object v3, v6, Lcom/facebook/delights/floating/DelightsFireworks;->d:Landroid/content/Context;

    iput-object v4, v6, Lcom/facebook/delights/floating/DelightsFireworks;->e:LX/1Ac;

    iput-object v5, v6, Lcom/facebook/delights/floating/DelightsFireworks;->f:LX/1Ad;

    iput-object p0, v6, Lcom/facebook/delights/floating/DelightsFireworks;->g:LX/0Ot;

    .line 210760
    move-object v0, v6

    .line 210761
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 210762
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/delights/floating/DelightsFireworks;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210763
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 210764
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/net/Uri;Landroid/content/Context;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 3

    .prologue
    .line 210765
    iget-object v0, p0, Lcom/facebook/delights/floating/DelightsFireworks;->f:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->o()LX/1Ad;

    move-result-object v0

    invoke-static {p2}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/delights/floating/DelightsFireworks;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 210766
    new-instance v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 210767
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 210768
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v2, LX/1Up;->d:LX/1Up;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/1Up;)V

    .line 210769
    return-object v1
.end method

.method public static b(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 210770
    const-class v0, Lcom/facebook/delights/floating/DelightsFireworks;

    const-string v1, "delights_fireworks"

    const-string v2, "animation_prefetch"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 210771
    invoke-static {}, LX/4AN;->b()LX/1HI;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/delights/floating/DelightsFireworks;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    sget-object v3, LX/1bc;->LOW:LX/1bc;

    invoke-virtual {v1, v2, v0, v3}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;LX/1bc;)LX/1ca;

    .line 210772
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 210773
    iget-object v0, p0, Lcom/facebook/delights/floating/DelightsFireworks;->e:LX/1Ac;

    sget-object v1, Lcom/facebook/delights/floating/DelightsFireworks;->b:Landroid/net/Uri;

    invoke-direct {p0, v1, p1}, Lcom/facebook/delights/floating/DelightsFireworks;->a(Landroid/net/Uri;Landroid/content/Context;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, LX/1Ac;->a(Landroid/view/View;J)V

    .line 210774
    iget-object v0, p0, Lcom/facebook/delights/floating/DelightsFireworks;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "delights_fireworks_multiple"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 210775
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 210776
    iget-object v0, p0, Lcom/facebook/delights/floating/DelightsFireworks;->e:LX/1Ac;

    sget-object v1, Lcom/facebook/delights/floating/DelightsFireworks;->c:Landroid/net/Uri;

    invoke-direct {p0, v1, p1}, Lcom/facebook/delights/floating/DelightsFireworks;->a(Landroid/net/Uri;Landroid/content/Context;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, LX/1Ac;->a(Landroid/view/View;J)V

    .line 210777
    iget-object v0, p0, Lcom/facebook/delights/floating/DelightsFireworks;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "delights_fireworks_single"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 210778
    return-void
.end method
