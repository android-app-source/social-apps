.class public Lcom/facebook/device/resourcemonitor/DataUsageBytes;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final a:Lcom/facebook/device/resourcemonitor/DataUsageBytes;


# instance fields
.field public b:J

.field public c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 147255
    new-instance v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    invoke-direct {v0, v2, v3, v2, v3}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;-><init>(JJ)V

    sput-object v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->a:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 147267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147268
    iput-wide p1, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    .line 147269
    iput-wide p3, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    .line 147270
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 147266
    iget-wide v0, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    return-wide v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 147265
    iget-wide v0, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    return-wide v0
.end method

.method public final b(Lcom/facebook/device/resourcemonitor/DataUsageBytes;)Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    .locals 10

    .prologue
    .line 147260
    new-instance v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    iget-wide v2, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    .line 147261
    iget-wide v8, p1, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v4, v8

    .line 147262
    add-long/2addr v2, v4

    iget-wide v4, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    .line 147263
    iget-wide v8, p1, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v6, v8

    .line 147264
    add-long/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;-><init>(JJ)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 147259
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 147256
    iget-wide v0, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 147257
    iget-wide v0, p0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 147258
    return-void
.end method
