.class public Lcom/facebook/device_id/DefaultPhoneIdProvider;
.super LX/0Ou;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27v;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/phoneid/PhoneIdSoftErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54426
    const-class v0, Lcom/facebook/device_id/DefaultPhoneIdProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54427
    invoke-direct {p0}, LX/0Ou;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/device_id/DefaultPhoneIdProvider;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/DefaultPhoneIdProvider;",
            "LX/0Ot",
            "<",
            "LX/27v;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/phoneid/PhoneIdSoftErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54417
    iput-object p1, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->b:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;

    const/16 v1, 0x4c6

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x4c5

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a(Lcom/facebook/device_id/DefaultPhoneIdProvider;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a()LX/282;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 54423
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54424
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->g()V

    .line 54425
    :cond_0
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->b()LX/282;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/4hA;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 54428
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54429
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->g()V

    .line 54430
    :cond_0
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->d()LX/4hA;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/27z;
    .locals 1

    .prologue
    .line 54422
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27z;

    return-object v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 54420
    invoke-virtual {p0}, Lcom/facebook/device_id/DefaultPhoneIdProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 54421
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 54419
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->a()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 54418
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdProvider;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->c()Z

    move-result v0

    return v0
.end method
