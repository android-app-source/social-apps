.class public Lcom/facebook/common/callercontext/CallerContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91040
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0}, Lcom/facebook/common/callercontext/CallerContext;-><init>()V

    sput-object v0, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 91041
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext$1;

    invoke-direct {v0}, Lcom/facebook/common/callercontext/CallerContext$1;-><init>()V

    sput-object v0, Lcom/facebook/common/callercontext/CallerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 91042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91043
    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    .line 91044
    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    .line 91045
    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    .line 91046
    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    .line 91047
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 91048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91049
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    .line 91050
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    .line 91051
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    .line 91052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    .line 91053
    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 91054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91055
    iget-object v0, p1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    .line 91056
    iget-object v0, p1, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    .line 91057
    iget-object v0, p1, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    .line 91058
    iget-object v0, p1, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    .line 91059
    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91061
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91062
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    .line 91063
    iput-object p2, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    .line 91064
    iput-object p3, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    .line 91065
    iput-object p4, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    .line 91066
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 91067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91068
    iput-object p1, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    .line 91069
    iput-object p2, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    .line 91070
    iput-object p3, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    .line 91071
    iput-object p4, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    .line 91072
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2

    .prologue
    .line 91079
    if-nez p0, :cond_0

    .line 91080
    const/4 v0, 0x0

    .line 91081
    :goto_0
    return-object v0

    .line 91082
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 91083
    const-class v1, LX/0f2;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91084
    check-cast p0, LX/0f2;

    .line 91085
    if-nez p0, :cond_2

    .line 91086
    const/4 v0, 0x0

    .line 91087
    :goto_1
    move-object v0, v0

    .line 91088
    goto :goto_0

    .line 91089
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-interface {p0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 91073
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 91074
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    iget-object v1, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;)",
            "Lcom/facebook/common/callercontext/CallerContext;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 91075
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p0, v1, v1, v1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/common/callercontext/CallerContext;"
        }
    .end annotation

    .prologue
    .line 91076
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 91077
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p0, p1, p1, p1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/common/callercontext/CallerContext;"
        }
    .end annotation

    .prologue
    .line 91078
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p0, p1, p1, p1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/common/callercontext/CallerContext;"
        }
    .end annotation

    .prologue
    .line 91039
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p0, p1, p2, p1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/common/callercontext/CallerContext;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 91038
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p0, v1, p1, v1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/common/callercontext/CallerContext;"
        }
    .end annotation

    .prologue
    .line 91020
    new-instance v0, Lcom/facebook/common/callercontext/CallerContext;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91021
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91022
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91023
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91024
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 91025
    const/4 v0, 0x0

    return v0
.end method

.method public e()LX/15g;
    .locals 3

    .prologue
    .line 91026
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "Calling Class Name"

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "Analytics Tag"

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "Feature tag"

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "Module Analytics Tag"

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 91027
    instance-of v1, p1, Lcom/facebook/common/callercontext/CallerContext;

    if-nez v1, :cond_1

    .line 91028
    :cond_0
    :goto_0
    return v0

    .line 91029
    :cond_1
    check-cast p1, Lcom/facebook/common/callercontext/CallerContext;

    .line 91030
    iget-object v1, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 91031
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/15f;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91032
    invoke-virtual {p0}, Lcom/facebook/common/callercontext/CallerContext;->e()LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 91033
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91034
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91035
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91036
    iget-object v0, p0, Lcom/facebook/common/callercontext/CallerContext;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91037
    return-void
.end method
