.class public final Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0oz;


# direct methods
.method public constructor <init>(LX/0oz;)V
    .locals 0

    .prologue
    .line 143574
    iput-object p1, p0, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;->a:LX/0oz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 143575
    iget-object v0, p0, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;->a:LX/0oz;

    iget-boolean v0, v0, LX/0oz;->s:Z

    if-nez v0, :cond_1

    .line 143576
    iget-object v0, p0, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;->a:LX/0oz;

    .line 143577
    iget-object v1, v0, LX/0oz;->t:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v2, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143578
    iget-object v1, v0, LX/0oz;->u:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v2, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143579
    iget-object v1, v0, LX/0oz;->i:LX/0p8;

    invoke-virtual {v1}, LX/0p8;->a()V

    .line 143580
    iget-object v1, v0, LX/0oz;->h:LX/0p7;

    .line 143581
    iget-object v2, v1, LX/0p7;->c:LX/1pb;

    if-eqz v2, :cond_0

    .line 143582
    iget-object v2, v1, LX/0p7;->c:LX/1pb;

    invoke-interface {v2}, LX/1pb;->b()V

    .line 143583
    :cond_0
    iget-object v2, v1, LX/0p7;->f:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143584
    iget-object v0, p0, Lcom/facebook/common/connectionstatus/FbDataConnectionManager$ConnectionQualityResetRunnable;->a:LX/0oz;

    invoke-static {v0}, LX/0oz;->t(LX/0oz;)V

    .line 143585
    :cond_1
    return-void
.end method
