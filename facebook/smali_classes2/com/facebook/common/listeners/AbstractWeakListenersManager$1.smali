.class public final Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/Collection;

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:LX/0UI;


# direct methods
.method public constructor <init>(LX/0UI;Ljava/util/Collection;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 327730
    iput-object p1, p0, Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;->c:LX/0UI;

    iput-object p2, p0, Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;->a:Ljava/util/Collection;

    iput-object p3, p0, Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 327731
    iget-object v0, p0, Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;->c:LX/0UI;

    iget-object v1, p0, Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;->a:Ljava/util/Collection;

    invoke-virtual {v0, v1}, LX/0UI;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    .line 327732
    if-eqz v0, :cond_1

    .line 327733
    iget-object v1, p0, Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;->c:LX/0UI;

    iget-object v2, p0, Lcom/facebook/common/listeners/AbstractWeakListenersManager$1;->b:Ljava/lang/Object;

    .line 327734
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 327735
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 327736
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v1, v6, v2, p0}, LX/0UI;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 327737
    :cond_1
    return-void
.end method
