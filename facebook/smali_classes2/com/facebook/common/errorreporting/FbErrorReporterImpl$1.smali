.class public final Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/Throwable;

.field public final synthetic d:LX/03U;


# direct methods
.method public constructor <init>(LX/03U;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 67592
    iput-object p1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->d:LX/03U;

    iput-object p2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->c:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 67593
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 67594
    const-string v0, "soft_error_category"

    iget-object v2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67595
    const-string v0, "soft_error_message"

    iget-object v2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67596
    iget-object v0, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->d:LX/03U;

    iget-object v0, v0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    .line 67597
    iget-object v2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->c:Ljava/lang/Throwable;

    invoke-virtual {v0, v2, v1}, LX/009;->handleException(Ljava/lang/Throwable;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 67598
    :cond_0
    return-void

    .line 67599
    :catch_0
    move-exception v0

    .line 67600
    iget-object v1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$1;->d:LX/03U;

    iget-boolean v1, v1, LX/03U;->h:Z

    if-eqz v1, :cond_0

    .line 67601
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 67602
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 67603
    :cond_1
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_2

    .line 67604
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 67605
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
