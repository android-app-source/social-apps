.class public final Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/03U;


# direct methods
.method public constructor <init>(LX/03U;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67617
    iput-object p1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->d:LX/03U;

    iput-object p2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 67608
    iget-object v0, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->d:LX/03U;

    iget-object v1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->a:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/03U;->a$redex0(LX/03U;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 67609
    if-nez v0, :cond_0

    .line 67610
    :goto_0
    return-void

    .line 67611
    :cond_0
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 67612
    const-string v2, "strict_mode_message"

    iget-object v3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67613
    const-string v2, "strict_mode_category"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67614
    iget-object v0, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->d:LX/03U;

    iget-object v0, v0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    .line 67615
    new-instance v2, LX/44M;

    iget-object v3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/44M;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$3;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, LX/009;->handleException(Ljava/lang/Throwable;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 67616
    :catch_0
    goto :goto_0
.end method
