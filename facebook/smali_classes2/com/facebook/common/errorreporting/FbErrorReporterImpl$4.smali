.class public final Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:Ljava/util/Map;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/03U;


# direct methods
.method public constructor <init>(LX/03U;Ljava/lang/String;ILjava/util/Map;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67526
    iput-object p1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->e:LX/03U;

    iput-object p2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->a:Ljava/lang/String;

    iput p3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->b:I

    iput-object p4, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->c:Ljava/util/Map;

    iput-object p5, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 67527
    iget-object v0, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->e:LX/03U;

    iget-object v1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->a:Ljava/lang/String;

    iget v2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->b:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/03U;->a$redex0(LX/03U;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 67528
    if-nez v0, :cond_0

    .line 67529
    :goto_0
    return-void

    .line 67530
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->c:Ljava/util/Map;

    const-string v2, "runtime_linter_message"

    iget-object v3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67531
    iget-object v1, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->c:Ljava/util/Map;

    const-string v2, "runtime_linter_category"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67532
    iget-object v0, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->e:LX/03U;

    iget-object v0, v0, LX/03U;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/009;

    .line 67533
    new-instance v1, LX/44L;

    iget-object v2, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, LX/44L;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/common/errorreporting/FbErrorReporterImpl$4;->c:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3}, LX/009;->handleException(Ljava/lang/Throwable;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 67534
    :catch_0
    goto :goto_0
.end method
