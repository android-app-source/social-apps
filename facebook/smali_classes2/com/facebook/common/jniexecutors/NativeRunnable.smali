.class public final Lcom/facebook/common/jniexecutors/NativeRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:LX/0ou;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ou",
            "<",
            "Lcom/facebook/common/jniexecutors/NativeRunnable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 210303
    new-instance v0, LX/0oq;

    const-class v1, Lcom/facebook/common/jniexecutors/NativeRunnable;

    .line 210304
    sget-object v2, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v2, v2

    .line 210305
    invoke-direct {v0, v1, v2}, LX/0oq;-><init>(Ljava/lang/Class;LX/0So;)V

    new-instance v1, LX/1j3;

    const-class v2, Lcom/facebook/common/jniexecutors/NativeRunnable;

    invoke-direct {v1, v2}, LX/1j3;-><init>(Ljava/lang/Class;)V

    .line 210306
    iput-object v1, v0, LX/0oq;->f:LX/0ot;

    .line 210307
    move-object v0, v0

    .line 210308
    invoke-virtual {v0}, LX/0oq;->a()LX/0ou;

    move-result-object v0

    sput-object v0, Lcom/facebook/common/jniexecutors/NativeRunnable;->a:LX/0ou;

    .line 210309
    const-string v0, "jniexecutors"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 210310
    return-void
.end method

.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210318
    iput-object p1, p0, Lcom/facebook/common/jniexecutors/NativeRunnable;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 210319
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/jni/HybridData;B)V
    .locals 0

    .prologue
    .line 210316
    invoke-direct {p0, p1}, Lcom/facebook/common/jniexecutors/NativeRunnable;-><init>(Lcom/facebook/jni/HybridData;)V

    return-void
.end method

.method public static allocate(Lcom/facebook/jni/HybridData;)Lcom/facebook/common/jniexecutors/NativeRunnable;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210320
    sget-object v0, Lcom/facebook/common/jniexecutors/NativeRunnable;->a:LX/0ou;

    invoke-virtual {v0}, LX/0ou;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/jniexecutors/NativeRunnable;

    .line 210321
    iput-object p0, v0, Lcom/facebook/common/jniexecutors/NativeRunnable;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 210322
    return-object v0
.end method


# virtual methods
.method public final native nativeRun()V
.end method

.method public final release()V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210314
    sget-object v0, Lcom/facebook/common/jniexecutors/NativeRunnable;->a:LX/0ou;

    invoke-virtual {v0, p0}, LX/0ou;->a(Ljava/lang/Object;)V

    .line 210315
    return-void
.end method

.method public final run()V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 210311
    iget-object v0, p0, Lcom/facebook/common/jniexecutors/NativeRunnable;->mHybridData:Lcom/facebook/jni/HybridData;

    if-nez v0, :cond_0

    .line 210312
    :goto_0
    return-void

    .line 210313
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/common/jniexecutors/NativeRunnable;->nativeRun()V

    goto :goto_0
.end method
