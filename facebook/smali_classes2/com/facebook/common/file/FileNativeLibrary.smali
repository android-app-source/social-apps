.class public Lcom/facebook/common/file/FileNativeLibrary;
.super LX/03m;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:Lcom/facebook/common/file/FileNativeLibrary;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104110
    const-class v0, Lcom/facebook/common/file/FileNativeLibrary;

    sput-object v0, Lcom/facebook/common/file/FileNativeLibrary;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 104111
    const-string v0, "gnustl_shared"

    const-string v1, "fb_filesystem"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/03m;-><init>(Ljava/util/List;)V

    .line 104112
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/file/FileNativeLibrary;
    .locals 3

    .prologue
    .line 104113
    sget-object v0, Lcom/facebook/common/file/FileNativeLibrary;->b:Lcom/facebook/common/file/FileNativeLibrary;

    if-nez v0, :cond_1

    .line 104114
    const-class v1, Lcom/facebook/common/file/FileNativeLibrary;

    monitor-enter v1

    .line 104115
    :try_start_0
    sget-object v0, Lcom/facebook/common/file/FileNativeLibrary;->b:Lcom/facebook/common/file/FileNativeLibrary;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 104116
    if-eqz v2, :cond_0

    .line 104117
    :try_start_1
    invoke-static {}, Lcom/facebook/common/file/FileNativeLibrary;->d()Lcom/facebook/common/file/FileNativeLibrary;

    move-result-object v0

    sput-object v0, Lcom/facebook/common/file/FileNativeLibrary;->b:Lcom/facebook/common/file/FileNativeLibrary;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104118
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 104119
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 104120
    :cond_1
    sget-object v0, Lcom/facebook/common/file/FileNativeLibrary;->b:Lcom/facebook/common/file/FileNativeLibrary;

    return-object v0

    .line 104121
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 104122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static d()Lcom/facebook/common/file/FileNativeLibrary;
    .locals 1

    .prologue
    .line 104123
    new-instance v0, Lcom/facebook/common/file/FileNativeLibrary;

    invoke-direct {v0}, Lcom/facebook/common/file/FileNativeLibrary;-><init>()V

    .line 104124
    return-object v0
.end method


# virtual methods
.method public native nativeGetFolderSize(Ljava/lang/String;)J
.end method
