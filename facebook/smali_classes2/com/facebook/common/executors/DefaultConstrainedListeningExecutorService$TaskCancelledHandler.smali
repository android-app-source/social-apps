.class public final Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TS;

.field private final b:LX/0Va;


# direct methods
.method public constructor <init>(LX/0TS;LX/0Va;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Va",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 77681
    iput-object p1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;->a:LX/0TS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77682
    iput-object p2, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;->b:LX/0Va;

    .line 77683
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 77684
    iget-object v0, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;->b:LX/0Va;

    invoke-virtual {v0}, LX/0Va;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77685
    iget-object v0, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;->a:LX/0TS;

    iget-object v0, v0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    iget-object v1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;->b:LX/0Va;

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 77686
    :cond_0
    return-void
.end method
