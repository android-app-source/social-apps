.class public final Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0TS;


# direct methods
.method public constructor <init>(LX/0TS;)V
    .locals 0

    .prologue
    .line 63222
    iput-object p1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 63223
    const-wide/16 v2, 0x0

    .line 63224
    const/4 v1, 0x0

    .line 63225
    const/4 v6, 0x0

    .line 63226
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    iget-object v0, v0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 63227
    if-eqz v0, :cond_1

    .line 63228
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 63229
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63230
    const/4 v6, 0x1

    .line 63231
    :goto_0
    if-eqz v0, :cond_0

    .line 63232
    invoke-static {}, LX/0TR;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63233
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 63234
    iget-object v1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    iget-object v1, v1, LX/0TS;->h:LX/0TR;

    iget-object v2, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    invoke-static {v0}, LX/22f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v1 .. v6}, LX/0TR;->a(LX/0TS;Ljava/lang/String;JZ)V

    .line 63235
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    iget-object v0, v0, LX/0TS;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 63236
    iget-object v0, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    iget-object v0, v0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 63237
    iget-object v0, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    invoke-static {v0}, LX/0TS;->h(LX/0TS;)V

    .line 63238
    :goto_1
    return-void

    .line 63239
    :cond_1
    goto :goto_0

    .line 63240
    :catchall_0
    move-exception v1

    move-wide v4, v2

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v3, :cond_2

    .line 63241
    invoke-static {}, LX/0TR;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 63242
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v4, v8, v4

    .line 63243
    iget-object v1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    iget-object v1, v1, LX/0TS;->h:LX/0TR;

    iget-object v2, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    invoke-static {v3}, LX/22f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v1 .. v6}, LX/0TR;->a(LX/0TS;Ljava/lang/String;JZ)V

    .line 63244
    :cond_2
    iget-object v1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    iget-object v1, v1, LX/0TS;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 63245
    iget-object v1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    iget-object v1, v1, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 63246
    iget-object v1, p0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;->a:LX/0TS;

    invoke-static {v1}, LX/0TS;->h(LX/0TS;)V

    .line 63247
    :goto_3
    throw v0

    .line 63248
    :cond_3
    goto :goto_1

    .line 63249
    :cond_4
    goto :goto_3

    .line 63250
    :catchall_1
    move-exception v0

    move-wide v4, v2

    move-object v3, v1

    goto :goto_2
.end method
