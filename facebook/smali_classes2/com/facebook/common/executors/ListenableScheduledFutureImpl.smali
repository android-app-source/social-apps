.class public Lcom/facebook/common/executors/ListenableScheduledFutureImpl;
.super LX/17C;
.source ""

# interfaces
.implements LX/0YG;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/17C",
        "<TV;>;",
        "LX/0YG",
        "<TV;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public final a:LX/0Va;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Va",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 189099
    invoke-direct {p0, p1}, LX/17C;-><init>(Landroid/os/Handler;)V

    .line 189100
    invoke-static {p2, p3}, LX/0Va;->a(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0Va;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;->a:LX/0Va;

    .line 189101
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 189096
    invoke-direct {p0, p1}, LX/17C;-><init>(Landroid/os/Handler;)V

    .line 189097
    invoke-static {p2}, LX/0Va;->a(Ljava/util/concurrent/Callable;)LX/0Va;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;->a:LX/0Va;

    .line 189098
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 189094
    iget-object v0, p0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;->a:LX/0Va;

    move-object v0, v0

    .line 189095
    return-object v0
.end method

.method public final addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 189102
    iget-object v0, p0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;->a:LX/0Va;

    move-object v0, v0

    .line 189103
    invoke-virtual {v0, p1, p2}, LX/0Va;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 189104
    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 189093
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/17C;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 189092
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 189090
    iget-object v0, p0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;->a:LX/0Va;

    move-object v0, v0

    .line 189091
    return-object v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 1

    .prologue
    .line 189089
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 189086
    iget-object v0, p0, Lcom/facebook/common/executors/ListenableScheduledFutureImpl;->a:LX/0Va;

    move-object v0, v0

    .line 189087
    invoke-virtual {v0}, LX/0Va;->run()V

    .line 189088
    return-void
.end method
