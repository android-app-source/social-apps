.class public Lcom/facebook/common/executors/LoggingRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0X9;
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:LX/0cV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 88631
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/facebook/common/executors/LoggingRunnable;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Runnable;LX/0Sj;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 88649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88650
    iput-object p1, p0, Lcom/facebook/common/executors/LoggingRunnable;->b:Ljava/lang/Runnable;

    .line 88651
    invoke-static {p1}, LX/22f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->c:Ljava/lang/String;

    .line 88652
    invoke-static {p1}, LX/22f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->d:Ljava/lang/String;

    .line 88653
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->c:Ljava/lang/String;

    invoke-interface {p2, p3, v0}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->g:LX/0cV;

    .line 88654
    iput-object p3, p0, Lcom/facebook/common/executors/LoggingRunnable;->e:Ljava/lang/String;

    .line 88655
    iput p4, p0, Lcom/facebook/common/executors/LoggingRunnable;->f:I

    .line 88656
    return-void
.end method

.method public static a(Ljava/lang/Runnable;LX/0Sj;Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 4

    .prologue
    .line 88644
    invoke-interface {p1}, LX/0Sj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88645
    sget-object v0, Lcom/facebook/common/executors/LoggingRunnable;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v1

    .line 88646
    const-wide/16 v2, 0x80

    invoke-static {v2, v3, p2, v1}, LX/018;->d(JLjava/lang/String;I)V

    .line 88647
    new-instance v0, Lcom/facebook/common/executors/LoggingRunnable;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/facebook/common/executors/LoggingRunnable;-><init>(Ljava/lang/Runnable;LX/0Sj;Ljava/lang/String;I)V

    move-object p0, v0

    .line 88648
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88657
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88643
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final run()V
    .locals 5

    .prologue
    .line 88632
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->g:LX/0cV;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 88633
    :goto_0
    if-eqz v1, :cond_0

    .line 88634
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->g:LX/0cV;

    invoke-interface {v0}, LX/0cV;->a()V

    .line 88635
    :cond_0
    const-wide/16 v2, 0x80

    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->e:Ljava/lang/String;

    iget v4, p0, Lcom/facebook/common/executors/LoggingRunnable;->f:I

    invoke-static {v2, v3, v0, v4}, LX/018;->e(JLjava/lang/String;I)V

    .line 88636
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88637
    if-eqz v1, :cond_1

    .line 88638
    iget-object v0, p0, Lcom/facebook/common/executors/LoggingRunnable;->g:LX/0cV;

    invoke-interface {v0}, LX/0cV;->b()V

    .line 88639
    :cond_1
    return-void

    .line 88640
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 88641
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 88642
    iget-object v1, p0, Lcom/facebook/common/executors/LoggingRunnable;->g:LX/0cV;

    invoke-interface {v1}, LX/0cV;->b()V

    :cond_3
    throw v0
.end method
