.class public final Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1bu;


# direct methods
.method public constructor <init>(LX/1bu;)V
    .locals 0

    .prologue
    .line 281416
    iput-object p1, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 281417
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    iget-object v0, v0, LX/1bu;->e:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 281418
    if-eqz v0, :cond_0

    .line 281419
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281420
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    iget-object v0, v0, LX/1bu;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 281421
    iget-object v0, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    iget-object v0, v0, LX/1bu;->e:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 281422
    iget-object v0, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    invoke-static {v0}, LX/1bu;->a(LX/1bu;)V

    .line 281423
    :cond_1
    return-void

    .line 281424
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    iget-object v1, v1, LX/1bu;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 281425
    iget-object v1, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    iget-object v1, v1, LX/1bu;->e:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 281426
    iget-object v1, p0, Lcom/facebook/common/executors/ConstrainedExecutorService$Worker;->a:LX/1bu;

    invoke-static {v1}, LX/1bu;->a(LX/1bu;)V

    .line 281427
    :cond_2
    throw v0
.end method
