.class public final Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;
.super Landroid/os/HandlerThread;
.source ""


# instance fields
.field public final synthetic a:LX/0Zr;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zr;Ljava/lang/String;LX/0TP;Z)V
    .locals 2

    .prologue
    .line 83954
    iput-object p1, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->a:LX/0Zr;

    .line 83955
    invoke-virtual {p3}, LX/0TP;->getAndroidThreadPriority()I

    move-result v0

    invoke-direct {p0, p2, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 83956
    iput-object p2, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->b:Ljava/lang/String;

    .line 83957
    iput-boolean p4, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->c:Z

    .line 83958
    sget-object v0, LX/0Zr;->d:Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83959
    return-void
.end method


# virtual methods
.method public final onLooperPrepared()V
    .locals 4

    .prologue
    .line 83960
    invoke-static {}, LX/00m;->c()V

    .line 83961
    iget-boolean v0, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->c:Z

    if-eqz v0, :cond_0

    .line 83962
    iget-object v0, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 83963
    iget-object v2, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->a:LX/0Zr;

    iget-object v2, v2, LX/0Zr;->c:LX/0Sj;

    new-instance v3, LX/0cT;

    invoke-direct {v3, p0, v0, v1}, LX/0cT;-><init>(Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;Ljava/lang/String;Landroid/os/Looper;)V

    invoke-interface {v2, v3}, LX/0Sj;->a(LX/0cT;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->d:Ljava/util/List;

    .line 83964
    iget-object v2, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->a:LX/0Zr;

    iget-object v3, p0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;->a:LX/0Zr;

    iget-object v3, v3, LX/0Zr;->c:LX/0Sj;

    invoke-interface {v3}, LX/0Sj;->a()Z

    move-result v3

    invoke-static {v2, v0, v1, v3}, LX/0Zr;->a$redex0(LX/0Zr;Ljava/lang/String;Landroid/os/Looper;Z)V

    .line 83965
    :cond_0
    return-void
.end method
