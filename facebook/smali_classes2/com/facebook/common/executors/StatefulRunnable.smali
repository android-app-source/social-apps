.class public abstract Lcom/facebook/common/executors/StatefulRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 282434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282435
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/common/executors/StatefulRunnable;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 282436
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 282417
    iget-object v0, p0, Lcom/facebook/common/executors/StatefulRunnable;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282418
    invoke-virtual {p0}, Lcom/facebook/common/executors/StatefulRunnable;->b()V

    .line 282419
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 282433
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 282432
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 282431
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 282430
    return-void
.end method

.method public abstract c()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 282420
    iget-object v0, p0, Lcom/facebook/common/executors/StatefulRunnable;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 282421
    :goto_0
    return-void

    .line 282422
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/common/executors/StatefulRunnable;->c()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 282423
    iget-object v1, p0, Lcom/facebook/common/executors/StatefulRunnable;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 282424
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/facebook/common/executors/StatefulRunnable;->a(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282425
    invoke-virtual {p0, v0}, Lcom/facebook/common/executors/StatefulRunnable;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 282426
    :catch_0
    move-exception v0

    .line 282427
    iget-object v1, p0, Lcom/facebook/common/executors/StatefulRunnable;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 282428
    invoke-virtual {p0, v0}, Lcom/facebook/common/executors/StatefulRunnable;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 282429
    :catchall_0
    move-exception v1

    invoke-virtual {p0, v0}, Lcom/facebook/common/executors/StatefulRunnable;->b(Ljava/lang/Object;)V

    throw v1
.end method
