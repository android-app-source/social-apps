.class public Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 159587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159588
    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159589
    const v0, 0x7f0d002c

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 159590
    if-eqz v0, :cond_1

    .line 159591
    :cond_0
    :goto_0
    return-object v0

    .line 159592
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :goto_1
    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 159593
    check-cast v0, Landroid/view/View;

    .line 159594
    const v2, 0x7f0d002c

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 159595
    if-nez v0, :cond_0

    .line 159596
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    .line 159597
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .locals 3

    .prologue
    .line 159598
    sget-object v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    if-nez v0, :cond_1

    .line 159599
    const-class v1, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    monitor-enter v1

    .line 159600
    :try_start_0
    sget-object v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 159601
    if-eqz v2, :cond_0

    .line 159602
    :try_start_1
    new-instance v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-direct {v0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;-><init>()V

    .line 159603
    move-object v0, v0

    .line 159604
    sput-object v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159605
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 159606
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 159607
    :cond_1
    sget-object v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-object v0

    .line 159608
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 159609
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/view/View;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 159610
    const v0, 0x7f0d002c

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 159611
    return-void
.end method

.method public static final a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 159612
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 159613
    new-instance v1, Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p2, p1, p1, p1}, Lcom/facebook/common/callercontext/CallerContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 159614
    invoke-static {p0, v0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 159615
    return-void
.end method

.method public static final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/callercontext/CallerContextable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159616
    invoke-static {p2, p1, p1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 159617
    return-void
.end method
