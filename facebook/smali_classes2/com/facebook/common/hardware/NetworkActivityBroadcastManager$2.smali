.class public final Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;
.super Lcom/facebook/common/executors/NamedRunnable;
.source ""


# instance fields
.field public final synthetic c:LX/1iu;


# direct methods
.method public constructor <init>(LX/1iu;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 299359
    iput-object p1, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    invoke-direct {p0, p2, p3}, Lcom/facebook/common/executors/NamedRunnable;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 299360
    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    iget-object v1, v0, LX/1iu;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 299361
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    iget-object v0, v0, LX/1iu;->i:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    iget-object v0, v0, LX/1iu;->i:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 299362
    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    sget-object v2, LX/1iu;->b:Ljava/lang/String;

    invoke-static {v0, v2}, LX/1iu;->a$redex0(LX/1iu;Ljava/lang/String;)V

    .line 299363
    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    const/4 v2, 0x0

    .line 299364
    iput-object v2, v0, LX/1iu;->i:Ljava/util/concurrent/Future;

    .line 299365
    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    iget-object v0, v0, LX/1iu;->h:Ljava/util/concurrent/Future;

    const-string v2, "Internal inconsistency managing intent futures"

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299366
    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    iget-object v0, v0, LX/1iu;->h:Ljava/util/concurrent/Future;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 299367
    iget-object v0, p0, Lcom/facebook/common/hardware/NetworkActivityBroadcastManager$2;->c:LX/1iu;

    const/4 v2, 0x0

    .line 299368
    iput-object v2, v0, LX/1iu;->h:Ljava/util/concurrent/Future;

    .line 299369
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
