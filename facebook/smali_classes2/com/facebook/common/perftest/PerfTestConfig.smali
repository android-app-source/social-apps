.class public Lcom/facebook/common/perftest/PerfTestConfig;
.super Lcom/facebook/common/perftest/base/PerfTestConfigBase;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/common/perftest/PerfTestConfig;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 79273
    invoke-direct {p0}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;
    .locals 3

    .prologue
    .line 79261
    sget-object v0, Lcom/facebook/common/perftest/PerfTestConfig;->a:Lcom/facebook/common/perftest/PerfTestConfig;

    if-nez v0, :cond_1

    .line 79262
    const-class v1, Lcom/facebook/common/perftest/PerfTestConfig;

    monitor-enter v1

    .line 79263
    :try_start_0
    sget-object v0, Lcom/facebook/common/perftest/PerfTestConfig;->a:Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79264
    if-eqz v2, :cond_0

    .line 79265
    :try_start_1
    new-instance v0, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {v0}, Lcom/facebook/common/perftest/PerfTestConfig;-><init>()V

    .line 79266
    move-object v0, v0

    .line 79267
    sput-object v0, Lcom/facebook/common/perftest/PerfTestConfig;->a:Lcom/facebook/common/perftest/PerfTestConfig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79268
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79269
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79270
    :cond_1
    sget-object v0, Lcom/facebook/common/perftest/PerfTestConfig;->a:Lcom/facebook/common/perftest/PerfTestConfig;

    return-object v0

    .line 79271
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79272
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
