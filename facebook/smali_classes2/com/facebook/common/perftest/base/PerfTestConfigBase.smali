.class public Lcom/facebook/common/perftest/base/PerfTestConfigBase;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static a:Z

.field public static b:Z

.field public static c:Z

.field public static d:Z

.field public static e:J

.field public static f:Z

.field private static g:Z

.field public static h:Z

.field public static i:Z

.field public static j:Z

.field public static k:Z

.field public static l:J

.field public static m:Z

.field public static n:Z

.field public static o:Z

.field public static p:Z

.field public static q:Z

.field public static r:Z

.field public static s:Z

.field private static t:Z

.field private static u:Z

.field public static v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static w:Lcom/facebook/common/perftest/base/PerfTestConfigBase;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 79322
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a:Z

    .line 79323
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->b:Z

    .line 79324
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->c:Z

    .line 79325
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->d:Z

    .line 79326
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->f:Z

    .line 79327
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->g:Z

    .line 79328
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->h:Z

    .line 79329
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->i:Z

    .line 79330
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->j:Z

    .line 79331
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->k:Z

    .line 79332
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->m:Z

    .line 79333
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->n:Z

    .line 79334
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->o:Z

    .line 79335
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->p:Z

    .line 79336
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->q:Z

    .line 79337
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->r:Z

    .line 79338
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->s:Z

    .line 79339
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->t:Z

    .line 79340
    sput-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->u:Z

    .line 79341
    sput-object v2, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->v:Ljava/lang/String;

    .line 79342
    sput-object v2, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->w:Lcom/facebook/common/perftest/base/PerfTestConfigBase;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 79318
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->t:Z

    if-nez v0, :cond_0

    .line 79319
    sget-boolean v0, LX/0as;->a:Z

    .line 79320
    :goto_0
    return v0

    :cond_0
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->u:Z

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 79317
    sget-boolean v0, LX/0as;->a:Z

    if-eqz v0, :cond_0

    sget-boolean v0, LX/35S;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final c()Z
    .locals 1

    .prologue
    .line 79316
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, LX/35S;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/facebook/common/perftest/base/PerfTestConfigBase;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79313
    sget-object v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->w:Lcom/facebook/common/perftest/base/PerfTestConfigBase;

    if-nez v0, :cond_0

    .line 79314
    new-instance v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;

    invoke-direct {v0}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;-><init>()V

    sput-object v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->w:Lcom/facebook/common/perftest/base/PerfTestConfigBase;

    .line 79315
    :cond_0
    sget-object v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->w:Lcom/facebook/common/perftest/base/PerfTestConfigBase;

    return-object v0
.end method

.method public static q()Z
    .locals 1

    .prologue
    .line 79312
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->g:Z

    return v0
.end method


# virtual methods
.method public setAllowMainTabActivityKillingOnBackPressHandler(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79310
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->c:Z

    .line 79311
    return-void
.end method

.method public setAlwaysLogComponentsPerf(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79308
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->p:Z

    .line 79309
    return-void
.end method

.method public setAlwaysLogDraweePerf(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79306
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->n:Z

    .line 79307
    return-void
.end method

.method public setAlwaysLogImagePipelinePerf(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79304
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->o:Z

    .line 79305
    return-void
.end method

.method public setDisableAnalyticsLogging(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79302
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a:Z

    .line 79303
    return-void
.end method

.method public setDisableNewsFeedAutoRefresh(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79300
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->d:Z

    .line 79301
    return-void
.end method

.method public setDisablePerfLogging(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79274
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->r:Z

    .line 79275
    return-void
.end method

.method public setDisablePrefetchControllerMemoryCacheFastpath(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79298
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->q:Z

    .line 79299
    return-void
.end method

.method public setFeedImagePreloaderDisabled(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79296
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->h:Z

    .line 79297
    return-void
.end method

.method public setForceRefreshNewsFeedOnResume(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79294
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->b:Z

    .line 79295
    return-void
.end method

.method public setForceSkipTimelineCache(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79292
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->f:Z

    .line 79293
    return-void
.end method

.method public setIsFreshFeedEnabled(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79290
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->m:Z

    .line 79291
    return-void
.end method

.method public setPerfTestInfo(Ljava/lang/String;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79288
    sput-object p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->v:Ljava/lang/String;

    .line 79289
    return-void
.end method

.method public setPlacePickerFlowsEnabled(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79286
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->k:Z

    .line 79287
    return-void
.end method

.method public setPlacePickerForceMockedLocation(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79284
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->i:Z

    .line 79285
    return-void
.end method

.method public setPlacePickerSuppressLocationSourceDialog(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79282
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->j:Z

    .line 79283
    return-void
.end method

.method public setPlacePickerTimeoutMs(J)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79280
    sput-wide p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->l:J

    .line 79281
    return-void
.end method

.method public setSychronousPerfLoggerEvents(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79278
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->s:Z

    .line 79279
    return-void
.end method

.method public setUseApiRequestCache(Z)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 79276
    sput-boolean p1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->g:Z

    .line 79277
    return-void
.end method
