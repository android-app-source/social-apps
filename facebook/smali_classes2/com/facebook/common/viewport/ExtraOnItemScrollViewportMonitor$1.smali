.class public final Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0g8;

.field public final synthetic b:LX/1k3;


# direct methods
.method public constructor <init>(LX/1k3;LX/0g8;)V
    .locals 0

    .prologue
    .line 311175
    iput-object p1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iput-object p2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->a:LX/0g8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 311176
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-boolean v0, v0, LX/1Kt;->f:Z

    if-nez v0, :cond_1

    .line 311177
    :cond_0
    :goto_0
    return-void

    .line 311178
    :cond_1
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->a:LX/0g8;

    invoke-interface {v1}, LX/0g8;->q()I

    move-result v1

    iput v1, v0, LX/1k3;->d:I

    .line 311179
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget v0, v0, LX/1Kt;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 311180
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->a:LX/0g8;

    invoke-interface {v1}, LX/0g8;->r()I

    move-result v1

    iput v1, v0, LX/1k3;->e:I

    .line 311181
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v0, v0, LX/1Kt;->a:LX/01J;

    iget-object v1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v1, v1, LX/1Kt;->g:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->a(LX/01J;)V

    .line 311182
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v0, v0, LX/1Kt;->b:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 311183
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget v0, v0, LX/1Kt;->d:I

    .line 311184
    :goto_1
    iget-object v1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget v1, v1, LX/1Kt;->e:I

    if-gt v0, v1, :cond_4

    iget-object v1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->a:LX/0g8;

    invoke-interface {v1}, LX/0g8;->s()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 311185
    iget-object v1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->a:LX/0g8;

    invoke-virtual {v1, v2, v0}, LX/1k3;->b(LX/0g8;I)Ljava/lang/Object;

    move-result-object v1

    .line 311186
    iget-object v2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v2, v2, LX/1Kt;->g:LX/01J;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 311187
    iget-object v2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    invoke-virtual {v2, v1}, LX/1Kt;->a(Ljava/lang/Object;)V

    .line 311188
    :cond_2
    iget-object v2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v2, v2, LX/1Kt;->b:LX/01J;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    .line 311189
    iget-object v2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v3, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->a:LX/0g8;

    iget-object v4, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget v4, v4, LX/1Kt;->d:I

    sub-int v4, v0, v4

    invoke-virtual {v2, v3, v1, v4}, LX/1Kt;->a(LX/0g8;Ljava/lang/Object;I)V

    .line 311190
    :cond_3
    iget-object v2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v2, v2, LX/1Kt;->a:LX/01J;

    invoke-virtual {v2, v1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311191
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 311192
    :cond_4
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v1, v1, LX/1Kt;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_6

    .line 311193
    iget-object v2, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v2, v2, LX/1Kt;->a:LX/01J;

    invoke-virtual {v2, v0}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v2

    .line 311194
    iget-object v3, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v3, v3, LX/1Kt;->g:LX/01J;

    invoke-virtual {v3, v2}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 311195
    iget-object v3, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    invoke-virtual {v3, v2}, LX/1Kt;->b(Ljava/lang/Object;)V

    .line 311196
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 311197
    :cond_6
    iget-object v0, p0, Lcom/facebook/common/viewport/ExtraOnItemScrollViewportMonitor$1;->b:LX/1k3;

    iget-object v0, v0, LX/1Kt;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    goto/16 :goto_0
.end method
