.class public Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0w3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 158578
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 158579
    invoke-direct {p0}, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;->a()V

    .line 158580
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 158581
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 158582
    invoke-direct {p0}, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;->a()V

    .line 158583
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 158573
    const-class v0, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;

    invoke-static {v0, p0}, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 158574
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;

    invoke-static {v0}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v0

    check-cast v0, LX/0w3;

    iput-object v0, p0, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;->a:LX/0w3;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 158575
    iget-object v0, p0, Lcom/facebook/common/ui/keyboard/SoftInputDetectingLinearLayout;->a:LX/0w3;

    invoke-virtual {v0, p0, p2}, LX/0w3;->a(Landroid/view/View;I)V

    .line 158576
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 158577
    return-void
.end method
