.class public final Lcom/facebook/common/network/FbNetworkManager$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/net/NetworkInfo;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0kb;


# direct methods
.method public constructor <init>(LX/0kb;Landroid/net/NetworkInfo;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 127782
    iput-object p1, p0, Lcom/facebook/common/network/FbNetworkManager$4;->c:LX/0kb;

    iput-object p2, p0, Lcom/facebook/common/network/FbNetworkManager$4;->a:Landroid/net/NetworkInfo;

    iput-object p3, p0, Lcom/facebook/common/network/FbNetworkManager$4;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127783
    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$4;->c:LX/0kb;

    iget-object v0, v0, LX/0kb;->j:LX/0Zb;

    const-string v3, "android_active_network_confirmed"

    invoke-interface {v0, v3, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 127784
    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$4;->c:LX/0kb;

    invoke-static {v0}, LX/0kb;->x(LX/0kb;)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 127785
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127786
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/facebook/common/network/FbNetworkManager$4;->a:Landroid/net/NetworkInfo;

    invoke-static {v5}, LX/0kb;->e(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "-"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lcom/facebook/common/network/FbNetworkManager$4;->a:Landroid/net/NetworkInfo;

    invoke-static {v5}, LX/0kb;->f(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 127787
    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$4;->a:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$4;->a:Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 127788
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, LX/0kb;->e(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4}, LX/0kb;->f(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 127789
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 127790
    :goto_1
    const-string v2, "returnedNetwork"

    invoke-virtual {v3, v2, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 127791
    const-string v2, "returnedNetworkConnection"

    invoke-virtual {v3, v2, v0}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 127792
    const-string v0, "newNetwork"

    invoke-virtual {v3, v0, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 127793
    const-string v0, "newNetworkConnection"

    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 127794
    const-string v0, "isPowerSaving"

    iget-object v1, p0, Lcom/facebook/common/network/FbNetworkManager$4;->c:LX/0kb;

    .line 127795
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v2, v4, :cond_3

    iget-object v2, v1, LX/0kb;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 127796
    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 127797
    const-string v0, "isDozing"

    iget-object v1, p0, Lcom/facebook/common/network/FbNetworkManager$4;->c:LX/0kb;

    invoke-static {v1}, LX/0kb;->D(LX/0kb;)Z

    move-result v1

    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 127798
    const-string v0, "source"

    iget-object v1, p0, Lcom/facebook/common/network/FbNetworkManager$4;->b:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 127799
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 127800
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 127801
    goto :goto_0

    :cond_2
    move v1, v2

    .line 127802
    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method
