.class public final Lcom/facebook/common/json/FbJsonField$BeanJsonField;
.super Lcom/facebook/common/json/FbJsonField;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private c:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;)V
    .locals 0
    .param p2    # Ljava/lang/reflect/Method;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 272074
    invoke-direct {p0, p1, p2}, Lcom/facebook/common/json/FbJsonField;-><init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;)V

    .line 272075
    return-void
.end method

.method private a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 272076
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v1, :cond_0

    .line 272077
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 272078
    const/4 v0, 0x0

    .line 272079
    :goto_0
    return-object v0

    .line 272080
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/json/FbJsonField$BeanJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v0, :cond_1

    .line 272081
    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 272082
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    if-nez v1, :cond_2

    .line 272083
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 272084
    instance-of v2, v1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v2, :cond_4

    move-object v2, v1

    .line 272085
    check-cast v2, Ljava/lang/reflect/ParameterizedType;

    .line 272086
    invoke-interface {v2}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    .line 272087
    invoke-interface {v2}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v6

    .line 272088
    const-class v2, LX/0Px;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 272089
    array-length v2, v6

    if-ne v2, v4, :cond_3

    move v2, v4

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 272090
    sget-object v2, Lcom/facebook/common/json/FbJsonField;->c:LX/0li;

    aget-object v4, v6, v5

    invoke-virtual {v2, v4}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    invoke-static {v3, v2}, LX/267;->a(Ljava/lang/Class;LX/0lJ;)LX/267;

    move-result-object v2

    .line 272091
    :goto_2
    move-object v1, v2

    .line 272092
    invoke-virtual {v0, p2, v1}, LX/0lB;->b(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/FbJsonField$BeanJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272093
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/facebook/common/json/FbJsonField$BeanJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 272094
    :cond_2
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getGenericParameterTypes()[Ljava/lang/reflect/Type;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p2, v1}, LX/0lB;->a(LX/0n3;Ljava/lang/reflect/Type;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/FbJsonField$BeanJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    goto :goto_3

    :cond_3
    move v2, v5

    .line 272095
    goto :goto_1

    .line 272096
    :cond_4
    sget-object v2, Lcom/facebook/common/json/FbJsonField;->c:LX/0li;

    invoke-virtual {v2, v1}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    goto :goto_2
.end method


# virtual methods
.method public deserialize(Ljava/lang/Object;LX/15w;LX/0n3;)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 272097
    :try_start_0
    invoke-direct {p0, p2, p3}, Lcom/facebook/common/json/FbJsonField$BeanJsonField;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 272098
    if-nez v0, :cond_0

    .line 272099
    :goto_0
    return-void

    .line 272100
    :cond_0
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_1

    .line 272101
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 272102
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 272103
    :catch_0
    move-exception v0

    .line 272104
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 272105
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 272106
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->a:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 272107
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
