.class public Lcom/facebook/common/json/ImmutableMapDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "LX/0P1",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;

.field private b:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TK;>;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:LX/0lJ;

.field private e:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 273111
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 273112
    iput-boolean v0, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->c:Z

    .line 273113
    invoke-virtual {p1, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v2

    .line 273114
    iget-object v3, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v3

    .line 273115
    iput-object v2, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->a:Ljava/lang/Class;

    .line 273116
    iget-object v2, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->a:Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    if-eq v2, v3, :cond_0

    const-class v2, Ljava/lang/Enum;

    iget-object v3, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->a:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    const-string v2, "Map keys must be a String or an enum."

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 273117
    invoke-virtual {p1, v1}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->d:LX/0lJ;

    .line 273118
    return-void
.end method

.method private a(LX/15w;LX/0n3;)LX/0P1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 273119
    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 273120
    invoke-virtual {p1}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v2, :cond_1

    .line 273121
    :cond_0
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 273122
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 273123
    :goto_0
    return-object v0

    .line 273124
    :cond_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_2

    .line 273125
    new-instance v0, LX/2aQ;

    const-string v1, "Failed to deserialize to a map - missing start_object token"

    invoke-virtual {p1}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 273126
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->c:Z

    if-nez v1, :cond_4

    .line 273127
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->a:Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    if-eq v1, v2, :cond_3

    .line 273128
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->a:Ljava/lang/Class;

    invoke-virtual {v0, p2, v1}, LX/0lB;->a(LX/0n3;Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->b:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 273129
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->c:Z

    .line 273130
    :cond_4
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v1, :cond_5

    .line 273131
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->d:LX/0lJ;

    invoke-virtual {v0, p2, v1}, LX/0lB;->b(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 273132
    :cond_5
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 273133
    :cond_6
    :goto_1
    invoke-static {p1}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_8

    .line 273134
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v2, v3, :cond_6

    .line 273135
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 273136
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 273137
    iget-object v3, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v3, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v3

    .line 273138
    if-eqz v3, :cond_6

    .line 273139
    iget-object v4, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->b:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v4, :cond_7

    .line 273140
    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v2

    .line 273141
    invoke-virtual {v2}, LX/15w;->c()LX/15z;

    .line 273142
    iget-object v4, p0, Lcom/facebook/common/json/ImmutableMapDeserializer;->b:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v4, v2, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v2

    .line 273143
    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 273144
    :cond_7
    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 273145
    :cond_8
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 273146
    invoke-direct {p0, p1, p2}, Lcom/facebook/common/json/ImmutableMapDeserializer;->a(LX/15w;LX/0n3;)LX/0P1;

    move-result-object v0

    return-object v0
.end method
