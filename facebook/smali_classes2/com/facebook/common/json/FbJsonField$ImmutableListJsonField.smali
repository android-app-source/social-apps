.class public final Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;
.super Lcom/facebook/common/json/FbJsonField;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private c:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "LX/0Px",
            "<*>;>;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private e:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;Ljava/lang/Class;LX/266;)V
    .locals 0
    .param p2    # Ljava/lang/reflect/Method;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/266;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/266",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 272196
    invoke-direct {p0, p1, p2}, Lcom/facebook/common/json/FbJsonField;-><init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;)V

    .line 272197
    iput-object p3, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->d:Ljava/lang/Class;

    .line 272198
    iput-object p4, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->e:LX/266;

    .line 272199
    return-void
.end method


# virtual methods
.method public final deserialize(Ljava/lang/Object;LX/15w;LX/0n3;)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 272174
    :try_start_0
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v1, :cond_0

    .line 272175
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 272176
    :goto_0
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_4

    .line 272177
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 272178
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 272179
    :goto_1
    return-void

    .line 272180
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v0, :cond_1

    .line 272181
    iget-object v0, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->d:Ljava/lang/Class;

    if-eqz v0, :cond_2

    .line 272182
    new-instance v0, Lcom/facebook/common/json/ImmutableListDeserializer;

    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->d:Ljava/lang/Class;

    invoke-direct {v0, v1}, Lcom/facebook/common/json/ImmutableListDeserializer;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272183
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p2, p3}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    goto :goto_0

    .line 272184
    :cond_2
    iget-object v0, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->e:LX/266;

    if-eqz v0, :cond_3

    .line 272185
    invoke-virtual {p2}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 272186
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->e:LX/266;

    .line 272187
    iget-object v2, v1, LX/266;->a:Ljava/lang/reflect/Type;

    move-object v1, v2

    .line 272188
    invoke-virtual {v0, p3, v1}, LX/0lB;->a(LX/0n3;Ljava/lang/reflect/Type;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 272189
    new-instance v1, Lcom/facebook/common/json/ImmutableListDeserializer;

    invoke-direct {v1, v0}, Lcom/facebook/common/json/ImmutableListDeserializer;-><init>(Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    iput-object v1, p0, Lcom/facebook/common/json/FbJsonField$ImmutableListJsonField;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 272190
    :catch_0
    move-exception v0

    .line 272191
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 272192
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 272193
    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Need to set simple or generic inner list type!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272194
    :cond_4
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->a:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 272195
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
