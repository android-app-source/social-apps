.class public Lcom/facebook/common/json/LinkedHashMapDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;

.field private b:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TK;>;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:LX/0lJ;

.field private e:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 272516
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 272517
    iput-boolean v0, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->c:Z

    .line 272518
    invoke-virtual {p1, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v2

    .line 272519
    iget-object v3, v2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v3

    .line 272520
    iput-object v2, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->a:Ljava/lang/Class;

    .line 272521
    iget-object v2, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->a:Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    if-eq v2, v3, :cond_0

    const-class v2, Ljava/lang/Enum;

    iget-object v3, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->a:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    const-string v2, "Map keys must be a String or an enum."

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 272522
    invoke-virtual {p1, v1}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->d:LX/0lJ;

    .line 272523
    return-void
.end method

.method private a(LX/15w;LX/0n3;)Ljava/util/LinkedHashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "Ljava/util/LinkedHashMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 272524
    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 272525
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 272526
    invoke-virtual {p1}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v3, :cond_1

    .line 272527
    :cond_0
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    move-object v0, v2

    .line 272528
    :goto_0
    return-object v0

    .line 272529
    :cond_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_2

    .line 272530
    new-instance v0, LX/2aQ;

    const-string v1, "Failed to deserialize to a map - missing start_object token"

    invoke-virtual {p1}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 272531
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->c:Z

    if-nez v1, :cond_4

    .line 272532
    iget-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->a:Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    if-eq v1, v3, :cond_3

    .line 272533
    iget-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->a:Ljava/lang/Class;

    invoke-virtual {v0, p2, v1}, LX/0lB;->a(LX/0n3;Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->b:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272534
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->c:Z

    .line 272535
    :cond_4
    iget-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v1, :cond_5

    .line 272536
    iget-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->d:LX/0lJ;

    invoke-virtual {v0, p2, v1}, LX/0lB;->b(LX/0n3;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272537
    :cond_5
    :goto_1
    invoke-static {p1}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 272538
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v1, v3, :cond_5

    .line 272539
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 272540
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 272541
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v4, :cond_6

    .line 272542
    iget-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->getNullValue()Ljava/lang/Object;

    move-result-object v1

    .line 272543
    :goto_2
    iget-object v4, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->b:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v4, :cond_7

    .line 272544
    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\""

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v3

    .line 272545
    invoke-virtual {v3}, LX/15w;->c()LX/15z;

    .line 272546
    iget-object v4, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->b:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v4, v3, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v3

    .line 272547
    invoke-virtual {v2, v3, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 272548
    :cond_6
    iget-object v1, p0, Lcom/facebook/common/json/LinkedHashMapDeserializer;->e:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    .line 272549
    if-eqz v1, :cond_5

    goto :goto_2

    .line 272550
    :cond_7
    invoke-virtual {v2, v3, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_8
    move-object v0, v2

    .line 272551
    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272552
    invoke-direct {p0, p1, p2}, Lcom/facebook/common/json/LinkedHashMapDeserializer;->a(LX/15w;LX/0n3;)Ljava/util/LinkedHashMap;

    move-result-object v0

    return-object v0
.end method
