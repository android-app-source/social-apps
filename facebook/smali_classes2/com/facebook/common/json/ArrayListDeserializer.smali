.class public Lcom/facebook/common/json/ArrayListDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:LX/0lJ;

.field private c:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 271907
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 271908
    iput-object v1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->a:Ljava/lang/Class;

    .line 271909
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/ArrayListDeserializer;->b:LX/0lJ;

    .line 271910
    iput-object v1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 271911
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 271897
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 271898
    iput-object v0, p0, Lcom/facebook/common/json/ArrayListDeserializer;->a:Ljava/lang/Class;

    .line 271899
    iput-object v0, p0, Lcom/facebook/common/json/ArrayListDeserializer;->b:LX/0lJ;

    .line 271900
    iput-object p1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 271901
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 271902
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 271903
    iput-object p1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->a:Ljava/lang/Class;

    .line 271904
    iput-object v0, p0, Lcom/facebook/common/json/ArrayListDeserializer;->b:LX/0lJ;

    .line 271905
    iput-object v0, p0, Lcom/facebook/common/json/ArrayListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 271906
    return-void
.end method

.method private a(LX/15w;LX/0n3;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 271882
    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 271883
    invoke-virtual {p1}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v2, :cond_2

    .line 271884
    :cond_0
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 271885
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 271886
    :cond_1
    return-object v0

    .line 271887
    :cond_2
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-eq v1, v2, :cond_3

    .line 271888
    new-instance v0, LX/2aQ;

    const-string v1, "Failed to deserialize to a list - missing start_array token"

    invoke-virtual {p1}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 271889
    :cond_3
    iget-object v1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v1, :cond_4

    .line 271890
    iget-object v1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->a:Ljava/lang/Class;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->a:Ljava/lang/Class;

    :goto_0
    check-cast v1, Ljava/lang/reflect/Type;

    invoke-virtual {v0, p2, v1}, LX/0lB;->a(LX/0n3;Ljava/lang/reflect/Type;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/ArrayListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 271891
    :cond_4
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 271892
    :cond_5
    :goto_1
    invoke-static {p1}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_1

    .line 271893
    iget-object v1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    .line 271894
    if-eqz v1, :cond_5

    .line 271895
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 271896
    :cond_6
    iget-object v1, p0, Lcom/facebook/common/json/ArrayListDeserializer;->b:LX/0lJ;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 271881
    invoke-direct {p0, p1, p2}, Lcom/facebook/common/json/ArrayListDeserializer;->a(LX/15w;LX/0n3;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
