.class public Lcom/facebook/common/json/FbJsonDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private mCtor:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 271595
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 271596
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/common/json/FbJsonDeserializer;->mCtor:Ljava/lang/reflect/Constructor;

    .line 271597
    return-void
.end method

.method public static getJsonParserText(LX/15w;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/16 v6, 0x64

    .line 271598
    invoke-virtual {p0}, LX/15w;->b()Ljava/lang/Object;

    move-result-object v0

    .line 271599
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 271600
    const-string v2, "current token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271601
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271602
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271603
    instance-of v2, v0, Ljava/io/InputStream;

    if-eqz v2, :cond_0

    .line 271604
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 271605
    invoke-virtual {p0, v2}, LX/15w;->a(Ljava/io/OutputStream;)I

    .line 271606
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 271607
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 271608
    array-length v4, v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    rsub-int/lit8 v5, v5, 0x64

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 271609
    invoke-virtual {v1, v3, v8, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 271610
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 271611
    check-cast v0, Ljava/io/InputStream;

    .line 271612
    :goto_0
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v2

    if-eq v2, v7, :cond_2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ge v3, v6, :cond_2

    .line 271613
    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 271614
    :cond_0
    instance-of v2, v0, Ljava/io/Reader;

    if-eqz v2, :cond_2

    .line 271615
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 271616
    invoke-virtual {p0, v2}, LX/15w;->a(Ljava/io/Writer;)I

    .line 271617
    invoke-virtual {v2}, Ljava/io/Writer;->flush()V

    .line 271618
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 271619
    array-length v4, v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    rsub-int/lit8 v5, v5, 0x64

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 271620
    invoke-virtual {v1, v3, v8, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 271621
    invoke-virtual {v2}, Ljava/io/Writer;->close()V

    .line 271622
    check-cast v0, Ljava/io/Reader;

    .line 271623
    :goto_1
    invoke-virtual {v0}, Ljava/io/Reader;->read()I

    move-result v2

    if-eq v2, v7, :cond_1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ge v3, v6, :cond_1

    .line 271624
    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 271625
    :cond_1
    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 271626
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 271627
    const-string v0, "..."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271628
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 271629
    :try_start_0
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonDeserializer;->mCtor:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 271630
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonDeserializer;->mCtor:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 271631
    :cond_0
    :goto_0
    invoke-static {p1}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v3, :cond_2

    .line 271632
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v1, v3, :cond_0

    .line 271633
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 271634
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 271635
    invoke-virtual {p0, v1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v1

    .line 271636
    if-eqz v1, :cond_1

    .line 271637
    invoke-virtual {v1, v2, p1, p2}, Lcom/facebook/common/json/FbJsonField;->deserialize(Ljava/lang/Object;LX/15w;LX/0n3;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 271638
    :catch_0
    move-exception v1

    .line 271639
    const-class v2, Ljava/io/IOException;

    invoke-static {v1, v2}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 271640
    new-instance v2, LX/2aQ;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to deserialize to instance "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/common/json/FbJsonDeserializer;->mCtor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v4}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getJsonParserText(LX/15w;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/15w;->l()LX/28G;

    move-result-object v4

    invoke-direct {v2, v3, v4, v1}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v2

    .line 271641
    :cond_1
    :try_start_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 271642
    :cond_2
    instance-of v1, v2, LX/0Pm;

    if-eqz v1, :cond_3

    .line 271643
    move-object v0, v2

    check-cast v0, LX/0Pm;

    move-object v1, v0

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 271644
    :cond_3
    return-object v2
.end method

.method public getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 1

    .prologue
    .line 271645
    const/4 v0, 0x0

    return-object v0
.end method

.method public final init(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 271646
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/FbJsonDeserializer;->mCtor:Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271647
    return-void

    .line 271648
    :catch_0
    move-exception v0

    .line 271649
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " missing default constructor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public isCachable()Z
    .locals 1

    .prologue
    .line 271650
    const/4 v0, 0x1

    return v0
.end method
