.class public final Lcom/facebook/common/json/FbJsonField$StringJsonField;
.super Lcom/facebook/common/json/FbJsonField;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;)V
    .locals 0
    .param p2    # Ljava/lang/reflect/Method;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 272034
    invoke-direct {p0, p1, p2}, Lcom/facebook/common/json/FbJsonField;-><init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;)V

    .line 272035
    return-void
.end method


# virtual methods
.method public final deserialize(Ljava/lang/Object;LX/15w;LX/0n3;)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 272036
    const/4 v0, 0x0

    .line 272037
    :try_start_0
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v2, :cond_1

    .line 272038
    invoke-virtual {p2}, LX/15w;->f()LX/15w;

    .line 272039
    :cond_0
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_2

    .line 272040
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 272041
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 272042
    :goto_0
    return-void

    .line 272043
    :cond_1
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    .line 272044
    if-nez v0, :cond_0

    .line 272045
    new-instance v0, LX/2aQ;

    const-string v1, "Failed to read text from Json stream"

    invoke-virtual {p2}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 272046
    :catch_0
    move-exception v0

    .line 272047
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 272048
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 272049
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->a:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 272050
    iget-object v1, p0, Lcom/facebook/common/json/FbJsonField;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
