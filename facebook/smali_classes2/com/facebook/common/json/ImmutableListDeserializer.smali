.class public Lcom/facebook/common/json/ImmutableListDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "LX/0Px",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:LX/0lJ;

.field private c:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 272200
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 272201
    iput-object v1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->a:Ljava/lang/Class;

    .line 272202
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->b:LX/0lJ;

    .line 272203
    iput-object v1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272204
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 272205
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 272206
    iput-object v0, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->a:Ljava/lang/Class;

    .line 272207
    iput-object v0, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->b:LX/0lJ;

    .line 272208
    iput-object p1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272209
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 272210
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 272211
    iput-object p1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->a:Ljava/lang/Class;

    .line 272212
    iput-object v0, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->b:LX/0lJ;

    .line 272213
    iput-object v0, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272214
    return-void
.end method

.method private a(LX/15w;LX/0n3;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "LX/0n3;",
            ")",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 272215
    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v0

    check-cast v0, LX/0lB;

    .line 272216
    invoke-virtual {p1}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v1, v2, :cond_1

    .line 272217
    :cond_0
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 272218
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 272219
    :goto_0
    return-object v0

    .line 272220
    :cond_1
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-eq v1, v2, :cond_2

    .line 272221
    new-instance v0, LX/2aQ;

    const-string v1, "Failed to deserialize to a list - missing start_array token"

    invoke-virtual {p1}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 272222
    :cond_2
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v1, :cond_3

    .line 272223
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->a:Ljava/lang/Class;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->a:Ljava/lang/Class;

    :goto_1
    check-cast v1, Ljava/lang/reflect/Type;

    invoke-virtual {v0, p2, v1}, LX/0lB;->a(LX/0n3;Ljava/lang/reflect/Type;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 272224
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 272225
    :cond_4
    :goto_2
    invoke-static {p1}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_6

    .line 272226
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->c:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    .line 272227
    if-eqz v1, :cond_4

    .line 272228
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 272229
    :cond_5
    iget-object v1, p0, Lcom/facebook/common/json/ImmutableListDeserializer;->b:LX/0lJ;

    goto :goto_1

    .line 272230
    :cond_6
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272231
    invoke-direct {p0, p1, p2}, Lcom/facebook/common/json/ImmutableListDeserializer;->a(LX/15w;LX/0n3;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
