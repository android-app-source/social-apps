.class public Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0wS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Ffo;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/ui/search/SearchEditText;

.field public h:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/glyph/GlyphView;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

.field private k:Lcom/facebook/widget/text/BetterTextView;

.field private l:LX/0zI;

.field private m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:LX/F5P;

.field public p:LX/Fh5;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 166890
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 166891
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->f:Ljava/util/Set;

    .line 166892
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->n:Ljava/lang/String;

    .line 166893
    iput-boolean v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->q:Z

    .line 166894
    iput-boolean v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->r:Z

    .line 166895
    iput-boolean v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->s:Z

    .line 166896
    iput v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->t:I

    .line 166897
    invoke-direct {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j()V

    .line 166898
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 166899
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 166900
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->f:Ljava/util/Set;

    .line 166901
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->n:Ljava/lang/String;

    .line 166902
    iput-boolean v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->q:Z

    .line 166903
    iput-boolean v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->r:Z

    .line 166904
    iput-boolean v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->s:Z

    .line 166905
    iput v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->t:I

    .line 166906
    invoke-direct {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j()V

    .line 166907
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 166908
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 166909
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->f:Ljava/util/Set;

    .line 166910
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->n:Ljava/lang/String;

    .line 166911
    iput-boolean v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->q:Z

    .line 166912
    iput-boolean v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->r:Z

    .line 166913
    iput-boolean v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->s:Z

    .line 166914
    iput v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->t:I

    .line 166915
    invoke-direct {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j()V

    .line 166916
    return-void
.end method

.method public static synthetic a(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 166917
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;LX/0wS;LX/0Uh;LX/0ad;LX/0wM;LX/0hL;)V
    .locals 0

    .prologue
    .line 166918
    iput-object p1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a:LX/0wS;

    iput-object p2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->b:LX/0Uh;

    iput-object p3, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->c:LX/0ad;

    iput-object p4, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->d:LX/0wM;

    iput-object p5, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->e:LX/0hL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-static {v5}, LX/0wS;->b(LX/0QB;)LX/0wS;

    move-result-object v1

    check-cast v1, LX/0wS;

    invoke-static {v5}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {v5}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v5}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v5}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v5

    check-cast v5, LX/0hL;

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;LX/0wS;LX/0Uh;LX/0ad;LX/0wM;LX/0hL;)V

    return-void
.end method

.method private static c(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 166919
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 166920
    sget-object v2, LX/103;->VIDEO:LX/103;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 166921
    :goto_0
    return v0

    .line 166922
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 166923
    sget-object v2, LX/103;->GROUP:LX/103;

    if-eq v0, v2, :cond_1

    .line 166924
    const/4 v0, 0x0

    goto :goto_0

    .line 166925
    :cond_1
    sget-object v0, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {p0, v0}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;)Landroid/os/Parcelable;

    move-result-object v0

    .line 166926
    if-eqz v0, :cond_2

    instance-of v2, v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    if-eqz v2, :cond_2

    .line 166927
    check-cast v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    .line 166928
    iget-boolean v1, v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->b:Z

    move v0, v1

    .line 166929
    goto :goto_0

    :cond_2
    move v0, v1

    .line 166930
    goto :goto_0
.end method

.method private j()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 166931
    invoke-virtual {p0, v5}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setOrientation(I)V

    .line 166932
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f01026f

    const v2, 0x7f020c1f

    invoke-static {v0, v1, v2}, LX/0WH;->e(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 166933
    invoke-static {p0, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 166934
    const-class v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-static {v0, p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 166935
    const v0, 0x7f0307ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 166936
    new-instance v1, LX/0zw;

    const v0, 0x7f0d14e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/0zx;

    invoke-direct {v2, p0}, LX/0zx;-><init>(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->i:LX/0zw;

    .line 166937
    new-instance v1, LX/0zw;

    const v0, 0x7f0d14e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/0zz;

    invoke-direct {v2, p0}, LX/0zz;-><init>(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    .line 166938
    const v0, 0x7f0d14e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 166939
    const v0, 0x7f0d1477

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    .line 166940
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010270

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    .line 166941
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->d:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020c16

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 166942
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1, v0, v4, v4, v4}, Lcom/facebook/ui/search/SearchEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 166943
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->c:LX/0ad;

    sget-short v1, LX/100;->aT:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166944
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    const v1, 0x7f082335

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHint(I)V

    .line 166945
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k()V

    .line 166946
    sget-object v0, LX/0zI;->DEFAULT:LX/0zI;

    invoke-virtual {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setSearchBoxType(LX/0zI;)V

    .line 166947
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 166948
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/101;

    invoke-direct {v1, p0}, LX/101;-><init>(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 166949
    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    .line 166950
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166951
    :goto_0
    return-void

    .line 166952
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 166953
    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 166954
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->d:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02081c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 166955
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->e:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 166956
    iget-object v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    invoke-virtual {v2, v1, v1, v0, v1}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 166957
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 166958
    return-void

    :cond_0
    move-object v0, v1

    .line 166959
    goto :goto_0

    .line 166960
    :cond_1
    iget-object v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    invoke-virtual {v2, v1, v1, v0, v1}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public static n(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V
    .locals 3

    .prologue
    .line 166961
    iget-boolean v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 166962
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a:LX/0wS;

    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    .line 166963
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, LX/0wS;->a(LX/0wS;Landroid/view/View;F)V

    .line 166964
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 166965
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->requestLayout()V

    .line 166966
    :cond_0
    return-void
.end method

.method public static o(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V
    .locals 3

    .prologue
    .line 166967
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166968
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0217b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 166969
    :goto_0
    return-void

    .line 166970
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->e:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0217b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0217b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1
.end method

.method private setNullStateBadgeEnabled(Z)V
    .locals 3

    .prologue
    const/16 v0, 0xa

    .line 166971
    iput-boolean p1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->s:Z

    .line 166972
    iget-boolean v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->s:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->t:I

    if-lez v1, :cond_1

    .line 166973
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 166974
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    iget v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->t:I

    if-le v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 166975
    :goto_1
    return-void

    .line 166976
    :cond_0
    iget v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->t:I

    goto :goto_0

    .line 166977
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private setScopedHintString(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 166978
    iput-object p1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->m:Ljava/lang/String;

    .line 166979
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v0

    .line 166980
    if-eqz v0, :cond_0

    .line 166981
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 166982
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 166983
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 166984
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166985
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->q:Z

    .line 166986
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 166987
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 166988
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 166989
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 166990
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->r:Z

    .line 166991
    invoke-static {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->n(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V

    .line 166992
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 3

    .prologue
    .line 166993
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->l:LX/0zI;

    sget-object v1, LX/0zI;->IMMERSIVE:LX/0zI;

    if-eq v0, v1, :cond_0

    .line 166994
    :goto_0
    return-void

    .line 166995
    :cond_0
    const/high16 v0, 0x42980000    # 76.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    rsub-int v0, v0, 0xcc

    .line 166996
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v2}, Lcom/facebook/ui/search/SearchEditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 166997
    const/high16 v0, 0x42cc0000    # 102.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    rsub-int v0, v0, 0x99

    .line 166998
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public final a(LX/103;)V
    .locals 2

    .prologue
    .line 166999
    sget-object v0, LX/102;->b:[I

    invoke-virtual {p1}, LX/103;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 167000
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0820b4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setScopedHintString(Ljava/lang/String;)V

    .line 167001
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setNullStateBadgeEnabled(Z)V

    .line 167002
    return-void

    .line 167003
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0820b5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setScopedHintString(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/Cwd;)V
    .locals 2

    .prologue
    .line 167004
    sget-object v0, LX/Cwd;->SCOPED:LX/Cwd;

    invoke-virtual {v0, p1}, LX/Cwd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167005
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v0

    .line 167006
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->m:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 167007
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 167008
    :cond_0
    :goto_0
    return-void

    .line 167009
    :cond_1
    sget-object v0, LX/Cwd;->GLOBAL:LX/Cwd;

    invoke-virtual {v0, p1}, LX/Cwd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167010
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g()V

    goto :goto_0
.end method

.method public final a(LX/Ffo;)V
    .locals 1

    .prologue
    .line 166884
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166885
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 166886
    new-instance v0, LX/104;

    invoke-direct {v0, p0, p1}, LX/104;-><init>(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;Landroid/view/View$OnClickListener;)V

    .line 166887
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->a(Landroid/view/View$OnTouchListener;)V

    .line 166888
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 166889
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 5

    .prologue
    .line 166800
    iget-boolean v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->q:Z

    if-eqz v0, :cond_0

    .line 166801
    :goto_0
    return-void

    .line 166802
    :cond_0
    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166803
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->e()V

    .line 166804
    :cond_1
    :goto_1
    sget-object v0, LX/0zI;->SCOPED:LX/0zI;

    invoke-virtual {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setSearchBoxType(LX/0zI;)V

    goto :goto_0

    .line 166805
    :cond_2
    invoke-static {p1}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 166806
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 166807
    invoke-virtual {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(LX/103;)V

    goto :goto_1

    .line 166808
    :cond_3
    invoke-static {p1}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166809
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0820b7

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 166810
    iget-object v4, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v4, v4

    .line 166811
    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setScopedHintString(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/Cwd;)V
    .locals 8

    .prologue
    const/16 v7, 0xff

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 166812
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->b()V

    .line 166813
    iput-boolean v2, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->r:Z

    .line 166814
    iput-boolean v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->q:Z

    .line 166815
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 166816
    const v3, 0x3f0ccccd    # 0.55f

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 166817
    iget-object v3, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v3, v0}, Lcom/facebook/ui/search/SearchEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 166818
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->i:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 166819
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0820b8

    new-array v5, v1, [Ljava/lang/Object;

    .line 166820
    iget-object v6, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v6, v6

    .line 166821
    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setText(Ljava/lang/CharSequence;)V

    .line 166822
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    const/16 v3, 0x80

    invoke-static {v3, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setTextColor(I)V

    .line 166823
    iget-object v3, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->j:Lcom/facebook/search/titlebar/ScopedSearchPillView;

    sget-object v0, LX/Cwd;->SCOPED:LX/Cwd;

    if-ne p2, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/search/titlebar/ScopedSearchPillView;->setEnabled(Z)V

    .line 166824
    invoke-static {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V

    .line 166825
    invoke-static {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->m(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V

    .line 166826
    return-void

    :cond_0
    move v0, v2

    .line 166827
    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 166828
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166829
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 166830
    :cond_0
    return-void
.end method

.method public final b(LX/Ffo;)V
    .locals 1

    .prologue
    .line 166831
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 166832
    return-void
.end method

.method public final b(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 166833
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0820b6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 166834
    iget-object v3, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v3, v3

    .line 166835
    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setScopedHintString(Ljava/lang/String;)V

    .line 166836
    invoke-direct {p0, v4}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setNullStateBadgeEnabled(Z)V

    .line 166837
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 166838
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->b:LX/0Uh;

    sget v1, LX/2SU;->e:I

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 166839
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->d()V

    .line 166840
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 166841
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 166842
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0820b3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setScopedHintString(Ljava/lang/String;)V

    .line 166843
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setNullStateBadgeEnabled(Z)V

    .line 166844
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 166845
    sget-object v0, LX/103;->MARKETPLACE:LX/103;

    invoke-virtual {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(LX/103;)V

    .line 166846
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 166847
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0822d6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166848
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v1

    .line 166849
    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 166850
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setNullStateBadgeEnabled(Z)V

    .line 166851
    return-void
.end method

.method public getSearchEditText()Lcom/facebook/ui/search/SearchEditText;
    .locals 1

    .prologue
    .line 166799
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 166852
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 166853
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 166854
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 166855
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 166856
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    const/4 v2, -0x1

    invoke-static {v1, v2}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 166857
    invoke-direct {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->l()V

    .line 166858
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3bd864d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 166859
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 166860
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a:LX/0wS;

    invoke-virtual {v1}, LX/0wS;->a()V

    .line 166861
    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 166862
    const/16 v1, 0x2d

    const v2, -0x5d10c286

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 166863
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 166864
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41900000    # 18.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    sub-int/2addr p1, v0

    .line 166865
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 166866
    return-void
.end method

.method public setNullStateBadge(I)V
    .locals 1

    .prologue
    .line 166867
    iput p1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->t:I

    .line 166868
    iget-boolean v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->s:Z

    invoke-direct {p0, v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->setNullStateBadgeEnabled(Z)V

    .line 166869
    return-void
.end method

.method public setOnInitStateLeftListener(LX/F5P;)V
    .locals 0

    .prologue
    .line 166870
    iput-object p1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 166871
    return-void
.end method

.method public setOnPillClickedListener(LX/Fh5;)V
    .locals 0

    .prologue
    .line 166872
    iput-object p1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->p:LX/Fh5;

    .line 166873
    return-void
.end method

.method public setSearchBoxType(LX/0zI;)V
    .locals 3

    .prologue
    .line 166874
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->l:LX/0zI;

    if-ne v0, p1, :cond_0

    .line 166875
    :goto_0
    return-void

    .line 166876
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->l:LX/0zI;

    .line 166877
    sget-object v0, LX/102;->a:[I

    invoke-virtual {p1}, LX/0zI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 166878
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    const/16 v2, 0xcc

    invoke-virtual {v1, v2}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 166879
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x99

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    .line 166880
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    iget-object v1, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 166881
    invoke-virtual {p0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 166882
    iget-object v0, p0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 166883
    return-void
.end method
