.class public Lcom/facebook/location/ImmutableLocation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/location/Location;

.field private final b:J

.field private final c:J

.field private final d:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166660
    new-instance v0, LX/0z6;

    invoke-direct {v0}, LX/0z6;-><init>()V

    sput-object v0, Lcom/facebook/location/ImmutableLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/location/Location;)V
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 166675
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/location/ImmutableLocation;-><init>(Landroid/location/Location;JJF)V

    .line 166676
    return-void
.end method

.method private constructor <init>(Landroid/location/Location;JJF)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 166667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166668
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "location must have latitude/longitude"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 166669
    invoke-static {p1}, Lcom/facebook/location/ImmutableLocation;->d(Landroid/location/Location;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    .line 166670
    iput-wide p2, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    .line 166671
    iput-wide p4, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    .line 166672
    iput p6, p0, Lcom/facebook/location/ImmutableLocation;->d:F

    .line 166673
    return-void

    .line 166674
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/location/Location;JJFB)V
    .locals 0

    .prologue
    .line 166666
    invoke-direct/range {p0 .. p6}, Lcom/facebook/location/ImmutableLocation;-><init>(Landroid/location/Location;JJF)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    .line 166664
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/location/ImmutableLocation;-><init>(Landroid/location/Location;JJF)V

    .line 166665
    return-void
.end method

.method public static a(DD)LX/0z7;
    .locals 8

    .prologue
    .line 166663
    new-instance v1, LX/0z7;

    move-wide v2, p0

    move-wide v4, p2

    invoke-direct/range {v1 .. v5}, LX/0z7;-><init>(DD)V

    return-object v1
.end method

.method public static a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;
    .locals 1

    .prologue
    .line 166661
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 166662
    new-instance v0, Lcom/facebook/location/ImmutableLocation;

    invoke-direct {v0, p0}, Lcom/facebook/location/ImmutableLocation;-><init>(Landroid/location/Location;)V

    return-object v0
.end method

.method public static b(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;
    .locals 1
    .param p0    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 166657
    if-nez p0, :cond_0

    .line 166658
    const/4 v0, 0x0

    .line 166659
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/facebook/location/ImmutableLocation;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;
    .locals 1
    .param p0    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 166654
    invoke-static {p0}, LX/0z8;->a(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166655
    invoke-static {p0}, Lcom/facebook/location/ImmutableLocation;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 166656
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/location/Location;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 166653
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    return-object v0
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 166652
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 166651
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public final c()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166648
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166649
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 166650
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166677
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166678
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 166679
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 166601
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166608
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166609
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 166610
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 166611
    if-ne p0, p1, :cond_1

    .line 166612
    :cond_0
    :goto_0
    return v0

    .line 166613
    :cond_1
    instance-of v2, p1, Lcom/facebook/location/ImmutableLocation;

    if-nez v2, :cond_2

    move v0, v1

    .line 166614
    goto :goto_0

    .line 166615
    :cond_2
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 166616
    iget v2, p0, Lcom/facebook/location/ImmutableLocation;->d:F

    iget v3, p1, Lcom/facebook/location/ImmutableLocation;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    iget-wide v4, p1, Lcom/facebook/location/ImmutableLocation;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    iget-wide v4, p1, Lcom/facebook/location/ImmutableLocation;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    iget-object v3, p1, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166617
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166618
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 166619
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166620
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    .line 166621
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 166622
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 166623
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()LX/0am;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166624
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 166625
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 166626
    :goto_0
    return-object v0

    .line 166627
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 166628
    iget-object v2, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    .line 166629
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_2

    .line 166630
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 166631
    :goto_2
    move-object v0, v2

    .line 166632
    goto :goto_0

    .line 166633
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 166634
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 166602
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 166603
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    iget-wide v4, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    shr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 166604
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    iget-wide v4, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    shr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 166605
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/location/ImmutableLocation;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 166606
    return v0

    .line 166607
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final i()LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166635
    iget-wide v0, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166636
    iget-wide v0, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()LX/0am;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166637
    iget v0, p0, Lcom/facebook/location/ImmutableLocation;->d:F

    float-to-double v0, v0

    const-wide/16 v2, 0x0

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    .line 166638
    const-string v6, "tolerance"

    invoke-static {v6, v4, v5}, LX/0z9;->a(Ljava/lang/String;D)D

    .line 166639
    sub-double v6, v0, v2

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->copySign(DD)D

    move-result-wide v6

    cmpg-double v6, v6, v4

    if-lez v6, :cond_0

    cmpl-double v6, v0, v2

    if-eqz v6, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    const/4 v6, 0x1

    :goto_0
    move v0, v6

    .line 166640
    if-eqz v0, :cond_1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    iget v0, p0, Lcom/facebook/location/ImmutableLocation;->d:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final l()Landroid/location/Location;
    .locals 1

    .prologue
    .line 166641
    iget-object v0, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-static {v0}, Lcom/facebook/location/ImmutableLocation;->d(Landroid/location/Location;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 166642
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "location"

    iget-object v2, p0, Lcom/facebook/location/ImmutableLocation;->a:Landroid/location/Location;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "geofenceStartTimestampMs"

    iget-wide v2, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "geofenceEndTimestampMs"

    iget-wide v2, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "geofenceRadiusMeters"

    iget v2, p0, Lcom/facebook/location/ImmutableLocation;->d:F

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;F)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 166643
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 166644
    iget-wide v0, p0, Lcom/facebook/location/ImmutableLocation;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 166645
    iget-wide v0, p0, Lcom/facebook/location/ImmutableLocation;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 166646
    iget v0, p0, Lcom/facebook/location/ImmutableLocation;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 166647
    return-void
.end method
