.class public Lcom/facebook/location/FbLocationOperationParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/location/FbLocationOperationParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0yF;

.field public final b:J

.field public final c:F

.field public final d:J

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final g:J

.field public final h:J

.field public final i:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247703
    new-instance v0, LX/1S6;

    invoke-direct {v0}, LX/1S6;-><init>()V

    sput-object v0, Lcom/facebook/location/FbLocationOperationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/1S7;)V
    .locals 2

    .prologue
    .line 247704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247705
    iget-object v0, p1, LX/1S7;->a:LX/0yF;

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->a:LX/0yF;

    .line 247706
    iget-wide v0, p1, LX/1S7;->b:J

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->b:J

    .line 247707
    iget v0, p1, LX/1S7;->c:F

    iput v0, p0, Lcom/facebook/location/FbLocationOperationParams;->c:F

    .line 247708
    iget-wide v0, p1, LX/1S7;->d:J

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->d:J

    .line 247709
    iget-object v0, p1, LX/1S7;->e:LX/0am;

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    .line 247710
    iget-object v0, p1, LX/1S7;->f:LX/0am;

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    .line 247711
    iget-wide v0, p1, LX/1S7;->g:J

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->g:J

    .line 247712
    iget-wide v0, p1, LX/1S7;->h:J

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->h:J

    .line 247713
    iget v0, p1, LX/1S7;->i:F

    iput v0, p0, Lcom/facebook/location/FbLocationOperationParams;->i:F

    .line 247714
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 247687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247688
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0yF;->valueOf(Ljava/lang/String;)LX/0yF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->a:LX/0yF;

    .line 247689
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->b:J

    .line 247690
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/location/FbLocationOperationParams;->c:F

    .line 247691
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->d:J

    .line 247692
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 247693
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    .line 247694
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 247695
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    .line 247696
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->g:J

    .line 247697
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->h:J

    .line 247698
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/location/FbLocationOperationParams;->i:F

    .line 247699
    return-void

    .line 247700
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    goto :goto_0

    .line 247701
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    goto :goto_1
.end method

.method public static a(LX/0yF;)LX/1S7;
    .locals 1

    .prologue
    .line 247702
    new-instance v0, LX/1S7;

    invoke-direct {v0, p0}, LX/1S7;-><init>(LX/0yF;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 247686
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 247685
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "priority"

    iget-object v2, p0, Lcom/facebook/location/FbLocationOperationParams;->a:LX/0yF;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "desired_age_ms"

    iget-wide v2, p0, Lcom/facebook/location/FbLocationOperationParams;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "desired_accuracy_meters"

    iget v2, p0, Lcom/facebook/location/FbLocationOperationParams;->c:F

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;F)LX/237;

    move-result-object v0

    const-string v1, "timeout_ms"

    iget-wide v2, p0, Lcom/facebook/location/FbLocationOperationParams;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "age_limit_ms"

    iget-object v2, p0, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "accuracy_limit_meters"

    iget-object v2, p0, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "time_between_updates_ms"

    iget-wide v2, p0, Lcom/facebook/location/FbLocationOperationParams;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "significant_time_improvement_ms"

    iget-wide v2, p0, Lcom/facebook/location/FbLocationOperationParams;->h:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "significant_accuracy_improvement_ratio"

    iget v2, p0, Lcom/facebook/location/FbLocationOperationParams;->i:F

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;F)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 247669
    iget-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->a:LX/0yF;

    invoke-virtual {v0}, LX/0yF;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 247670
    iget-wide v4, p0, Lcom/facebook/location/FbLocationOperationParams;->b:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 247671
    iget v0, p0, Lcom/facebook/location/FbLocationOperationParams;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 247672
    iget-wide v4, p0, Lcom/facebook/location/FbLocationOperationParams;->d:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 247673
    iget-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 247674
    iget-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247675
    iget-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 247676
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 247677
    iget-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247678
    iget-object v0, p0, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 247679
    :cond_1
    iget-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 247680
    iget-wide v0, p0, Lcom/facebook/location/FbLocationOperationParams;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 247681
    iget v0, p0, Lcom/facebook/location/FbLocationOperationParams;->i:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 247682
    return-void

    :cond_2
    move v0, v2

    .line 247683
    goto :goto_0

    :cond_3
    move v1, v2

    .line 247684
    goto :goto_1
.end method
