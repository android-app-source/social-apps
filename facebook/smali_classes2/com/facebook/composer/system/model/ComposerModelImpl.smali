.class public Lcom/facebook/composer/system/model/ComposerModelImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0io;
.implements LX/0ip;
.implements LX/0iq;
.implements LX/0ir;
.implements LX/0is;
.implements LX/0it;
.implements LX/0iu;
.implements LX/0iv;
.implements LX/0iw;
.implements LX/0ix;
.implements LX/0iy;
.implements LX/0iz;
.implements LX/0j0;
.implements LX/0j1;
.implements LX/0j2;
.implements LX/0j3;
.implements LX/0j4;
.implements LX/0j5;
.implements LX/0j6;
.implements LX/0j7;
.implements LX/0j8;
.implements LX/0j9;
.implements LX/0jA;
.implements LX/0jB;
.implements LX/0jC;
.implements LX/0jD;
.implements LX/0jE;
.implements LX/0jF;
.implements LX/0jG;
.implements LX/0jH;
.implements LX/0jI;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/system/model/ComposerModelImpl$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/system/model/ComposerModelImplSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/system/model/ComposerModelImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Z

.field public final B:J

.field public final C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

.field public final E:J

.field public final F:I

.field public final G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final H:Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final L:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final M:LX/5Rn;

.field public final N:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final O:I

.field public final P:Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final S:I

.field public final T:Ljava/lang/String;

.field public final U:J

.field public final V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public final Z:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final a:Lcom/facebook/share/model/ComposerAppAttribution;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final ac:Z

.field public final ad:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

.field public final d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

.field public final e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

.field public final g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field public final h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public final i:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

.field public final p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

.field public final r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

.field public final s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

.field public final t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

.field public final u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final v:Z

.field public final w:Z

.field public final x:Z

.field public final y:Z

.field public final z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 122598
    const-class v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 122587
    const-class v0, Lcom/facebook/composer/system/model/ComposerModelImplSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122586
    new-instance v0, LX/HvC;

    invoke-direct {v0}, LX/HvC;-><init>()V

    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 122457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122458
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 122459
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 122460
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/composer/attachments/ComposerAttachment;

    move v1, v2

    .line 122461
    :goto_1
    array-length v0, v4

    if-ge v1, v0, :cond_1

    .line 122462
    sget-object v0, Lcom/facebook/composer/attachments/ComposerAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 122463
    aput-object v0, v4, v1

    .line 122464
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 122465
    :cond_0
    sget-object v0, Lcom/facebook/share/model/ComposerAppAttribution;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    goto :goto_0

    .line 122466
    :cond_1
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    .line 122467
    sget-object v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 122468
    sget-object v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 122469
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 122470
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 122471
    :goto_2
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 122472
    sget-object v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 122473
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 122474
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 122475
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    move v1, v2

    .line 122476
    :goto_3
    if-ge v1, v5, :cond_3

    .line 122477
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 122478
    sget-object v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    .line 122479
    invoke-interface {v4, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122480
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 122481
    :cond_2
    sget-object v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    goto :goto_2

    .line 122482
    :cond_3
    invoke-static {v4}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    .line 122483
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 122484
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 122485
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_5

    move v0, v3

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    .line 122486
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_6

    move v0, v3

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    .line 122487
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_7

    move v0, v3

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    .line 122488
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_8

    move v0, v3

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    .line 122489
    sget-object v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 122490
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    .line 122491
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 122492
    :goto_9
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 122493
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 122494
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 122495
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 122496
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    .line 122497
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 122498
    :goto_a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_b

    move v0, v3

    :goto_b
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    .line 122499
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_c

    move v0, v3

    :goto_c
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    .line 122500
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_d

    move v0, v3

    :goto_d
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    .line 122501
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_e

    move v0, v3

    :goto_e
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    .line 122502
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_f

    move v0, v3

    :goto_f
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    .line 122503
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_10

    move v0, v3

    :goto_10
    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    .line 122504
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    .line 122505
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_11

    .line 122506
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 122507
    :goto_11
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 122508
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    .line 122509
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    .line 122510
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_12

    .line 122511
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 122512
    :goto_12
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_13

    .line 122513
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 122514
    :goto_13
    sget-object v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 122515
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_14

    .line 122516
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 122517
    :goto_14
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_15

    .line 122518
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 122519
    :goto_15
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_16

    .line 122520
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 122521
    :goto_16
    invoke-static {}, LX/5Rn;->values()[LX/5Rn;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    .line 122522
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_17

    .line 122523
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    .line 122524
    :goto_17
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    .line 122525
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_18

    .line 122526
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 122527
    :goto_18
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    move v0, v2

    .line 122528
    :goto_19
    array-length v4, v1

    if-ge v0, v4, :cond_19

    .line 122529
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 122530
    aput-object v4, v1, v0

    .line 122531
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 122532
    :cond_4
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 122533
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 122534
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 122535
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 122536
    goto/16 :goto_8

    .line 122537
    :cond_9
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    goto/16 :goto_9

    .line 122538
    :cond_a
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 122539
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 122540
    goto/16 :goto_c

    :cond_d
    move v0, v2

    .line 122541
    goto/16 :goto_d

    :cond_e
    move v0, v2

    .line 122542
    goto/16 :goto_e

    :cond_f
    move v0, v2

    .line 122543
    goto/16 :goto_f

    :cond_10
    move v0, v2

    .line 122544
    goto/16 :goto_10

    .line 122545
    :cond_11
    sget-object v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    goto/16 :goto_11

    .line 122546
    :cond_12
    sget-object v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    goto/16 :goto_12

    .line 122547
    :cond_13
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    goto/16 :goto_13

    .line 122548
    :cond_14
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    goto/16 :goto_14

    .line 122549
    :cond_15
    sget-object v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    goto/16 :goto_15

    .line 122550
    :cond_16
    sget-object v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    goto/16 :goto_16

    .line 122551
    :cond_17
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    goto/16 :goto_17

    .line 122552
    :cond_18
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    goto/16 :goto_18

    .line 122553
    :cond_19
    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    .line 122554
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1a

    .line 122555
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 122556
    :goto_1a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    .line 122557
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    .line 122558
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    .line 122559
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1b

    .line 122560
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 122561
    :goto_1b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1c

    .line 122562
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 122563
    :goto_1c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1d

    .line 122564
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 122565
    :goto_1d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move v1, v2

    .line 122566
    :goto_1e
    array-length v0, v4

    if-ge v1, v0, :cond_1e

    .line 122567
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 122568
    aput-object v0, v4, v1

    .line 122569
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1e

    .line 122570
    :cond_1a
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_1a

    .line 122571
    :cond_1b
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    goto :goto_1b

    .line 122572
    :cond_1c
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    goto :goto_1c

    .line 122573
    :cond_1d
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    goto :goto_1d

    .line 122574
    :cond_1e
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    .line 122575
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1f

    .line 122576
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 122577
    :goto_1f
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 122578
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 122579
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_20

    :goto_20
    iput-boolean v3, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    .line 122580
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_21

    .line 122581
    iput-object v7, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 122582
    :goto_21
    return-void

    .line 122583
    :cond_1f
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    goto :goto_1f

    :cond_20
    move v3, v2

    .line 122584
    goto :goto_20

    .line 122585
    :cond_21
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    goto :goto_21
.end method

.method public constructor <init>(Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;)V
    .locals 2

    .prologue
    .line 122399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122400
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->r:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 122401
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->s:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    .line 122402
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->t:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 122403
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->u:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 122404
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->v:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 122405
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->w:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 122406
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->x:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 122407
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->y:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 122408
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->z:LX/0P1;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    .line 122409
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->A:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 122410
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->B:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    .line 122411
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->C:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    .line 122412
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->D:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    .line 122413
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->E:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    .line 122414
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->F:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 122415
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->G:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 122416
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->H:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 122417
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->I:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 122418
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->J:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 122419
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->K:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 122420
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->L:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 122421
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->M:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    .line 122422
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->N:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    .line 122423
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->O:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    .line 122424
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->P:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    .line 122425
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Q:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    .line 122426
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->R:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    .line 122427
    iget-wide v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->S:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    .line 122428
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->T:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 122429
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->U:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 122430
    iget-wide v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->V:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    .line 122431
    iget v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->W:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    .line 122432
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->X:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 122433
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Y:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 122434
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Z:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 122435
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 122436
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ab:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 122437
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ac:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 122438
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ad:LX/5Rn;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Rn;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    .line 122439
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ae:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    .line 122440
    iget v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->af:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    .line 122441
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ag:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 122442
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ah:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    .line 122443
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ai:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 122444
    iget v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aj:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    .line 122445
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ak:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    .line 122446
    iget-wide v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->al:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    .line 122447
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->am:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 122448
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->an:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 122449
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ao:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 122450
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ap:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    .line 122451
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aq:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 122452
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ar:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 122453
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 122454
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->at:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    .line 122455
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->au:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 122456
    return-void
.end method

.method public static a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 2

    .prologue
    .line 122398
    new-instance v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 2

    .prologue
    .line 122397
    new-instance v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-direct {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122271
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    return v0
.end method

.method public final b()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122395
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    return v0
.end method

.method public final c()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122394
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    return v0
.end method

.method public final d()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122393
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 122396
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122392
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 122275
    if-ne p0, p1, :cond_1

    .line 122276
    :cond_0
    :goto_0
    return v0

    .line 122277
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    if-nez v2, :cond_2

    move v0, v1

    .line 122278
    goto :goto_0

    .line 122279
    :cond_2
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 122280
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 122281
    goto :goto_0

    .line 122282
    :cond_3
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 122283
    goto :goto_0

    .line 122284
    :cond_4
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 122285
    goto :goto_0

    .line 122286
    :cond_5
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 122287
    goto :goto_0

    .line 122288
    :cond_6
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 122289
    goto :goto_0

    .line 122290
    :cond_7
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 122291
    goto :goto_0

    .line 122292
    :cond_8
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 122293
    goto :goto_0

    .line 122294
    :cond_9
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 122295
    goto :goto_0

    .line 122296
    :cond_a
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 122297
    goto :goto_0

    .line 122298
    :cond_b
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 122299
    goto :goto_0

    .line 122300
    :cond_c
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 122301
    goto/16 :goto_0

    .line 122302
    :cond_d
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 122303
    goto/16 :goto_0

    .line 122304
    :cond_e
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 122305
    goto/16 :goto_0

    .line 122306
    :cond_f
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 122307
    goto/16 :goto_0

    .line 122308
    :cond_10
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 122309
    goto/16 :goto_0

    .line 122310
    :cond_11
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 122311
    goto/16 :goto_0

    .line 122312
    :cond_12
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 122313
    goto/16 :goto_0

    .line 122314
    :cond_13
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 122315
    goto/16 :goto_0

    .line 122316
    :cond_14
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 122317
    goto/16 :goto_0

    .line 122318
    :cond_15
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 122319
    goto/16 :goto_0

    .line 122320
    :cond_16
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    move v0, v1

    .line 122321
    goto/16 :goto_0

    .line 122322
    :cond_17
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 122323
    goto/16 :goto_0

    .line 122324
    :cond_18
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 122325
    goto/16 :goto_0

    .line 122326
    :cond_19
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 122327
    goto/16 :goto_0

    .line 122328
    :cond_1a
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 122329
    goto/16 :goto_0

    .line 122330
    :cond_1b
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 122331
    goto/16 :goto_0

    .line 122332
    :cond_1c
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 122333
    goto/16 :goto_0

    .line 122334
    :cond_1d
    iget-wide v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    iget-wide v4, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1e

    move v0, v1

    .line 122335
    goto/16 :goto_0

    .line 122336
    :cond_1e
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    move v0, v1

    .line 122337
    goto/16 :goto_0

    .line 122338
    :cond_1f
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    move v0, v1

    .line 122339
    goto/16 :goto_0

    .line 122340
    :cond_20
    iget-wide v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    iget-wide v4, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_21

    move v0, v1

    .line 122341
    goto/16 :goto_0

    .line 122342
    :cond_21
    iget v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    iget v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    if-eq v2, v3, :cond_22

    move v0, v1

    .line 122343
    goto/16 :goto_0

    .line 122344
    :cond_22
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move v0, v1

    .line 122345
    goto/16 :goto_0

    .line 122346
    :cond_23
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    move v0, v1

    .line 122347
    goto/16 :goto_0

    .line 122348
    :cond_24
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    move v0, v1

    .line 122349
    goto/16 :goto_0

    .line 122350
    :cond_25
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    move v0, v1

    .line 122351
    goto/16 :goto_0

    .line 122352
    :cond_26
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    move v0, v1

    .line 122353
    goto/16 :goto_0

    .line 122354
    :cond_27
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    move v0, v1

    .line 122355
    goto/16 :goto_0

    .line 122356
    :cond_28
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_29

    move v0, v1

    .line 122357
    goto/16 :goto_0

    .line 122358
    :cond_29
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2a

    move v0, v1

    .line 122359
    goto/16 :goto_0

    .line 122360
    :cond_2a
    iget v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    iget v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    if-eq v2, v3, :cond_2b

    move v0, v1

    .line 122361
    goto/16 :goto_0

    .line 122362
    :cond_2b
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2c

    move v0, v1

    .line 122363
    goto/16 :goto_0

    .line 122364
    :cond_2c
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2d

    move v0, v1

    .line 122365
    goto/16 :goto_0

    .line 122366
    :cond_2d
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2e

    move v0, v1

    .line 122367
    goto/16 :goto_0

    .line 122368
    :cond_2e
    iget v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    iget v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    if-eq v2, v3, :cond_2f

    move v0, v1

    .line 122369
    goto/16 :goto_0

    .line 122370
    :cond_2f
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_30

    move v0, v1

    .line 122371
    goto/16 :goto_0

    .line 122372
    :cond_30
    iget-wide v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    iget-wide v4, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_31

    move v0, v1

    .line 122373
    goto/16 :goto_0

    .line 122374
    :cond_31
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_32

    move v0, v1

    .line 122375
    goto/16 :goto_0

    .line 122376
    :cond_32
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_33

    move v0, v1

    .line 122377
    goto/16 :goto_0

    .line 122378
    :cond_33
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_34

    move v0, v1

    .line 122379
    goto/16 :goto_0

    .line 122380
    :cond_34
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_35

    move v0, v1

    .line 122381
    goto/16 :goto_0

    .line 122382
    :cond_35
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_36

    move v0, v1

    .line 122383
    goto/16 :goto_0

    .line 122384
    :cond_36
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_37

    move v0, v1

    .line 122385
    goto/16 :goto_0

    .line 122386
    :cond_37
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_38

    move v0, v1

    .line 122387
    goto/16 :goto_0

    .line 122388
    :cond_38
    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    iget-boolean v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    if-eq v2, v3, :cond_39

    move v0, v1

    .line 122389
    goto/16 :goto_0

    .line 122390
    :cond_39
    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iget-object v3, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 122391
    goto/16 :goto_0
.end method

.method public final f()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122274
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    return v0
.end method

.method public final g()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122273
    iget v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    return v0
.end method

.method public getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "app_attribution"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122272
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    return-object v0
.end method

.method public getAttachments()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122592
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    return-object v0
.end method

.method public getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_logging_data"
    .end annotation

    .prologue
    .line 122599
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    return-object v0
.end method

.method public getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "configuration"
    .end annotation

    .prologue
    .line 122600
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    return-object v0
.end method

.method public getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "date_info"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122601
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    return-object v0
.end method

.method public getLastXyTagChangeTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_xy_tag_change_time"
    .end annotation

    .prologue
    .line 122602
    iget-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    return-wide v0
.end method

.method public getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "location_info"
    .end annotation

    .prologue
    .line 122603
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    return-object v0
.end method

.method public getMarketplaceId()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "marketplace_id"
    .end annotation

    .prologue
    .line 122119
    iget-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    return-wide v0
.end method

.method public getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_object"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122604
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    return-object v0
.end method

.method public getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122597
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    return-object v0
.end method

.method public getPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "privacy_override"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122596
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_attachment"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122595
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    return-object v0
.end method

.method public getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_analytics"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122594
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    return-object v0
.end method

.method public getPublishMode()LX/5Rn;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_mode"
    .end annotation

    .prologue
    .line 122593
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    return-object v0
.end method

.method public getPublishScheduleTime()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_schedule_time"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122589
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    return-object v0
.end method

.method public getRating()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rating"
    .end annotation

    .prologue
    .line 122591
    iget v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    return v0
.end method

.method public getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "referenced_sticker_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122590
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    return-object v0
.end method

.method public getRemovedUrls()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "removed_urls"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122588
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    return-object v0
.end method

.method public getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rich_text_style"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122117
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    return-object v0
.end method

.method public getSavedSessionLoadAttempts()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "saved_session_load_attempts"
    .end annotation

    .prologue
    .line 122116
    iget v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    return v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .prologue
    .line 122115
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    return-object v0
.end method

.method public getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_params"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122114
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    return-object v0
.end method

.method public getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "slideshow_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122113
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    return-object v0
.end method

.method public getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "storyline_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122112
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    return-object v0
.end method

.method public getTaggedUsers()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_users"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122111
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    return-object v0
.end method

.method public getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_album"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122110
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_data"
    .end annotation

    .prologue
    .line 122109
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    return-object v0
.end method

.method public getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_with_entities"
    .end annotation

    .prologue
    .line 122108
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "viewer_coordinates"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122107
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    return-object v0
.end method

.method public final h()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122106
    iget-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    return-wide v0
.end method

.method public hasPrivacyChanged()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_privacy_changed"
    .end annotation

    .prologue
    .line 122105
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 122104
    const/16 v0, 0x38

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-wide v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-wide v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    iget v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x29

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    iget v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    iget-wide v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    iget-boolean v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x37

    iget-object v2, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122103
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    return-object v0
.end method

.method public isBackoutDraft()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_backout_draft"
    .end annotation

    .prologue
    .line 122102
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    return v0
.end method

.method public isFeedOnlyPost()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_feed_only_post"
    .end annotation

    .prologue
    .line 122101
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    return v0
.end method

.method public isTargetAlbumNew()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_target_album_new"
    .end annotation

    .prologue
    .line 122263
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    return v0
.end method

.method public isUserSelectedTags()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_selected_tags"
    .end annotation

    .prologue
    .line 122270
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    return v0
.end method

.method public final j()Lcom/facebook/friendsharing/inspiration/model/CameraState;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122269
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    return-object v0
.end method

.method public final k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122268
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    return-object v0
.end method

.method public final l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122267
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    return-object v0
.end method

.method public final m()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122266
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    return-object v0
.end method

.method public final n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122265
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    return-object v0
.end method

.method public final o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122264
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122118
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    return-object v0
.end method

.method public final q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122262
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    return-object v0
.end method

.method public final r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122261
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    return-object v0
.end method

.method public final s()LX/0P1;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122260
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    return-object v0
.end method

.method public final t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122259
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    return-object v0
.end method

.method public final u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122258
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    return-object v0
.end method

.method public final v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 122257
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 122120
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    if-nez v0, :cond_0

    .line 122121
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122122
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122123
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v1, v3

    :goto_1
    if-ge v1, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 122124
    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/attachments/ComposerAttachment;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 122126
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122127
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/share/model/ComposerAppAttribution;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 122128
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122129
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122130
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    if-nez v0, :cond_2

    .line 122131
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122132
    :goto_2
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122133
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122134
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122135
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122136
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 122137
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122138
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_3

    .line 122139
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122140
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_2

    .line 122141
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    if-nez v0, :cond_4

    .line 122142
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122143
    :goto_4
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122144
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122145
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122146
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122147
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122148
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    if-nez v0, :cond_9

    .line 122149
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122150
    :goto_9
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122151
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122152
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122153
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122154
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    if-nez v0, :cond_a

    .line 122155
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122156
    :goto_a
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122157
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    if-eqz v0, :cond_c

    move v0, v2

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122158
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    if-eqz v0, :cond_d

    move v0, v2

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122159
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    if-eqz v0, :cond_e

    move v0, v2

    :goto_e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122160
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    if-eqz v0, :cond_f

    move v0, v2

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122161
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    if-eqz v0, :cond_10

    move v0, v2

    :goto_10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122162
    iget-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 122163
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    if-nez v0, :cond_11

    .line 122164
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122165
    :goto_11
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122166
    iget-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 122167
    iget v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122168
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_12

    .line 122169
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122170
    :goto_12
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v0, :cond_13

    .line 122171
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122172
    :goto_13
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122173
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v0, :cond_14

    .line 122174
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122175
    :goto_14
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-nez v0, :cond_15

    .line 122176
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122177
    :goto_15
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-nez v0, :cond_16

    .line 122178
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122179
    :goto_16
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    invoke-virtual {v0}, LX/5Rn;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122180
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    if-nez v0, :cond_17

    .line 122181
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122182
    :goto_17
    iget v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122183
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    if-nez v0, :cond_18

    .line 122184
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122185
    :goto_18
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122186
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v1, v3

    :goto_19
    if-ge v1, v4, :cond_19

    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 122187
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122188
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_19

    .line 122189
    :cond_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122190
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_4

    :cond_5
    move v0, v3

    .line 122191
    goto/16 :goto_5

    :cond_6
    move v0, v3

    .line 122192
    goto/16 :goto_6

    :cond_7
    move v0, v3

    .line 122193
    goto/16 :goto_7

    :cond_8
    move v0, v3

    .line 122194
    goto/16 :goto_8

    .line 122195
    :cond_9
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122196
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_9

    .line 122197
    :cond_a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122198
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_a

    :cond_b
    move v0, v3

    .line 122199
    goto/16 :goto_b

    :cond_c
    move v0, v3

    .line 122200
    goto/16 :goto_c

    :cond_d
    move v0, v3

    .line 122201
    goto/16 :goto_d

    :cond_e
    move v0, v3

    .line 122202
    goto/16 :goto_e

    :cond_f
    move v0, v3

    .line 122203
    goto/16 :goto_f

    :cond_10
    move v0, v3

    .line 122204
    goto/16 :goto_10

    .line 122205
    :cond_11
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122206
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_11

    .line 122207
    :cond_12
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122208
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_12

    .line 122209
    :cond_13
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122210
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_13

    .line 122211
    :cond_14
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122212
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto/16 :goto_14

    .line 122213
    :cond_15
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122214
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_15

    .line 122215
    :cond_16
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122216
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_16

    .line 122217
    :cond_17
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122218
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_17

    .line 122219
    :cond_18
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122220
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_18

    .line 122221
    :cond_19
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-nez v0, :cond_1a

    .line 122222
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122223
    :goto_1a
    iget v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122224
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122225
    iget-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 122226
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    if-nez v0, :cond_1b

    .line 122227
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122228
    :goto_1b
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    if-nez v0, :cond_1c

    .line 122229
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122230
    :goto_1c
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    if-nez v0, :cond_1d

    .line 122231
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122232
    :goto_1d
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122233
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v1, v3

    :goto_1e
    if-ge v1, v4, :cond_1e

    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 122234
    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122235
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1e

    .line 122236
    :cond_1a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122237
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1a

    .line 122238
    :cond_1b
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122239
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1b

    .line 122240
    :cond_1c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122241
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1c

    .line 122242
    :cond_1d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122243
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1d

    .line 122244
    :cond_1e
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v0, :cond_1f

    .line 122245
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122246
    :goto_1f
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122247
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 122248
    iget-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    if-eqz v0, :cond_20

    move v0, v2

    :goto_20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 122249
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    if-nez v0, :cond_21

    .line 122250
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122251
    :goto_21
    return-void

    .line 122252
    :cond_1f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122253
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_1f

    :cond_20
    move v0, v3

    .line 122254
    goto :goto_20

    .line 122255
    :cond_21
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 122256
    iget-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocation;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_21
.end method
