.class public Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile j:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;


# instance fields
.field public final c:LX/0aG;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qL;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:LX/03V;

.field public h:Z

.field public i:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90974
    sget-object v0, LX/0dp;->e:LX/0Tn;

    sput-object v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->a:LX/0Tn;

    .line 90975
    const-class v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/1Ck;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/1qL;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90977
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->h:Z

    .line 90978
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->i:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    .line 90979
    iput-object p1, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->c:LX/0aG;

    .line 90980
    iput-object p2, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->d:LX/1Ck;

    .line 90981
    iput-object p3, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->e:LX/0Ot;

    .line 90982
    iput-object p4, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 90983
    iput-object p5, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->g:LX/03V;

    .line 90984
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;
    .locals 9

    .prologue
    .line 90985
    sget-object v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->j:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    if-nez v0, :cond_1

    .line 90986
    const-class v1, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    monitor-enter v1

    .line 90987
    :try_start_0
    sget-object v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->j:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90988
    if-eqz v2, :cond_0

    .line 90989
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90990
    new-instance v3, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    const/16 v6, 0x3d3

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;-><init>(LX/0aG;LX/1Ck;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)V

    .line 90991
    move-object v0, v3

    .line 90992
    sput-object v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->j:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90993
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90994
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90995
    :cond_1
    sget-object v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->j:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    return-object v0

    .line 90996
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V
    .locals 3
    .param p0    # Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 90998
    iput-object p1, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->i:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    .line 90999
    iget-object v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->a:LX/0Tn;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 91000
    return-void

    .line 91001
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 8

    .prologue
    .line 91002
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->h:Z

    .line 91003
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    .line 91004
    iget-object v6, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->d:LX/1Ck;

    const-string v7, "delete_session"

    iget-object v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->c:LX/0aG;

    const-string v1, "composer_delete_session"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x706aa268

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/1Ms;

    invoke-direct {v1, p0}, LX/1Ms;-><init>(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 91005
    return-void
.end method

.method public final c()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 91006
    iget-object v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91007
    :goto_0
    return-void

    .line 91008
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qL;

    .line 91009
    iget-object v1, v0, LX/1qL;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1qN;

    invoke-virtual {v1}, LX/1qN;->b()Ljava/lang/String;

    move-result-object v1

    .line 91010
    iget-object v2, v0, LX/1qL;->b:LX/0lB;

    const-class v3, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    invoke-virtual {v2, v1, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    move-object v0, v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 91011
    invoke-static {p0, v0}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    goto :goto_0

    .line 91012
    :catch_0
    move-exception v0

    .line 91013
    iget-object v1, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->g:LX/03V;

    const-string v2, "composer_session_load_failed"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91014
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 3

    .prologue
    .line 91015
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    .line 91016
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qL;

    invoke-virtual {v0}, LX/1qL;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 91017
    :goto_0
    return-void

    .line 91018
    :catch_0
    move-exception v0

    .line 91019
    iget-object v1, p0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->g:LX/03V;

    const-string v2, "composer_session_clear_data_failed"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
