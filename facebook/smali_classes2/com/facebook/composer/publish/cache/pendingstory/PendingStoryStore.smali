.class public Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile m:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;


# instance fields
.field private final b:LX/0aG;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/26s;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/03V;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/PendingStory;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/lang/Object;

.field private final i:LX/0ad;

.field private final j:LX/0qn;

.field private final k:LX/1CI;

.field private final l:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216039
    const-class v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/03V;LX/0ad;LX/0qn;LX/1CI;LX/0SG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Ot",
            "<",
            "LX/26s;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "LX/0qn;",
            "LX/1CI;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216027
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    .line 216028
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    .line 216029
    iput-object p1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b:LX/0aG;

    .line 216030
    iput-object p2, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->c:LX/0Ot;

    .line 216031
    iput-object p3, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 216032
    iput-object p4, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    .line 216033
    iput-object p5, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->f:LX/03V;

    .line 216034
    iput-object p6, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->i:LX/0ad;

    .line 216035
    iput-object p7, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->j:LX/0qn;

    .line 216036
    iput-object p8, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->k:LX/1CI;

    .line 216037
    iput-object p9, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->l:LX/0SG;

    .line 216038
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;
    .locals 13

    .prologue
    .line 216010
    sget-object v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->m:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    if-nez v0, :cond_1

    .line 216011
    const-class v1, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    monitor-enter v1

    .line 216012
    :try_start_0
    sget-object v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->m:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 216013
    if-eqz v2, :cond_0

    .line 216014
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 216015
    new-instance v3, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    const/16 v5, 0x3cc

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v7, 0x3bf

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v10

    check-cast v10, LX/0qn;

    .line 216016
    new-instance v12, LX/1CI;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v11

    check-cast v11, LX/0Zb;

    invoke-direct {v12, v11}, LX/1CI;-><init>(LX/0Zb;)V

    .line 216017
    move-object v11, v12

    .line 216018
    check-cast v11, LX/1CI;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;-><init>(LX/0aG;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/03V;LX/0ad;LX/0qn;LX/1CI;LX/0SG;)V

    .line 216019
    move-object v0, v3

    .line 216020
    sput-object v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->m:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216021
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 216022
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216023
    :cond_1
    sget-object v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->m:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    return-object v0

    .line 216024
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 216025
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 4

    .prologue
    .line 216001
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216002
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->k:LX/1CI;

    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    .line 216003
    iget-object v2, v0, LX/1CI;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fb4a_pending_stories"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "photo_upload_progress"

    .line 216004
    iput-object p1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 216005
    move-object v3, v3

    .line 216006
    const-string p1, "event"

    const-string p2, "queue_size"

    invoke-virtual {v3, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "queue_size"

    invoke-virtual {v3, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 216007
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x1f4

    if-le v0, v1, :cond_0

    .line 216008
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->f:LX/03V;

    const-string v1, "pending_story_too_many_stories"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reached "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stories"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216009
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 215988
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26s;

    invoke-virtual {v0}, LX/26s;->a()LX/0Px;

    move-result-object v2

    .line 215989
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215990
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/0dp;->f:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 215991
    :goto_0
    return-void

    .line 215992
    :cond_0
    iget-object v3, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v3

    .line 215993
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 215994
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 215995
    iget-object v5, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v5}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 215996
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->f:LX/03V;

    const-string v5, "pending_story_has_null_id"

    const-string v6, "story ID is null"

    invoke-virtual {v0, v5, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215997
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 215998
    :cond_1
    iget-object v5, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->j:LX/0qn;

    iget-object v6, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v5, v6, v7}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 215999
    iget-object v5, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    iget-object v6, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->postParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/5M1;

    invoke-direct {v7, v0}, LX/5M1;-><init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V

    invoke-virtual {v7}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 216000
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V
    .locals 8

    .prologue
    .line 215963
    const/4 v1, 0x0

    .line 215964
    iget-object v2, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 215965
    if-nez p2, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215966
    const/4 v1, 0x1

    .line 215967
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 215968
    invoke-static {v0}, LX/5M1;->a(Lcom/facebook/composer/publish/common/PendingStory;)LX/5M1;

    move-result-object v3

    new-instance v4, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    if-nez p3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object p3

    :cond_0
    invoke-direct {v4, v5, p1, p3}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 215969
    iput-object v4, v3, LX/5M1;->a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 215970
    move-object v0, v3

    .line 215971
    move v6, v1

    .line 215972
    :goto_0
    invoke-virtual {v0}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v7

    .line 215973
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v7}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 215974
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215975
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 215976
    const-string v0, "pending_story_param_key"

    .line 215977
    iget-object v1, v7, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    move-object v1, v1

    .line 215978
    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 215979
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b:LX/0aG;

    const-string v1, "save_pending_story"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x317130d7

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 215980
    invoke-virtual {v7}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v3

    .line 215981
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    if-eqz v6, :cond_2

    const/16 v1, 0x16

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215982
    new-instance v0, LX/7m5;

    invoke-direct {v0, p0, v3}, LX/7m5;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Ljava/lang/String;)V

    invoke-static {v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 215983
    return-void

    .line 215984
    :cond_1
    :try_start_1
    invoke-static {p2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 215985
    new-instance v0, LX/5M1;

    new-instance v4, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    invoke-direct {v4, v3, p1, p3}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    invoke-direct {v0, v4}, LX/5M1;-><init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V

    move v6, v1

    goto :goto_0

    .line 215986
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 215987
    :cond_2
    const/16 v1, 0x13

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/publish/common/PendingStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215949
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 215950
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215951
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 215952
    monitor-exit v1

    .line 215953
    :goto_0
    return-object v0

    .line 215954
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 215955
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 215956
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 215957
    invoke-static {v0}, LX/5M1;->a(Lcom/facebook/composer/publish/common/PendingStory;)LX/5M1;

    move-result-object v4

    new-instance v5, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-static {v6}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v6

    invoke-virtual {v6}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    new-instance v7, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v0

    invoke-direct {v5, v6, v7, v0}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 215958
    iput-object v5, v4, LX/5M1;->a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 215959
    move-object v0, v4

    .line 215960
    invoke-virtual {v0}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 215961
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 215962
    :cond_2
    :try_start_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/publish/common/PostParamsWrapper;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 215944
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215945
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 215946
    :try_start_0
    new-instance v0, LX/5M1;

    new-instance v2, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, p1, v4}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    invoke-direct {v0, v2}, LX/5M1;-><init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V

    invoke-virtual {v0}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 215947
    invoke-virtual {p1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 215948
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V
    .locals 0

    .prologue
    .line 215942
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 215943
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 215887
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 215888
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 215889
    iget-object v2, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215890
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215891
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 215892
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 215893
    const-string v0, "request_id_param_key"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215894
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b:LX/0aG;

    const-string v1, "delete_pending_story"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x3fac9f6

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 215895
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215896
    new-instance v0, LX/7m6;

    invoke-direct {v0, p0, v6}, LX/7m6;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Ljava/lang/String;)V

    invoke-static {v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 215897
    return-void

    .line 215898
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 215899
    :cond_0
    const/4 v0, 0x0

    move-object v6, v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 215925
    iget-object v2, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 215926
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215927
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215928
    monitor-exit v2

    .line 215929
    :goto_0
    return-void

    .line 215930
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 215931
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-nez v1, :cond_1

    .line 215932
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215933
    monitor-exit v2

    goto :goto_0

    .line 215934
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 215935
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->j:LX/0qn;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v1, v3, v4}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 215936
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gd;

    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, p1, v3}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215937
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215938
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 215939
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->i:LX/0ad;

    invoke-static {v1, v0}, LX/8Nv;->a(LX/0ad;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 215940
    invoke-virtual {p0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 215941
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 215912
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 215913
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215914
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215915
    monitor-exit v1

    .line 215916
    :goto_0
    return-void

    .line 215917
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 215918
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-nez v2, :cond_1

    .line 215919
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215920
    monitor-exit v1

    goto :goto_0

    .line 215921
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 215922
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->j:LX/0qn;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v2, v0, v3}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 215923
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215924
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 215909
    iget-object v1, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 215910
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 215911
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 215900
    iget-object v2, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 215901
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 215902
    if-nez v0, :cond_0

    .line 215903
    monitor-exit v2

    move-object v0, v1

    .line 215904
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/5M1;->a(Lcom/facebook/composer/publish/common/PendingStory;)LX/5M1;

    move-result-object v3

    new-instance v4, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    :cond_1
    new-instance v5, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v0

    invoke-direct {v4, v1, v5, v0}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 215905
    iput-object v4, v3, LX/5M1;->a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 215906
    move-object v0, v3

    .line 215907
    invoke-virtual {v0}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    monitor-exit v2

    goto :goto_0

    .line 215908
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
