.class public Lcom/facebook/composer/publish/common/PublishPostParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/publish/common/PublishPostParamsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/common/PublishPostParamsSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/greetingcards/model/GreetingCard;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final albumId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "album_id"
    .end annotation
.end field

.field public final androidKeyHash:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "android_key_hash"
    .end annotation
.end field

.field public final b:Z

.field public final c:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final canHandleSentryWarning:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "canHandleWarning"
    .end annotation
.end field

.field public final caption:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "caption"
    .end annotation
.end field

.field public final composerSessionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_id"
    .end annotation
.end field

.field public final composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_logging_data"
    .end annotation
.end field

.field public final composerType:LX/2rt;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_type"
    .end annotation
.end field

.field public final connectionClass:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "connection_class"
    .end annotation
.end field

.field public final ctaLink:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cta_link"
    .end annotation
.end field

.field public final ctaType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cta_type"
    .end annotation
.end field

.field public final d:Lcom/facebook/composer/publish/common/PollUploadParams;

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description"
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/publish/common/MediaAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final idempotenceToken:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "idempotence_token"
    .end annotation
.end field

.field public final isBackoutDraft:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_backout_draft"
    .end annotation
.end field

.field public final isCheckin:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_checkin"
    .end annotation
.end field

.field public final isCompostDraftable:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_compost_draftable"
    .end annotation
.end field

.field public final isExplicitLocation:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_explicit_location"
    .end annotation
.end field

.field public final isFeedOnlyPost:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "posting_to_feed_only"
    .end annotation
.end field

.field public final isGroupMemberBioPost:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_member_bio_post"
    .end annotation
.end field

.field public final isMemeShare:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_meme_share"
    .end annotation
.end field

.field public final isPhotoContainer:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_photo_container"
    .end annotation
.end field

.field public final isPlacelistPost:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_placelist_post"
    .end annotation
.end field

.field public final isTagsUserSelected:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_tags_user_selected"
    .end annotation
.end field

.field public final isThrowbackPost:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_throwback_post"
    .end annotation
.end field

.field public final link:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link"
    .end annotation
.end field

.field public final mBudgetData:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "promote_budget"
    .end annotation
.end field

.field public final mMarketplaceId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "marketplace_id"
    .end annotation
.end field

.field public final mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item"
    .end annotation
.end field

.field public final mediaCaptions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_captions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mediaFbIds:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_fbids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_tag"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final nectarModule:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nectarModule"
    .end annotation
.end field

.field public final originalPostTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "original_post_time"
    .end annotation
.end field

.field public final picture:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "picture"
    .end annotation
.end field

.field public final placeAttachmentRemoved:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "place_attachment_removed"
    .end annotation
.end field

.field public final placeTag:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_id"
    .end annotation
.end field

.field public final privacy:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "privacy"
    .end annotation
.end field

.field public final promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_analytics"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final proxiedAppId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "proxied_app_id"
    .end annotation
.end field

.field public final proxiedAppName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "proxied_app_name"
    .end annotation
.end field

.field public final publishMode:LX/5Rn;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_mode"
    .end annotation
.end field

.field public final quote:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "quote"
    .end annotation
.end field

.field public final rawMessage:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "raw_message"
    .end annotation
.end field

.field public final ref:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ref"
    .end annotation
.end field

.field public final reshareOriginalPost:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reshare_original_post"
    .end annotation
.end field

.field public final richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rich_text_style"
    .end annotation
.end field

.field public final schedulePublishTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "schedule_publish_time"
    .end annotation
.end field

.field public final shareable:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shareable"
    .end annotation
.end field

.field public final sourceType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source_type"
    .end annotation
.end field

.field public final souvenir:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "souvenir"
    .end annotation
.end field

.field public final sponsorId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sponsor_id"
    .end annotation
.end field

.field public final stickerId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "referenced_sticker_id"
    .end annotation
.end field

.field public final syncObjectUUIDs:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sync_object_uuid"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final taggedIds:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final targetId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_id"
    .end annotation
.end field

.field public final textOnlyPlace:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_only_place"
    .end annotation
.end field

.field public final throwbackCardJson:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "throwback_card"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field

.field public final tracking:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tracking"
    .end annotation
.end field

.field public final userId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_id"
    .end annotation
.end field

.field public final warnAcknowledged:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "warnAcknowledged"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 112582
    const-class v0, Lcom/facebook/composer/publish/common/PublishPostParamsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 112581
    const-class v0, Lcom/facebook/composer/publish/common/PublishPostParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112580
    new-instance v0, LX/5M8;

    invoke-direct {v0}, LX/5M8;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/common/PublishPostParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 112510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112511
    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    .line 112512
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    .line 112513
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    .line 112514
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 112515
    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    .line 112516
    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->albumId:J

    .line 112517
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    .line 112518
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    .line 112519
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 112520
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    .line 112521
    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->userId:J

    .line 112522
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 112523
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    .line 112524
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    .line 112525
    sget-object v0, LX/2rt;->STATUS:LX/2rt;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    .line 112526
    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    .line 112527
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    .line 112528
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    .line 112529
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    .line 112530
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    .line 112531
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    .line 112532
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    .line 112533
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    .line 112534
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    .line 112535
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    .line 112536
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    .line 112537
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    .line 112538
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    .line 112539
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 112540
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    .line 112541
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    .line 112542
    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    .line 112543
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 112544
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->b:Z

    .line 112545
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 112546
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mBudgetData:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 112547
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 112548
    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    .line 112549
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 112550
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    .line 112551
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    .line 112552
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    .line 112553
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCheckin:Z

    .line 112554
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    .line 112555
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    .line 112556
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    .line 112557
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    .line 112558
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeAttachmentRemoved:Z

    .line 112559
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    .line 112560
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    .line 112561
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    .line 112562
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    .line 112563
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    .line 112564
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaCaptions:LX/0Px;

    .line 112565
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    .line 112566
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    .line 112567
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    .line 112568
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->d:Lcom/facebook/composer/publish/common/PollUploadParams;

    .line 112569
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->e:Ljava/lang/String;

    .line 112570
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 112571
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isFeedOnlyPost:Z

    .line 112572
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    .line 112573
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 112574
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isGroupMemberBioPost:Z

    .line 112575
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->f:LX/0Px;

    .line 112576
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    .line 112577
    iput-object v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    .line 112578
    iput-boolean v2, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    .line 112579
    return-void
.end method

.method public constructor <init>(LX/5M9;)V
    .locals 2

    .prologue
    .line 112291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112292
    iget-wide v0, p1, LX/5M9;->b:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    .line 112293
    iget-object v0, p1, LX/5M9;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    .line 112294
    iget-object v0, p1, LX/5M9;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    .line 112295
    iget-object v0, p1, LX/5M9;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    .line 112296
    iget-wide v0, p1, LX/5M9;->f:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->albumId:J

    .line 112297
    iget-object v0, p1, LX/5M9;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    .line 112298
    iget-object v0, p1, LX/5M9;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    .line 112299
    iget-object v0, p1, LX/5M9;->i:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 112300
    iget-object v0, p1, LX/5M9;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    .line 112301
    iget-wide v0, p1, LX/5M9;->k:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->userId:J

    .line 112302
    iget-object v0, p1, LX/5M9;->l:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 112303
    iget-object v0, p1, LX/5M9;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    .line 112304
    iget-object v0, p1, LX/5M9;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    .line 112305
    iget-object v0, p1, LX/5M9;->q:LX/2rt;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rt;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    .line 112306
    iget-wide v0, p1, LX/5M9;->o:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    .line 112307
    iget-object v0, p1, LX/5M9;->p:LX/5Rn;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    .line 112308
    iget-boolean v0, p1, LX/5M9;->w:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    .line 112309
    iget-object v0, p1, LX/5M9;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    .line 112310
    iget-object v0, p1, LX/5M9;->y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    .line 112311
    iget-object v0, p1, LX/5M9;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    .line 112312
    iget-object v0, p1, LX/5M9;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    .line 112313
    iget-object v0, p1, LX/5M9;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    .line 112314
    iget-object v0, p1, LX/5M9;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    .line 112315
    iget-object v0, p1, LX/5M9;->D:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    .line 112316
    iget-object v0, p1, LX/5M9;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    .line 112317
    iget-object v0, p1, LX/5M9;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    .line 112318
    iget-boolean v0, p1, LX/5M9;->r:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    .line 112319
    iget-object v0, p1, LX/5M9;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 112320
    iget-object v0, p1, LX/5M9;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    .line 112321
    iget-boolean v0, p1, LX/5M9;->t:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    .line 112322
    iget-wide v0, p1, LX/5M9;->u:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    .line 112323
    iget-object v0, p1, LX/5M9;->H:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 112324
    iget-boolean v0, p1, LX/5M9;->K:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->b:Z

    .line 112325
    iget-object v0, p1, LX/5M9;->L:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 112326
    iget-object v0, p1, LX/5M9;->I:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mBudgetData:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 112327
    iget-object v0, p1, LX/5M9;->M:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 112328
    iget-wide v0, p1, LX/5M9;->N:J

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    .line 112329
    iget-object v0, p1, LX/5M9;->O:Lcom/facebook/greetingcards/model/GreetingCard;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 112330
    iget-boolean v0, p1, LX/5M9;->P:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    .line 112331
    iget-boolean v0, p1, LX/5M9;->Q:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    .line 112332
    iget-object v0, p1, LX/5M9;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    .line 112333
    iget-boolean v0, p1, LX/5M9;->S:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCheckin:Z

    .line 112334
    iget-object v0, p1, LX/5M9;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    .line 112335
    iget-object v0, p1, LX/5M9;->U:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    .line 112336
    iget-object v0, p1, LX/5M9;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    .line 112337
    iget-object v0, p1, LX/5M9;->J:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    .line 112338
    iget-boolean v0, p1, LX/5M9;->v:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeAttachmentRemoved:Z

    .line 112339
    iget-boolean v0, p1, LX/5M9;->V:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    .line 112340
    iget-boolean v0, p1, LX/5M9;->W:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    .line 112341
    iget-object v0, p1, LX/5M9;->X:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    .line 112342
    iget-object v0, p1, LX/5M9;->Z:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    .line 112343
    iget-object v0, p1, LX/5M9;->aa:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    .line 112344
    iget-object v0, p1, LX/5M9;->Y:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaCaptions:LX/0Px;

    .line 112345
    iget-object v0, p1, LX/5M9;->ab:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    .line 112346
    iget-boolean v0, p1, LX/5M9;->ac:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    .line 112347
    iget-boolean v0, p1, LX/5M9;->ad:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    .line 112348
    iget-object v0, p1, LX/5M9;->ae:Lcom/facebook/composer/publish/common/PollUploadParams;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->d:Lcom/facebook/composer/publish/common/PollUploadParams;

    .line 112349
    iget-object v0, p1, LX/5M9;->af:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->e:Ljava/lang/String;

    .line 112350
    iget-object v0, p1, LX/5M9;->ag:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 112351
    iget-boolean v0, p1, LX/5M9;->ah:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isFeedOnlyPost:Z

    .line 112352
    iget-boolean v0, p1, LX/5M9;->ai:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    .line 112353
    iget-object v0, p1, LX/5M9;->aj:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 112354
    iget-boolean v0, p1, LX/5M9;->ak:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isGroupMemberBioPost:Z

    .line 112355
    iget-object v0, p1, LX/5M9;->al:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->f:LX/0Px;

    .line 112356
    iget-object v0, p1, LX/5M9;->am:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    .line 112357
    iget-object v0, p1, LX/5M9;->an:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    .line 112358
    iget-boolean v0, p1, LX/5M9;->ao:Z

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    .line 112359
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 112431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112432
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    .line 112433
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    .line 112434
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    .line 112435
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 112436
    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 112437
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    .line 112438
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->albumId:J

    .line 112439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    .line 112440
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    .line 112441
    const-class v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 112442
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    .line 112443
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->userId:J

    .line 112444
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 112445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    .line 112446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    .line 112447
    const-class v0, LX/2rt;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2rt;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    .line 112448
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    .line 112449
    const-class v0, LX/5Rn;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rn;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    .line 112450
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    .line 112451
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    .line 112452
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    .line 112453
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    .line 112454
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    .line 112455
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    .line 112456
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    .line 112457
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    .line 112458
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    .line 112459
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    .line 112460
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    .line 112461
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 112462
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    .line 112463
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    .line 112464
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    .line 112465
    const-class v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 112466
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->b:Z

    .line 112467
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 112468
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mBudgetData:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 112469
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 112470
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    .line 112471
    const-class v0, Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCard;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 112472
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    .line 112473
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    .line 112474
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    .line 112475
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCheckin:Z

    .line 112476
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    .line 112477
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    .line 112478
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    .line 112479
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    .line 112480
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeAttachmentRemoved:Z

    .line 112481
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    .line 112482
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    .line 112483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    .line 112484
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    .line 112485
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v3

    :cond_0
    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    .line 112486
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    .line 112487
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v0, v3

    :cond_1
    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    .line 112488
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    .line 112489
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v0, v3

    :cond_2
    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaCaptions:LX/0Px;

    .line 112490
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    .line 112491
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    .line 112492
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    .line 112493
    const-class v0, Lcom/facebook/composer/publish/common/PollUploadParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PollUploadParams;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->d:Lcom/facebook/composer/publish/common/PollUploadParams;

    .line 112494
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->e:Ljava/lang/String;

    .line 112495
    const-class v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 112496
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isFeedOnlyPost:Z

    .line 112497
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    .line 112498
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 112499
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isGroupMemberBioPost:Z

    .line 112500
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 112501
    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 112502
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_2
    iput-object v3, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->f:LX/0Px;

    .line 112503
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    .line 112504
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    .line 112505
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    .line 112506
    return-void

    :cond_3
    move v0, v2

    .line 112507
    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 112508
    goto/16 :goto_1

    .line 112509
    :cond_5
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    goto :goto_2
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 112430
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112360
    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 112361
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112362
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112363
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 112364
    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->albumId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 112365
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sponsorId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112366
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112367
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionLoggingData:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112368
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->link:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112369
    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->userId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 112370
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 112371
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112372
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->nectarModule:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112373
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 112374
    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->schedulePublishTime:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 112375
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->publishMode:LX/5Rn;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 112376
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 112377
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112378
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112379
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112380
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ref:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112381
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112382
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->caption:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112383
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112384
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->quote:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112385
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->picture:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112386
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112387
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112388
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->idempotenceToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112389
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112390
    iget-wide v4, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 112391
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->minutiaeTag:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112392
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->b:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 112393
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->c:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112394
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mBudgetData:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 112395
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112396
    iget-wide v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mMarketplaceId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 112397
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112398
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isThrowbackPost:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112399
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->reshareOriginalPost:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112400
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->connectionClass:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112401
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCheckin:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112402
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->textOnlyPlace:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112403
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->sourceType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112404
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->throwbackCardJson:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112405
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->stickerId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112406
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->placeAttachmentRemoved:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112407
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112408
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112409
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112410
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaFbIds:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 112411
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->syncObjectUUIDs:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 112412
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->mediaCaptions:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 112413
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->souvenir:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112414
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->warnAcknowledged:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112415
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->canHandleSentryWarning:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112416
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->d:Lcom/facebook/composer/publish/common/PollUploadParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112417
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112418
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112419
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isFeedOnlyPost:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112420
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isMemeShare:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112421
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->richTextStyle:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112422
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isGroupMemberBioPost:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112423
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->f:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 112424
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaLink:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112425
    iget-object v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->ctaType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112426
    iget-boolean v0, p0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPlacelistPost:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 112427
    return-void

    :cond_0
    move v0, v2

    .line 112428
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 112429
    goto/16 :goto_1
.end method
