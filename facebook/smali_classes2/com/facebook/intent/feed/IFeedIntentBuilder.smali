.class public interface abstract Lcom/facebook/intent/feed/IFeedIntentBuilder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# virtual methods
.method public abstract a()Landroid/content/Intent;
.end method

.method public abstract a(JLjava/lang/String;LX/74S;)Landroid/content/Intent;
.end method

.method public abstract a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/8s1;)Landroid/content/Intent;
.end method

.method public abstract a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;
.end method

.method public abstract a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Qt;)Landroid/content/Intent;
.end method

.method public abstract a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)Landroid/content/Intent;
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(LX/21D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
.end method

.method public abstract a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/21D;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
.end method

.method public abstract a(Landroid/content/Context;LX/47I;)Z
.end method

.method public abstract a(Landroid/content/Context;Ljava/lang/String;)Z
.end method

.method public abstract a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract b()Landroid/content/Intent;
.end method

.method public abstract b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract b(Ljava/lang/String;)Landroid/content/Intent;
.end method
