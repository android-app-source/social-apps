.class public Lcom/facebook/gk/sessionless/GkSessionlessModule;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation

.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76448
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 76449
    return-void
.end method

.method public static a()LX/0UW;
    .locals 1
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76450
    new-instance v0, LX/0WY;

    invoke-direct {v0}, LX/0WY;-><init>()V

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/0UW;LX/0UY;)LX/0Uh;
    .locals 1
    .param p1    # LX/0UW;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p2    # LX/0UY;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76451
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 76452
    new-instance p0, LX/0UZ;

    invoke-direct {p0, v0}, LX/0UZ;-><init>(Landroid/content/Context;)V

    .line 76453
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0UZ;->c:Z

    .line 76454
    move-object p0, p0

    .line 76455
    move-object v0, p0

    .line 76456
    iput-object p1, v0, LX/0UZ;->b:LX/0UW;

    .line 76457
    move-object v0, v0

    .line 76458
    iput-object p2, v0, LX/0UZ;->e:LX/0UY;

    .line 76459
    move-object v0, v0

    .line 76460
    invoke-virtual {v0}, LX/0UZ;->b()LX/0Uh;

    move-result-object v0

    return-object v0
.end method

.method public static b()LX/0UY;
    .locals 2
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76461
    new-instance v0, LX/0UY;

    const-string v1, "SessionlessGatekeeperStore"

    invoke-direct {v0, v1}, LX/0UY;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getInstanceForTest_GatekeeperWriter(LX/0QA;)Lcom/facebook/gk/store/GatekeeperWriter;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 76462
    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, Lcom/facebook/gk/store/GatekeeperWriter;

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 76463
    return-void
.end method
