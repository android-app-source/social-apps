.class public Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;
.super Lcom/facebook/widget/popover/PopoverFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0hF;
.implements LX/0hG;


# instance fields
.field private m:LX/8qU;

.field public n:LX/1AM;

.field public o:LX/8qM;

.field public p:LX/8qC;

.field public q:LX/3zd;

.field public r:LX/195;

.field public s:LX/193;

.field public t:LX/8qa;

.field public u:LX/1nJ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115258
    invoke-direct {p0}, Lcom/facebook/widget/popover/PopoverFragment;-><init>()V

    return-void
.end method

.method private a(LX/8qC;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 115259
    iput-object p1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    .line 115260
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 115261
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    invoke-interface {v0}, LX/8qC;->k()V

    .line 115262
    :cond_0
    if-eqz p2, :cond_1

    .line 115263
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->f()Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v2}, LX/8qC;->a(Landroid/view/View;)V

    .line 115264
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 115265
    invoke-direct {p0, v0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->b(Landroid/view/View;)V

    .line 115266
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    if-eqz p2, :cond_3

    const v0, 0x7f040056

    :goto_0
    const v3, 0x7f040092

    const v4, 0x7f040055

    if-eqz p2, :cond_2

    const v1, 0x7f040093

    :cond_2
    invoke-virtual {v2, v0, v3, v4, v1}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0807

    check-cast p1, Landroid/support/v4/app/Fragment;

    const-string v2, "ufi:popover:content:fragment:tag"

    invoke-virtual {v0, v1, p1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 115267
    return-void

    :cond_3
    move v0, v1

    .line 115268
    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 115269
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115270
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 115271
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 115272
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    invoke-interface {v0}, LX/8qC;->hu_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115273
    :goto_0
    return v2

    .line 115274
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 115275
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 115276
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->f()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, LX/8qC;->a(Landroid/view/View;)V

    goto :goto_0

    .line 115277
    :cond_1
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->S_()Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 115278
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 115279
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 115280
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115281
    const-string v0, "story_feedback_flyout"

    return-object v0
.end method

.method public final a(LX/8qC;)V
    .locals 1

    .prologue
    .line 115282
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->a(LX/8qC;Z)V

    .line 115283
    return-void
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 115229
    invoke-static {p1}, LX/8qa;->a(Landroid/app/Dialog;)V

    .line 115230
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115284
    if-eqz p1, :cond_0

    .line 115285
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->a(Landroid/view/View;)V

    .line 115286
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115287
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v1

    invoke-static {v0, v1}, LX/8qa;->a(LX/8qC;LX/8qC;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final eJ_()V
    .locals 1

    .prologue
    .line 115252
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    invoke-interface {v0}, LX/8qC;->l()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->a(Landroid/view/View;)V

    .line 115253
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 2

    .prologue
    .line 115254
    const v0, 0x7f0d1605

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 115255
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 115256
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 115257
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()V
    .locals 2

    .prologue
    .line 115216
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 115217
    invoke-direct {p0, v0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->b(Landroid/view/View;)V

    .line 115218
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->k()V

    .line 115219
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->n:LX/1AM;

    new-instance v1, LX/1ZJ;

    invoke-direct {v1}, LX/1ZJ;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 115220
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->u:LX/1nJ;

    invoke-virtual {v0}, LX/1nJ;->c()V

    .line 115221
    return-void
.end method

.method public final o()LX/8qU;
    .locals 1

    .prologue
    .line 115222
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->m:LX/8qU;

    if-nez v0, :cond_0

    .line 115223
    new-instance v0, LX/8qZ;

    invoke-direct {v0, p0}, LX/8qZ;-><init>(Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;)V

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->m:LX/8qU;

    .line 115224
    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->m:LX/8qU;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3fcad9fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115225
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 115226
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->o:LX/8qM;

    const/4 v2, 0x0

    .line 115227
    iput-boolean v2, v1, LX/8qM;->a:Z

    .line 115228
    const/16 v1, 0x2b

    const v2, 0x352f9439

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x24db0d48

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115231
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 115232
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v5, p0

    check-cast v5, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-static {v1}, LX/3zd;->a(LX/0QB;)LX/3zd;

    move-result-object v6

    check-cast v6, LX/3zd;

    invoke-static {v1}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v7

    check-cast v7, LX/1AM;

    invoke-static {v1}, LX/8qM;->a(LX/0QB;)LX/8qM;

    move-result-object v8

    check-cast v8, LX/8qM;

    const-class v9, LX/193;

    invoke-interface {v1, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/193;

    invoke-static {v1}, LX/8qa;->b(LX/0QB;)LX/8qa;

    move-result-object p1

    check-cast p1, LX/8qa;

    invoke-static {v1}, LX/1nJ;->a(LX/0QB;)LX/1nJ;

    move-result-object v1

    check-cast v1, LX/1nJ;

    iput-object v6, v5, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->q:LX/3zd;

    iput-object v7, v5, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->n:LX/1AM;

    iput-object v8, v5, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->o:LX/8qM;

    iput-object v9, v5, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->s:LX/193;

    iput-object p1, v5, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->t:LX/8qa;

    iput-object v1, v5, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->u:LX/1nJ;

    .line 115233
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    if-eqz v1, :cond_0

    .line 115234
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    invoke-direct {p0, v1, v2}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->a(LX/8qC;Z)V

    .line 115235
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->s:LX/193;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    invoke-interface {v3}, LX/8qC;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->r:LX/195;

    .line 115236
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x310d6bd7

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1d45ac8b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115237
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    .line 115238
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onDestroy()V

    .line 115239
    const/16 v1, 0x2b

    const v2, -0x4848dc8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6fb5df0e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115240
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onPause()V

    .line 115241
    const/16 v1, 0x2b

    const v2, 0x2e02275f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2533a323

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 115242
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onResume()V

    .line 115243
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->o:LX/8qM;

    const/4 v2, 0x1

    .line 115244
    iput-boolean v2, v1, LX/8qM;->a:Z

    .line 115245
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->n:LX/1AM;

    new-instance v2, LX/1ZH;

    invoke-direct {v2}, LX/1ZH;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 115246
    const/16 v1, 0x2b

    const v2, 0x7c45e040

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 115247
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x7db

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 115248
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->t:LX/8qa;

    if-nez v0, :cond_0

    .line 115249
    const/4 v0, 0x1

    .line 115250
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->t:LX/8qa;

    invoke-virtual {v0}, LX/8qa;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final z()LX/8qC;
    .locals 2

    .prologue
    .line 115251
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0807

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/8qC;

    return-object v0
.end method
