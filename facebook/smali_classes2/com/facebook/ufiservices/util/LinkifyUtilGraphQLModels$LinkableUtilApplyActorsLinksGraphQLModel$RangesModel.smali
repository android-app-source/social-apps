.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/1eF;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1101c3b4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 287958
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 287957
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 287955
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 287956
    return-void
.end method

.method private a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287953
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 287954
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 287945
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 287946
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 287947
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 287948
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 287949
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 287950
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 287951
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 287952
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 287937
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 287938
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 287939
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 287940
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 287941
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;

    .line 287942
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 287943
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 287944
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 287933
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 287934
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->f:I

    .line 287935
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->g:I

    .line 287936
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 287929
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 287930
    iget v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 287959
    new-instance v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;

    invoke-direct {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;-><init>()V

    .line 287960
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 287961
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 287931
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 287932
    iget v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->g:I

    return v0
.end method

.method public final synthetic d()LX/1y9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287928
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 287927
    const v0, -0x5838a3c4

    return v0
.end method

.method public final synthetic e()LX/1yC;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287926
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eL_()LX/1yH;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287925
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eM_()LX/1yI;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287924
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 287923
    const v0, -0x3d10ccb9

    return v0
.end method

.method public final synthetic j()LX/1yK;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287922
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel;->a()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method
