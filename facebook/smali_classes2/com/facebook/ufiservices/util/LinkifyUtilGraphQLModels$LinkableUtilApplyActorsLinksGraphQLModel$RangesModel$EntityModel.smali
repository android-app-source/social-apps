.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/1y9;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1db40f0a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 338409
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 338410
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 338411
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 338412
    return-void
.end method

.method private t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338413
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 338414
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    return-object v0
.end method

.method private u()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338415
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 338416
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    return-object v0
.end method

.method private v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338417
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    .line 338418
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    return-object v0
.end method

.method private w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338419
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 338420
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 338421
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 338422
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 338423
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 338424
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 338425
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->u()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 338426
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 338427
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 338428
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->v_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 338429
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 338430
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 338431
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 338432
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->w_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 338433
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 338434
    const/16 v12, 0xf

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 338435
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 338436
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 338437
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 338438
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 338439
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 338440
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 338441
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338442
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338443
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338444
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 338445
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 338446
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 338447
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 338448
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 338449
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 338450
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 338451
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 338452
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 338453
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 338454
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 338455
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 338456
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 338457
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 338458
    :cond_0
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->u()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 338459
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->u()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 338460
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->u()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 338461
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 338462
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 338463
    :cond_1
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 338464
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    .line 338465
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 338466
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 338467
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    .line 338468
    :cond_2
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 338469
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 338470
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 338471
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 338472
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 338473
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 338474
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 338475
    if-eqz v2, :cond_4

    .line 338476
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    .line 338477
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->q:Ljava/util/List;

    move-object v1, v0

    .line 338478
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 338479
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 338480
    new-instance v0, LX/8sM;

    invoke-direct {v0, p1}, LX/8sM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338481
    invoke-virtual {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 338482
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 338483
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->k:Z

    .line 338484
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->l:Z

    .line 338485
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->m:Z

    .line 338486
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 338487
    invoke-virtual {p2}, LX/18L;->a()V

    .line 338488
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 338489
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338490
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 338491
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 338492
    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 338406
    new-instance v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;

    invoke-direct {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;-><init>()V

    .line 338407
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 338408
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 338493
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->f:Ljava/util/List;

    .line 338494
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338381
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 338382
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 338383
    const v0, 0x473699a0    # 46745.625f

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338384
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    .line 338385
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 338386
    const v0, 0x7c02d003

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338387
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    .line 338388
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 338389
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338390
    iget-boolean v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->l:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 338391
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338392
    iget-boolean v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->m:Z

    return v0
.end method

.method public final synthetic m()LX/3Bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338393
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()LX/1yP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338394
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 338395
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->q:Ljava/util/List;

    .line 338396
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic p()LX/6R4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338397
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->w()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338398
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->u()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 338399
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338400
    iget-boolean v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->k:Z

    return v0
.end method

.method public final synthetic s()LX/1yO;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338401
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338402
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    .line 338403
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final w_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338404
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    .line 338405
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    return-object v0
.end method
