.class public Lcom/facebook/runtimepermissions/RequestPermissionsConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/runtimepermissions/RequestPermissionsConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0jt;

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124666
    new-instance v0, LX/2s4;

    invoke-direct {v0}, LX/2s4;-><init>()V

    sput-object v0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/2rN;)V
    .locals 1

    .prologue
    .line 124667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124668
    iget-object v0, p1, LX/2rN;->a:Ljava/lang/String;

    move-object v0, v0

    .line 124669
    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->a:Ljava/lang/String;

    .line 124670
    iget-object v0, p1, LX/2rN;->b:Ljava/lang/String;

    move-object v0, v0

    .line 124671
    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->b:Ljava/lang/String;

    .line 124672
    iget-object v0, p1, LX/2rN;->c:LX/0jt;

    move-object v0, v0

    .line 124673
    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->c:LX/0jt;

    .line 124674
    iget-boolean v0, p1, LX/2rN;->d:Z

    move v0, v0

    .line 124675
    iput-boolean v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->d:Z

    .line 124676
    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->c:LX/0jt;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124677
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 124678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124679
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->a:Ljava/lang/String;

    .line 124680
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->b:Ljava/lang/String;

    .line 124681
    const-class v0, LX/0jt;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0jt;

    iput-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->c:LX/0jt;

    .line 124682
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->d:Z

    .line 124683
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 124684
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 124685
    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124686
    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124687
    iget-object v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->c:LX/0jt;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 124688
    iget-boolean v0, p0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 124689
    return-void
.end method
