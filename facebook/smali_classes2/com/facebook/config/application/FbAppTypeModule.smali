.class public Lcom/facebook/config/application/FbAppTypeModule;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60574
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 60575
    return-void
.end method

.method public static a(LX/00H;)LX/01T;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 60576
    iget-object v0, p0, LX/00H;->j:LX/01T;

    move-object v0, v0

    .line 60577
    return-object v0
.end method

.method public static a()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 60578
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 60579
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/00H;)LX/01U;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 60580
    iget-object v0, p0, LX/00H;->k:LX/01U;

    move-object v0, v0

    .line 60581
    return-object v0
.end method

.method public static b()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 60582
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 60583
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static c()LX/00H;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 60584
    sget-object v0, LX/00H;->a:LX/00H;

    move-object v0, v0

    .line 60585
    if-nez v0, :cond_0

    .line 60586
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Application did not provide its own FbAppType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60587
    :cond_0
    return-object v0
.end method

.method public static c(LX/00H;)LX/01S;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 60588
    iget-object v0, p0, LX/00H;->i:LX/01S;

    move-object v0, v0

    .line 60589
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 2

    .prologue
    .line 60590
    iget-object v0, p0, LX/0Q3;->mBinder:LX/0RH;

    move-object v0, v0

    .line 60591
    const-class v1, LX/00H;

    invoke-virtual {v0, v1}, LX/0RH;->b(Ljava/lang/Class;)LX/0RO;

    move-result-object v1

    new-instance p0, LX/0S0;

    invoke-direct {p0}, LX/0S0;-><init>()V

    invoke-interface {v1, p0}, LX/0RS;->a(LX/0Or;)LX/0RR;

    const-class v1, LX/00I;

    invoke-virtual {v0, v1}, LX/0RH;->a(Ljava/lang/Class;)LX/0RO;

    move-result-object v1

    const-class p0, LX/00H;

    invoke-interface {v1, p0}, LX/0RS;->b(Ljava/lang/Class;)LX/0RR;

    .line 60592
    return-void
.end method
