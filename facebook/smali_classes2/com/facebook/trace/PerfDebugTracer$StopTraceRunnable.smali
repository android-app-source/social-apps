.class public final Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Y9;


# direct methods
.method public constructor <init>(LX/0Y9;)V
    .locals 0

    .prologue
    .line 80688
    iput-object p1, p0, Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;->a:LX/0Y9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 80689
    iget-object v0, p0, Lcom/facebook/trace/PerfDebugTracer$StopTraceRunnable;->a:LX/0Y9;

    const/4 v1, 0x0

    .line 80690
    iget-object v2, v0, LX/0Y9;->b:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v3, LX/0YA;->TRACING:LX/0YA;

    sget-object p0, LX/0YA;->DONE:LX/0YA;

    invoke-virtual {v2, v3, p0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 80691
    :goto_0
    return-void

    :cond_0
    invoke-static {v0, v1}, LX/0Y9;->a(LX/0Y9;Z)Z

    goto :goto_0
.end method
