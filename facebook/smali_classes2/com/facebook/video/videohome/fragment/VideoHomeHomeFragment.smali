.class public Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0o2;
.implements LX/0oB;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->VIDEOHOME_HOME_FRAGMENT:LX/0cQ;
.end annotation

.annotation runtime Lcom/facebook/navigation/annotations/NavigationComponent;
    scrollableContent = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        id = "android.R.id.list"
        type = .enum LX/8Cz;->RECYCLER_VIEW:LX/8Cz;
    .end subannotation
.end annotation


# static fields
.field public static final W:Ljava/lang/String;

.field public static final X:Landroid/os/Handler;

.field private static final Y:Lcom/facebook/search/api/GraphSearchQuery;


# instance fields
.field public A:LX/0xX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/ESm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/ETz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/EVd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/video/player/IsDebugAutoplayLayerEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/EUE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/EUG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/EUY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/13l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/ETB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/ESo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/EUa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ESr;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ETN;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/2zB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/EVb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final Z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final aa:LX/EUW;

.field private final ab:LX/1OD;

.field public ac:LX/0JP;

.field public ad:LX/0J9;

.field public ae:J

.field public af:Landroid/view/View;

.field public ag:Z

.field private ah:Z

.field private ai:Z

.field public aj:LX/1Rq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:Landroid/os/Parcelable;

.field private am:LX/1ly;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:I

.field public ao:Landroid/support/v4/widget/SwipeRefreshLayout;

.field public ap:Landroid/view/View;

.field private aq:LX/195;

.field public ar:LX/1P0;

.field public as:LX/0g6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Landroid/view/ViewGroup;

.field private au:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field public av:Z

.field public aw:LX/EUD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:LX/EUF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:LX/ETy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:LX/0fu;

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2xj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/18q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3AW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0hn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/EVo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1k3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/EUH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0JZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/EU4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1JD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1B1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1vx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/3DC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/D7a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Mp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Or;
    .annotation runtime Lcom/facebook/video/videohome/annotations/IsVideoHomeDebugOverlayEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Jb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/Bwd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 139739
    const-class v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->W:Ljava/lang/String;

    .line 139740
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->X:Landroid/os/Handler;

    .line 139741
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->VIDEO:LX/103;

    const-string v2, ""

    const-string v3, ""

    sget-object v4, LX/7B5;->TAB:LX/7B5;

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 139742
    sput-object v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Y:Lcom/facebook/search/api/GraphSearchQuery;

    sget-object v1, LX/7B4;->SCOPED_TAB:LX/7B4;

    new-instance v2, LX/7BC;

    invoke-direct {v2}, LX/7BC;-><init>()V

    .line 139743
    iput-boolean v5, v2, LX/7BC;->b:Z

    .line 139744
    move-object v2, v2

    .line 139745
    invoke-virtual {v2}, LX/7BC;->a()Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    .line 139746
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 139747
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 139748
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Z:Ljava/util/List;

    .line 139749
    new-instance v0, LX/EUW;

    invoke-direct {v0, p0}, LX/EUW;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aa:LX/EUW;

    .line 139750
    new-instance v0, LX/EUU;

    invoke-direct {v0, p0}, LX/EUU;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ab:LX/1OD;

    .line 139751
    sget-object v0, LX/0JP;->INIT:LX/0JP;

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ac:LX/0JP;

    .line 139752
    sget-object v0, LX/0J9;->NOT_PREFETCHED:LX/0J9;

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    .line 139753
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ae:J

    .line 139754
    iput-boolean v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ai:Z

    .line 139755
    iput-boolean v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->av:Z

    .line 139756
    return-void
.end method

.method public static A(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 2

    .prologue
    .line 139757
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 139758
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 139759
    :cond_0
    return-void
.end method

.method public static B(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 2

    .prologue
    .line 139760
    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-eqz v0, :cond_0

    .line 139761
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    .line 139762
    iget-object v1, v0, LX/2xj;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Qv;

    const-string p0, "SAW_DATA"

    invoke-virtual {v1, p0}, LX/7Qv;->a(Ljava/lang/String;)V

    .line 139763
    :cond_0
    return-void
.end method

.method private L()V
    .locals 4

    .prologue
    .line 139764
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-eqz v0, :cond_1

    .line 139765
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->S(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)LX/1ly;

    move-result-object v0

    .line 139766
    invoke-virtual {v0}, LX/1ly;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 139767
    invoke-virtual {v0}, LX/1ly;->a()V

    .line 139768
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->s:LX/3DC;

    sget-object v1, LX/379;->VIDEO_HOME:LX/379;

    .line 139769
    iget-object v2, v0, LX/3DC;->d:LX/19s;

    invoke-virtual {v1}, LX/379;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139770
    invoke-virtual {v2}, LX/19s;->b()LX/04j;

    move-result-object p0

    .line 139771
    if-eqz p0, :cond_1

    .line 139772
    :try_start_0
    invoke-interface {p0, v3}, LX/04j;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139773
    :cond_1
    :goto_0
    return-void

    .line 139774
    :catch_0
    move-exception p0

    .line 139775
    sget-object v0, LX/19s;->a:Ljava/lang/String;

    const-string v1, "Exception enablePrefetchForOrigin in exo service"

    invoke-static {v0, v1, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private M()V
    .locals 4

    .prologue
    .line 139776
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->S(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)LX/1ly;

    move-result-object v0

    .line 139777
    invoke-virtual {v0}, LX/1ly;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139778
    invoke-virtual {v0}, LX/1ly;->b()V

    .line 139779
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->s:LX/3DC;

    sget-object v1, LX/379;->VIDEO_HOME:LX/379;

    .line 139780
    iget-object v2, v0, LX/3DC;->d:LX/19s;

    invoke-virtual {v1}, LX/379;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139781
    invoke-virtual {v2}, LX/19s;->b()LX/04j;

    move-result-object p0

    .line 139782
    if-eqz p0, :cond_1

    .line 139783
    :try_start_0
    invoke-interface {p0, v3}, LX/04j;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139784
    :cond_1
    :goto_0
    return-void

    .line 139785
    :catch_0
    move-exception p0

    .line 139786
    sget-object v0, LX/19s;->a:Ljava/lang/String;

    const-string v1, "Exception disablePrefetchForOrigin in exo service"

    invoke-static {v0, v1, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static P(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 139787
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v0, :cond_0

    .line 139788
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    .line 139789
    iget-object v2, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getScrollX()I

    move-result v2

    move v2, v2

    .line 139790
    sub-int v2, v1, v2

    .line 139791
    iget-object v3, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getScrollY()I

    move-result v3

    move v3, v3

    .line 139792
    sub-int v3, v1, v3

    .line 139793
    iget-object v4, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, v2, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->scrollBy(II)V

    .line 139794
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-nez v0, :cond_1

    .line 139795
    :goto_0
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->s(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139796
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Z)V

    .line 139797
    return-void

    .line 139798
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->R:LX/2zB;

    invoke-virtual {v0}, LX/2zB;->a()V

    .line 139799
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    .line 139800
    iget-object v1, v0, LX/ETy;->t:LX/ETk;

    move-object v0, v1

    .line 139801
    iget-object v1, v0, LX/ETk;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 139802
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->l()V

    goto :goto_0
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 139803
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->b()V

    .line 139804
    return-void
.end method

.method public static S(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)LX/1ly;
    .locals 3

    .prologue
    .line 139805
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->am:LX/1ly;

    if-nez v0, :cond_0

    .line 139806
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->o:LX/1JD;

    sget-object v1, LX/1Li;->VIDEO_HOME:LX/1Li;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->am:LX/1ly;

    .line 139807
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->am:LX/1ly;

    return-object v0
.end method

.method public static U(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 2

    .prologue
    .line 139808
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ap:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139809
    iget-object p0, v1, LX/ETB;->t:LX/ETQ;

    move-object v1, p0

    .line 139810
    invoke-virtual {v1}, LX/ETQ;->isEmpty()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Landroid/view/View;Z)V

    .line 139811
    return-void
.end method

.method private Y()V
    .locals 12

    .prologue
    .line 139812
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139813
    iget-object v1, v0, LX/ETB;->t:LX/ETQ;

    move-object v0, v1

    .line 139814
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v0, v1}, LX/ETQ;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Ljava/util/List;

    move-result-object v0

    .line 139815
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 139816
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    invoke-virtual {v1}, LX/ETQ;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 139817
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v1

    .line 139818
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v3

    .line 139819
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v3, :cond_0

    .line 139820
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->P:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ESr;

    .line 139821
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 139822
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v4

    invoke-virtual {v4}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 139823
    invoke-static {v4}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 139824
    invoke-static {v4}, LX/ESr;->f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Ljava/lang/String;

    move-result-object v4

    .line 139825
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 139826
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 139827
    iget-object v4, v1, LX/ESr;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    sget-object v4, LX/0zS;->c:LX/0zS;

    .line 139828
    new-instance v8, LX/7R3;

    invoke-direct {v8}, LX/7R3;-><init>()V

    move-object v8, v8

    .line 139829
    const-string v9, "creators_ids"

    invoke-virtual {v8, v9, v5}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 139830
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    invoke-virtual {v8, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    const-wide/16 v10, 0xa

    invoke-virtual {v8, v10, v11}, LX/0zO;->a(J)LX/0zO;

    move-result-object v8

    .line 139831
    move-object v5, v8

    .line 139832
    iget-object v4, v1, LX/ESr;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7R6;

    .line 139833
    new-instance v6, LX/ESq;

    invoke-direct {v6, v1, v0}, LX/ESq;-><init>(LX/ESr;Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    move-object v6, v6

    .line 139834
    iget-object v7, v4, LX/7R6;->a:LX/0tX;

    invoke-virtual {v7, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    .line 139835
    iget-object v8, v4, LX/7R6;->b:Ljava/util/concurrent/Executor;

    invoke-static {v7, v6, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 139836
    :cond_3
    goto/16 :goto_0

    .line 139837
    :cond_4
    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/ETy;LX/0g6;)LX/1Rq;
    .locals 4

    .prologue
    .line 139980
    new-instance v0, LX/EUO;

    invoke-direct {v0, p0}, LX/EUO;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139981
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->E:LX/1DS;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->F:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139982
    iget-object p0, v3, LX/ETB;->t:LX/ETQ;

    move-object v3, p0

    .line 139983
    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 139984
    iput-object p1, v1, LX/1Ql;->f:LX/1PW;

    .line 139985
    move-object v1, v1

    .line 139986
    invoke-virtual {v1, v0}, LX/1Ql;->a(LX/99g;)LX/1Ql;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1Ql;->a(LX/0g8;)LX/1Ql;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ql;->d()LX/1Rq;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    .line 139838
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "VIDEO_HOME"

    invoke-static {v0, v1}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v1

    .line 139839
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 139840
    const v0, 0x7f0315d3

    const/4 v2, 0x0

    invoke-virtual {v9, v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 139841
    const v2, 0x7f0d0595

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 139842
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v3, LX/EUR;

    invoke-direct {v3, p0}, LX/EUR;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 139843
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 139844
    move-object v10, v0

    .line 139845
    const v0, 0x102000a

    invoke-static {v10, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 139846
    new-instance v2, LX/1Oz;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->U:LX/0ad;

    invoke-direct {v2, v0, v3, v4}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/03V;LX/0ad;)V

    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ar:LX/1P0;

    .line 139847
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ar:LX/1P0;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 139848
    new-instance v2, LX/0g6;

    invoke-direct {v2, v0}, LX/0g6;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    .line 139849
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aa:LX/EUW;

    .line 139850
    iget-object v3, v0, LX/ETB;->l:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 139851
    iget-object v3, v0, LX/ETB;->l:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139852
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->C:LX/ETz;

    .line 139853
    sget-object v2, LX/EU0;->a:LX/EU0;

    move-object v2, v2

    .line 139854
    new-instance v3, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment$2;

    invoke-direct {v3, p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment$2;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->au:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 139855
    new-instance v5, LX/EUK;

    invoke-direct {v5, p0}, LX/EUK;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    move-object v5, v5

    .line 139856
    iget-object v6, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->z:LX/Bwd;

    new-instance v7, LX/ETY;

    iget-object v8, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-direct {v7, v8}, LX/ETY;-><init>(LX/ETB;)V

    iget-object v8, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139857
    iget-object p1, v8, LX/ETB;->t:LX/ETQ;

    move-object v8, p1

    .line 139858
    invoke-virtual/range {v0 .. v8}, LX/ETz;->a(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/1PY;LX/Bwd;LX/ETX;LX/ETQ;)LX/ETy;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    .line 139859
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-nez v0, :cond_3

    .line 139860
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/ETy;LX/0g6;)LX/1Rq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    .line 139861
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ab:LX/1OD;

    invoke-interface {v0, v1}, LX/1OQ;->a(LX/1OD;)V

    .line 139862
    const/4 v1, 0x0

    .line 139863
    const v0, 0x7f0315d1

    invoke-virtual {v9, v0, v10, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->at:Landroid/view/ViewGroup;

    .line 139864
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->at:Landroid/view/ViewGroup;

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 139865
    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->av:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139866
    :cond_1
    invoke-static {p0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Z)V

    .line 139867
    :cond_2
    const v0, 0x7f0315c9

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v10, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ap:Landroid/view/View;

    .line 139868
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ap:Landroid/view/View;

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 139869
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ap:Landroid/view/View;

    iget-boolean v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->av:Z

    invoke-static {v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Landroid/view/View;Z)V

    .line 139870
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->p:LX/1B1;

    new-instance v1, LX/EUV;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-direct {v1, p0, v2}, LX/EUV;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/ETB;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 139871
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->p:LX/1B1;

    new-instance v1, LX/EUN;

    invoke-direct {v1, p0}, LX/EUN;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 139872
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    .line 139873
    iget-object v1, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v1

    .line 139874
    new-instance v1, LX/5Mq;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aq:LX/195;

    invoke-direct {v1, v2}, LX/5Mq;-><init>(LX/195;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 139875
    return-object v10
.end method

.method public static a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;I)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 139876
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 139877
    :goto_0
    return-object v0

    .line 139878
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 139879
    instance-of v2, v0, LX/1Rk;

    if-nez v2, :cond_1

    move-object v0, v1

    .line 139880
    goto :goto_0

    .line 139881
    :cond_1
    check-cast v0, LX/1Rk;

    .line 139882
    invoke-virtual {v0}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 139883
    goto :goto_0

    .line 139884
    :cond_2
    invoke-virtual {v0}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 139885
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 139886
    if-eqz p0, :cond_0

    .line 139887
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 139888
    :cond_0
    return-void

    .line 139889
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private static a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2xj;LX/18q;LX/0Xl;LX/3AW;LX/0hn;LX/EVo;LX/0So;LX/1Db;LX/1k3;LX/EUH;LX/0JZ;LX/EU4;LX/1JD;LX/1B1;LX/0bH;LX/1vx;LX/3DC;LX/D7a;LX/193;LX/0Ot;LX/0Or;LX/0Jb;LX/3Fx;LX/Bwd;LX/0xX;LX/ESm;LX/ETz;Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;LX/1DS;LX/0Ot;LX/EVd;Ljava/lang/Boolean;LX/EUE;LX/EUG;LX/EUY;LX/13l;LX/ETB;LX/ESo;LX/EUa;LX/0Ot;LX/0Ot;LX/2zB;LX/EVb;LX/0Or;LX/0ad;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2xj;",
            "LX/18q;",
            "LX/0Xl;",
            "LX/3AW;",
            "LX/0hn;",
            "LX/EVo;",
            "LX/0So;",
            "LX/1Db;",
            "LX/1k3;",
            "LX/EUH;",
            "LX/0JZ;",
            "LX/EU4;",
            "LX/1JD;",
            "LX/1B1;",
            "LX/0bH;",
            "LX/1vx;",
            "LX/3DC;",
            "LX/D7a;",
            "LX/193;",
            "LX/0Ot",
            "<",
            "LX/5Mp;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Jb;",
            "LX/3Fx;",
            "LX/Bwd;",
            "LX/0xX;",
            "LX/ESm;",
            "LX/ETz;",
            "Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/partdefinitions/VideoHomeRootPartDefinition;",
            ">;",
            "LX/EVd;",
            "Ljava/lang/Boolean;",
            "LX/EUE;",
            "LX/EUG;",
            "LX/EUY;",
            "LX/13l;",
            "LX/ETB;",
            "LX/ESo;",
            "LX/EUa;",
            "LX/0Ot",
            "<",
            "LX/ESr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ETN;",
            ">;",
            "LX/2zB;",
            "LX/EVb;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/3AX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139890
    iput-object p1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    iput-object p4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->d:LX/18q;

    iput-object p5, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->e:LX/0Xl;

    iput-object p6, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->f:LX/3AW;

    iput-object p7, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->g:LX/0hn;

    iput-object p8, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    iput-object p9, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->i:LX/0So;

    iput-object p10, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->j:LX/1Db;

    iput-object p11, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    iput-object p12, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l:LX/EUH;

    iput-object p13, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    iput-object p14, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->n:LX/EU4;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->o:LX/1JD;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->p:LX/1B1;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->q:LX/0bH;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->r:LX/1vx;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->s:LX/3DC;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->t:LX/D7a;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->u:LX/193;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->v:LX/0Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->w:LX/0Or;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->x:LX/0Jb;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->y:LX/3Fx;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->z:LX/Bwd;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B:LX/ESm;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->C:LX/ETz;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->D:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->E:LX/1DS;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->F:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->G:LX/EVd;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->H:Ljava/lang/Boolean;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->I:LX/EUE;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->J:LX/EUG;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->K:LX/EUY;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->L:LX/13l;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->O:LX/EUa;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->P:LX/0Ot;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Q:LX/0Ot;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->R:LX/2zB;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->S:LX/EVb;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->T:LX/0Or;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->U:LX/0ad;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->V:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 6

    .prologue
    .line 139891
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139892
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 139893
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 139894
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    .line 139895
    iget-object v2, v1, LX/0xX;->c:LX/0ad;

    sget-short v3, LX/0xY;->e:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v1, v2

    .line 139896
    if-eqz v1, :cond_2

    .line 139897
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->D:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    sget-object v2, LX/379;->VIDEO_HOME:LX/379;

    .line 139898
    iget-object v3, v1, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->h:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController$4;

    invoke-direct {v4, v1, v0, v2}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController$4;-><init>(Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)V

    const v5, 0x774119a4

    invoke-static {v3, v4, v5}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 139899
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 139900
    :cond_1
    return-void

    .line 139901
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->s:LX/3DC;

    sget-object v2, LX/379;->VIDEO_HOME:LX/379;

    invoke-virtual {v1, v0, v2}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 52

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v50

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-static/range {v50 .. v50}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {v50 .. v50}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v50 .. v50}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v5

    check-cast v5, LX/2xj;

    invoke-static/range {v50 .. v50}, LX/18q;->a(LX/0QB;)LX/18q;

    move-result-object v6

    check-cast v6, LX/18q;

    invoke-static/range {v50 .. v50}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static/range {v50 .. v50}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v8

    check-cast v8, LX/3AW;

    invoke-static/range {v50 .. v50}, LX/0hn;->a(LX/0QB;)LX/0hn;

    move-result-object v9

    check-cast v9, LX/0hn;

    invoke-static/range {v50 .. v50}, LX/EVo;->a(LX/0QB;)LX/EVo;

    move-result-object v10

    check-cast v10, LX/EVo;

    invoke-static/range {v50 .. v50}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static/range {v50 .. v50}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v12

    check-cast v12, LX/1Db;

    invoke-static/range {v50 .. v50}, LX/1k3;->a(LX/0QB;)LX/1k3;

    move-result-object v13

    check-cast v13, LX/1k3;

    invoke-static/range {v50 .. v50}, LX/EUH;->b(LX/0QB;)LX/EUH;

    move-result-object v14

    check-cast v14, LX/EUH;

    invoke-static/range {v50 .. v50}, LX/0JZ;->a(LX/0QB;)LX/0JZ;

    move-result-object v15

    check-cast v15, LX/0JZ;

    invoke-static/range {v50 .. v50}, LX/EU4;->a(LX/0QB;)LX/EU4;

    move-result-object v16

    check-cast v16, LX/EU4;

    const-class v17, LX/1JD;

    move-object/from16 v0, v50

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/1JD;

    invoke-static/range {v50 .. v50}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v18

    check-cast v18, LX/1B1;

    invoke-static/range {v50 .. v50}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v19

    check-cast v19, LX/0bH;

    invoke-static/range {v50 .. v50}, LX/1vx;->a(LX/0QB;)LX/1vx;

    move-result-object v20

    check-cast v20, LX/1vx;

    invoke-static/range {v50 .. v50}, LX/3DC;->a(LX/0QB;)LX/3DC;

    move-result-object v21

    check-cast v21, LX/3DC;

    invoke-static/range {v50 .. v50}, LX/D7a;->a(LX/0QB;)LX/D7a;

    move-result-object v22

    check-cast v22, LX/D7a;

    const-class v23, LX/193;

    move-object/from16 v0, v50

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/193;

    const/16 v24, 0x1a76

    move-object/from16 v0, v50

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x1598

    move-object/from16 v0, v50

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    invoke-static/range {v50 .. v50}, LX/0Jb;->a(LX/0QB;)LX/0Jb;

    move-result-object v26

    check-cast v26, LX/0Jb;

    invoke-static/range {v50 .. v50}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v27

    check-cast v27, LX/3Fx;

    invoke-static/range {v50 .. v50}, LX/Bwd;->a(LX/0QB;)LX/Bwd;

    move-result-object v28

    check-cast v28, LX/Bwd;

    invoke-static/range {v50 .. v50}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v29

    check-cast v29, LX/0xX;

    invoke-static/range {v50 .. v50}, LX/ESm;->a(LX/0QB;)LX/ESm;

    move-result-object v30

    check-cast v30, LX/ESm;

    const-class v31, LX/ETz;

    move-object/from16 v0, v50

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/ETz;

    invoke-static/range {v50 .. v50}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->a(LX/0QB;)Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    move-result-object v32

    check-cast v32, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    invoke-static/range {v50 .. v50}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v33

    check-cast v33, LX/1DS;

    const/16 v34, 0x382f

    move-object/from16 v0, v50

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v34

    invoke-static/range {v50 .. v50}, LX/EVd;->a(LX/0QB;)LX/EVd;

    move-result-object v35

    check-cast v35, LX/EVd;

    invoke-static/range {v50 .. v50}, LX/ESj;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v36

    check-cast v36, Ljava/lang/Boolean;

    const-class v37, LX/EUE;

    move-object/from16 v0, v50

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v37

    check-cast v37, LX/EUE;

    const-class v38, LX/EUG;

    move-object/from16 v0, v50

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/EUG;

    invoke-static/range {v50 .. v50}, LX/EUY;->a(LX/0QB;)LX/EUY;

    move-result-object v39

    check-cast v39, LX/EUY;

    invoke-static/range {v50 .. v50}, LX/13l;->a(LX/0QB;)LX/13l;

    move-result-object v40

    check-cast v40, LX/13l;

    invoke-static/range {v50 .. v50}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v41

    check-cast v41, LX/ETB;

    invoke-static/range {v50 .. v50}, LX/ESo;->a(LX/0QB;)LX/ESo;

    move-result-object v42

    check-cast v42, LX/ESo;

    invoke-static/range {v50 .. v50}, LX/EUa;->a(LX/0QB;)LX/EUa;

    move-result-object v43

    check-cast v43, LX/EUa;

    const/16 v44, 0x3806

    move-object/from16 v0, v50

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v44

    const/16 v45, 0x380d

    move-object/from16 v0, v50

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v45

    invoke-static/range {v50 .. v50}, LX/2zB;->a(LX/0QB;)LX/2zB;

    move-result-object v46

    check-cast v46, LX/2zB;

    invoke-static/range {v50 .. v50}, LX/EVb;->a(LX/0QB;)LX/EVb;

    move-result-object v47

    check-cast v47, LX/EVb;

    const/16 v48, 0x122d

    move-object/from16 v0, v50

    move/from16 v1, v48

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v48

    invoke-static/range {v50 .. v50}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v49

    check-cast v49, LX/0ad;

    const/16 v51, 0x1381

    invoke-static/range {v50 .. v51}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v50

    invoke-static/range {v2 .. v50}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2xj;LX/18q;LX/0Xl;LX/3AW;LX/0hn;LX/EVo;LX/0So;LX/1Db;LX/1k3;LX/EUH;LX/0JZ;LX/EU4;LX/1JD;LX/1B1;LX/0bH;LX/1vx;LX/3DC;LX/D7a;LX/193;LX/0Ot;LX/0Or;LX/0Jb;LX/3Fx;LX/Bwd;LX/0xX;LX/ESm;LX/ETz;Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;LX/1DS;LX/0Ot;LX/EVd;Ljava/lang/Boolean;LX/EUE;LX/EUG;LX/EUY;LX/13l;LX/ETB;LX/ESo;LX/EUa;LX/0Ot;LX/0Ot;LX/2zB;LX/EVb;LX/0Or;LX/0ad;LX/0Ot;)V

    return-void
.end method

.method private a(ZZ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 139902
    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139903
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->c()Z

    move-result v0

    .line 139904
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v2}, LX/ETB;->d()Z

    move-result v2

    .line 139905
    iget-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v3}, LX/ETB;->e()Z

    move-result v3

    .line 139906
    if-eqz v0, :cond_4

    .line 139907
    sget-object v0, LX/0J9;->PREFETCH_ONGOING:LX/0J9;

    .line 139908
    :goto_0
    move-object v0, v0

    .line 139909
    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    .line 139910
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 139911
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "tabbar_target_intent"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 139912
    if-eqz v0, :cond_3

    .line 139913
    const-string v2, "is_from_push_notification"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 139914
    :goto_1
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l()I

    move-result v3

    .line 139915
    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    sget-object p2, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    invoke-virtual {v4, p2}, LX/0xX;->a(LX/1vy;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->V:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3AX;

    invoke-virtual {v4}, LX/3AX;->d()Z

    move-result v4

    .line 139916
    :goto_2
    if-nez v4, :cond_a

    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 139917
    invoke-virtual {v1, v2, v0, v3, v4}, LX/2xj;->a(LX/0J9;ZIZ)V

    .line 139918
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    sget-object v1, LX/0J9;->PREFETCHED_NOT_LOADED:LX/0J9;

    if-ne v0, v1, :cond_0

    .line 139919
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139920
    invoke-virtual {v0}, LX/ETB;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/ETB;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2xj;

    invoke-virtual {v1}, LX/2xj;->j()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 139921
    :cond_0
    :goto_4
    if-eqz p1, :cond_2

    sget-object v0, LX/0JP;->RESTORE:LX/0JP;

    .line 139922
    :goto_5
    invoke-static {p0, v0, p1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a$redex0(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/0JP;Z)V

    .line 139923
    :cond_1
    return-void

    .line 139924
    :cond_2
    sget-object v0, LX/0JP;->INIT:LX/0JP;

    goto :goto_5

    :cond_3
    move v0, v1

    goto :goto_1

    .line 139925
    :cond_4
    if-eqz v2, :cond_6

    if-nez v3, :cond_6

    .line 139926
    if-eqz p2, :cond_5

    sget-object v0, LX/0J9;->PREFETCHED_NOT_LOADED:LX/0J9;

    goto/16 :goto_0

    :cond_5
    sget-object v0, LX/0J9;->PREFETCHED_LOADED:LX/0J9;

    goto/16 :goto_0

    .line 139927
    :cond_6
    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    .line 139928
    sget-object v0, LX/0J9;->STALED:LX/0J9;

    goto/16 :goto_0

    .line 139929
    :cond_7
    if-nez p2, :cond_8

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->q()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->r()Z

    move-result v0

    if-nez v0, :cond_8

    .line 139930
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a:LX/03V;

    sget-object v2, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->W:Ljava/lang/String;

    const-string v3, "NOT_PREFETCHED cache status on non-fragment-creation case"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139931
    :cond_8
    sget-object v0, LX/0J9;->NOT_PREFETCHED:LX/0J9;

    goto/16 :goto_0

    .line 139932
    :cond_9
    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->g:LX/0hn;

    .line 139933
    iget-object p2, v4, LX/0hn;->x:LX/EUc;

    invoke-virtual {v4, p2}, LX/0hn;->b(LX/EUc;)Z

    move-result p2

    move v4, p2

    .line 139934
    goto :goto_2

    .line 139935
    :cond_a
    const/4 v4, 0x0

    goto :goto_3

    .line 139936
    :cond_b
    iget-object v1, v0, LX/ETB;->t:LX/ETQ;

    invoke-virtual {v1}, LX/ETQ;->a()LX/0Px;

    move-result-object v2

    .line 139937
    iget-object v1, v0, LX/ETB;->r:LX/19j;

    iget v3, v1, LX/19j;->aS:I

    .line 139938
    sget-object v4, LX/379;->VIDEO_HOME_OCCLUSION:LX/379;

    .line 139939
    iget-object v1, v0, LX/ETB;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    .line 139940
    iget-object v5, v1, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->h:Landroid/os/Handler;

    new-instance p2, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController$3;

    invoke-direct {p2, v1, v2, v3, v4}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController$3;-><init>(Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;LX/0Px;ILX/379;)V

    const v0, 0x721096fe

    invoke-static {v5, p2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 139941
    goto :goto_4
.end method

.method public static a$redex0(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/0JP;Z)V
    .locals 4

    .prologue
    .line 139942
    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-wide v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ae:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 139943
    iput-object p1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ac:LX/0JP;

    .line 139944
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->i:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ae:J

    .line 139945
    new-instance v0, LX/EUM;

    invoke-direct {v0, p0}, LX/EUM;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->az:LX/0fu;

    .line 139946
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ac:LX/0JP;

    const p2, 0x1d0009

    .line 139947
    iget-object v3, v0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 139948
    iget-object v3, v0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "is_cached_"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, p2, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 139949
    iget-object v3, v0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "loading_type_"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, p2, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 139950
    iget-object v3, v0, LX/0JZ;->b:LX/0Jc;

    .line 139951
    iput-object v1, v3, LX/0Jc;->c:LX/0J9;

    .line 139952
    iput-object v2, v3, LX/0Jc;->d:LX/0JP;

    .line 139953
    sget-object p0, LX/0JP;->INIT:LX/0JP;

    if-ne v2, p0, :cond_1

    .line 139954
    iget p0, v3, LX/0Jc;->b:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v3, LX/0Jc;->b:I

    .line 139955
    sget-object p0, LX/0J9;->NOT_PREFETCHED:LX/0J9;

    if-eq v1, p0, :cond_1

    sget-object p0, LX/0J9;->PREFETCH_ONGOING:LX/0J9;

    if-eq v1, p0, :cond_1

    .line 139956
    iget p0, v3, LX/0Jc;->a:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v3, LX/0Jc;->a:I

    .line 139957
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/2rJ;)V
    .locals 1

    .prologue
    .line 139958
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    invoke-virtual {v0}, LX/0JZ;->c()V

    .line 139959
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-eqz v0, :cond_0

    .line 139960
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    invoke-interface {v0}, LX/ETh;->b()V

    .line 139961
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->j()V

    .line 139962
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0, p1}, LX/ETB;->a(LX/2rJ;)Z

    .line 139963
    return-void
.end method

.method private b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 139694
    const-string v0, "VideoHomeHomeFragment.initViewFromBaseReactionFragment"

    const v1, 0x72ba67f6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 139695
    :try_start_0
    invoke-static {p0, p1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139696
    const v1, -0x4b19fcc2

    invoke-static {v1}, LX/02m;->a(I)V

    .line 139697
    const-string v1, "VideoHomeHomeFragment.setupViews"

    const v2, -0xf065f

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 139698
    :try_start_1
    const v1, 0x7f0d3120

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 139699
    if-nez v1, :cond_4

    .line 139700
    :goto_0
    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139701
    const v4, 0x7f0d0d53

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->af:Landroid/view/View;

    .line 139702
    const v4, 0x7f0d1133

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    .line 139703
    const v5, 0x7f08224b

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 139704
    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    iget-object v5, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->af:Landroid/view/View;

    .line 139705
    iput-object v5, v4, LX/EVo;->e:Landroid/view/View;

    .line 139706
    new-instance v6, LX/EVm;

    invoke-direct {v6, v4}, LX/EVm;-><init>(LX/EVo;)V

    iput-object v6, v4, LX/EVo;->g:LX/EVm;

    .line 139707
    iget-object v6, v4, LX/EVo;->b:LX/0wW;

    invoke-virtual {v6}, LX/0wW;->a()LX/0wd;

    move-result-object v6

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v8, v9}, LX/0wd;->a(D)LX/0wd;

    move-result-object v6

    iget-object v7, v4, LX/EVo;->g:LX/EVm;

    invoke-virtual {v6, v7}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v6

    sget-object v7, LX/EVo;->a:LX/0wT;

    invoke-virtual {v6, v7}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v6

    iput-object v6, v4, LX/EVo;->f:LX/0wd;

    .line 139708
    iget-object v6, v4, LX/EVo;->e:Landroid/view/View;

    new-instance v7, LX/EVl;

    invoke-direct {v7, v4}, LX/EVl;-><init>(LX/EVo;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 139709
    sget-object v6, LX/EVn;->HIDDEN:LX/EVn;

    iput-object v6, v4, LX/EVo;->h:LX/EVn;

    .line 139710
    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->af:Landroid/view/View;

    new-instance v5, LX/EUQ;

    invoke-direct {v5, p0}, LX/EUQ;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139711
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 139712
    iput-object v2, v1, LX/ESo;->j:Landroid/view/View;

    .line 139713
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->H:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139714
    invoke-static {v0}, Lcom/facebook/video/videohome/views/VideoHomeAutoplayDebugOverlayView;->a(Landroid/view/ViewGroup;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 139715
    :cond_0
    const v1, 0x88197b4

    invoke-static {v1}, LX/02m;->a(I)V

    .line 139716
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0g6;

    .line 139717
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v2}, LX/ETB;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139718
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->az:LX/0fu;

    if-eqz v2, :cond_1

    .line 139719
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->az:LX/0fu;

    invoke-virtual {v1, v2}, LX/0g7;->b(LX/0fu;)V

    .line 139720
    :cond_1
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->t:LX/D7a;

    .line 139721
    iput-object v0, v2, LX/D7a;->a:Landroid/view/ViewGroup;

    .line 139722
    iget-object v3, v2, LX/D7a;->b:LX/1PJ;

    iget-object v4, v2, LX/D7a;->a:Landroid/view/ViewGroup;

    new-instance v5, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v6, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v5, v6}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v3, v4, v5}, LX/1PJ;->a(Landroid/view/ViewGroup;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 139723
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->j:LX/1Db;

    invoke-virtual {v2}, LX/1Db;->a()LX/1St;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0g7;->a(LX/1St;)V

    .line 139724
    new-instance v2, LX/EUX;

    invoke-direct {v2, p0}, LX/EUX;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    invoke-virtual {v1, v2}, LX/0g7;->a(LX/0fx;)V

    .line 139725
    const-string v2, "VideoHomeHomeFragment.setRecyclerViewAdapter"

    const v3, -0x34105d30    # -3.1409568E7f

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 139726
    :try_start_2
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-virtual {v1, v2}, LX/0g7;->a(LX/1OO;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 139727
    const v2, 0x51d5bf9c

    invoke-static {v2}, LX/02m;->a(I)V

    .line 139728
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->O:LX/EUa;

    .line 139729
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 139730
    iget-object v3, v2, LX/EUa;->c:LX/0fx;

    if-nez v3, :cond_2

    .line 139731
    new-instance v3, LX/EUZ;

    invoke-direct {v3, v2}, LX/EUZ;-><init>(LX/EUa;)V

    iput-object v3, v2, LX/EUa;->c:LX/0fx;

    .line 139732
    :cond_2
    iget-object v3, v2, LX/EUa;->c:LX/0fx;

    move-object v3, v3

    .line 139733
    invoke-interface {v1, v3}, LX/0g8;->b(LX/0fx;)V

    .line 139734
    :cond_3
    return-object v0

    .line 139735
    :catchall_0
    move-exception v0

    const v1, 0x501a694f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 139736
    :catchall_1
    move-exception v0

    const v1, 0x5a084c49

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 139737
    :catchall_2
    move-exception v0

    const v1, -0x4b7212e1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 139738
    :cond_4
    new-instance v2, LX/EUP;

    invoke-direct {v2, p0}, LX/EUP;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public static c(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Z)V
    .locals 1

    .prologue
    .line 139364
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->at:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Landroid/view/View;Z)V

    .line 139365
    return-void
.end method

.method private l()I
    .locals 8

    .prologue
    .line 139964
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    const/4 v2, 0x0

    .line 139965
    iget-object v1, v0, LX/ETB;->t:LX/ETQ;

    move-object v1, v1

    .line 139966
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v1, v3}, LX/ETQ;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Ljava/util/List;

    move-result-object v1

    .line 139967
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 139968
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 139969
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 139970
    invoke-interface {v1}, LX/9uc;->ac()I

    move-result v1

    .line 139971
    :goto_0
    move v0, v1

    .line 139972
    return v0

    .line 139973
    :cond_0
    iget-object v1, v0, LX/ETB;->J:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 139974
    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 139975
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 139976
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v7

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v7, p0, :cond_2

    .line 139977
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->ac()I

    move-result v1

    goto :goto_0

    .line 139978
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_3
    move v1, v2

    .line 139979
    goto :goto_0
.end method

.method public static p$redex0(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 14

    .prologue
    .line 139332
    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ae:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 139333
    iget-object v10, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->f:LX/3AW;

    new-instance v4, LX/7Qn;

    sget-object v5, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v6, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ac:LX/0JP;

    iget-object v7, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    iget-object v8, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->i:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    iget-wide v12, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ae:J

    sub-long/2addr v8, v12

    invoke-direct/range {v4 .. v9}, LX/7Qn;-><init>(LX/04D;LX/0JP;LX/0J9;J)V

    invoke-virtual {v10, v4}, LX/3AW;->a(LX/7Qf;)V

    .line 139334
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ae:J

    .line 139335
    iget-object v4, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    const v7, 0x1d0009

    .line 139336
    iget-object v5, v4, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v5, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 139337
    iget-object v5, v4, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v6, 0x2

    invoke-interface {v5, v7, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 139338
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139339
    iget-object v1, v0, LX/ETB;->t:LX/ETQ;

    move-object v1, v1

    .line 139340
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v1, v2}, LX/ETQ;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Ljava/util/List;

    move-result-object v1

    .line 139341
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 139342
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 139343
    :goto_0
    move-object v1, v1

    .line 139344
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :goto_1
    iget-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    .line 139345
    invoke-virtual {v2}, LX/2xj;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 139346
    iget-object v4, v2, LX/2xj;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1CC;

    .line 139347
    iget v5, v4, LX/1CC;->h:I

    move v5, v5

    .line 139348
    if-lez v5, :cond_1

    if-nez v0, :cond_1

    .line 139349
    iget-object v4, v2, LX/2xj;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    sget-object v6, LX/2xj;->b:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Inconsistency between session badgecount = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " and notification section = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " sessionId = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, v2, LX/2xj;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1CC;

    .line 139350
    iget-object v8, v5, LX/1CC;->d:Ljava/lang/String;

    move-object v5, v8

    .line 139351
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v5

    iget-object v6, v3, LX/0J9;->value:Ljava/lang/String;

    .line 139352
    iput-object v6, v5, LX/0VK;->a:Ljava/lang/String;

    .line 139353
    move-object v5, v5

    .line 139354
    invoke-virtual {v5}, LX/0VK;->g()LX/0VG;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/03V;->a(LX/0VG;)V

    .line 139355
    :cond_1
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-eqz v0, :cond_2

    .line 139356
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    invoke-interface {v0, v1}, LX/ETh;->c(Ljava/lang/Object;)Z

    .line 139357
    :cond_2
    return-void

    .line 139358
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 139359
    :cond_4
    iget-object v1, v0, LX/ETB;->t:LX/ETQ;

    move-object v1, v1

    .line 139360
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v1, v2}, LX/ETQ;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Ljava/util/List;

    move-result-object v1

    .line 139361
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 139362
    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v3

    if-eqz v3, :cond_5

    goto/16 :goto_0

    .line 139363
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static s(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 5

    .prologue
    .line 139366
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    if-eqz v0, :cond_0

    .line 139367
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    .line 139368
    sget-object v1, LX/EVn;->HIDDEN:LX/EVn;

    iget-object v2, v0, LX/EVo;->h:LX/EVn;

    invoke-virtual {v1, v2}, LX/EVn;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 139369
    if-nez v1, :cond_0

    invoke-static {v0}, LX/EVo;->k(LX/EVo;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139370
    :cond_0
    :goto_0
    return-void

    .line 139371
    :cond_1
    sget-object v1, LX/EVn;->HIDING:LX/EVn;

    iput-object v1, v0, LX/EVo;->h:LX/EVn;

    .line 139372
    iget-object v1, v0, LX/EVo;->f:LX/0wd;

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v3, v4}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public static w(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 2

    .prologue
    .line 139373
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v0, :cond_0

    .line 139374
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->d(I)V

    .line 139375
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139376
    const-string v0, "video_home"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 139377
    const-string v0, "VideoHomeHomeFragment.injectMe"

    const v3, 0x3485163f

    invoke-static {v0, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 139378
    :try_start_0
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139379
    const v0, -0x413a38e3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 139380
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v0, p0}, LX/2xj;->a(LX/0oB;)V

    .line 139381
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    sget-object v3, LX/1vy;->DOWNLOAD_SECTION:LX/1vy;

    invoke-virtual {v0, v3}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139382
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    iget-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B:LX/ESm;

    .line 139383
    iget-object v4, v3, LX/ESm;->b:LX/1Aa;

    move-object v3, v4

    .line 139384
    iput-object v3, v0, LX/ETN;->h:LX/1Aa;

    .line 139385
    iget-object v4, v0, LX/ETN;->h:LX/1Aa;

    invoke-virtual {v4, v0}, LX/1Aa;->a(LX/7zg;)V

    .line 139386
    :cond_0
    if-eqz p1, :cond_3

    move v0, v1

    .line 139387
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(ZZ)V

    .line 139388
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 139389
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->j()V

    .line 139390
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139391
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    invoke-virtual {v0}, LX/0JZ;->c()V

    .line 139392
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    sget-object v1, LX/1vy;->CACHED_SECTIONS:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 139393
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    .line 139394
    iget-object v1, v0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1d0009

    const/16 v4, 0x22

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 139395
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    sget-object v1, LX/2rJ;->CACHED_SECTION:LX/2rJ;

    invoke-virtual {v0, v1}, LX/ETB;->a(LX/2rJ;)Z

    .line 139396
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->u:LX/193;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_reaction_fragment_scroll_perf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aq:LX/195;

    .line 139397
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->w:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139398
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Mp;

    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->x:LX/0Jb;

    .line 139399
    iput-object v1, v0, LX/5Mp;->e:LX/0Jb;

    .line 139400
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.apptab.ui.TAB_BAR_ITEM_TAP"

    new-instance v2, LX/EUT;

    invoke-direct {v2, p0}, LX/EUT;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/EUS;

    invoke-direct {v2, p0}, LX/EUS;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ak:LX/0Yb;

    .line 139401
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ak:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 139402
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->g:LX/0hn;

    invoke-virtual {v0, p0}, LX/0hn;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139403
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->S:LX/EVb;

    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->R:LX/2zB;

    .line 139404
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/EVb;->b:Ljava/lang/ref/WeakReference;

    .line 139405
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    .line 139406
    iget-object v1, v0, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x1d0009

    const/16 v3, 0x4e

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 139407
    return-void

    .line 139408
    :catchall_0
    move-exception v0

    const v1, -0x3c8a7964

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_3
    move v0, v2

    .line 139409
    goto/16 :goto_0

    .line 139410
    :cond_4
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    sget-object v1, LX/2rJ;->NORMAL:LX/2rJ;

    invoke-virtual {v0, v1}, LX/ETB;->a(LX/2rJ;)Z

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 139411
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v0, :cond_0

    .line 139412
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 139413
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    invoke-virtual {v0}, LX/ESo;->b()V

    .line 139414
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 139415
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v0, :cond_0

    .line 139416
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 139417
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    invoke-virtual {v0}, LX/ESo;->a()V

    .line 139418
    return-void
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 139419
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v0, :cond_0

    .line 139420
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 139421
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->L:LX/13l;

    invoke-virtual {v0}, LX/13l;->d()V

    .line 139422
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    invoke-virtual {v0}, LX/ESo;->b()V

    .line 139423
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    .line 139424
    iget-object v1, v0, LX/2xj;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1CC;

    move-object v0, v1

    .line 139425
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 139426
    if-eqz v3, :cond_1

    invoke-virtual {v0}, LX/1CC;->g()J

    move-result-wide v3

    const-wide/16 v5, 0x1f4

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 139427
    iget-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->T:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gt;

    const-string v4, "996145670432082"

    .line 139428
    iput-object v4, v3, LX/0gt;->a:Ljava/lang/String;

    .line 139429
    move-object v3, v3

    .line 139430
    const-string v4, "session_id"

    .line 139431
    iget-object v5, v0, LX/1CC;->d:Ljava/lang/String;

    move-object v5, v5

    .line 139432
    invoke-virtual {v3, v4, v5}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0gt;->a(Landroid/content/Context;)V

    .line 139433
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 139434
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESr;

    invoke-virtual {v0}, LX/ESr;->b()V

    .line 139435
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 139436
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Y()V

    .line 139437
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 1

    .prologue
    .line 139438
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->r:LX/1vx;

    invoke-virtual {v0}, LX/1vx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Y:Lcom/facebook/search/api/GraphSearchQuery;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    goto :goto_0
.end method

.method public final k()V
    .locals 4

    .prologue
    .line 139439
    const/4 v0, 0x0

    .line 139440
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-nez v1, :cond_2

    .line 139441
    :cond_0
    :goto_0
    move v0, v0

    .line 139442
    if-eqz v0, :cond_1

    .line 139443
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->a()V

    .line 139444
    :cond_1
    return-void

    .line 139445
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-interface {v2}, LX/1OP;->ij_()I

    move-result v2

    invoke-interface {v1, v2}, LX/1Qr;->h_(I)I

    move-result v1

    .line 139446
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    iget-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ar:LX/1P0;

    invoke-virtual {v3}, LX/1P1;->n()I

    move-result v3

    invoke-interface {v2, v3}, LX/1Qr;->h_(I)I

    move-result v2

    .line 139447
    sub-int/2addr v1, v2

    .line 139448
    const/16 v2, 0xa

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final kQ_()V
    .locals 3

    .prologue
    .line 139449
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v0, :cond_0

    .line 139450
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 139451
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    invoke-virtual {v0}, LX/ESo;->a()V

    .line 139452
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Y()V

    .line 139453
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5d285508

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139454
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 139455
    const/4 v2, 0x0

    .line 139456
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class p1, LX/0f0;

    invoke-static {v1, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0f0;

    .line 139457
    if-eqz v1, :cond_2

    .line 139458
    const-class p1, LX/0f3;

    invoke-interface {v1, p1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0f3;

    .line 139459
    if-eqz v1, :cond_2

    .line 139460
    invoke-interface {v1}, LX/0f3;->n()Lcom/facebook/apptab/state/TabTag;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-interface {v1}, LX/0f3;->n()Lcom/facebook/apptab/state/TabTag;

    move-result-object v1

    sget-object p1, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    if-eq v1, p1, :cond_1

    const/4 v1, 0x1

    .line 139461
    :goto_0
    move v1, v1

    .line 139462
    if-eqz v1, :cond_0

    .line 139463
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->G:LX/EVd;

    .line 139464
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/EVd;->a(LX/EVd;Z)V

    .line 139465
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x2e3b4902

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move v1, v2

    .line 139466
    goto :goto_0

    :cond_2
    move v1, v2

    .line 139467
    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 139468
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 139469
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-eqz v0, :cond_0

    .line 139470
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 139471
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x173f06c5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139472
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 139473
    invoke-direct {p0, p2}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->b(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 139474
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    .line 139475
    iget-object v3, v2, LX/0JZ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p0, 0x1d0009

    const/16 p1, 0x65

    invoke-interface {v3, p0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 139476
    const/16 v2, 0x2b

    const v3, 0x22fe4379

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x106df04f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139477
    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->R:LX/2zB;

    .line 139478
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->z:LX/Bwd;

    invoke-virtual {v1}, LX/Bwd;->a()V

    .line 139479
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 139480
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ak:LX/0Yb;

    if-eqz v1, :cond_0

    .line 139481
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ak:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 139482
    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ak:LX/0Yb;

    .line 139483
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    if-eqz v1, :cond_2

    .line 139484
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    .line 139485
    iget-object v4, v1, LX/EVo;->f:LX/0wd;

    if-eqz v4, :cond_1

    .line 139486
    iget-object v4, v1, LX/EVo;->f:LX/0wd;

    invoke-virtual {v4}, LX/0wd;->a()V

    .line 139487
    :cond_1
    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->h:LX/EVo;

    .line 139488
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->S(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)LX/1ly;

    move-result-object v1

    invoke-virtual {v1}, LX/1ly;->d()V

    .line 139489
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    .line 139490
    invoke-virtual {v1}, LX/2xj;->i()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 139491
    invoke-virtual {v1}, LX/2xj;->b()V

    .line 139492
    :cond_3
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1, p0}, LX/2xj;->b(LX/0oB;)V

    .line 139493
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    if-eqz v1, :cond_4

    .line 139494
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    .line 139495
    iget-object v4, v1, LX/EUD;->c:LX/2xj;

    iget-object v5, v1, LX/EUD;->b:LX/0oB;

    invoke-virtual {v4, v5}, LX/2xj;->b(LX/0oB;)V

    .line 139496
    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    .line 139497
    :cond_4
    const/16 v1, 0x2b

    const v2, -0x2cb2f7cb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x60c76ade

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139498
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->O:LX/EUa;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    .line 139499
    iget-object v5, v1, LX/EUa;->c:LX/0fx;

    if-eqz v5, :cond_0

    .line 139500
    iget-object v5, v1, LX/EUa;->c:LX/0fx;

    invoke-interface {v2, v5}, LX/0g8;->c(LX/0fx;)V

    .line 139501
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 139502
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v1, :cond_1

    .line 139503
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 139504
    iput-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 139505
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->t:LX/D7a;

    .line 139506
    iget-object v2, v1, LX/D7a;->b:LX/1PJ;

    iget-object v5, v1, LX/D7a;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, LX/1PJ;->a(Landroid/view/ViewGroup;)V

    .line 139507
    const/4 v2, 0x0

    iput-object v2, v1, LX/D7a;->a:Landroid/view/ViewGroup;

    .line 139508
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v1, :cond_2

    .line 139509
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-virtual {v1, v3}, LX/0g7;->a(LX/1OO;)V

    .line 139510
    iput-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    .line 139511
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-eqz v1, :cond_3

    .line 139512
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ab:LX/1OD;

    invoke-interface {v1, v2}, LX/1OQ;->b(LX/1OD;)V

    .line 139513
    iput-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    .line 139514
    :cond_3
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-eqz v1, :cond_5

    .line 139515
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    .line 139516
    iget-object v2, v1, LX/ETy;->H:LX/0oB;

    if-eqz v2, :cond_4

    .line 139517
    iget-object v2, v1, LX/ETy;->G:LX/2xj;

    iget-object v5, v1, LX/ETy;->H:LX/0oB;

    invoke-virtual {v2, v5}, LX/2xj;->b(LX/0oB;)V

    .line 139518
    :cond_4
    iput-object v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    .line 139519
    :cond_5
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    if-eqz v1, :cond_6

    .line 139520
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->N:LX/ESo;

    .line 139521
    iput-object v3, v1, LX/ESo;->j:Landroid/view/View;

    .line 139522
    :cond_6
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aa:LX/EUW;

    .line 139523
    iget-object v3, v1, LX/ETB;->l:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 139524
    const/16 v1, 0x2b

    const v2, -0x1ed65f7a

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4120937b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139525
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 139526
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v1, :cond_0

    .line 139527
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    invoke-virtual {v1}, LX/0g7;->A()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->al:Landroid/os/Parcelable;

    .line 139528
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->n:LX/EU4;

    invoke-virtual {v1}, LX/EU4;->a()V

    .line 139529
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0JZ;->a(Z)V

    .line 139530
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    if-eqz v1, :cond_1

    .line 139531
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139532
    iget-object v2, v1, LX/ETB;->t:LX/ETQ;

    move-object v1, v2

    .line 139533
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    invoke-virtual {v1, v2}, LX/ETQ;->b(LX/ESs;)V

    .line 139534
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ax:LX/EUF;

    if-eqz v1, :cond_2

    .line 139535
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139536
    iget-object v2, v1, LX/ETB;->t:LX/ETQ;

    move-object v1, v2

    .line 139537
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ax:LX/EUF;

    invoke-virtual {v1, v2}, LX/ETQ;->b(LX/ESs;)V

    .line 139538
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->q:LX/0bH;

    if-eqz v1, :cond_3

    .line 139539
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->p:LX/1B1;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->q:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 139540
    :cond_3
    const/16 v1, 0x2b

    const v2, 0x5c0dbcaf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x33c932b9    # -4.7920412E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139541
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 139542
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->al:Landroid/os/Parcelable;

    if-eqz v1, :cond_0

    .line 139543
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    .line 139544
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->al:Landroid/os/Parcelable;

    invoke-virtual {v1, v2}, LX/0g7;->a(Landroid/os/Parcelable;)V

    .line 139545
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-eqz v1, :cond_1

    .line 139546
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l:LX/EUH;

    const-string v2, "video_home"

    invoke-virtual {v1, v2}, LX/1LV;->a(Ljava/lang/String;)V

    .line 139547
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    invoke-virtual {v1}, LX/0xX;->j()Z

    move-result v1

    if-nez v1, :cond_6

    .line 139548
    :goto_0
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139549
    iget-object v2, v1, LX/ETB;->t:LX/ETQ;

    move-object v1, v2

    .line 139550
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->K:LX/EUY;

    invoke-virtual {v1, v2}, LX/ETQ;->a(LX/ESs;)V

    .line 139551
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->K:LX/EUY;

    .line 139552
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    if-nez v2, :cond_2

    .line 139553
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->I:LX/EUE;

    .line 139554
    new-instance v7, LX/EUD;

    invoke-static {v2}, LX/ESr;->a(LX/0QB;)LX/ESr;

    move-result-object v4

    check-cast v4, LX/ESr;

    invoke-static {v2}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v5

    check-cast v5, LX/2xj;

    invoke-static {v2}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v6

    check-cast v6, LX/ETB;

    invoke-direct {v7, v4, p0, v5, v6}, LX/EUD;-><init>(LX/ESr;Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/2xj;LX/ETB;)V

    .line 139555
    move-object v2, v7

    .line 139556
    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    .line 139557
    :cond_2
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aw:LX/EUD;

    move-object v2, v2

    .line 139558
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 139559
    if-eqz v2, :cond_3

    if-eqz v4, :cond_3

    .line 139560
    iget-object v5, v1, LX/EUY;->a:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 139561
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 139562
    :goto_1
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139563
    iget-object v6, v1, LX/EUY;->a:Ljava/util/Map;

    invoke-interface {v6, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139564
    :cond_3
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->q:LX/0bH;

    if-eqz v1, :cond_4

    .line 139565
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->p:LX/1B1;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->q:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 139566
    :cond_4
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v1}, LX/ETB;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 139567
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->P(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139568
    sget-object v1, LX/2rJ;->NORMAL:LX/2rJ;

    invoke-static {p0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a$redex0(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/2rJ;)V

    .line 139569
    :cond_5
    const/16 v1, 0x2b

    const v2, -0x1479e703

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 139570
    :cond_6
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    .line 139571
    iget-object v2, v1, LX/ETB;->t:LX/ETQ;

    move-object v1, v2

    .line 139572
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ax:LX/EUF;

    if-nez v2, :cond_7

    .line 139573
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Z:Ljava/util/List;

    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->S(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)LX/1ly;

    move-result-object v4

    .line 139574
    new-instance v5, LX/EUF;

    invoke-direct {v5, v2, v4}, LX/EUF;-><init>(Ljava/util/List;LX/1ly;)V

    .line 139575
    move-object v2, v5

    .line 139576
    iput-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ax:LX/EUF;

    .line 139577
    :cond_7
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ax:LX/EUF;

    move-object v2, v2

    .line 139578
    invoke-virtual {v1, v2}, LX/ETQ;->a(LX/ESs;)V

    goto/16 :goto_0

    .line 139579
    :cond_8
    iget-object v5, v1, LX/EUY;->a:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    goto :goto_1
.end method

.method public final onStart()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5f789b2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139580
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 139581
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->L()V

    .line 139582
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 139583
    if-eqz v1, :cond_0

    .line 139584
    const-string v2, "source_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 139585
    const-class v2, LX/1ZF;

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 139586
    if-eqz v2, :cond_0

    .line 139587
    invoke-interface {v2, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 139588
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    .line 139589
    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    invoke-virtual {v1, v2}, LX/0g7;->b(LX/0fx;)V

    .line 139590
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l:LX/EUH;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->R:LX/2zB;

    .line 139591
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, v1, LX/EUH;->g:Ljava/lang/ref/WeakReference;

    .line 139592
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l:LX/EUH;

    invoke-virtual {v1, v2}, LX/1Kt;->a(LX/1Ce;)V

    .line 139593
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 139594
    const/16 v1, 0x2b

    const v2, 0x3a02de10

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 139595
    :goto_0
    return-void

    .line 139596
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ai:Z

    if-eqz v1, :cond_3

    .line 139597
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 139598
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->e()V

    .line 139599
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ai:Z

    .line 139600
    :cond_3
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->j()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 139601
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    .line 139602
    iget-object v4, v1, LX/2xj;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7Qv;

    const-string v5, "RESUME"

    invoke-virtual {v4, v5}, LX/7Qv;->a(Ljava/lang/String;)V

    .line 139603
    iget-object v4, v1, LX/2xj;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1CC;

    .line 139604
    iget-object v6, v4, LX/1CC;->d:Ljava/lang/String;

    if-nez v6, :cond_7

    .line 139605
    iget-object v6, v4, LX/1CC;->b:LX/03V;

    iget-object v7, v4, LX/1CC;->a:Ljava/lang/String;

    const-string v8, "Foreground a session before session start."

    invoke-virtual {v6, v7, v8}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139606
    :cond_4
    const/4 v6, 0x0

    iput-boolean v6, v4, LX/1CC;->f:Z

    .line 139607
    iget-object v6, v4, LX/1CC;->c:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    iput-wide v6, v4, LX/1CC;->g:J

    .line 139608
    :goto_1
    iget-object v4, v1, LX/2xj;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3AW;

    .line 139609
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/0JT;->VIDEO_HOME_FOREGROUNDED:LX/0JT;

    iget-object v6, v6, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 139610
    invoke-static {v4, v5}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 139611
    sget-object v4, LX/D7e;->FOREGROUNDED:LX/D7e;

    invoke-static {v1, v4}, LX/2xj;->a(LX/2xj;LX/D7e;)V

    .line 139612
    invoke-static {v1}, LX/2xj;->l(LX/2xj;)V

    .line 139613
    :cond_5
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->g:LX/0hn;

    invoke-virtual {v1, p0}, LX/0hn;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139614
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2ns;->c:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 139615
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-eqz v1, :cond_6

    .line 139616
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    .line 139617
    new-instance v2, LX/EUL;

    invoke-direct {v2, p0}, LX/EUL;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    move-object v2, v2

    .line 139618
    invoke-interface {v1, v2}, LX/ETb;->a(Ljava/lang/Object;)V

    .line 139619
    :cond_6
    const v1, 0x7f929c4c

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto/16 :goto_0

    .line 139620
    :cond_7
    iget-boolean v6, v4, LX/1CC;->f:Z

    if-nez v6, :cond_4

    .line 139621
    iget-object v6, v4, LX/1CC;->b:LX/03V;

    iget-object v7, v4, LX/1CC;->a:Ljava/lang/String;

    const-string v8, "Session is already in foreground."

    invoke-virtual {v6, v7, v8}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onStop()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x79582f05

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 139622
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 139623
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M()V

    .line 139624
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    if-eqz v1, :cond_0

    .line 139625
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->as:LX/0g6;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    invoke-virtual {v1, v2}, LX/0g7;->c(LX/0fx;)V

    .line 139626
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 139627
    const/16 v1, 0x2b

    const v2, 0x126d4682

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 139628
    :goto_0
    return-void

    .line 139629
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->k()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 139630
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ai:Z

    .line 139631
    :goto_1
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->j()Z

    move-result v1

    if-nez v1, :cond_3

    .line 139632
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    .line 139633
    iget-object v5, v1, LX/2xj;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7Qv;

    const-string v6, "PAUSE"

    invoke-virtual {v5, v6}, LX/7Qv;->a(Ljava/lang/String;)V

    .line 139634
    iget-object v5, v1, LX/2xj;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1CC;

    invoke-virtual {v5}, LX/1CC;->g()J

    move-result-wide v7

    .line 139635
    iget-object v5, v1, LX/2xj;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1CC;

    .line 139636
    iget-object v9, v5, LX/1CC;->d:Ljava/lang/String;

    if-nez v9, :cond_7

    .line 139637
    iget-object v9, v5, LX/1CC;->b:LX/03V;

    iget-object v10, v5, LX/1CC;->a:Ljava/lang/String;

    const-string v11, "Background a session before session start."

    invoke-virtual {v9, v10, v11}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139638
    :cond_2
    const/4 v9, 0x1

    iput-boolean v9, v5, LX/1CC;->f:Z

    .line 139639
    const-wide/16 v9, 0x0

    iput-wide v9, v5, LX/1CC;->g:J

    .line 139640
    :goto_2
    iget-object v5, v1, LX/2xj;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3AW;

    .line 139641
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v9, LX/0JT;->VIDEO_HOME_BACKGROUNDED:LX/0JT;

    iget-object v9, v9, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v6, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 139642
    sget-object v9, LX/0JS;->SESSION_DURATION:LX/0JS;

    iget-object v9, v9, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v6, v9, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 139643
    invoke-static {v5, v6}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 139644
    sget-object v5, LX/D7e;->BACKGROUNDED:LX/D7e;

    invoke-static {v1, v5}, LX/2xj;->a(LX/2xj;LX/D7e;)V

    .line 139645
    invoke-static {v1}, LX/2xj;->l(LX/2xj;)V

    .line 139646
    :cond_3
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Q()V

    .line 139647
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->g:LX/0hn;

    invoke-virtual {v1, v4}, LX/0hn;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139648
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-eqz v1, :cond_4

    .line 139649
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    invoke-interface {v1, v4}, LX/ETb;->a(Ljava/lang/Object;)V

    .line 139650
    :cond_4
    const v1, 0x12083363

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto/16 :goto_0

    .line 139651
    :cond_5
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->k()Z

    move-result v1

    if-nez v1, :cond_6

    .line 139652
    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v1}, LX/2xj;->d()V

    .line 139653
    :cond_6
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ai:Z

    goto/16 :goto_1

    .line 139654
    :cond_7
    iget-boolean v9, v5, LX/1CC;->f:Z

    if-eqz v9, :cond_2

    .line 139655
    iget-object v9, v5, LX/1CC;->b:LX/03V;

    iget-object v10, v5, LX/1CC;->a:Ljava/lang/String;

    const-string v11, "Session is already in background."

    invoke-virtual {v9, v10, v11}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final setUserVisibleHint(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 139656
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 139657
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l:LX/EUH;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 139658
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l:LX/EUH;

    const-string v3, "video_home"

    invoke-virtual {v0, v3}, LX/1LV;->a(Ljava/lang/String;)V

    .line 139659
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ah:Z

    if-ne v0, p1, :cond_2

    iget-boolean v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ah:Z

    iget-boolean v3, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 139660
    :goto_0
    iput-boolean p1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ag:Z

    .line 139661
    if-eqz v0, :cond_3

    .line 139662
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 139663
    goto :goto_0

    .line 139664
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    if-eqz v0, :cond_1

    .line 139665
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->G:LX/EVd;

    .line 139666
    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/EVd;->a(LX/EVd;Z)V

    .line 139667
    iput-boolean p1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ah:Z

    .line 139668
    invoke-direct {p0, v2, v2}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(ZZ)V

    .line 139669
    if-eqz p1, :cond_6

    .line 139670
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->L()V

    .line 139671
    :goto_2
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->e()Z

    move-result v0

    if-nez v0, :cond_9

    .line 139672
    if-eqz p1, :cond_4

    .line 139673
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139674
    :cond_4
    :goto_3
    if-nez p1, :cond_8

    .line 139675
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    invoke-interface {v0}, LX/ETh;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139676
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 139677
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->m:LX/0JZ;

    invoke-virtual {v0, v1}, LX/0JZ;->a(Z)V

    .line 139678
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->b()V

    .line 139679
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->n:LX/EU4;

    invoke-virtual {v0}, LX/EU4;->a()V

    .line 139680
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    sget-object v1, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 139681
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->V:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AX;

    .line 139682
    sget-object v1, LX/0JU;->EXIT_VIDEO_HOME:LX/0JU;

    invoke-static {v0, v1}, LX/3AX;->a(LX/3AX;LX/0JU;)Z

    .line 139683
    :goto_4
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->Q()V

    .line 139684
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->l:LX/EUH;

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, LX/1LV;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 139685
    :cond_6
    invoke-direct {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M()V

    goto :goto_2

    .line 139686
    :cond_7
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->e()Z

    move-result v0

    if-nez v0, :cond_a

    .line 139687
    :goto_5
    goto :goto_4

    .line 139688
    :cond_8
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ad:LX/0J9;

    sget-object v1, LX/0J9;->PREFETCHED_LOADED:LX/0J9;

    if-ne v0, v1, :cond_1

    .line 139689
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->p$redex0(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    goto/16 :goto_1

    .line 139690
    :cond_9
    invoke-static {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->P(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    .line 139691
    if-eqz p1, :cond_4

    .line 139692
    sget-object v0, LX/2rJ;->NORMAL:LX/2rJ;

    invoke-static {p0, v0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a$redex0(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/2rJ;)V

    goto :goto_3

    .line 139693
    :cond_a
    iget-object v0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->M:LX/ETB;

    iget-object v1, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->g:LX/0hn;

    iget-object v2, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    invoke-virtual {v2}, LX/0xX;->e()I

    move-result v2

    sget-object v3, LX/0JU;->EXIT_VIDEO_HOME:LX/0JU;

    invoke-virtual {v0, v1, v2, v3}, LX/ETB;->a(LX/0ho;ILX/0JU;)V

    goto :goto_5
.end method
