.class public final Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Lv;


# direct methods
.method public constructor <init>(LX/1Lv;)V
    .locals 0

    .prologue
    .line 234579
    iput-object p1, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mReentrantLock"
    .end annotation

    .prologue
    .line 234601
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    invoke-static {v0}, LX/1Lv;->e(LX/1Lv;)V

    .line 234602
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->f:LX/1Lg;

    invoke-virtual {v0}, LX/1Lg;->b()LX/374;

    move-result-object v0

    .line 234603
    if-nez v0, :cond_0

    .line 234604
    :goto_0
    return-void

    .line 234605
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->D:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/19w;

    .line 234606
    iget-object v2, v0, LX/374;->b:LX/36s;

    move-object v2, v2

    .line 234607
    iget-object v2, v2, LX/36s;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234608
    const/4 v1, 0x1

    .line 234609
    :goto_1
    move v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234610
    iget-object v2, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->f:LX/1Lg;

    invoke-virtual {v2, v0, v1}, LX/1Lg;->a(LX/374;Z)V

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->f:LX/1Lg;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/1Lg;->a(LX/374;Z)V

    throw v1

    .line 234611
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    .line 234612
    new-instance v2, LX/375;

    iget-object v3, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    invoke-direct {v2, v3, v1, v0}, LX/375;-><init>(LX/1Lv;ILX/374;)V

    invoke-virtual {v2}, LX/375;->a()Z

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 234580
    const/4 v1, 0x0

    .line 234581
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-boolean v0, v0, LX/1Lv;->C:Z

    if-nez v0, :cond_0

    .line 234582
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->z:LX/19s;

    invoke-virtual {v0}, LX/19s;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->z:LX/19s;

    invoke-virtual {v0}, LX/19s;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234583
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->d:LX/1Ln;

    instance-of v0, v0, LX/1Lo;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 234584
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->d:LX/1Ln;

    check-cast v0, LX/1Lo;

    .line 234585
    invoke-virtual {v0}, LX/1Lo;->b()V

    .line 234586
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    const/4 v2, 0x1

    .line 234587
    iput-boolean v2, v0, LX/1Lv;->C:Z

    .line 234588
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    invoke-static {v0}, LX/1Lv;->g(LX/1Lv;)Z
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 234589
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a()V
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 234590
    if-eqz v0, :cond_1

    .line 234591
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    invoke-static {v0}, LX/1Lv;->h(LX/1Lv;)V

    .line 234592
    :cond_1
    :goto_1
    return-void

    .line 234593
    :cond_2
    goto :goto_0

    .line 234594
    :catch_0
    move v0, v1

    .line 234595
    :goto_2
    if-eqz v0, :cond_1

    .line 234596
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    invoke-static {v0}, LX/1Lv;->h(LX/1Lv;)V

    goto :goto_1

    .line 234597
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    .line 234598
    iget-object v1, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$2;->a:LX/1Lv;

    invoke-static {v1}, LX/1Lv;->h(LX/1Lv;)V

    :cond_3
    throw v0

    .line 234599
    :catchall_1
    move-exception v1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_3

    move-exception v1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_3

    .line 234600
    :catch_1
    goto :goto_2
.end method
