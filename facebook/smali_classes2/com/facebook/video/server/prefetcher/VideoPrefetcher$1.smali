.class public final Lcom/facebook/video/server/prefetcher/VideoPrefetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Lv;


# direct methods
.method public constructor <init>(LX/1Lv;)V
    .locals 0

    .prologue
    .line 234568
    iput-object p1, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$1;->a:LX/1Lv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 234569
    iget-object v0, p0, Lcom/facebook/video/server/prefetcher/VideoPrefetcher$1;->a:LX/1Lv;

    .line 234570
    iget-object v1, v0, LX/1Lv;->j:LX/1Lr;

    iget-boolean v1, v1, LX/1Lr;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1Lv;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/1Lv;->l:LX/0yH;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v1, v2}, LX/0yH;->a(LX/0yY;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 234571
    if-eqz v1, :cond_2

    .line 234572
    :cond_1
    :goto_1
    return-void

    .line 234573
    :cond_2
    invoke-static {v0}, LX/1Lv;->j(LX/1Lv;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234574
    :try_start_0
    iget-object v1, v0, LX/1Lv;->g:LX/0TD;

    iget-object v2, v0, LX/1Lv;->J:Ljava/lang/Runnable;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0TD;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 234575
    new-instance v2, LX/36u;

    invoke-direct {v2, v0}, LX/36u;-><init>(LX/1Lv;)V

    iget-object v3, v0, LX/1Lv;->g:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234576
    :goto_2
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 234577
    :catch_0
    move-exception v1

    .line 234578
    iget-object v2, v0, LX/1Lv;->h:LX/03V;

    sget-object v3, LX/1Lv;->a:Ljava/lang/String;

    const-string p0, "Error while submitting prefetching task"

    invoke-virtual {v2, v3, p0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
