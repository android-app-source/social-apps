.class public final Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Lu;

.field public b:I


# direct methods
.method public constructor <init>(LX/1Lu;)V
    .locals 0

    .prologue
    .line 234447
    iput-object p1, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->a:LX/1Lu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 234448
    iget-object v0, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->a:LX/1Lu;

    iget-object v0, v0, LX/1Lu;->b:LX/0ad;

    sget v1, LX/7Or;->b:I

    const/16 v2, 0x2710

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v1

    .line 234449
    iget-object v2, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->a:LX/1Lu;

    monitor-enter v2

    .line 234450
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->a:LX/1Lu;

    iget-object v0, v0, LX/1Lu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 234451
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->b:I

    .line 234452
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->a:LX/1Lu;

    iget-object v0, v0, LX/1Lu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7PP;

    .line 234453
    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, LX/7PP;->a(J)V

    goto :goto_1

    .line 234454
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 234455
    :cond_2
    iget v0, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->b:I

    add-int/lit8 v3, v0, 0x1

    iput v3, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->b:I

    const/16 v3, 0xa

    if-le v0, v3, :cond_0

    .line 234456
    iget-object v0, p0, Lcom/facebook/video/server/TimeoutStreamHelper$MonitoringRunnable;->a:LX/1Lu;

    invoke-static {v0}, LX/1Lu;->b(LX/1Lu;)V

    goto :goto_0
.end method
