.class public Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Rd;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/Azy;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0oJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 246573
    new-instance v0, LX/1Re;

    invoke-direct {v0}, LX/1Re;-><init>()V

    sput-object v0, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0oJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246574
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 246575
    iput-object p1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;->b:LX/0oJ;

    .line 246576
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;
    .locals 4

    .prologue
    .line 246561
    const-class v1, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;

    monitor-enter v1

    .line 246562
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 246563
    sput-object v2, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 246564
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246565
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 246566
    new-instance p0, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;

    invoke-static {v0}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v3

    check-cast v3, LX/0oJ;

    invoke-direct {p0, v3}, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;-><init>(LX/0oJ;)V

    .line 246567
    move-object v0, p0

    .line 246568
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 246569
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246570
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 246571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Azy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246572
    sget-object v0, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x215a5788

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 246550
    check-cast p4, LX/Azy;

    .line 246551
    iget-object v1, p4, LX/Azy;->e:LX/87h;

    new-instance v2, LX/Azx;

    invoke-direct {v2, p4}, LX/Azx;-><init>(LX/Azy;)V

    .line 246552
    new-instance p0, LX/88A;

    invoke-direct {p0}, LX/88A;-><init>()V

    move-object p0, p0

    .line 246553
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->d:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    const/4 p1, 0x1

    .line 246554
    iput-boolean p1, p0, LX/0zO;->p:Z

    .line 246555
    move-object p0, p0

    .line 246556
    iget-object p1, v1, LX/87h;->a:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    move-object p0, p0

    .line 246557
    new-instance p1, LX/87g;

    invoke-direct {p1, v1, v2}, LX/87g;-><init>(LX/87h;LX/Azx;)V

    iget-object p2, v1, LX/87h;->b:Ljava/util/concurrent/Executor;

    invoke-static {p0, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 246558
    iget-object v1, p4, LX/Azy;->b:LX/Azu;

    .line 246559
    iget-object v2, v1, LX/Azu;->e:LX/Azt;

    sget-object p0, LX/Azs;->RESUMED:LX/Azs;

    invoke-virtual {v2, p0}, LX/Azt;->a(LX/Azs;)V

    .line 246560
    const/16 v1, 0x1f

    const v2, -0x10890556

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 246548
    check-cast p1, LX/1Rd;

    .line 246549
    iget-boolean v0, p1, LX/1Rd;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsComposerCameraPartDefinition;->b:LX/0oJ;

    invoke-virtual {v0}, LX/0oJ;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 246543
    check-cast p4, LX/Azy;

    .line 246544
    iget-object p0, p4, LX/Azy;->b:LX/Azu;

    .line 246545
    iget-object p1, p0, LX/Azu;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/Azt;

    .line 246546
    sget-object p4, LX/Azs;->DESTROYED:LX/Azs;

    invoke-virtual {p1, p4}, LX/Azt;->a(LX/Azs;)V

    goto :goto_0

    .line 246547
    :cond_0
    return-void
.end method
