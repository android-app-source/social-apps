.class public Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264994
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 264995
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    .line 264996
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;

    .line 264997
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    .line 264998
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;
    .locals 6

    .prologue
    .line 264999
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;

    monitor-enter v1

    .line 265000
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265001
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265002
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265003
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265004
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;)V

    .line 265005
    move-object v0, p0

    .line 265006
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265007
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265008
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265009
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 265010
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265011
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 265012
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265013
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265014
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265015
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 265016
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265017
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265018
    invoke-static {p1}, Lcom/facebook/feedplugins/hotconversations/HotConversationsGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
