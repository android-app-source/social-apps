.class public Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ph;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3m6;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:LX/1Ua;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iF",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pf;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 268709
    new-instance v0, LX/1WW;

    invoke-direct {v0}, LX/1WW;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->a:LX/1Cz;

    .line 268710
    const-class v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 268711
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 1
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iF;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pf;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268712
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 268713
    iput-object p1, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->d:LX/0Ot;

    .line 268714
    iput-object p2, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->e:LX/0Ot;

    .line 268715
    iput-object p3, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->f:LX/0Ot;

    .line 268716
    iput-object p4, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->g:LX/0Ot;

    .line 268717
    iput-object p5, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->h:LX/0Ot;

    .line 268718
    iput-object p7, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->i:LX/0Ot;

    .line 268719
    invoke-interface {p6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->k:Ljava/lang/String;

    .line 268720
    iput-object p8, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->j:LX/0Or;

    .line 268721
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;
    .locals 12

    .prologue
    .line 268722
    const-class v1, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    monitor-enter v1

    .line 268723
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268724
    sput-object v2, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268725
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268726
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268727
    new-instance v3, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    const/16 v4, 0xe0f

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x9a6

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xe12

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x756

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xa5f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x12cb

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xdd

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x19e

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 268728
    move-object v0, v3

    .line 268729
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268730
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268731
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268732
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 268733
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pf;

    invoke-virtual {v0, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 268734
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 268735
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 268736
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 268737
    :goto_1
    return-object v0

    .line 268738
    :cond_0
    iget-object v1, v0, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v1

    .line 268739
    goto :goto_0

    .line 268740
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 268741
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 268742
    iget-boolean p1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, p1

    .line 268743
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 268744
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 268745
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->k:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 268746
    sget-object v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 268747
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 268748
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268749
    const v1, 0x7f0d3132

    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v1, v0, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 268750
    const v2, 0x7f0d3134

    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v3

    .line 268751
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 268752
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v1}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v3, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 268753
    iput-object v3, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 268754
    move-object v1, v1

    .line 268755
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v2, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 268756
    const v1, 0x7f0d3134

    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/3iE;

    invoke-direct {v2, p0, p2, p3}, LX/3iE;-><init>(Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)V

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 268757
    const v1, 0x7f0d3135

    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/3iE;

    invoke-direct {v2, p0, p2, p3}, LX/3iE;-><init>(Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)V

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 268758
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iF;

    .line 268759
    iget-object v5, v0, LX/3iF;->g:LX/0pf;

    .line 268760
    iget-object v4, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 268761
    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v5, v4}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v4

    .line 268762
    if-eqz v4, :cond_0

    .line 268763
    iget-object v5, v4, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v5, v5

    .line 268764
    if-eqz v5, :cond_0

    invoke-static {p2}, LX/3iF;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 268765
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 268766
    :cond_1
    iget-object v5, v4, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v6, v5

    .line 268767
    iget-object v4, v0, LX/3iF;->b:LX/3iK;

    .line 268768
    iget-object v5, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 268769
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    const-class v7, LX/3iF;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const/4 v8, 0x1

    new-instance v9, LX/CDn;

    invoke-direct {v9, v0, p2, v6}, LX/CDn;-><init>(LX/3iF;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    invoke-virtual/range {v4 .. v9}, LX/3iK;->a(Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;ZLX/451;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 268770
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268771
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268772
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268773
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268774
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268775
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
