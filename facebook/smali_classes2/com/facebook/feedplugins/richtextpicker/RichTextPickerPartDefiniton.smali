.class public Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/1Pf;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/1Vx;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Vx;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 266974
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 266975
    iput-object p2, p0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->d:LX/1Vx;

    .line 266976
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266958
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->d:LX/1Vx;

    const/4 v1, 0x0

    .line 266959
    new-instance v2, LX/CCA;

    invoke-direct {v2, v0}, LX/CCA;-><init>(LX/1Vx;)V

    .line 266960
    sget-object p0, LX/1Vx;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CC9;

    .line 266961
    if-nez p0, :cond_0

    .line 266962
    new-instance p0, LX/CC9;

    invoke-direct {p0}, LX/CC9;-><init>()V

    .line 266963
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/CC9;->a$redex0(LX/CC9;LX/1De;IILX/CCA;)V

    .line 266964
    move-object v2, p0

    .line 266965
    move-object v1, v2

    .line 266966
    move-object v0, v1

    .line 266967
    iget-object v1, v0, LX/CC9;->a:LX/CCA;

    iput-object p3, v1, LX/CCA;->a:LX/1Pf;

    .line 266968
    iget-object v1, v0, LX/CC9;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266969
    move-object v0, v0

    .line 266970
    iget-object v1, v0, LX/CC9;->a:LX/CCA;

    iput-object p2, v1, LX/CCA;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266971
    iget-object v1, v0, LX/CC9;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266972
    move-object v0, v0

    .line 266973
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;
    .locals 5

    .prologue
    .line 266938
    const-class v1, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    monitor-enter v1

    .line 266939
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266940
    sput-object v2, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266941
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266942
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266943
    new-instance p0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Vx;->a(LX/0QB;)LX/1Vx;

    move-result-object v4

    check-cast v4, LX/1Vx;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;-><init>(Landroid/content/Context;LX/1Vx;)V

    .line 266944
    move-object v0, p0

    .line 266945
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266946
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266947
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266948
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 266957
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 266977
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerPartDefiniton;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 266949
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 266950
    if-eqz p1, :cond_0

    .line 266951
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266952
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 266953
    :goto_0
    return v0

    .line 266954
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266955
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const v2, -0x74569f26

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 266956
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_TEXT_FORMATTING:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 266935
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266936
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266937
    check-cast v0, LX/0jW;

    return-object v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 266934
    sget-object v0, LX/1X8;->FOLLOWUP_FEEDUNIT:LX/1X8;

    return-object v0
.end method
