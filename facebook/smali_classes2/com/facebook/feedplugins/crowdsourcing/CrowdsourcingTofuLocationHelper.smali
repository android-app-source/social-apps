.class public Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final g:Lcom/facebook/location/FbLocationOperationParams;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1S8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Cc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 247638
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/16 v2, 0x2710

    .line 247639
    iput-wide v2, v0, LX/1S7;->d:J

    .line 247640
    move-object v0, v0

    .line 247641
    const/high16 v1, 0x42c80000    # 100.0f

    .line 247642
    iput v1, v0, LX/1S7;->c:F

    .line 247643
    move-object v0, v0

    .line 247644
    const-wide/32 v2, 0xea60

    .line 247645
    iput-wide v2, v0, LX/1S7;->b:J

    .line 247646
    move-object v0, v0

    .line 247647
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->g:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247649
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;
    .locals 7

    .prologue
    .line 247650
    new-instance v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    invoke-direct {v0}, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;-><init>()V

    .line 247651
    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 247652
    new-instance v5, LX/1S8;

    invoke-direct {v5}, LX/1S8;-><init>()V

    .line 247653
    invoke-static {p0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v4

    check-cast v4, LX/0yD;

    .line 247654
    iput-object v4, v5, LX/1S8;->a:LX/0yD;

    .line 247655
    move-object v4, v5

    .line 247656
    check-cast v4, LX/1S8;

    const/16 v5, 0xc81

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x13ac

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    .line 247657
    iput-object v1, v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->a:Ljava/util/concurrent/Executor;

    iput-object v2, v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->b:LX/0SG;

    iput-object v3, v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->c:Landroid/content/Context;

    iput-object v4, v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->d:LX/1S8;

    iput-object v5, v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->e:LX/0Or;

    iput-object v6, v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->f:LX/0Or;

    .line 247658
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2ta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247659
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    .line 247660
    iget-object v2, p0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1sS;

    .line 247661
    sget-object v3, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->g:Lcom/facebook/location/FbLocationOperationParams;

    const-class v4, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    const-string v5, "crowdsourcing"

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 247662
    move-object v2, v2

    .line 247663
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 247664
    iget-object v3, p0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2Cc;

    .line 247665
    new-instance v4, LX/1sO;

    const-wide/16 v5, 0x2710

    const-wide/32 v7, 0xea60

    invoke-direct {v4, v5, v6, v7, v8}, LX/1sO;-><init>(JJ)V

    invoke-virtual {v3, v4}, LX/2Cc;->a(LX/1sO;)V

    .line 247666
    move-object v2, v3

    .line 247667
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 247668
    new-instance v1, LX/34k;

    invoke-direct {v1, p0}, LX/34k;-><init>(Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;)V

    iget-object v2, p0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
