.class public Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pe;",
        "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/14w;


# direct methods
.method public constructor <init>(LX/0Ot;LX/14w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;",
            ">;",
            "LX/14w;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269652
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 269653
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a:LX/0Ot;

    .line 269654
    iput-object p2, p0, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->b:LX/14w;

    .line 269655
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;
    .locals 5

    .prologue
    .line 269656
    const-class v1, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    monitor-enter v1

    .line 269657
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269658
    sput-object v2, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269659
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269660
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269661
    new-instance v4, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    const/16 v3, 0x8ae

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-direct {v4, p0, v3}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;-><init>(LX/0Ot;LX/14w;)V

    .line 269662
    move-object v0, v4

    .line 269663
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269664
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269665
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269666
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 269667
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 269668
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 269669
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/20y;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, LX/20y;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 269670
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 269671
    invoke-static {p1}, LX/14w;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 269672
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
