.class public Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/20E;
.implements LX/20F;
.implements LX/1wK;
.implements LX/0xi;


# static fields
.field public static final h:LX/1Cz;


# instance fields
.field public a:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/20J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/20O;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/20P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:LX/20Q;

.field private final j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

.field public final k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

.field public l:LX/20H;

.field public m:LX/21M;

.field private n:LX/0wd;

.field public o:LX/20z;

.field private p:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;

.field public q:LX/20I;

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 338796
    new-instance v0, LX/20G;

    invoke-direct {v0}, LX/20G;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 338797
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 338798
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338799
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 338800
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->l:LX/20H;

    .line 338801
    sget-object v0, LX/20I;->LIGHT:LX/20I;

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->q:LX/20I;

    .line 338802
    const-class v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 338803
    invoke-static {p1, p2}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->c(Z)V

    .line 338804
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f:LX/20O;

    invoke-interface {v0}, LX/20O;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 338805
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f:LX/20O;

    invoke-interface {v0}, LX/20O;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/20Q;

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    .line 338806
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f:LX/20O;

    invoke-interface {v0}, LX/20O;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    .line 338807
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    invoke-interface {v0, v1}, LX/1wK;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    .line 338808
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->g:LX/20P;

    invoke-virtual {v0, p0}, LX/20P;->a(LX/20E;)LX/20b;

    move-result-object v0

    .line 338809
    iget-object v1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 338810
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/1zf;LX/20J;LX/20K;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/20O;LX/20P;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            "LX/1zf;",
            "LX/20J;",
            "LX/20K;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/20O;",
            "LX/20P;",
            ")V"
        }
    .end annotation

    .prologue
    .line 338811
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a:LX/1zf;

    iput-object p2, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->b:LX/20J;

    iput-object p3, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->c:LX/20K;

    iput-object p4, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p6, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f:LX/20O;

    iput-object p7, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->g:LX/20P;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-static {v7}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v1

    check-cast v1, LX/1zf;

    invoke-static {v7}, LX/20J;->b(LX/0QB;)LX/20J;

    move-result-object v2

    check-cast v2, LX/20J;

    invoke-static {v7}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v3

    check-cast v3, LX/20K;

    const/16 v4, 0x3567

    invoke-static {v7, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v7}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v5

    check-cast v5, Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v6, LX/20N;

    invoke-direct {v6}, LX/20N;-><init>()V

    move-object v6, v6

    move-object v6, v6

    check-cast v6, LX/20O;

    const-class v8, LX/20P;

    invoke-interface {v7, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/20P;

    invoke-static/range {v0 .. v7}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/1zf;LX/20J;LX/20K;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/20O;LX/20P;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/util/AttributeSet;)Z
    .locals 3
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 338812
    if-nez p1, :cond_0

    .line 338813
    :goto_0
    return v0

    .line 338814
    :cond_0
    sget-object v1, LX/03r;->ReactionsFooterView:[I

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 338815
    if-eqz v1, :cond_1

    const/16 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 338816
    :cond_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 338817
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->l:LX/20H;

    sget-object v1, LX/20H;->REACTIONS:LX/20H;

    if-eq v0, v1, :cond_0

    .line 338818
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 338819
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 338820
    const/4 v0, 0x0

    .line 338821
    :goto_0
    return v0

    .line 338822
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->c:LX/20K;

    invoke-virtual {v0, p0}, LX/20K;->a(LX/20F;)V

    .line 338823
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->c:LX/20K;

    iget-object v1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v1}, LX/20Q;->getReactionsDockAnchor()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/20K;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 338824
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(Z)V
    .locals 1

    .prologue
    .line 338825
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f:LX/20O;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f:LX/20O;

    invoke-interface {v0}, LX/20O;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 338826
    :cond_0
    new-instance v0, LX/20N;

    invoke-direct {v0}, LX/20N;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f:LX/20O;

    .line 338827
    :cond_1
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 338828
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, v1}, LX/20Q;->setFooterAlpha(F)V

    .line 338829
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 338830
    return-void
.end method

.method private e(LX/0wd;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    const/high16 v9, 0x42c80000    # 100.0f

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const/high16 v8, 0x42480000    # 50.0f

    .line 338831
    iget-object v6, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v9

    sub-float v0, v8, v0

    div-float/2addr v0, v8

    invoke-interface {v6, v0}, LX/20Q;->setFooterAlpha(F)V

    .line 338832
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v2 .. v7}, LX/0xw;->a(DDD)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v9

    sub-float/2addr v1, v8

    div-float/2addr v1, v8

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 338833
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    cmpl-double v0, v0, v4

    if-ltz v0, :cond_0

    .line 338834
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, v11}, LX/20Q;->setFooterVisibility(I)V

    .line 338835
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v10}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 338836
    :goto_0
    return-void

    .line 338837
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, v10}, LX/20Q;->setFooterVisibility(I)V

    .line 338838
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v11}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    goto :goto_0
.end method

.method private f()LX/0Ve;
    .locals 1

    .prologue
    .line 338839
    new-instance v0, LX/C4J;

    invoke-direct {v0, p0}, LX/C4J;-><init>(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;)V

    return-object v0
.end method

.method private getRequestLayoutRunnable()Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;
    .locals 2

    .prologue
    .line 338841
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->p:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;

    if-nez v0, :cond_0

    .line 338842
    new-instance v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;-><init>(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->p:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;

    .line 338843
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->p:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;

    return-object v0
.end method


# virtual methods
.method public final a(LX/20X;)Landroid/view/View;
    .locals 1

    .prologue
    .line 338840
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 338889
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0}, LX/1wK;->a()V

    .line 338890
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    if-eqz v0, :cond_0

    .line 338891
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 338892
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    .line 338893
    return-void
.end method

.method public final a(LX/0wd;)V
    .locals 0

    .prologue
    .line 338887
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->e(LX/0wd;)V

    .line 338888
    return-void
.end method

.method public final a(LX/20H;Z)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 338867
    sget-object v0, LX/2yt;->a:[I

    invoke-virtual {p1}, LX/20H;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 338868
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->l:LX/20H;

    .line 338869
    return-void

    .line 338870
    :pswitch_0
    if-eqz p2, :cond_2

    .line 338871
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    if-eqz v0, :cond_1

    .line 338872
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 338873
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 338874
    invoke-virtual {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 338875
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, v4}, LX/20Q;->setFooterVisibility(I)V

    .line 338876
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 338877
    invoke-direct {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->e()V

    goto :goto_1

    .line 338878
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v4}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a(Z)V

    .line 338879
    if-eqz p2, :cond_4

    .line 338880
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    if-eqz v0, :cond_3

    .line 338881
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 338882
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 338883
    invoke-virtual {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 338884
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, v2}, LX/20Q;->setFooterVisibility(I)V

    .line 338885
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v4}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 338886
    invoke-direct {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->e()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/View;LX/1zt;)V
    .locals 3

    .prologue
    .line 338859
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->m:LX/21M;

    if-eqz v0, :cond_0

    sget-object v0, LX/1zt;->c:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 338860
    :cond_0
    :goto_0
    return-void

    .line 338861
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "reactions_like_up"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 338862
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->m:LX/21M;

    invoke-direct {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->f()LX/0Ve;

    move-result-object v1

    invoke-interface {v0, p0, p2, v1}, LX/21M;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 338863
    invoke-virtual {p0, p2}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setReaction(LX/1zt;)V

    .line 338864
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 338865
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getRequestLayoutRunnable()Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView$RequestLayoutRunnable;

    move-result-object v1

    const v2, -0x31223f4

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 338866
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 338853
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    if-ne p1, v0, :cond_0

    .line 338854
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820003

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 338855
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820004

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 338856
    sget-object v0, LX/20H;->REACTIONS:LX/20H;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20H;Z)V

    .line 338857
    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(Landroid/view/MotionEvent;)Z

    .line 338858
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 338894
    if-eqz p1, :cond_0

    sget-object v0, LX/20H;->REACTIONS:LX/20H;

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20H;Z)V

    .line 338895
    return-void

    .line 338896
    :cond_0
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 338851
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0}, LX/20Q;->b()V

    .line 338852
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 0

    .prologue
    .line 338850
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 338848
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->a(Z)V

    .line 338849
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 0

    .prologue
    .line 338847
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 338844
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, v1}, LX/20Q;->setFooterVisibility(I)V

    .line 338845
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->setVisibility(I)V

    .line 338846
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 338792
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 338793
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->b:LX/20J;

    invoke-virtual {v0, p0, p1}, LX/20J;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 338794
    return-void
.end method

.method public getDockTheme()LX/20I;
    .locals 1

    .prologue
    .line 338795
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->q:LX/20I;

    return-object v0
.end method

.method public getInteractionLogger()LX/20z;
    .locals 1

    .prologue
    .line 338763
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->o:LX/20z;

    return-object v0
.end method

.method public getMode()LX/20H;
    .locals 1

    .prologue
    .line 338762
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->l:LX/20H;

    return-object v0
.end method

.method public getSupportedReactions()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338759
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->r:LX/0Px;

    if-eqz v0, :cond_0

    .line 338760
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->r:LX/0Px;

    .line 338761
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->d()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 338755
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 338756
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20H;Z)V

    .line 338757
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->c:LX/20K;

    invoke-virtual {v0}, LX/20K;->a()V

    .line 338758
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 338736
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1db2e692

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 338754
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x79342f1f

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setBottomDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 338751
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->b:LX/20J;

    .line 338752
    iput-object p1, v0, LX/20J;->e:LX/1Wl;

    .line 338753
    return-void
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 338747
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V

    .line 338748
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->j:Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;

    .line 338749
    iput-object p1, v0, Lcom/facebook/feedback/reactions/ui/ReactionsFooterSelectionView;->c:Landroid/graphics/drawable/Drawable;

    .line 338750
    return-void
.end method

.method public setButtonContainerHeight(I)V
    .locals 1

    .prologue
    .line 338745
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setButtonContainerHeight(I)V

    .line 338746
    return-void
.end method

.method public setButtonWeights([F)V
    .locals 1

    .prologue
    .line 338743
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setButtonWeights([F)V

    .line 338744
    return-void
.end method

.method public setButtons(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338741
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setButtons(Ljava/util/Set;)V

    .line 338742
    return-void
.end method

.method public setDockTheme(LX/20I;)V
    .locals 0

    .prologue
    .line 338739
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->q:LX/20I;

    .line 338740
    return-void
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 1

    .prologue
    .line 338737
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setDownstateType(LX/1Wk;)V

    .line 338738
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 338764
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setEnabled(Z)V

    .line 338765
    return-void
.end method

.method public setFadeStateSpring(LX/0wd;)V
    .locals 0

    .prologue
    .line 338766
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->n:LX/0wd;

    .line 338767
    invoke-virtual {p1, p0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 338768
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->e(LX/0wd;)V

    .line 338769
    return-void
.end method

.method public setHasCachedComments(Z)V
    .locals 1

    .prologue
    .line 338770
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setHasCachedComments(Z)V

    .line 338771
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 0

    .prologue
    .line 338772
    return-void
.end method

.method public setOnButtonClickedListener(LX/20Z;)V
    .locals 1

    .prologue
    .line 338773
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setOnButtonClickedListener(LX/20Z;)V

    .line 338774
    return-void
.end method

.method public setProgressiveUfiState(LX/21H;)V
    .locals 1

    .prologue
    .line 338775
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/20Q;->setProgressiveUfiState(LX/21H;)V

    .line 338776
    return-void
.end method

.method public setReaction(LX/1zt;)V
    .locals 1

    .prologue
    .line 338777
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->k:Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setReaction(LX/1zt;)V

    .line 338778
    return-void
.end method

.method public setReactionMutateListener(LX/21M;)V
    .locals 0

    .prologue
    .line 338779
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->m:LX/21M;

    .line 338780
    return-void
.end method

.method public setReactionsLogger(LX/20z;)V
    .locals 0

    .prologue
    .line 338781
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->o:LX/20z;

    .line 338782
    return-void
.end method

.method public setShowIcons(Z)V
    .locals 1

    .prologue
    .line 338783
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setShowIcons(Z)V

    .line 338784
    return-void
.end method

.method public setSprings(Ljava/util/EnumMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338785
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->i:LX/20Q;

    invoke-interface {v0, p1}, LX/1wK;->setSprings(Ljava/util/EnumMap;)V

    .line 338786
    return-void
.end method

.method public setSupportedReactions(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338787
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->r:LX/0Px;

    .line 338788
    return-void
.end method

.method public setTopDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 338789
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->b:LX/20J;

    .line 338790
    iput-object p1, v0, LX/20J;->d:LX/1Wl;

    .line 338791
    return-void
.end method
