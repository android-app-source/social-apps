.class public Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/20y;",
        "LX/21I;",
        "TE;",
        "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1EQ;

.field private final c:LX/20K;

.field private final d:LX/1zf;

.field private final e:LX/20e;

.field private final f:LX/20g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/20g",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 339417
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/20K;LX/1zf;LX/20e;LX/1EQ;LX/20g;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 339410
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 339411
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->c:LX/20K;

    .line 339412
    iput-object p2, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->d:LX/1zf;

    .line 339413
    iput-object p3, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->e:LX/20e;

    .line 339414
    iput-object p4, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->b:LX/1EQ;

    .line 339415
    iput-object p5, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->f:LX/20g;

    .line 339416
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;
    .locals 9

    .prologue
    .line 339399
    const-class v1, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    monitor-enter v1

    .line 339400
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 339401
    sput-object v2, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 339402
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339403
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 339404
    new-instance v3, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    invoke-static {v0}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v4

    check-cast v4, LX/20K;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v5

    check-cast v5, LX/1zf;

    invoke-static {v0}, LX/20e;->b(LX/0QB;)LX/20e;

    move-result-object v6

    check-cast v6, LX/20e;

    invoke-static {v0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v7

    check-cast v7, LX/1EQ;

    invoke-static {v0}, LX/20g;->a(LX/0QB;)LX/20g;

    move-result-object v8

    check-cast v8, LX/20g;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;-><init>(LX/20K;LX/1zf;LX/20e;LX/1EQ;LX/20g;)V

    .line 339405
    move-object v0, v3

    .line 339406
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 339407
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 339408
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 339409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/20y;LX/21I;Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20y;",
            "LX/21I;",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 339418
    iget-object v0, p1, LX/20y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 339419
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 339420
    move-object v9, v0

    check-cast v9, Lcom/facebook/graphql/model/GraphQLStory;

    .line 339421
    iget-object v0, p2, LX/21I;->g:Ljava/util/EnumMap;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v2, p2, LX/21I;->c:LX/20Z;

    invoke-static {p3, v0, v1, v2}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 339422
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v2, p2, LX/21I;->d:LX/21M;

    iget-object v3, p2, LX/21I;->e:LX/0wd;

    iget-object v4, p2, LX/21I;->f:LX/20z;

    iget-object v5, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->d:LX/1zf;

    iget-object v6, p2, LX/21I;->h:LX/20I;

    iget-object v7, p2, LX/21I;->i:LX/0Px;

    iget-object v8, p2, LX/21I;->j:LX/21H;

    move-object v0, p3

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 339423
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->e:LX/20e;

    invoke-virtual {v0, p3, v9}, LX/20e;->a(LX/1wK;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 339424
    iget-boolean v0, p2, LX/21I;->a:Z

    if-eqz v0, :cond_0

    .line 339425
    sget-object v0, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {p3, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p2, LX/21I;->b:LX/20b;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 339426
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 339397
    check-cast p2, LX/20y;

    check-cast p3, LX/1Po;

    .line 339398
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->f:LX/20g;

    iget-object v1, p2, LX/20y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v2, p2, LX/20y;->b:Z

    invoke-virtual {v0, v1, p3, v2}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Z)LX/21I;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x29a0171a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 339396
    check-cast p1, LX/20y;

    check-cast p2, LX/21I;

    check-cast p4, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->a(LX/20y;LX/21I;Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;)V

    const/16 v1, 0x1f

    const v2, 0x22928350

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 339393
    check-cast p4, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 339394
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->c:LX/20K;

    invoke-static {p4, v0}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 339395
    return-void
.end method
