.class public Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/21P;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/FooterBackgroundStyleResolver;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final e:LX/1dr;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/0Ot;Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;LX/1dr;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/FooterBackgroundStyleResolver;",
            ">;",
            "Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;",
            "LX/1dr;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 339563
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 339564
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    .line 339565
    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    .line 339566
    iput-object p3, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->b:LX/0Ot;

    .line 339567
    iput-object p4, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    .line 339568
    iput-object p5, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->e:LX/1dr;

    .line 339569
    iput-object p6, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->f:LX/0Ot;

    .line 339570
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;
    .locals 10

    .prologue
    .line 339571
    const-class v1, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    monitor-enter v1

    .line 339572
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 339573
    sput-object v2, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 339574
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339575
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 339576
    new-instance v3, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    const/16 v6, 0x856

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    invoke-static {v0}, LX/1dr;->a(LX/0QB;)LX/1dr;

    move-result-object v8

    check-cast v8, LX/1dr;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/0Ot;Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;LX/1dr;LX/0Ot;)V

    .line 339577
    move-object v0, v3

    .line 339578
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 339579
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 339580
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 339581
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 339582
    check-cast p2, LX/21P;

    .line 339583
    iget-object v0, p2, LX/21P;->d:LX/1Wi;

    .line 339584
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1We;

    invoke-virtual {v1, v0}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v1

    move-object v1, v1

    .line 339585
    if-nez v1, :cond_0

    .line 339586
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Footer Level = "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 339587
    iget-object v0, p2, LX/21P;->d:LX/1Wi;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 339588
    const-string v0, "\nDebug info: \n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339589
    iget-object v0, p2, LX/21P;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 339590
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 339591
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339592
    const-string v0, "Null_Footer_Background_Style"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    const/4 v2, 0x1

    .line 339593
    iput-boolean v2, v0, LX/0VK;->d:Z

    .line 339594
    move-object v0, v0

    .line 339595
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    .line 339596
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v2}, LX/03V;->a(LX/0VG;)V

    .line 339597
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/FooterBackgroundStylePartDefinition;

    iget-object v2, p2, LX/21P;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 339598
    iget-object v3, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->e:LX/1dr;

    invoke-virtual {v3, v2}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    .line 339599
    iget-object v4, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v4, v3}, LX/1Wj;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 339600
    iget-object v5, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v5, v3}, LX/1Wj;->b(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 339601
    iget-object v5, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->e:LX/1dr;

    invoke-virtual {v5, v2, v4}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/graphics/drawable/Drawable;)V

    .line 339602
    iget-object v5, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->e:LX/1dr;

    invoke-virtual {v5, v2, v3}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/graphics/drawable/Drawable;)V

    .line 339603
    new-instance v5, LX/21Q;

    invoke-direct {v5, v1, v4, v3}, LX/21Q;-><init>(LX/1Wj;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object v2, v5

    .line 339604
    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 339605
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    new-instance v2, LX/21R;

    iget-boolean v3, p2, LX/21P;->a:Z

    iget-boolean v4, p2, LX/21P;->b:Z

    iget-boolean v5, p2, LX/21P;->c:Z

    invoke-direct {v2, v3, v4, v5, v1}, LX/21R;-><init>(ZZZLX/1Wj;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 339606
    const/4 v0, 0x0

    return-object v0
.end method
