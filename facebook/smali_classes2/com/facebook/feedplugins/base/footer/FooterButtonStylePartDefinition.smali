.class public Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/21R;",
        "LX/21d;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/1DR;

.field private final b:Landroid/content/res/Resources;

.field private c:LX/1Wo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/21T;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1DR;Landroid/content/res/Resources;LX/1Wo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269616
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 269617
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a:LX/1DR;

    .line 269618
    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->b:Landroid/content/res/Resources;

    .line 269619
    iput-object p3, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->c:LX/1Wo;

    .line 269620
    return-void
.end method

.method public static a(LX/1Wj;LX/1DR;Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 269612
    invoke-virtual {p1}, LX/1DR;->a()I

    move-result v0

    .line 269613
    iget-object v1, p0, LX/1Wj;->c:LX/1Wh;

    iget v1, v1, LX/1Wh;->a:F

    .line 269614
    invoke-static {p2, v1}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v1

    .line 269615
    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;
    .locals 6

    .prologue
    .line 269600
    const-class v1, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    monitor-enter v1

    .line 269601
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269602
    sput-object v2, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269603
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269604
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269605
    new-instance p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v3

    check-cast v3, LX/1DR;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    const-class v5, LX/1Wo;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1Wo;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;-><init>(LX/1DR;Landroid/content/res/Resources;LX/1Wo;)V

    .line 269606
    move-object v0, p0

    .line 269607
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269608
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269609
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Wj;)I
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 269611
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a:LX/1DR;

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->b:Landroid/content/res/Resources;

    invoke-static {p1, v0, v1}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/1Wj;LX/1DR;Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method public final a()LX/21T;
    .locals 2

    .prologue
    .line 269596
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->c:LX/1Wo;

    if-eqz v0, :cond_0

    .line 269597
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->c:LX/1Wo;

    sget-object v1, LX/21S;->INLINE:LX/21S;

    invoke-virtual {v0, v1}, LX/1Wo;->a(LX/21S;)LX/21T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->d:LX/21T;

    .line 269598
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->c:LX/1Wo;

    .line 269599
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->d:LX/21T;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 269587
    check-cast p2, LX/21R;

    .line 269588
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a()LX/21T;

    move-result-object v0

    iget-boolean v1, p2, LX/21R;->a:Z

    iget-boolean v2, p2, LX/21R;->b:Z

    iget-boolean v3, p2, LX/21R;->c:Z

    invoke-virtual {v0, v1, v2, v3}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v0

    .line 269589
    iget-object v1, p2, LX/21R;->d:LX/1Wj;

    invoke-virtual {p0, v1}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/1Wj;)I

    move-result v1

    .line 269590
    invoke-virtual {v0, v1}, LX/21Y;->a(I)LX/21c;

    move-result-object v1

    .line 269591
    new-instance v2, LX/21d;

    invoke-static {v1}, LX/21c;->hasIcons(LX/21c;)Z

    move-result v3

    invoke-virtual {v0, v1}, LX/21Y;->a(LX/21c;)[F

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/21d;-><init>(Z[F)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3f029816

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 269592
    check-cast p2, LX/21d;

    .line 269593
    move-object v1, p4

    check-cast v1, LX/1wK;

    iget-object v2, p2, LX/21d;->b:[F

    invoke-interface {v1, v2}, LX/1wK;->setButtonWeights([F)V

    .line 269594
    check-cast p4, LX/1wK;

    iget-boolean v1, p2, LX/21d;->a:Z

    invoke-interface {p4, v1}, LX/1wK;->setShowIcons(Z)V

    .line 269595
    const/16 v1, 0x1f

    const v2, -0x2ae677e8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
