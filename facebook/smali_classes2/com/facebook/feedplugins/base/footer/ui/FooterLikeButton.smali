.class public Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;
.super Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
.source ""


# instance fields
.field public c:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1VI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Landroid/graphics/drawable/Drawable;

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:I

.field private final k:I

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:I

.field private o:Z

.field private p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 339300
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 339301
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 339298
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339299
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 339281
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 339282
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->p:I

    .line 339283
    const-class v0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 339284
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->d:LX/1VI;

    invoke-static {v0, v1}, LX/20W;->a(Landroid/content/Context;LX/1VI;)I

    move-result v1

    .line 339285
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0102a2

    const v3, -0xa76f01

    invoke-static {v0, v2, v3}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v2

    .line 339286
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 339287
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0102a6

    invoke-static {v0, v4}, LX/0WH;->b(Landroid/content/Context;I)LX/0am;

    move-result-object v0

    .line 339288
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->e:Landroid/graphics/drawable/Drawable;

    .line 339289
    const v0, 0x7f080fc8

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->g:Ljava/lang/String;

    .line 339290
    iput v1, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->h:I

    .line 339291
    const v0, 0x7f081994

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->i:Ljava/lang/String;

    .line 339292
    const v0, 0x7f0219c6

    invoke-direct {p0, v0, v2}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->f:Landroid/graphics/drawable/Drawable;

    .line 339293
    const v0, 0x7f0b009c

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->j:I

    .line 339294
    iget v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->j:I

    const v1, 0x7f0b0fd7

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->k:I

    .line 339295
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->f()V

    .line 339296
    return-void

    .line 339297
    :cond_0
    const v0, 0x7f0219c6

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 339280
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->c:LX/0wM;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;IILjava/lang/String;)V
    .locals 1

    .prologue
    .line 339263
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 339264
    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->l:Ljava/lang/String;

    .line 339265
    iget-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->m:Z

    if-nez v0, :cond_0

    .line 339266
    invoke-virtual {p0, p2}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setText(Ljava/lang/CharSequence;)V

    .line 339267
    :cond_0
    invoke-virtual {p0, p3}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setTextColor(I)V

    .line 339268
    iput p4, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->n:I

    .line 339269
    iget-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->o:Z

    if-nez v0, :cond_1

    .line 339270
    invoke-virtual {p0, p4}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setCompoundDrawablePadding(I)V

    .line 339271
    :cond_1
    invoke-virtual {p0, p5}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 339272
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;LX/0wM;LX/1VI;)V
    .locals 0

    .prologue
    .line 339279
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->c:LX/0wM;

    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->d:LX/1VI;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v1

    check-cast v1, LX/1VI;

    invoke-static {p0, v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;LX/0wM;LX/1VI;)V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 339276
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setIsLiked(Z)V

    .line 339277
    sget v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-virtual {p0, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setWarmupBackgroundResId(I)V

    .line 339278
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 339273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->m:Z

    .line 339274
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setText(Ljava/lang/CharSequence;)V

    .line 339275
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 339260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->m:Z

    .line 339261
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setText(Ljava/lang/CharSequence;)V

    .line 339262
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 339257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->o:Z

    .line 339258
    iget v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->n:I

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setCompoundDrawablePadding(I)V

    .line 339259
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 1

    .prologue
    .line 339254
    if-eqz p1, :cond_0

    sget-object v0, LX/1zt;->b:LX/1zt;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setReaction(LX/1zt;)V

    .line 339255
    return-void

    .line 339256
    :cond_0
    sget-object v0, LX/1zt;->a:LX/1zt;

    goto :goto_0
.end method

.method public setReaction(LX/1zt;)V
    .locals 6

    .prologue
    .line 339236
    iget v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->p:I

    .line 339237
    iget v1, p1, LX/1zt;->e:I

    move v1, v1

    .line 339238
    if-ne v0, v1, :cond_0

    .line 339239
    :goto_0
    return-void

    .line 339240
    :cond_0
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 339241
    if-eqz v0, :cond_1

    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p1, v0, :cond_2

    .line 339242
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->e:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->g:Ljava/lang/String;

    iget v3, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->h:I

    iget v4, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->j:I

    iget-object v5, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->i:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;IILjava/lang/String;)V

    .line 339243
    :goto_1
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 339244
    iput v0, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->p:I

    goto :goto_0

    .line 339245
    :cond_2
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 339246
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 339247
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->f:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0102a2

    const v4, -0xa76f01

    invoke-static {v0, v3, v4}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v3

    iget v4, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->j:I

    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v5, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->g:Ljava/lang/String;

    invoke-static {v0, v5}, LX/20W;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1

    .line 339248
    :cond_3
    invoke-virtual {p1}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 339249
    iget-object v0, p1, LX/1zt;->f:Ljava/lang/String;

    move-object v2, v0

    .line 339250
    iget v0, p1, LX/1zt;->g:I

    move v3, v0

    .line 339251
    iget v4, p0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->k:I

    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 339252
    iget-object v5, p1, LX/1zt;->f:Ljava/lang/String;

    move-object v5, v5

    .line 339253
    invoke-static {v0, v5}, LX/20W;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1
.end method
