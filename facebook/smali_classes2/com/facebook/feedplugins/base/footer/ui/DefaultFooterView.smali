.class public Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/20Q;
.implements LX/1wL;


# static fields
.field public static final a:LX/1Cz;

.field private static final h:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/20J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/13Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/20S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1VI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/20X;",
            "Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/view/ViewGroup;

.field private final k:Landroid/view/ViewStub;

.field private final l:LX/20Y;

.field private final m:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final n:I

.field private final o:I

.field private final p:[I

.field private final q:[I

.field private final r:[F

.field private final s:Landroid/graphics/drawable/Drawable;

.field private t:LX/21H;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:D

.field private x:Z

.field private y:Z

.field private z:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 339019
    new-instance v0, LX/20R;

    invoke-direct {v0}, LX/20R;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    .line 339020
    const-class v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 339021
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 339022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 339023
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 339024
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->w:D

    .line 339025
    iput-boolean v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->A:Z

    .line 339026
    iput-boolean v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->B:Z

    .line 339027
    iput-boolean v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->C:Z

    .line 339028
    const-class v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 339029
    const v0, 0x7f0303f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 339030
    invoke-virtual {p0, v2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->setOrientation(I)V

    .line 339031
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->g:LX/1VI;

    invoke-virtual {v0}, LX/1VI;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0102a4

    const v2, -0xe2ded7

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    .line 339032
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0102a5

    invoke-static {v0, v1}, LX/0WH;->b(Landroid/content/Context;I)LX/0am;

    move-result-object v1

    .line 339033
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0102a7

    invoke-static {v0, v2}, LX/0WH;->b(Landroid/content/Context;I)LX/0am;

    move-result-object v2

    .line 339034
    const v0, 0x7f0d0c3f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 339035
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 339036
    sget-object v4, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v3, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 339037
    sget-object v4, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :goto_1
    const v1, 0x7f080fcc

    const v5, 0x7f0d0c42

    sget v6, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-direct {p0, v0, v1, v5, v6}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 339038
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :goto_2
    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->s:Landroid/graphics/drawable/Drawable;

    .line 339039
    sget-object v0, LX/20X;->SHARE:LX/20X;

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->s:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f080fd9

    const v4, 0x7f0d0c43

    sget v5, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 339040
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    .line 339041
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->r:[F

    .line 339042
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getDefaultButtonWeights()[F

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->z:[F

    .line 339043
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getDefaultButtonWidths()[I

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->p:[I

    .line 339044
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getDefaultButtonDrawablePaddings()[I

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->q:[I

    .line 339045
    const v0, 0x7f0d0c40

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->k:Landroid/view/ViewStub;

    .line 339046
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->n:I

    .line 339047
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339048
    iget-object v1, v0, LX/20S;->k:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 339049
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget v2, LX/0fe;->aV:I

    :goto_3
    const/16 v3, 0x10

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/20S;->k:Ljava/lang/Integer;

    .line 339050
    :cond_0
    iget-object v1, v0, LX/20S;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 339051
    int-to-float v0, v0

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->o:I

    .line 339052
    const v0, 0x7f0d0395

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    .line 339053
    new-instance v0, LX/20Y;

    invoke-direct {v0}, LX/20Y;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->l:LX/20Y;

    .line 339054
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 339055
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20X;

    .line 339056
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 339057
    new-instance v3, LX/20a;

    iget-object v4, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->l:LX/20Y;

    iget-object v5, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->d:LX/13Q;

    invoke-direct {v3, v1, v4, v5}, LX/20a;-><init>(LX/20X;LX/20Z;LX/13Q;)V

    .line 339058
    invoke-virtual {v0, v3}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 339059
    sget-object v0, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v1}, LX/20X;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339060
    iget-object v1, v0, LX/20S;->l:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    .line 339061
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v4

    if-eqz v4, :cond_8

    sget-short v4, LX/0fe;->aW:S

    :goto_5
    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/20S;->l:Ljava/lang/Boolean;

    .line 339062
    :cond_2
    iget-object v1, v0, LX/20S;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 339063
    if-eqz v0, :cond_1

    .line 339064
    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(LX/20a;)V

    goto :goto_4

    .line 339065
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0102a3

    const v2, -0xb1a99b

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    goto/16 :goto_0

    .line 339066
    :cond_4
    const v0, 0x7f0219c3

    iget v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_1

    .line 339067
    :cond_5
    const v0, 0x7f0219c9

    iget v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_2

    .line 339068
    :cond_6
    sget-object v0, LX/1vY;->FEEDBACK_SECTION:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 339069
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget-object v1, LX/1vY;->LIKE_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 339070
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget-object v1, LX/1vY;->COMMENT_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 339071
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v1, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget-object v1, LX/1vY;->SHARE_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 339072
    return-void

    .line 339073
    :cond_7
    sget v2, LX/0wj;->e:I

    goto/16 :goto_3

    .line 339074
    :cond_8
    sget-short v4, LX/0wj;->f:S

    goto :goto_5

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private a(DI)F
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide/16 v2, 0x0

    .line 339075
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->z:[F

    aget v0, v0, p3

    float-to-double v6, v0

    move-wide v0, p1

    move-wide v8, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 339076
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339077
    iget-object v2, v1, LX/20S;->i:Ljava/lang/Float;

    if-nez v2, :cond_0

    .line 339078
    iget-object v2, v1, LX/20S;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v1}, LX/20S;->o(LX/20S;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget v3, LX/0fe;->bo:F

    :goto_0
    const v4, 0x3e19999a    # 0.15f

    invoke-interface {v2, v3, v4}, LX/0ad;->a(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, LX/20S;->i:Ljava/lang/Float;

    .line 339079
    :cond_0
    iget-object v2, v1, LX/20S;->i:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move v1, v2

    .line 339080
    add-float/2addr v1, v0

    iget-object v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->z:[F

    aget v2, v2, p3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 339081
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->z:[F

    aget v0, v0, p3

    .line 339082
    :cond_1
    :goto_1
    return v0

    .line 339083
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339084
    iget-object v2, v1, LX/20S;->j:Ljava/lang/Float;

    if-nez v2, :cond_3

    .line 339085
    iget-object v2, v1, LX/20S;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v1}, LX/20S;->o(LX/20S;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget v3, LX/0fe;->bp:F

    :goto_2
    const v4, 0x3d4ccccd    # 0.05f

    invoke-interface {v2, v3, v4}, LX/0ad;->a(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, LX/20S;->j:Ljava/lang/Float;

    .line 339086
    :cond_3
    iget-object v2, v1, LX/20S;->j:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move v1, v2

    .line 339087
    sub-float v1, v0, v1

    cmpg-float v1, v1, v10

    if-gez v1, :cond_1

    move v0, v10

    .line 339088
    goto :goto_1

    .line 339089
    :cond_4
    sget v3, LX/0wj;->x:F

    goto :goto_0

    .line 339090
    :cond_5
    sget v3, LX/0wj;->y:F

    goto :goto_2
.end method

.method private a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 339091
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b:LX/0wM;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
    .locals 2

    .prologue
    .line 339092
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 339093
    invoke-virtual {v0, p2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 339094
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSoundEffectsEnabled(Z)V

    .line 339095
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 339096
    invoke-virtual {v0, p4}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setWarmupBackgroundResId(I)V

    .line 339097
    iget v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTextColor(I)V

    .line 339098
    return-object v0
.end method

.method private a(DD)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 339099
    invoke-static {p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->d(D)Z

    move-result v2

    .line 339100
    invoke-static {p3, p4}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->d(D)Z

    move-result v0

    if-eq v2, v0, :cond_0

    const/4 v0, 0x1

    .line 339101
    :goto_0
    if-nez v0, :cond_1

    .line 339102
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 339103
    goto :goto_0

    .line 339104
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v3, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    .line 339105
    if-eqz v2, :cond_2

    .line 339106
    invoke-virtual {v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->d()V

    .line 339107
    invoke-virtual {v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->e()V

    goto :goto_1

    .line 339108
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->c()V

    .line 339109
    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setCompoundDrawablePadding(I)V

    goto :goto_1
.end method

.method private a(LX/20a;)V
    .locals 5
    .param p1    # LX/20a;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 339110
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->u:Landroid/view/View;

    if-nez v0, :cond_0

    .line 339111
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->k:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->u:Landroid/view/View;

    .line 339112
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->u:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 339113
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->u:Landroid/view/View;

    sget-object v1, LX/1vY;->COMMENT_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 339114
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->v:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_3

    .line 339115
    :cond_1
    :goto_1
    return-void

    .line 339116
    :cond_2
    new-instance p1, LX/20a;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    iget-object v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->l:LX/20Y;

    iget-object v3, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->d:LX/13Q;

    invoke-direct {p1, v1, v2, v3}, LX/20a;-><init>(LX/20X;LX/20Z;LX/13Q;)V

    goto :goto_0

    .line 339117
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->u:Landroid/view/View;

    const v1, 0x7f0d278e

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 339118
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339119
    iget-object v1, v0, LX/20S;->o:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    .line 339120
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-short v2, LX/0fe;->bl:S

    :goto_2
    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/20S;->o:Ljava/lang/Boolean;

    .line 339121
    :cond_4
    iget-object v1, v0, LX/20S;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 339122
    if-eqz v0, :cond_5

    .line 339123
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->v:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 339124
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339125
    iget-object v1, v0, LX/20S;->p:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 339126
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-short v2, LX/0fe;->bb:S

    :goto_3
    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/20S;->p:Ljava/lang/Boolean;

    .line 339127
    :cond_6
    iget-object v1, v0, LX/20S;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 339128
    if-eqz v0, :cond_1

    .line 339129
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->v:Lcom/facebook/resources/ui/FbTextView;

    iget v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 339130
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 339131
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339132
    iget-object v2, v1, LX/20S;->q:Ljava/lang/Integer;

    if-nez v2, :cond_7

    .line 339133
    iget-object v2, v1, LX/20S;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    iget-object v3, v1, LX/20S;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v1}, LX/20S;->o(LX/20S;)Z

    move-result v4

    if-eqz v4, :cond_a

    sget v4, LX/0fe;->aT:F

    :goto_4
    const/high16 p1, 0x3f400000    # 0.75f

    invoke-interface {v3, v4, p1}, LX/0ad;->a(FF)F

    move-result v3

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/20S;->q:Ljava/lang/Integer;

    .line 339134
    :cond_7
    iget-object v2, v1, LX/20S;->q:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v1, v2

    .line 339135
    iget v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    goto/16 :goto_1

    .line 339136
    :cond_8
    sget-short v2, LX/0wj;->u:S

    goto/16 :goto_2

    .line 339137
    :cond_9
    sget-short v2, LX/0wj;->l:S

    goto :goto_3

    .line 339138
    :cond_a
    sget v4, LX/0wj;->c:F

    goto :goto_4
.end method

.method private static a(Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;LX/0wM;LX/20J;LX/13Q;LX/20S;LX/0Ot;LX/1VI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            "LX/0wM;",
            "LX/20J;",
            "LX/13Q;",
            "LX/20S;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 339139
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b:LX/0wM;

    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->c:LX/20J;

    iput-object p3, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->d:LX/13Q;

    iput-object p4, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    iput-object p5, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->f:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->g:LX/1VI;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    invoke-static {v6}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {v6}, LX/20J;->b(LX/0QB;)LX/20J;

    move-result-object v2

    check-cast v2, LX/20J;

    invoke-static {v6}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v3

    check-cast v3, LX/13Q;

    invoke-static {v6}, LX/20S;->a(LX/0QB;)LX/20S;

    move-result-object v4

    check-cast v4, LX/20S;

    const/16 v5, 0x259

    invoke-static {v6, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v6}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v6

    check-cast v6, LX/1VI;

    invoke-static/range {v0 .. v6}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;LX/0wM;LX/20J;LX/13Q;LX/20S;LX/0Ot;LX/1VI;)V

    return-void
.end method

.method private a(ZD)V
    .locals 10

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 339140
    if-nez p1, :cond_0

    .line 339141
    :goto_0
    return-void

    .line 339142
    :cond_0
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/16 v6, 0x0

    move-wide v0, p2

    move-wide v8, v4

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 339143
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getCommentComposerView()Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v0}, LX/0vv;->c(Landroid/view/View;F)V

    goto :goto_0
.end method

.method private a(Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 339144
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->y:Z

    if-nez v0, :cond_1

    .line 339145
    :cond_0
    :goto_0
    return v1

    .line 339146
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getCommentComposerView()Landroid/view/View;

    move-result-object v3

    .line 339147
    iget-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->A:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    iget-boolean v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->C:Z

    if-eqz v2, :cond_3

    move v2, v1

    :goto_2
    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-static {v3, v0, v4, v2, v5}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 339148
    iput-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->y:Z

    .line 339149
    const/4 v1, 0x1

    goto :goto_0

    .line 339150
    :cond_2
    iget v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->o:I

    goto :goto_1

    :cond_3
    iget v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->o:I

    goto :goto_2
.end method

.method private a(ZZ)Z
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 339151
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->x:Z

    if-nez v0, :cond_0

    .line 339152
    :goto_0
    return v1

    .line 339153
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_6

    .line 339154
    :cond_1
    if-eqz p2, :cond_3

    move v0, v1

    .line 339155
    :goto_1
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getCommentComposerView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v0, v4, :cond_6

    .line 339156
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getCommentComposerView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    move v4, v3

    .line 339157
    :goto_2
    if-nez p2, :cond_2

    iget-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->B:Z

    if-nez v0, :cond_4

    .line 339158
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v5, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 339159
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eq v2, v5, :cond_5

    .line 339160
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    move v0, v3

    .line 339161
    :goto_4
    iput-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->x:Z

    move v1, v0

    .line 339162
    goto :goto_0

    :cond_3
    move v0, v2

    .line 339163
    goto :goto_1

    :cond_4
    move v2, v1

    .line 339164
    goto :goto_3

    :cond_5
    move v0, v4

    goto :goto_4

    :cond_6
    move v4, v1

    goto :goto_2
.end method

.method private a([F)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 339183
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v3

    move v1, v0

    move v2, v0

    .line 339184
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339185
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 339186
    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 339187
    if-eqz v0, :cond_1

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    aget v5, p1, v2

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    .line 339188
    aget v1, p1, v2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 339189
    const/4 v1, 0x1

    move v0, v1

    .line 339190
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    .line 339191
    goto :goto_0

    .line 339192
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private b(D)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 339165
    iget-wide v4, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->w:D

    .line 339166
    iput-wide p1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->w:D

    .line 339167
    invoke-static {p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->c(D)Z

    move-result v3

    .line 339168
    invoke-static {v4, v5}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->c(D)Z

    move-result v0

    if-eq v3, v0, :cond_3

    move v0, v1

    .line 339169
    :goto_0
    invoke-direct {p0, v0, v3}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(ZZ)Z

    move-result v6

    .line 339170
    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(Z)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz v6, :cond_1

    :cond_0
    move v2, v1

    .line 339171
    :cond_1
    invoke-static {v4, v5, p1, p2}, Ljava/lang/Double;->compare(DD)I

    move-result v1

    if-nez v1, :cond_4

    .line 339172
    if-eqz v2, :cond_2

    .line 339173
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->d()V

    .line 339174
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    .line 339175
    goto :goto_0

    .line 339176
    :cond_4
    if-eqz v0, :cond_5

    .line 339177
    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->setButtonWidthsAndPadding(Z)V

    .line 339178
    :cond_5
    invoke-direct {p0, p1, p2, v4, v5}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(DD)V

    .line 339179
    invoke-direct {p0, p1, p2, v4, v5}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b(DD)V

    .line 339180
    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->setButtonWeights(D)V

    .line 339181
    invoke-direct {p0, v3, p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(ZD)V

    .line 339182
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->d()V

    goto :goto_1
.end method

.method private b(DD)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 339220
    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->f(D)Z

    move-result v4

    .line 339221
    invoke-direct {p0, p3, p4}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->f(D)Z

    move-result v1

    if-eq v4, v1, :cond_0

    move v1, v0

    .line 339222
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e(D)Z

    move-result v5

    .line 339223
    invoke-direct {p0, p3, p4}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e(D)Z

    move-result v3

    if-eq v5, v3, :cond_1

    move v3, v0

    .line 339224
    :goto_1
    if-nez v1, :cond_2

    if-nez v3, :cond_2

    .line 339225
    :goto_2
    return-void

    :cond_0
    move v1, v2

    .line 339226
    goto :goto_0

    :cond_1
    move v3, v2

    .line 339227
    goto :goto_1

    .line 339228
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v6, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v0, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 339229
    if-eqz v1, :cond_3

    .line 339230
    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f080fd9

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(Ljava/lang/CharSequence;)V

    .line 339231
    :cond_3
    if-eqz v3, :cond_4

    .line 339232
    if-eqz v5, :cond_7

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->s:Landroid/graphics/drawable/Drawable;

    :goto_4
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 339233
    :cond_4
    if-eqz v4, :cond_5

    if-eqz v5, :cond_5

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->q:[I

    const/4 v2, 0x2

    aget v2, v1, v2

    :cond_5
    invoke-virtual {v0, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setCompoundDrawablePadding(I)V

    goto :goto_2

    .line 339234
    :cond_6
    const-string v1, ""

    goto :goto_3

    .line 339235
    :cond_7
    const/4 v1, 0x0

    goto :goto_4
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 339218
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->getCommentComposerTextView()Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 339219
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 339215
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    if-nez v0, :cond_0

    .line 339216
    :goto_0
    return-void

    .line 339217
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->h:Ljava/lang/String;

    const-string v2, "progressive UFI state not null when calling setButtons()"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static c(D)Z
    .locals 2

    .prologue
    .line 339214
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 339211
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 339212
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 339213
    return-void
.end method

.method private static d(D)Z
    .locals 2

    .prologue
    .line 339210
    const-wide v0, 0x3fe4cccccccccccdL    # 0.65

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 339209
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->u:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(D)Z
    .locals 3

    .prologue
    .line 339204
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339205
    iget-object v1, v0, LX/20S;->r:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 339206
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v2, LX/0fe;->be:S

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/20S;->r:Ljava/lang/Boolean;

    .line 339207
    :cond_0
    iget-object v1, v0, LX/20S;->r:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 339208
    if-nez v0, :cond_1

    const-wide v0, 0x3fe4cccccccccccdL    # 0.65

    cmpg-double v0, p1, v0

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 339202
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(LX/20a;)V

    .line 339203
    return-void
.end method

.method private f(D)Z
    .locals 3

    .prologue
    .line 339012
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->e:LX/20S;

    .line 339013
    iget-object v1, v0, LX/20S;->s:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 339014
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v2, LX/0fe;->bf:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/20S;->s:Ljava/lang/Boolean;

    .line 339015
    :cond_0
    iget-object v1, v0, LX/20S;->s:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 339016
    if-nez v0, :cond_1

    const-wide v0, 0x3fe4cccccccccccdL    # 0.65

    cmpg-double v0, p1, v0

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCommentComposerTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 339193
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->f()V

    .line 339194
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->v:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method private getCommentComposerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 339017
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->f()V

    .line 339018
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->u:Landroid/view/View;

    return-object v0
.end method

.method private getDefaultButtonDrawablePaddings()[I
    .locals 4

    .prologue
    .line 339195
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 339196
    const/4 v0, 0x0

    .line 339197
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 339198
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawablePadding()I

    move-result v0

    aput v0, v2, v1

    .line 339199
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 339200
    goto :goto_0

    .line 339201
    :cond_0
    return-object v2
.end method

.method private getDefaultButtonWeights()[F
    .locals 4

    .prologue
    .line 338943
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    new-array v2, v0, [F

    .line 338944
    const/4 v0, 0x0

    .line 338945
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 338946
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    aput v0, v2, v1

    .line 338947
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 338948
    goto :goto_0

    .line 338949
    :cond_0
    return-object v2
.end method

.method private getDefaultButtonWidths()[I
    .locals 4

    .prologue
    .line 338936
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 338937
    const/4 v0, 0x0

    .line 338938
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 338939
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    aput v0, v2, v1

    .line 338940
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 338941
    goto :goto_0

    .line 338942
    :cond_0
    return-object v2
.end method

.method private setButtonWeights(D)V
    .locals 3

    .prologue
    .line 338931
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->z:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->r:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 338932
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->r:[F

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(DI)F

    move-result v2

    aput v2, v1, v0

    .line 338933
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 338934
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->r:[F

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a([F)Z

    .line 338935
    return-void
.end method

.method private setButtonWidthsAndPadding(Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 338922
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->o:I

    move v1, v0

    .line 338923
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 338924
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    if-eqz p1, :cond_1

    const/4 v4, -0x2

    :goto_2
    iput v4, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 338925
    invoke-static {v0, v1, v3, v1, v3}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 338926
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 338927
    goto :goto_1

    .line 338928
    :cond_0
    iget v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->n:I

    move v1, v0

    goto :goto_0

    .line 338929
    :cond_1
    iget-object v4, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->p:[I

    aget v4, v4, v2

    goto :goto_2

    .line 338930
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(LX/20X;)Landroid/view/View;
    .locals 1

    .prologue
    .line 338921
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 338918
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 338919
    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a()V

    goto :goto_0

    .line 338920
    :cond_0
    return-void
.end method

.method public final a(D)V
    .locals 1

    .prologue
    .line 338916
    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b(D)V

    .line 338917
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 338914
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b(Ljava/lang/String;)V

    .line 338915
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 338910
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    if-eqz v0, :cond_0

    .line 338911
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    invoke-virtual {v0, v1}, LX/21H;->a(LX/1wL;)V

    .line 338912
    iput-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    .line 338913
    :cond_0
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 338907
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 338908
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->c:LX/20J;

    invoke-virtual {v0, p0, p1}, LX/20J;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 338909
    return-void
.end method

.method public getReactionsDockAnchor()Landroid/view/View;
    .locals 0

    .prologue
    .line 338906
    return-object p0
.end method

.method public setBottomDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 338903
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->c:LX/20J;

    .line 338904
    iput-object p1, v0, LX/20J;->e:LX/1Wl;

    .line 338905
    return-void
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 338901
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 338902
    return-void
.end method

.method public setButtonContainerHeight(I)V
    .locals 2

    .prologue
    .line 338897
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 338898
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 338899
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 338900
    return-void
.end method

.method public setButtonWeights([F)V
    .locals 4

    .prologue
    .line 338950
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->z:[F

    .line 338951
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    invoke-virtual {v0}, LX/21H;->a()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a([F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338952
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 338953
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 338954
    :cond_1
    return-void
.end method

.method public setButtons(Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 338955
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->c()V

    .line 338956
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 338957
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 338958
    sget-object v1, LX/1wM;->a:[I

    invoke-virtual {v0}, LX/20X;->ordinal()I

    move-result v6

    aget v1, v1, v6

    packed-switch v1, :pswitch_data_0

    .line 338959
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    if-eqz v5, :cond_4

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto :goto_0

    .line 338960
    :pswitch_0
    iget-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->y:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->A:Z

    if-eq v1, v5, :cond_1

    :cond_0
    move v1, v3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->y:Z

    .line 338961
    iput-boolean v5, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->A:Z

    goto :goto_1

    :cond_1
    move v1, v2

    .line 338962
    goto :goto_3

    .line 338963
    :pswitch_1
    iput-boolean v5, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->B:Z

    goto :goto_1

    .line 338964
    :pswitch_2
    iget-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->y:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->C:Z

    if-eq v1, v5, :cond_3

    :cond_2
    move v1, v3

    :goto_4
    iput-boolean v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->y:Z

    .line 338965
    iput-boolean v5, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->C:Z

    goto :goto_1

    :cond_3
    move v1, v2

    .line 338966
    goto :goto_4

    .line 338967
    :cond_4
    const/16 v1, 0x8

    goto :goto_2

    .line 338968
    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 2

    .prologue
    .line 338969
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 338970
    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setDownstateType(LX/1Wk;)V

    goto :goto_0

    .line 338971
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 338972
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 338973
    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setEnabled(Z)V

    goto :goto_0

    .line 338974
    :cond_0
    return-void
.end method

.method public setFooterAlpha(F)V
    .locals 0

    .prologue
    .line 338975
    invoke-static {p0, p1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 338976
    return-void
.end method

.method public setFooterVisibility(I)V
    .locals 0

    .prologue
    .line 338977
    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->setVisibility(I)V

    .line 338978
    return-void
.end method

.method public setHasCachedComments(Z)V
    .locals 3

    .prologue
    .line 338979
    if-eqz p1, :cond_0

    .line 338980
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v1, 0x7f0219c4

    iget v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    invoke-direct {p0, v1, v2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 338981
    :goto_0
    return-void

    .line 338982
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v1, 0x7f0219c3

    iget v2, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->m:I

    invoke-direct {p0, v1, v2}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setIsLiked(Z)V
    .locals 2

    .prologue
    .line 338983
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setIsLiked(Z)V

    .line 338984
    return-void
.end method

.method public setOnButtonClickedListener(LX/20Z;)V
    .locals 1

    .prologue
    .line 338985
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->l:LX/20Y;

    .line 338986
    iput-object p1, v0, LX/20Y;->a:LX/20Z;

    .line 338987
    return-void
.end method

.method public setProgressiveUfiState(LX/21H;)V
    .locals 2

    .prologue
    .line 338988
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    .line 338989
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->x:Z

    .line 338990
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/21H;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->B:Z

    if-nez v0, :cond_2

    .line 338991
    :cond_0
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b(D)V

    .line 338992
    :cond_1
    :goto_0
    return-void

    .line 338993
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    invoke-virtual {v0, p0}, LX/21H;->a(LX/1wL;)V

    .line 338994
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    invoke-virtual {v0}, LX/21H;->a()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b(D)V

    .line 338995
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->t:LX/21H;

    .line 338996
    iget-boolean v1, v0, LX/21H;->k:Z

    move v0, v1

    .line 338997
    if-nez v0, :cond_1

    .line 338998
    iget-object v0, p1, LX/21H;->j:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 338999
    sget-object v0, LX/1zt;->c:LX/1zt;

    invoke-static {p1, v0}, LX/21H;->c(LX/21H;LX/1zt;)V

    .line 339000
    :cond_3
    iget-object v0, p1, LX/21H;->j:Ljava/lang/String;

    move-object v0, v0

    .line 339001
    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setShowIcons(Z)V
    .locals 2

    .prologue
    .line 339002
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    .line 339003
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339004
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Z)V

    goto :goto_0

    .line 339005
    :cond_0
    return-void
.end method

.method public setSprings(Ljava/util/EnumMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 339006
    invoke-virtual {p1}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 339007
    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->i:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {p1, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    goto :goto_0

    .line 339008
    :cond_0
    return-void
.end method

.method public setTopDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 339009
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->c:LX/20J;

    .line 339010
    iput-object p1, v0, LX/20J;->d:LX/1Wl;

    .line 339011
    return-void
.end method
