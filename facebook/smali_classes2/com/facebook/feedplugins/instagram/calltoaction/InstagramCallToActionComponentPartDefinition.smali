.class public Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/1Vk;

.field private final f:LX/1Vl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/1Vk;LX/1Vl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266346
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 266347
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->d:LX/1V0;

    .line 266348
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->e:LX/1Vk;

    .line 266349
    iput-object p4, p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->f:LX/1Vl;

    .line 266350
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266329
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->e:LX/1Vk;

    const/4 v1, 0x0

    .line 266330
    new-instance v2, LX/C8X;

    invoke-direct {v2, v0}, LX/C8X;-><init>(LX/1Vk;)V

    .line 266331
    iget-object v3, v0, LX/1Vk;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C8W;

    .line 266332
    if-nez v3, :cond_0

    .line 266333
    new-instance v3, LX/C8W;

    invoke-direct {v3, v0}, LX/C8W;-><init>(LX/1Vk;)V

    .line 266334
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C8W;->a$redex0(LX/C8W;LX/1De;IILX/C8X;)V

    .line 266335
    move-object v2, v3

    .line 266336
    move-object v1, v2

    .line 266337
    move-object v0, v1

    .line 266338
    iget-object v1, v0, LX/C8W;->a:LX/C8X;

    iput-object p2, v1, LX/C8X;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266339
    iget-object v1, v0, LX/C8W;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266340
    move-object v0, v0

    .line 266341
    iget-object v1, v0, LX/C8W;->a:LX/C8X;

    iput-object p3, v1, LX/C8X;->b:LX/1Pp;

    .line 266342
    iget-object v1, v0, LX/C8W;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266343
    move-object v0, v0

    .line 266344
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 266345
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->d:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;
    .locals 7

    .prologue
    .line 266318
    const-class v1, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 266319
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266320
    sput-object v2, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266321
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266322
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266323
    new-instance p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/1Vk;->a(LX/0QB;)LX/1Vk;

    move-result-object v5

    check-cast v5, LX/1Vk;

    invoke-static {v0}, LX/1Vl;->a(LX/0QB;)LX/1Vl;

    move-result-object v6

    check-cast v6, LX/1Vl;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/1Vk;LX/1Vl;)V

    .line 266324
    move-object v0, p0

    .line 266325
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266326
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266327
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266328
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 266317
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 266316
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266312
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->f:LX/1Vl;

    .line 266313
    invoke-static {p1}, LX/1Vl;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266314
    iget-object v1, v0, LX/1Vl;->f:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short p0, LX/C8V;->a:S

    invoke-interface {v1, v2, p0}, LX/0ad;->a(LX/0c0;S)V

    .line 266315
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 266306
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266307
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/calltoaction/InstagramCallToActionComponentPartDefinition;->f:LX/1Vl;

    const/4 v1, 0x0

    .line 266308
    invoke-static {p1}, LX/1Vl;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/1Vl;->f:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short p0, LX/C8V;->a:S

    invoke-interface {v2, v3, v4, p0, v1}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 266309
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 266310
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266311
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
