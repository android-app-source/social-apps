.class public Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1Vy;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Vy;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267032
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 267033
    iput-object p2, p0, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->d:LX/1Vy;

    .line 267034
    iput-object p3, p0, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->e:LX/1V0;

    .line 267035
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 267013
    iget-object v0, p0, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->d:LX/1Vy;

    const/4 v1, 0x0

    .line 267014
    new-instance v2, LX/CCu;

    invoke-direct {v2, v0}, LX/CCu;-><init>(LX/1Vy;)V

    .line 267015
    iget-object v3, v0, LX/1Vy;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CCt;

    .line 267016
    if-nez v3, :cond_0

    .line 267017
    new-instance v3, LX/CCt;

    invoke-direct {v3, v0}, LX/CCt;-><init>(LX/1Vy;)V

    .line 267018
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/CCt;->a$redex0(LX/CCt;LX/1De;IILX/CCu;)V

    .line 267019
    move-object v2, v3

    .line 267020
    move-object v1, v2

    .line 267021
    move-object v0, v1

    .line 267022
    iget-object v1, v0, LX/CCt;->a:LX/CCu;

    iput-object p2, v1, LX/CCu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267023
    iget-object v1, v0, LX/CCt;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 267024
    move-object v1, v0

    .line 267025
    move-object v0, p3

    check-cast v0, LX/1Pk;

    .line 267026
    iget-object v2, v1, LX/CCt;->a:LX/CCu;

    iput-object v0, v2, LX/CCu;->b:LX/1Pk;

    .line 267027
    iget-object v2, v1, LX/CCt;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 267028
    move-object v0, v1

    .line 267029
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 267030
    iget-object v2, p0, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 267031
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 266998
    const-class v1, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;

    monitor-enter v1

    .line 266999
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267000
    sput-object v2, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267001
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267002
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267003
    new-instance p0, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Vy;->a(LX/0QB;)LX/1Vy;

    move-result-object v4

    check-cast v4, LX/1Vy;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1Vy;LX/1V0;)V

    .line 267004
    move-object v0, p0

    .line 267005
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267006
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267007
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267008
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 267012
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 267011
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/topics/rows/TopicStoryHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 267009
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267010
    invoke-static {p1}, LX/1VF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
