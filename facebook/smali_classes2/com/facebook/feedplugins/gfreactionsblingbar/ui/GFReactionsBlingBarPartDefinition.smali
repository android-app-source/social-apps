.class public Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Integer;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1WU;


# direct methods
.method public constructor <init>(LX/1WU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268648
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 268649
    iput-object p1, p0, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;->a:LX/1WU;

    .line 268650
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;
    .locals 4

    .prologue
    .line 268637
    const-class v1, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;

    monitor-enter v1

    .line 268638
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268639
    sput-object v2, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268640
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268641
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268642
    new-instance p0, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;

    invoke-static {v0}, LX/1WU;->a(LX/0QB;)LX/1WU;

    move-result-object v3

    check-cast v3, LX/1WU;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;-><init>(LX/1WU;)V

    .line 268643
    move-object v0, p0

    .line 268644
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268645
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268646
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 268605
    sget-object v0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->a:LX/1Cz;

    move-object v0, v0

    .line 268606
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x36ff6bad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 268613
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;

    .line 268614
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 268615
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268616
    iget-object v2, p0, Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;->a:LX/1WU;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 268617
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    .line 268618
    iget-object p2, v2, LX/1WU;->b:LX/1WV;

    new-instance p3, LX/9As;

    invoke-direct {p3, v2, p4}, LX/9As;-><init>(LX/1WU;Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;)V

    .line 268619
    new-instance p0, LX/88G;

    invoke-direct {p0}, LX/88G;-><init>()V

    move-object p0, p0

    .line 268620
    const-string p1, "0"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 268621
    const-string p1, "1"

    invoke-virtual {p0, p1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 268622
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->d:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    const/4 p1, 0x1

    .line 268623
    iput-boolean p1, p0, LX/0zO;->p:Z

    .line 268624
    move-object p0, p0

    .line 268625
    iget-object p1, p2, LX/1WV;->a:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    move-object p0, p0

    .line 268626
    new-instance p1, LX/87r;

    invoke-direct {p1, p2, p3}, LX/87r;-><init>(LX/1WV;LX/9As;)V

    iget-object v2, p2, LX/1WV;->b:Ljava/util/concurrent/Executor;

    invoke-static {p0, p1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 268627
    if-eqz v4, :cond_0

    .line 268628
    invoke-static {v1}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    .line 268629
    iput v4, p4, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->c:I

    .line 268630
    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    const/4 p0, 0x1

    .line 268631
    if-lez v4, :cond_2

    .line 268632
    if-le v4, p0, :cond_1

    invoke-virtual {p4}, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0f008c

    invoke-virtual {p2, p3, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p2

    .line 268633
    :goto_0
    iget-object p3, p4, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->f:Lcom/facebook/resources/ui/FbTextView;

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p1, " "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 268634
    :cond_0
    :goto_1
    const/16 v1, 0x1f

    const v2, 0x4cd8ebec    # 1.13729376E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 268635
    :cond_1
    invoke-virtual {p4}, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0f008c

    invoke-virtual {p2, p3, p0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 268636
    :cond_2
    iget-object p2, p4, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 268607
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268608
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268609
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268610
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 268611
    if-eqz v1, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v1, p0

    .line 268612
    if-eqz v1, :cond_0

    invoke-static {v0}, LX/0sa;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
