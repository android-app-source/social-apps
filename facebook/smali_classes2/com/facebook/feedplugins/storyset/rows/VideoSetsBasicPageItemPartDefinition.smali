.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHe;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DHe;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

.field private final d:LX/DGp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254862
    new-instance v0, LX/1Tx;

    invoke-direct {v0}, LX/1Tx;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;LX/DGp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;",
            ">;",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;",
            "LX/DGp;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254863
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254864
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->b:LX/0Ot;

    .line 254865
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    .line 254866
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->d:LX/DGp;

    .line 254867
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254868
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 254869
    check-cast p2, LX/DGm;

    .line 254870
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254871
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 254872
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 254873
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 254874
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->d:LX/DGp;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v1, v0, v2}, LX/DGp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;

    move-result-object v1

    .line 254875
    const v2, 0x7f0d2de4

    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v2, v0, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254876
    const v0, 0x7f0d2de5

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBasicPageItemPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    new-instance v3, LX/DGg;

    iget-object v4, v1, LX/DGo;->a:Ljava/lang/CharSequence;

    iget-object v1, v1, LX/DGo;->b:Ljava/lang/CharSequence;

    invoke-direct {v3, v4, v1}, LX/DGg;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254877
    const/4 v0, 0x0

    return-object v0
.end method
