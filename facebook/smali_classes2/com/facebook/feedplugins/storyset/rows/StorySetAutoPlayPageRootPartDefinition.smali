.class public Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1PV;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHp;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Tp;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 254666
    new-instance v0, LX/1Tp;

    invoke-direct {v0}, LX/1Tp;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->a:LX/1Tp;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254667
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254668
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    .line 254669
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    .line 254670
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 254671
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;
    .locals 6

    .prologue
    .line 254672
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;

    monitor-enter v1

    .line 254673
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254674
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254675
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254676
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254677
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;)V

    .line 254678
    move-object v0, p0

    .line 254679
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254680
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254681
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254683
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->a:LX/1Tp;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 254684
    check-cast p2, LX/DGm;

    .line 254685
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254686
    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 254687
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254688
    const v1, 0x7f0d0539

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254689
    const v1, 0x7f0d0539

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v3, LX/3EE;

    .line 254690
    iget v4, p2, LX/DGm;->c:I

    move v4, v4

    .line 254691
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    new-instance v6, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v3, v0, v4, v5, v6}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254692
    const/4 v0, 0x0

    return-object v0
.end method
