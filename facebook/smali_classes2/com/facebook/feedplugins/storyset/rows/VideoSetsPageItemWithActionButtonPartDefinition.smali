.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHf;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DHf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;

.field private final d:LX/DGp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254774
    new-instance v0, LX/1Tt;

    invoke-direct {v0}, LX/1Tt;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;LX/DGp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254775
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254776
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    .line 254777
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;

    .line 254778
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->d:LX/DGp;

    .line 254779
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254773
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 254764
    check-cast p2, LX/DGm;

    .line 254765
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254766
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 254767
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 254768
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 254769
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->d:LX/DGp;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v1, v0, v2}, LX/DGp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;

    move-result-object v1

    .line 254770
    const v2, 0x7f0d2de4

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    invoke-interface {p1, v2, v3, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254771
    const v2, 0x7f0d2de3

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithActionButtonPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;

    new-instance v4, LX/DGi;

    iget-object v5, v1, LX/DGo;->a:Ljava/lang/CharSequence;

    iget-object v1, v1, LX/DGo;->b:Ljava/lang/CharSequence;

    invoke-direct {v4, v5, v1, v0}, LX/DGi;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254772
    const/4 v0, 0x0

    return-object v0
.end method
