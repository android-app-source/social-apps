.class public Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/DHm;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DHm;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254741
    new-instance v0, LX/1Ts;

    invoke-direct {v0}, LX/1Ts;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254758
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254759
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;

    .line 254760
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    .line 254761
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;
    .locals 5

    .prologue
    .line 254747
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;

    monitor-enter v1

    .line 254748
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254749
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254750
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254751
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254752
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;)V

    .line 254753
    move-object v0, p0

    .line 254754
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254755
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254756
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254746
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 254742
    check-cast p2, LX/DGm;

    const/4 v1, 0x0

    .line 254743
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254744
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254745
    return-object v1
.end method
