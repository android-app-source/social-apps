.class public Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/DG3;",
        "TE;",
        "Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Kf;

.field public final d:Landroid/content/Context;

.field public final e:LX/5up;

.field public final f:LX/03V;

.field public final g:LX/0kL;

.field public final h:LX/2yK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 255037
    new-instance v0, LX/1U0;

    invoke-direct {v0}, LX/1U0;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/1Kf;Landroid/content/Context;LX/5up;LX/03V;LX/0kL;LX/2yK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/1Kf;",
            "Landroid/content/Context;",
            "LX/5up;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0kL;",
            "LX/2yK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255028
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 255029
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->b:LX/0Or;

    .line 255030
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->c:LX/1Kf;

    .line 255031
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->d:Landroid/content/Context;

    .line 255032
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->e:LX/5up;

    .line 255033
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->f:LX/03V;

    .line 255034
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->g:LX/0kL;

    .line 255035
    iput-object p7, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->h:LX/2yK;

    .line 255036
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;
    .locals 11

    .prologue
    .line 255017
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    monitor-enter v1

    .line 255018
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 255019
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 255020
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255021
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 255022
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    const/16 v4, 0x13a4

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v7

    check-cast v7, LX/5up;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static {v0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v10

    check-cast v10, LX/2yK;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;-><init>(LX/0Or;LX/1Kf;Landroid/content/Context;LX/5up;LX/03V;LX/0kL;LX/2yK;)V

    .line 255023
    move-object v0, v3

    .line 255024
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 255025
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255026
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 255027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 254995
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 254996
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 254997
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 254998
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    move-object v2, p3

    check-cast v2, LX/1Po;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-static {p3, v0, v1, v2}, LX/212;->a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStorySet;LX/1PT;)Ljava/util/EnumMap;

    move-result-object v1

    .line 254999
    invoke-virtual {v1}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255000
    sget-object v2, LX/20X;->SHARE:LX/20X;

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255001
    sget-object v2, LX/20X;->SAVE:LX/20X;

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255002
    :cond_0
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 255003
    new-instance v3, LX/Aoy;

    invoke-direct {v3, v2}, LX/Aoy;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 255004
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 255005
    check-cast v0, LX/0jW;

    invoke-interface {p3, v3, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aox;

    .line 255006
    iget-object v3, v0, LX/Aox;->a:Ljava/lang/Boolean;

    move-object v3, v3

    .line 255007
    if-nez v3, :cond_3

    .line 255008
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 255009
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    const v4, -0x3625f733

    invoke-static {v3, v4}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 255010
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 255011
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 255012
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    .line 255013
    :cond_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v3, v4, :cond_2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v3, v4, :cond_4

    :cond_2
    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v0, v3}, LX/Aox;->a(Z)V

    .line 255014
    :cond_3
    new-instance v4, LX/DG0;

    move-object v5, p0

    move-object v6, v2

    move-object v7, p3

    move-object v8, p2

    move-object v9, v0

    invoke-direct/range {v4 .. v9}, LX/DG0;-><init>(Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;LX/Aox;)V

    move-object v2, v4

    .line 255015
    new-instance v3, LX/DG3;

    invoke-direct {v3, v2, v1, v0}, LX/DG3;-><init>(LX/20Z;Ljava/util/EnumMap;LX/Aox;)V

    return-object v3

    .line 255016
    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5dc9e4be

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 254985
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/DG3;

    check-cast p4, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;

    .line 254986
    iget-object v2, p2, LX/DG3;->b:Ljava/util/EnumMap;

    .line 254987
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 254988
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object p0, p2, LX/DG3;->a:LX/20Z;

    invoke-static {p4, v2, v1, p0}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 254989
    iget-object v1, p2, LX/DG3;->c:LX/Aox;

    .line 254990
    iget-object v2, v1, LX/Aox;->a:Ljava/lang/Boolean;

    move-object v1, v2

    .line 254991
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254992
    invoke-virtual {p4}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->b()V

    .line 254993
    :goto_0
    const/16 v1, 0x1f

    const v2, 0xd7e065f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 254994
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 254982
    check-cast p4, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;

    .line 254983
    invoke-virtual {p4}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a()V

    .line 254984
    return-void
.end method
