.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHg;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DHg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

.field private final d:Lcom/facebook/feedplugins/storyset/rows/VideoSetsFooterSelectorPartDefinition;

.field private final e:LX/DGp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254811
    new-instance v0, LX/1Tv;

    invoke-direct {v0}, LX/1Tv;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;Lcom/facebook/feedplugins/storyset/rows/VideoSetsFooterSelectorPartDefinition;LX/DGp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254812
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254813
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    .line 254814
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    .line 254815
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/VideoSetsFooterSelectorPartDefinition;

    .line 254816
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->e:LX/DGp;

    .line 254817
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254818
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 254819
    check-cast p2, LX/DGm;

    .line 254820
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254821
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 254822
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 254823
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 254824
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->e:LX/DGp;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v1, v0, v2}, LX/DGp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;

    move-result-object v0

    .line 254825
    const v1, 0x7f0d2de4

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254826
    const v1, 0x7f0d2de5

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    new-instance v3, LX/DGg;

    iget-object v4, v0, LX/DGo;->a:Ljava/lang/CharSequence;

    iget-object v0, v0, LX/DGo;->b:Ljava/lang/CharSequence;

    invoke-direct {v3, v4, v0}, LX/DGg;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254827
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithFooterPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/VideoSetsFooterSelectorPartDefinition;

    .line 254828
    iget-object v1, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 254829
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254830
    const/4 v0, 0x0

    return-object v0
.end method
