.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHi;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DHi;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;

.field private final d:LX/DGp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254782
    new-instance v0, LX/1Tu;

    invoke-direct {v0}, LX/1Tu;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;LX/DGp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254783
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254784
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    .line 254785
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;

    .line 254786
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->d:LX/DGp;

    .line 254787
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254788
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 254789
    check-cast p2, LX/DGm;

    .line 254790
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254791
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 254792
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 254793
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 254794
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->d:LX/DGp;

    .line 254795
    if-eqz v0, :cond_0

    .line 254796
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 254797
    if-nez v3, :cond_1

    .line 254798
    :cond_0
    new-instance v3, LX/DGo;

    invoke-direct {v3}, LX/DGo;-><init>()V

    .line 254799
    :goto_0
    move-object v0, v3

    .line 254800
    const v1, 0x7f0d2de4

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254801
    const v1, 0x7f0d2de5

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSocialContextPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;

    new-instance v3, LX/DGj;

    iget-object v4, v0, LX/DGo;->a:Ljava/lang/CharSequence;

    iget-object v5, v0, LX/DGo;->b:Ljava/lang/CharSequence;

    iget-object v0, v0, LX/DGo;->c:Ljava/lang/CharSequence;

    invoke-direct {v3, v4, v5, v0}, LX/DGj;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254802
    const/4 v0, 0x0

    return-object v0

    .line 254803
    :cond_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v2, v1, v3}, LX/DGp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;

    move-result-object v3

    .line 254804
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 254805
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 254806
    :goto_1
    move-object v4, v4

    .line 254807
    iput-object v4, v3, LX/DGo;->c:Ljava/lang/CharSequence;

    .line 254808
    goto :goto_0

    :cond_2
    const-string v4, ""

    goto :goto_1
.end method
