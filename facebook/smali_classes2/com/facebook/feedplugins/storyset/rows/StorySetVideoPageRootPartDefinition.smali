.class public Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1PV;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHq;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Tq;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 254695
    new-instance v0, LX/1Tq;

    invoke-direct {v0}, LX/1Tq;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->a:LX/1Tq;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254706
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254707
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    .line 254708
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    .line 254709
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    .line 254710
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;
    .locals 6

    .prologue
    .line 254711
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;

    monitor-enter v1

    .line 254712
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254713
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254714
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254715
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254716
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;)V

    .line 254717
    move-object v0, p0

    .line 254718
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254719
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254720
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254721
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254705
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->a:LX/1Tq;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 254696
    check-cast p2, LX/DGm;

    .line 254697
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254698
    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 254699
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254700
    const v2, 0x7f0d2de4

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    new-instance v4, LX/3Ds;

    .line 254701
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 254702
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v4, v0}, LX/3Ds;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254703
    const v0, 0x7f0d2de4

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageVideoAttachmentPartDefinition;

    invoke-interface {p1, v0, v2, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254704
    const/4 v0, 0x0

    return-object v0
.end method
