.class public Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/BtM;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/BtM;",
            ">;"
        }
    .end annotation
.end field

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254724
    new-instance v0, LX/1Tr;

    invoke-direct {v0}, LX/1Tr;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254725
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254726
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;
    .locals 3

    .prologue
    .line 254727
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;

    monitor-enter v1

    .line 254728
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254729
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 254732
    new-instance v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;-><init>()V

    .line 254733
    move-object v0, v0

    .line 254734
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254735
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254736
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/BtM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254738
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;->a:LX/1Cz;

    return-object v0
.end method
