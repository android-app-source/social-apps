.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHo;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1To;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 254627
    new-instance v0, LX/1To;

    invoke-direct {v0}, LX/1To;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->a:LX/1To;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254628
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254629
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    .line 254630
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    .line 254631
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 254632
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;
    .locals 6

    .prologue
    .line 254633
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;

    monitor-enter v1

    .line 254634
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254635
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254636
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254637
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254638
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;)V

    .line 254639
    move-object v0, p0

    .line 254640
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254641
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254642
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254643
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254644
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->a:LX/1To;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 254645
    check-cast p2, LX/DGm;

    .line 254646
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254647
    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 254648
    new-instance v0, LX/DGm;

    .line 254649
    iget-object v2, p2, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 254650
    iget v3, p2, LX/DGm;->c:I

    move v3, v3

    .line 254651
    iget v4, p2, LX/DGm;->d:I

    move v4, v4

    .line 254652
    const/4 v5, 0x1

    invoke-direct {v0, v2, v3, v4, v5}, LX/DGm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    .line 254653
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254654
    const v2, 0x7f0d2de6

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    new-instance v4, LX/3Ds;

    .line 254655
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 254656
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v4, v0}, LX/3Ds;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254657
    const v0, 0x7f0d2de6

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v3, LX/2ya;

    invoke-direct {v3, v1}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254658
    const/4 v0, 0x0

    return-object v0
.end method
