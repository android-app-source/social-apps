.class public Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHj;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DHj;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final d:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

.field private final e:LX/DGp;

.field private final f:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254968
    new-instance v0, LX/1Tz;

    invoke-direct {v0}, LX/1Tz;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;LX/DGp;Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254945
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254946
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    .line 254947
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 254948
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    .line 254949
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->e:LX/DGp;

    .line 254950
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->f:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    .line 254951
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;
    .locals 9

    .prologue
    .line 254969
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;

    monitor-enter v1

    .line 254970
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254971
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254972
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254973
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254974
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    invoke-static {v0}, LX/DGp;->a(LX/0QB;)LX/DGp;

    move-result-object v7

    check-cast v7, LX/DGp;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;LX/DGp;Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;)V

    .line 254975
    move-object v0, v3

    .line 254976
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254977
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254978
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254967
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 254952
    check-cast p2, LX/DGm;

    .line 254953
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254954
    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 254955
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->e:LX/DGp;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v1, v0, v2}, LX/DGp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;

    move-result-object v1

    .line 254956
    new-instance v2, LX/2ya;

    invoke-direct {v2, v0}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 254957
    const v3, 0x7f0d2de6

    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    new-instance v5, LX/3Ds;

    .line 254958
    iget-object v6, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v6

    .line 254959
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v6, v1, LX/DGo;->a:Ljava/lang/CharSequence;

    invoke-direct {v5, v0, v6}, LX/3Ds;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254960
    const v0, 0x7f0d2de6

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-interface {p1, v0, v3, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254961
    const v0, 0x7f0d2de5

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    new-instance v4, LX/DGg;

    iget-object v5, v1, LX/DGo;->a:Ljava/lang/CharSequence;

    iget-object v1, v1, LX/DGo;->b:Ljava/lang/CharSequence;

    invoke-direct {v4, v5, v1}, LX/DGg;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254962
    const v0, 0x7f0d2de5

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254963
    const v0, 0x7f0d2de7

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->f:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    .line 254964
    iget-object v2, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 254965
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254966
    const/4 v0, 0x0

    return-object v0
.end method
