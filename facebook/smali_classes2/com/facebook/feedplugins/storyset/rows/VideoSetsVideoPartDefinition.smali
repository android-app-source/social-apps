.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/DGm;",
        "Ljava/lang/Integer;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1fv;

.field public final g:LX/04J;

.field public final h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254941
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->a:Ljava/lang/String;

    .line 254942
    new-instance v0, LX/1Ty;

    invoke-direct {v0}, LX/1Ty;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;LX/1fv;LX/04J;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254880
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 254881
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->c:Landroid/content/Context;

    .line 254882
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 254883
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    .line 254884
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->f:LX/1fv;

    .line 254885
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->g:LX/04J;

    .line 254886
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->h:LX/03V;

    .line 254887
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;
    .locals 10

    .prologue
    .line 254930
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    monitor-enter v1

    .line 254931
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254932
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254933
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254934
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254935
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    invoke-static {v0}, LX/1fv;->a(LX/0QB;)LX/1fv;

    move-result-object v7

    check-cast v7, LX/1fv;

    invoke-static {v0}, LX/04J;->a(LX/0QB;)LX/04J;

    move-result-object v8

    check-cast v8, LX/04J;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;LX/1fv;LX/04J;LX/03V;)V

    .line 254936
    move-object v0, v3

    .line 254937
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254938
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254939
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254929
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 254893
    check-cast p2, LX/DGm;

    .line 254894
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254895
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 254896
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 254897
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 254898
    if-eqz v0, :cond_0

    .line 254899
    iget-object v2, p2, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 254900
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 254901
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 254902
    :cond_0
    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->h:LX/03V;

    sget-object v4, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v2, "Video is null"

    :goto_0
    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 254903
    :goto_1
    new-instance v4, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 254904
    iget-object v0, p2, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254905
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 254906
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 254907
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v0

    .line 254908
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_EVENT_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 254909
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 254910
    :goto_2
    move-object v3, v0

    .line 254911
    iget-object v6, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v0, LX/3EE;

    .line 254912
    iget-object v2, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 254913
    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 254914
    iget v2, p2, LX/DGm;->c:I

    move v2, v2

    .line 254915
    sget-object v5, LX/04D;->VIDEO_SETS:LX/04D;

    invoke-direct/range {v0 .. v5}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;LX/04D;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254916
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    new-instance v1, LX/DGq;

    const/4 v2, 0x3

    invoke-direct {v1, p2, v4, v2}, LX/DGq;-><init>(LX/DGm;Ljava/util/concurrent/atomic/AtomicReference;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 254917
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDf;

    iget v0, v0, LX/CDf;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    .line 254918
    :cond_2
    const-string v2, "StorySet cache_id is null"

    goto :goto_0

    .line 254919
    :cond_3
    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->g:LX/04J;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v4

    sget-object v2, LX/04I;->STORY_SET_ID:LX/04I;

    iget-object v5, v2, LX/04I;->value:Ljava/lang/String;

    .line 254920
    iget-object v2, p2, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 254921
    iget-object v6, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v6

    .line 254922
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v5, v2}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 254923
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->g:LX/04J;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/04I;->STORY_SET_VIDEO_POSITION:LX/04I;

    iget-object v4, v4, LX/04I;->value:Ljava/lang/String;

    .line 254924
    iget v5, p2, LX/DGm;->c:I

    move v5, v5

    .line 254925
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 254926
    :cond_4
    iget v0, p2, LX/DGm;->d:I

    move v0, v0

    .line 254927
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;->c:Landroid/content/Context;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    sub-int/2addr v0, v2

    .line 254928
    new-instance v2, LX/CDf;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v0, v3}, LX/CDf;-><init>(III)V

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x307de901

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 254889
    check-cast p2, Ljava/lang/Integer;

    check-cast p4, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 254890
    if-eqz p2, :cond_0

    invoke-virtual {p4}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 254891
    invoke-virtual {p4}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 254892
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x36966f67

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 254888
    const/4 v0, 0x1

    return v0
.end method
