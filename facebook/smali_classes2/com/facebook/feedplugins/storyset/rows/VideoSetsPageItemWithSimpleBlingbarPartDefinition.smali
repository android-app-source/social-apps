.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHh;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DHh;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;

.field private final d:LX/DGp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254854
    new-instance v0, LX/1Tw;

    invoke-direct {v0}, LX/1Tw;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;LX/DGp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/VideoSetsVideoPartDefinition;",
            ">;",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;",
            "LX/DGp;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 254855
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 254856
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->b:LX/0Ot;

    .line 254857
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;

    .line 254858
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->d:LX/DGp;

    .line 254859
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;
    .locals 6

    .prologue
    .line 254843
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;

    monitor-enter v1

    .line 254844
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 254845
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 254846
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254847
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 254848
    new-instance v5, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;

    const/16 v3, 0xa3e

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;

    invoke-static {v0}, LX/DGp;->a(LX/0QB;)LX/DGp;

    move-result-object v4

    check-cast v4, LX/DGp;

    invoke-direct {v5, p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;-><init>(LX/0Ot;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;LX/DGp;)V

    .line 254849
    move-object v0, v5

    .line 254850
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 254851
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254852
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 254853
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DHh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254842
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 254833
    check-cast p2, LX/DGm;

    .line 254834
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 254835
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 254836
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 254837
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 254838
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->d:LX/DGp;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v2, v1, v3}, LX/DGp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;

    move-result-object v2

    .line 254839
    const v3, 0x7f0d2de4

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    invoke-interface {p1, v3, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254840
    const v1, 0x7f0d2de5

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;

    new-instance v4, LX/DGh;

    iget-object v5, v2, LX/DGo;->a:Ljava/lang/CharSequence;

    iget-object v2, v2, LX/DGo;->b:Ljava/lang/CharSequence;

    invoke-direct {v4, v5, v2, v0}, LX/DGh;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 254841
    const/4 v0, 0x0

    return-object v0
.end method
