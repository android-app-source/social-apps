.class public Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CCp;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1Vw;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/1Vw;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/CCp;",
            ">;",
            "LX/1Vw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 266831
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 266832
    iput-object p3, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->e:LX/1Vw;

    .line 266833
    iput-object p2, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->d:LX/0Ot;

    .line 266834
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266862
    iget-object v0, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CCp;

    const/4 v1, 0x0

    .line 266863
    new-instance v2, LX/CCo;

    invoke-direct {v2, v0}, LX/CCo;-><init>(LX/CCp;)V

    .line 266864
    sget-object v3, LX/CCp;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CCn;

    .line 266865
    if-nez v3, :cond_0

    .line 266866
    new-instance v3, LX/CCn;

    invoke-direct {v3}, LX/CCn;-><init>()V

    .line 266867
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/CCn;->a$redex0(LX/CCn;LX/1De;IILX/CCo;)V

    .line 266868
    move-object v2, v3

    .line 266869
    move-object v1, v2

    .line 266870
    move-object v0, v1

    .line 266871
    iget-object v1, v0, LX/CCn;->a:LX/CCo;

    iput-object p3, v1, LX/CCo;->b:LX/1Pm;

    .line 266872
    iget-object v1, v0, LX/CCn;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266873
    move-object v0, v0

    .line 266874
    iget-object v1, v0, LX/CCn;->a:LX/CCo;

    iput-object p2, v1, LX/CCo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266875
    iget-object v1, v0, LX/CCn;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 266876
    move-object v0, v0

    .line 266877
    iget-object v1, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->e:LX/1Vw;

    .line 266878
    iget-object v2, v1, LX/1Vw;->c:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    .line 266879
    iget-object v2, v1, LX/1Vw;->a:LX/0ad;

    sget-short v3, LX/CCm;->c:S

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, LX/1Vw;->c:Ljava/lang/Boolean;

    .line 266880
    :cond_1
    iget-object v2, v1, LX/1Vw;->c:Ljava/lang/Boolean;

    move-object v1, v2

    .line 266881
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 266882
    iget-object v2, v0, LX/CCn;->a:LX/CCo;

    iput-boolean v1, v2, LX/CCo;->c:Z

    .line 266883
    iget-object v2, v0, LX/CCn;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 266884
    move-object v0, v0

    .line 266885
    iget-object v1, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->e:LX/1Vw;

    .line 266886
    iget-object v2, v1, LX/1Vw;->g:Ljava/lang/Integer;

    if-nez v2, :cond_3

    .line 266887
    invoke-static {v1}, LX/1Vw;->f(LX/1Vw;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    :cond_2
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 266888
    const v2, 0x7f0e0a53

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1Vw;->g:Ljava/lang/Integer;

    .line 266889
    :cond_3
    :goto_1
    iget-object v2, v1, LX/1Vw;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v1, v2

    .line 266890
    iget-object v2, v0, LX/CCn;->a:LX/CCo;

    iput v1, v2, LX/CCo;->e:I

    .line 266891
    iget-object v2, v0, LX/CCn;->d:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 266892
    move-object v0, v0

    .line 266893
    iget-object v1, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->e:LX/1Vw;

    .line 266894
    iget-object v2, v1, LX/1Vw;->d:Ljava/lang/Integer;

    if-nez v2, :cond_5

    .line 266895
    invoke-static {v1}, LX/1Vw;->f(LX/1Vw;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result p1

    sparse-switch p1, :sswitch_data_1

    :cond_4
    :goto_2
    packed-switch v2, :pswitch_data_1

    .line 266896
    const v2, 0x7f0a07d8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1Vw;->d:Ljava/lang/Integer;

    .line 266897
    :cond_5
    :goto_3
    iget-object v2, v1, LX/1Vw;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v1, v2

    .line 266898
    iget-object v2, v0, LX/CCn;->a:LX/CCo;

    invoke-virtual {v0, v1}, LX/1Dp;->d(I)I

    move-result v3

    iput v3, v2, LX/CCo;->d:I

    .line 266899
    iget-object v2, v0, LX/CCn;->d:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 266900
    move-object v0, v0

    .line 266901
    iget-object v1, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->e:LX/1Vw;

    .line 266902
    iget-object v2, v1, LX/1Vw;->f:Ljava/lang/Integer;

    if-nez v2, :cond_7

    .line 266903
    invoke-static {v1}, LX/1Vw;->f(LX/1Vw;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_2

    :cond_6
    :goto_4
    packed-switch v2, :pswitch_data_2

    .line 266904
    const v2, 0x7f02079d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1Vw;->f:Ljava/lang/Integer;

    .line 266905
    :cond_7
    :goto_5
    iget-object v2, v1, LX/1Vw;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v1, v2

    .line 266906
    iget-object v2, v0, LX/CCn;->a:LX/CCo;

    invoke-virtual {v0, v1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v3

    iput-object v3, v2, LX/CCo;->f:LX/1dc;

    .line 266907
    iget-object v2, v0, LX/CCn;->d:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 266908
    move-object v0, v0

    .line 266909
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 266910
    :sswitch_0
    const-string p1, "BLUE"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x0

    goto/16 :goto_0

    :sswitch_1
    const-string p1, "GREY"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    goto/16 :goto_0

    :sswitch_2
    const-string p1, "DEFAULT"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x2

    goto/16 :goto_0

    .line 266911
    :pswitch_0
    const v2, 0x7f0e0a52

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1Vw;->g:Ljava/lang/Integer;

    goto/16 :goto_1

    .line 266912
    :sswitch_3
    const-string p1, "BLUE"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v2, 0x0

    goto/16 :goto_2

    :sswitch_4
    const-string p1, "GREY"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v2, 0x1

    goto/16 :goto_2

    :sswitch_5
    const-string p1, "DEFAULT"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v2, 0x2

    goto/16 :goto_2

    .line 266913
    :pswitch_1
    const v2, 0x7f0a07d7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1Vw;->d:Ljava/lang/Integer;

    goto/16 :goto_3

    .line 266914
    :sswitch_6
    const-string p0, "BLUE"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x0

    goto/16 :goto_4

    :sswitch_7
    const-string p0, "GREY"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x1

    goto/16 :goto_4

    :sswitch_8
    const-string p0, "DEFAULT"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x2

    goto/16 :goto_4

    .line 266915
    :pswitch_2
    const v2, 0x7f02079a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1Vw;->f:Ljava/lang/Integer;

    goto/16 :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        -0x79209ddf -> :sswitch_2
        0x1f285a -> :sswitch_0
        0x2182df -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x79209ddf -> :sswitch_5
        0x1f285a -> :sswitch_3
        0x2182df -> :sswitch_4
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        -0x79209ddf -> :sswitch_8
        0x1f285a -> :sswitch_6
        0x2182df -> :sswitch_7
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;
    .locals 6

    .prologue
    .line 266851
    const-class v1, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    monitor-enter v1

    .line 266852
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266853
    sput-object v2, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266854
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266855
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266856
    new-instance v5, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x21d9

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1Vw;->a(LX/0QB;)LX/1Vw;

    move-result-object v4

    check-cast v4, LX/1Vw;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/1Vw;)V

    .line 266857
    move-object v0, v5

    .line 266858
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266859
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266860
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 266850
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 266849
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 266835
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 266836
    if-eqz p1, :cond_0

    .line 266837
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266838
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 266839
    :goto_0
    return v0

    .line 266840
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266841
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const v2, -0x74569f26

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 266842
    if-eqz v0, :cond_3

    .line 266843
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v2

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_TEXT_FORMATTING:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    if-eq v2, p1, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 266844
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->e:LX/1Vw;

    .line 266845
    iget-object v2, v0, LX/1Vw;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 266846
    iget-object v2, v0, LX/1Vw;->a:LX/0ad;

    sget-short p0, LX/CCm;->b:S

    const/4 p1, 0x0

    invoke-interface {v2, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, LX/1Vw;->b:Ljava/lang/Boolean;

    .line 266847
    :cond_2
    iget-object v2, v0, LX/1Vw;->b:Ljava/lang/Boolean;

    move-object v0, v2

    .line 266848
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method
