.class public Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1Wd;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Wd;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269134
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 269135
    iput-object p2, p0, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->d:LX/1Wd;

    .line 269136
    iput-object p3, p0, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->e:LX/1V0;

    .line 269137
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 269138
    iget-object v0, p0, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->d:LX/1Wd;

    const/4 v1, 0x0

    .line 269139
    new-instance v2, LX/C7Y;

    invoke-direct {v2, v0}, LX/C7Y;-><init>(LX/1Wd;)V

    .line 269140
    sget-object v3, LX/1Wd;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C7X;

    .line 269141
    if-nez v3, :cond_0

    .line 269142
    new-instance v3, LX/C7X;

    invoke-direct {v3}, LX/C7X;-><init>()V

    .line 269143
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C7X;->a$redex0(LX/C7X;LX/1De;IILX/C7Y;)V

    .line 269144
    move-object v2, v3

    .line 269145
    move-object v1, v2

    .line 269146
    move-object v0, v1

    .line 269147
    iget-object v1, v0, LX/C7X;->a:LX/C7Y;

    iput-object p2, v1, LX/C7Y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 269148
    iget-object v1, v0, LX/C7X;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 269149
    move-object v0, v0

    .line 269150
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 269151
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 269152
    iget-object v2, p0, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->e:LX/1V0;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;
    .locals 6

    .prologue
    .line 269153
    const-class v1, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;

    monitor-enter v1

    .line 269154
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269155
    sput-object v2, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269156
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269157
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269158
    new-instance p0, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Wd;->a(LX/0QB;)LX/1Wd;

    move-result-object v4

    check-cast v4, LX/1Wd;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/1Wd;LX/1V0;)V

    .line 269159
    move-object v0, p0

    .line 269160
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269161
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269162
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269163
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 269164
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 269165
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 269166
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 269167
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 269168
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const v1, 0x490bb763

    invoke-static {v0, v1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
