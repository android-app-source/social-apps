.class public Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1Vm;

.field private final e:LX/03V;

.field private final f:LX/1V0;

.field private final g:LX/1Vt;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Vm;LX/03V;LX/1V0;LX/1Vt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266684
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 266685
    iput-object p2, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->d:LX/1Vm;

    .line 266686
    iput-object p3, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->e:LX/03V;

    .line 266687
    iput-object p4, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->f:LX/1V0;

    .line 266688
    iput-object p5, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->g:LX/1Vt;

    .line 266689
    return-void
.end method

.method private static a(LX/1De;)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266683
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 266653
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266654
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266655
    new-instance v1, LX/CAS;

    invoke-direct {v1, v0}, LX/CAS;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CAT;

    .line 266656
    iget-boolean v2, v1, LX/CAT;->a:Z

    move v2, v2

    .line 266657
    if-nez v2, :cond_0

    .line 266658
    invoke-static {p1}, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    .line 266659
    :goto_0
    return-object v0

    .line 266660
    :cond_0
    const v2, 0x1172f9ae

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 266661
    if-nez v2, :cond_1

    .line 266662
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->e:LX/03V;

    const-string v1, "platform_cta_get_component_null_action_link"

    const-string v2, "actionLink was null in getComponent for Platform CTA"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 266663
    invoke-static {p1}, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 266664
    :cond_1
    iget-object v3, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->g:LX/1Vt;

    .line 266665
    const v4, 0x1172f9ae

    invoke-static {v0, v4}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 266666
    new-instance v5, LX/CAQ;

    invoke-direct {v5, v3, v4}, LX/CAQ;-><init>(LX/1Vt;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    move-object v0, v5

    .line 266667
    iget-object v3, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->d:LX/1Vm;

    invoke-virtual {v3, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v0

    .line 266668
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 266669
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bo()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/C2N;->a(Landroid/net/Uri;)LX/C2N;

    .line 266670
    :cond_2
    iget-object v3, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->g:LX/1Vt;

    .line 266671
    iget-boolean v4, v1, LX/CAT;->b:Z

    move v4, v4

    .line 266672
    if-nez v4, :cond_3

    .line 266673
    const/4 v4, 0x1

    .line 266674
    iput-boolean v4, v1, LX/CAT;->b:Z

    .line 266675
    const/4 v4, 0x0

    .line 266676
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 266677
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 266678
    :goto_1
    iget-object v4, v3, LX/1Vt;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CAP;

    .line 266679
    iget-object v6, v4, LX/CAP;->a:LX/0Zb;

    const-string v7, "platform_cta_impression"

    invoke-static {v7}, LX/CAP;->c(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v1, "app_id"

    invoke-virtual {v7, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 266680
    iget-object v4, v3, LX/1Vt;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CAR;

    invoke-virtual {v4}, LX/CAR;->a()V

    .line 266681
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->f:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 266682
    :cond_4
    iget-object v5, v3, LX/1Vt;->a:LX/03V;

    const-string v6, "platform_cta_impression_null_application"

    const-string v7, "application was null in OnClickListener for Platform CTA"

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;
    .locals 9

    .prologue
    .line 266628
    const-class v1, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 266629
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266630
    sput-object v2, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266631
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266632
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266633
    new-instance v3, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v5

    check-cast v5, LX/1Vm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1Vt;->a(LX/0QB;)LX/1Vt;

    move-result-object v8

    check-cast v8, LX/1Vt;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/1Vm;LX/03V;LX/1V0;LX/1Vt;)V

    .line 266634
    move-object v0, v3

    .line 266635
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266636
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266637
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 266652
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 266651
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 266640
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266641
    iget-object v1, p0, Lcom/facebook/feedplugins/platform/calltoaction/PlatformCallToActionComponentPartDefinition;->g:LX/1Vt;

    .line 266642
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266643
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 p0, 0x0

    .line 266644
    invoke-static {v0}, LX/1Vt;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    .line 266645
    if-nez v2, :cond_0

    move v2, p0

    .line 266646
    :goto_0
    move v0, v2

    .line 266647
    return v0

    .line 266648
    :cond_0
    iget-object v2, v1, LX/1Vt;->f:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    .line 266649
    iget-object v2, v1, LX/1Vt;->e:LX/0ad;

    sget-short p1, LX/CAO;->a:S

    invoke-interface {v2, p1, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, LX/1Vt;->f:Ljava/lang/Boolean;

    .line 266650
    :cond_1
    iget-object v2, v1, LX/1Vt;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/1Vt;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CAR;

    invoke-virtual {v2}, LX/CAR;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    move v2, p0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 266639
    const/4 v0, 0x0

    return-object v0
.end method
