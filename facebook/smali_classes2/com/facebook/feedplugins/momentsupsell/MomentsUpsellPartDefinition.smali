.class public Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/Aoc;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Vg;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ww;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 266083
    new-instance v0, LX/1Vf;

    invoke-direct {v0}, LX/1Vf;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/1Vg;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/1Vg;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ww;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266100
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 266101
    iput-object p1, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->b:LX/0Ot;

    .line 266102
    iput-object p2, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->c:LX/1Vg;

    .line 266103
    iput-object p3, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->d:LX/0Ot;

    .line 266104
    iput-object p4, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->e:LX/0Ot;

    .line 266105
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;
    .locals 7

    .prologue
    .line 266089
    const-class v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    monitor-enter v1

    .line 266090
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266091
    sput-object v2, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266092
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266093
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266094
    new-instance v4, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    const/16 v3, 0xe0f

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v3, LX/1Vg;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1Vg;

    const/16 v6, 0x978

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x97a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v5, v3, v6, p0}, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;-><init>(LX/0Ot;LX/1Vg;LX/0Ot;LX/0Ot;)V

    .line 266095
    move-object v0, v4

    .line 266096
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266097
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266098
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266099
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 266088
    sget-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 266086
    const v1, 0x7f0d1c38

    iget-object v0, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/Aof;

    invoke-direct {v2, p0}, LX/Aof;-><init>(Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;)V

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 266087
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 266084
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266085
    iget-object v0, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
