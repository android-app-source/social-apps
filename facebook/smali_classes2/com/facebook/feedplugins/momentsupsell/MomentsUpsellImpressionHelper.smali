.class public Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile p:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23T;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;

.field public final f:LX/1Wv;

.field public final g:LX/1Ww;

.field private final h:Landroid/content/Context;

.field public final i:LX/0SG;

.field private final j:LX/1Vr;

.field public final k:I

.field public l:LX/9g2;

.field public m:LX/Aod;

.field public n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 270132
    const-class v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->a:Ljava/lang/String;

    .line 270133
    const-class v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    const-string v1, "moments_upsell"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Or;LX/0ad;LX/1Wv;LX/1Ww;Landroid/content/Context;LX/0SG;LX/1Vg;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/23T;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ad;",
            "LX/1Wv;",
            "LX/1Ww;",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/1Vg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 270134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270135
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->n:Ljava/util/Set;

    .line 270136
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->o:Ljava/util/Set;

    .line 270137
    iput-object p1, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->c:LX/0Ot;

    .line 270138
    iput-object p2, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->d:LX/0Or;

    .line 270139
    iput-object p3, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->e:LX/0ad;

    .line 270140
    iput-object p4, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->f:LX/1Wv;

    .line 270141
    iput-object p5, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->g:LX/1Ww;

    .line 270142
    iput-object p6, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->h:Landroid/content/Context;

    .line 270143
    iput-object p7, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->i:LX/0SG;

    .line 270144
    new-instance v0, LX/1Wx;

    invoke-direct {v0}, LX/1Wx;-><init>()V

    invoke-virtual {p8, v0}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->j:LX/1Vr;

    .line 270145
    iget-object v0, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->e:LX/0ad;

    sget v1, LX/1Wy;->b:I

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->k:I

    .line 270146
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;
    .locals 12

    .prologue
    .line 270147
    sget-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->p:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    if-nez v0, :cond_1

    .line 270148
    const-class v1, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    monitor-enter v1

    .line 270149
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->p:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 270150
    if-eqz v2, :cond_0

    .line 270151
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 270152
    new-instance v3, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    const/16 v4, 0xf2d

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/1Wv;->b(LX/0QB;)LX/1Wv;

    move-result-object v7

    check-cast v7, LX/1Wv;

    invoke-static {v0}, LX/1Ww;->a(LX/0QB;)LX/1Ww;

    move-result-object v8

    check-cast v8, LX/1Ww;

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    const-class v11, LX/1Vg;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/1Vg;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;-><init>(LX/0Ot;LX/0Or;LX/0ad;LX/1Wv;LX/1Ww;Landroid/content/Context;LX/0SG;LX/1Vg;)V

    .line 270153
    move-object v0, v3

    .line 270154
    sput-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->p:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270155
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 270156
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 270157
    :cond_1
    sget-object v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->p:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    return-object v0

    .line 270158
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 270159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 270160
    iget-object v1, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->e:LX/0ad;

    sget-short v2, LX/1Wy;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->j:LX/1Vr;

    invoke-virtual {v1}, LX/1Vr;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270161
    :cond_0
    :goto_0
    return v0

    .line 270162
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->n:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 270163
    :cond_2
    if-eqz v0, :cond_0

    .line 270164
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    .line 270165
    :cond_3
    :goto_1
    goto :goto_0

    .line 270166
    :cond_4
    iget-object v3, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->o:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 270167
    iget-object v3, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->f:LX/1Wv;

    .line 270168
    invoke-virtual {v3}, LX/1Wv;->d()I

    move-result v5

    .line 270169
    iget-object v6, v3, LX/1Wv;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, LX/1Wv;->b:LX/0Tn;

    add-int/lit8 v5, v5, 0x1

    invoke-interface {v6, v7, v5}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 270170
    iget-object v5, v3, LX/1Wv;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v6, LX/1Wv;->c:LX/0Tn;

    iget-object v7, v3, LX/1Wv;->e:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-interface {v5, v6, v7, v8}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 270171
    iget-object v3, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->o:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 270172
    iget-object v3, p0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->g:LX/1Ww;

    .line 270173
    iget-object v4, v3, LX/1Ww;->b:LX/1Wv;

    invoke-virtual {v4}, LX/1Wv;->d()I

    move-result v4

    .line 270174
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "moments_in_feed_upsell_impression"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 270175
    const-string v6, "total_impressions"

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 270176
    iget-object v4, v3, LX/1Ww;->a:LX/0Zb;

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 270177
    goto :goto_1
.end method
