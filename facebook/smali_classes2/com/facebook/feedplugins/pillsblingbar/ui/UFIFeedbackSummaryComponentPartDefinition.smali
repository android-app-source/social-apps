.class public Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/1WX;

.field private final f:LX/1V0;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1VI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 268778
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1WX;LX/1V0;LX/0Ot;LX/1VI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1WX;",
            "LX/1V0;",
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268779
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 268780
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->e:LX/1WX;

    .line 268781
    iput-object p3, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->f:LX/1V0;

    .line 268782
    iput-object p4, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->g:LX/0Ot;

    .line 268783
    iput-object p5, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->h:LX/1VI;

    .line 268784
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 268785
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268786
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268787
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->e:LX/1WX;

    invoke-virtual {v1, p1}, LX/1WX;->c(LX/1De;)LX/1XJ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1XJ;->a(Z)LX/1XJ;

    move-result-object v0

    .line 268788
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->h:LX/1VI;

    invoke-virtual {v1}, LX/1VI;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268789
    const v1, 0x7f0a00aa

    invoke-virtual {v0, v1}, LX/1XJ;->h(I)LX/1XJ;

    .line 268790
    :cond_0
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 268791
    iget-object v2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->f:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    new-instance v3, LX/1X6;

    sget-object v4, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v3, p2, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v2, p1, v0, v3, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v1

    .line 268792
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1XK;

    invoke-virtual {v0, p1}, LX/1XK;->c(LX/1De;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1XM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XM;

    move-result-object v0

    check-cast p3, LX/1Po;

    invoke-virtual {v0, p3}, LX/1XM;->a(LX/1Po;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1XM;->a(Z)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1XM;->a(LX/1X1;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;
    .locals 9

    .prologue
    .line 268793
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;

    monitor-enter v1

    .line 268794
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268795
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268796
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268797
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268798
    new-instance v3, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1WX;->a(LX/0QB;)LX/1WX;

    move-result-object v5

    check-cast v5, LX/1WX;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    const/16 v7, 0x853

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v8

    check-cast v8, LX/1VI;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;-><init>(Landroid/content/Context;LX/1WX;LX/1V0;LX/0Ot;LX/1VI;)V

    .line 268799
    move-object v0, v3

    .line 268800
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268801
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268802
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 268804
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 268805
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 268806
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268807
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268808
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268809
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/1WY;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 268810
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268811
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 268812
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
