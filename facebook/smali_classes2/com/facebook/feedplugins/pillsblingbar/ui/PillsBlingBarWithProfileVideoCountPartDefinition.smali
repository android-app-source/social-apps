.class public Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/String;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268702
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 268703
    iput-object p1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->a:LX/0Ot;

    .line 268704
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->b:LX/0Ot;

    .line 268705
    iput-object p3, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->c:LX/0ad;

    .line 268706
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;
    .locals 6

    .prologue
    .line 268691
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;

    monitor-enter v1

    .line 268692
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268693
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268694
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268695
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268696
    new-instance v4, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;

    const/16 v3, 0x850

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x756

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v5, p0, v3}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 268697
    move-object v0, v4

    .line 268698
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268699
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268700
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268701
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 268707
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    move-object v0, v0

    .line 268708
    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 268686
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268687
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268688
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268689
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268690
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x46a7c0e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 268678
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/String;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 268679
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 268680
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 268681
    const/4 v2, 0x0

    invoke-virtual {p4, v2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setReactorsCount(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 268682
    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    invoke-virtual {p4, v2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setCommentsCount(I)V

    .line 268683
    invoke-static {v1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSharesCount(I)V

    .line 268684
    invoke-virtual {p4, p2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setProfileVideoViewsCount(Ljava/lang/String;)V

    .line 268685
    const/16 v1, 0x1f

    const v2, 0x4827260e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 268675
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 268676
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268677
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;->c:LX/0ad;

    sget-short v2, LX/9Io;->a:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
