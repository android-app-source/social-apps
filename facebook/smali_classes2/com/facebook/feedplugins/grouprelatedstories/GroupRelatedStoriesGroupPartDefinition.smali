.class public Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266057
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 266058
    iput-object p4, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->d:LX/0Uh;

    .line 266059
    iput-object p1, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->b:LX/0Ot;

    .line 266060
    iput-object p2, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->a:LX/0Ot;

    .line 266061
    iput-object p3, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->c:LX/0Ot;

    .line 266062
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;
    .locals 7

    .prologue
    .line 266063
    const-class v1, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;

    monitor-enter v1

    .line 266064
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266065
    sput-object v2, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266066
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266067
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266068
    new-instance v4, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;

    const/16 v3, 0x94a

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x1fa8

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x857

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v5, v6, p0, v3}, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 266069
    move-object v0, v4

    .line 266070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 266074
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266075
    iget-object v0, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 266076
    iget-object v0, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 266077
    iget-object v0, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 266078
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 266079
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 266080
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266081
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266082
    iget-object v2, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesGroupPartDefinition;->d:LX/0Uh;

    const/16 v3, 0x9a

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
