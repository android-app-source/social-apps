.class public Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/14w;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/14w;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "LX/1WL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 258902
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 258903
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a:LX/14w;

    .line 258904
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->b:LX/0Ot;

    .line 258905
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->c:LX/0Ot;

    .line 258906
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;
    .locals 6

    .prologue
    .line 258907
    const-class v1, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    monitor-enter v1

    .line 258908
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 258909
    sput-object v2, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 258910
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258911
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 258912
    new-instance v4, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    const/16 v5, 0x6c2

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xf47

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;-><init>(LX/14w;LX/0Ot;LX/0Ot;)V

    .line 258913
    move-object v0, v4

    .line 258914
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 258915
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258916
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 258917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 258918
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 258919
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 258920
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 258921
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    .line 258922
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 258923
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 258924
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a:LX/14w;

    invoke-virtual {v4, v3}, LX/14w;->m(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    move v4, v4

    .line 258925
    if-eqz v4, :cond_0

    .line 258926
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object p3

    .line 258927
    const/4 v4, 0x0

    move v5, v4

    :goto_1
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_1

    .line 258928
    invoke-virtual {p3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 258929
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1WL;

    invoke-interface {v0, v4}, LX/1WL;->a(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)LX/1RB;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-object v4, v0

    .line 258930
    if-eqz v4, :cond_4

    invoke-virtual {p1, v4, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 258931
    :cond_1
    goto :goto_0

    .line 258932
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 258933
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 258934
    :cond_3
    const/4 v0, 0x0

    return-object v0

    .line 258935
    :cond_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 258936
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 258937
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 258938
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 258939
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_UNIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    .line 258940
    if-nez v1, :cond_0

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
