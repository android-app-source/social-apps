.class public Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static s:LX/0Xm;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/22R",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/22T",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/22Y;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3u;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1et",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0ad;

.field private final j:LX/0bH;

.field private final k:LX/0yc;

.field private final l:LX/1CK;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1eq;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1f7;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/22P;

.field private r:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 264551
    new-instance v0, LX/1VV;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/1VV;-><init>(I)V

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->b:Landroid/util/SparseArray;

    .line 264552
    const-class v0, LX/1VW;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0bH;LX/0yc;LX/1CK;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/22P;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/22R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/22T;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/22Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/C3u;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1et;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0ad;",
            "LX/0bH;",
            "LX/0yc;",
            "LX/1CK;",
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1eq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1f7;",
            ">;",
            "LX/22P;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 264553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264554
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->c:LX/0Ot;

    .line 264555
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->d:LX/0Ot;

    .line 264556
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->e:LX/0Ot;

    .line 264557
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->f:LX/0Ot;

    .line 264558
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->g:LX/0Ot;

    .line 264559
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->h:LX/0Ot;

    .line 264560
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->i:LX/0ad;

    .line 264561
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->j:LX/0bH;

    .line 264562
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->k:LX/0yc;

    .line 264563
    iput-object p10, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->l:LX/1CK;

    .line 264564
    iput-object p11, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->m:LX/0Ot;

    .line 264565
    iput-object p12, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->n:LX/0Ot;

    .line 264566
    iput-object p13, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->o:LX/0Ot;

    .line 264567
    iput-object p14, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->p:LX/0Ot;

    .line 264568
    iput-object p15, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->q:LX/22P;

    .line 264569
    return-void
.end method

.method public static a(LX/1Ad;Lcom/facebook/common/callercontext/CallerContext;LX/1f6;)LX/1aZ;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 264570
    invoke-virtual {p0, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 264571
    iget-object v1, p2, LX/1f6;->c:LX/1bf;

    move-object v1, v1

    .line 264572
    invoke-virtual {v0, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v1

    .line 264573
    iget-object v0, p2, LX/1f6;->a:[LX/1bf;

    move-object v0, v0

    .line 264574
    if-eqz v0, :cond_0

    .line 264575
    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 264576
    iget-object v0, p2, LX/1f6;->a:[LX/1bf;

    move-object v0, v0

    .line 264577
    invoke-virtual {v1, v0}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    .line 264578
    :goto_0
    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0

    .line 264579
    :cond_0
    iget-object v0, p2, LX/1f6;->b:LX/1bf;

    move-object v0, v0

    .line 264580
    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->a([Ljava/lang/Object;)LX/1Ae;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;
    .locals 3

    .prologue
    .line 264596
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;

    monitor-enter v1

    .line 264597
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 264598
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264599
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264600
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->b(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 264601
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264602
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 264603
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/0bH;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bH;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264581
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 264582
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 264583
    if-nez v0, :cond_0

    .line 264584
    :goto_0
    return-void

    .line 264585
    :cond_0
    invoke-static {p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 264586
    new-instance v2, LX/1Zd;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v3, v0}, LX/1Zd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/1eq;LX/0Ot;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1eq;",
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 264587
    invoke-static {p3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264588
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 264589
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9jH;

    .line 264590
    iget-object v3, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 264591
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, LX/9jH;->a(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;Landroid/app/Activity;)V

    .line 264592
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 264593
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, v1}, LX/1eq;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 264594
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-interface {p4, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 264595
    return-void
.end method

.method public static a(LX/1eq;LX/0bH;LX/0yc;LX/1CK;LX/0Ot;Landroid/view/View;ILX/1aZ;ZLX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 14
    .param p7    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pb;",
            ":",
            "LX/1Pc;",
            ":",
            "LX/1Pd;",
            ":",
            "LX/1Pn;",
            ":",
            "LX/1Po;",
            ":",
            "LX/1Pp;",
            ":",
            "LX/1Pq;",
            ":",
            "LX/1Pk;",
            ":",
            "LX/1Pr;",
            ":",
            "LX/1Pt;",
            ":",
            "LX/1Pu;",
            ">(",
            "LX/1eq;",
            "LX/0bH;",
            "LX/0yc;",
            "LX/1CK;",
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;",
            "Landroid/view/View;",
            "I",
            "LX/1aZ;",
            "Z",
            "LX/1bf;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 264536
    invoke-static/range {p10 .. p10}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 264537
    invoke-virtual {v12}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/facebook/graphql/model/GraphQLStory;

    .line 264538
    invoke-static {p1, v12}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/0bH;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 264539
    invoke-virtual/range {p2 .. p2}, LX/0yc;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p9 .. p9}, LX/1bf;->b()Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p2

    move-object/from16 v1, p12

    invoke-virtual {v0, v2, v1}, LX/0yc;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {p2 .. p2}, LX/0yc;->n()Z

    move-result v2

    if-nez v2, :cond_1

    .line 264540
    check-cast p11, LX/1Pq;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p3

    move-object/from16 v1, p11

    invoke-virtual {v0, v13, v1, v2}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V

    .line 264541
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/0yc;->a(Z)Z

    .line 264542
    :cond_0
    :goto_0
    return-void

    .line 264543
    :cond_1
    new-instance v9, LX/C0s;

    move-object/from16 v0, p3

    move-object/from16 v1, p11

    invoke-direct {v9, v0, v13, v1}, LX/C0s;-><init>(LX/1CK;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pb;)V

    .line 264544
    invoke-static/range {p10 .. p10}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v10

    .line 264545
    invoke-static {v10}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v10}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v10

    .line 264546
    :cond_3
    invoke-interface/range {p4 .. p4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BaD;

    invoke-static/range {p10 .. p10}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v11, LX/9hK;

    move-object/from16 v0, p11

    invoke-direct {v11, v0}, LX/9hK;-><init>(LX/1Pb;)V

    :goto_1
    move-object/from16 v3, p10

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    move-object/from16 v6, p9

    move/from16 v7, p8

    move/from16 v8, p6

    invoke-virtual/range {v2 .. v11}, LX/BaD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V

    .line 264547
    if-eqz p8, :cond_0

    .line 264548
    invoke-virtual {p0, v13}, LX/1eq;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 264549
    check-cast p11, LX/1Pq;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    aput-object v12, v2, v3

    move-object/from16 v0, p11

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    .line 264550
    :cond_4
    const/4 v11, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 264494
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 264495
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;
    .locals 17

    .prologue
    .line 264534
    new-instance v1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;

    const/16 v2, 0x826

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x828

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x82b

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1ed3

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x82d

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x509

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v10

    check-cast v10, LX/0yc;

    invoke-static/range {p0 .. p0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v11

    check-cast v11, LX/1CK;

    const/16 v12, 0x179e

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x15d

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x2f25

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xf21

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/22P;->a(LX/0QB;)LX/22P;

    move-result-object v16

    check-cast v16, LX/22P;

    invoke-direct/range {v1 .. v16}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0bH;LX/0yc;LX/1CK;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/22P;)V

    .line 264535
    return-object v1
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 264532
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 264533
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;LX/1f6;ZLX/1dQ;ZLX/1np;LX/1np;LX/1np;)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1f6;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/1f6;",
            "Z",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;Z",
            "LX/1np",
            "<",
            "LX/1bf;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "LX/1aZ;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 264503
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 264504
    const/4 v1, 0x0

    .line 264505
    :goto_0
    return-object v1

    .line 264506
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 264507
    new-instance v4, LX/22Q;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, LX/22Q;-><init>(Ljava/lang/String;)V

    .line 264508
    if-nez p5, :cond_1

    .line 264509
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1et;

    move-object v2, p3

    check-cast v2, LX/1Po;

    invoke-virtual {v1, p2, v2}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1f6;

    move-result-object p5

    .line 264510
    :cond_1
    invoke-virtual {p5}, LX/1f6;->h()LX/1bf;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 264511
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1f7;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p5}, LX/1f6;->h()LX/1bf;

    move-result-object v5

    invoke-virtual {p5}, LX/1f6;->c()I

    move-result v6

    invoke-virtual {p5}, LX/1f6;->d()I

    move-result v7

    invoke-virtual {v1, v2, v5, v6, v7}, LX/1f7;->a(Ljava/lang/String;LX/1bf;II)V

    move-object v1, p3

    .line 264512
    check-cast v1, LX/1Pt;

    invoke-virtual {p5}, LX/1f6;->h()LX/1bf;

    move-result-object v2

    invoke-interface {v1, v2, p4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 264513
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1eo;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 264514
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {v1, p4, p5}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1Ad;Lcom/facebook/common/callercontext/CallerContext;LX/1f6;)LX/1aZ;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    move-object v1, p3

    .line 264515
    check-cast v1, LX/1Pp;

    invoke-virtual/range {p11 .. p11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1aZ;

    invoke-static {p2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5}, LX/1f6;->h()LX/1bf;

    move-result-object v6

    invoke-interface {v1, v2, v5, v6, p4}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 264516
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/22R;

    invoke-virtual {v1, p1}, LX/22R;->c(LX/1De;)LX/22S;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/22S;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22S;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/22S;->a(LX/22Q;)LX/22S;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/22S;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/22S;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/22S;->a(LX/1f6;)LX/22S;

    move-result-object v2

    invoke-virtual/range {p11 .. p11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1aZ;

    invoke-virtual {v2, v1}, LX/22S;->a(LX/1aZ;)LX/22S;

    move-result-object v2

    move-object v1, p3

    check-cast v1, LX/1Pr;

    invoke-virtual {v2, v1}, LX/22S;->a(LX/1Pr;)LX/22S;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/22S;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/22S;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 264517
    if-nez p6, :cond_2

    .line 264518
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_0

    .line 264519
    :cond_2
    if-nez p7, :cond_3

    .line 264520
    invoke-static {p1}, LX/1xN;->d(LX/1De;)LX/1dQ;

    move-result-object p7

    .line 264521
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->r:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    .line 264522
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->i:LX/0ad;

    sget-short v5, LX/1xO;->b:S

    const/4 v6, 0x0

    invoke-interface {v1, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->r:Ljava/lang/Boolean;

    .line 264523
    :cond_4
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->r:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 264524
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->q:LX/22P;

    invoke-virtual {v1, p1}, LX/22P;->c(LX/1De;)LX/32z;

    move-result-object v2

    invoke-virtual/range {p11 .. p11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1aZ;

    invoke-virtual {v2, v1}, LX/32z;->a(LX/1aZ;)LX/32z;

    move-result-object v1

    invoke-static {v3}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/32z;->a(Landroid/graphics/PointF;)LX/32z;

    move-result-object v2

    invoke-virtual/range {p10 .. p10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, LX/32z;->h(I)LX/32z;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->b:Landroid/util/SparseArray;

    invoke-interface {v1, v2}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v1

    invoke-virtual {p5}, LX/1f6;->e()I

    move-result v2

    invoke-interface {v1, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->k:LX/0yc;

    invoke-static {v2, p5, p4}, LX/1VW;->a(LX/0yc;LX/1f6;Lcom/facebook/common/callercontext/CallerContext;)I

    move-result v2

    invoke-interface {v1, v2}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 264525
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->k:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v4

    .line 264526
    if-eqz p8, :cond_5

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1eq;

    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/1eq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1eq;

    invoke-virtual {v1, p2, v3}, LX/1eq;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    .line 264527
    :goto_1
    if-nez v4, :cond_6

    if-nez v1, :cond_6

    move-object v1, v2

    .line 264528
    goto/16 :goto_0

    .line 264529
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 264530
    :cond_6
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x2

    invoke-interface {v1, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v1, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->k:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    if-nez p8, :cond_8

    const/4 v1, 0x0

    :goto_3
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/C3u;

    invoke-virtual {v1, p1}, LX/C3u;->c(LX/1De;)LX/C3s;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/C3s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3s;

    move-result-object v1

    check-cast p3, LX/1Pq;

    invoke-virtual {v1, p3}, LX/C3s;->a(LX/1Pq;)LX/C3s;

    move-result-object v1

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/22Y;

    invoke-virtual {v1, p1}, LX/22Y;->c(LX/1De;)LX/22a;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/22a;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22a;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/22a;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/22a;

    move-result-object v1

    invoke-static {p1}, LX/1xN;->e(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/22a;->a(LX/1dQ;)LX/22a;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-interface {v1, v3, v4}, LX/1Di;->m(II)LX/1Di;

    move-result-object v1

    const/4 v3, 0x3

    const/16 v4, 0x8

    invoke-interface {v1, v3, v4}, LX/1Di;->m(II)LX/1Di;

    move-result-object v1

    goto :goto_3

    .line 264531
    :cond_9
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x2

    invoke-interface {v1, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x1

    invoke-interface {v1, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->k:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x0

    :goto_4
    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/22T;

    invoke-virtual {v1, p1}, LX/22T;->c(LX/1De;)LX/22V;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/22V;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22V;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/22V;->a(LX/22Q;)LX/22V;

    move-result-object v4

    invoke-virtual/range {p10 .. p10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v4, v1}, LX/22V;->a(Ljava/lang/Integer;)LX/22V;

    move-result-object v1

    check-cast p3, LX/1Pq;

    invoke-virtual {v1, p3}, LX/22V;->a(LX/1Pq;)LX/22V;

    move-result-object v1

    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v2, p7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    sget-object v4, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->b:Landroid/util/SparseArray;

    invoke-interface {v2, v4}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    if-nez p8, :cond_b

    const/4 v1, 0x0

    :goto_5
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_0

    :cond_a
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/C3u;

    invoke-virtual {v1, p1}, LX/C3u;->c(LX/1De;)LX/C3s;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/C3s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3s;

    move-result-object v6

    move-object v1, p3

    check-cast v1, LX/1Pq;

    invoke-virtual {v6, v1}, LX/C3s;->a(LX/1Pq;)LX/C3s;

    move-result-object v1

    goto :goto_4

    :cond_b
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/22Y;

    invoke-virtual {v1, p1}, LX/22Y;->c(LX/1De;)LX/22a;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/22a;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22a;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/22a;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/22a;

    move-result-object v1

    invoke-static {p1}, LX/1xN;->e(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/22a;->a(LX/1dQ;)LX/22a;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-interface {v1, v3, v4}, LX/1Di;->m(II)LX/1Di;

    move-result-object v1

    const/4 v3, 0x3

    const/16 v4, 0x8

    invoke-interface {v1, v3, v4}, LX/1Di;->m(II)LX/1Di;

    move-result-object v1

    goto :goto_5
.end method

.method public final a(Landroid/view/View;LX/1bf;ILX/1aZ;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 13
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1Pb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1bf;",
            "I",
            "LX/1aZ;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 264501
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1eq;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->j:LX/0bH;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->k:LX/0yc;

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->l:LX/1CK;

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->m:LX/0Ot;

    const/4 v8, 0x0

    move-object v5, p1

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object v9, p2

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    invoke-static/range {v0 .. v12}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1eq;LX/0bH;LX/0yc;LX/1CK;LX/0Ot;Landroid/view/View;ILX/1aZ;ZLX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 264502
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Integer;LX/1bf;ILX/1aZ;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 16
    .param p6    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1Pb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            "LX/1bf;",
            "I",
            "LX/1aZ;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 264496
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 264497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->n:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1eq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->o:LX/0Ot;

    check-cast p7, LX/1Pq;

    move-object/from16 v0, p1

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-static {v3, v4, v0, v1, v2}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1eq;LX/0Ot;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 264498
    :goto_0
    return-void

    .line 264499
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    const v4, 0x7f0d24e0

    invoke-static {v3, v4}, LX/5JR;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v8

    .line 264500
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->n:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1eq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->j:LX/0bH;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->k:LX/0yc;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->l:LX/1CK;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->m:LX/0Ot;

    const/4 v11, 0x1

    move/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v12, p3

    move-object/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    invoke-static/range {v3 .. v15}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1eq;LX/0bH;LX/0yc;LX/1CK;LX/0Ot;Landroid/view/View;ILX/1aZ;ZLX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
