.class public Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/24e;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/24n;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static p:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0bH;

.field public final g:LX/1eq;

.field private final h:LX/1Ad;

.field public final i:LX/0yc;

.field public final j:LX/1er;

.field private final k:LX/0ad;

.field public final l:LX/1CK;

.field private final m:LX/0sX;

.field private final n:LX/1et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1et",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1f7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 289113
    const-class v0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;LX/0bH;LX/0Ot;LX/0Ot;LX/1eq;LX/1Ad;LX/0yc;LX/1er;LX/0ad;LX/1CK;LX/0sX;LX/1et;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;",
            "LX/0bH;",
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;",
            "LX/1eq;",
            "LX/1Ad;",
            "LX/0yc;",
            "LX/1er;",
            "LX/0ad;",
            "LX/1CK;",
            "LX/0sX;",
            "LX/1et;",
            "LX/0Ot",
            "<",
            "LX/1f7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289096
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 289097
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 289098
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    .line 289099
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->f:LX/0bH;

    .line 289100
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->d:LX/0Ot;

    .line 289101
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->e:LX/0Ot;

    .line 289102
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->g:LX/1eq;

    .line 289103
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->h:LX/1Ad;

    .line 289104
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->i:LX/0yc;

    .line 289105
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->j:LX/1er;

    .line 289106
    iput-object p11, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->l:LX/1CK;

    .line 289107
    iput-object p10, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->k:LX/0ad;

    .line 289108
    iput-object p12, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->m:LX/0sX;

    .line 289109
    iput-object p13, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->n:LX/1et;

    .line 289110
    iput-object p14, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->o:LX/0Ot;

    .line 289111
    invoke-virtual {p9}, LX/1er;->a()V

    .line 289112
    return-void
.end method

.method private a(LX/24k;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1f6;LX/1Pb;Ljava/lang/String;)LX/24n;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/24k;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1f6;",
            "TE;",
            "Ljava/lang/String;",
            ")",
            "LX/24n;"
        }
    .end annotation

    .prologue
    .line 289080
    invoke-virtual/range {p3 .. p3}, LX/1f6;->b()Ljava/lang/String;

    move-result-object v9

    .line 289081
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 289082
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 289083
    invoke-static {v1}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Landroid/graphics/PointF;

    move-result-object v7

    move-object/from16 v1, p4

    .line 289084
    check-cast v1, LX/1Pt;

    invoke-virtual/range {p3 .. p3}, LX/1f6;->h()LX/1bf;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v1, v2, v3}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 289085
    new-instance v1, LX/24l;

    invoke-virtual/range {p3 .. p3}, LX/1f6;->h()LX/1bf;

    move-result-object v3

    move-object v2, p0

    move-object v4, p2

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v5}, LX/24l;-><init>(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)V

    .line 289086
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->g:LX/1eq;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/1eq;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v10

    .line 289087
    const/4 v11, 0x0

    .line 289088
    const/4 v6, 0x0

    .line 289089
    packed-switch v10, :pswitch_data_0

    .line 289090
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->h:LX/1Ad;

    sget-object v3, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p3

    invoke-static {v2, v3, v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1Ad;Lcom/facebook/common/callercontext/CallerContext;LX/1f6;)LX/1aZ;

    move-result-object v4

    .line 289091
    check-cast p4, LX/1Pp;

    invoke-static {p2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, LX/1f6;->h()LX/1bf;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p4

    invoke-interface {v0, v4, v2, v3, v5}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 289092
    new-instance v2, LX/24n;

    invoke-virtual/range {p3 .. p3}, LX/1f6;->a()Landroid/net/Uri;

    move-result-object v8

    move-object v3, p1

    move-object v5, v1

    move-object/from16 v12, p5

    invoke-direct/range {v2 .. v12}, LX/24n;-><init>(LX/24k;LX/1aZ;LX/24m;LX/24m;Landroid/graphics/PointF;Landroid/net/Uri;Ljava/lang/String;IILjava/lang/String;)V

    return-object v2

    .line 289093
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->g:LX/1eq;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1eq;->b(Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v11

    move-object v6, v1

    .line 289094
    goto :goto_0

    .line 289095
    :pswitch_1
    new-instance v6, LX/C0o;

    move-object/from16 v2, p4

    check-cast v2, LX/1Pq;

    invoke-direct {v6, p0, p2, v2}, LX/C0o;-><init>(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 289072
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 289073
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 289074
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 289075
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289076
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 289077
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289078
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 289079
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic b(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;)LX/0ad;
    .locals 1

    .prologue
    .line 289114
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->k:LX/0ad;

    return-object v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;
    .locals 15

    .prologue
    .line 289070
    new-instance v0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    const/16 v4, 0x179e

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2f25

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/1eq;->a(LX/0QB;)LX/1eq;

    move-result-object v6

    check-cast v6, LX/1eq;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-static {p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v8

    check-cast v8, LX/0yc;

    invoke-static {p0}, LX/1er;->a(LX/0QB;)LX/1er;

    move-result-object v9

    check-cast v9, LX/1er;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {p0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v11

    check-cast v11, LX/1CK;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v12

    check-cast v12, LX/0sX;

    invoke-static {p0}, LX/1et;->a(LX/0QB;)LX/1et;

    move-result-object v13

    check-cast v13, LX/1et;

    const/16 v14, 0xf21

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;LX/0bH;LX/0Ot;LX/0Ot;LX/1eq;LX/1Ad;LX/0yc;LX/1er;LX/0ad;LX/1CK;LX/0sX;LX/1et;LX/0Ot;)V

    .line 289071
    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 289051
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    .line 289052
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 289053
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 289054
    sget-object v1, LX/1Ua;->e:LX/1Ua;

    move-object v1, v1

    .line 289055
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p2, v1}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 289056
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->n:LX/1et;

    move-object v1, p3

    check-cast v1, LX/1Po;

    invoke-virtual {v2, p2, v1}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1f6;

    move-result-object v3

    move-object v1, p3

    .line 289057
    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->m:LX/0sX;

    invoke-static {v1, v2, v0}, LX/1VW;->a(Landroid/content/Context;LX/0sX;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v5

    .line 289058
    new-instance v1, LX/24k;

    .line 289059
    iget v2, v3, LX/1f6;->g:I

    move v2, v2

    .line 289060
    iget v4, v3, LX/1f6;->h:I

    move v4, v4

    .line 289061
    invoke-direct {v1, v2, v4}, LX/24k;-><init>(II)V

    .line 289062
    const v2, 0x7f0d24e0

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    invoke-interface {p1, v2, v4, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 289063
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 289064
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1f7;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    .line 289065
    iget-object v4, v3, LX/1f6;->b:LX/1bf;

    move-object v4, v4

    .line 289066
    iget v6, v3, LX/1f6;->e:I

    move v6, v6

    .line 289067
    iget v7, v3, LX/1f6;->f:I

    move v7, v7

    .line 289068
    invoke-virtual {v2, v0, v4, v6, v7}, LX/1f7;->a(Ljava/lang/String;LX/1bf;II)V

    :cond_0
    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    .line 289069
    invoke-direct/range {v0 .. v5}, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a(LX/24k;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1f6;LX/1Pb;Ljava/lang/String;)LX/24n;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5b280279

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 289023
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/24n;

    const/4 v5, 0x0

    .line 289024
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 289025
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-object v2, p4

    .line 289026
    check-cast v2, LX/24e;

    iget-object v4, p2, LX/24n;->a:LX/1aZ;

    invoke-interface {v2, v4}, LX/24e;->setController(LX/1aZ;)V

    move-object v2, p4

    .line 289027
    check-cast v2, LX/24e;

    iget-object v4, p2, LX/24n;->b:LX/24m;

    invoke-interface {v2, v4}, LX/24e;->setOnPhotoClickListener(LX/24m;)V

    move-object v2, p4

    .line 289028
    check-cast v2, LX/24e;

    iget-object v4, p2, LX/24n;->c:LX/24m;

    invoke-interface {v2, v4}, LX/24e;->setOnBadgeClickListener(LX/24m;)V

    move-object v2, p4

    .line 289029
    check-cast v2, LX/24e;

    iget-object v4, p2, LX/24n;->e:Ljava/lang/String;

    invoke-interface {v2, v4}, LX/24e;->setPairedVideoUri(Ljava/lang/String;)V

    .line 289030
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_0
    move-object v1, p4

    .line 289031
    check-cast v1, LX/24e;

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->j:LX/1er;

    .line 289032
    iget p3, v4, LX/1er;->c:I

    move v4, p3

    .line 289033
    invoke-interface {v1, v2, v4}, LX/24e;->a(Ljava/lang/String;I)V

    move-object v1, p4

    .line 289034
    check-cast v1, LX/24e;

    iget-object v2, p2, LX/24n;->f:Landroid/graphics/PointF;

    invoke-interface {v1, v2}, LX/24e;->setActualImageFocusPoint(Landroid/graphics/PointF;)V

    .line 289035
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->g:LX/1eq;

    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1eq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, p4

    .line 289036
    check-cast v1, LX/24e;

    iget v2, p2, LX/24n;->g:I

    iget v4, p2, LX/24n;->h:I

    invoke-interface {v1, v2, v4}, LX/24e;->a(II)V

    .line 289037
    :goto_1
    check-cast p4, LX/24e;

    invoke-interface {p4}, LX/24e;->getPhotoAttachmentView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p2, LX/24n;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 289038
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->i:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->i:LX/0yc;

    iget-object v2, p2, LX/24n;->d:Landroid/net/Uri;

    sget-object v4, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v4}, LX/0yc;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->i:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->n()Z

    move-result v1

    if-nez v1, :cond_2

    .line 289039
    iget-object v1, p2, LX/24n;->j:LX/24k;

    const/4 v2, 0x1

    iput-boolean v2, v1, LX/24k;->b:Z

    .line 289040
    iget-object v1, p2, LX/24n;->j:LX/24k;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->i:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->d()I

    move-result v2

    iput v2, v1, LX/24k;->a:I

    .line 289041
    :goto_2
    const/16 v1, 0x1f

    const v2, -0x1049655c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 289042
    :cond_0
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_0

    :cond_1
    move-object v1, p4

    .line 289043
    check-cast v1, LX/24e;

    invoke-interface {v1, v5, v5}, LX/24e;->a(II)V

    goto :goto_1

    .line 289044
    :cond_2
    iget-object v1, p2, LX/24n;->j:LX/24k;

    iput-boolean v5, v1, LX/24k;->b:Z

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 289045
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 289046
    move-object v0, p4

    check-cast v0, LX/24e;

    invoke-interface {v0}, LX/24e;->getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    move-object v0, p4

    .line 289047
    check-cast v0, LX/24e;

    invoke-interface {v0, v1}, LX/24e;->setOnPhotoClickListener(LX/24m;)V

    move-object v0, p4

    .line 289048
    check-cast v0, LX/24e;

    invoke-interface {v0, v2, v2}, LX/24e;->a(II)V

    .line 289049
    check-cast p4, LX/24e;

    invoke-interface {p4, v1}, LX/24e;->setOnBadgeClickListener(LX/24m;)V

    .line 289050
    return-void
.end method
