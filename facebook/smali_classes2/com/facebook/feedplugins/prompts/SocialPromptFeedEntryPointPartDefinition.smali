.class public Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/C2P;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CAt;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0ad;

.field private final e:Landroid/content/Context;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 266187
    new-instance v0, LX/1Vi;

    invoke-direct {v0}, LX/1Vi;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0ad;Landroid/content/Context;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CAt;",
            ">;",
            "LX/0ad;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266180
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 266181
    iput-object p1, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->b:LX/0Ot;

    .line 266182
    iput-object p2, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->c:LX/0Ot;

    .line 266183
    iput-object p3, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->d:LX/0ad;

    .line 266184
    iput-object p4, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->e:Landroid/content/Context;

    .line 266185
    iput-object p5, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->f:LX/0Or;

    .line 266186
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;
    .locals 9

    .prologue
    .line 266169
    const-class v1, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    monitor-enter v1

    .line 266170
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266171
    sput-object v2, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266172
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266173
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266174
    new-instance v3, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    const/16 v4, 0x871

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x20b1

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0ad;Landroid/content/Context;LX/0Or;)V

    .line 266175
    move-object v0, v3

    .line 266176
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266177
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266178
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266179
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 266164
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 266165
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 266166
    :goto_1
    return-object v0

    .line 266167
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 266168
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/C2P;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266163
    sget-object v0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 266150
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v4, 0x0

    .line 266151
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266152
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aC()Ljava/lang/String;

    move-result-object v0

    .line 266153
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 266154
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 266155
    new-instance v2, LX/CAu;

    invoke-direct {v2, p0, v1, v0}, LX/CAu;-><init>(Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;Landroid/content/Context;Ljava/lang/String;)V

    move-object v2, v2

    .line 266156
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266157
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266158
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v1

    .line 266159
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    :goto_0
    move-object v1, v1

    .line 266160
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f082a3b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object v0, p2

    move-object v3, v2

    move-object v5, v4

    invoke-static/range {v0 .. v7}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Z)LX/C2O;

    move-result-object v1

    .line 266161
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 266162
    return-object v4

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 266141
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 266142
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->d:LX/0ad;

    sget-short v1, LX/1Nu;->p:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 266143
    :goto_0
    return v0

    .line 266144
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266145
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266146
    iget-object v1, p0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move v0, v2

    .line 266147
    goto :goto_0

    .line 266148
    :cond_1
    invoke-static {v0}, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 266149
    sget-object v0, LX/1X8;->FOLLOWUP_FEEDUNIT:LX/1X8;

    return-object v0
.end method
