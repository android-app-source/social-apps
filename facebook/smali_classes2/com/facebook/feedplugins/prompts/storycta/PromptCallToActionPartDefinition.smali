.class public Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/CB7;",
        "TE;",
        "LX/C2P;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final c:LX/1Qh;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public final d:LX/1Ns;

.field public final e:LX/14w;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Nq",
            "<",
            "Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;",
            ">;>;"
        }
    .end annotation
.end field

.field public final g:LX/0ad;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5oW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 266252
    new-instance v0, LX/1Vj;

    invoke-direct {v0}, LX/1Vj;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->a:LX/1Cz;

    .line 266253
    new-instance v0, LX/1Qg;

    invoke-direct {v0}, LX/1Qg;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->c:LX/1Qh;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/1Ns;LX/14w;LX/0Ot;LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;",
            ">;",
            "LX/1Ns;",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "LX/1Nq;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/5oW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266244
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 266245
    iput-object p1, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->b:LX/0Ot;

    .line 266246
    iput-object p2, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->d:LX/1Ns;

    .line 266247
    iput-object p3, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->e:LX/14w;

    .line 266248
    iput-object p4, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->f:LX/0Ot;

    .line 266249
    iput-object p5, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->g:LX/0ad;

    .line 266250
    iput-object p6, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->h:LX/0Ot;

    .line 266251
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;
    .locals 1

    .prologue
    .line 266243
    invoke-static {p0}, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;
    .locals 7

    .prologue
    .line 266241
    new-instance v0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    const/16 v1, 0x871

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const-class v2, LX/1Ns;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/1Ns;

    invoke-static {p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    const/16 v4, 0xbdd

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    const/16 v6, 0x2ff9

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;-><init>(LX/0Ot;LX/1Ns;LX/14w;LX/0Ot;LX/0ad;LX/0Ot;)V

    .line 266242
    return-object v0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 11

    .prologue
    const-wide/16 v6, 0x2710

    .line 266254
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 266255
    new-instance v1, LX/2ry;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/2ry;-><init>(Ljava/lang/String;)V

    .line 266256
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineActivity;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivity;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 266257
    const/4 v5, 0x0

    .line 266258
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v4

    .line 266259
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    move v4, v4

    .line 266260
    if-eqz v4, :cond_0

    .line 266261
    invoke-static {v0}, LX/2s1;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/2s1;

    move-result-object v5

    .line 266262
    iget-object v4, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 266263
    new-instance v8, LX/5LL;

    invoke-direct {v8}, LX/5LL;-><init>()V

    .line 266264
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->j()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 266265
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->k()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->b:Ljava/lang/String;

    .line 266266
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->b()LX/0Px;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->c:LX/0Px;

    .line 266267
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->d:Ljava/lang/String;

    .line 266268
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->l()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$IconModel;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->e:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$IconModel;

    .line 266269
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 266270
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    .line 266271
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->o()Z

    move-result v9

    iput-boolean v9, v8, LX/5LL;->h:Z

    .line 266272
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->p()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->i:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;

    .line 266273
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ag_()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, LX/5LL;->j:Ljava/lang/String;

    .line 266274
    move-object v8, v8

    .line 266275
    iget-object v4, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v4

    .line 266276
    new-instance v9, LX/5Lc;

    invoke-direct {v9}, LX/5Lc;-><init>()V

    .line 266277
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 266278
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->b:Ljava/lang/String;

    .line 266279
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->c()Z

    move-result v10

    iput-boolean v10, v9, LX/5Lc;->c:Z

    .line 266280
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->d:Ljava/lang/String;

    .line 266281
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$OpenGraphComposerPreviewModel;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->e:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$OpenGraphComposerPreviewModel;

    .line 266282
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$PageModel;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->f:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$PageModel;

    .line 266283
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->n()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->g:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 266284
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 266285
    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->j()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Lc;->i:Ljava/lang/String;

    .line 266286
    move-object v9, v9

    .line 266287
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;->a()LX/0Px;

    move-result-object v4

    const/4 v10, 0x0

    invoke-virtual {v4, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    .line 266288
    iput-object v4, v9, LX/5Lc;->b:Ljava/lang/String;

    .line 266289
    move-object v4, v9

    .line 266290
    invoke-virtual {v4}, LX/5Lc;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v4

    .line 266291
    iput-object v4, v8, LX/5LL;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    .line 266292
    invoke-virtual {v8}, LX/5LL;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v4

    .line 266293
    iput-object v4, v5, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 266294
    move-object v4, v5

    .line 266295
    invoke-virtual {v4}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v4

    move-object v0, v4

    .line 266296
    :cond_0
    iput-object v0, v1, LX/2ry;->i:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 266297
    sub-long v4, v2, v6

    .line 266298
    iput-wide v4, v1, LX/2ry;->f:J

    .line 266299
    add-long/2addr v2, v6

    .line 266300
    iput-wide v2, v1, LX/2ry;->g:J

    .line 266301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v0

    .line 266302
    iput-object v0, v1, LX/2ry;->p:Ljava/lang/String;

    .line 266303
    invoke-virtual {v1}, LX/2ry;->J()Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    return-object v0

    :cond_1
    move v4, v5

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/C2P;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266240
    sget-object v0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 266218
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 266219
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266220
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v5, 0x0

    .line 266221
    new-instance v3, LX/4Ys;

    invoke-direct {v3}, LX/4Ys;-><init>()V

    .line 266222
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->j()Ljava/lang/String;

    move-result-object v1

    .line 266223
    iput-object v1, v3, LX/4Ys;->bz:Ljava/lang/String;

    .line 266224
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    .line 266225
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->k()Ljava/lang/String;

    move-result-object v1

    .line 266226
    iput-object v1, v4, LX/173;->f:Ljava/lang/String;

    .line 266227
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 266228
    iput-object v1, v3, LX/4Ys;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 266229
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    .line 266230
    invoke-virtual {v3}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 266231
    iput-object v3, v1, LX/23u;->b:LX/0Px;

    .line 266232
    move-object v1, v1

    .line 266233
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    move-object v4, v1

    .line 266234
    new-instance v8, LX/CB7;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PromptCtaTrackingString"

    const-string v5, ""

    invoke-direct {v8, v0, v1, v3, v5}, LX/CB7;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266235
    invoke-virtual {p2, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 266236
    new-instance v3, LX/CB6;

    invoke-direct {v3, p0, v4, v8}, LX/CB6;-><init>(Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/CB7;)V

    move-object v3, v3

    .line 266237
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->j()Ljava/lang/String;

    move-result-object v6

    move-object v4, v2

    move-object v5, v2

    invoke-static/range {v0 .. v7}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Z)LX/C2O;

    move-result-object v1

    .line 266238
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 266239
    return-object v8
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1ed2f511

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 266207
    check-cast p2, LX/CB7;

    check-cast p4, LX/C2P;

    const/4 v2, 0x0

    .line 266208
    iget-boolean v1, p2, LX/CB7;->b:Z

    if-nez v1, :cond_0

    .line 266209
    const/4 v1, 0x1

    .line 266210
    iput-boolean v1, p2, LX/CB7;->b:Z

    .line 266211
    iget-object v1, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5oW;

    iget-object p1, p2, LX/CB7;->a:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 266212
    sget-object p0, LX/5oV;->IMPRESSION:LX/5oV;

    invoke-static {v1, p0, p1}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 266213
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p4, v1}, LX/C2P;->setBackgroundColor(I)V

    .line 266214
    const v1, 0x7f0d0390

    invoke-virtual {p4, v1}, LX/C2P;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 266215
    invoke-virtual {p4}, LX/C2P;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p0, 0x7f0201b7

    invoke-virtual {p1, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-static {v1, p1}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 266216
    invoke-virtual {p4}, LX/C2P;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p4, v1, v2, v2, v2}, LX/C2P;->setPadding(IIII)V

    .line 266217
    const/16 v1, 0x1f

    const v2, 0x6c8c8d60

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 266190
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 266191
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266192
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266193
    iget-object v1, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->g:LX/0ad;

    sget-short v2, LX/1Nu;->q:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 266194
    if-eqz v1, :cond_2

    .line 266195
    iget-object v1, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->e:LX/14w;

    invoke-virtual {v1, v0}, LX/14w;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    move v1, v1

    .line 266196
    if-nez v1, :cond_2

    const/4 v2, 0x0

    .line 266197
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsEdge;->a()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 266198
    if-eqz v1, :cond_2

    .line 266199
    iget-object v1, p0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->g:LX/0ad;

    sget-short v2, LX/1Nu;->r:S

    const/4 p1, 0x1

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 266200
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 266201
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 266202
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->V()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 266203
    :cond_0
    :goto_1
    move v0, v1

    .line 266204
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_0

    .line 266205
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 266206
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method
