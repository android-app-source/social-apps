.class public Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        "P::",
        "Lcom/facebook/graphql/model/HideableUnit;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/1VN",
        "<TE;TP;>;",
        "LX/1Vb;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 249589
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 249590
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;
    .locals 3

    .prologue
    .line 249591
    const-class v1, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;

    monitor-enter v1

    .line 249592
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 249593
    sput-object v2, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 249594
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249595
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 249596
    new-instance v0, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;-><init>()V

    .line 249597
    move-object v0, v0

    .line 249598
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 249599
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hidden/HideableUnitWrapperPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249600
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 249601
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 249602
    check-cast p2, LX/1VN;

    check-cast p3, LX/1Pr;

    .line 249603
    iget-object v0, p2, LX/1VN;->a:LX/1VM;

    iget-object v1, p2, LX/1VN;->b:Lcom/facebook/graphql/model/HideableUnit;

    .line 249604
    iget-object v2, v0, LX/1VM;->a:Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    invoke-virtual {p1, v2, p0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 249605
    new-instance v0, LX/1VZ;

    iget-object v1, p2, LX/1VN;->b:Lcom/facebook/graphql/model/HideableUnit;

    invoke-direct {v0, v1}, LX/1VZ;-><init>(Lcom/facebook/graphql/model/HideableUnit;)V

    iget-object v1, p2, LX/1VN;->b:Lcom/facebook/graphql/model/HideableUnit;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Va;

    .line 249606
    new-instance v1, LX/1Vb;

    invoke-direct {v1, v0}, LX/1Vb;-><init>(LX/1Va;)V

    return-object v1
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 1

    .prologue
    .line 249607
    check-cast p2, LX/1Vb;

    .line 249608
    iget-object v0, p2, LX/1Vb;->a:LX/1Va;

    invoke-virtual {v0}, LX/1Va;->b()V

    .line 249609
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 249610
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 2

    .prologue
    .line 249611
    check-cast p1, LX/1VN;

    check-cast p2, LX/1Vb;

    .line 249612
    iget-object v0, p1, LX/1VN;->b:Lcom/facebook/graphql/model/HideableUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v0, v1, :cond_0

    .line 249613
    :goto_0
    return-void

    .line 249614
    :cond_0
    iget-object v0, p2, LX/1Vb;->a:LX/1Va;

    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 249615
    iget p0, v0, LX/1Va;->a:I

    move p0, p0

    .line 249616
    add-int/2addr p0, v1

    .line 249617
    iput p0, v0, LX/1Va;->a:I

    .line 249618
    goto :goto_0
.end method
