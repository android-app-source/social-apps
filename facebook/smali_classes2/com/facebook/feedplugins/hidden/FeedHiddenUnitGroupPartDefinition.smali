.class public Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition",
            "<",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            "TE;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 256203
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 256204
    iput-object p1, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;->a:LX/0Ot;

    .line 256205
    iput-object p2, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;->b:LX/0Ot;

    .line 256206
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;
    .locals 5

    .prologue
    .line 256192
    const-class v1, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;

    monitor-enter v1

    .line 256193
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 256194
    sput-object v2, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 256195
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256196
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 256197
    new-instance v3, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;

    const/16 v4, 0x955

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x92f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 256198
    move-object v0, v3

    .line 256199
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 256200
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256201
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 256202
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 256180
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 256181
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 256182
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 256183
    iget-object v1, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 256184
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 256185
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 256186
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 256187
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 256188
    instance-of v1, v0, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v1}, Lcom/facebook/graphql/model/Sponsorable;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 256189
    :goto_0
    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v0, v4, :cond_1

    if-nez v1, :cond_1

    :goto_1
    return v2

    :cond_0
    move v1, v3

    .line 256190
    goto :goto_0

    :cond_1
    move v2, v3

    .line 256191
    goto :goto_1
.end method
