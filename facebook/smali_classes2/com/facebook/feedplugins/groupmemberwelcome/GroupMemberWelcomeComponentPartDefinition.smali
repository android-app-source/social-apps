.class public Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/0if;

.field private final f:LX/C7c;

.field private final g:LX/0W3;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/0if;LX/C7c;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265161
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 265162
    iput-object p2, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->d:LX/1V0;

    .line 265163
    iput-object p3, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->e:LX/0if;

    .line 265164
    iput-object p4, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->f:LX/C7c;

    .line 265165
    iput-object p5, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->g:LX/0W3;

    .line 265166
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 265126
    iget-object v0, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->f:LX/C7c;

    const/4 v1, 0x0

    .line 265127
    new-instance v2, LX/C7b;

    invoke-direct {v2, v0}, LX/C7b;-><init>(LX/C7c;)V

    .line 265128
    iget-object v3, v0, LX/C7c;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C7a;

    .line 265129
    if-nez v3, :cond_0

    .line 265130
    new-instance v3, LX/C7a;

    invoke-direct {v3, v0}, LX/C7a;-><init>(LX/C7c;)V

    .line 265131
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C7a;->a$redex0(LX/C7a;LX/1De;IILX/C7b;)V

    .line 265132
    move-object v2, v3

    .line 265133
    move-object v1, v2

    .line 265134
    move-object v1, v1

    .line 265135
    move-object v0, p3

    check-cast v0, LX/1Pn;

    .line 265136
    iget-object v2, v1, LX/C7a;->a:LX/C7b;

    iput-object v0, v2, LX/C7b;->a:LX/1Pn;

    .line 265137
    iget-object v2, v1, LX/C7a;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 265138
    move-object v0, v1

    .line 265139
    iget-object v1, v0, LX/C7a;->a:LX/C7b;

    iput-object p2, v1, LX/C7b;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265140
    iget-object v1, v0, LX/C7a;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 265141
    move-object v0, v0

    .line 265142
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 265143
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 265144
    iget-object v2, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;
    .locals 9

    .prologue
    .line 265145
    const-class v1, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;

    monitor-enter v1

    .line 265146
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265147
    sput-object v2, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265148
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265149
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265150
    new-instance v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v6

    check-cast v6, LX/0if;

    invoke-static {v0}, LX/C7c;->a(LX/0QB;)LX/C7c;

    move-result-object v7

    check-cast v7, LX/C7c;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/0if;LX/C7c;LX/0W3;)V

    .line 265151
    move-object v0, v3

    .line 265152
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265153
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265154
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265155
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 265121
    check-cast p3, LX/1Pn;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 265122
    iget-object v0, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->e:LX/0if;

    sget-object v1, LX/0ig;->aF:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 265123
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 265156
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265157
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    const v1, 0x3404bdf5

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 265158
    check-cast p3, LX/1Pn;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 265159
    iget-object v0, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->e:LX/0if;

    sget-object v1, LX/0ig;->aF:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 265160
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 265124
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 265125
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 265113
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x66404cd4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 265114
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pb;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, -0x233e073

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 265115
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265116
    invoke-static {p1}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->g:LX/0W3;

    sget-wide v2, LX/0X5;->fe:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 265117
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265118
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 265119
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 265120
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pb;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method
