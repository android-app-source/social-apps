.class public Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C2O;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3WJ;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 268250
    const-class v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268244
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 268245
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 268246
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 268247
    iput-object p3, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 268248
    iput-object p4, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 268249
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;
    .locals 7

    .prologue
    .line 268233
    const-class v1, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    monitor-enter v1

    .line 268234
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268235
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268236
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268237
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268238
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 268239
    move-object v0, p0

    .line 268240
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268241
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268242
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 268232
    invoke-static {p0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 268230
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 268231
    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 268251
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 268252
    if-nez v1, :cond_1

    .line 268253
    :cond_0
    :goto_0
    return v0

    .line 268254
    :cond_1
    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 268255
    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 268175
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 268176
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 268178
    :goto_0
    return-object v0

    .line 268179
    :cond_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 268180
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 268181
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 268182
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v0

    add-int/2addr v0, v7

    const/16 v7, 0x11

    invoke-virtual {v1, v4, v6, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 268183
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 268184
    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 268185
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1

    .prologue
    .line 268186
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 268187
    check-cast p2, LX/C2O;

    check-cast p3, LX/1Ps;

    .line 268188
    iget-boolean v0, p2, LX/C2O;->j:Z

    if-eqz v0, :cond_0

    .line 268189
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p2, LX/C2O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C2O;->i:LX/1Ua;

    const/4 v4, -0x1

    .line 268190
    new-instance v3, LX/1X6;

    invoke-direct {v3, v1, v2, v4, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    move-object v1, v3

    .line 268191
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268192
    :cond_0
    const v0, 0x7f0d038f

    iget-object v1, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/C2O;->g:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 268193
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/C2O;->b:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268194
    const v0, 0x7f0d0390

    iget-object v1, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v2, p2, LX/C2O;->c:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 268195
    iget-object v0, p2, LX/C2O;->d:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 268196
    check-cast p3, LX/1Pt;

    iget-object v0, p2, LX/C2O;->d:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v0, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 268197
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x73fa2454

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 268198
    check-cast p1, LX/C2O;

    check-cast p4, LX/3WJ;

    .line 268199
    iget-object v1, p1, LX/C2O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268200
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 268201
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268202
    iget-object v2, p1, LX/C2O;->d:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 268203
    iget-object v2, p1, LX/C2O;->d:Landroid/net/Uri;

    sget-object p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 268204
    iget-object p2, p4, LX/3WJ;->l:LX/0zw;

    invoke-virtual {p2}, LX/0zw;->a()Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 268205
    iget-object p2, p4, LX/3WJ;->l:LX/0zw;

    invoke-virtual {p2}, LX/0zw;->a()Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2, v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 268206
    :goto_0
    iget-object v2, p1, LX/C2O;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 268207
    iget-object v2, p1, LX/C2O;->h:Ljava/lang/String;

    .line 268208
    iget-object p0, p4, LX/3WJ;->m:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268209
    iget-object p0, p4, LX/3WJ;->m:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268210
    :goto_1
    const/4 p0, 0x1

    const/4 p2, 0x0

    .line 268211
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-nez v2, :cond_4

    .line 268212
    :cond_0
    :goto_2
    move v1, p2

    .line 268213
    iput-boolean v1, p4, LX/3WJ;->n:Z

    .line 268214
    invoke-virtual {p4}, LX/3WJ;->invalidate()V

    .line 268215
    const/16 v1, 0x1f

    const v2, -0x715d7e6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 268216
    :cond_1
    iget-object v2, p1, LX/C2O;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    .line 268217
    iget-object v2, p1, LX/C2O;->e:Landroid/graphics/drawable/Drawable;

    .line 268218
    iget-object p0, p4, LX/3WJ;->l:LX/0zw;

    invoke-virtual {p0}, LX/0zw;->a()Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 268219
    iget-object p0, p4, LX/3WJ;->l:LX/0zw;

    invoke-virtual {p0}, LX/0zw;->a()Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 268220
    goto :goto_0

    .line 268221
    :cond_2
    iget-object v2, p4, LX/3WJ;->l:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->c()V

    .line 268222
    goto :goto_0

    .line 268223
    :cond_3
    iget-object v2, p4, LX/3WJ;->m:Landroid/widget/TextView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268224
    goto :goto_1

    .line 268225
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v2

    if-lez v2, :cond_6

    move v2, p0

    .line 268226
    :goto_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object p3

    if-eqz p3, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->a()I

    move-result p3

    if-lez p3, :cond_7

    move p3, p0

    .line 268227
    :goto_4
    if-nez v2, :cond_5

    if-eqz p3, :cond_0

    :cond_5
    move p2, p0

    goto :goto_2

    :cond_6
    move v2, p2

    .line 268228
    goto :goto_3

    :cond_7
    move p3, p2

    .line 268229
    goto :goto_4
.end method
