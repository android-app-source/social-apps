.class public final Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1nu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:LX/4Ab;

.field public g:LX/1Up;

.field public h:Landroid/graphics/PointF;

.field public i:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/1Up;

.field public k:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/1Up;

.field public m:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1Up;

.field public o:I

.field public p:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/1Up;

.field public r:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public s:F

.field public t:Landroid/graphics/ColorFilter;

.field public u:LX/33B;

.field public v:Ljava/lang/String;

.field public w:LX/1Ai;

.field public x:LX/1f9;

.field public final synthetic y:LX/1nu;


# direct methods
.method public constructor <init>(LX/1nu;)V
    .locals 1

    .prologue
    .line 317701
    iput-object p1, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->y:LX/1nu;

    .line 317702
    move-object v0, p1

    .line 317703
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 317704
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->e:Z

    .line 317705
    sget-object v0, LX/1nv;->a:LX/1Up;

    iput-object v0, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->g:LX/1Up;

    .line 317706
    sget-object v0, LX/1nv;->c:LX/1Up;

    iput-object v0, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->j:LX/1Up;

    .line 317707
    sget-object v0, LX/1nv;->b:LX/1Up;

    iput-object v0, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->l:LX/1Up;

    .line 317708
    sget-object v0, LX/1nv;->d:LX/1Up;

    iput-object v0, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->n:LX/1Up;

    .line 317709
    sget-object v0, LX/1nv;->e:LX/1Up;

    iput-object v0, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->q:LX/1Up;

    .line 317710
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->s:F

    .line 317711
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317700
    const-string v0, "FbFeedFrescoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 317623
    if-ne p0, p1, :cond_1

    .line 317624
    :cond_0
    :goto_0
    return v0

    .line 317625
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 317626
    goto :goto_0

    .line 317627
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    .line 317628
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 317629
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 317630
    if-eq v2, v3, :cond_0

    .line 317631
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 317632
    goto :goto_0

    .line 317633
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 317634
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 317635
    goto :goto_0

    .line 317636
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 317637
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 317638
    goto :goto_0

    .line 317639
    :cond_b
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_a

    .line 317640
    :cond_c
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->d:LX/1Pp;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->d:LX/1Pp;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->d:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 317641
    goto :goto_0

    .line 317642
    :cond_e
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->d:LX/1Pp;

    if-nez v2, :cond_d

    .line 317643
    :cond_f
    iget-boolean v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->e:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->e:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 317644
    goto :goto_0

    .line 317645
    :cond_10
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->f:LX/4Ab;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->f:LX/4Ab;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->f:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 317646
    goto :goto_0

    .line 317647
    :cond_12
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->f:LX/4Ab;

    if-nez v2, :cond_11

    .line 317648
    :cond_13
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->g:LX/1Up;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->g:LX/1Up;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->g:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 317649
    goto/16 :goto_0

    .line 317650
    :cond_15
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->g:LX/1Up;

    if-nez v2, :cond_14

    .line 317651
    :cond_16
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->h:Landroid/graphics/PointF;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->h:Landroid/graphics/PointF;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->h:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 317652
    goto/16 :goto_0

    .line 317653
    :cond_18
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->h:Landroid/graphics/PointF;

    if-nez v2, :cond_17

    .line 317654
    :cond_19
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 317655
    goto/16 :goto_0

    .line 317656
    :cond_1b
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    if-nez v2, :cond_1a

    .line 317657
    :cond_1c
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->j:LX/1Up;

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->j:LX/1Up;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->j:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 317658
    goto/16 :goto_0

    .line 317659
    :cond_1e
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->j:LX/1Up;

    if-nez v2, :cond_1d

    .line 317660
    :cond_1f
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->k:LX/1dc;

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->k:LX/1dc;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->k:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    .line 317661
    goto/16 :goto_0

    .line 317662
    :cond_21
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->k:LX/1dc;

    if-nez v2, :cond_20

    .line 317663
    :cond_22
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->l:LX/1Up;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->l:LX/1Up;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->l:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 317664
    goto/16 :goto_0

    .line 317665
    :cond_24
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->l:LX/1Up;

    if-nez v2, :cond_23

    .line 317666
    :cond_25
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->m:LX/1dc;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->m:LX/1dc;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->m:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    :cond_26
    move v0, v1

    .line 317667
    goto/16 :goto_0

    .line 317668
    :cond_27
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->m:LX/1dc;

    if-nez v2, :cond_26

    .line 317669
    :cond_28
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->n:LX/1Up;

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->n:LX/1Up;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->n:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2b

    :cond_29
    move v0, v1

    .line 317670
    goto/16 :goto_0

    .line 317671
    :cond_2a
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->n:LX/1Up;

    if-nez v2, :cond_29

    .line 317672
    :cond_2b
    iget v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->o:I

    iget v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->o:I

    if-eq v2, v3, :cond_2c

    move v0, v1

    .line 317673
    goto/16 :goto_0

    .line 317674
    :cond_2c
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->p:LX/1dc;

    if-eqz v2, :cond_2e

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->p:LX/1dc;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->p:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f

    :cond_2d
    move v0, v1

    .line 317675
    goto/16 :goto_0

    .line 317676
    :cond_2e
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->p:LX/1dc;

    if-nez v2, :cond_2d

    .line 317677
    :cond_2f
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->q:LX/1Up;

    if-eqz v2, :cond_31

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->q:LX/1Up;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->q:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_32

    :cond_30
    move v0, v1

    .line 317678
    goto/16 :goto_0

    .line 317679
    :cond_31
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->q:LX/1Up;

    if-nez v2, :cond_30

    .line 317680
    :cond_32
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->r:LX/1dc;

    if-eqz v2, :cond_34

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->r:LX/1dc;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->r:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_35

    :cond_33
    move v0, v1

    .line 317681
    goto/16 :goto_0

    .line 317682
    :cond_34
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->r:LX/1dc;

    if-nez v2, :cond_33

    .line 317683
    :cond_35
    iget v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->s:F

    iget v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->s:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_36

    move v0, v1

    .line 317684
    goto/16 :goto_0

    .line 317685
    :cond_36
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->t:Landroid/graphics/ColorFilter;

    if-eqz v2, :cond_38

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->t:Landroid/graphics/ColorFilter;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->t:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_39

    :cond_37
    move v0, v1

    .line 317686
    goto/16 :goto_0

    .line 317687
    :cond_38
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->t:Landroid/graphics/ColorFilter;

    if-nez v2, :cond_37

    .line 317688
    :cond_39
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->u:LX/33B;

    if-eqz v2, :cond_3b

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->u:LX/33B;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->u:LX/33B;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3c

    :cond_3a
    move v0, v1

    .line 317689
    goto/16 :goto_0

    .line 317690
    :cond_3b
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->u:LX/33B;

    if-nez v2, :cond_3a

    .line 317691
    :cond_3c
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->v:Ljava/lang/String;

    if-eqz v2, :cond_3e

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->v:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->v:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3f

    :cond_3d
    move v0, v1

    .line 317692
    goto/16 :goto_0

    .line 317693
    :cond_3e
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->v:Ljava/lang/String;

    if-nez v2, :cond_3d

    .line 317694
    :cond_3f
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->w:LX/1Ai;

    if-eqz v2, :cond_41

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->w:LX/1Ai;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->w:LX/1Ai;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_42

    :cond_40
    move v0, v1

    .line 317695
    goto/16 :goto_0

    .line 317696
    :cond_41
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->w:LX/1Ai;

    if-nez v2, :cond_40

    .line 317697
    :cond_42
    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->x:LX/1f9;

    if-eqz v2, :cond_43

    iget-object v2, p0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->x:LX/1f9;

    iget-object v3, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->x:LX/1f9;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 317698
    goto/16 :goto_0

    .line 317699
    :cond_43
    iget-object v2, p1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->x:LX/1f9;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
