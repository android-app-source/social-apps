.class public Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        "LX/Aob;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static d:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0iA;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 338296
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEE_FIRST_INDICATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 338297
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 338298
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->b:Landroid/content/Context;

    .line 338299
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->c:LX/0iA;

    .line 338300
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;
    .locals 5

    .prologue
    .line 338301
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;

    monitor-enter v1

    .line 338302
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 338303
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 338304
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338305
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 338306
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;-><init>(Landroid/content/Context;LX/0iA;)V

    .line 338307
    move-object v0, p0

    .line 338308
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 338309
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 338310
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 338311
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 338312
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    .line 338313
    const/4 v1, 0x0

    .line 338314
    move-object v0, p2

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 338315
    :goto_0
    move-object v1, v0

    .line 338316
    if-eqz v1, :cond_0

    new-instance v0, LX/Aob;

    invoke-direct {v0, v1}, LX/Aob;-><init>(LX/0hs;)V

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 338317
    :cond_1
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 338318
    if-nez v0, :cond_2

    move-object v0, v1

    .line 338319
    goto :goto_0

    .line 338320
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 338321
    if-nez v0, :cond_3

    move-object v0, v1

    .line 338322
    goto :goto_0

    .line 338323
    :cond_3
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->c:LX/0iA;

    sget-object p1, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p3, LX/35R;

    invoke-virtual {v2, p1, p3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    .line 338324
    if-nez v2, :cond_4

    move-object v0, v1

    .line 338325
    goto :goto_0

    .line 338326
    :cond_4
    new-instance v1, LX/0hs;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->b:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {v1, v2, p1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 338327
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0810fc

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v0, p3, p2

    invoke-virtual {v2, p1, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 338328
    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 338329
    const/4 v2, -0x1

    .line 338330
    iput v2, v1, LX/0hs;->t:I

    .line 338331
    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 338332
    move-object v0, v1

    .line 338333
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x21d35452

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 338334
    check-cast p2, LX/Aob;

    .line 338335
    if-eqz p2, :cond_0

    iget-boolean v1, p2, LX/Aob;->a:Z

    if-eqz v1, :cond_0

    .line 338336
    const/4 v1, 0x0

    .line 338337
    iput-boolean v1, p2, LX/Aob;->a:Z

    .line 338338
    new-instance v1, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition$1;

    invoke-direct {v1, p0, p4, p2}, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition$1;-><init>(Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;Landroid/view/View;LX/Aob;)V

    invoke-virtual {p4, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 338339
    :cond_0
    const/16 v1, 0x1f

    const v2, -0xd51d504

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
