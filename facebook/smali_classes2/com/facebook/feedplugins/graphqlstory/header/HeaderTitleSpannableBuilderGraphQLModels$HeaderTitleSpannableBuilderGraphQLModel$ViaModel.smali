.class public final Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/1y5;
.implements LX/1y6;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf082c82
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 338059
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 338060
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 338061
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 338062
    return-void
.end method

.method private n()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338063
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->j:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->j:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 338064
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->j:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 338079
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 338080
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 338081
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 338082
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 338083
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->n()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 338084
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 338085
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 338086
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 338087
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 338088
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338089
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338090
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 338091
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 338092
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 338093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 338094
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 338065
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 338066
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->n()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 338067
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->n()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 338068
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->n()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 338069
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;

    .line 338070
    iput-object v0, v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->j:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 338071
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 338072
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 338073
    new-instance v0, LX/AoP;

    invoke-direct {v0, p1}, LX/AoP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338074
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 338075
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 338076
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->g:Z

    .line 338077
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->h:Z

    .line 338078
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 338056
    invoke-virtual {p2}, LX/18L;->a()V

    .line 338057
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 338058
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338037
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 338038
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 338039
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 338040
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;-><init>()V

    .line 338041
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 338042
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 338043
    const v0, 0x1823fbd9

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338054
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->f:Ljava/lang/String;

    .line 338055
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 338044
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338045
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->k:Ljava/lang/String;

    .line 338046
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 338047
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338048
    iget-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->g:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 338049
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338050
    iget-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->h:Z

    return v0
.end method

.method public final synthetic m()LX/3Bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338051
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->n()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338052
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->i:Ljava/lang/String;

    .line 338053
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->i:Ljava/lang/String;

    return-object v0
.end method
