.class public final Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1y7;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/1y5;
.implements LX/1y6;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x273a458c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 338122
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 338123
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 338124
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 338125
    return-void
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338126
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 338127
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method private p()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338128
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->m:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->m:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    .line 338129
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->m:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    return-object v0
.end method

.method private q()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338130
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->n:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->n:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    .line 338131
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->n:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 338153
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 338154
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 338155
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 338156
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 338157
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 338158
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 338159
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->p()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 338160
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->q()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 338161
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 338162
    const/16 v8, 0xb

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 338163
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 338164
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 338165
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 338166
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 338167
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338168
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338169
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 338170
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 338171
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 338172
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 338173
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 338174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 338175
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 338132
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 338133
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->p()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 338134
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->p()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    .line 338135
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->p()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 338136
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;

    .line 338137
    iput-object v0, v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->m:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    .line 338138
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->q()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 338139
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->q()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    .line 338140
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->q()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 338141
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;

    .line 338142
    iput-object v0, v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->n:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    .line 338143
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 338144
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 338145
    new-instance v0, LX/AoN;

    invoke-direct {v0, p1}, LX/AoN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338146
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 338147
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 338148
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->i:Z

    .line 338149
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->j:Z

    .line 338150
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->k:Z

    .line 338151
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 338120
    invoke-virtual {p2}, LX/18L;->a()V

    .line 338121
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 338152
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338095
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 338096
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 338097
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 338098
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;-><init>()V

    .line 338099
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 338100
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338101
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->q()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 338102
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->f:Ljava/util/List;

    .line 338103
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 338104
    const v0, -0x1443b785

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338105
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->h:Ljava/lang/String;

    .line 338106
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 338107
    const v0, 0x50c72189

    return v0
.end method

.method public final hT_()Z
    .locals 2

    .prologue
    .line 338108
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338109
    iget-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->i:Z

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338110
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->o:Ljava/lang/String;

    .line 338111
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 338112
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338113
    iget-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->j:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 338114
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 338115
    iget-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->k:Z

    return v0
.end method

.method public final synthetic m()LX/3Bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338116
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->q()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338117
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->p()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 338118
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->l:Ljava/lang/String;

    .line 338119
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->l:Ljava/lang/String;

    return-object v0
.end method
