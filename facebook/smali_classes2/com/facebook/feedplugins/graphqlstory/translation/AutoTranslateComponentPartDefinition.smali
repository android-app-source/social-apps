.class public Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7E;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7M;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field private final g:Z

.field public h:Ljava/lang/Boolean;

.field public i:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/1V0;LX/0Or;LX/0ad;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsAutoTranslateEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/C7E;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/C7M;",
            ">;",
            "LX/1V0;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267092
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 267093
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->d:LX/0Ot;

    .line 267094
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->e:LX/0Ot;

    .line 267095
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->f:LX/1V0;

    .line 267096
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->g:Z

    .line 267097
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->i:LX/0ad;

    .line 267098
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 267099
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->f:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    new-instance v3, LX/1X6;

    sget-object v1, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v3, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 267100
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 267101
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->i:LX/0ad;

    sget-short v4, LX/1Dd;->v:S

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->h:Ljava/lang/Boolean;

    .line 267102
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v1, v1

    .line 267103
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/C7M;

    const/4 v4, 0x0

    .line 267104
    new-instance v5, LX/C7J;

    invoke-direct {v5, v1}, LX/C7J;-><init>(LX/C7M;)V

    .line 267105
    iget-object p0, v1, LX/C7M;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C7K;

    .line 267106
    if-nez p0, :cond_1

    .line 267107
    new-instance p0, LX/C7K;

    invoke-direct {p0, v1}, LX/C7K;-><init>(LX/C7M;)V

    .line 267108
    :cond_1
    invoke-static {p0, p1, v4, v4, v5}, LX/C7K;->a$redex0(LX/C7K;LX/1De;IILX/C7J;)V

    .line 267109
    move-object v5, p0

    .line 267110
    move-object v4, v5

    .line 267111
    move-object v1, v4

    .line 267112
    iget-object v4, v1, LX/C7K;->a:LX/C7J;

    iput-object p2, v4, LX/C7J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267113
    iget-object v4, v1, LX/C7K;->e:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 267114
    move-object v1, v1

    .line 267115
    check-cast p3, LX/1Pr;

    .line 267116
    iget-object v4, v1, LX/C7K;->a:LX/C7J;

    iput-object p3, v4, LX/C7J;->b:LX/1Pr;

    .line 267117
    iget-object v4, v1, LX/C7K;->e:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 267118
    move-object v1, v1

    .line 267119
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, p1, v0, v3, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/C7E;

    const/4 v4, 0x0

    .line 267120
    new-instance v5, LX/C7C;

    invoke-direct {v5, v1}, LX/C7C;-><init>(LX/C7E;)V

    .line 267121
    iget-object p0, v1, LX/C7E;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C7D;

    .line 267122
    if-nez p0, :cond_3

    .line 267123
    new-instance p0, LX/C7D;

    invoke-direct {p0, v1}, LX/C7D;-><init>(LX/C7E;)V

    .line 267124
    :cond_3
    invoke-static {p0, p1, v4, v4, v5}, LX/C7D;->a$redex0(LX/C7D;LX/1De;IILX/C7C;)V

    .line 267125
    move-object v5, p0

    .line 267126
    move-object v4, v5

    .line 267127
    move-object v1, v4

    .line 267128
    iget-object v4, v1, LX/C7D;->a:LX/C7C;

    iput-object p2, v4, LX/C7C;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267129
    iget-object v4, v1, LX/C7D;->e:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 267130
    move-object v1, v1

    .line 267131
    check-cast p3, LX/1Pr;

    .line 267132
    iget-object v4, v1, LX/C7D;->a:LX/C7C;

    iput-object p3, v4, LX/C7C;->b:LX/1Pr;

    .line 267133
    iget-object v4, v1, LX/C7D;->e:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 267134
    move-object v1, v1

    .line 267135
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;
    .locals 10

    .prologue
    .line 267136
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;

    monitor-enter v1

    .line 267137
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267138
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267139
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267140
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267141
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x1f88

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1f8a

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    const/16 v8, 0x1496

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/1V0;LX/0Or;LX/0ad;)V

    .line 267142
    move-object v0, v3

    .line 267143
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267144
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267145
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 267147
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 267148
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 267149
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 267150
    iget-boolean v0, p0, Lcom/facebook/feedplugins/graphqlstory/translation/AutoTranslateComponentPartDefinition;->g:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 267151
    :goto_0
    return v0

    .line 267152
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267153
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 267154
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    .line 267155
    if-nez v0, :cond_1

    move v0, v1

    .line 267156
    goto :goto_0

    .line 267157
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v2

    .line 267158
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v0

    .line 267159
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v2, v3, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTranslation;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 267160
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 267161
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 267162
    check-cast v0, LX/0jW;

    return-object v0
.end method
