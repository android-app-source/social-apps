.class public Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C79;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field private final g:LX/1VH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 265681
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/1VH;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1V0;",
            "LX/1VH;",
            "LX/0Ot",
            "<",
            "LX/C79;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265722
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 265723
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->f:LX/1V0;

    .line 265724
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->g:LX/1VH;

    .line 265725
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->e:LX/0Ot;

    .line 265726
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 265703
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C79;

    const/4 v1, 0x0

    .line 265704
    new-instance v2, LX/C78;

    invoke-direct {v2, v0}, LX/C78;-><init>(LX/C79;)V

    .line 265705
    iget-object v3, v0, LX/C79;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C77;

    .line 265706
    if-nez v3, :cond_0

    .line 265707
    new-instance v3, LX/C77;

    invoke-direct {v3, v0}, LX/C77;-><init>(LX/C79;)V

    .line 265708
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C77;->a$redex0(LX/C77;LX/1De;IILX/C78;)V

    .line 265709
    move-object v2, v3

    .line 265710
    move-object v1, v2

    .line 265711
    move-object v0, v1

    .line 265712
    iget-object v1, v0, LX/C77;->a:LX/C78;

    iput-object p2, v1, LX/C78;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265713
    iget-object v1, v0, LX/C77;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 265714
    move-object v1, v0

    .line 265715
    move-object v0, p3

    check-cast v0, LX/1Pp;

    .line 265716
    iget-object v2, v1, LX/C77;->a:LX/C78;

    iput-object v0, v2, LX/C78;->b:LX/1Pp;

    .line 265717
    iget-object v2, v1, LX/C77;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 265718
    move-object v0, v1

    .line 265719
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 265720
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v3

    invoke-virtual {v3}, LX/1UY;->i()LX/1Ua;

    move-result-object v3

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 265721
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->g:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .locals 7

    .prologue
    .line 265692
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    monitor-enter v1

    .line 265693
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265694
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265695
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265696
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265697
    new-instance v6, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v5

    check-cast v5, LX/1VH;

    const/16 p0, 0x1f86

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/1VH;LX/0Ot;)V

    .line 265698
    move-object v0, v6

    .line 265699
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265700
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265701
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 265689
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265690
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265691
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/0x1;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 265688
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 265687
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265685
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265686
    invoke-static {p1}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 265683
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265684
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 265682
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
