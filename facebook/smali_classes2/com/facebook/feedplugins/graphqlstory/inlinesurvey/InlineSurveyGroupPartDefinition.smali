.class public Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/17Q;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 258973
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 258974
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a:LX/0Zb;

    .line 258975
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->b:LX/17Q;

    .line 258976
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->c:LX/0Ot;

    .line 258977
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->d:LX/0Ot;

    .line 258978
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->e:LX/0Ot;

    .line 258979
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;
    .locals 9

    .prologue
    .line 258962
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    monitor-enter v1

    .line 258963
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 258964
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 258965
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258966
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 258967
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    const/16 v6, 0x91a

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x918

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x917

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;-><init>(LX/0Zb;LX/17Q;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 258968
    move-object v0, v3

    .line 258969
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 258970
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258971
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 258972
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)Ljava/lang/Void;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "LX/1Pf;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Pf;",
            ")",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 258947
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 258948
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 258949
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 258950
    new-instance v2, LX/C6X;

    invoke-direct {v2, v0}, LX/C6X;-><init>(LX/0jW;)V

    invoke-interface {p3, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/C6a;

    .line 258951
    new-instance v2, LX/C6E;

    invoke-direct {v2, v0}, LX/C6E;-><init>(LX/0jW;)V

    invoke-interface {p3, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/C6H;

    .line 258952
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 258953
    :goto_0
    iput-boolean v0, v7, LX/C6a;->a:Z

    .line 258954
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v0

    .line 258955
    iput-object v0, v7, LX/C6a;->j:Ljava/lang/String;

    .line 258956
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v0, LX/C6Y;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->o()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al()Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak()LX/0Px;

    move-result-object v6

    move-object v2, p2

    invoke-direct/range {v0 .. v8}, LX/C6Y;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/C6a;LX/C6H;)V

    invoke-virtual {p1, v9, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 258957
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v0, LX/C6Y;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L()LX/0Px;

    move-result-object v6

    move v1, v10

    move-object v2, p2

    move-object v4, v11

    invoke-direct/range {v0 .. v8}, LX/C6Y;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/C6a;LX/C6H;)V

    invoke-virtual {p1, v9, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 258958
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v1, LX/C6N;

    invoke-direct {v1, p2, v7, v8}, LX/C6N;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C6a;LX/C6H;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 258959
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v1, LX/C6F;

    invoke-direct {v1, p2, v8}, LX/C6F;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C6H;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 258960
    return-object v11

    :cond_0
    move v0, v10

    .line 258961
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 258941
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 258942
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 258943
    invoke-static {p1}, LX/1VY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 258944
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-eqz v0, :cond_0

    .line 258945
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 258946
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/0x1;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
