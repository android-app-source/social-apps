.class public Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1W0;",
        "Landroid/text/SpannableStringBuilder;",
        "TE;",
        "LX/C7B;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 265607
    new-instance v0, LX/1Vd;

    invoke-direct {v0}, LX/1Vd;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265608
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 265609
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->b:Landroid/content/res/Resources;

    .line 265610
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->c:LX/0Ot;

    .line 265611
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;
    .locals 5

    .prologue
    .line 265612
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    monitor-enter v1

    .line 265613
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265614
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265615
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265616
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265617
    new-instance v4, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const/16 p0, 0xa71

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;-><init>(Landroid/content/res/Resources;LX/0Ot;)V

    .line 265618
    move-object v0, v4

    .line 265619
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265620
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265621
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 265623
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265624
    check-cast p2, LX/1W0;

    check-cast p3, LX/1Pq;

    const/4 v6, 0x0

    .line 265625
    iget-object v2, p2, LX/1W0;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, p3

    .line 265626
    check-cast v0, LX/1Pr;

    new-instance v1, LX/1W1;

    invoke-direct {v1, v2}, LX/1W1;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W2;

    .line 265627
    iget-object v1, v0, LX/1W2;->b:Ljava/lang/String;

    move-object v1, v1

    .line 265628
    if-nez v1, :cond_0

    .line 265629
    const/4 v0, 0x0

    .line 265630
    :goto_0
    return-object v0

    .line 265631
    :cond_0
    iget-boolean v1, v0, LX/1W2;->c:Z

    move v1, v1

    .line 265632
    if-eqz v1, :cond_1

    const v1, 0x7f081a23

    .line 265633
    :goto_1
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->b:Landroid/content/res/Resources;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 265634
    iget-object v5, v0, LX/1W2;->a:Ljava/lang/String;

    move-object v5, v5

    .line 265635
    aput-object v5, v4, v6

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 265636
    new-instance v3, Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->b:Landroid/content/res/Resources;

    const v5, 0x7f080032

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 265637
    new-instance v4, LX/C76;

    invoke-direct {v4, p0, v2, v0, p3}, LX/C76;-><init>(Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1W2;LX/1Pq;)V

    .line 265638
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const/16 v2, 0x12

    invoke-virtual {v3, v4, v6, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 265639
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0

    .line 265640
    :cond_1
    const v1, 0x7f081a22

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5353d1bd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 265641
    check-cast p2, Landroid/text/SpannableStringBuilder;

    check-cast p4, LX/C7B;

    .line 265642
    if-eqz p2, :cond_0

    .line 265643
    invoke-virtual {p4, p2}, LX/C7B;->setTitle(Ljava/lang/CharSequence;)V

    .line 265644
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x19bb0fd5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 265645
    check-cast p1, LX/1W0;

    .line 265646
    iget-object v0, p1, LX/1W0;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 265647
    iget-object v1, p1, LX/1W0;->b:LX/1Pr;

    new-instance v2, LX/1W1;

    invoke-direct {v2, v0}, LX/1W1;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W2;

    .line 265648
    iget-boolean v1, v0, LX/1W2;->d:Z

    move v0, v1

    .line 265649
    return v0
.end method
