.class public Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/20d;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 256725
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 256726
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->b:LX/0Ot;

    .line 256727
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a:LX/0Ot;

    .line 256728
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .locals 5

    .prologue
    .line 256729
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    monitor-enter v1

    .line 256730
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 256731
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 256732
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256733
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 256734
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    const/16 v4, 0x855

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 256735
    move-object v0, v3

    .line 256736
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 256737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 256739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 256740
    check-cast p2, LX/20d;

    .line 256741
    iget-object v0, p2, LX/20d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 256742
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 256743
    move-object v3, v0

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 256744
    iget-object v0, p2, LX/20d;->b:LX/1Wi;

    if-nez v0, :cond_0

    .line 256745
    const-string v0, "Null_Footer_Level"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Debug info: \n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    const/4 v1, 0x1

    .line 256746
    iput-boolean v1, v0, LX/0VK;->d:Z

    .line 256747
    move-object v0, v0

    .line 256748
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 256749
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 256750
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/1Nt;

    new-instance v0, LX/21P;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v2

    invoke-static {v3}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    iget-object v4, p2, LX/20d;->b:LX/1Wi;

    iget-object v5, p2, LX/20d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct/range {v0 .. v5}, LX/21P;-><init>(ZZZLX/1Wi;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 256751
    const/4 v0, 0x0

    return-object v0
.end method
