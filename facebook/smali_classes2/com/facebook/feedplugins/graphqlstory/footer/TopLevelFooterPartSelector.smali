.class public Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/1Wt;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1WZ;


# direct methods
.method public constructor <init>(LX/1WZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269085
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 269086
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a:LX/1WZ;

    .line 269087
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;
    .locals 4

    .prologue
    .line 269088
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    monitor-enter v1

    .line 269089
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269090
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269091
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269092
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269093
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-static {v0}, LX/1WZ;->a(LX/0QB;)LX/1WZ;

    move-result-object v3

    check-cast v3, LX/1WZ;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;-><init>(LX/1WZ;)V

    .line 269094
    move-object v0, p0

    .line 269095
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269096
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269097
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269098
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 269099
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 269100
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a:LX/1WZ;

    invoke-virtual {v0, p1, p2, p3}, LX/1WZ;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1Wt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 269101
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 269102
    invoke-static {p1}, LX/1WZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
