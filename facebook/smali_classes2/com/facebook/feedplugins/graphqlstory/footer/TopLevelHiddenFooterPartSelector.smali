.class public Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/1Wt;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WZ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Wa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1WZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Wa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268976
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 268977
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->a:LX/0Ot;

    .line 268978
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->b:LX/0Ot;

    .line 268979
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;
    .locals 5

    .prologue
    .line 268965
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;

    monitor-enter v1

    .line 268966
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268967
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268968
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268969
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268970
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;

    const/16 v4, 0x8f6

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x8fe

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;-><init>(LX/0Ot;LX/0Ot;)V

    .line 268971
    move-object v0, v3

    .line 268972
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268973
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268974
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268975
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 268963
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 268964
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1WZ;

    invoke-virtual {v0, p1, p2, p3}, LX/1WZ;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1Wt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 11

    .prologue
    .line 268929
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1Wt;

    check-cast p3, LX/1Pf;

    .line 268930
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V

    .line 268931
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Wa;

    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d2938

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 268932
    if-eqz v1, :cond_0

    iget-object v3, v0, LX/1Wa;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Wc;

    invoke-virtual {v3}, LX/1Wc;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, LX/1Wt;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 268933
    :cond_0
    :goto_0
    return-void

    .line 268934
    :cond_1
    iget-object v3, p2, LX/1Wt;->a:LX/1Wu;

    move-object v3, v3

    .line 268935
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00a6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 268936
    sget-object v4, LX/1Wu;->PLAYED:LX/1Wu;

    if-eq v3, v4, :cond_2

    iget-object v3, v0, LX/1Wa;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1VF;

    invoke-virtual {v3, p1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, LX/1Wa;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1VF;

    invoke-virtual {v3, p1}, LX/1VF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 268937
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 268938
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 268939
    :cond_3
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v3, v4, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 268940
    :cond_4
    new-instance v4, LX/C5o;

    iget-object v3, v0, LX/1Wa;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Wc;

    .line 268941
    iget-object v5, v3, LX/1Wc;->e:LX/0am;

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-nez v5, :cond_5

    .line 268942
    iget-object v5, v3, LX/1Wc;->a:LX/0ad;

    sget v6, LX/0fe;->k:F

    const/4 v8, 0x0

    invoke-interface {v5, v6, v8}, LX/0ad;->a(FF)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    iput-object v5, v3, LX/1Wc;->e:LX/0am;

    .line 268943
    :cond_5
    iget-object v5, v3, LX/1Wc;->e:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    move v8, v5

    .line 268944
    iget-object v3, v0, LX/1Wa;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Wc;

    invoke-virtual {v3}, LX/1Wc;->c()J

    move-result-wide v9

    move-object v5, p2

    move-object v6, v1

    invoke-direct/range {v4 .. v10}, LX/C5o;-><init>(LX/1Wt;Landroid/view/View;IFJ)V

    .line 268945
    iget-object v3, v0, LX/1Wa;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Wc;

    .line 268946
    iget-object v5, v3, LX/1Wc;->f:LX/0am;

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-nez v5, :cond_6

    .line 268947
    iget-object v5, v3, LX/1Wc;->a:LX/0ad;

    sget-short v6, LX/0fe;->j:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    iput-object v5, v3, LX/1Wc;->f:LX/0am;

    .line 268948
    :cond_6
    iget-object v5, v3, LX/1Wc;->f:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move v3, v5

    .line 268949
    if-eqz v3, :cond_7

    .line 268950
    iget-object v3, v0, LX/1Wa;->e:LX/1Wb;

    .line 268951
    new-instance v5, LX/C67;

    invoke-direct {v5, v4}, LX/C67;-><init>(LX/C5o;)V

    .line 268952
    const/16 v6, 0x1f71

    invoke-static {v3, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    .line 268953
    iput-object v6, v5, LX/C67;->c:LX/0Ot;

    .line 268954
    move-object v5, v5

    .line 268955
    iget-object v3, v0, LX/1Wa;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Sy;

    .line 268956
    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, p2, LX/1Wt;->c:Ljava/lang/ref/WeakReference;

    .line 268957
    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, p2, LX/1Wt;->d:Ljava/lang/ref/WeakReference;

    .line 268958
    iget-object v3, v0, LX/1Wa;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Sy;

    invoke-virtual {v3, v5}, LX/0Sy;->a(LX/0Vq;)V

    .line 268959
    move-object v3, v5

    .line 268960
    move-object v6, v3

    .line 268961
    :goto_1
    iget-object v3, v0, LX/1Wa;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C5p;

    iget-object v5, v0, LX/1Wa;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Wc;

    invoke-virtual {v5}, LX/1Wc;->b()J

    move-result-wide v7

    invoke-virtual {v3, v4, v6, v7, v8}, LX/C5p;->a(LX/C5o;LX/C67;J)V

    goto/16 :goto_0

    .line 268962
    :cond_7
    const/4 v3, 0x0

    move-object v6, v3

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 268912
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268913
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/1WZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Wa;

    .line 268914
    iget-object p0, v0, LX/1Wa;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1Wc;

    invoke-virtual {p0}, LX/1Wc;->a()Z

    move-result p0

    move v0, p0

    .line 268915
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 3

    .prologue
    .line 268916
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1Wt;

    check-cast p3, LX/1Pf;

    .line 268917
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V

    .line 268918
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Wa;

    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d2938

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 268919
    if-eqz v1, :cond_0

    invoke-virtual {p2}, LX/1Wt;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 268920
    :cond_0
    :goto_0
    return-void

    .line 268921
    :cond_1
    iget-object v2, v0, LX/1Wa;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C5p;

    .line 268922
    iget-object p0, p2, LX/1Wt;->b:Ljava/lang/ref/WeakReference;

    if-eqz p0, :cond_4

    iget-object p0, p2, LX/1Wt;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Runnable;

    :goto_1
    move-object v0, p0

    .line 268923
    if-eqz v0, :cond_2

    .line 268924
    iget-object p0, v2, LX/C5p;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0Sh;

    invoke-virtual {p0, v0}, LX/0Sh;->c(Ljava/lang/Runnable;)V

    .line 268925
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 268926
    invoke-virtual {p2}, LX/1Wt;->a()Z

    move-result p0

    if-eqz p0, :cond_3

    .line 268927
    invoke-virtual {p2}, LX/1Wt;->e()V

    .line 268928
    :cond_3
    goto :goto_0

    :cond_4
    const/4 p0, 0x0

    goto :goto_1
.end method
