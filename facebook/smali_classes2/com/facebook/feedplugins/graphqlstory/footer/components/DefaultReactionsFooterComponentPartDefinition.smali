.class public Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static n:LX/0Xm;


# instance fields
.field private final e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1We;

.field private final g:LX/1Wm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Wm",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final h:LX/1Wn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Wn",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20g",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;

.field public final l:LX/0ad;

.field public m:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 269243
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/1We;LX/1Wm;LX/1Wn;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/0Ot;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;",
            "LX/1We;",
            "LX/1Wm;",
            "LX/1Wn;",
            "Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;",
            "LX/0Ot",
            "<",
            "LX/20g;",
            ">;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269233
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 269234
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    .line 269235
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->f:LX/1We;

    .line 269236
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->g:LX/1Wm;

    .line 269237
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->h:LX/1Wn;

    .line 269238
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->i:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    .line 269239
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->j:LX/0Ot;

    .line 269240
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;

    .line 269241
    iput-object p9, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->l:LX/0ad;

    .line 269242
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 269229
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->f:LX/1We;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-object v0, p3

    check-cast v0, LX/1Pr;

    invoke-virtual {v2, p2, v0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v0

    .line 269230
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->i:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/1Wj;)I

    move-result v1

    .line 269231
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->h:LX/1Wn;

    invoke-virtual {v2, p1}, LX/1Wn;->c(LX/1De;)LX/39X;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/39X;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39X;

    move-result-object v2

    check-cast p3, LX/1Po;

    invoke-virtual {v2, p3}, LX/39X;->a(LX/1Po;)LX/39X;

    move-result-object v2

    iget-object v3, v0, LX/1Wj;->f:LX/1Wk;

    invoke-virtual {v2, v3}, LX/39X;->a(LX/1Wk;)LX/39X;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/39X;->h(I)LX/39X;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 269232
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->g:LX/1Wm;

    invoke-virtual {v2, p1}, LX/1Wm;->c(LX/1De;)LX/39Z;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/39Z;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39Z;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/39Z;->a(LX/1Wj;)LX/39Z;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/39Z;->a(LX/1X1;)LX/39Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;
    .locals 13

    .prologue
    .line 269218
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;

    monitor-enter v1

    .line 269219
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269220
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269221
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269222
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269223
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    invoke-static {v0}, LX/1We;->a(LX/0QB;)LX/1We;

    move-result-object v6

    check-cast v6, LX/1We;

    invoke-static {v0}, LX/1Wm;->a(LX/0QB;)LX/1Wm;

    move-result-object v7

    check-cast v7, LX/1Wm;

    invoke-static {v0}, LX/1Wn;->a(LX/0QB;)LX/1Wn;

    move-result-object v8

    check-cast v8, LX/1Wn;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    const/16 v10, 0x8ad

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/1We;LX/1Wm;LX/1Wn;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/0Ot;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;LX/0ad;)V

    .line 269224
    move-object v0, v3

    .line 269225
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269226
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269227
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269228
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 269217
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 269216
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 269211
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269212
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->m:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 269213
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->l:LX/0ad;

    sget-short v1, LX/1Dd;->w:S

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->m:Ljava/lang/Boolean;

    .line 269214
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 269215
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 269210
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 269207
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 269208
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 269209
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
