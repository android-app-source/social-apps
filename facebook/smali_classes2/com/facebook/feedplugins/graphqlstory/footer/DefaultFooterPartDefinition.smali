.class public Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/1VF;

.field private final d:LX/14w;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/1VF;LX/14w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;",
            ">;",
            "LX/1VF;",
            "LX/14w;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269244
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 269245
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a:LX/0Ot;

    .line 269246
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->b:LX/0Ot;

    .line 269247
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->c:LX/1VF;

    .line 269248
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->d:LX/14w;

    .line 269249
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;
    .locals 7

    .prologue
    .line 269250
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    monitor-enter v1

    .line 269251
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269252
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269253
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269254
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269255
    new-instance v5, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    const/16 v3, 0x8f2

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x8f1

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v3

    check-cast v3, LX/1VF;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-direct {v5, v6, p0, v3, v4}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/1VF;LX/14w;)V

    .line 269256
    move-object v0, v5

    .line 269257
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269258
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269259
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269260
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269261
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1Wi;"
        }
    .end annotation

    .prologue
    .line 269262
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->c:LX/1VF;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->d:LX/14w;

    invoke-static {p1, p2, v0, v1}, LX/1Wp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;LX/1VF;LX/14w;)LX/1Wi;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 269263
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 269264
    invoke-virtual {p0, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;

    move-result-object v1

    .line 269265
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 269266
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/20d;

    invoke-direct {v2, p2, v1}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 269267
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 269268
    invoke-static {p1}, LX/14w;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 269269
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
