.class public Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3Vs;",
        "E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1PV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0tJ;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1VF;


# direct methods
.method public constructor <init>(LX/0tJ;LX/0Ot;LX/0Ot;LX/0Ot;LX/1VF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tJ;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;",
            ">;",
            "LX/1VF;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268538
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 268539
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->a:LX/0tJ;

    .line 268540
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->b:LX/0Ot;

    .line 268541
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->c:LX/0Ot;

    .line 268542
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->d:LX/0Ot;

    .line 268543
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->e:LX/1VF;

    .line 268544
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;
    .locals 9

    .prologue
    .line 268545
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;

    monitor-enter v1

    .line 268546
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268547
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268548
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268549
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268550
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;

    invoke-static {v0}, LX/0tJ;->a(LX/0QB;)LX/0tJ;

    move-result-object v4

    check-cast v4, LX/0tJ;

    const/16 v5, 0x9a8

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x756

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1f6f

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v8

    check-cast v8, LX/1VF;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;-><init>(LX/0tJ;LX/0Ot;LX/0Ot;LX/0Ot;LX/1VF;)V

    .line 268551
    move-object v0, v3

    .line 268552
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 268553
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268554
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 268555
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 268556
    sget-object v0, LX/3Vq;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 268557
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268558
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268559
    const v1, 0x7f0d1101

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v1, v0, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 268560
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268561
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268562
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 268563
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 268564
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    .line 268565
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 268566
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 268567
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268568
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->a:LX/0tJ;

    invoke-virtual {v0}, LX/0tJ;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;->e:LX/1VF;

    .line 268569
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 268570
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 268571
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 268572
    :cond_0
    const/4 v1, 0x0

    .line 268573
    :goto_0
    move v0, v1

    .line 268574
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 268575
    :cond_2
    invoke-virtual {v0, p1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 268576
    const/4 v1, 0x1

    goto :goto_0

    .line 268577
    :cond_3
    invoke-static {p1}, LX/14w;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 268578
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 268579
    :goto_2
    move-object v2, v2

    .line 268580
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-static {v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 268581
    :cond_4
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 268582
    :goto_3
    move-object v1, v3

    .line 268583
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 268584
    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_5
    move v3, v6

    .line 268585
    :goto_4
    move v1, v3

    .line 268586
    goto :goto_0

    .line 268587
    :cond_6
    invoke-static {p1}, LX/14w;->u(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v4

    .line 268588
    if-eqz v4, :cond_7

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 268589
    :cond_7
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 268590
    goto :goto_2

    .line 268591
    :cond_8
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 268592
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v6, :cond_a

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLComment;

    .line 268593
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 268594
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 268595
    :cond_9
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 268596
    :cond_a
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;->a()LX/0Px;

    move-result-object v3

    goto :goto_3

    .line 268597
    :cond_c
    if-eqz v2, :cond_d

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_d
    move v3, v5

    .line 268598
    goto :goto_4

    .line 268599
    :cond_e
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_f
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLActor;

    .line 268600
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLActor;

    .line 268601
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_10

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    move v3, v6

    .line 268602
    :goto_6
    if-eqz v3, :cond_f

    move v3, v5

    .line 268603
    goto/16 :goto_4

    :cond_11
    move v3, v6

    .line 268604
    goto/16 :goto_4

    :cond_12
    move v3, v5

    goto :goto_6
.end method
