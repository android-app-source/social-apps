.class public Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/14w;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition",
            "<*",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WR;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1VK;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/14w;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1VK;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gfreactionsblingbar/ui/GFReactionsBlingBarPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithProfileVideoCountPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryPillsBlingBarPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1WR;",
            ">;",
            "LX/1VK;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0oJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265843
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265844
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->b:LX/0Ot;

    .line 265845
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->c:LX/0Ot;

    .line 265846
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->d:LX/0Ot;

    .line 265847
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->e:LX/0Ot;

    .line 265848
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->f:LX/0Ot;

    .line 265849
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a:LX/14w;

    .line 265850
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->g:LX/0Ot;

    .line 265851
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->h:LX/1VK;

    .line 265852
    iput-object p9, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->i:LX/0Ot;

    .line 265853
    iput-object p10, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->j:LX/0Ot;

    .line 265854
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;
    .locals 14

    .prologue
    .line 265900
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    monitor-enter v1

    .line 265901
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265902
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265903
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265904
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265905
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    const/16 v5, 0x9a9

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x8c5

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x9a7

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xa5e

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x8ef

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x79b

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, LX/1VK;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/1VK;

    const/16 v12, 0x20a4

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xad0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;-><init>(LX/14w;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1VK;LX/0Ot;LX/0Ot;)V

    .line 265906
    move-object v0, v3

    .line 265907
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265908
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265909
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265910
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 265857
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v3, 0x0

    .line 265858
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 265859
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265860
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1WR;

    .line 265861
    iget-object v4, v1, LX/1WR;->a:LX/0if;

    .line 265862
    sget-object v5, LX/0ig;->l:LX/0ih;

    move-object v5, v5

    .line 265863
    invoke-static {v1, v0}, LX/1WR;->h(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;)S

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, LX/0if;->a(LX/0ih;J)V

    .line 265864
    iget-object v4, v1, LX/1WR;->a:LX/0if;

    .line 265865
    sget-object v8, LX/0ig;->l:LX/0ih;

    move-object v8, v8

    .line 265866
    invoke-static {v1, v0}, LX/1WR;->h(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;)S

    move-result v9

    int-to-long v10, v9

    .line 265867
    iget-object v9, v1, LX/1WR;->c:LX/0tJ;

    invoke-virtual {v9}, LX/0tJ;->a()Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "QE_TEST_GROUP"

    :goto_0
    move-object v9, v9

    .line 265868
    invoke-virtual {v4, v8, v10, v11, v9}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 265869
    sget-object v8, LX/0ig;->l:LX/0ih;

    move-object v8, v8

    .line 265870
    invoke-static {v1, v0}, LX/1WR;->h(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;)S

    move-result v9

    int-to-long v10, v9

    .line 265871
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v9

    invoke-static {v9}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v9, 0x1

    .line 265872
    :goto_1
    if-eqz v9, :cond_3

    const-string v9, "REAL_TIME_ACTIVITY_INFO_PRESENT"

    :goto_2
    move-object v9, v9

    .line 265873
    invoke-virtual {v4, v8, v10, v11, v9}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 265874
    const/4 v2, 0x0

    .line 265875
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 265876
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265877
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 265878
    if-nez v4, :cond_4

    move v1, v2

    .line 265879
    :goto_3
    move v1, v1

    .line 265880
    if-eqz v1, :cond_0

    .line 265881
    :goto_4
    return-object v3

    .line 265882
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->f:LX/0Ot;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->c:LX/0Ot;

    invoke-virtual {v1, v2, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->d:LX/0Ot;

    invoke-virtual {v1, v2, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    .line 265883
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0oJ;

    .line 265884
    sget-short v4, LX/0ob;->I:S

    sget-short v5, LX/0ob;->w:S

    const/4 v6, 0x0

    invoke-static {v2, v4, v5, v6}, LX/0oJ;->a(LX/0oJ;SSZ)Z

    move-result v4

    move v2, v4

    .line 265885
    if-eqz v2, :cond_8

    .line 265886
    invoke-static {v0}, LX/0sa;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a:LX/14w;

    invoke-virtual {v2, v0}, LX/14w;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_5
    move v2, v2

    .line 265887
    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_6
    move v0, v2

    .line 265888
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->i:LX/0Ot;

    invoke-virtual {v1, v0, v2, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->e:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->b:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    goto :goto_4

    :cond_1
    const-string v9, "QE_CONTROL_GROUP"

    goto :goto_0

    .line 265889
    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    .line 265890
    :cond_3
    const-string v9, "REAL_TIME_ACTIVITY_INFO_ABSENT"

    goto :goto_2

    .line 265891
    :cond_4
    invoke-static {v1}, LX/14w;->o(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-nez v5, :cond_5

    move v1, v2

    .line 265892
    goto :goto_3

    .line 265893
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 265894
    if-nez v4, :cond_6

    move v1, v2

    .line 265895
    goto :goto_3

    .line 265896
    :cond_6
    new-instance v5, LX/2oK;

    iget-object v6, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->h:LX/1VK;

    invoke-direct {v5, p2, v4, v6}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 265897
    invoke-interface {p3, v5, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2oL;

    .line 265898
    iget-object v4, v1, LX/2oL;->h:LX/2oN;

    move-object v1, v4

    .line 265899
    sget-object v4, LX/2oN;->NONE:LX/2oN;

    if-eq v1, v4, :cond_7

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_7
    move v1, v2

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x0

    goto :goto_6

    :cond_9
    const/4 v2, 0x0

    goto :goto_5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265855
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265856
    invoke-static {p1}, LX/14w;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
