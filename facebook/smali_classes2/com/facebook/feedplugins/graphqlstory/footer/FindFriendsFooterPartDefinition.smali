.class public Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 269133
    const-string v0, "faceweb/f?href=%2Ffindfriends%2Fbrowser%2F"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;)V
    .locals 0
    .param p2    # LX/0Ot;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269126
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 269127
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->e:LX/0Ot;

    .line 269128
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->b:LX/0Ot;

    .line 269129
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->c:LX/0Ot;

    .line 269130
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->d:LX/0Ot;

    .line 269131
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->f:Landroid/content/Context;

    .line 269132
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;
    .locals 9

    .prologue
    .line 269115
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;

    monitor-enter v1

    .line 269116
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269117
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269118
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269119
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269120
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;

    const/16 v4, 0x85e

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v5

    const/16 v6, 0xbc6

    invoke-static {v5, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xe0f

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xe17

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;)V

    .line 269121
    move-object v0, v3

    .line 269122
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269123
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269124
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269125
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 269114
    sget-object v0, LX/3VK;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 269103
    const/4 v4, 0x0

    .line 269104
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 269105
    const v1, 0x7f0d175f

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->f:Landroid/content/Context;

    const v3, 0x7f080faa

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 269106
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/C5k;

    invoke-direct {v1, p0}, LX/C5k;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 269107
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 269108
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 269109
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    .line 269110
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 269111
    :goto_0
    return v0

    .line 269112
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 269113
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x59ea29ec

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
