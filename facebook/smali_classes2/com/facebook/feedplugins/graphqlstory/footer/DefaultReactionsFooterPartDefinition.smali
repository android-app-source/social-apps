.class public Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/14w;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/0Ot;LX/14w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;",
            ">;",
            "LX/14w;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269625
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 269626
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->a:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    .line 269627
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    .line 269628
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->c:LX/0Ot;

    .line 269629
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->d:LX/14w;

    .line 269630
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;
    .locals 7

    .prologue
    .line 269631
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;

    monitor-enter v1

    .line 269632
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269633
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269634
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269635
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269636
    new-instance v6, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    const/16 v5, 0x8f1

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    invoke-direct {v6, v3, v4, p0, v5}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/0Ot;LX/14w;)V

    .line 269637
    move-object v0, v6

    .line 269638
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269639
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269640
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269641
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269642
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 269643
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    .line 269644
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->a:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 269645
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/20d;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    check-cast p3, LX/1Pr;

    invoke-virtual {v2, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;

    move-result-object v2

    invoke-direct {v1, p2, v2}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 269646
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 269647
    invoke-static {p1}, LX/14w;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 269648
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->a:Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;

    invoke-virtual {v1, p1}, Lcom/facebook/feedplugins/feedbackreactions/ReactionsFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    .line 269649
    const/4 p0, 0x0

    invoke-virtual {v1, p1, p0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;

    move-result-object p0

    move-object v1, p0

    .line 269650
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 269651
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
