.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/0tN;

.field private final e:LX/1VF;

.field private final f:LX/1Ve;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/0tN;LX/1VF;LX/1Ve;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/0tN;",
            "LX/1VF;",
            "LX/1Ve;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265992
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265993
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a:LX/0Ot;

    .line 265994
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->b:LX/0Ot;

    .line 265995
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->c:Landroid/content/res/Resources;

    .line 265996
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->d:LX/0tN;

    .line 265997
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->e:LX/1VF;

    .line 265998
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->f:LX/1Ve;

    .line 265999
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;
    .locals 10

    .prologue
    .line 265981
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    monitor-enter v1

    .line 265982
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265983
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265984
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265985
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265986
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    const/16 v4, 0x90c

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x90a

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0tN;->a(LX/0QB;)LX/0tN;

    move-result-object v7

    check-cast v7, LX/0tN;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v8

    check-cast v8, LX/1VF;

    invoke-static {v0}, LX/1Ve;->a(LX/0QB;)LX/1Ve;

    move-result-object v9

    check-cast v9, LX/1Ve;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;-><init>(LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/0tN;LX/1VF;LX/1Ve;)V

    .line 265987
    move-object v0, v3

    .line 265988
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 265957
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v1, 0x0

    .line 265958
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->f:LX/1Ve;

    .line 265959
    const/4 v3, 0x0

    .line 265960
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 265961
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 265962
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 265963
    if-nez v4, :cond_2

    move v2, v3

    .line 265964
    :goto_0
    move v2, v2

    .line 265965
    move v0, v2

    .line 265966
    if-eqz v0, :cond_0

    .line 265967
    :goto_1
    return-object v1

    .line 265968
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->d:LX/0tN;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->e:LX/1VF;

    invoke-static {v0, v2, p2}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->a(LX/0tN;LX/1VF;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    .line 265969
    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->c:Landroid/content/res/Resources;

    invoke-static {p2, v0, p3}, LX/35G;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/res/Resources;LX/1Pr;)LX/35L;

    move-result-object v0

    .line 265970
    :goto_2
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->b:LX/0Ot;

    invoke-static {p1, v2, v3, v0}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 265971
    goto :goto_2

    .line 265972
    :cond_2
    invoke-static {v2}, LX/14w;->o(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-nez v5, :cond_3

    move v2, v3

    .line 265973
    goto :goto_0

    .line 265974
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 265975
    if-nez v4, :cond_4

    move v2, v3

    .line 265976
    goto :goto_0

    .line 265977
    :cond_4
    new-instance v5, LX/2oK;

    iget-object v6, v0, LX/1Ve;->b:LX/1VK;

    invoke-direct {v5, p2, v4, v6}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 265978
    invoke-interface {p3, v5, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2oL;

    .line 265979
    iget-object v4, v2, LX/2oL;->h:LX/2oN;

    move-object v2, v4

    .line 265980
    sget-object v4, LX/2oN;->NONE:LX/2oN;

    if-eq v2, v4, :cond_5

    const/4 v2, 0x1

    goto :goto_0

    :cond_5
    move v2, v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 265953
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 265954
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->e:LX/1VF;

    invoke-virtual {v0, p1}, LX/1VF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->d:LX/0tN;

    .line 265955
    iget-object v1, v0, LX/0tN;->a:LX/0ad;

    sget-short p0, LX/0wl;->d:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 265956
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
