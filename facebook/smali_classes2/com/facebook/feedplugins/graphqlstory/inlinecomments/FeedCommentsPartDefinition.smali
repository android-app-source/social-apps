.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1PV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/1VF;

.field private final b:LX/14w;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1JR;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bo0;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1VF;LX/14w;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1VF;",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1JR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Bo0;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 265911
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 265912
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a:LX/1VF;

    .line 265913
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->b:LX/14w;

    .line 265914
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->c:LX/0Ot;

    .line 265915
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->d:LX/0Ot;

    .line 265916
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->e:LX/0Ot;

    .line 265917
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->f:LX/0Ot;

    .line 265918
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .locals 10

    .prologue
    .line 265919
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    monitor-enter v1

    .line 265920
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 265921
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265922
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265923
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 265924
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v4

    check-cast v4, LX/1VF;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    const/16 v6, 0xbc

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x5f9

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1c88

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x908

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;-><init>(LX/1VF;LX/14w;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 265925
    move-object v0, v3

    .line 265926
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 265927
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265928
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 265929
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;LX/1Pn;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 265930
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1JR;

    invoke-virtual {v0}, LX/1JR;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 265931
    :cond_0
    :goto_0
    return-void

    .line 265932
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bo0;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 265933
    iget-object v2, v0, LX/Bo0;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    move v0, v2

    .line 265934
    if-eqz v0, :cond_0

    .line 265935
    check-cast p1, LX/1Pr;

    new-instance v0, LX/C6A;

    invoke-direct {v0, p2}, LX/C6A;-><init>(Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {p1, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C69;

    .line 265936
    iget-boolean v1, v0, LX/C69;->a:Z

    move v1, v1

    .line 265937
    if-nez v1, :cond_0

    .line 265938
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/C69;->a:Z

    .line 265939
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "ff_live_comment_prepared"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 265940
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265941
    const-string v1, "id"

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 265942
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 265943
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    .line 265944
    invoke-static {p2}, LX/14w;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v0

    .line 265945
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 265946
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 265947
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->f:LX/0Ot;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v3, v4, v1}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 265948
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p0, p3, v1}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;LX/1Pn;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 265949
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 265950
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 265951
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a:LX/1VF;

    invoke-virtual {v0, p1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265952
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
