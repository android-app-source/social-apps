.class public Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;
.source ""


# static fields
.field private static final a:Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 183238
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 183239
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<+",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183240
    const-class v0, LX/0m9;

    if-ne p0, v0, :cond_0

    .line 183241
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer$ObjectDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer$ObjectDeserializer;

    move-object v0, v0

    .line 183242
    :goto_0
    return-object v0

    .line 183243
    :cond_0
    const-class v0, LX/162;

    if-ne p0, v0, :cond_1

    .line 183244
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer$ArrayDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer$ArrayDeserializer;

    move-object v0, v0

    .line 183245
    goto :goto_0

    .line 183246
    :cond_1
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;

    goto :goto_0
.end method


# virtual methods
.method public a(LX/15w;LX/0n3;)LX/0lF;
    .locals 2

    .prologue
    .line 183247
    sget-object v0, LX/164;->a:[I

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 183248
    invoke-virtual {p2}, LX/0n3;->i()LX/0mC;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->c(LX/15w;LX/0n3;LX/0mC;)LX/0lF;

    move-result-object v0

    :goto_0
    return-object v0

    .line 183249
    :pswitch_0
    invoke-virtual {p2}, LX/0n3;->i()LX/0mC;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->a(LX/15w;LX/0n3;LX/0mC;)LX/0m9;

    move-result-object v0

    goto :goto_0

    .line 183250
    :pswitch_1
    invoke-virtual {p2}, LX/0n3;->i()LX/0mC;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->b(LX/15w;LX/0n3;LX/0mC;)LX/162;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 183251
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/JsonNodeDeserializer;->a(LX/15w;LX/0n3;)LX/0lF;

    move-result-object v0

    return-object v0
.end method
