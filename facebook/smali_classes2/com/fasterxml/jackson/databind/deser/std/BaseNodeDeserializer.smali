.class public abstract Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<",
        "LX/0lF;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 183308
    const-class v0, LX/0lF;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(Ljava/lang/Class;)V

    .line 183309
    return-void
.end method


# virtual methods
.method public a()LX/0lF;
    .locals 1

    .prologue
    .line 183306
    sget-object v0, LX/2FN;->a:LX/2FN;

    move-object v0, v0

    .line 183307
    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;LX/0mC;)LX/0m9;
    .locals 4

    .prologue
    .line 183252
    invoke-virtual {p3}, LX/0mC;->c()LX/0m9;

    move-result-object v1

    .line 183253
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 183254
    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v2, :cond_0

    .line 183255
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 183256
    :cond_0
    :goto_0
    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_1

    .line 183257
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 183258
    sget-object v0, LX/165;->a:[I

    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    invoke-virtual {v3}, LX/15z;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 183259
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->c(LX/15w;LX/0n3;LX/0mC;)LX/0lF;

    move-result-object v0

    .line 183260
    :goto_1
    invoke-virtual {v1, v2, v0}, LX/0m9;->b(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 183261
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto :goto_0

    .line 183262
    :pswitch_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->a(LX/15w;LX/0n3;LX/0mC;)LX/0m9;

    move-result-object v0

    goto :goto_1

    .line 183263
    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->b(LX/15w;LX/0n3;LX/0mC;)LX/162;

    move-result-object v0

    goto :goto_1

    .line 183264
    :pswitch_2
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v0

    goto :goto_1

    .line 183265
    :cond_1
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(LX/15w;LX/0n3;LX/0mC;)LX/162;
    .locals 3

    .prologue
    .line 183296
    invoke-virtual {p3}, LX/0mC;->b()LX/162;

    move-result-object v0

    .line 183297
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    .line 183298
    if-nez v1, :cond_0

    .line 183299
    const-string v0, "Unexpected end-of-input when binding data into ArrayNode"

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 183300
    :cond_0
    sget-object v2, LX/165;->a:[I

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 183301
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->c(LX/15w;LX/0n3;LX/0mC;)LX/0lF;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 183302
    :pswitch_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->a(LX/15w;LX/0n3;LX/0mC;)LX/0m9;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 183303
    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->b(LX/15w;LX/0n3;LX/0mC;)LX/162;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 183304
    :pswitch_2
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 183305
    :pswitch_3
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c(LX/15w;LX/0n3;LX/0mC;)LX/0lF;
    .locals 3

    .prologue
    .line 183268
    sget-object v0, LX/165;->a:[I

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 183269
    :pswitch_0
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    move-object v0, v0

    .line 183270
    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 183271
    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->a(LX/15w;LX/0n3;LX/0mC;)LX/0m9;

    move-result-object v0

    .line 183272
    :goto_0
    return-object v0

    .line 183273
    :pswitch_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->b(LX/15w;LX/0n3;LX/0mC;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 183274
    :pswitch_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->a(LX/15w;LX/0n3;LX/0mC;)LX/0m9;

    move-result-object v0

    goto :goto_0

    .line 183275
    :pswitch_4
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 183276
    if-nez v0, :cond_0

    .line 183277
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v0

    goto :goto_0

    .line 183278
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 183279
    const-class v2, [B

    if-ne v1, v2, :cond_1

    .line 183280
    check-cast v0, [B

    check-cast v0, [B

    invoke-static {v0}, LX/0mC;->a([B)LX/4rH;

    move-result-object v0

    goto :goto_0

    .line 183281
    :cond_1
    invoke-static {v0}, LX/0mC;->a(Ljava/lang/Object;)LX/0mE;

    move-result-object v0

    goto :goto_0

    .line 183282
    :pswitch_5
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v0

    goto :goto_0

    .line 183283
    :pswitch_6
    invoke-virtual {p1}, LX/15w;->u()LX/16L;

    move-result-object v0

    .line 183284
    sget-object v1, LX/16L;->BIG_INTEGER:LX/16L;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/0mv;->USE_BIG_INTEGER_FOR_INTS:LX/0mv;

    invoke-virtual {p2, v1}, LX/0n3;->a(LX/0mv;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 183285
    :cond_2
    invoke-virtual {p1}, LX/15w;->z()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, LX/4rG;->a(Ljava/math/BigInteger;)LX/4rG;

    move-result-object v0

    goto :goto_0

    .line 183286
    :cond_3
    sget-object v1, LX/16L;->INT:LX/16L;

    if-ne v0, v1, :cond_4

    .line 183287
    invoke-virtual {p1}, LX/15w;->x()I

    move-result v0

    invoke-static {v0}, LX/0mC;->a(I)LX/0rP;

    move-result-object v0

    goto :goto_0

    .line 183288
    :cond_4
    invoke-virtual {p1}, LX/15w;->y()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/0mC;->a(J)LX/0rP;

    move-result-object v0

    goto :goto_0

    .line 183289
    :pswitch_7
    invoke-virtual {p1}, LX/15w;->u()LX/16L;

    move-result-object v0

    .line 183290
    sget-object v1, LX/16L;->BIG_DECIMAL:LX/16L;

    if-eq v0, v1, :cond_5

    sget-object v0, LX/0mv;->USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 183291
    :cond_5
    invoke-virtual {p1}, LX/15w;->C()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0mC;->a(Ljava/math/BigDecimal;)LX/0rP;

    move-result-object v0

    goto :goto_0

    .line 183292
    :cond_6
    invoke-virtual {p1}, LX/15w;->B()D

    move-result-wide v0

    invoke-static {v0, v1}, LX/0mC;->a(D)LX/0rP;

    move-result-object v0

    goto/16 :goto_0

    .line 183293
    :pswitch_8
    const/4 v0, 0x1

    invoke-static {v0}, LX/0mC;->a(Z)LX/1Xb;

    move-result-object v0

    goto/16 :goto_0

    .line 183294
    :pswitch_9
    const/4 v0, 0x0

    invoke-static {v0}, LX/0mC;->a(Z)LX/1Xb;

    move-result-object v0

    goto/16 :goto_0

    .line 183295
    :pswitch_a
    invoke-static {}, LX/0mC;->a()LX/2FN;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 183267
    invoke-virtual {p3, p1, p2}, LX/4qw;->d(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getNullValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 183266
    invoke-virtual {p0}, Lcom/fasterxml/jackson/databind/deser/std/BaseNodeDeserializer;->a()LX/0lF;

    move-result-object v0

    return-object v0
.end method
