.class public Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;

.field private static final b:[Ljava/lang/Object;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184493
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->b:[Ljava/lang/Object;

    .line 184494
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 184492
    const-class v0, Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 184495
    sget-object v0, LX/0mv;->USE_JAVA_ARRAY_FOR_JSON_ARRAY:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184496
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->d(LX/15w;LX/0n3;)[Ljava/lang/Object;

    move-result-object v0

    .line 184497
    :goto_0
    return-object v0

    .line 184498
    :cond_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-ne v0, v2, :cond_1

    .line 184499
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0

    .line 184500
    :cond_1
    invoke-virtual {p2}, LX/0n3;->l()LX/4rv;

    move-result-object v5

    .line 184501
    invoke-virtual {v5}, LX/4rv;->a()[Ljava/lang/Object;

    move-result-object v0

    move v2, v1

    move-object v3, v0

    move v0, v1

    .line 184502
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v6

    .line 184503
    add-int/lit8 v0, v0, 0x1

    .line 184504
    array-length v4, v3

    if-lt v2, v4, :cond_3

    .line 184505
    invoke-virtual {v5, v3}, LX/4rv;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    move v4, v1

    .line 184506
    :goto_1
    add-int/lit8 v2, v4, 0x1

    aput-object v6, v3, v4

    .line 184507
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-ne v4, v6, :cond_2

    .line 184508
    new-instance v1, Ljava/util/ArrayList;

    shr-int/lit8 v4, v0, 0x3

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 184509
    invoke-virtual {v5, v3, v2, v1}, LX/4rv;->a([Ljava/lang/Object;ILjava/util/List;)V

    move-object v0, v1

    .line 184510
    goto :goto_0

    :cond_3
    move v4, v2

    goto :goto_1
.end method

.method private c(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x4

    .line 184466
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 184467
    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 184468
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 184469
    :cond_0
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v1, :cond_1

    .line 184470
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v6}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 184471
    :goto_0
    return-object v0

    .line 184472
    :cond_1
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 184473
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 184474
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v2

    .line 184475
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v3, :cond_2

    .line 184476
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v6}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 184477
    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 184478
    :cond_2
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    .line 184479
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 184480
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v4

    .line 184481
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v5, :cond_3

    .line 184482
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v6}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 184483
    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184484
    invoke-virtual {v0, v3, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 184485
    :cond_3
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 184486
    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184487
    invoke-virtual {v0, v3, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184488
    :cond_4
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 184489
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 184490
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184491
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-ne v1, v2, :cond_4

    goto :goto_0
.end method

.method private d(LX/15w;LX/0n3;)[Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 184455
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-ne v0, v2, :cond_0

    .line 184456
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->b:[Ljava/lang/Object;

    .line 184457
    :goto_0
    return-object v0

    .line 184458
    :cond_0
    invoke-virtual {p2}, LX/0n3;->l()LX/4rv;

    move-result-object v4

    .line 184459
    invoke-virtual {v4}, LX/4rv;->a()[Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 184460
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v5

    .line 184461
    array-length v3, v2

    if-lt v0, v3, :cond_2

    .line 184462
    invoke-virtual {v4, v2}, LX/4rv;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    move v3, v1

    .line 184463
    :goto_1
    add-int/lit8 v0, v3, 0x1

    aput-object v5, v2, v3

    .line 184464
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-ne v3, v5, :cond_1

    .line 184465
    invoke-virtual {v4, v2, v0}, LX/4rv;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v3, v0

    goto :goto_1
.end method


# virtual methods
.method public deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 184438
    sget-object v0, LX/2bp;->a:[I

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 184439
    const-class v0, Ljava/lang/Object;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 184440
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->c(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 184441
    :goto_0
    return-object v0

    .line 184442
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 184443
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/UntypedObjectDeserializer;->c(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 184444
    :pswitch_3
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 184445
    :pswitch_4
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184446
    :pswitch_5
    sget-object v0, LX/0mv;->USE_BIG_INTEGER_FOR_INTS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184447
    invoke-virtual {p1}, LX/15w;->z()Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0

    .line 184448
    :cond_0
    invoke-virtual {p1}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    goto :goto_0

    .line 184449
    :pswitch_6
    sget-object v0, LX/0mv;->USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184450
    invoke-virtual {p1}, LX/15w;->C()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 184451
    :cond_1
    invoke-virtual {p1}, LX/15w;->B()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 184452
    :pswitch_7
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 184453
    :pswitch_8
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 184454
    :pswitch_9
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 184422
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 184423
    sget-object v1, LX/2bp;->a:[I

    invoke-virtual {v0}, LX/15z;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 184424
    const-class v0, Ljava/lang/Object;

    invoke-virtual {p2, v0}, LX/0n3;->b(Ljava/lang/Class;)LX/28E;

    move-result-object v0

    throw v0

    .line 184425
    :pswitch_0
    invoke-virtual {p3, p1, p2}, LX/4qw;->d(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 184426
    :goto_0
    return-object v0

    .line 184427
    :pswitch_1
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184428
    :pswitch_2
    sget-object v0, LX/0mv;->USE_BIG_INTEGER_FOR_INTS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184429
    invoke-virtual {p1}, LX/15w;->z()Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0

    .line 184430
    :cond_0
    invoke-virtual {p1}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    goto :goto_0

    .line 184431
    :pswitch_3
    sget-object v0, LX/0mv;->USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

    invoke-virtual {p2, v0}, LX/0n3;->a(LX/0mv;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184432
    invoke-virtual {p1}, LX/15w;->C()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 184433
    :cond_1
    invoke-virtual {p1}, LX/15w;->B()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 184434
    :pswitch_4
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 184435
    :pswitch_5
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 184436
    :pswitch_6
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 184437
    :pswitch_7
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method
