.class public final Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;
.super Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184300
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;-><init>()V

    sput-object v0, Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 184283
    const-class v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdScalarDeserializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private c(LX/15w;LX/0n3;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 184299
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/15w;LX/0n3;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 184286
    invoke-virtual {p1}, LX/15w;->I()Ljava/lang/String;

    move-result-object v0

    .line 184287
    if-eqz v0, :cond_0

    .line 184288
    :goto_0
    return-object v0

    .line 184289
    :cond_0
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 184290
    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_3

    .line 184291
    invoke-virtual {p1}, LX/15w;->D()Ljava/lang/Object;

    move-result-object v0

    .line 184292
    if-nez v0, :cond_1

    .line 184293
    const/4 v0, 0x0

    goto :goto_0

    .line 184294
    :cond_1
    instance-of v1, v0, [B

    if-eqz v1, :cond_2

    .line 184295
    sget-object v1, LX/0lm;->b:LX/0ln;

    move-object v1, v1

    .line 184296
    check-cast v0, [B

    check-cast v0, [B

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/0ln;->a([BZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184297
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184298
    :cond_3
    iget-object v1, p0, Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;->_valueClass:Ljava/lang/Class;

    invoke-virtual {p2, v1, v0}, LX/0n3;->a(Ljava/lang/Class;LX/15z;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 184285
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;->a(LX/15w;LX/0n3;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 184284
    invoke-direct {p0, p1, p2}, Lcom/fasterxml/jackson/databind/deser/std/StringDeserializer;->c(LX/15w;LX/0n3;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
