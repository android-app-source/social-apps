.class public Lcom/fasterxml/jackson/databind/ser/impl/UnknownSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 133563
    const-class v0, Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    .line 133564
    return-void
.end method

.method private static b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 133565
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No serializer found for class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 133566
    sget-object v0, LX/0mt;->FAIL_ON_EMPTY_BEANS:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133567
    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/impl/UnknownSerializer;->b(Ljava/lang/Object;)V

    .line 133568
    :cond_0
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 133569
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 133570
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 1

    .prologue
    .line 133571
    sget-object v0, LX/0mt;->FAIL_ON_EMPTY_BEANS:LX/0mt;

    invoke-virtual {p3, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133572
    invoke-static {p1}, Lcom/fasterxml/jackson/databind/ser/impl/UnknownSerializer;->b(Ljava/lang/Object;)V

    .line 133573
    :cond_0
    invoke-virtual {p4, p1, p2}, LX/4qz;->b(Ljava/lang/Object;LX/0nX;)V

    .line 133574
    invoke-virtual {p4, p1, p2}, LX/4qz;->e(Ljava/lang/Object;LX/0nX;)V

    .line 133575
    return-void
.end method
