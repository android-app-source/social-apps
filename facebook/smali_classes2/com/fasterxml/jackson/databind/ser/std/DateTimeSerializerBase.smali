.class public abstract Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;
.super Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;
.source ""

# interfaces
.implements LX/0nU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer",
        "<TT;>;",
        "LX/0nU;"
    }
.end annotation


# instance fields
.field public final b:Z

.field public final c:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Ljava/lang/Class;ZLjava/text/DateFormat;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;Z",
            "Ljava/text/DateFormat;",
            ")V"
        }
    .end annotation

    .prologue
    .line 135964
    invoke-direct {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;-><init>(Ljava/lang/Class;)V

    .line 135965
    iput-boolean p2, p0, Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;->b:Z

    .line 135966
    iput-object p3, p0, Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;->c:Ljava/text/DateFormat;

    .line 135967
    return-void
.end method


# virtual methods
.method public final a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 135968
    if-eqz p2, :cond_0

    .line 135969
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v0

    invoke-interface {p2}, LX/2Ay;->b()LX/2An;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lU;->e(LX/0lO;)LX/4pN;

    move-result-object v0

    .line 135970
    if-eqz v0, :cond_0

    .line 135971
    iget-object v1, v0, LX/4pN;->b:LX/2BA;

    move-object v1, v1

    .line 135972
    invoke-virtual {v1}, LX/2BA;->isNumeric()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135973
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;->a(ZLjava/text/DateFormat;)Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;

    move-result-object p0

    .line 135974
    :cond_0
    :goto_0
    return-object p0

    .line 135975
    :cond_1
    iget-object v1, v0, LX/4pN;->d:Ljava/util/TimeZone;

    move-object v1, v1

    .line 135976
    iget-object v2, v0, LX/4pN;->a:Ljava/lang/String;

    move-object v2, v2

    .line 135977
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 135978
    iget-object v3, v0, LX/4pN;->c:Ljava/util/Locale;

    move-object v0, v3

    .line 135979
    if-nez v0, :cond_2

    .line 135980
    invoke-virtual {p1}, LX/0my;->h()Ljava/util/Locale;

    move-result-object v0

    .line 135981
    :cond_2
    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-direct {v3, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 135982
    if-nez v1, :cond_5

    .line 135983
    invoke-virtual {p1}, LX/0my;->i()Ljava/util/TimeZone;

    move-result-object v0

    .line 135984
    :goto_1
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 135985
    invoke-virtual {p0, v4, v3}, Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;->a(ZLjava/text/DateFormat;)Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;

    move-result-object p0

    goto :goto_0

    .line 135986
    :cond_3
    if-eqz v1, :cond_0

    .line 135987
    iget-object v0, p1, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 135988
    invoke-virtual {v0}, LX/0m4;->o()Ljava/text/DateFormat;

    move-result-object v0

    .line 135989
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, LX/0ll;

    if-ne v2, v3, :cond_4

    .line 135990
    invoke-static {v1}, LX/0ll;->b(Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v0

    .line 135991
    :goto_2
    invoke-virtual {p0, v4, v0}, Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;->a(ZLjava/text/DateFormat;)Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;

    move-result-object p0

    goto :goto_0

    .line 135992
    :cond_4
    invoke-virtual {v0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    .line 135993
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public abstract a(ZLjava/text/DateFormat;)Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/text/DateFormat;",
            ")",
            "Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 135994
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/jackson/databind/ser/std/DateTimeSerializerBase;->b(Ljava/lang/Object;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b(Ljava/lang/Object;)J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)J"
        }
    .end annotation
.end method
