.class public Lcom/fasterxml/jackson/databind/ser/std/TokenBufferSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdSerializer",
        "<",
        "LX/0nW;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 136346
    const-class v0, LX/0nW;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private static a(LX/0nW;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 136347
    invoke-virtual {p3, p0, p1}, LX/4qz;->a(Ljava/lang/Object;LX/0nX;)V

    .line 136348
    invoke-virtual {p0, p1}, LX/0nW;->a(LX/0nX;)V

    .line 136349
    invoke-virtual {p3, p0, p1}, LX/4qz;->d(Ljava/lang/Object;LX/0nX;)V

    .line 136350
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 136351
    check-cast p1, LX/0nW;

    .line 136352
    invoke-virtual {p1, p2}, LX/0nW;->a(LX/0nX;)V

    .line 136353
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V
    .locals 0

    .prologue
    .line 136354
    check-cast p1, LX/0nW;

    invoke-static {p1, p2, p3, p4}, Lcom/fasterxml/jackson/databind/ser/std/TokenBufferSerializer;->a(LX/0nW;LX/0nX;LX/0my;LX/4qz;)V

    return-void
.end method
