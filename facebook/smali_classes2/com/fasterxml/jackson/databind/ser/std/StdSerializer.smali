.class public abstract Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 1

    .prologue
    .line 133548
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    .line 133549
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 133550
    iput-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    .line 133551
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 133545
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    .line 133546
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    .line 133547
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;B)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;B)V"
        }
    .end annotation

    .prologue
    .line 133542
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    .line 133543
    iput-object p1, p0, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    .line 133544
    return-void
.end method

.method public static a(LX/0my;LX/2Ay;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/2Ay;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 133532
    invoke-virtual {p0}, LX/0my;->e()LX/0lU;

    move-result-object v0

    .line 133533
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 133534
    invoke-interface {p1}, LX/2Ay;->b()LX/2An;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lU;->h(LX/2An;)Ljava/lang/Object;

    move-result-object v0

    .line 133535
    if-eqz v0, :cond_1

    .line 133536
    invoke-interface {p1}, LX/2Ay;->b()LX/2An;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/0mz;->a(LX/0lO;Ljava/lang/Object;)LX/1Xr;

    move-result-object v1

    .line 133537
    invoke-virtual {p0}, LX/0mz;->c()LX/0li;

    invoke-interface {v1}, LX/1Xr;->c()LX/0lJ;

    move-result-object v2

    .line 133538
    if-nez p2, :cond_0

    .line 133539
    invoke-virtual {p0, v2, p1}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object p2

    .line 133540
    :cond_0
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    invoke-direct {v0, v1, v2, p2}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    move-object p2, v0

    .line 133541
    :cond_1
    return-object p2
.end method

.method public static a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 133519
    move-object v0, p1

    :goto_0
    instance-of v1, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 133520
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 133521
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 133522
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 133523
    :cond_1
    if-eqz p0, :cond_2

    sget-object v1, LX/0mt;->WRAP_EXCEPTIONS:LX/0mt;

    invoke-virtual {p0, v1}, LX/0my;->a(LX/0mt;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    const/4 v1, 0x1

    .line 133524
    :goto_1
    instance-of v2, v0, Ljava/io/IOException;

    if-eqz v2, :cond_5

    .line 133525
    if-eqz v1, :cond_3

    instance-of v1, v0, LX/28E;

    if-nez v1, :cond_6

    .line 133526
    :cond_3
    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 133527
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 133528
    :cond_5
    if-nez v1, :cond_6

    .line 133529
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_6

    .line 133530
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 133531
    :cond_6
    invoke-static {v0, p2, p3}, LX/28E;->a(Ljava/lang/Throwable;Ljava/lang/Object;I)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public static a(LX/0my;Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 133504
    move-object v0, p1

    :goto_0
    instance-of v1, v0, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 133505
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 133506
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 133507
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 133508
    :cond_1
    if-eqz p0, :cond_2

    sget-object v1, LX/0mt;->WRAP_EXCEPTIONS:LX/0mt;

    invoke-virtual {p0, v1}, LX/0my;->a(LX/0mt;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    const/4 v1, 0x1

    .line 133509
    :goto_1
    instance-of v2, v0, Ljava/io/IOException;

    if-eqz v2, :cond_5

    .line 133510
    if-eqz v1, :cond_3

    instance-of v1, v0, LX/28E;

    if-nez v1, :cond_6

    .line 133511
    :cond_3
    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 133512
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 133513
    :cond_5
    if-nez v1, :cond_6

    .line 133514
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_6

    .line 133515
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 133516
    :cond_6
    invoke-static {v0, p2, p3}, LX/28E;->a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method

.method public static a(Lcom/fasterxml/jackson/databind/JsonSerializer;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 133518
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 133517
    iget-object v0, p0, Lcom/fasterxml/jackson/databind/ser/std/StdSerializer;->k:Ljava/lang/Class;

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/0nX;",
            "LX/0my;",
            ")V"
        }
    .end annotation
.end method
