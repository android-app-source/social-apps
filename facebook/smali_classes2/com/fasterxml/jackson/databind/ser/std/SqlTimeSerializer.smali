.class public Lcom/fasterxml/jackson/databind/ser/std/SqlTimeSerializer;
.super Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer",
        "<",
        "Ljava/sql/Time;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 136019
    const-class v0, Ljava/sql/Time;

    invoke-direct {p0, v0}, Lcom/fasterxml/jackson/databind/ser/std/StdScalarSerializer;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private static a(Ljava/sql/Time;LX/0nX;)V
    .locals 1

    .prologue
    .line 136020
    invoke-virtual {p0}, Ljava/sql/Time;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 136021
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 136022
    check-cast p1, Ljava/sql/Time;

    invoke-static {p1, p2}, Lcom/fasterxml/jackson/databind/ser/std/SqlTimeSerializer;->a(Ljava/sql/Time;LX/0nX;)V

    return-void
.end method
