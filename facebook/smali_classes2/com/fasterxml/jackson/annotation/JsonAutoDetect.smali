.class public interface abstract annotation Lcom/fasterxml/jackson/annotation/JsonAutoDetect;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JacksonAnnotation;
.end annotation

.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/fasterxml/jackson/annotation/JsonAutoDetect;
        creatorVisibility = .enum LX/0lX;->DEFAULT:LX/0lX;
        fieldVisibility = .enum LX/0lX;->DEFAULT:LX/0lX;
        getterVisibility = .enum LX/0lX;->DEFAULT:LX/0lX;
        isGetterVisibility = .enum LX/0lX;->DEFAULT:LX/0lX;
        setterVisibility = .enum LX/0lX;->DEFAULT:LX/0lX;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->ANNOTATION_TYPE:Ljava/lang/annotation/ElementType;,
        .enum Ljava/lang/annotation/ElementType;->TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract creatorVisibility()LX/0lX;
.end method

.method public abstract fieldVisibility()LX/0lX;
.end method

.method public abstract getterVisibility()LX/0lX;
.end method

.method public abstract isGetterVisibility()LX/0lX;
.end method

.method public abstract setterVisibility()LX/0lX;
.end method
