.class public LX/1iw;
.super LX/1iY;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0yW;

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/00H;

.field private final f:LX/1hs;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 299379
    const-class v0, LX/1iw;

    sput-object v0, LX/1iw;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0yW;LX/0Or;LX/0Or;LX/00H;LX/1hs;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/datacheck/annotations/IsInAndroidBalanceDetection;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yW;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/00H;",
            "LX/1hs;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299407
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 299408
    iput-object p1, p0, LX/1iw;->b:LX/0yW;

    .line 299409
    iput-object p2, p0, LX/1iw;->c:LX/0Or;

    .line 299410
    iput-object p3, p0, LX/1iw;->d:LX/0Or;

    .line 299411
    iput-object p4, p0, LX/1iw;->e:LX/00H;

    .line 299412
    iput-object p5, p0, LX/1iw;->f:LX/1hs;

    .line 299413
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 299392
    iget-object v0, p0, LX/1iw;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299393
    iget-object v0, p0, LX/1iw;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 299394
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1iw;->e:LX/00H;

    .line 299395
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 299396
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/1iw;->e:LX/00H;

    .line 299397
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 299398
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_1

    .line 299399
    :cond_0
    :goto_1
    return-void

    .line 299400
    :cond_1
    invoke-virtual {p0}, LX/1iY;->b()Lorg/apache/http/HttpRequest;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    .line 299401
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 299402
    if-nez v1, :cond_2

    .line 299403
    iget-object v0, p0, LX/1iw;->b:LX/0yW;

    invoke-virtual {v0}, LX/0yW;->c()V

    goto :goto_1

    .line 299404
    :cond_2
    iget-object v1, p0, LX/1iw;->f:LX/1hs;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1hs;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 299405
    iget-object v1, p0, LX/1iw;->b:LX/0yW;

    invoke-direct {p0}, LX/1iw;->f()I

    move-result v2

    invoke-virtual {v1, v2, v0}, LX/0yW;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 299406
    :cond_3
    iget-object v1, p0, LX/1iw;->b:LX/0yW;

    invoke-direct {p0}, LX/1iw;->f()I

    move-result v2

    invoke-virtual {v1, v2, v0}, LX/0yW;->b(ILjava/lang/String;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()I
    .locals 1

    .prologue
    .line 299386
    iget-object v0, p0, LX/1iY;->c:Lorg/apache/http/HttpResponse;

    move-object v0, v0

    .line 299387
    if-eqz v0, :cond_0

    .line 299388
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 299389
    if-eqz v0, :cond_0

    .line 299390
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 299391
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 0
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299383
    invoke-super/range {p0 .. p5}, LX/1iY;->a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    .line 299384
    invoke-direct {p0}, LX/1iw;->e()V

    .line 299385
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 0

    .prologue
    .line 299380
    invoke-super {p0, p1, p2}, LX/1iY;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 299381
    invoke-direct {p0}, LX/1iw;->e()V

    .line 299382
    return-void
.end method
