.class public LX/1l8;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 311160
    const-class v0, LX/1l8;

    sput-object v0, LX/1l8;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(LX/1kx;LX/0zO;LX/11H;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1kx;",
            "LX/0zO",
            "<TT;>;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "II)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 311148
    const-string v0, "RunnerHelper.fetchFromNetwork"

    const v2, -0xc14f988

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 311149
    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 311150
    iget-object v3, p1, LX/0zO;->n:LX/0w5;

    move-object v3, v3

    .line 311151
    invoke-static {v2, v3}, LX/0zO;->a(Lcom/facebook/common/callercontext/CallerContext;LX/0w5;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {p2, p0, p1, v0, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 311152
    const-string v2, "network"

    invoke-static {v2, p3, p4, p5}, LX/1l8;->a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 311153
    sget-object v2, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_0

    move-object v0, v1

    .line 311154
    :cond_0
    const v1, -0x7568a142

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 311155
    :catch_0
    move-exception v0

    .line 311156
    :try_start_1
    const-string v1, "network"

    invoke-static {v1, p3, p4, p5}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 311157
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311158
    :catchall_0
    move-exception v0

    const v1, -0x6c9ce3e0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static final a(Ljava/lang/String;LX/1kt;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;II)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/1kt",
            "<TT;>;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "II)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x17

    .line 311126
    const-string v0, "RunnerHelper.fetchFromDBCache"

    const v2, -0x6e64a5f4

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 311127
    :try_start_0
    invoke-interface {p1}, LX/1kt;->b()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 311128
    const-string v2, "local_db_read"

    .line 311129
    sget-object v3, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eq v0, v3, :cond_0

    .line 311130
    if-nez v0, :cond_4

    .line 311131
    const-string v3, "miss"

    invoke-interface {p2, p4, p5, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 311132
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, LX/0tX;->a:Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_2

    .line 311133
    :cond_1
    const v0, 0x54b37e8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311134
    invoke-interface {p2, p4, p5, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    move-object v0, v1

    :goto_1
    return-object v0

    .line 311135
    :cond_2
    :try_start_1
    iget-boolean v2, v0, Lcom/facebook/graphql/executor/GraphQLResult;->m:Z

    move v2, v2

    .line 311136
    if-eqz v2, :cond_3

    .line 311137
    const-string v2, "resolve_consistency_experimental"

    invoke-interface {p2, p4, p5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311138
    :cond_3
    const v1, 0x7a4c4034

    invoke-static {v1}, LX/02m;->a(I)V

    .line 311139
    invoke-interface {p2, p4, p5, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_1

    .line 311140
    :catch_0
    move-exception v0

    .line 311141
    :try_start_2
    const-string v2, "local_db_read"

    invoke-static {v2, p2, p4, p5}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 311142
    const-string v2, "RunnerHelper.fetchFromDBCache"

    const/16 v3, 0x7d0

    invoke-virtual {p3, v2, p0, v0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311143
    const v0, -0x28767288

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311144
    invoke-interface {p2, p4, p5, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    move-object v0, v1

    goto :goto_1

    .line 311145
    :catchall_0
    move-exception v0

    const v1, -0x32045a5d

    invoke-static {v1}, LX/02m;->a(I)V

    .line 311146
    invoke-interface {p2, p4, p5, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    throw v0

    .line 311147
    :cond_4
    const-string v3, "hit"

    invoke-interface {p2, p4, p5, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;LX/2lk;LX/1kt;LX/0zO;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;II)V
    .locals 5
    .param p2    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1NB;",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            "LX/2lk;",
            "LX/1kt",
            "<TT;>;",
            "LX/0zO",
            "<TT;>;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "II)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x1e

    .line 311056
    const-string v0, "RunnerHelper.updateDBFromNetwork"

    const v1, 0x32447305

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 311057
    :try_start_0
    invoke-virtual {p0}, LX/1NB;->d()V

    .line 311058
    if-eqz p2, :cond_0

    .line 311059
    invoke-interface {p2}, LX/2lk;->b()V

    .line 311060
    const/16 v0, 0x13

    invoke-interface {p5, p7, p8, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 311061
    :cond_0
    iget-object v0, p4, LX/0zO;->a:LX/0zS;

    iget-boolean v0, v0, LX/0zS;->h:Z

    if-eqz v0, :cond_1

    .line 311062
    const/4 v0, 0x1

    move v0, v0

    .line 311063
    if-eqz v0, :cond_2

    .line 311064
    instance-of v0, p3, LX/1ks;

    if-eqz v0, :cond_2

    .line 311065
    check-cast p3, LX/1ks;

    invoke-virtual {p3, p1}, LX/1ks;->b(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    .line 311066
    :goto_0
    move v0, v0

    .line 311067
    if-eqz v0, :cond_1

    .line 311068
    const-string v0, "local_db_write"

    invoke-static {v0, p5, p7, p8}, LX/1l8;->a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311069
    :cond_1
    const v0, -0x71c841d5

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311070
    invoke-interface {p5, p7, p8, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 311071
    :goto_1
    return-void

    .line 311072
    :catch_0
    move-exception v0

    .line 311073
    :try_start_1
    const-string v1, "local_db_write"

    invoke-static {v1, p5, p7, p8}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 311074
    const-string v1, "RunnerHelper.updateDBFromNetwork"

    .line 311075
    iget-object v2, p4, LX/0zO;->m:LX/0gW;

    move-object v2, v2

    .line 311076
    iget-object v3, v2, LX/0gW;->f:Ljava/lang/String;

    move-object v2, v3

    .line 311077
    const/16 v3, 0x7d0

    invoke-virtual {p6, v1, v2, v0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311078
    const v0, -0x6143ea6b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311079
    invoke-interface {p5, p7, p8, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_1

    .line 311080
    :catchall_0
    move-exception v0

    const v1, 0x4f50c25b

    invoke-static {v1}, LX/02m;->a(I)V

    .line 311081
    invoke-interface {p5, p7, p8, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    throw v0

    :cond_2
    invoke-interface {p3, p1}, LX/1kt;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/quicklog/QuickPerformanceLogger;ZII)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1NB;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "ZII)V"
        }
    .end annotation

    .prologue
    .line 311161
    const-string v0, "RunnerHelper.mutateResult"

    const v1, 0x5e19e05b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 311162
    :try_start_0
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    .line 311163
    iput-object p0, v0, LX/1lO;->d:LX/1NB;

    .line 311164
    move-object v0, v0

    .line 311165
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 311166
    invoke-virtual {p0, v0, p3, p5, p6}, LX/1NB;->b(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 311167
    const/16 v1, 0x14

    invoke-interface {p3, p5, p6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 311168
    if-eqz p4, :cond_0

    .line 311169
    invoke-virtual {p0}, LX/1NB;->b()V

    .line 311170
    :cond_0
    const v1, 0x1575dccb

    invoke-static {p2, v0, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 311171
    const/16 v0, 0x9b

    invoke-interface {p3, p5, p6, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311172
    const v0, -0x6367b73b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311173
    return-void

    .line 311174
    :catchall_0
    move-exception v0

    const v1, 0x5c869e91

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/executor/GraphQLResult;LX/2lk;)V
    .locals 2

    .prologue
    .line 311117
    const-string v0, "RunnerHelper.writeResultToConsistencyCache"

    const v1, -0x49d5cd09

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 311118
    :try_start_0
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 311119
    instance-of v1, v0, LX/0jT;

    if-eqz v1, :cond_1

    .line 311120
    check-cast v0, LX/0jT;

    invoke-interface {p1, v0}, LX/2lk;->a(LX/0jT;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311121
    :cond_0
    :goto_0
    const v0, 0x658ce67f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311122
    return-void

    .line 311123
    :cond_1
    :try_start_1
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_0

    .line 311124
    check-cast v0, Ljava/util/List;

    invoke-interface {p1, v0}, LX/2lk;->a(Ljava/util/List;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 311125
    :catchall_0
    move-exception v0

    const v1, -0x7c189c31

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(Ljava/lang/String;LX/1NB;LX/2lk;Ljava/util/Set;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/03V;II)V
    .locals 4
    .param p2    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/1NB;",
            "LX/2lk;",
            "Ljava/util/Set",
            "<",
            "LX/0jV;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "II)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x11

    .line 311100
    const-string v0, "RunnerHelper.updateMemoryFromNetwork"

    const v1, -0x601f23da

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 311101
    :try_start_0
    invoke-virtual {p1}, LX/1NB;->c()V

    .line 311102
    if-eqz p2, :cond_1

    invoke-interface {p2}, LX/2lk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 311103
    invoke-interface {p2}, LX/2lk;->d()LX/3Bq;

    move-result-object v1

    .line 311104
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jV;

    .line 311105
    invoke-interface {v0, v1}, LX/0jV;->a(LX/3Bq;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 311106
    :catch_0
    move-exception v0

    .line 311107
    :try_start_1
    const-string v1, "local_memory_write"

    invoke-static {v1, p4, p6, p7}, LX/1l8;->b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V

    .line 311108
    const-string v1, "RunnerHelper.updateMemoryFromNetwork"

    const/16 v2, 0x7d0

    invoke-virtual {p5, v1, p0, v0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311109
    const v0, -0x9b1f110

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311110
    invoke-interface {p4, p6, p7, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 311111
    :goto_1
    return-void

    .line 311112
    :cond_0
    :try_start_2
    const-string v0, "local_memory_write"

    invoke-static {v0, p4, p6, p7}, LX/1l8;->a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311113
    :cond_1
    const v0, 0x355e60df

    invoke-static {v0}, LX/02m;->a(I)V

    .line 311114
    invoke-interface {p4, p6, p7, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_1

    .line 311115
    :catchall_0
    move-exception v0

    const v1, 0x74fa005c

    invoke-static {v1}, LX/02m;->a(I)V

    .line 311116
    invoke-interface {p4, p6, p7, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    throw v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    .locals 1

    .prologue
    .line 311098
    const-string v0, "success"

    invoke-interface {p1, p2, p3, p0, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 311099
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    .locals 2

    .prologue
    .line 311084
    invoke-interface {p4, p5, p6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311085
    const-string v0, "failure_cause"

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, p5, p6, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 311086
    :cond_0
    const/4 v0, 0x0

    .line 311087
    instance-of v1, p2, LX/4bK;

    if-eqz v1, :cond_3

    .line 311088
    :cond_1
    :goto_0
    move v0, v0

    .line 311089
    if-eqz v0, :cond_2

    .line 311090
    const/16 v0, 0x7d0

    invoke-virtual {p3, p0, p1, p2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 311091
    :cond_2
    return-void

    .line 311092
    :cond_3
    instance-of v1, p2, LX/4bL;

    if-nez v1, :cond_1

    .line 311093
    instance-of v1, p2, Lorg/apache/http/client/HttpResponseException;

    if-nez v1, :cond_1

    .line 311094
    instance-of v1, p2, Ljava/io/EOFException;

    if-nez v1, :cond_1

    .line 311095
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 311096
    if-eqz v1, :cond_4

    const-string p4, "TigonError"

    invoke-virtual {v1, p4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 311097
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;II)V
    .locals 1

    .prologue
    .line 311082
    const-string v0, "error"

    invoke-interface {p1, p2, p3, p0, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 311083
    return-void
.end method
