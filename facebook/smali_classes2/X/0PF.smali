.class public abstract LX/0PF;
.super LX/0Ov;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/UriMatcher;

.field public c:LX/2A8;

.field public d:LX/3Ek;

.field public e:LX/0xB;

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0xW;

.field public h:LX/3Fe;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55341
    const-class v0, LX/0PF;

    sput-object v0, LX/0PF;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55462
    invoke-direct {p0}, LX/0Ov;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v1, p1

    check-cast v1, LX/0PF;

    invoke-static {v7}, LX/3Ek;->a(LX/0QB;)LX/3Ek;

    move-result-object v2

    check-cast v2, LX/3Ek;

    invoke-static {v7}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v3

    check-cast v3, LX/0xB;

    invoke-static {v7}, LX/2A8;->b(LX/0QB;)LX/2A8;

    move-result-object v4

    check-cast v4, LX/2A8;

    const/16 v5, 0x15e7

    invoke-static {v7, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v7}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v6

    check-cast v6, LX/0xW;

    new-instance v0, LX/3Fe;

    invoke-static {v7}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    invoke-static {v7}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p1

    check-cast p1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v0, p0, p1}, LX/3Fe;-><init>(LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    const/16 p0, 0xe5e

    invoke-static {v7, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    iput-object p0, v0, LX/3Fe;->c:LX/0Ot;

    move-object v7, v0

    check-cast v7, LX/3Fe;

    iput-object v2, v1, LX/0PF;->d:LX/3Ek;

    iput-object v3, v1, LX/0PF;->e:LX/0xB;

    iput-object v4, v1, LX/0PF;->c:LX/2A8;

    iput-object v5, v1, LX/0PF;->f:LX/0Or;

    iput-object v6, v1, LX/0PF;->g:LX/0xW;

    iput-object v7, v1, LX/0PF;->h:LX/3Fe;

    return-void
.end method

.method private d(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 55459
    const-string v0, "NO_NOTIFY"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55460
    :goto_0
    return-void

    .line 55461
    :cond_0
    invoke-virtual {p0, p1}, LX/0PF;->c(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private e()V
    .locals 14

    .prologue
    .line 55438
    const/4 v10, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 55439
    const-string v3, " "

    invoke-static {v3}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v3

    const-string v4, "SELECT COUNT(*)"

    const-string v5, "FROM"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "gql_notifications"

    aput-object v7, v6, v11

    const-string v7, "WHERE"

    aput-object v7, v6, v12

    const-string v7, "%s == %s"

    new-array v8, v10, [Ljava/lang/Object;

    sget-object v9, LX/2A7;->d:LX/0U1;

    .line 55440
    iget-object v13, v9, LX/0U1;->d:Ljava/lang/String;

    move-object v9, v13

    .line 55441
    aput-object v9, v8, v11

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v12

    invoke-static {v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    const/4 v7, 0x3

    const-string v8, "AND"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "%s == %s"

    new-array v9, v10, [Ljava/lang/Object;

    sget-object v10, LX/2A7;->c:LX/0U1;

    .line 55442
    iget-object v13, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v13

    .line 55443
    aput-object v10, v9, v11

    iget-object v10, p0, LX/0PF;->f:LX/0Or;

    invoke-interface {v10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 55444
    iget-object v4, p0, LX/0PF;->d:LX/3Ek;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 55445
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 55446
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 55447
    invoke-interface {v3, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    long-to-int v4, v5

    .line 55448
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 55449
    move v0, v4

    .line 55450
    iget-object v1, p0, LX/0PF;->h:LX/3Fe;

    .line 55451
    iget-object v2, v1, LX/3Fe;->a:LX/0Uh;

    const/16 v3, 0x3bb

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 55452
    :cond_0
    :goto_0
    iget-object v1, p0, LX/0PF;->e:LX/0xB;

    sget-object v2, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v1, v2, v0}, LX/0xB;->a(LX/12j;I)V

    .line 55453
    return-void

    .line 55454
    :cond_1
    const-string v2, ""

    .line 55455
    if-eqz v0, :cond_2

    .line 55456
    iget-object v2, v1, LX/3Fe;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f()Ljava/lang/String;

    move-result-object v2

    .line 55457
    :cond_2
    if-eqz v2, :cond_0

    .line 55458
    iget-object v3, v1, LX/3Fe;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/0hM;->M:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 55423
    iget-object v0, p0, LX/0PF;->d:LX/3Ek;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 55424
    iget-object v0, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55425
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55426
    :pswitch_0
    const-string v0, "gql_notifications"

    invoke-virtual {v1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 55427
    :goto_0
    if-lez v0, :cond_0

    sget-object v1, LX/2A7;->d:LX/0U1;

    .line 55428
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 55429
    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55430
    invoke-direct {p0}, LX/0PF;->e()V

    .line 55431
    :cond_0
    if-lez v0, :cond_1

    .line 55432
    invoke-direct {p0, p1}, LX/0PF;->d(Landroid/net/Uri;)V

    .line 55433
    :cond_1
    return v0

    .line 55434
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55435
    const-string v2, "gql_notifications"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2A7;->a:LX/0U1;

    .line 55436
    iget-object p3, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, p3

    .line 55437
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 55398
    const/4 v1, 0x0

    .line 55399
    iget-object v0, p0, LX/0PF;->d:LX/3Ek;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 55400
    iget-object v0, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55401
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55402
    :pswitch_0
    :try_start_0
    const-string v0, "gql_notifications"

    invoke-virtual {v2, v0, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 55403
    :goto_0
    if-lez v0, :cond_0

    .line 55404
    invoke-direct {p0}, LX/0PF;->e()V

    .line 55405
    invoke-direct {p0, p1}, LX/0PF;->d(Landroid/net/Uri;)V

    .line 55406
    :cond_0
    return v0

    .line 55407
    :catch_0
    move-exception v0

    .line 55408
    sget-object v2, LX/0PF;->a:Ljava/lang/Class;

    const-string v3, "db.delete SQLiteFullException"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 55409
    goto :goto_0

    .line 55410
    :catch_1
    move-exception v0

    .line 55411
    sget-object v2, LX/0PF;->a:Ljava/lang/Class;

    const-string v3, "db.delete SQLiteDiskIOException"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 55412
    goto :goto_0

    .line 55413
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55414
    :try_start_1
    const-string v3, "gql_notifications"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/2A7;->a:LX/0U1;

    .line 55415
    iget-object p2, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p2

    .line 55416
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v0

    goto :goto_0

    .line 55417
    :catch_2
    move-exception v0

    .line 55418
    sget-object v2, LX/0PF;->a:Ljava/lang/Class;

    const-string v3, "db.delete SQLiteFullException"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 55419
    goto :goto_0

    .line 55420
    :catch_3
    move-exception v0

    .line 55421
    sget-object v2, LX/0PF;->a:Ljava/lang/Class;

    const-string v3, "db.delete SQLiteDiskIOException"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 55422
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 55383
    iget-object v1, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 55384
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55385
    :cond_0
    iget-object v1, p0, LX/0PF;->d:LX/3Ek;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    move v1, v0

    .line 55386
    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_2

    .line 55387
    const-string v3, "gql_notifications"

    sget-object v4, LX/2A7;->b:LX/0U1;

    .line 55388
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 55389
    aget-object v5, p2, v0

    const v6, -0x5fdad3a2

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const v3, 0x61b2f8ba

    invoke-static {v3}, LX/03h;->a(I)V

    .line 55390
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 55391
    add-int/lit8 v1, v1, 0x1

    .line 55392
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55393
    :cond_2
    if-lez v1, :cond_3

    .line 55394
    invoke-direct {p0}, LX/0PF;->e()V

    .line 55395
    invoke-direct {p0, p1}, LX/0PF;->d(Landroid/net/Uri;)V

    .line 55396
    return v1

    .line 55397
    :cond_3
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to insert row into "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 55368
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 55369
    iget-object v1, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 55370
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55371
    :pswitch_0
    const-string v1, "gql_notifications"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 55372
    :goto_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v7, "updated DESC"

    .line 55373
    :goto_1
    const-string v1, "LIMIT"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 55374
    iget-object v1, p0, LX/0PF;->d:LX/3Ek;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    .line 55375
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 55376
    invoke-virtual {p0}, LX/0PF;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 55377
    return-object v0

    .line 55378
    :pswitch_1
    const-string v1, "gql_notifications"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 55379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/2A7;->a:LX/0U1;

    .line 55380
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 55381
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    move-object v7, p5

    .line 55382
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 55353
    if-eqz p2, :cond_0

    .line 55354
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 55355
    :goto_0
    iget-object v1, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 55356
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55357
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    goto :goto_0

    .line 55358
    :cond_1
    iget-object v1, p0, LX/0PF;->d:LX/3Ek;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 55359
    const-string v2, "gql_notifications"

    sget-object v3, LX/2A7;->b:LX/0U1;

    .line 55360
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 55361
    const v4, -0x376f5df7

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const v2, 0x122ad385

    invoke-static {v2}, LX/03h;->a(I)V

    .line 55362
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 55363
    invoke-direct {p0}, LX/0PF;->e()V

    .line 55364
    iget-object v2, p0, LX/0PF;->c:LX/2A8;

    iget-object v2, v2, LX/2A8;->b:Landroid/net/Uri;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 55365
    invoke-direct {p0, p1}, LX/0PF;->d(Landroid/net/Uri;)V

    .line 55366
    return-object v0

    .line 55367
    :cond_2
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to insert row into "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 55350
    iget-object v0, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 55351
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55352
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.facebook.katana.gql_notifications"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 4

    .prologue
    .line 55344
    const-class v0, LX/0PF;

    invoke-static {v0, p0}, LX/0PF;->a(Ljava/lang/Class;LX/02k;)V

    .line 55345
    invoke-virtual {p0}, LX/0PF;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 55346
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    .line 55347
    iget-object v0, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    iget-object v1, p0, LX/0PF;->c:LX/2A8;

    iget-object v1, v1, LX/2A8;->a:Ljava/lang/String;

    const-string v2, "gql_notifications"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 55348
    iget-object v0, p0, LX/0PF;->b:Landroid/content/UriMatcher;

    iget-object v1, p0, LX/0PF;->c:LX/2A8;

    iget-object v1, v1, LX/2A8;->a:Ljava/lang/String;

    const-string v2, "gql_notifications/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 55349
    return-void
.end method

.method public c(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 55342
    invoke-virtual {p0}, LX/0PF;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 55343
    return-void
.end method
