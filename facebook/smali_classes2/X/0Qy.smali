.class public final LX/0Qy;
.super Ljava/util/AbstractQueue;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "LX/0R1",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59294
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 59295
    new-instance v0, LX/0Qz;

    invoke-direct {v0, p0}, LX/0Qz;-><init>(LX/0Qy;)V

    iput-object v0, p0, LX/0Qy;->a:LX/0R1;

    return-void
.end method

.method private a()LX/0R1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 59292
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    .line 59293
    iget-object v1, p0, LX/0Qy;->a:LX/0R1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 59284
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    .line 59285
    :goto_0
    iget-object v1, p0, LX/0Qy;->a:LX/0R1;

    if-eq v0, v1, :cond_0

    .line 59286
    invoke-interface {v0}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v1

    .line 59287
    invoke-static {v0}, LX/0Qd;->b(LX/0R1;)V

    move-object v0, v1

    .line 59288
    goto :goto_0

    .line 59289
    :cond_0
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    iget-object v1, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0, v1}, LX/0R1;->setNextInAccessQueue(LX/0R1;)V

    .line 59290
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    iget-object v1, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0, v1}, LX/0R1;->setPreviousInAccessQueue(LX/0R1;)V

    .line 59291
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 59282
    check-cast p1, LX/0R1;

    .line 59283
    invoke-interface {p1}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    sget-object v1, LX/0SZ;->INSTANCE:LX/0SZ;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 59281
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    iget-object v1, p0, LX/0Qy;->a:LX/0R1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0R1",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 59296
    new-instance v0, LX/4wC;

    invoke-direct {p0}, LX/0Qy;->a()LX/0R1;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/4wC;-><init>(LX/0Qy;LX/0R1;)V

    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 59276
    check-cast p1, LX/0R1;

    .line 59277
    invoke-interface {p1}, LX/0R1;->getPreviousInAccessQueue()LX/0R1;

    move-result-object v0

    invoke-interface {p1}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qd;->a(LX/0R1;LX/0R1;)V

    .line 59278
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getPreviousInAccessQueue()LX/0R1;

    move-result-object v0

    invoke-static {v0, p1}, LX/0Qd;->a(LX/0R1;LX/0R1;)V

    .line 59279
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    invoke-static {p1, v0}, LX/0Qd;->a(LX/0R1;LX/0R1;)V

    .line 59280
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 59275
    invoke-direct {p0}, LX/0Qy;->a()LX/0R1;

    move-result-object v0

    return-object v0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 59270
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    .line 59271
    iget-object v1, p0, LX/0Qy;->a:LX/0R1;

    if-ne v0, v1, :cond_0

    .line 59272
    const/4 v0, 0x0

    .line 59273
    :goto_0
    return-object v0

    .line 59274
    :cond_0
    invoke-virtual {p0, v0}, LX/0Qy;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 59264
    check-cast p1, LX/0R1;

    .line 59265
    invoke-interface {p1}, LX/0R1;->getPreviousInAccessQueue()LX/0R1;

    move-result-object v0

    .line 59266
    invoke-interface {p1}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v1

    .line 59267
    invoke-static {v0, v1}, LX/0Qd;->a(LX/0R1;LX/0R1;)V

    .line 59268
    invoke-static {p1}, LX/0Qd;->b(LX/0R1;)V

    .line 59269
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 59259
    const/4 v1, 0x0

    .line 59260
    iget-object v0, p0, LX/0Qy;->a:LX/0R1;

    invoke-interface {v0}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    :goto_0
    iget-object v2, p0, LX/0Qy;->a:LX/0R1;

    if-eq v0, v2, :cond_0

    .line 59261
    add-int/lit8 v1, v1, 0x1

    .line 59262
    invoke-interface {v0}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    goto :goto_0

    .line 59263
    :cond_0
    return v1
.end method
