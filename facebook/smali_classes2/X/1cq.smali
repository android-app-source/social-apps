.class public LX/1cq;
.super LX/1cr;
.source ""


# instance fields
.field private final b:LX/1cn;


# direct methods
.method public constructor <init>(LX/1cn;)V
    .locals 0

    .prologue
    .line 283423
    invoke-direct {p0, p1}, LX/1cr;-><init>(Landroid/view/View;)V

    .line 283424
    iput-object p1, p0, LX/1cq;->b:LX/1cn;

    .line 283425
    return-void
.end method

.method private static a(LX/1cn;)LX/1cv;
    .locals 4

    .prologue
    .line 283417
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/1cn;->getMountItemCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 283418
    invoke-virtual {p0, v1}, LX/1cn;->a(I)LX/1cv;

    move-result-object v0

    .line 283419
    invoke-virtual {v0}, LX/1cv;->u()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 283420
    :goto_1
    return-object v0

    .line 283421
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 283422
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(FF)I
    .locals 6

    .prologue
    const/high16 v1, -0x80000000

    .line 283405
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-static {v0}, LX/1cq;->a(LX/1cn;)LX/1cv;

    move-result-object v0

    .line 283406
    if-nez v0, :cond_1

    move v0, v1

    .line 283407
    :cond_0
    :goto_0
    return v0

    .line 283408
    :cond_1
    iget-object v2, v0, LX/1cv;->a:LX/1X1;

    move-object v2, v2

    .line 283409
    iget-object v3, v2, LX/1X1;->e:LX/1S3;

    move-object v3, v3

    .line 283410
    invoke-virtual {v3, v2}, LX/1S3;->a(LX/1X1;)I

    move-result v4

    if-nez v4, :cond_2

    move v0, v1

    .line 283411
    goto :goto_0

    .line 283412
    :cond_2
    iget-object v4, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v4

    .line 283413
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 283414
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 283415
    float-to-int v4, p1

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    float-to-int v5, p2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v5, v0

    invoke-virtual {v3, v4, v0, v2}, LX/1S3;->a(IILX/1X1;)I

    move-result v0

    .line 283416
    if-gez v0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)LX/3t3;
    .locals 2

    .prologue
    .line 283398
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-static {v0}, LX/1cq;->a(LX/1cn;)LX/1cv;

    move-result-object v0

    .line 283399
    if-eqz v0, :cond_0

    .line 283400
    iget-object v1, v0, LX/1cv;->a:LX/1X1;

    move-object v0, v1

    .line 283401
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 283402
    invoke-virtual {v0}, LX/1S3;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283403
    invoke-super {p0, p1}, LX/1cr;->a(Landroid/view/View;)LX/3t3;

    move-result-object v0

    .line 283404
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILX/3sp;)V
    .locals 6

    .prologue
    .line 283383
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-static {v0}, LX/1cq;->a(LX/1cn;)LX/1cv;

    move-result-object v1

    .line 283384
    if-nez v1, :cond_0

    .line 283385
    const-string v0, "ComponentAccessibility"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No accessible mount item found for view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1cq;->b:LX/1cn;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283386
    const-string v0, ""

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 283387
    :goto_0
    return-void

    .line 283388
    :cond_0
    iget-object v0, v1, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 283389
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 283390
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 283391
    iget-object v0, v1, LX/1cv;->a:LX/1X1;

    move-object v5, v0

    .line 283392
    iget-object v0, v5, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 283393
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 283394
    invoke-virtual {v0, v5}, LX/1S3;->a(LX/1X1;)I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 283395
    const-string v0, "ComponentAccessibility"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received unrecognized virtual view id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283396
    const-string v0, ""

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 283397
    :cond_1
    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    move-object v1, p2

    move v2, p1

    invoke-virtual/range {v0 .. v5}, LX/1S3;->a(LX/3sp;IIILX/1X1;)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 283381
    const-string v0, ""

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 283382
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 283366
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283367
    iget-object v1, v0, LX/1cn;->H:LX/1dQ;

    move-object v0, v1

    .line 283368
    if-eqz v0, :cond_1

    .line 283369
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283370
    iget-object v1, v0, LX/1cn;->H:LX/1dQ;

    move-object v0, v1

    .line 283371
    invoke-static {}, LX/1dS;->b()V

    .line 283372
    sget-object v1, LX/1dY;->n:LX/48F;

    if-nez v1, :cond_0

    .line 283373
    new-instance v1, LX/48F;

    invoke-direct {v1}, LX/48F;-><init>()V

    sput-object v1, LX/1dY;->n:LX/48F;

    .line 283374
    :cond_0
    sget-object v1, LX/1dY;->n:LX/48F;

    iput-object p1, v1, LX/48F;->a:Landroid/view/View;

    .line 283375
    sget-object v1, LX/1dY;->n:LX/48F;

    iput p2, v1, LX/48F;->b:I

    .line 283376
    iget-object v1, v0, LX/1dQ;->a:LX/1X1;

    .line 283377
    iget-object v2, v1, LX/1X1;->e:LX/1S3;

    move-object v1, v2

    .line 283378
    sget-object v2, LX/1dY;->n:LX/48F;

    invoke-virtual {v1, v0, v2}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283379
    :cond_1
    invoke-super {p0, p1, p2}, LX/1cr;->a(Landroid/view/View;I)V

    .line 283380
    return-void
.end method

.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 2

    .prologue
    .line 283346
    invoke-super {p0, p1, p2}, LX/1cr;->a(Landroid/view/View;LX/3sp;)V

    .line 283347
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-static {v0}, LX/1cq;->a(LX/1cn;)LX/1cv;

    move-result-object v0

    .line 283348
    if-eqz v0, :cond_0

    .line 283349
    iget-object v1, v0, LX/1cv;->a:LX/1X1;

    move-object v0, v1

    .line 283350
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v1, v1

    .line 283351
    invoke-virtual {v1, p2, v0}, LX/1S3;->a(LX/3sp;LX/1X1;)V

    .line 283352
    :cond_0
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283353
    iget-object v1, v0, LX/1cn;->E:LX/1dQ;

    move-object v0, v1

    .line 283354
    if-eqz v0, :cond_2

    .line 283355
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283356
    iget-object v1, v0, LX/1cn;->E:LX/1dQ;

    move-object v0, v1

    .line 283357
    invoke-static {}, LX/1dS;->b()V

    .line 283358
    sget-object v1, LX/1dY;->j:LX/3CI;

    if-nez v1, :cond_1

    .line 283359
    new-instance v1, LX/3CI;

    invoke-direct {v1}, LX/3CI;-><init>()V

    sput-object v1, LX/1dY;->j:LX/3CI;

    .line 283360
    :cond_1
    sget-object v1, LX/1dY;->j:LX/3CI;

    iput-object p1, v1, LX/3CI;->a:Landroid/view/View;

    .line 283361
    sget-object v1, LX/1dY;->j:LX/3CI;

    iput-object p2, v1, LX/3CI;->b:LX/3sp;

    .line 283362
    iget-object v1, v0, LX/1dQ;->a:LX/1X1;

    .line 283363
    iget-object p0, v1, LX/1X1;->e:LX/1S3;

    move-object v1, p0

    .line 283364
    sget-object p0, LX/1dY;->j:LX/3CI;

    invoke-virtual {v1, v0, p0}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283365
    :cond_2
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    .prologue
    .line 283331
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283332
    iget-object v1, v0, LX/1cn;->I:LX/1dQ;

    move-object v0, v1

    .line 283333
    if-eqz v0, :cond_1

    .line 283334
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283335
    iget-object v1, v0, LX/1cn;->I:LX/1dQ;

    move-object v0, v1

    .line 283336
    invoke-static {}, LX/1dS;->b()V

    .line 283337
    sget-object v1, LX/1dY;->o:LX/48G;

    if-nez v1, :cond_0

    .line 283338
    new-instance v1, LX/48G;

    invoke-direct {v1}, LX/48G;-><init>()V

    sput-object v1, LX/1dY;->o:LX/48G;

    .line 283339
    :cond_0
    sget-object v1, LX/1dY;->o:LX/48G;

    iput-object p1, v1, LX/48G;->a:Landroid/view/View;

    .line 283340
    sget-object v1, LX/1dY;->o:LX/48G;

    iput-object p2, v1, LX/48G;->b:Landroid/view/accessibility/AccessibilityEvent;

    .line 283341
    iget-object v1, v0, LX/1dQ;->a:LX/1X1;

    .line 283342
    iget-object v2, v1, LX/1X1;->e:LX/1S3;

    move-object v1, v2

    .line 283343
    sget-object v2, LX/1dY;->o:LX/48G;

    invoke-virtual {v1, v0, v2}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283344
    :cond_1
    invoke-super {p0, p1, p2}, LX/1cr;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 283345
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283231
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-static {v0}, LX/1cq;->a(LX/1cn;)LX/1cv;

    move-result-object v0

    .line 283232
    if-nez v0, :cond_1

    .line 283233
    :cond_0
    return-void

    .line 283234
    :cond_1
    iget-object v1, v0, LX/1cv;->a:LX/1X1;

    move-object v0, v1

    .line 283235
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v1, v1

    .line 283236
    invoke-virtual {v1, v0}, LX/1S3;->a(LX/1X1;)I

    move-result v1

    .line 283237
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 283238
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283239
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 283322
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-static {v0}, LX/1cq;->a(LX/1cn;)LX/1cv;

    move-result-object v0

    .line 283323
    if-eqz v0, :cond_2

    .line 283324
    iget-object v1, v0, LX/1cv;->a:LX/1X1;

    move-object v0, v1

    .line 283325
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 283326
    invoke-virtual {v0}, LX/1S3;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283327
    invoke-super {p0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-virtual {v0, p1}, LX/1cn;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 283328
    :goto_0
    return v0

    .line 283329
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 283330
    :cond_2
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-virtual {v0, p1}, LX/1cn;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 283307
    iget-object v1, p0, LX/1cq;->b:LX/1cn;

    .line 283308
    iget-object v2, v1, LX/1cn;->G:LX/1dQ;

    move-object v1, v2

    .line 283309
    if-eqz v1, :cond_2

    .line 283310
    iget-object v1, p0, LX/1cq;->b:LX/1cn;

    .line 283311
    iget-object v2, v1, LX/1cn;->G:LX/1dQ;

    move-object v1, v2

    .line 283312
    invoke-static {}, LX/1dS;->b()V

    .line 283313
    sget-object v2, LX/1dY;->m:LX/48E;

    if-nez v2, :cond_0

    .line 283314
    new-instance v2, LX/48E;

    invoke-direct {v2}, LX/48E;-><init>()V

    sput-object v2, LX/1dY;->m:LX/48E;

    .line 283315
    :cond_0
    sget-object v2, LX/1dY;->m:LX/48E;

    iput-object p1, v2, LX/48E;->a:Landroid/view/View;

    .line 283316
    sget-object v2, LX/1dY;->m:LX/48E;

    iput p2, v2, LX/48E;->b:I

    .line 283317
    sget-object v2, LX/1dY;->m:LX/48E;

    iput-object p3, v2, LX/48E;->c:Landroid/os/Bundle;

    .line 283318
    iget-object v2, v1, LX/1dQ;->a:LX/1X1;

    .line 283319
    iget-object v3, v2, LX/1X1;->e:LX/1S3;

    move-object v2, v3

    .line 283320
    sget-object v3, LX/1dY;->m:LX/48E;

    invoke-virtual {v2, v1, v3}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 283321
    :goto_0
    if-eqz v1, :cond_1

    invoke-super {p0, p1, p2, p3}, LX/1cr;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 283292
    iget-object v1, p0, LX/1cq;->b:LX/1cn;

    .line 283293
    iget-object v2, v1, LX/1cn;->F:LX/1dQ;

    move-object v1, v2

    .line 283294
    if-eqz v1, :cond_2

    .line 283295
    iget-object v1, p0, LX/1cq;->b:LX/1cn;

    .line 283296
    iget-object v2, v1, LX/1cn;->F:LX/1dQ;

    move-object v1, v2

    .line 283297
    invoke-static {}, LX/1dS;->b()V

    .line 283298
    sget-object v2, LX/1dY;->l:LX/2uO;

    if-nez v2, :cond_0

    .line 283299
    new-instance v2, LX/2uO;

    invoke-direct {v2}, LX/2uO;-><init>()V

    sput-object v2, LX/1dY;->l:LX/2uO;

    .line 283300
    :cond_0
    sget-object v2, LX/1dY;->l:LX/2uO;

    iput-object p1, v2, LX/2uO;->a:Landroid/view/ViewGroup;

    .line 283301
    sget-object v2, LX/1dY;->l:LX/2uO;

    iput-object p2, v2, LX/2uO;->b:Landroid/view/View;

    .line 283302
    sget-object v2, LX/1dY;->l:LX/2uO;

    iput-object p3, v2, LX/2uO;->c:Landroid/view/accessibility/AccessibilityEvent;

    .line 283303
    iget-object v2, v1, LX/1dQ;->a:LX/1X1;

    .line 283304
    iget-object v3, v2, LX/1X1;->e:LX/1S3;

    move-object v2, v3

    .line 283305
    sget-object v3, LX/1dY;->l:LX/2uO;

    invoke-virtual {v2, v1, v3}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 283306
    :goto_0
    if-eqz v1, :cond_1

    invoke-super {p0, p1, p2, p3}, LX/1cr;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 283285
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    invoke-static {v0}, LX/1cq;->a(LX/1cn;)LX/1cv;

    move-result-object v0

    .line 283286
    if-eqz v0, :cond_0

    .line 283287
    iget-object v1, v0, LX/1cv;->a:LX/1X1;

    move-object v0, v1

    .line 283288
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 283289
    invoke-virtual {v0}, LX/1S3;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 283290
    :cond_0
    :goto_0
    return-void

    .line 283291
    :cond_1
    invoke-super {p0}, LX/1cr;->b()V

    goto :goto_0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 283284
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 283270
    iget-object v1, p0, LX/1cq;->b:LX/1cn;

    .line 283271
    iget-object v2, v1, LX/1cn;->B:LX/1dQ;

    move-object v1, v2

    .line 283272
    if-eqz v1, :cond_2

    .line 283273
    iget-object v1, p0, LX/1cq;->b:LX/1cn;

    .line 283274
    iget-object v2, v1, LX/1cn;->B:LX/1dQ;

    move-object v1, v2

    .line 283275
    invoke-static {}, LX/1dS;->b()V

    .line 283276
    sget-object v2, LX/1dY;->h:LX/32D;

    if-nez v2, :cond_0

    .line 283277
    new-instance v2, LX/32D;

    invoke-direct {v2}, LX/32D;-><init>()V

    sput-object v2, LX/1dY;->h:LX/32D;

    .line 283278
    :cond_0
    sget-object v2, LX/1dY;->h:LX/32D;

    iput-object p1, v2, LX/32D;->a:Landroid/view/View;

    .line 283279
    sget-object v2, LX/1dY;->h:LX/32D;

    iput-object p2, v2, LX/32D;->b:Landroid/view/accessibility/AccessibilityEvent;

    .line 283280
    iget-object v2, v1, LX/1dQ;->a:LX/1X1;

    .line 283281
    iget-object v3, v2, LX/1X1;->e:LX/1S3;

    move-object v2, v3

    .line 283282
    sget-object v3, LX/1dY;->h:LX/32D;

    invoke-virtual {v2, v1, v3}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 283283
    :goto_0
    if-eqz v1, :cond_1

    invoke-super {p0, p1, p2}, LX/1cr;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public final c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 283255
    invoke-super {p0, p1, p2}, LX/1cr;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 283256
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283257
    iget-object v1, v0, LX/1cn;->D:LX/1dQ;

    move-object v0, v1

    .line 283258
    if-eqz v0, :cond_1

    .line 283259
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283260
    iget-object v1, v0, LX/1cn;->D:LX/1dQ;

    move-object v0, v1

    .line 283261
    invoke-static {}, LX/1dS;->b()V

    .line 283262
    sget-object v1, LX/1dY;->k:LX/48D;

    if-nez v1, :cond_0

    .line 283263
    new-instance v1, LX/48D;

    invoke-direct {v1}, LX/48D;-><init>()V

    sput-object v1, LX/1dY;->k:LX/48D;

    .line 283264
    :cond_0
    sget-object v1, LX/1dY;->k:LX/48D;

    iput-object p1, v1, LX/48D;->a:Landroid/view/View;

    .line 283265
    sget-object v1, LX/1dY;->k:LX/48D;

    iput-object p2, v1, LX/48D;->b:Landroid/view/accessibility/AccessibilityEvent;

    .line 283266
    iget-object v1, v0, LX/1dQ;->a:LX/1X1;

    .line 283267
    iget-object p0, v1, LX/1X1;->e:LX/1S3;

    move-object v1, p0

    .line 283268
    sget-object p0, LX/1dY;->k:LX/48D;

    invoke-virtual {v1, v0, p0}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283269
    :cond_1
    return-void
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 283240
    invoke-super {p0, p1, p2}, LX/1cr;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 283241
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283242
    iget-object v1, v0, LX/1cn;->C:LX/1dQ;

    move-object v0, v1

    .line 283243
    if-eqz v0, :cond_1

    .line 283244
    iget-object v0, p0, LX/1cq;->b:LX/1cn;

    .line 283245
    iget-object v1, v0, LX/1cn;->C:LX/1dQ;

    move-object v0, v1

    .line 283246
    invoke-static {}, LX/1dS;->b()V

    .line 283247
    sget-object v1, LX/1dY;->i:LX/32A;

    if-nez v1, :cond_0

    .line 283248
    new-instance v1, LX/32A;

    invoke-direct {v1}, LX/32A;-><init>()V

    sput-object v1, LX/1dY;->i:LX/32A;

    .line 283249
    :cond_0
    sget-object v1, LX/1dY;->i:LX/32A;

    iput-object p1, v1, LX/32A;->a:Landroid/view/View;

    .line 283250
    sget-object v1, LX/1dY;->i:LX/32A;

    iput-object p2, v1, LX/32A;->b:Landroid/view/accessibility/AccessibilityEvent;

    .line 283251
    iget-object v1, v0, LX/1dQ;->a:LX/1X1;

    .line 283252
    iget-object p0, v1, LX/1X1;->e:LX/1S3;

    move-object v1, p0

    .line 283253
    sget-object p0, LX/1dY;->i:LX/32A;

    invoke-virtual {v1, v0, p0}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283254
    :cond_1
    return-void
.end method
