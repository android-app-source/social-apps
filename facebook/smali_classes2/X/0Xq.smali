.class public final LX/0Xq;
.super LX/0Xr;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Xr",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public transient a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 79605
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, LX/0Xr;-><init>(Ljava/util/Map;)V

    .line 79606
    const/4 v0, 0x3

    iput v0, p0, LX/0Xq;->a:I

    .line 79607
    return-void
.end method

.method private constructor <init>(II)V
    .locals 1

    .prologue
    .line 79608
    invoke-static {p1}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0Xr;-><init>(Ljava/util/Map;)V

    .line 79609
    const-string v0, "expectedValuesPerKey"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 79610
    iput p2, p0, LX/0Xq;->a:I

    .line 79611
    return-void
.end method

.method public constructor <init>(LX/0Xu;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 79612
    invoke-interface {p1}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    instance-of v0, p1, LX/0Xq;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/0Xq;

    iget v0, v0, LX/0Xq;->a:I

    :goto_0
    invoke-direct {p0, v1, v0}, LX/0Xq;-><init>(II)V

    .line 79613
    invoke-virtual {p0, p1}, LX/0Xq;->a(LX/0Xu;)Z

    .line 79614
    return-void

    .line 79615
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 79597
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 79598
    const/4 v0, 0x3

    iput v0, p0, LX/0Xq;->a:I

    .line 79599
    invoke-static {p1}, LX/50X;->a(Ljava/io/ObjectInputStream;)I

    move-result v0

    .line 79600
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 79601
    invoke-virtual {p0, v1}, LX/0Xs;->a(Ljava/util/Map;)V

    .line 79602
    invoke-static {p0, p1, v0}, LX/50X;->a(LX/0Xu;Ljava/io/ObjectInputStream;I)V

    .line 79603
    return-void
.end method

.method public static t()LX/0Xq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Xq",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 79616
    new-instance v0, LX/0Xq;

    invoke-direct {v0}, LX/0Xq;-><init>()V

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 79617
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 79618
    invoke-static {p0, p1}, LX/50X;->a(LX/0Xu;Ljava/io/ObjectOutputStream;)V

    .line 79619
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 79620
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, LX/0Xq;->a:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public final bridge synthetic a(LX/0Xu;)Z
    .locals 1

    .prologue
    .line 79621
    invoke-super {p0, p1}, LX/0Xr;->a(LX/0Xu;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79622
    invoke-super {p0, p1, p2}, LX/0Xr;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79623
    invoke-super {p0, p1, p2}, LX/0Xr;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic c()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 79624
    invoke-virtual {p0}, LX/0Xq;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79604
    invoke-super {p0, p1, p2}, LX/0Xr;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()I
    .locals 1

    .prologue
    .line 79596
    invoke-super {p0}, LX/0Xr;->f()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79595
    invoke-super {p0, p1}, LX/0Xr;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic g()V
    .locals 0

    .prologue
    .line 79594
    invoke-super {p0}, LX/0Xr;->g()V

    return-void
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79593
    invoke-super {p0, p1}, LX/0Xr;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 79592
    invoke-super {p0}, LX/0Xr;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 79591
    invoke-super {p0}, LX/0Xr;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 79590
    invoke-super {p0}, LX/0Xr;->k()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n()Z
    .locals 1

    .prologue
    .line 79586
    invoke-super {p0}, LX/0Xr;->n()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic p()Ljava/util/Set;
    .locals 1

    .prologue
    .line 79589
    invoke-super {p0}, LX/0Xr;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()LX/1M1;
    .locals 1

    .prologue
    .line 79588
    invoke-super {p0}, LX/0Xr;->q()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79587
    invoke-super {p0}, LX/0Xr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
