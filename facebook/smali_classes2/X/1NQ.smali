.class public final LX/1NQ;
.super LX/1NR;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/fragment/NewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/fragment/NewsFeedFragment;)V
    .locals 0

    .prologue
    .line 237392
    iput-object p1, p0, LX/1NQ;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-direct {p0}, LX/1NR;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 237393
    check-cast p1, LX/1Nf;

    .line 237394
    iget-object v0, p1, LX/1Nf;->a:Lcom/facebook/graphql/model/FeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 237395
    iget-object v0, p0, LX/1NQ;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    iget-object v0, p1, LX/1Nf;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0fz;->c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 237396
    if-eqz v0, :cond_0

    .line 237397
    iget-object v1, p0, LX/1NQ;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v1, v1, LX/1Aw;->C:LX/1CY;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 237398
    :cond_0
    :goto_0
    return-void

    .line 237399
    :cond_1
    iget-object v0, p0, LX/1NQ;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->B:LX/0jU;

    iget-object v1, p1, LX/1Nf;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0, v1}, LX/0jU;->c(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 237400
    iget-object v0, p0, LX/1NQ;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->C:LX/1CY;

    iget-object v1, p0, LX/1NQ;->a:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v1

    iget-object v2, p1, LX/1Nf;->a:Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CY;->a(Ljava/lang/Iterable;)V

    goto :goto_0
.end method
