.class public LX/1lI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;I)LX/0w5;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 311410
    invoke-static {p0, p1, v0, v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I

    move-result v0

    .line 311411
    packed-switch v0, :pswitch_data_0

    .line 311412
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown format: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 311413
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    .line 311414
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 311415
    invoke-static {v0}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
