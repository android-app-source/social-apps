.class public LX/0Su;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .prologue
    .line 62296
    invoke-static {}, LX/0Sv;->a()V

    .line 62297
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62298
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 62299
    return-void
.end method

.method public static a(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/NormalNewExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62300
    const-string v0, "NormalNewExecutor"

    iget v1, p0, LX/0TJ;->b:I

    const/16 v2, 0x100

    const-string v3, "Shared"

    invoke-virtual {p0, v3}, LX/0TJ;->c(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0TJ;LX/0Sj;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/LightSharedPrefExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62301
    const-string v0, "LightSharedPrefExecutor"

    const/4 v1, 0x2

    const/16 v2, 0x100

    const-string v3, "LightSP"

    invoke-virtual {p0, v3}, LX/0TJ;->c(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .param p0    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/common/executors/ImageDecodeExecutorService;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/ImageDecodeExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62302
    const-string v0, "ImageDecode"

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, 0x7fffffff

    const-string v3, "ImgDecode"

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v3, v4}, LX/0TJ;->a(Ljava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p2

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p3, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/ThreadPoolExecutor;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .param p0    # Ljava/util/concurrent/ThreadPoolExecutor;
        .annotation runtime Lcom/facebook/common/executors/SharedThreadPool;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ThreadPoolExecutor;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62303
    const-string v0, "DefaultExecutor"

    const/16 v1, 0x8

    const/16 v2, 0x100

    move-object v3, p0

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Or;)LX/0Tf;
    .locals 2
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "LX/0Tf;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62304
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 62305
    new-instance v1, LX/0Td;

    invoke-direct {v1, v0}, LX/0Td;-><init>(Landroid/os/Handler;)V

    invoke-static {p0, v1}, LX/0TW;->a(LX/0Or;LX/0Tf;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Or;LX/0So;)LX/0Tf;
    .locals 3
    .annotation runtime Lcom/facebook/common/executors/DelayedListeningExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0So;",
            ")",
            "LX/0Tf;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62306
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 62307
    new-instance v1, LX/44Q;

    new-instance v2, LX/0Td;

    invoke-direct {v2, v0}, LX/0Td;-><init>(Landroid/os/Handler;)V

    invoke-direct {v1, v2, p1}, LX/44Q;-><init>(LX/0Td;LX/0So;)V

    invoke-static {p0, v1}, LX/0TW;->a(LX/0Or;LX/0Tf;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/28q;LX/0Or;)LX/0Tf;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadWakeup;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/28q;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "LX/0Tf;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 62308
    invoke-virtual {p0, v0, v0}, LX/28q;->a(Ljava/lang/String;Landroid/os/Handler;)LX/2a2;

    move-result-object v0

    invoke-static {p1, v0}, LX/0TW;->a(LX/0Or;LX/0Tf;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Looper;LX/0Or;)LX/0Tf;
    .locals 2
    .param p0    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "LX/0Tf;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62309
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 62310
    new-instance v1, LX/0Td;

    invoke-direct {v1, v0}, LX/0Td;-><init>(Landroid/os/Handler;)V

    invoke-static {p1, v1}, LX/0TW;->a(LX/0Or;LX/0Tf;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;LX/0Sj;LX/0TR;)LX/1fW;
    .locals 1
    .param p0    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 62311
    new-instance v0, LX/1fW;

    invoke-direct {v0, p0, p1, p2}, LX/1fW;-><init>(Ljava/util/concurrent/Executor;LX/0Sj;LX/0TR;)V

    return-object v0
.end method

.method public static a(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1
    .param p0    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 62328
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v0
.end method

.method public static a(LX/0Zr;)Landroid/os/HandlerThread;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62312
    const-string v0, "BgHandler"

    invoke-virtual {p0, v0}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 62313
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 62314
    return-object v0
.end method

.method public static a()Landroid/os/Looper;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 62315
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/HandlerThread;)Landroid/os/Looper;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 62316
    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "Ljava/util/concurrent/ScheduledExecutorService;"
        }
    .end annotation

    .prologue
    .line 62294
    const-string v0, "SingleSch"

    invoke-virtual {p0, v0}, LX/0TJ;->g(Ljava/lang/String;)LX/0UB;

    move-result-object v0

    invoke-static {p1, v0}, LX/0TW;->a(LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0TJ;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/SharedThreadPool;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62319
    const-string v0, "Shared"

    invoke-virtual {p0, v0}, LX/0TJ;->b(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62320
    const-string v0, "BackgroundExecutor"

    iget v1, p0, LX/0TJ;->a:I

    const/16 v2, 0x100

    const-string v3, "Shared"

    invoke-virtual {p0, v3}, LX/0TJ;->a(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Or;)LX/0Tf;
    .locals 2
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "LX/0Tf;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62321
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 62322
    new-instance v1, LX/0Wv;

    invoke-direct {v1, v0}, LX/0Wv;-><init>(Landroid/os/Handler;)V

    invoke-static {p0, v1}, LX/0TW;->a(LX/0Or;LX/0Tf;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1
    .param p0    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 62323
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v0
.end method

.method public static b()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/ImageDecodeExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 62324
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0TJ;)Ljava/util/concurrent/ExecutorService;
    .locals 11
    .annotation runtime Lcom/facebook/common/executors/AnalyticsThreadExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62325
    const-string v0, "AnalyticsThread"

    .line 62326
    new-instance v2, LX/0Xg;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-wide/16 v5, 0x3c

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, LX/0TO;

    sget-object v1, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-direct {v9, v0, v1}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    iget-object v10, p0, LX/0TJ;->e:LX/0Sj;

    invoke-direct/range {v2 .. v10}, LX/0Xg;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;LX/0Sj;)V

    invoke-static {v2}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    check-cast v1, LX/0Xg;

    move-object v0, v1

    .line 62327
    return-object v0
.end method

.method public static b(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/CpuSpinCheckerScheduledExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "Ljava/util/concurrent/ScheduledExecutorService;"
        }
    .end annotation

    .prologue
    .line 62295
    const-string v0, "CpuSpinDetector-"

    invoke-virtual {p0, v0}, LX/0TJ;->g(Ljava/lang/String;)LX/0UB;

    move-result-object v0

    invoke-static {p1, v0}, LX/0TW;->a(LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0TJ;)LX/0TD;
    .locals 2
    .annotation runtime Lcom/facebook/common/executors/VideoPerformanceExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62317
    const/4 v0, 0x3

    const-string v1, "video-perf-"

    invoke-virtual {p0, v0, v1}, LX/0TJ;->a(ILjava/lang/String;)LX/0Xg;

    move-result-object v0

    .line 62318
    invoke-static {v0}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62280
    const-string v0, "ForegroundExecutor"

    iget v1, p0, LX/0TJ;->c:I

    const/16 v2, 0x100

    const-string v3, "Shared"

    invoke-virtual {p0, v3}, LX/0TJ;->d(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "Ljava/util/concurrent/ScheduledExecutorService;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62277
    const-string v0, "Shared"

    .line 62278
    new-instance v1, LX/0UB;

    const/4 v2, 0x2

    new-instance v3, LX/0TO;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ScNorm_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0TP;->NORMAL:LX/0TP;

    invoke-direct {v3, v4, v5}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    iget-object v4, p0, LX/0TJ;->e:LX/0Sj;

    invoke-direct {v1, v2, v3, v4}, LX/0UB;-><init>(ILjava/util/concurrent/ThreadFactory;LX/0Sj;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    check-cast v1, LX/0UB;

    move-object v0, v1

    .line 62279
    invoke-static {p1, v0}, LX/0TW;->a(LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/FeedFetchExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62276
    const-string v0, "FeedFetchExecutor"

    iget v1, p0, LX/0TJ;->d:I

    const/16 v2, 0x100

    const-string v3, "Shared"

    invoke-virtual {p0, v3}, LX/0TJ;->e(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0TJ;)Ljava/util/concurrent/ExecutorService;
    .locals 2
    .annotation runtime Lcom/facebook/common/executors/PhotoUploadSerialExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62261
    const/4 v0, 0x1

    const-string v1, "photos-upload-"

    invoke-virtual {p0, v0, v1}, LX/0TJ;->a(ILjava/lang/String;)LX/0Xg;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "Ljava/util/concurrent/ScheduledExecutorService;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62272
    const-string v0, "Shared"

    .line 62273
    new-instance v1, LX/0UB;

    const/4 v2, 0x2

    new-instance v3, LX/0TO;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ScBg_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0TP;->BACKGROUND:LX/0TP;

    invoke-direct {v3, v4, v5}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    iget-object v4, p0, LX/0TJ;->e:LX/0Sj;

    invoke-direct {v1, v2, v3, v4}, LX/0UB;-><init>(ILjava/util/concurrent/ThreadFactory;LX/0Sj;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    check-cast v1, LX/0UB;

    move-object v0, v1

    .line 62274
    invoke-static {p1, v0}, LX/0TW;->a(LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/1fX;
    .locals 2
    .annotation runtime Lcom/facebook/common/executors/InboxLoaderExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/1fX;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62269
    new-instance v0, LX/1fW;

    const-string v1, "InboxLoader"

    invoke-virtual {p0, v1}, LX/0TJ;->h(Ljava/lang/String;)LX/0UB;

    move-result-object v1

    invoke-direct {v0, v1, p1, p3}, LX/1fW;-><init>(Ljava/util/concurrent/Executor;LX/0Sj;LX/0TR;)V

    .line 62270
    new-instance v1, LX/44U;

    invoke-direct {v1, v0, p2}, LX/44U;-><init>(LX/1fX;LX/0Or;)V

    move-object v0, v1

    .line 62271
    return-object v0
.end method

.method public static e(LX/0TJ;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/VideoServerHttpServiceExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62268
    const-string v0, "VideoServerThread"

    invoke-virtual {p0, v0}, LX/0TJ;->b(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/MqttClientScheduledExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "Ljava/util/concurrent/ScheduledExecutorService;"
        }
    .end annotation

    .prologue
    .line 62265
    const-string v0, "MqttClient"

    .line 62266
    new-instance v1, LX/0UB;

    iget v2, p0, LX/0TJ;->c:I

    new-instance v3, LX/0TO;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ScUg_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0TP;->URGENT:LX/0TP;

    invoke-direct {v3, v4, v5}, LX/0TO;-><init>(Ljava/lang/String;LX/0TP;)V

    iget-object v4, p0, LX/0TJ;->e:LX/0Sj;

    invoke-direct {v1, v2, v3, v4}, LX/0UB;-><init>(ILjava/util/concurrent/ThreadFactory;LX/0Sj;)V

    invoke-static {v1}, LX/0TQ;->a(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    check-cast v1, LX/0UB;

    move-object v0, v1

    .line 62267
    invoke-static {p1, v0}, LX/0TW;->a(LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/MqttClientSingleThreadExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .prologue
    .line 62264
    const-string v0, "MqttClientSingleThreadExecutorService"

    const/4 v1, 0x1

    const/16 v2, 0x100

    const-string v3, "MqttClient"

    invoke-static {v3}, LX/0TJ;->f(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/WhistleSingleThreadExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "Ljava/util/concurrent/ScheduledExecutorService;"
        }
    .end annotation

    .prologue
    .line 62263
    const-string v0, "whistle"

    invoke-virtual {p0, v0}, LX/0TJ;->h(Ljava/lang/String;)LX/0UB;

    move-result-object v0

    invoke-static {p1, v0}, LX/0TW;->a(LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/UrgentSingleThreadExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .prologue
    .line 62262
    const-string v0, "UrgentSingleThreadExecutorService"

    const/4 v1, 0x1

    const/16 v2, 0x100

    const-string v3, "UrgentSingleThread"

    invoke-static {v3}, LX/0TJ;->f(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/ResourceNetworkRequestExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62281
    const-string v0, "ResourceFetch"

    const/4 v1, 0x6

    const v2, 0x7fffffff

    const-string v3, "ResourceFetch"

    invoke-virtual {p0, v3}, LX/0TJ;->b(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/BrowserBackgroundRequestExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62282
    const-string v0, "BrowserBackground"

    const/4 v1, 0x6

    const v2, 0x7fffffff

    const-string v3, "BrowserBackground"

    invoke-virtual {p0, v3}, LX/0TJ;->a(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static j(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/ImageNetworkRequestExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 62275
    const-string v0, "ImageFetch"

    const v2, 0x7fffffff

    const-string v3, "ImgFetch"

    invoke-static {v3, v1}, LX/0TJ;->a(Ljava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static k(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/1Fx;
    .locals 9
    .annotation runtime Lcom/facebook/common/executors/ImageCacheRequestExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/1Fx;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 62283
    const-string v0, "ImageCache"

    const-string v1, "ImgCache"

    invoke-static {v1, v2}, LX/0TJ;->a(Ljava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    .line 62284
    new-instance v3, LX/1Fw;

    move-object v4, v0

    move v5, v2

    move-object v6, v1

    move-object v7, p1

    move-object v8, p3

    invoke-direct/range {v3 .. v8}, LX/1Fw;-><init>(Ljava/lang/String;ILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)V

    move-object v0, v3

    .line 62285
    new-instance v1, LX/1G2;

    invoke-direct {v1, v0, p2}, LX/1G2;-><init>(LX/1Fx;LX/0Or;)V

    move-object v0, v1

    .line 62286
    return-object v0
.end method

.method public static l(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/ImageTransformExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 62287
    const-string v0, "ImageTransform"

    const v2, 0x7fffffff

    const-string v3, "ImgTrans"

    invoke-static {v3, v1}, LX/0TJ;->a(Ljava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static m(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/ImageOffUiThreadExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 62288
    const-string v0, "ImageOffUiThread"

    const v2, 0x7fffffff

    const-string v3, "ImgOffUI"

    invoke-static {v3, v1}, LX/0TJ;->a(Ljava/lang/String;I)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static n(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/SearchRequestExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62289
    const-string v0, "UberSearch"

    const/16 v1, 0x1e

    const v2, 0x7fffffff

    const-string v3, "UberSearch"

    invoke-virtual {p0, v3}, LX/0TJ;->d(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static o(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TV;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/SearchTypeaheadNetworkExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TV;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62290
    const-string v0, "TypeaheadSearch"

    const/4 v1, 0x2

    const v2, 0x7fffffff

    const-string v3, "TypeaheadSearch"

    invoke-virtual {p0, v3}, LX/0TJ;->d(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static p(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/StorylineMuxerExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62291
    const-string v0, "StorylineMuxerExecutor"

    const/4 v1, 0x1

    const/16 v2, 0x100

    const-string v3, "SlMuxer"

    invoke-virtual {p0, v3}, LX/0TJ;->h(Ljava/lang/String;)LX/0UB;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static q(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;
    .locals 6
    .annotation runtime Lcom/facebook/common/executors/SendMessageAsyncExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")",
            "LX/0TD;"
        }
    .end annotation

    .prologue
    .line 62292
    const-string v0, "SendMessageExecutor"

    const/4 v1, 0x5

    const/16 v2, 0x100

    const-string v3, "SendMessageExecutor"

    invoke-virtual {p0, v3}, LX/0TJ;->a(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    move-object v4, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {p2, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 62293
    return-void
.end method
