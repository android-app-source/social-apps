.class public LX/0xC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0xC;


# instance fields
.field private final a:LX/0W3;

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 162216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162217
    iput-object p1, p0, LX/0xC;->b:LX/0ad;

    .line 162218
    iput-object p2, p0, LX/0xC;->a:LX/0W3;

    .line 162219
    return-void
.end method

.method public static a(LX/0QB;)LX/0xC;
    .locals 5

    .prologue
    .line 162220
    sget-object v0, LX/0xC;->c:LX/0xC;

    if-nez v0, :cond_1

    .line 162221
    const-class v1, LX/0xC;

    monitor-enter v1

    .line 162222
    :try_start_0
    sget-object v0, LX/0xC;->c:LX/0xC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 162223
    if-eqz v2, :cond_0

    .line 162224
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 162225
    new-instance p0, LX/0xC;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {p0, v3, v4}, LX/0xC;-><init>(LX/0ad;LX/0W3;)V

    .line 162226
    move-object v0, p0

    .line 162227
    sput-object v0, LX/0xC;->c:LX/0xC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162228
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 162229
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 162230
    :cond_1
    sget-object v0, LX/0xC;->c:LX/0xC;

    return-object v0

    .line 162231
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 162232
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 162233
    iget-object v1, p0, LX/0xC;->b:LX/0ad;

    sget-short v2, LX/0xb;->m:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 162234
    iget-object v2, p0, LX/0xC;->a:LX/0W3;

    sget-wide v4, LX/0X5;->jO:J

    invoke-interface {v2, v4, v5, v0}, LX/0W4;->a(JZ)Z

    move-result v2

    .line 162235
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
