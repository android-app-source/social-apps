.class public LX/0jd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0T3;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/base/activity/FbActivityOverrider;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/app/Activity;

.field private f:LX/0jn;

.field private g:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0kH;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0T3;",
            ">;",
            "LX/0kH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 124012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124013
    iput-boolean v0, p0, LX/0jd;->a:Z

    .line 124014
    iput v0, p0, LX/0jd;->g:I

    .line 124015
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 124016
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, LX/0jd;->c:Ljava/util/Set;

    .line 124017
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, LX/0jd;->d:Ljava/util/Set;

    .line 124018
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, LX/0jd;->b:Ljava/util/Set;

    .line 124019
    monitor-enter p0

    .line 124020
    :try_start_0
    invoke-static {p0, p2}, LX/0jd;->c(LX/0jd;LX/0T2;)V

    .line 124021
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124022
    invoke-static {p0, v0}, LX/0jd;->c(LX/0jd;LX/0T2;)V

    goto :goto_0

    .line 124023
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static b(LX/0QB;)LX/0jd;
    .locals 4

    .prologue
    .line 124309
    new-instance v1, LX/0jd;

    .line 124310
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/0kF;

    invoke-direct {v3, p0}, LX/0kF;-><init>(LX/0QB;)V

    invoke-direct {v0, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v0

    .line 124311
    invoke-static {p0}, LX/0kG;->b(LX/0QB;)LX/0kG;

    move-result-object v0

    check-cast v0, LX/0kH;

    invoke-direct {v1, v2, v0}, LX/0jd;-><init>(Ljava/util/Set;LX/0kH;)V

    .line 124312
    return-object v1
.end method

.method private static c(LX/0jd;LX/0T2;)V
    .locals 1

    .prologue
    .line 124313
    iget v0, p0, LX/0jd;->g:I

    if-nez v0, :cond_0

    .line 124314
    invoke-direct {p0, p1}, LX/0jd;->d(LX/0T2;)V

    .line 124315
    :goto_0
    return-void

    .line 124316
    :cond_0
    iget-object v0, p0, LX/0jd;->i:Ljava/util/List;

    if-nez v0, :cond_1

    .line 124317
    const/4 v0, 0x1

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0jd;->i:Ljava/util/List;

    .line 124318
    :cond_1
    iget-object v0, p0, LX/0jd;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private d(LX/0T2;)V
    .locals 2

    .prologue
    .line 124319
    iget-object v0, p0, LX/0jd;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124320
    instance-of v0, p1, LX/0T3;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 124321
    check-cast v0, LX/0T3;

    .line 124322
    iget-object v1, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124323
    :cond_0
    instance-of v0, p1, LX/0kI;

    if-eqz v0, :cond_1

    .line 124324
    check-cast p1, LX/0kI;

    .line 124325
    iget-object v0, p0, LX/0jd;->e:Landroid/app/Activity;

    iget-object v1, p0, LX/0jd;->f:LX/0jn;

    invoke-virtual {p1, v0, v1}, LX/0kI;->a(Landroid/app/Activity;LX/0jn;)V

    .line 124326
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124327
    :cond_1
    return-void
.end method

.method private static e(LX/0jd;LX/0T2;)V
    .locals 1

    .prologue
    .line 124328
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 124329
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 124330
    iget-object v0, p0, LX/0jd;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 124331
    return-void
.end method

.method private static declared-synchronized p(LX/0jd;)V
    .locals 1

    .prologue
    .line 124332
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0jd;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0jd;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124333
    monitor-exit p0

    return-void

    .line 124334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized q(LX/0jd;)V
    .locals 2

    .prologue
    .line 124335
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0jd;->g:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 124336
    iget v0, p0, LX/0jd;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0jd;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_2

    .line 124337
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 124338
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 124339
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/0jd;->h:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 124340
    iget-object v0, p0, LX/0jd;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 124341
    invoke-static {p0, v0}, LX/0jd;->e(LX/0jd;LX/0T2;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 124342
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 124343
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LX/0jd;->h:Ljava/util/List;

    .line 124344
    :cond_4
    iget-object v0, p0, LX/0jd;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 124345
    iget-object v0, p0, LX/0jd;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 124346
    invoke-direct {p0, v0}, LX/0jd;->d(LX/0T2;)V

    goto :goto_3

    .line 124347
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, LX/0jd;->i:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private static r(LX/0jd;)V
    .locals 2

    .prologue
    .line 124348
    iget-boolean v0, p0, LX/0jd;->a:Z

    if-nez v0, :cond_1

    .line 124349
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0jd;->a:Z

    .line 124350
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124351
    const-string v0, "FbActivityListeners.activate"

    const v1, -0x6ae85b6c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124352
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124353
    :catchall_0
    move-exception v0

    const v1, 0x26b277f5

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124354
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124355
    :cond_0
    const v0, 0x4310e3f5

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124356
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124357
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124358
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124359
    const-string v0, "FbActivityListeners.overrideOnCreatePanelView"

    const v1, 0x23df2646

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124360
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124361
    goto :goto_0

    .line 124362
    :goto_1
    return-object v0

    .line 124363
    :cond_0
    const v0, -0x7567cb6f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124364
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124365
    const/4 v0, 0x0

    goto :goto_1

    .line 124366
    :catchall_0
    move-exception v0

    const v1, 0x1127bf06

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124367
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(ILandroid/view/KeyEvent;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124368
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124369
    const-string v0, "FbActivityListeners.onKeyDown"

    const v1, -0x5d74fa42

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124370
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124371
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1, p2}, LX/0T3;->a(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    .line 124372
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124373
    const v1, -0x7e75034f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124374
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124375
    :goto_0
    return-object v0

    .line 124376
    :cond_1
    const v0, 0x4668d3a1

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124377
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124378
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 124379
    :catchall_0
    move-exception v0

    const v1, -0x347a5ad3    # -1.7517146E7f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124380
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(ILandroid/view/Menu;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/Menu;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124381
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124382
    const-string v0, "FbActivityListeners.overrideOnCreatePanelMenu"

    const v1, 0x7a8e0fde

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124383
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124384
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    move-object v0, v2

    .line 124385
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124386
    const v1, 0x5dc11a6a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124387
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124388
    :goto_0
    return-object v0

    .line 124389
    :cond_1
    const v0, -0xc8ff950

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124390
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124391
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 124392
    :catchall_0
    move-exception v0

    const v1, -0x42d91544

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124393
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(ILandroid/view/MenuItem;)LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/MenuItem;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124394
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124395
    const-string v0, "FbActivityListeners.overrideOnMenuItemSelected"

    const v1, 0x2e7ce3f1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124396
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124397
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    iget-object v3, p0, LX/0jd;->f:LX/0jn;

    invoke-virtual {v0, v2, v3, p1, p2}, LX/0kI;->a(Landroid/app/Activity;LX/0jn;ILandroid/view/MenuItem;)LX/0am;

    move-result-object v0

    .line 124398
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124399
    const v1, -0x29b3f2c8

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124400
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124401
    :goto_0
    return-object v0

    .line 124402
    :cond_1
    const v0, -0x5cbe4469

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124403
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124404
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 124405
    :catchall_0
    move-exception v0

    const v1, -0x5cea3fcf

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124406
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/Menu;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/View;",
            "Landroid/view/Menu;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124407
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124408
    const-string v0, "FbActivityListeners.overrideOnPreparePanel"

    const v1, -0x54357b5f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124409
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124410
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    move-object v0, v2

    .line 124411
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124412
    const v1, -0x14ee7558

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124413
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124414
    :goto_0
    return-object v0

    .line 124415
    :cond_1
    const v0, 0x3a73a4b8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124416
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124417
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 124418
    :catchall_0
    move-exception v0

    const v1, -0x463c2bd0

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124419
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 124518
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124519
    const-string v0, "FbActivityListeners.onActivityResult"

    const v1, -0x748c7ee9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124520
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124521
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1, p2, p3}, LX/0T3;->a(Landroid/app/Activity;IILandroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124522
    :catchall_0
    move-exception v0

    const v1, -0x7e464c4a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124523
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124524
    :cond_0
    const v0, 0x7c12f5b9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124525
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124526
    return-void
.end method

.method public final declared-synchronized a(LX/0T2;)V
    .locals 1

    .prologue
    .line 124420
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/0jd;->c(LX/0jd;LX/0T2;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124421
    monitor-exit p0

    return-void

    .line 124422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/app/Activity;LX/0jn;)V
    .locals 0

    .prologue
    .line 124515
    iput-object p1, p0, LX/0jd;->e:Landroid/app/Activity;

    .line 124516
    iput-object p2, p0, LX/0jd;->f:LX/0jn;

    .line 124517
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 124505
    invoke-static {p0}, LX/0jd;->r(LX/0jd;)V

    .line 124506
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124507
    const-string v0, "FbActivityListeners.onNewIntent"

    const v1, -0x60ffa97f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124508
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124509
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1}, LX/0T3;->a(Landroid/app/Activity;Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124510
    :catchall_0
    move-exception v0

    const v1, 0x5a904093

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124511
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124512
    :cond_0
    const v0, -0x67425f57

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124513
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124514
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 124496
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124497
    const-string v0, "FbActivityListeners.onConfigurationChanged"

    const v1, 0x43ca2664    # 404.29993f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124498
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124499
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1}, LX/0T3;->a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124500
    :catchall_0
    move-exception v0

    const v1, -0x3caa24c0

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124501
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124502
    :cond_0
    const v0, -0x6aaab015

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124503
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124504
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 124487
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124488
    const-string v0, "FbActivityListeners.onAttachFragment"

    const v1, -0x2766c614

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124489
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124490
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1}, LX/0T3;->a(Landroid/app/Activity;Landroid/support/v4/app/Fragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124491
    :catchall_0
    move-exception v0

    const v1, 0x316618b8

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124492
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124493
    :cond_0
    const v0, -0x31df90ff

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124494
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124495
    return-void
.end method

.method public final a(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 124479
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124480
    const-string v0, "FbActivityListeners.onCreateOptionsMenu"

    const v1, -0xbef2dba

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124481
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124482
    :catchall_0
    move-exception v0

    const v1, -0x447e0df5

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124483
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124484
    :cond_0
    const v0, -0x1d575c93

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124485
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124486
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 124471
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124472
    const-string v0, "FbActivityListeners.onTitleChanged"

    const v1, 0x5796d931

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124473
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124474
    :catchall_0
    move-exception v0

    const v1, 0x177efadf

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124475
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124476
    :cond_0
    const v0, -0x503818bc

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124477
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124478
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 124463
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124464
    const-string v0, "FbActivityListeners.onWindowFocusChanged"

    const v1, 0x8234501

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124465
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124466
    :catchall_0
    move-exception v0

    const v1, 0x6d57e419

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124467
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124468
    :cond_0
    const v0, -0x2f8f773f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124469
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124470
    return-void
.end method

.method public final a(ILandroid/app/Dialog;)Z
    .locals 2

    .prologue
    .line 124451
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124452
    const-string v0, "FbActivityListeners.onPrepareDialog"

    const v1, 0x28f71f59

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124453
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124454
    invoke-interface {v0, p1, p2}, LX/0T3;->a(ILandroid/app/Dialog;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124455
    const v0, 0x227114c5

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124456
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124457
    :goto_0
    return v0

    .line 124458
    :cond_1
    const v0, -0x6fc21b62

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124459
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124460
    const/4 v0, 0x0

    goto :goto_0

    .line 124461
    :catchall_0
    move-exception v0

    const v1, 0x1650a698

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124462
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 124435
    iget-object v0, p0, LX/0jd;->f:LX/0jn;

    if-nez v0, :cond_0

    .line 124436
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Call setActivitySuper() before activity callbacks"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124437
    :cond_0
    invoke-static {p0}, LX/0jd;->r(LX/0jd;)V

    .line 124438
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124439
    const-string v0, "FbActivityListeners.onBeforeSuperOnCreate"

    const v1, 0x3113b35b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124440
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124441
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1}, LX/0T3;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 124442
    iget-object v0, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 124443
    const v0, -0x52c1fe18

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124444
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124445
    :goto_0
    return v0

    .line 124446
    :cond_2
    const v0, -0x409e6f95

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124447
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124448
    const/4 v0, 0x0

    goto :goto_0

    .line 124449
    :catchall_0
    move-exception v0

    const v1, -0x478571b1

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124450
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 124423
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124424
    const-string v0, "FbActivityListeners.onOptionsItemSelected"

    const v1, 0x60c57313

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124425
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124426
    invoke-interface {v0}, LX/0T3;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124427
    const v0, 0x1bd49924

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124428
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124429
    :goto_0
    return v0

    .line 124430
    :cond_1
    const v0, -0x35d5e99f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124431
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124432
    const/4 v0, 0x0

    goto :goto_0

    .line 124433
    :catchall_0
    move-exception v0

    const v1, -0x3f556dc1

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124434
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 124285
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124286
    const-string v0, "FbActivityListeners.overrideSetContentView"

    const v1, 0x3d764933

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124287
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124288
    invoke-virtual {v0, p1}, LX/0kI;->a(Landroid/view/View;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124289
    const v0, 0x610a1ac9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124290
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124291
    :goto_0
    return v0

    .line 124292
    :cond_1
    const v0, -0x6faf7650

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124293
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124294
    const/4 v0, 0x0

    goto :goto_0

    .line 124295
    :catchall_0
    move-exception v0

    const v1, -0x317e1fcd

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124296
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2

    .prologue
    .line 124297
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124298
    const-string v0, "FbActivityListeners.overrideSetContentView"

    const v1, 0x78fa2d21

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124299
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124300
    invoke-virtual {v0, p1, p2}, LX/0kI;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124301
    const v0, 0x4355a496

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124302
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124303
    :goto_0
    return v0

    .line 124304
    :cond_1
    const v0, 0x385be1f2

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124305
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124306
    const/4 v0, 0x0

    goto :goto_0

    .line 124307
    :catchall_0
    move-exception v0

    const v1, 0x3915dda1

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124308
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 3

    .prologue
    .line 124000
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124001
    const-string v0, "FbActivityListeners.handleServiceException"

    const v1, -0x3f1993ec

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124002
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124003
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1}, LX/0T3;->a(Landroid/app/Activity;Ljava/lang/Throwable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124004
    const v0, 0x12bf60da

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124005
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124006
    :goto_0
    return v0

    .line 124007
    :cond_1
    const v0, -0x24061de6

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124008
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124009
    const/4 v0, 0x0

    goto :goto_0

    .line 124010
    :catchall_0
    move-exception v0

    const v1, -0x46a6d7cd

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124011
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final b(ILandroid/view/KeyEvent;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124024
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124025
    const-string v0, "FbActivityListeners.onKeyUp"

    const v1, -0xc61461b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124026
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124027
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1, p2}, LX/0T3;->b(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    .line 124028
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124029
    const v1, -0x74185d15

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124030
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124031
    :goto_0
    return-object v0

    .line 124032
    :cond_1
    const v0, 0x4ea065

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124033
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124034
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 124035
    :catchall_0
    move-exception v0

    const v1, -0x6b7a8687

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124036
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final declared-synchronized b(LX/0T2;)V
    .locals 1

    .prologue
    .line 124037
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0jd;->g:I

    if-nez v0, :cond_0

    .line 124038
    invoke-static {p0, p1}, LX/0jd;->e(LX/0jd;LX/0T2;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124039
    :goto_0
    monitor-exit p0

    return-void

    .line 124040
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0jd;->h:Ljava/util/List;

    if-nez v0, :cond_1

    .line 124041
    const/4 v0, 0x1

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0jd;->h:Ljava/util/List;

    .line 124042
    :cond_1
    iget-object v0, p0, LX/0jd;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124043
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 124044
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124045
    const-string v0, "FbActivityListeners.onPrepareOptionsMenu"

    const v1, -0x46b187cb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124046
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124047
    :catchall_0
    move-exception v0

    const v1, 0x23902005

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124048
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124049
    :cond_0
    const v0, -0x78459932

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124050
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124051
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 124052
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124053
    const-string v0, "FbActivityListeners.overrideInvalidateOptionsMenu"

    const v1, 0x141ea0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124054
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124055
    iget-object v2, p0, LX/0jd;->f:LX/0jn;

    invoke-virtual {v0, v2}, LX/0kI;->a(LX/0jn;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124056
    const v0, 0x27187e72

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124057
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124058
    :goto_0
    return v0

    .line 124059
    :cond_1
    const v0, 0x472d1a2b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124060
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124061
    const/4 v0, 0x0

    goto :goto_0

    .line 124062
    :catchall_0
    move-exception v0

    const v1, -0x4e71dad2

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124063
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 124064
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124065
    const-string v0, "FbActivityListeners.overrideSetContentView"

    const v1, -0x29962224

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124066
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124067
    invoke-virtual {v0, p1}, LX/0kI;->b(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124068
    const v0, 0x22d4c949

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124069
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124070
    :goto_0
    return v0

    .line 124071
    :cond_1
    const v0, 0x64bb78f7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124072
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124073
    const/4 v0, 0x0

    goto :goto_0

    .line 124074
    :catchall_0
    move-exception v0

    const v1, -0x20b846e4

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124075
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final b(Landroid/os/Bundle;)Z
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 124076
    invoke-static {p0}, LX/0jd;->r(LX/0jd;)V

    .line 124077
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124078
    const-string v0, "FbActivityListeners.onBeforeActivityCreate"

    const v1, 0x680ce1ef

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124079
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124080
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124081
    const v0, 0x7cc4d01b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124082
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124083
    :goto_1
    return v0

    .line 124084
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x151fae54

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124085
    :try_start_2
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2, p1}, LX/0T3;->b(Landroid/app/Activity;Landroid/os/Bundle;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 124086
    const v0, 0x13509487

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 124087
    :catchall_0
    move-exception v0

    const v1, -0x65f6e1a2

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124088
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124089
    :catchall_1
    move-exception v0

    const v1, -0x11fb03a2

    :try_start_4
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 124090
    :cond_1
    const v0, 0x9ae8207

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124091
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124092
    iget-object v0, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    goto :goto_1
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2

    .prologue
    .line 124093
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124094
    const-string v0, "FbActivityListeners.addContentView"

    const v1, -0x71ec6a1c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124095
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124096
    invoke-virtual {v0, p1, p2}, LX/0kI;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124097
    const v0, -0x6bfe707c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124098
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124099
    :goto_0
    return v0

    .line 124100
    :cond_1
    const v0, 0x49ec9470    # 1938062.0f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124101
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124102
    const/4 v0, 0x0

    goto :goto_0

    .line 124103
    :catchall_0
    move-exception v0

    const v1, -0x463bbace

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124104
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final c(I)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 124105
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124106
    const-string v0, "FbActivityListeners.onCreateDialog"

    const v1, 0x58f27650

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124107
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124108
    invoke-interface {v0, p1}, LX/0T3;->a(I)Landroid/app/Dialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 124109
    if-eqz v0, :cond_0

    .line 124110
    const v1, -0x4488affe

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124111
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124112
    :goto_0
    return-object v0

    .line 124113
    :cond_1
    const v0, 0x220a5f2d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124114
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124115
    const/4 v0, 0x0

    goto :goto_0

    .line 124116
    :catchall_0
    move-exception v0

    const v1, 0x7f5c29bd

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124117
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final c()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 124118
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124119
    const-string v0, "FbActivityListeners.overrideGetMenuInflater"

    const v1, 0x3d1f0ac8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124120
    :try_start_0
    iget-object v0, p0, LX/0jd;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kI;

    .line 124121
    invoke-virtual {v0}, LX/0kI;->c()Landroid/view/MenuInflater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 124122
    if-eqz v0, :cond_0

    .line 124123
    const v1, 0x3463c0fa

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124124
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124125
    :goto_0
    return-object v0

    .line 124126
    :cond_1
    const v0, -0x26ef823e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124127
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124128
    const/4 v0, 0x0

    goto :goto_0

    .line 124129
    :catchall_0
    move-exception v0

    const v1, 0x7df20607

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124130
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 124131
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124132
    const-string v0, "FbActivityListeners.onSaveInstanceState"

    const v1, -0x200ad486

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124133
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124134
    invoke-interface {v0, p1}, LX/0T3;->b(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124135
    :catchall_0
    move-exception v0

    const v1, -0x24c13d07

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124136
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124137
    :cond_0
    const v0, 0x483f7c12

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124138
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124139
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 124140
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124141
    const-string v0, "FbActivityListeners.onActivityCreate"

    const v1, -0x58d19b46

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124142
    :try_start_0
    iget-object v0, p0, LX/0jd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 124143
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124144
    const v0, -0x1bff24e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124145
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124146
    :goto_1
    return-void

    .line 124147
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, -0x15fdf8cc

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124148
    :try_start_2
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T2;->a(Landroid/app/Activity;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 124149
    const v0, 0x2a830823

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 124150
    :catchall_0
    move-exception v0

    const v1, 0x19582e4a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124151
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124152
    :catchall_1
    move-exception v0

    const v1, -0x2c322a53

    :try_start_4
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 124153
    :cond_1
    const v0, -0x20d88356

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124154
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    goto :goto_1
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 124155
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124156
    const-string v0, "FbActivityListeners.onTrimMemory"

    const v1, 0x7338c4ab

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124157
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124158
    :catchall_0
    move-exception v0

    const v1, 0x4ad774cf    # 7060071.5f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124159
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124160
    :cond_0
    const v0, 0x4c409272    # 5.0481608E7f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124161
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124162
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 124163
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124164
    const-string v0, "FbActivityListeners.onPostCreate"

    const v1, -0x5b9cdf51

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124165
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124166
    invoke-interface {v0, p1}, LX/0T3;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124167
    :catchall_0
    move-exception v0

    const v1, -0x686ad482

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124168
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124169
    :cond_0
    const v0, 0x534c9d06

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124170
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124171
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 124172
    invoke-static {p0}, LX/0jd;->r(LX/0jd;)V

    .line 124173
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124174
    const-string v0, "FbActivityListeners.onStart"

    const v1, -0x1e17b83

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124175
    :try_start_0
    iget-object v0, p0, LX/0jd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 124176
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x4ac1f410    # 6355464.0f

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124177
    :try_start_1
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T2;->b(Landroid/app/Activity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 124178
    const v0, -0x75e3785f

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 124179
    :catchall_0
    move-exception v0

    const v1, -0xb7a5b37

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124180
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124181
    :catchall_1
    move-exception v0

    const v1, 0x4c82a5bc    # 6.8496864E7f

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124182
    :cond_0
    const v0, 0x7dd41516

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124183
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124184
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 124185
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124186
    const-string v0, "FbActivityListeners.onStop"

    const v1, 0x5c8f441f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124187
    :try_start_0
    iget-object v0, p0, LX/0jd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 124188
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T2;->e(Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124189
    :catchall_0
    move-exception v0

    const v1, -0x2e91def3

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124190
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124191
    :cond_0
    const v0, -0x21da0a3b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124192
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124193
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 124194
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0jd;->a:Z

    .line 124195
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124196
    const-string v0, "FbActivityListeners.onPause"

    const v1, -0x1db4e807

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124197
    :try_start_0
    iget-object v0, p0, LX/0jd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 124198
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T2;->d(Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124199
    :catchall_0
    move-exception v0

    const v1, -0xd5112ad

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124200
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124201
    :cond_0
    const v0, -0x3f75b73a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124202
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124203
    return-void
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 124204
    invoke-static {p0}, LX/0jd;->r(LX/0jd;)V

    .line 124205
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124206
    const-string v0, "FbActivityListeners.onResume"

    const v1, -0x78464559

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124207
    :try_start_0
    iget-object v0, p0, LX/0jd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 124208
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x484114ee

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124209
    :try_start_1
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T2;->c(Landroid/app/Activity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 124210
    const v0, -0x22ba956e

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 124211
    :catchall_0
    move-exception v0

    const v1, 0x4eb2a58

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124212
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124213
    :catchall_1
    move-exception v0

    const v1, 0xab0e819

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124214
    :cond_0
    const v0, 0x60fdb78f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124215
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124216
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 124217
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124218
    const-string v0, "FbActivityListeners.onResumeFragments"

    const v1, 0x19e18a8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124219
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124220
    :catchall_0
    move-exception v0

    const v1, -0x679caab1

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124221
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124222
    :cond_0
    const v0, 0x271f1c8e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124223
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124224
    return-void
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 124225
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124226
    const-string v0, "FbActivityListeners.onDestroy"

    const v1, 0x24bc1d30

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124227
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124228
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T2;->f(Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124229
    :catchall_0
    move-exception v0

    const v1, -0xb1c575a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124230
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124231
    :cond_0
    const v0, 0x55a8255

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124232
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124233
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 124234
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124235
    const-string v0, "FbActivityListeners.onUserInteraction"

    const v1, -0x69290ee5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124236
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124237
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T3;->h(Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124238
    :catchall_0
    move-exception v0

    const v1, -0x691a0c17

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124239
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124240
    :cond_0
    const v0, 0x72283c44

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124241
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124242
    return-void
.end method

.method public final l()LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124243
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124244
    const-string v0, "FbActivityListeners.onSearchRequest"

    const v1, 0x180dba0c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124245
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124246
    invoke-interface {v0}, LX/0T3;->a()LX/0am;

    move-result-object v0

    .line 124247
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 124248
    const v1, -0x14752c96

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124249
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124250
    :goto_0
    return-object v0

    .line 124251
    :cond_1
    const v0, 0x42c60760

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124252
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124253
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 124254
    :catchall_0
    move-exception v0

    const v1, 0x5bc94a

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124255
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 124256
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124257
    const-string v0, "FbActivityListeners.finish"

    const v1, -0x10b8ba5a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124258
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124259
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T3;->g(Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124260
    :catchall_0
    move-exception v0

    const v1, -0x795bdae8

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124261
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124262
    :cond_0
    const v0, -0x11840a80

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124263
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124264
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 124265
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124266
    const-string v0, "FbActivityListeners.onContentCreated"

    const v1, -0x2495d2d4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124267
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124268
    :catchall_0
    move-exception v0

    const v1, 0x2f45abc2

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124269
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0

    .line 124270
    :cond_0
    const v0, 0x7a3b642a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124271
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124272
    return-void
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 124273
    invoke-static {p0}, LX/0jd;->p(LX/0jd;)V

    .line 124274
    const-string v0, "FbActivityListeners.onBackPressed"

    const v1, -0x17b58e8f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 124275
    :try_start_0
    iget-object v0, p0, LX/0jd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T3;

    .line 124276
    iget-object v2, p0, LX/0jd;->e:Landroid/app/Activity;

    invoke-interface {v0, v2}, LX/0T3;->i(Landroid/app/Activity;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124277
    const v0, 0x5a1c68d8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124278
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    const/4 v0, 0x1

    .line 124279
    :goto_0
    return v0

    .line 124280
    :cond_1
    const v0, -0x4d8307de

    invoke-static {v0}, LX/02m;->a(I)V

    .line 124281
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    .line 124282
    const/4 v0, 0x0

    goto :goto_0

    .line 124283
    :catchall_0
    move-exception v0

    const v1, 0x2f2513a3

    invoke-static {v1}, LX/02m;->a(I)V

    .line 124284
    invoke-static {p0}, LX/0jd;->q(LX/0jd;)V

    throw v0
.end method
