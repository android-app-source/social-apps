.class public LX/1Fg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;
.implements LX/1Fh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0rf;",
        "LX/1Fh",
        "<TK;TV;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:J
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field public final b:LX/1HP;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1HP",
            "<TK;",
            "LX/1lX",
            "<TK;TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final c:LX/1HP;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1HP",
            "<TK;",
            "LX/1lX",
            "<TK;TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public e:LX/1HR;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final f:LX/1HM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1HM",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final g:LX/1HO;

.field private final h:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "LX/1HR;",
            ">;"
        }
    .end annotation
.end field

.field private i:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 224421
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/1Fg;->a:J

    return-void
.end method

.method public constructor <init>(LX/1HM;LX/1HO;LX/1Gd;LX/1FZ;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1HM",
            "<TV;>;",
            "LX/1HO;",
            "LX/1Gd",
            "<",
            "LX/1HR;",
            ">;",
            "LX/1FZ;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 224422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224423
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/1Fg;->d:Ljava/util/Map;

    .line 224424
    iput-object p1, p0, LX/1Fg;->f:LX/1HM;

    .line 224425
    new-instance v0, LX/1HP;

    invoke-direct {p0, p1}, LX/1Fg;->a(LX/1HM;)LX/1HM;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1HP;-><init>(LX/1HM;)V

    iput-object v0, p0, LX/1Fg;->b:LX/1HP;

    .line 224426
    new-instance v0, LX/1HP;

    invoke-direct {p0, p1}, LX/1Fg;->a(LX/1HM;)LX/1HM;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1HP;-><init>(LX/1HM;)V

    iput-object v0, p0, LX/1Fg;->c:LX/1HP;

    .line 224427
    iput-object p2, p0, LX/1Fg;->g:LX/1HO;

    .line 224428
    iput-object p3, p0, LX/1Fg;->h:LX/1Gd;

    .line 224429
    iget-object v0, p0, LX/1Fg;->h:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HR;

    iput-object v0, p0, LX/1Fg;->e:LX/1HR;

    .line 224430
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/1Fg;->i:J

    .line 224431
    if-eqz p5, :cond_0

    .line 224432
    new-instance v0, LX/1Ff;

    invoke-direct {v0, p0}, LX/1Ff;-><init>(LX/1Fg;)V

    .line 224433
    sget-object v1, LX/1FZ;->a:LX/1Ff;

    if-nez v1, :cond_0

    .line 224434
    sput-object v0, LX/1FZ;->a:LX/1Ff;

    .line 224435
    :cond_0
    return-void
.end method

.method private declared-synchronized a(LX/1lX;)LX/1FJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lX",
            "<TK;TV;>;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 224436
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LX/1Fg;->g(LX/1lX;)V

    .line 224437
    iget-object v0, p1, LX/1lX;->b:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, LX/1lY;

    invoke-direct {v1, p0, p1}, LX/1lY;-><init>(LX/1Fg;LX/1lX;)V

    invoke-static {v0, v1}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 224438
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LX/1HM;)LX/1HM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1HM",
            "<TV;>;)",
            "LX/1HM",
            "<",
            "LX/1lX",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 224439
    new-instance v0, LX/1HQ;

    invoke-direct {v0, p0, p1}, LX/1HQ;-><init>(LX/1Fg;LX/1HM;)V

    return-object v0
.end method

.method private declared-synchronized a(II)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "LX/1lX",
            "<TK;TV;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224440
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 224441
    const/4 v0, 0x0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 224442
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->a()I

    move-result v0

    if-gt v0, v1, :cond_1

    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gt v0, v2, :cond_1

    .line 224443
    const/4 v0, 0x0

    .line 224444
    :cond_0
    monitor-exit p0

    return-object v0

    .line 224445
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 224446
    :goto_0
    iget-object v3, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v3}, LX/1HP;->a()I

    move-result v3

    if-gt v3, v1, :cond_2

    iget-object v3, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v3}, LX/1HP;->b()I

    move-result v3

    if-le v3, v2, :cond_0

    .line 224447
    :cond_2
    iget-object v3, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v3}, LX/1HP;->c()Ljava/lang/Object;

    move-result-object v3

    .line 224448
    iget-object v4, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v4, v3}, LX/1HP;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224449
    iget-object v4, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v4, v3}, LX/1HP;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 224450
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/1lX",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 224451
    if-eqz p1, :cond_0

    .line 224452
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lX;

    .line 224453
    invoke-static {p0, v0}, LX/1Fg;->i(LX/1Fg;LX/1lX;)LX/1FJ;

    move-result-object v0

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 224454
    :cond_0
    return-void
.end method

.method public static b(LX/1Fg;LX/1lX;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lX",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 224455
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224456
    monitor-enter p0

    .line 224457
    :try_start_0
    invoke-static {p0, p1}, LX/1Fg;->h(LX/1Fg;LX/1lX;)V

    .line 224458
    invoke-static {p0, p1}, LX/1Fg;->c(LX/1Fg;LX/1lX;)Z

    move-result v0

    .line 224459
    invoke-static {p0, p1}, LX/1Fg;->i(LX/1Fg;LX/1lX;)LX/1FJ;

    move-result-object v1

    .line 224460
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224461
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 224462
    if-eqz v0, :cond_1

    .line 224463
    :goto_0
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1lX;->e:LX/4dc;

    if-eqz v0, :cond_0

    .line 224464
    iget-object v0, p1, LX/1lX;->e:LX/4dc;

    iget-object v1, p1, LX/1lX;->a:Ljava/lang/Object;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/4dc;->a(Ljava/lang/Object;Z)V

    .line 224465
    :cond_0
    invoke-static {p0}, LX/1Fg;->e(LX/1Fg;)V

    .line 224466
    invoke-static {p0}, LX/1Fg;->f(LX/1Fg;)V

    .line 224467
    return-void

    .line 224468
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 224469
    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/util/ArrayList;)V
    .locals 2
    .param p0    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/1lX",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 224501
    if-eqz p0, :cond_0

    .line 224502
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lX;

    .line 224503
    invoke-static {v0}, LX/1Fg;->d(LX/1lX;)V

    goto :goto_0

    .line 224504
    :cond_0
    return-void
.end method

.method private declared-synchronized c(Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/1lX",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 224470
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 224471
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lX;

    .line 224472
    invoke-direct {p0, v0}, LX/1Fg;->f(LX/1lX;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 224473
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 224474
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized c(LX/1Fg;LX/1lX;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lX",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 224475
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p1, LX/1lX;->d:Z

    if-nez v0, :cond_0

    iget v0, p1, LX/1lX;->c:I

    if-nez v0, :cond_0

    .line 224476
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    iget-object v1, p1, LX/1lX;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, LX/1HP;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224477
    const/4 v0, 0x1

    .line 224478
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224479
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 224480
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->f:LX/1HM;

    invoke-interface {v0, p1}, LX/1HM;->a(Ljava/lang/Object;)I

    move-result v0

    .line 224481
    iget-object v1, p0, LX/1Fg;->e:LX/1HR;

    iget v1, v1, LX/1HR;->e:I

    if-gt v0, v1, :cond_0

    invoke-static {p0}, LX/1Fg;->g(LX/1Fg;)I

    move-result v1

    iget-object v2, p0, LX/1Fg;->e:LX/1HR;

    iget v2, v2, LX/1HR;->b:I

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_0

    invoke-static {p0}, LX/1Fg;->h(LX/1Fg;)I

    move-result v1

    iget-object v2, p0, LX/1Fg;->e:LX/1HR;

    iget v2, v2, LX/1HR;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int v0, v2, v0

    if-gt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224482
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static d(LX/1lX;)V
    .locals 3
    .param p0    # LX/1lX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1lX",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 224483
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/1lX;->e:LX/4dc;

    if-eqz v0, :cond_0

    .line 224484
    iget-object v0, p0, LX/1lX;->e:LX/4dc;

    iget-object v1, p0, LX/1lX;->a:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/4dc;->a(Ljava/lang/Object;Z)V

    .line 224485
    :cond_0
    return-void
.end method

.method private static declared-synchronized e(LX/1Fg;)V
    .locals 4

    .prologue
    .line 224486
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1Fg;->i:J

    sget-wide v2, LX/1Fg;->a:J

    add-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 224487
    :goto_0
    monitor-exit p0

    return-void

    .line 224488
    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/1Fg;->i:J

    .line 224489
    iget-object v0, p0, LX/1Fg;->h:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HR;

    iput-object v0, p0, LX/1Fg;->e:LX/1HR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 224490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static f(LX/1Fg;)V
    .locals 4

    .prologue
    .line 224491
    monitor-enter p0

    .line 224492
    :try_start_0
    iget-object v0, p0, LX/1Fg;->e:LX/1HR;

    iget v0, v0, LX/1HR;->d:I

    iget-object v1, p0, LX/1Fg;->e:LX/1HR;

    iget v1, v1, LX/1HR;->b:I

    invoke-static {p0}, LX/1Fg;->g(LX/1Fg;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 224493
    iget-object v1, p0, LX/1Fg;->e:LX/1HR;

    iget v1, v1, LX/1HR;->c:I

    iget-object v2, p0, LX/1Fg;->e:LX/1HR;

    iget v2, v2, LX/1HR;->a:I

    invoke-static {p0}, LX/1Fg;->h(LX/1Fg;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 224494
    invoke-direct {p0, v0, v1}, LX/1Fg;->a(II)Ljava/util/ArrayList;

    move-result-object v0

    .line 224495
    invoke-direct {p0, v0}, LX/1Fg;->c(Ljava/util/ArrayList;)V

    .line 224496
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224497
    invoke-direct {p0, v0}, LX/1Fg;->a(Ljava/util/ArrayList;)V

    .line 224498
    invoke-static {v0}, LX/1Fg;->b(Ljava/util/ArrayList;)V

    .line 224499
    return-void

    .line 224500
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized f(LX/1lX;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lX",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 224414
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224415
    iget-boolean v1, p1, LX/1lX;->d:Z

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 224416
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/1lX;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224417
    monitor-exit p0

    return-void

    .line 224418
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized g(LX/1Fg;)I
    .locals 2

    .prologue
    .line 224420
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->a()I

    move-result v0

    iget-object v1, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v1}, LX/1HP;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    sub-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g(LX/1lX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lX",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 224322
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224323
    iget-boolean v0, p1, LX/1lX;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 224324
    iget v0, p1, LX/1lX;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, LX/1lX;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224325
    monitor-exit p0

    return-void

    .line 224326
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized h(LX/1Fg;)I
    .locals 2

    .prologue
    .line 224329
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->b()I

    move-result v0

    iget-object v1, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v1}, LX/1HP;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    sub-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized h(LX/1Fg;LX/1lX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lX",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 224330
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224331
    iget v0, p1, LX/1lX;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 224332
    iget v0, p1, LX/1lX;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, LX/1lX;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224333
    monitor-exit p0

    return-void

    .line 224334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized i(LX/1Fg;LX/1lX;)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1lX",
            "<TK;TV;>;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224336
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224337
    iget-boolean v0, p1, LX/1lX;->d:Z

    if-eqz v0, :cond_0

    iget v0, p1, LX/1lX;->c:I

    if-nez v0, :cond_0

    iget-object v0, p1, LX/1lX;->b:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 224339
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/android/internal/util/Predicate;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<TK;>;)I"
        }
    .end annotation

    .prologue
    .line 224340
    monitor-enter p0

    .line 224341
    :try_start_0
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0, p1}, LX/1HP;->b(Lcom/android/internal/util/Predicate;)Ljava/util/ArrayList;

    move-result-object v0

    .line 224342
    iget-object v1, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v1, p1}, LX/1HP;->b(Lcom/android/internal/util/Predicate;)Ljava/util/ArrayList;

    move-result-object v1

    .line 224343
    invoke-direct {p0, v1}, LX/1Fg;->c(Ljava/util/ArrayList;)V

    .line 224344
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224345
    invoke-direct {p0, v1}, LX/1Fg;->a(Ljava/util/ArrayList;)V

    .line 224346
    invoke-static {v0}, LX/1Fg;->b(Ljava/util/ArrayList;)V

    .line 224347
    invoke-static {p0}, LX/1Fg;->e(LX/1Fg;)V

    .line 224348
    invoke-static {p0}, LX/1Fg;->f(LX/1Fg;)V

    .line 224349
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0

    .line 224350
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Object;)LX/1FJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 224351
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224352
    const/4 v2, 0x0

    .line 224353
    monitor-enter p0

    .line 224354
    :try_start_0
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0, p1}, LX/1HP;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lX;

    .line 224355
    iget-object v1, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v1, p1}, LX/1HP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lX;

    .line 224356
    if-eqz v1, :cond_0

    .line 224357
    invoke-direct {p0, v1}, LX/1Fg;->a(LX/1lX;)LX/1FJ;

    move-result-object v1

    .line 224358
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224359
    invoke-static {v0}, LX/1Fg;->d(LX/1lX;)V

    .line 224360
    invoke-static {p0}, LX/1Fg;->e(LX/1Fg;)V

    .line 224361
    invoke-static {p0}, LX/1Fg;->f(LX/1Fg;)V

    .line 224362
    return-object v1

    .line 224363
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1FJ;)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/1FJ",
            "<TV;>;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 224364
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1Fg;->a(Ljava/lang/Object;LX/1FJ;LX/4dc;)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1FJ;LX/4dc;)LX/1FJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/1FJ",
            "<TV;>;",
            "Lcom/facebook/imagepipeline/cache/CountingMemoryCache$EntryStateObserver",
            "<TK;>;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 224365
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224366
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224367
    invoke-static {p0}, LX/1Fg;->e(LX/1Fg;)V

    .line 224368
    monitor-enter p0

    .line 224369
    :try_start_0
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0, p1}, LX/1HP;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lX;

    .line 224370
    iget-object v1, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v1, p1}, LX/1HP;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lX;

    .line 224371
    if-eqz v1, :cond_1

    .line 224372
    invoke-direct {p0, v1}, LX/1Fg;->f(LX/1lX;)V

    .line 224373
    invoke-static {p0, v1}, LX/1Fg;->i(LX/1Fg;LX/1lX;)LX/1FJ;

    move-result-object v1

    move-object v3, v1

    .line 224374
    :goto_0
    invoke-virtual {p2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, LX/1Fg;->c(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 224375
    new-instance v1, LX/1lX;

    invoke-direct {v1, p1, p2, p3}, LX/1lX;-><init>(Ljava/lang/Object;LX/1FJ;LX/4dc;)V

    move-object v1, v1

    .line 224376
    iget-object v2, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v2, p1, v1}, LX/1HP;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224377
    invoke-direct {p0, v1}, LX/1Fg;->a(LX/1lX;)LX/1FJ;

    move-result-object v1

    .line 224378
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224379
    invoke-static {v3}, LX/1FJ;->c(LX/1FJ;)V

    .line 224380
    invoke-static {v0}, LX/1Fg;->d(LX/1lX;)V

    .line 224381
    invoke-static {p0}, LX/1Fg;->f(LX/1Fg;)V

    .line 224382
    return-object v1

    .line 224383
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object v1, v2

    goto :goto_1

    :cond_1
    move-object v3, v2

    goto :goto_0
.end method

.method public final a(LX/32G;)V
    .locals 6

    .prologue
    .line 224384
    iget-object v0, p0, LX/1Fg;->g:LX/1HO;

    invoke-interface {v0, p1}, LX/1HO;->a(LX/32G;)D

    move-result-wide v0

    .line 224385
    monitor-enter p0

    .line 224386
    :try_start_0
    iget-object v2, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v2}, LX/1HP;->b()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v0, v4, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 224387
    const/4 v1, 0x0

    invoke-static {p0}, LX/1Fg;->h(LX/1Fg;)I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 224388
    const v1, 0x7fffffff

    invoke-direct {p0, v1, v0}, LX/1Fg;->a(II)Ljava/util/ArrayList;

    move-result-object v0

    .line 224389
    invoke-direct {p0, v0}, LX/1Fg;->c(Ljava/util/ArrayList;)V

    .line 224390
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224391
    invoke-direct {p0, v0}, LX/1Fg;->a(Ljava/util/ArrayList;)V

    .line 224392
    invoke-static {v0}, LX/1Fg;->b(Ljava/util/ArrayList;)V

    .line 224393
    invoke-static {p0}, LX/1Fg;->e(LX/1Fg;)V

    .line 224394
    invoke-static {p0}, LX/1Fg;->f(LX/1Fg;)V

    .line 224395
    return-void

    .line 224396
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 224397
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/Object;)LX/1FJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/1FJ",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 224398
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224399
    const/4 v1, 0x0

    .line 224400
    monitor-enter p0

    .line 224401
    :try_start_0
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0, p1}, LX/1HP;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lX;

    .line 224402
    if-eqz v0, :cond_2

    .line 224403
    iget-object v1, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v1, p1}, LX/1HP;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lX;

    .line 224404
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224405
    iget v4, v1, LX/1lX;->c:I

    if-nez v4, :cond_0

    move v3, v2

    :cond_0
    invoke-static {v3}, LX/03g;->b(Z)V

    .line 224406
    iget-object v1, v1, LX/1lX;->b:LX/1FJ;

    move v5, v2

    move-object v2, v1

    move v1, v5

    .line 224407
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224408
    if-eqz v1, :cond_1

    .line 224409
    invoke-static {v0}, LX/1Fg;->d(LX/1lX;)V

    .line 224410
    :cond_1
    return-object v2

    .line 224411
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v2, v1

    move v1, v3

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/android/internal/util/Predicate;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<TK;>;)Z"
        }
    .end annotation

    .prologue
    .line 224412
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->c:LX/1HP;

    invoke-virtual {v0, p1}, LX/1HP;->a(Lcom/android/internal/util/Predicate;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 224413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()I
    .locals 1

    .prologue
    .line 224328
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Fg;->b:LX/1HP;

    invoke-virtual {v0}, LX/1HP;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
