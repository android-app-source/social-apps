.class public LX/153;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field private final b:LX/154;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/154;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 179931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179932
    iput-object p1, p0, LX/153;->a:Landroid/content/res/Resources;

    .line 179933
    iput-object p2, p0, LX/153;->b:LX/154;

    .line 179934
    iput-object p3, p0, LX/153;->c:LX/03V;

    .line 179935
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 179936
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/153;
    .locals 4

    .prologue
    .line 179937
    new-instance v3, LX/153;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v1

    check-cast v1, LX/154;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v1, v2}, LX/153;-><init>(Landroid/content/res/Resources;LX/154;LX/03V;)V

    .line 179938
    return-object v3
.end method


# virtual methods
.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179939
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 179940
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v3

    if-nez v3, :cond_3

    .line 179941
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 179942
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v4

    if-eqz v4, :cond_13

    .line 179943
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v4

    .line 179944
    :goto_2
    move v4, v4

    .line 179945
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    sub-int v5, v4, v5

    if-eqz v0, :cond_4

    move v4, v1

    :goto_3
    sub-int v4, v5, v4

    .line 179946
    if-gez v4, :cond_1

    .line 179947
    iget-object v4, p0, LX/153;->c:LX/03V;

    const-string v5, "feedback_socialcontext_negative_count"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "feedback.id = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", feedback.reactors = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 179948
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v7

    if-nez v7, :cond_15

    const-string v7, "null"

    :goto_4
    move-object v7, v7

    .line 179949
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", feedback.likers = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 179950
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v7

    if-nez v7, :cond_16

    const-string v7, "null"

    :goto_5
    move-object v7, v7

    .line 179951
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", feedback.important_reactors = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 179952
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;->a()LX/0Px;

    move-result-object v7

    if-nez v7, :cond_17

    .line 179953
    :cond_0
    const-string v7, "null"

    .line 179954
    :goto_6
    move-object v7, v7

    .line 179955
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v2

    .line 179956
    :cond_1
    if-eqz v0, :cond_d

    .line 179957
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 179958
    if-nez v4, :cond_8

    .line 179959
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 179960
    iget-object v0, p0, LX/153;->a:Landroid/content/res/Resources;

    const v1, 0x7f08199d    # 1.80908E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 179961
    :goto_7
    return-object v0

    :cond_2
    move v0, v2

    .line 179962
    goto/16 :goto_0

    .line 179963
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;->a()LX/0Px;

    move-result-object v3

    goto/16 :goto_1

    :cond_4
    move v4, v2

    .line 179964
    goto/16 :goto_3

    .line 179965
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 179966
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 179967
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 179968
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 179969
    :cond_7
    iget-object v0, p0, LX/153;->a:Landroid/content/res/Resources;

    const v1, 0x7f08199d    # 1.80908E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 179970
    :cond_8
    iget-object v0, p0, LX/153;->a:Landroid/content/res/Resources;

    const v3, 0x7f0f00be

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/153;->b:LX/154;

    invoke-virtual {v5, v4}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 179971
    :cond_9
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_b

    .line 179972
    if-nez v4, :cond_a

    .line 179973
    iget-object v4, p0, LX/153;->a:Landroid/content/res/Resources;

    const v5, 0x7f08199e

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {v4, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 179974
    :cond_a
    iget-object v5, p0, LX/153;->a:Landroid/content/res/Resources;

    const v6, 0x7f0f00bf

    new-array v7, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    iget-object v0, p0, LX/153;->b:LX/154;

    invoke-virtual {v0, v4}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179975
    :cond_b
    if-nez v4, :cond_c

    .line 179976
    iget-object v4, p0, LX/153;->a:Landroid/content/res/Resources;

    const v5, 0x7f0819a0

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179977
    :cond_c
    iget-object v5, p0, LX/153;->a:Landroid/content/res/Resources;

    const v6, 0x7f0f00bf

    add-int/lit8 v7, v4, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    iget-object v0, p0, LX/153;->b:LX/154;

    add-int/lit8 v2, v4, 0x1

    invoke-virtual {v0, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179978
    :cond_d
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 179979
    if-nez v4, :cond_e

    .line 179980
    iget-object v0, p0, LX/153;->a:Landroid/content/res/Resources;

    const v1, 0x7f08199c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179981
    :cond_e
    iget-object v0, p0, LX/153;->b:LX/154;

    invoke-virtual {v0, v4}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179982
    :cond_f
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_11

    .line 179983
    if-nez v4, :cond_10

    .line 179984
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179985
    :cond_10
    iget-object v5, p0, LX/153;->a:Landroid/content/res/Resources;

    const v6, 0x7f0f00c0

    new-array v7, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    iget-object v0, p0, LX/153;->b:LX/154;

    invoke-virtual {v0, v4}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179986
    :cond_11
    if-nez v4, :cond_12

    .line 179987
    iget-object v4, p0, LX/153;->a:Landroid/content/res/Resources;

    const v5, 0x7f0819a1

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 179988
    :cond_12
    iget-object v5, p0, LX/153;->a:Landroid/content/res/Resources;

    const v6, 0x7f0f00c0

    add-int/lit8 v7, v4, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    iget-object v0, p0, LX/153;->b:LX/154;

    add-int/lit8 v2, v4, 0x1

    invoke-virtual {v0, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :cond_13
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    if-eqz v4, :cond_14

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v4

    goto/16 :goto_2

    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_15
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_4

    :cond_16
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_5

    :cond_17
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_6
.end method
