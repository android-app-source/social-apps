.class public LX/1Z1;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fm;


# instance fields
.field private final a:LX/03V;

.field private final b:LX/1Z2;

.field public c:Landroid/support/design/widget/AppBarLayout;


# direct methods
.method public constructor <init>(LX/03V;LX/1Z2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 274170
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 274171
    iput-object p1, p0, LX/1Z1;->a:LX/03V;

    .line 274172
    iput-object p2, p0, LX/1Z1;->b:LX/1Z2;

    .line 274173
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 274174
    const v0, 0x7f0d120b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 274175
    const v0, 0x7f0d120e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    .line 274176
    const v1, 0x7f0d120d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/AppBarLayout;

    iput-object v1, p0, LX/1Z1;->c:Landroid/support/design/widget/AppBarLayout;

    .line 274177
    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 274178
    :cond_0
    iget-object v0, p0, LX/1Z1;->a:LX/03V;

    const-string v1, "scroll_away_composer"

    const-string v2, "layout is not valid for scroll away composer"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 274179
    :goto_0
    return-void

    .line 274180
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 274181
    instance-of v2, v1, LX/1uK;

    if-nez v2, :cond_2

    .line 274182
    iget-object v0, p0, LX/1Z1;->a:LX/03V;

    const-string v1, "scroll_away_composer"

    const-string v2, "newsfeed container is not in a CoordinatorLayout"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 274183
    :cond_2
    check-cast v1, LX/1uK;

    .line 274184
    new-instance v2, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;

    invoke-direct {v2}, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;-><init>()V

    invoke-virtual {v1, v2}, LX/1uK;->a(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V

    .line 274185
    iget-object v1, p0, LX/1Z1;->b:LX/1Z2;

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 274186
    if-nez v0, :cond_3

    .line 274187
    :goto_1
    goto :goto_0

    .line 274188
    :cond_3
    iget-object v5, v1, LX/1Z2;->k:Landroid/content/res/Resources;

    iget-object p0, v1, LX/1Z2;->f:LX/1EE;

    iget-object p1, v1, LX/1Z2;->h:LX/0W9;

    iget-object v2, v1, LX/1Z2;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1E8;

    invoke-virtual {v2}, LX/1E8;->j()Z

    move-result v2

    invoke-static {v5, p0, p1, v2}, LX/1aH;->a(Landroid/content/res/Resources;LX/1EE;LX/0W9;Z)LX/1aH;

    move-result-object v5

    .line 274189
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274190
    iget-boolean p0, v2, LX/1EE;->c:Z

    move v2, p0

    .line 274191
    if-eqz v2, :cond_a

    move v2, v3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonVisibility(I)V

    .line 274192
    iget-object v2, v5, LX/1aH;->b:Ljava/lang/String;

    iget-object p0, v5, LX/1aH;->a:Ljava/lang/String;

    iget-object p1, v5, LX/1aH;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, p0, p1}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274193
    iget v2, v5, LX/1aH;->d:I

    if-eqz v2, :cond_4

    .line 274194
    iget v2, v5, LX/1aH;->d:I

    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setStatusButtonDrawable(I)V

    .line 274195
    :cond_4
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274196
    iget p0, v2, LX/1EE;->k:I

    move v2, p0

    .line 274197
    if-eqz v2, :cond_5

    .line 274198
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274199
    iget p0, v2, LX/1EE;->k:I

    move v2, p0

    .line 274200
    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setPhotoButtonDrawable(I)V

    .line 274201
    :cond_5
    iget-object v2, v5, LX/1aH;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_6

    .line 274202
    iget-object v2, v5, LX/1aH;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 274203
    :cond_6
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274204
    iget-boolean v5, v2, LX/1EE;->u:Z

    move v2, v5

    .line 274205
    if-eqz v2, :cond_7

    .line 274206
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274207
    iget v5, v2, LX/1EE;->m:I

    move v2, v5

    .line 274208
    iget-object v5, v1, LX/1Z2;->f:LX/1EE;

    .line 274209
    iget p0, v5, LX/1EE;->n:I

    move v5, p0

    .line 274210
    iget-object p0, v1, LX/1Z2;->f:LX/1EE;

    .line 274211
    iget p1, p0, LX/1EE;->o:I

    move p0, p1

    .line 274212
    invoke-virtual {v0, v2, v5, p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(III)V

    .line 274213
    :cond_7
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274214
    iget-object v5, v2, LX/1EE;->r:Ljava/lang/Integer;

    move-object v2, v5

    .line 274215
    if-eqz v2, :cond_8

    .line 274216
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274217
    iget-object v5, v2, LX/1EE;->r:Ljava/lang/Integer;

    move-object v2, v5

    .line 274218
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setButtonTextColor(I)V

    .line 274219
    :cond_8
    const/4 v2, 0x0

    .line 274220
    iget-object v5, v1, LX/1Z2;->b:LX/1RX;

    invoke-virtual {v5}, LX/1RX;->a()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, v1, LX/1Z2;->b:LX/1RX;

    invoke-virtual {v5}, LX/1RX;->b()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, v1, LX/1Z2;->j:LX/0ad;

    sget-short p0, LX/1aO;->R:S

    invoke-interface {v5, p0, v2}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v2, 0x1

    :cond_9
    move v2, v2

    .line 274221
    if-eqz v2, :cond_b

    .line 274222
    :goto_3
    new-instance v2, LX/1uU;

    invoke-direct {v2, v1}, LX/1uU;-><init>(LX/1Z2;)V

    move-object v2, v2

    .line 274223
    iget-object v4, v1, LX/1Z2;->j:LX/0ad;

    sget-char v5, LX/1aO;->P:C

    const/4 p0, 0x0

    invoke-interface {v4, v5, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v2, v4}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(ILandroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 274224
    const v2, 0x7f0d1123

    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 274225
    const v2, 0x7f0d1124

    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 274226
    const v2, 0x7f0d1125

    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 274227
    iget-object v2, v1, LX/1Z2;->f:LX/1EE;

    .line 274228
    iget-boolean p0, v2, LX/1EE;->t:Z

    move v2, p0

    .line 274229
    if-eqz v2, :cond_c

    .line 274230
    new-instance v2, LX/2o3;

    invoke-direct {v2, v1}, LX/2o3;-><init>(LX/1Z2;)V

    move-object v2, v2

    .line 274231
    :goto_4
    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274232
    new-instance v2, LX/1uW;

    invoke-direct {v2, v1}, LX/1uW;-><init>(LX/1Z2;)V

    move-object v2, v2

    .line 274233
    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274234
    new-instance v2, LX/1uX;

    invoke-direct {v2, v1}, LX/1uX;-><init>(LX/1Z2;)V

    move-object v2, v2

    .line 274235
    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :cond_a
    move v2, v4

    .line 274236
    goto/16 :goto_2

    :cond_b
    move v3, v4

    .line 274237
    goto :goto_3

    .line 274238
    :cond_c
    new-instance v2, LX/1uV;

    invoke-direct {v2, v1}, LX/1uV;-><init>(LX/1Z2;)V

    move-object v2, v2

    .line 274239
    goto :goto_4
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 274240
    return-void
.end method

.method public final kw_()Z
    .locals 3

    .prologue
    .line 274241
    iget-object v0, p0, LX/1Z1;->b:LX/1Z2;

    const/4 v1, 0x0

    .line 274242
    iget-object v2, v0, LX/1Z2;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/1Z2;->j:LX/0ad;

    sget-short p0, LX/0fe;->aG:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/1Z2;->j:LX/0ad;

    sget-short p0, LX/1Nu;->o:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 274243
    return v0
.end method
