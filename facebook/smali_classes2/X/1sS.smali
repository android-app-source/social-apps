.class public LX/1sS;
.super LX/0SQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0SQ",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0y3;

.field private final b:Lcom/facebook/location/BaseFbLocationManager;

.field private final c:LX/0y2;

.field public final d:LX/0SG;

.field public final e:Ljava/util/concurrent/ScheduledExecutorService;

.field public f:Lcom/facebook/location/FbLocationOperationParams;

.field private g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:LX/2vj;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public i:Ljava/util/concurrent/ScheduledFuture;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:Lcom/facebook/location/ImmutableLocation;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0y3;Lcom/facebook/location/BaseFbLocationManager;LX/0y2;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 334827
    invoke-direct {p0}, LX/0SQ;-><init>()V

    .line 334828
    iput-object p1, p0, LX/1sS;->a:LX/0y3;

    .line 334829
    iput-object p2, p0, LX/1sS;->b:Lcom/facebook/location/BaseFbLocationManager;

    .line 334830
    iput-object p3, p0, LX/1sS;->c:LX/0y2;

    .line 334831
    iput-object p4, p0, LX/1sS;->d:LX/0SG;

    .line 334832
    iput-object p5, p0, LX/1sS;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 334833
    return-void
.end method

.method private static a(LX/1sS;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 334745
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1sS;->g:Z

    .line 334746
    iget-object v0, p0, LX/1sS;->b:Lcom/facebook/location/BaseFbLocationManager;

    invoke-virtual {v0}, Lcom/facebook/location/BaseFbLocationManager;->c()V

    .line 334747
    invoke-direct {p0}, LX/1sS;->d()V

    .line 334748
    invoke-virtual {p0, p1}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    .line 334749
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/1sS;LX/2Ce;)V
    .locals 1

    .prologue
    .line 334750
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1sS;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 334751
    :goto_0
    monitor-exit p0

    return-void

    .line 334752
    :cond_0
    :try_start_1
    invoke-static {p0, p1}, LX/1sS;->a(LX/1sS;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 334753
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/1sS;Lcom/facebook/location/ImmutableLocation;)V
    .locals 8

    .prologue
    .line 334754
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1sS;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 334755
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 334756
    :cond_1
    :try_start_1
    iput-object p1, p0, LX/1sS;->j:Lcom/facebook/location/ImmutableLocation;

    .line 334757
    iget-object v4, p0, LX/1sS;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v6, v4

    move-wide v0, v4

    .line 334758
    iget-object v2, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v2, v2, Lcom/facebook/location/FbLocationOperationParams;->b:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget v1, v1, Lcom/facebook/location/FbLocationOperationParams;->c:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 334759
    invoke-static {p0, p1}, LX/1sS;->b(LX/1sS;Lcom/facebook/location/ImmutableLocation;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 334760
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/0QB;)LX/1sS;
    .locals 15

    .prologue
    .line 334761
    new-instance v0, LX/1sS;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v1

    check-cast v1, LX/0y3;

    .line 334762
    const/16 v6, 0x32f

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/1wY;->a(LX/0QB;)LX/1wY;

    move-result-object v7

    check-cast v7, LX/1wY;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v8

    check-cast v8, LX/0yH;

    const/16 v9, 0xc85

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x2614

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0xc8c

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v12

    check-cast v12, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v13

    check-cast v13, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {v6 .. v14}, LX/1sQ;->a(LX/0Or;LX/1wY;LX/0yH;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)Lcom/facebook/location/BaseFbLocationManager;

    move-result-object v6

    move-object v2, v6

    .line 334763
    check-cast v2, Lcom/facebook/location/BaseFbLocationManager;

    invoke-static {p0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v3

    check-cast v3, LX/0y2;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct/range {v0 .. v5}, LX/1sS;-><init>(LX/0y3;Lcom/facebook/location/BaseFbLocationManager;LX/0y2;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 334764
    return-object v0
.end method

.method public static declared-synchronized b(LX/1sS;)V
    .locals 2

    .prologue
    .line 334765
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1sS;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 334766
    :goto_0
    monitor-exit p0

    return-void

    .line 334767
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1sS;->j:Lcom/facebook/location/ImmutableLocation;

    if-eqz v0, :cond_1

    .line 334768
    iget-object v0, p0, LX/1sS;->j:Lcom/facebook/location/ImmutableLocation;

    invoke-static {p0, v0}, LX/1sS;->b(LX/1sS;Lcom/facebook/location/ImmutableLocation;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 334769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 334770
    :cond_1
    :try_start_2
    new-instance v0, LX/2Ce;

    sget-object v1, LX/2sk;->TIMEOUT:LX/2sk;

    invoke-direct {v0, v1}, LX/2Ce;-><init>(LX/2sk;)V

    invoke-static {p0, v0}, LX/1sS;->a(LX/1sS;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static b(LX/1sS;Lcom/facebook/location/ImmutableLocation;)V
    .locals 1

    .prologue
    .line 334771
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1sS;->g:Z

    .line 334772
    iget-object v0, p0, LX/1sS;->b:Lcom/facebook/location/BaseFbLocationManager;

    invoke-virtual {v0}, Lcom/facebook/location/BaseFbLocationManager;->c()V

    .line 334773
    invoke-direct {p0}, LX/1sS;->d()V

    .line 334774
    invoke-virtual {p0, p1}, LX/0SQ;->set(Ljava/lang/Object;)Z

    .line 334775
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 334776
    iget-object v0, p0, LX/1sS;->i:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    .line 334777
    :goto_0
    return-void

    .line 334778
    :cond_0
    iget-object v0, p0, LX/1sS;->i:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 334779
    const/4 v0, 0x0

    iput-object v0, p0, LX/1sS;->i:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/facebook/location/ImmutableLocation;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 334780
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1sS;->g:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "must be started"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 334781
    iget-object v0, p0, LX/1sS;->j:Lcom/facebook/location/ImmutableLocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 334782
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 334783
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, LX/1sS;->g:Z

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    const-string v3, "already running"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 334784
    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    const-string v1, "already done"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 334785
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/FbLocationOperationParams;

    iput-object v0, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    .line 334786
    iget-object v0, p0, LX/1sS;->a:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_2

    .line 334787
    new-instance v0, LX/2Ce;

    sget-object v1, LX/2sk;->LOCATION_UNAVAILABLE:LX/2sk;

    invoke-direct {v0, v1}, LX/2Ce;-><init>(LX/2sk;)V

    invoke-virtual {p0, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334788
    :goto_2
    monitor-exit p0

    return-void

    :cond_0
    move v2, v1

    .line 334789
    goto :goto_0

    :cond_1
    move v0, v1

    .line 334790
    goto :goto_1

    .line 334791
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/1sS;->c:LX/0y2;

    iget-object v1, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v2, v1, Lcom/facebook/location/FbLocationOperationParams;->b:J

    iget-object v1, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget v1, v1, Lcom/facebook/location/FbLocationOperationParams;->c:F

    invoke-virtual {v0, v2, v3, v1}, LX/0y2;->a(JF)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 334792
    if-eqz v0, :cond_3

    .line 334793
    iput-object v0, p0, LX/1sS;->j:Lcom/facebook/location/ImmutableLocation;

    .line 334794
    invoke-virtual {p0, v0}, LX/0SQ;->set(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 334795
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 334796
    :cond_3
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/1sS;->g:Z

    .line 334797
    new-instance v0, LX/2vj;

    invoke-direct {v0, p0}, LX/2vj;-><init>(LX/1sS;)V

    iput-object v0, p0, LX/1sS;->h:LX/2vj;

    .line 334798
    iget-object v0, p0, LX/1sS;->b:Lcom/facebook/location/BaseFbLocationManager;

    iget-object v1, p0, LX/1sS;->e:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {v0, v1}, Lcom/facebook/location/BaseFbLocationManager;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 334799
    iget-object v4, p0, LX/1sS;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/location/FbLocationOperation$1;

    invoke-direct {v5, p0}, Lcom/facebook/location/FbLocationOperation$1;-><init>(LX/1sS;)V

    iget-object v6, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v6, v6, Lcom/facebook/location/FbLocationOperationParams;->d:J

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v4

    iput-object v4, p0, LX/1sS;->i:Ljava/util/concurrent/ScheduledFuture;

    .line 334800
    iget-object v0, p0, LX/1sS;->b:Lcom/facebook/location/BaseFbLocationManager;

    .line 334801
    iget-object v4, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-object v4, v4, Lcom/facebook/location/FbLocationOperationParams;->a:LX/0yF;

    .line 334802
    new-instance v5, LX/2b6;

    invoke-direct {v5, v4}, LX/2b6;-><init>(LX/0yF;)V

    move-object v4, v5

    .line 334803
    iget-object v5, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v6, v5, Lcom/facebook/location/FbLocationOperationParams;->d:J

    .line 334804
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    iput-object v5, v4, LX/2b6;->d:LX/0am;

    .line 334805
    move-object v4, v4

    .line 334806
    iget-object v5, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-object v5, v5, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    .line 334807
    iput-object v5, v4, LX/2b6;->b:LX/0am;

    .line 334808
    move-object v4, v4

    .line 334809
    iget-object v5, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-object v5, v5, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    .line 334810
    iput-object v5, v4, LX/2b6;->c:LX/0am;

    .line 334811
    move-object v4, v4

    .line 334812
    iget-object v5, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v6, v5, Lcom/facebook/location/FbLocationOperationParams;->g:J

    .line 334813
    iput-wide v6, v4, LX/2b6;->e:J

    .line 334814
    move-object v4, v4

    .line 334815
    const/4 v5, 0x0

    .line 334816
    iput v5, v4, LX/2b6;->f:F

    .line 334817
    move-object v4, v4

    .line 334818
    iget-object v5, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v6, v5, Lcom/facebook/location/FbLocationOperationParams;->h:J

    .line 334819
    iput-wide v6, v4, LX/2b6;->g:J

    .line 334820
    move-object v4, v4

    .line 334821
    iget-object v5, p0, LX/1sS;->f:Lcom/facebook/location/FbLocationOperationParams;

    iget v5, v5, Lcom/facebook/location/FbLocationOperationParams;->i:F

    .line 334822
    iput v5, v4, LX/2b6;->h:F

    .line 334823
    move-object v4, v4

    .line 334824
    new-instance v5, LX/2vk;

    invoke-direct {v5, v4}, LX/2vk;-><init>(LX/2b6;)V

    move-object v4, v5

    .line 334825
    move-object v1, v4

    .line 334826
    iget-object v2, p0, LX/1sS;->h:LX/2vj;

    invoke-virtual {v0, v1, v2, p2}, Lcom/facebook/location/BaseFbLocationManager;->a(LX/2vk;LX/2vj;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2
.end method
