.class public abstract LX/1px;
.super LX/1jo;
.source ""


# instance fields
.field public e:LX/1pq;

.field public f:LX/0u5;

.field public g:LX/1py;

.field public h:LX/1jk;

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "LX/0uE;",
            ">;"
        }
    .end annotation
.end field

.field public j:[LX/0uE;

.field public k:LX/0uc;


# direct methods
.method public constructor <init>(LX/1jk;LX/1pq;ILX/0ue;LX/0u0;LX/0uc;)V
    .locals 6

    .prologue
    .line 329869
    invoke-direct {p0, p2, p3}, LX/1jo;-><init>(LX/1jm;I)V

    .line 329870
    iget-object v0, p2, LX/1pq;->i:LX/1pt;

    if-nez v0, :cond_0

    .line 329871
    new-instance v0, LX/5MH;

    const-string v1, "Missing context in config"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329872
    :cond_0
    iget-object v0, p2, LX/1pq;->i:LX/1pt;

    iget-object v0, v0, LX/1pt;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0uE;->a(Ljava/lang/String;)LX/0uN;

    move-result-object v0

    .line 329873
    iget-object v1, p2, LX/1pq;->i:LX/1pt;

    iget-object v1, v1, LX/1pt;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 329874
    :cond_1
    new-instance v0, LX/5MH;

    const-string v1, "Bad context identifier"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329875
    :cond_2
    new-instance v0, LX/1py;

    invoke-direct {v0}, LX/1py;-><init>()V

    iput-object v0, p0, LX/1px;->g:LX/1py;

    .line 329876
    iget-object v0, p2, LX/1pq;->i:LX/1pt;

    iget-object v0, v0, LX/1pt;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 329877
    iget-object v0, p2, LX/1pq;->i:LX/1pt;

    iget-object v0, v0, LX/1pt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5MT;

    .line 329878
    iget-object v2, v0, LX/5MT;->a:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/5MT;->b:Ljava/lang/String;

    iget-object v0, v0, LX/5MT;->c:Ljava/util/List;

    invoke-virtual {p4, v2, v3, v0}, LX/0ue;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/5MG;

    move-result-object v0

    .line 329879
    iget-object v2, p0, LX/1px;->g:LX/1py;

    invoke-virtual {v2, v0}, LX/1py;->a(LX/5MG;)V

    goto :goto_0

    .line 329880
    :cond_3
    iget-object v0, p2, LX/1pq;->i:LX/1pt;

    iget-boolean v0, v0, LX/1pt;->d:Z

    if-nez v0, :cond_4

    .line 329881
    iget-object v0, p2, LX/1pq;->i:LX/1pt;

    iget-object v0, v0, LX/1pt;->a:Ljava/lang/String;

    invoke-virtual {p5, v0}, LX/0u0;->a(Ljava/lang/String;)LX/0u5;

    move-result-object v0

    iput-object v0, p0, LX/1px;->f:LX/0u5;

    .line 329882
    :goto_1
    iput-object p2, p0, LX/1px;->e:LX/1pq;

    .line 329883
    iput-object p1, p0, LX/1px;->h:LX/1jk;

    .line 329884
    iput-object p6, p0, LX/1px;->k:LX/0uc;

    .line 329885
    return-void

    .line 329886
    :cond_4
    new-instance v0, LX/0u5;

    iget-object v1, p2, LX/1pq;->i:LX/1pt;

    iget-object v1, v1, LX/1pt;->a:Ljava/lang/String;

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    iput-object v0, p0, LX/1px;->f:LX/0u5;

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1jk;
    .locals 1

    .prologue
    .line 329887
    iget-object v0, p0, LX/1px;->h:LX/1jk;

    return-object v0
.end method

.method public final a(LX/8K8;)LX/1jr;
    .locals 7
    .param p1    # LX/8K8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 329888
    iget-object v1, p0, LX/1px;->f:LX/0u5;

    iget-object v2, p0, LX/1px;->e:LX/1pq;

    iget-object v2, v2, LX/1pq;->i:LX/1pt;

    iget-wide v2, v2, LX/1pt;->b:J

    invoke-virtual {v1, v2, v3, p1}, LX/0u5;->a(JLX/8K8;)LX/0uE;

    move-result-object v3

    .line 329889
    if-eqz v3, :cond_3

    .line 329890
    iget-object v1, p0, LX/1px;->e:LX/1pq;

    iget-object v1, v1, LX/1pq;->i:LX/1pt;

    iget-object v1, v1, LX/1pt;->e:Ljava/util/List;

    if-nez v1, :cond_1

    .line 329891
    invoke-virtual {v3}, LX/0uE;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 329892
    :goto_0
    if-eqz v4, :cond_2

    .line 329893
    iget-object v0, p0, LX/1px;->i:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0uE;

    move-object v5, v0

    .line 329894
    :goto_1
    if-nez v5, :cond_0

    .line 329895
    iget-object v5, p0, LX/1px;->j:[LX/0uE;

    .line 329896
    :cond_0
    new-instance v0, LX/1jr;

    iget-object v2, p0, LX/1px;->f:LX/0u5;

    iget-object v6, p0, LX/1px;->k:LX/0uc;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/1jr;-><init>(LX/1jp;LX/0u5;LX/0uE;Ljava/lang/String;[LX/0uE;LX/0uc;)V

    return-object v0

    .line 329897
    :cond_1
    iget-object v1, p0, LX/1px;->g:LX/1py;

    invoke-virtual {v1, v3}, LX/1py;->a(LX/0uE;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    move-object v5, v0

    goto :goto_1

    :cond_3
    move-object v4, v0

    move-object v5, v0

    goto :goto_1
.end method
