.class public final LX/0md;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0me;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132708
    const/4 v0, -0x1

    iput v0, p0, LX/0md;->c:I

    .line 132709
    iput-object p1, p0, LX/0md;->a:LX/0Ot;

    .line 132710
    iput-object p2, p0, LX/0md;->b:LX/0Ot;

    .line 132711
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 132701
    iget-object v0, p0, LX/0md;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    invoke-static {v0}, LX/2DE;->b(LX/0Uq;)V

    .line 132702
    const-string v0, "readMaxEventsPerBatchQE"

    const v1, 0x29ccc1d6

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 132703
    :try_start_0
    iget-object v0, p0, LX/0md;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/2DF;->i:I

    const/16 v2, 0x32

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0md;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132704
    const v0, 0x47cf17d0    # 106031.625f

    invoke-static {v0}, LX/03q;->a(I)V

    .line 132705
    return-void

    .line 132706
    :catchall_0
    move-exception v0

    const v1, 0x736c30e2

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 132698
    iget v0, p0, LX/0md;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 132699
    invoke-direct {p0}, LX/0md;->b()V

    .line 132700
    :cond_0
    iget v0, p0, LX/0md;->c:I

    return v0
.end method
