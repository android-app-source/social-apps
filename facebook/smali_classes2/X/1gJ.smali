.class public final LX/1gJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1gI;


# instance fields
.field public final a:LX/1gI;

.field public final synthetic b:LX/1HZ;


# direct methods
.method public constructor <init>(LX/1HZ;LX/1gI;)V
    .locals 0

    .prologue
    .line 293945
    iput-object p1, p0, LX/1gJ;->b:LX/1HZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293946
    iput-object p2, p0, LX/1gJ;->a:LX/1gI;

    .line 293947
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 293948
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v0, p0, LX/1gJ;->a:LX/1gI;

    invoke-interface {v0}, LX/1gI;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 293949
    :try_start_0
    sget v0, LX/1HZ;->n:I

    invoke-virtual {v1, v0}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 293950
    sget v0, LX/1HZ;->n:I

    new-array v0, v0, [B

    .line 293951
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v1, v0, v2, v3}, LX/0aP;->b(Ljava/io/InputStream;[BII)V

    .line 293952
    sget-object v2, LX/1HZ;->j:[B

    invoke-static {v0, v2}, LX/1HZ;->b([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 293953
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->reset()V

    .line 293954
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->read()I

    .line 293955
    iget-object v0, p0, LX/1gJ;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->c:LX/1Hz;

    sget-object v2, LX/1HZ;->a:LX/1Hb;

    invoke-virtual {v0, v1, v2}, LX/1Hz;->a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;

    move-result-object v0

    .line 293956
    :goto_0
    return-object v0

    .line 293957
    :cond_0
    sget-object v2, LX/1HZ;->k:[B

    invoke-static {v0, v2}, LX/1HZ;->b([B[B)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 293958
    iget-object v0, p0, LX/1gJ;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->d:LX/1Hz;

    sget-object v2, LX/1HZ;->a:LX/1Hb;

    invoke-virtual {v0, v1, v2}, LX/1Hz;->a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 293959
    :cond_1
    sget-object v2, LX/1HZ;->m:[B

    invoke-static {v0, v2}, LX/1HZ;->b([B[B)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 293960
    iget-object v0, p0, LX/1gJ;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->e:LX/1Hz;

    sget-object v2, LX/1HZ;->a:LX/1Hb;

    invoke-virtual {v0, v1, v2}, LX/1Hz;->a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 293961
    :cond_2
    sget-object v2, LX/1HZ;->l:[B

    invoke-static {v0, v2}, LX/1HZ;->b([B[B)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 293962
    iget-object v0, p0, LX/1gJ;->b:LX/1HZ;

    iget-object v0, v0, LX/1HZ;->f:LX/1IW;

    sget-object v2, LX/1HZ;->a:LX/1Hb;

    invoke-virtual {v0, v1, v2}, LX/1IW;->a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 293963
    :cond_3
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->reset()V
    :try_end_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48k; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    .line 293964
    goto :goto_0

    .line 293965
    :catch_0
    move-exception v0

    .line 293966
    :goto_1
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 293967
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error decrypting"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 293968
    :catch_1
    move-exception v0

    .line 293969
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 293970
    throw v0

    .line 293971
    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method public final b()[B
    .locals 4

    .prologue
    .line 293972
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {p0}, LX/1gJ;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 293973
    :try_start_0
    invoke-virtual {p0}, LX/1gJ;->c()J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {v1, v0}, LX/0aP;->a(Ljava/io/InputStream;I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 293974
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    throw v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 293975
    iget-object v0, p0, LX/1gJ;->a:LX/1gI;

    invoke-interface {v0}, LX/1gI;->c()J

    move-result-wide v0

    return-wide v0
.end method
