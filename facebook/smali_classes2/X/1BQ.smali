.class public LX/1BQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field public final b:J

.field public final c:J

.field public final d:I


# direct methods
.method public constructor <init>(LX/0Uh;LX/0W3;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213145
    iput-object p1, p0, LX/1BQ;->a:LX/0Uh;

    .line 213146
    sget-wide v0, LX/0X5;->gO:J

    invoke-interface {p2, v0, v1}, LX/0W4;->c(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/1BQ;->b:J

    .line 213147
    sget-wide v0, LX/0X5;->gP:J

    invoke-interface {p2, v0, v1}, LX/0W4;->c(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/1BQ;->c:J

    .line 213148
    sget-wide v0, LX/0X5;->gQ:J

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/1BQ;->d:I

    .line 213149
    return-void
.end method

.method public static b(LX/0QB;)LX/1BQ;
    .locals 3

    .prologue
    .line 213150
    new-instance v2, LX/1BQ;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-direct {v2, v0, v1}, LX/1BQ;-><init>(LX/0Uh;LX/0W3;)V

    .line 213151
    return-object v2
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 213152
    iget-object v0, p0, LX/1BQ;->a:LX/0Uh;

    const/16 v1, 0x247

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 213153
    iget-object v0, p0, LX/1BQ;->a:LX/0Uh;

    const/16 v1, 0x246

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
