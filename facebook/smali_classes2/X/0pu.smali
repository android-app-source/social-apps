.class public LX/0pu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0pu;


# instance fields
.field private final a:Landroid/os/PowerManager;

.field private final b:LX/0pw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0pw",
            "<",
            "LX/0q0;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/PowerManager;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 146807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146808
    iput-object p2, p0, LX/0pu;->a:Landroid/os/PowerManager;

    .line 146809
    new-instance v0, LX/0pv;

    new-instance v1, LX/0px;

    invoke-direct {v1, p0}, LX/0px;-><init>(LX/0pu;)V

    .line 146810
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 146811
    const-string p2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 146812
    const-string p2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 146813
    const/16 p2, 0x3e7

    invoke-virtual {v2, p2}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 146814
    move-object v2, v2

    .line 146815
    invoke-direct {v0, p1, v1, v2}, LX/0pv;-><init>(Landroid/content/Context;LX/0py;Landroid/content/IntentFilter;)V

    iput-object v0, p0, LX/0pu;->b:LX/0pw;

    .line 146816
    return-void
.end method

.method public static a(LX/0QB;)LX/0pu;
    .locals 5

    .prologue
    .line 146794
    sget-object v0, LX/0pu;->d:LX/0pu;

    if-nez v0, :cond_1

    .line 146795
    const-class v1, LX/0pu;

    monitor-enter v1

    .line 146796
    :try_start_0
    sget-object v0, LX/0pu;->d:LX/0pu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 146797
    if-eqz v2, :cond_0

    .line 146798
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 146799
    new-instance p0, LX/0pu;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0kZ;->b(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    invoke-direct {p0, v3, v4}, LX/0pu;-><init>(Landroid/content/Context;Landroid/os/PowerManager;)V

    .line 146800
    move-object v0, p0

    .line 146801
    sput-object v0, LX/0pu;->d:LX/0pu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146802
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 146803
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146804
    :cond_1
    sget-object v0, LX/0pu;->d:LX/0pu;

    return-object v0

    .line 146805
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 146806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0q0;)V
    .locals 1

    .prologue
    .line 146792
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0pu;->a(LX/0q0;Landroid/os/Handler;)V

    .line 146793
    return-void
.end method

.method public final a(LX/0q0;Landroid/os/Handler;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 146817
    monitor-enter p0

    .line 146818
    :try_start_0
    iget-object v0, p0, LX/0pu;->b:LX/0pw;

    invoke-virtual {v0, p1, p2}, LX/0pw;->a(Ljava/lang/Object;Landroid/os/Handler;)V

    .line 146819
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 146783
    iget-object v0, p0, LX/0pu;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 146784
    iget-object v0, p0, LX/0pu;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 146785
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/0pu;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/0q0;)V
    .locals 1

    .prologue
    .line 146787
    monitor-enter p0

    .line 146788
    :try_start_0
    iget-object v0, p0, LX/0pu;->b:LX/0pw;

    invoke-virtual {v0, p1}, LX/0pw;->a(Ljava/lang/Object;)V

    .line 146789
    iget-object v0, p0, LX/0pu;->b:LX/0pw;

    invoke-virtual {v0}, LX/0pw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146790
    const/4 v0, 0x0

    iput-object v0, p0, LX/0pu;->c:Ljava/lang/Boolean;

    .line 146791
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 146786
    iget-object v0, p0, LX/0pu;->a:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method
