.class public final enum LX/0c7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0c7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0c7;

.field public static final enum Fb4a:LX/0c7;

.field public static final enum Fscam:LX/0c7;

.field public static final enum Messenger:LX/0c7;

.field public static final enum PMA:LX/0c7;

.field public static final enum Sample:LX/0c7;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87809
    new-instance v0, LX/0c7;

    const-string v1, "Sample"

    invoke-direct {v0, v1, v2}, LX/0c7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0c7;->Sample:LX/0c7;

    .line 87810
    new-instance v0, LX/0c7;

    const-string v1, "Messenger"

    invoke-direct {v0, v1, v3}, LX/0c7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0c7;->Messenger:LX/0c7;

    .line 87811
    new-instance v0, LX/0c7;

    const-string v1, "Fb4a"

    invoke-direct {v0, v1, v4}, LX/0c7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0c7;->Fb4a:LX/0c7;

    .line 87812
    new-instance v0, LX/0c7;

    const-string v1, "PMA"

    invoke-direct {v0, v1, v5}, LX/0c7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0c7;->PMA:LX/0c7;

    .line 87813
    new-instance v0, LX/0c7;

    const-string v1, "Fscam"

    invoke-direct {v0, v1, v6}, LX/0c7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0c7;->Fscam:LX/0c7;

    .line 87814
    const/4 v0, 0x5

    new-array v0, v0, [LX/0c7;

    sget-object v1, LX/0c7;->Sample:LX/0c7;

    aput-object v1, v0, v2

    sget-object v1, LX/0c7;->Messenger:LX/0c7;

    aput-object v1, v0, v3

    sget-object v1, LX/0c7;->Fb4a:LX/0c7;

    aput-object v1, v0, v4

    sget-object v1, LX/0c7;->PMA:LX/0c7;

    aput-object v1, v0, v5

    sget-object v1, LX/0c7;->Fscam:LX/0c7;

    aput-object v1, v0, v6

    sput-object v0, LX/0c7;->$VALUES:[LX/0c7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87817
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0c7;
    .locals 1

    .prologue
    .line 87816
    const-class v0, LX/0c7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0c7;

    return-object v0
.end method

.method public static values()[LX/0c7;
    .locals 1

    .prologue
    .line 87815
    sget-object v0, LX/0c7;->$VALUES:[LX/0c7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0c7;

    return-object v0
.end method
