.class public final LX/0Re;
.super LX/0Rf;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rf",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0Re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Re",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final transient b:[Ljava/lang/Object;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final transient c:[Ljava/lang/Object;

.field private final transient d:I

.field private final transient e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60250
    new-instance v0, LX/0Re;

    sget-object v1, LX/0P8;->a:[Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2, v3}, LX/0Re;-><init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V

    sput-object v0, LX/0Re;->a:LX/0Re;

    return-void
.end method

.method public constructor <init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 60244
    invoke-direct {p0}, LX/0Rf;-><init>()V

    .line 60245
    iput-object p1, p0, LX/0Re;->c:[Ljava/lang/Object;

    .line 60246
    iput-object p3, p0, LX/0Re;->b:[Ljava/lang/Object;

    .line 60247
    iput p4, p0, LX/0Re;->d:I

    .line 60248
    iput p2, p0, LX/0Re;->e:I

    .line 60249
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 60233
    iget-object v2, p0, LX/0Re;->b:[Ljava/lang/Object;

    .line 60234
    if-eqz p1, :cond_0

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 60235
    :goto_0
    return v0

    .line 60236
    :cond_1
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v0

    .line 60237
    :goto_1
    iget v3, p0, LX/0Re;->d:I

    and-int/2addr v0, v3

    .line 60238
    aget-object v3, v2, v0

    .line 60239
    if-nez v3, :cond_2

    move v0, v1

    .line 60240
    goto :goto_0

    .line 60241
    :cond_2
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 60242
    const/4 v0, 0x1

    goto :goto_0

    .line 60243
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final copyIntoArray([Ljava/lang/Object;I)I
    .locals 3

    .prologue
    .line 60231
    iget-object v0, p0, LX/0Re;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0Re;->c:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v0, v1, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 60232
    iget-object v0, p0, LX/0Re;->c:[Ljava/lang/Object;

    array-length v0, v0

    add-int/2addr v0, p2

    return v0
.end method

.method public final createAsList()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60228
    iget-object v0, p0, LX/0Re;->b:[Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 60229
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 60230
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/12p;

    iget-object v1, p0, LX/0Re;->c:[Ljava/lang/Object;

    invoke-direct {v0, p0, v1}, LX/12p;-><init>(LX/0Py;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 60251
    iget v0, p0, LX/0Re;->e:I

    return v0
.end method

.method public final isHashCodeFast()Z
    .locals 1

    .prologue
    .line 60227
    const/4 v0, 0x1

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 60226
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60225
    iget-object v0, p0, LX/0Re;->c:[Ljava/lang/Object;

    invoke-static {v0}, LX/0RZ;->a([Ljava/lang/Object;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 60224
    invoke-virtual {p0}, LX/0Re;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 60223
    iget-object v0, p0, LX/0Re;->c:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method
