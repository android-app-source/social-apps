.class public LX/1fi;
.super LX/1ah;
.source ""


# instance fields
.field private a:Landroid/graphics/Matrix;

.field private c:Landroid/graphics/Matrix;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 292123
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 292124
    iput v1, p0, LX/1fi;->d:I

    .line 292125
    iput v1, p0, LX/1fi;->e:I

    .line 292126
    iput-object p2, p0, LX/1fi;->a:Landroid/graphics/Matrix;

    .line 292127
    return-void
.end method

.method public static c(LX/1fi;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 292128
    invoke-virtual {p0}, LX/1fi;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 292129
    invoke-virtual {p0}, LX/1fi;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 292130
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iput v2, p0, LX/1fi;->d:I

    .line 292131
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    iput v3, p0, LX/1fi;->e:I

    .line 292132
    if-lez v2, :cond_0

    if-gtz v3, :cond_1

    .line 292133
    :cond_0
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 292134
    const/4 v0, 0x0

    iput-object v0, p0, LX/1fi;->c:Landroid/graphics/Matrix;

    .line 292135
    :goto_0
    return-void

    .line 292136
    :cond_1
    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 292137
    iget-object v0, p0, LX/1fi;->a:Landroid/graphics/Matrix;

    iput-object v0, p0, LX/1fi;->c:Landroid/graphics/Matrix;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 292138
    invoke-super {p0, p1}, LX/1ah;->a(Landroid/graphics/Matrix;)V

    .line 292139
    iget-object v0, p0, LX/1fi;->c:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 292140
    iget-object v0, p0, LX/1fi;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 292141
    :cond_0
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 292142
    invoke-super {p0, p1}, LX/1ah;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 292143
    invoke-static {p0}, LX/1fi;->c(LX/1fi;)V

    .line 292144
    return-object v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 292145
    iget v0, p0, LX/1fi;->d:I

    invoke-virtual {p0}, LX/1fi;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/1fi;->e:I

    invoke-virtual {p0}, LX/1fi;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 292146
    :cond_0
    invoke-static {p0}, LX/1fi;->c(LX/1fi;)V

    .line 292147
    :cond_1
    iget-object v0, p0, LX/1fi;->c:Landroid/graphics/Matrix;

    if-eqz v0, :cond_2

    .line 292148
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 292149
    invoke-virtual {p0}, LX/1fi;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 292150
    iget-object v1, p0, LX/1fi;->c:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 292151
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 292152
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 292153
    :goto_0
    return-void

    .line 292154
    :cond_2
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 292155
    invoke-super {p0, p1}, LX/1ah;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 292156
    invoke-static {p0}, LX/1fi;->c(LX/1fi;)V

    .line 292157
    return-void
.end method
