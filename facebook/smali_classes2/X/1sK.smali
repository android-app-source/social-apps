.class public LX/1sK;
.super LX/1sz;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1sK;


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 334557
    invoke-direct {p0, p1}, LX/1sz;-><init>(Landroid/app/ActivityManager;)V

    .line 334558
    return-void
.end method

.method public static a(LX/0QB;)LX/1sK;
    .locals 4

    .prologue
    .line 334562
    sget-object v0, LX/1sK;->a:LX/1sK;

    if-nez v0, :cond_1

    .line 334563
    const-class v1, LX/1sK;

    monitor-enter v1

    .line 334564
    :try_start_0
    sget-object v0, LX/1sK;->a:LX/1sK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 334565
    if-eqz v2, :cond_0

    .line 334566
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 334567
    new-instance p0, LX/1sK;

    invoke-static {v0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    invoke-direct {p0, v3}, LX/1sK;-><init>(Landroid/app/ActivityManager;)V

    .line 334568
    move-object v0, p0

    .line 334569
    sput-object v0, LX/1sK;->a:LX/1sK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 334570
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 334571
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 334572
    :cond_1
    sget-object v0, LX/1sK;->a:LX/1sK;

    return-object v0

    .line 334573
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 334574
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/ActivityManager$MemoryInfo;)J
    .locals 2
    .param p1    # Landroid/app/ActivityManager$MemoryInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 334559
    if-eqz p1, :cond_0

    .line 334560
    iget-wide v0, p1, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    .line 334561
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
