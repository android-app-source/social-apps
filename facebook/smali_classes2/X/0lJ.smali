.class public abstract LX/0lJ;
.super LX/0lK;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/reflect/Type;


# static fields
.field private static final serialVersionUID:J = 0x5e03193550d4eef6L


# instance fields
.field public final _asStatic:Z

.field public final _class:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final _hashCode:I

.field public final _typeHandler:Ljava/lang/Object;

.field public final _valueHandler:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Class;ILjava/lang/Object;Ljava/lang/Object;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;I",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 128852
    invoke-direct {p0}, LX/0lK;-><init>()V

    .line 128853
    iput-object p1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    .line 128854
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, p2

    iput v0, p0, LX/0lJ;->_hashCode:I

    .line 128855
    iput-object p3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    .line 128856
    iput-object p4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    .line 128857
    iput-boolean p5, p0, LX/0lJ;->_asStatic:Z

    .line 128858
    return-void
.end method

.method private h(Ljava/lang/Class;)LX/0lJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 128859
    invoke-virtual {p0, p1}, LX/0lJ;->d(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method private i(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 128860
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128861
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not assignable to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128862
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)LX/0lJ;
    .locals 1

    .prologue
    .line 128863
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)LX/0lJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 128864
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    if-ne p1, v0, :cond_0

    .line 128865
    :goto_0
    return-object p0

    .line 128866
    :cond_0
    invoke-direct {p0, p1}, LX/0lJ;->i(Ljava/lang/Class;)V

    .line 128867
    invoke-virtual {p0, p1}, LX/0lJ;->d(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 128868
    iget-object v1, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    invoke-virtual {v0}, LX/0lJ;->t()Ljava/lang/Object;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 128869
    iget-object v1, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v0

    .line 128870
    :cond_1
    iget-object v1, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    invoke-virtual {v0}, LX/0lJ;->u()Ljava/lang/Object;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 128871
    iget-object v1, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v0

    :cond_2
    move-object p0, v0

    .line 128872
    goto :goto_0
.end method

.method public abstract a(Ljava/lang/Object;)LX/0lJ;
.end method

.method public abstract b()LX/0lJ;
.end method

.method public final b(Ljava/lang/Class;)LX/0lJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 128873
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    if-ne p1, v0, :cond_0

    .line 128874
    :goto_0
    return-object p0

    .line 128875
    :cond_0
    invoke-virtual {p0, p1}, LX/0lJ;->d(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 128876
    iget-object v1, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    invoke-virtual {v0}, LX/0lJ;->t()Ljava/lang/Object;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 128877
    iget-object v1, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v0

    .line 128878
    :cond_1
    iget-object v1, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    invoke-virtual {v0}, LX/0lJ;->u()Ljava/lang/Object;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 128879
    iget-object v1, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v0

    :cond_2
    move-object p0, v0

    .line 128880
    goto :goto_0
.end method

.method public abstract b(Ljava/lang/Object;)LX/0lJ;
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128881
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Ljava/lang/Class;)LX/0lJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 128882
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    if-ne p1, v0, :cond_0

    .line 128883
    :goto_0
    return-object p0

    .line 128884
    :cond_0
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-direct {p0, v0}, LX/0lJ;->i(Ljava/lang/Class;)V

    .line 128885
    invoke-direct {p0, p1}, LX/0lJ;->h(Ljava/lang/Class;)LX/0lJ;

    move-result-object p0

    goto :goto_0
.end method

.method public abstract c(Ljava/lang/Object;)LX/0lJ;
.end method

.method public final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 128886
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    return-object v0
.end method

.method public abstract d(Ljava/lang/Class;)LX/0lJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation
.end method

.method public abstract d(Ljava/lang/Object;)LX/0lJ;
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 128887
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isAbstract(I)Z

    move-result v0

    return v0
.end method

.method public abstract e(Ljava/lang/Class;)LX/0lJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 128888
    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getModifiers()I

    move-result v1

    .line 128889
    and-int/lit16 v1, v1, 0x600

    if-nez v1, :cond_1

    .line 128890
    :cond_0
    :goto_0
    return v0

    .line 128891
    :cond_1
    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->isPrimitive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128892
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method public abstract f(Ljava/lang/Class;)LX/0lJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 128836
    const-class v0, Ljava/lang/Throwable;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 128835
    const/4 v0, 0x0

    return v0
.end method

.method public final g(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 128837
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 128838
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 128839
    iget v0, p0, LX/0lJ;->_hashCode:I

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 128840
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 128841
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 128842
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v0

    return v0
.end method

.method public abstract l()Z
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 128843
    const/4 v0, 0x0

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 128844
    const/4 v0, 0x0

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 128845
    iget-boolean v0, p0, LX/0lJ;->_asStatic:Z

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 128846
    invoke-virtual {p0}, LX/0lJ;->s()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()LX/0lJ;
    .locals 1

    .prologue
    .line 128847
    const/4 v0, 0x0

    return-object v0
.end method

.method public r()LX/0lJ;
    .locals 1

    .prologue
    .line 128848
    const/4 v0, 0x0

    return-object v0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 128849
    const/4 v0, 0x0

    return v0
.end method

.method public t()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 128850
    iget-object v0, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    return-object v0
.end method

.method public abstract toString()Ljava/lang/String;
.end method

.method public u()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 128851
    iget-object v0, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    return-object v0
.end method
