.class public LX/0pT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile v:LX/0pT;


# instance fields
.field public a:Z

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:J

.field public j:I

.field public k:I

.field public l:I

.field public final m:LX/0SG;

.field private final n:LX/0Zb;

.field public final o:LX/0pU;

.field public final p:Landroid/os/Handler;

.field public final q:Ljava/lang/Runnable;

.field public final r:Ljava/lang/Runnable;

.field private final s:LX/0W3;

.field public t:Z

.field public u:Z


# direct methods
.method public constructor <init>(LX/0SG;LX/0Zb;LX/0pU;LX/0W3;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 144447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144448
    iput-boolean v2, p0, LX/0pT;->a:Z

    .line 144449
    iput-wide v0, p0, LX/0pT;->b:J

    .line 144450
    iput-wide v0, p0, LX/0pT;->c:J

    .line 144451
    iput-wide v0, p0, LX/0pT;->d:J

    .line 144452
    iput-wide v0, p0, LX/0pT;->e:J

    .line 144453
    iput-wide v0, p0, LX/0pT;->f:J

    .line 144454
    iput-wide v0, p0, LX/0pT;->g:J

    .line 144455
    iput-wide v0, p0, LX/0pT;->h:J

    .line 144456
    iput-wide v0, p0, LX/0pT;->i:J

    .line 144457
    iput v2, p0, LX/0pT;->j:I

    .line 144458
    iput v2, p0, LX/0pT;->k:I

    .line 144459
    iput v2, p0, LX/0pT;->l:I

    .line 144460
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/0pT;->p:Landroid/os/Handler;

    .line 144461
    new-instance v0, Lcom/facebook/feed/pulltorefresh/PullToRefreshLogger$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/pulltorefresh/PullToRefreshLogger$1;-><init>(LX/0pT;)V

    iput-object v0, p0, LX/0pT;->q:Ljava/lang/Runnable;

    .line 144462
    new-instance v0, Lcom/facebook/feed/pulltorefresh/PullToRefreshLogger$2;

    invoke-direct {v0, p0}, Lcom/facebook/feed/pulltorefresh/PullToRefreshLogger$2;-><init>(LX/0pT;)V

    iput-object v0, p0, LX/0pT;->r:Ljava/lang/Runnable;

    .line 144463
    iput-boolean v2, p0, LX/0pT;->t:Z

    .line 144464
    iput-boolean v2, p0, LX/0pT;->u:Z

    .line 144465
    iput-object p1, p0, LX/0pT;->m:LX/0SG;

    .line 144466
    iput-object p2, p0, LX/0pT;->n:LX/0Zb;

    .line 144467
    iput-object p3, p0, LX/0pT;->o:LX/0pU;

    .line 144468
    iput-object p4, p0, LX/0pT;->s:LX/0W3;

    .line 144469
    return-void
.end method

.method public static a(LX/0QB;)LX/0pT;
    .locals 7

    .prologue
    .line 144434
    sget-object v0, LX/0pT;->v:LX/0pT;

    if-nez v0, :cond_1

    .line 144435
    const-class v1, LX/0pT;

    monitor-enter v1

    .line 144436
    :try_start_0
    sget-object v0, LX/0pT;->v:LX/0pT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 144437
    if-eqz v2, :cond_0

    .line 144438
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 144439
    new-instance p0, LX/0pT;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0pU;->a(LX/0QB;)LX/0pU;

    move-result-object v5

    check-cast v5, LX/0pU;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0pT;-><init>(LX/0SG;LX/0Zb;LX/0pU;LX/0W3;)V

    .line 144440
    move-object v0, p0

    .line 144441
    sput-object v0, LX/0pT;->v:LX/0pT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144442
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 144443
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144444
    :cond_1
    sget-object v0, LX/0pT;->v:LX/0pT;

    return-object v0

    .line 144445
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 144446
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/0pT;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 144414
    iget-object v0, p0, LX/0pT;->n:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 144415
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 144416
    const-wide/16 v8, 0x0

    .line 144417
    iget-wide v2, p0, LX/0pT;->b:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_0

    .line 144418
    const-string v2, "time_elapsed_since_manual_refresh"

    iget-wide v4, p0, LX/0pT;->b:J

    iget-wide v6, p0, LX/0pT;->e:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 144419
    :cond_0
    iget-wide v2, p0, LX/0pT;->c:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_1

    .line 144420
    const-string v2, "time_elapsed_since_fetch_attempt"

    iget-wide v4, p0, LX/0pT;->c:J

    iget-wide v6, p0, LX/0pT;->f:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 144421
    const-string v2, "time_elapsed_current_fetch_attempt"

    iget-object v3, p0, LX/0pT;->m:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/0pT;->c:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 144422
    :cond_1
    iget-wide v2, p0, LX/0pT;->d:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_2

    .line 144423
    const-string v2, "time_elapsed_since_fetch_result"

    iget-wide v4, p0, LX/0pT;->d:J

    iget-wide v6, p0, LX/0pT;->g:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 144424
    const-string v2, "num_stories_fetched_in_last_result"

    iget v3, p0, LX/0pT;->j:I

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 144425
    :cond_2
    iget-wide v2, p0, LX/0pT;->h:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_3

    .line 144426
    const-string v2, "time_elapsed_since_scroll_to_top"

    iget-object v3, p0, LX/0pT;->m:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/0pT;->h:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 144427
    :cond_3
    iget-wide v2, p0, LX/0pT;->i:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_4

    .line 144428
    const-string v2, "time_elapsed_since_newsfeed_fragment_active"

    iget-object v3, p0, LX/0pT;->m:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/0pT;->i:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 144429
    :cond_4
    const-string v2, "num_server_unseen_bumped_stories_in_last_result"

    iget v3, p0, LX/0pT;->k:I

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 144430
    const-string v2, "num_client_unseen_stories_in_last_result"

    iget v3, p0, LX/0pT;->l:I

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 144431
    const-string v2, "android_pull_to_refresh"

    invoke-virtual {v0, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 144432
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 144433
    :cond_5
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 144410
    iget-wide v0, p0, LX/0pT;->d:J

    iput-wide v0, p0, LX/0pT;->g:J

    .line 144411
    iget-object v0, p0, LX/0pT;->m:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0pT;->d:J

    .line 144412
    iput p1, p0, LX/0pT;->j:I

    .line 144413
    return-void
.end method

.method public final a(II)V
    .locals 11

    .prologue
    .line 144470
    iget-object v0, p0, LX/0pT;->o:LX/0pU;

    .line 144471
    iget-object v1, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144472
    const/4 v5, 0x0

    .line 144473
    sget-object v4, LX/391;->UNKNOWN:LX/391;

    .line 144474
    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    move-object v3, v4

    .line 144475
    :cond_0
    :goto_0
    move-object v6, v3

    .line 144476
    sget-object v3, LX/391;->UNKNOWN:LX/391;

    if-ne v6, v3, :cond_2

    .line 144477
    :cond_1
    :goto_1
    return-void

    .line 144478
    :cond_2
    const/4 v4, 0x0

    .line 144479
    iget-object v3, v0, LX/0pU;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 144480
    if-lez p2, :cond_3

    .line 144481
    new-instance v4, LX/3m0;

    sget-object v7, LX/3m1;->DOWN:LX/3m1;

    iget-object v3, v0, LX/0pU;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v9

    move-object v5, v0

    move v8, p2

    invoke-direct/range {v4 .. v10}, LX/3m0;-><init>(LX/0pU;LX/391;LX/3m1;IJ)V

    .line 144482
    :cond_3
    :goto_2
    if-eqz v4, :cond_1

    .line 144483
    iget-object v3, v0, LX/0pU;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144484
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Scroll Action: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v4, LX/3m0;->a:LX/391;

    invoke-virtual {v5}, LX/391;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v4, LX/3m0;->b:LX/3m1;

    invoke-virtual {v5}, LX/3m1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v4, LX/3m0;->c:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v5, v4, LX/3m0;->d:J

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 144485
    iget-object v3, v0, LX/0pU;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x5

    if-le v3, v4, :cond_1

    .line 144486
    iget-object v3, v0, LX/0pU;->b:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 144487
    :cond_4
    invoke-static {v0}, LX/0pU;->g(LX/0pU;)LX/3m0;

    move-result-object v3

    .line 144488
    iget v5, v3, LX/3m0;->c:I

    if-eq p2, v5, :cond_3

    .line 144489
    iget v3, v3, LX/3m0;->c:I

    sub-int v3, p2, v3

    if-gez v3, :cond_5

    sget-object v7, LX/3m1;->UP:LX/3m1;

    .line 144490
    :goto_3
    new-instance v4, LX/3m0;

    iget-object v3, v0, LX/0pU;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v9

    move-object v5, v0

    move v8, p2

    invoke-direct/range {v4 .. v10}, LX/3m0;-><init>(LX/0pU;LX/391;LX/3m1;IJ)V

    goto :goto_2

    .line 144491
    :cond_5
    sget-object v7, LX/3m1;->DOWN:LX/3m1;

    goto :goto_3

    .line 144492
    :cond_6
    :goto_4
    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_7

    .line 144493
    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_4

    .line 144494
    :cond_7
    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object v3, v4

    .line 144495
    goto/16 :goto_0

    .line 144496
    :cond_8
    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 144497
    const/4 v3, 0x1

    if-ne v5, v3, :cond_b

    .line 144498
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 144499
    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v8, 0x2

    if-lt v3, v8, :cond_d

    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v6, :cond_d

    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_d

    move v3, v6

    :goto_5
    move v3, v3

    .line 144500
    if-nez v3, :cond_9

    const/4 v9, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 144501
    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v8, 0x3

    if-lt v3, v8, :cond_e

    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v6, :cond_e

    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v9, :cond_e

    iget-object v3, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_e

    move v3, v6

    :goto_6
    move v3, v3

    .line 144502
    if-eqz v3, :cond_c

    :cond_9
    const/4 v3, 0x1

    :goto_7
    move v3, v3

    .line 144503
    if-eqz v3, :cond_b

    .line 144504
    sget-object v4, LX/391;->MANUAL:LX/391;

    move-object v3, v4

    .line 144505
    :goto_8
    const/4 v4, 0x2

    if-ne v5, v4, :cond_a

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 144506
    iget-object v4, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v4, v7, :cond_f

    iget-object v4, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v7, :cond_f

    iget-object v4, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_f

    move v4, v5

    :goto_9
    move v4, v4

    .line 144507
    if-eqz v4, :cond_a

    .line 144508
    sget-object v3, LX/391;->AUTOMATIC:LX/391;

    .line 144509
    :cond_a
    sget-object v4, LX/391;->UNKNOWN:LX/391;

    if-eq v3, v4, :cond_0

    .line 144510
    iget-object v4, v0, LX/0pU;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    :cond_b
    move-object v3, v4

    goto :goto_8

    :cond_c
    const/4 v3, 0x0

    goto :goto_7

    :cond_d
    move v3, v7

    goto/16 :goto_5

    :cond_e
    move v3, v7

    goto :goto_6

    :cond_f
    move v4, v6

    goto :goto_9
.end method

.method public final a(Landroid/util/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144407
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/0pT;->k:I

    .line 144408
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/0pT;->l:I

    .line 144409
    return-void
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 144406
    iget-wide v0, p0, LX/0pT;->c:J

    return-wide v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 144404
    iget-object v0, p0, LX/0pT;->m:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/0pT;->i:J

    .line 144405
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 144391
    iget-boolean v0, p0, LX/0pT;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0pT;->a:Z

    if-eqz v0, :cond_0

    .line 144392
    iget-object v0, p0, LX/0pT;->p:Landroid/os/Handler;

    iget-object v1, p0, LX/0pT;->q:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 144393
    iput-boolean v2, p0, LX/0pT;->t:Z

    .line 144394
    const-string v0, "ptr_scroll_then_ptr"

    invoke-static {p0, v0}, LX/0pT;->a$redex0(LX/0pT;Ljava/lang/String;)V

    .line 144395
    :cond_0
    iget-boolean v0, p0, LX/0pT;->u:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/0pT;->a:Z

    if-eqz v0, :cond_1

    .line 144396
    iget-object v0, p0, LX/0pT;->p:Landroid/os/Handler;

    iget-object v1, p0, LX/0pT;->r:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 144397
    iput-boolean v2, p0, LX/0pT;->u:Z

    .line 144398
    const-string v0, "ptr_jump_then_ptr"

    invoke-static {p0, v0}, LX/0pT;->a$redex0(LX/0pT;Ljava/lang/String;)V

    .line 144399
    :cond_1
    iget-boolean v0, p0, LX/0pT;->a:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, LX/0pT;->b:J

    iget-wide v2, p0, LX/0pT;->e:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 144400
    const-string v0, "ptr_back_to_back_ptr"

    invoke-static {p0, v0}, LX/0pT;->a$redex0(LX/0pT;Ljava/lang/String;)V

    .line 144401
    :cond_2
    iget-boolean v0, p0, LX/0pT;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "ptr_manual_refresh"

    :goto_0
    invoke-static {p0, v0}, LX/0pT;->a$redex0(LX/0pT;Ljava/lang/String;)V

    .line 144402
    return-void

    .line 144403
    :cond_3
    const-string v0, "ptr_auto_refresh"

    goto :goto_0
.end method

.method public final f()Z
    .locals 6

    .prologue
    .line 144387
    iget-object v0, p0, LX/0pT;->s:LX/0W3;

    sget-wide v2, LX/0X5;->hW:J

    const-wide/16 v4, 0x1388

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    .line 144388
    iget-object v2, p0, LX/0pT;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/0pT;->b:J

    sub-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 144389
    const/4 v0, 0x1

    .line 144390
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
