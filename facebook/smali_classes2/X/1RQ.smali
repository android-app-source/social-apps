.class public LX/1RQ;
.super LX/1RI;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/ipc/productionprompts/annotations/ForProductionPrompts;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1RJ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246170
    invoke-direct {p0, p1}, LX/1RI;-><init>(Ljava/util/Set;)V

    .line 246171
    iput-object p2, p0, LX/1RQ;->a:LX/0Ot;

    .line 246172
    return-void
.end method


# virtual methods
.method public final d(LX/1RN;)LX/1RJ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 246168
    invoke-super {p0, p1}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 246169
    if-nez v0, :cond_0

    iget-object v0, p0, LX/1RQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RJ;

    :cond_0
    return-object v0
.end method

.method public final e(LX/1RN;)Z
    .locals 1

    .prologue
    .line 246166
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246167
    instance-of v0, v0, LX/1kW;

    return v0
.end method
