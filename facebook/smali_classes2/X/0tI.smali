.class public LX/0tI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0tI;


# instance fields
.field private final a:LX/0tJ;


# direct methods
.method public constructor <init>(LX/0tJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154279
    iput-object p1, p0, LX/0tI;->a:LX/0tJ;

    .line 154280
    return-void
.end method

.method public static a(LX/0QB;)LX/0tI;
    .locals 4

    .prologue
    .line 154281
    sget-object v0, LX/0tI;->b:LX/0tI;

    if-nez v0, :cond_1

    .line 154282
    const-class v1, LX/0tI;

    monitor-enter v1

    .line 154283
    :try_start_0
    sget-object v0, LX/0tI;->b:LX/0tI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154284
    if-eqz v2, :cond_0

    .line 154285
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154286
    new-instance p0, LX/0tI;

    invoke-static {v0}, LX/0tJ;->a(LX/0QB;)LX/0tJ;

    move-result-object v3

    check-cast v3, LX/0tJ;

    invoke-direct {p0, v3}, LX/0tI;-><init>(LX/0tJ;)V

    .line 154287
    move-object v0, p0

    .line 154288
    sput-object v0, LX/0tI;->b:LX/0tI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154289
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154290
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154291
    :cond_1
    sget-object v0, LX/0tI;->b:LX/0tI;

    return-object v0

    .line 154292
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 6

    .prologue
    .line 154273
    const-string v0, "enable_real_time_activity_info"

    iget-object v1, p0, LX/0tI;->a:LX/0tJ;

    invoke-virtual {v1}, LX/0tJ;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 154274
    const-string v0, "threshold_duration_for_feed_discovery"

    iget-object v1, p0, LX/0tI;->a:LX/0tJ;

    .line 154275
    iget-object v2, v1, LX/0tJ;->a:LX/0ad;

    sget v3, LX/0wn;->S:I

    const/16 v4, 0x3c

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    move v1, v2

    .line 154276
    int-to-long v2, v1

    const-wide/16 v4, 0x3c

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 154277
    return-void
.end method
