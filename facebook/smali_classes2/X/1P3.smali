.class public final LX/1P3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public final synthetic d:LX/1P1;


# direct methods
.method public constructor <init>(LX/1P1;)V
    .locals 0

    .prologue
    .line 243557
    iput-object p1, p0, LX/1P3;->d:LX/1P1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(LX/1P3;Landroid/view/View;LX/1Ok;)Z
    .locals 1

    .prologue
    .line 243558
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 243559
    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {v0}, LX/1a3;->e()I

    move-result p0

    if-ltz p0, :cond_0

    invoke-virtual {v0}, LX/1a3;->e()I

    move-result v0

    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result p0

    if-ge v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 243560
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 243561
    iget-boolean v0, p0, LX/1P3;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1P3;->d:LX/1P1;

    iget-object v0, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    :goto_0
    iput v0, p0, LX/1P3;->b:I

    .line 243562
    return-void

    .line 243563
    :cond_0
    iget-object v0, p0, LX/1P3;->d:LX/1P1;

    iget-object v0, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 243564
    iget-boolean v0, p0, LX/1P3;->c:Z

    if-eqz v0, :cond_0

    .line 243565
    iget-object v0, p0, LX/1P3;->d:LX/1P1;

    iget-object v0, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, p1}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, LX/1P3;->d:LX/1P1;

    iget-object v1, v1, LX/1P1;->k:LX/1P5;

    invoke-virtual {v1}, LX/1P5;->b()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/1P3;->b:I

    .line 243566
    :goto_0
    invoke-static {p1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/1P3;->a:I

    .line 243567
    return-void

    .line 243568
    :cond_0
    iget-object v0, p0, LX/1P3;->d:LX/1P1;

    iget-object v0, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, p1}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/1P3;->b:I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243569
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AnchorInfo{mPosition="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/1P3;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCoordinate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1P3;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLayoutFromEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/1P3;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
