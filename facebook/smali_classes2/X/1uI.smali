.class public LX/1uI;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 340442
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010051

    aput v2, v0, v1

    sput-object v0, LX/1uI;->a:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 340441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 340434
    sget-object v1, LX/1uI;->a:[I

    invoke-virtual {p0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 340435
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 340436
    :cond_0
    if-eqz v1, :cond_1

    .line 340437
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 340438
    :cond_1
    if-eqz v0, :cond_2

    .line 340439
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with the design library."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340440
    :cond_2
    return-void
.end method
