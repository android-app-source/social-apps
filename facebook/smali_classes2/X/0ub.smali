.class public LX/0ub;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0uc;


# instance fields
.field private final a:LX/0Zb;

.field private final b:Ljava/util/Random;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1

    .prologue
    .line 156454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156455
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/0ub;->b:Ljava/util/Random;

    .line 156456
    iput-object p1, p0, LX/0ub;->a:LX/0Zb;

    .line 156457
    return-void
.end method

.method private static a(LX/0oG;LX/1jk;)V
    .locals 3

    .prologue
    .line 156458
    if-eqz p1, :cond_0

    .line 156459
    iget-object v0, p1, LX/1jk;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 156460
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0

    .line 156461
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1jk;LX/1jr;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 156462
    invoke-static {p1, p2, p3}, LX/1ju;->b(LX/1jk;LX/1jr;I)V

    .line 156463
    iget-object v0, p0, LX/0ub;->b:Ljava/util/Random;

    invoke-virtual {v0, p3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 156464
    :cond_0
    :goto_0
    return-void

    .line 156465
    :cond_1
    iget-object v0, p0, LX/0ub;->a:LX/0Zb;

    const-string v1, "contextual_config_exposure"

    invoke-interface {v0, v1, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 156466
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156467
    invoke-static {v0, p1}, LX/0ub;->a(LX/0oG;LX/1jk;)V

    .line 156468
    const-string v1, "sample_rate"

    .line 156469
    iget v2, v0, LX/0oG;->c:I

    move v2, v2

    .line 156470
    mul-int/2addr v2, p3

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 156471
    const-string v1, "policy_id"

    invoke-virtual {p2}, LX/1jr;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156472
    const-string v1, "config_name"

    invoke-virtual {p2}, LX/1jr;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156473
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 156474
    invoke-virtual {p2, v1, v5}, LX/1jr;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 156475
    const-string v2, "context"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156476
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 156477
    :cond_2
    invoke-virtual {p2, v1, v5}, LX/1jr;->b(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 156478
    const-string v2, "context_value"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156479
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 156480
    :cond_3
    invoke-virtual {p2, v1, v5}, LX/1jr;->c(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 156481
    const-string v2, "bucket"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156482
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 156483
    :cond_4
    invoke-virtual {p2, v1, v5}, LX/1jr;->d(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 156484
    const-string v2, "monitor"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156485
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 156486
    :cond_5
    invoke-virtual {p2, v1, v5}, LX/1jr;->e(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 156487
    const-string v2, "monitor_value"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156488
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 156489
    :cond_6
    invoke-virtual {p2, v1, v5}, LX/1jr;->f(Ljava/lang/StringBuilder;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 156490
    const-string v2, "result"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156491
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 156492
    :goto_1
    iget-object v1, p0, LX/0ub;->b:Ljava/util/Random;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-nez v1, :cond_7

    .line 156493
    const-string v1, "json"

    .line 156494
    iget-object v2, p1, LX/1jk;->a:Ljava/lang/String;

    move-object v2, v2

    .line 156495
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156496
    :cond_7
    invoke-virtual {v0}, LX/0oG;->d()V

    goto/16 :goto_0

    .line 156497
    :cond_8
    const-string v1, "result"

    const-string v2, "INVALID"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_1
.end method

.method public final a(LX/1jk;Ljava/lang/String;I)V
    .locals 3
    .param p1    # LX/1jk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 156498
    invoke-static {p1, p2, p3}, LX/1ju;->b(LX/1jk;Ljava/lang/String;I)V

    .line 156499
    iget-object v0, p0, LX/0ub;->b:Ljava/util/Random;

    invoke-virtual {v0, p3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 156500
    :cond_0
    :goto_0
    return-void

    .line 156501
    :cond_1
    iget-object v0, p0, LX/0ub;->a:LX/0Zb;

    const-string v1, "contextual_config_exposure"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 156502
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156503
    invoke-static {v0, p1}, LX/0ub;->a(LX/0oG;LX/1jk;)V

    .line 156504
    const-string v1, "sample_rate"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 156505
    const-string v1, "exception"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156506
    if-eqz p1, :cond_2

    iget-object v1, p0, LX/0ub;->b:Ljava/util/Random;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-nez v1, :cond_2

    .line 156507
    const-string v1, "json"

    .line 156508
    iget-object v2, p1, LX/1jk;->a:Ljava/lang/String;

    move-object v2, v2

    .line 156509
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 156510
    :cond_2
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
