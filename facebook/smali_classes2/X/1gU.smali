.class public LX/1gU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Landroid/net/Uri;

.field private final b:LX/0cG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cG",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 294281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294282
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/1gU;->c:Ljava/util/Set;

    .line 294283
    iput-object p1, p0, LX/1gU;->a:Landroid/net/Uri;

    .line 294284
    new-instance v0, LX/0cG;

    invoke-direct {v0}, LX/0cG;-><init>()V

    iput-object v0, p0, LX/1gU;->b:LX/0cG;

    .line 294285
    iget-object v0, p0, LX/1gU;->b:LX/0cG;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/{thread_id}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "THREAD"

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 294286
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1gU;->d:Ljava/lang/String;

    .line 294287
    return-void
.end method

.method public static b(LX/1gU;Landroid/net/Uri;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294288
    :try_start_0
    iget-object v0, p0, LX/1gU;->b:LX/0cG;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cG;->a(Ljava/lang/String;)LX/47X;

    move-result-object v0

    .line 294289
    if-eqz v0, :cond_0

    const-string v1, "THREAD"

    iget-object v2, v0, LX/47X;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294290
    iget-object v0, v0, LX/47X;->b:Landroid/os/Bundle;

    const-string v1, "thread_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 294291
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_0
    .catch LX/47T; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 294292
    :goto_0
    return-object v0

    :catch_0
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 294293
    iget-object v0, p0, LX/1gU;->a:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, LX/1gU;->b(LX/1gU;Landroid/net/Uri;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
