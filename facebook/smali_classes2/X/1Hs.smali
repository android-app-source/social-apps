.class public final LX/1Hs;
.super Ljava/security/Provider;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 227796
    const-string v0, "LinuxPRNG"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-string v1, "A Linux-specific random number provider that uses /dev/urandom"

    invoke-direct {p0, v0, v2, v3, v1}, Ljava/security/Provider;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    .line 227797
    const-string v0, "SecureRandom.SHA1PRNG"

    const-class v1, LX/1Ht;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1Hs;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227798
    const-string v0, "SecureRandom.SHA1PRNG ImplementedIn"

    const-string v1, "Software"

    invoke-virtual {p0, v0, v1}, LX/1Hs;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227799
    return-void
.end method
