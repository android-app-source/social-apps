.class public LX/0nk;
.super LX/0nl;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 136573
    invoke-direct {p0}, LX/0nl;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 136574
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 136575
    const-class v1, LX/0am;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136576
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/ser/GuavaOptionalSerializer;

    invoke-direct {v0, p2}, Lcom/fasterxml/jackson/datatype/guava/ser/GuavaOptionalSerializer;-><init>(LX/0lJ;)V

    .line 136577
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0nl;->a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0m2;LX/1Xo;LX/0lS;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/1Xo;",
            "LX/0lS;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 136578
    const-class v0, LX/0Xu;

    .line 136579
    iget-object v1, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 136580
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136581
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;

    invoke-direct {v0, p2, p4, p5, p6}, Lcom/fasterxml/jackson/datatype/guava/ser/MultimapSerializer;-><init>(LX/1Xo;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 136582
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
