.class public LX/1fN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/1fN;


# instance fields
.field private final a:Ljava/lang/Runnable;

.field private final b:LX/1fO;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field private final e:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/feedcache/db/FeedDb$UpdateListener;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/os/Handler;LX/0TD;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;",
            "Landroid/os/Handler;",
            "LX/0TD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 291542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291543
    new-instance v0, Lcom/facebook/api/feedcache/db/FeedDb$1;

    invoke-direct {v0, p0}, Lcom/facebook/api/feedcache/db/FeedDb$1;-><init>(LX/1fN;)V

    iput-object v0, p0, LX/1fN;->a:Ljava/lang/Runnable;

    .line 291544
    new-instance v0, LX/1fO;

    invoke-direct {v0, p0}, LX/1fO;-><init>(LX/1fN;)V

    iput-object v0, p0, LX/1fN;->b:LX/1fO;

    .line 291545
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1fN;->g:Z

    .line 291546
    iput-object p1, p0, LX/1fN;->c:LX/0Ot;

    .line 291547
    iput-object p2, p0, LX/1fN;->d:Landroid/os/Handler;

    .line 291548
    iput-object p3, p0, LX/1fN;->e:LX/0TD;

    .line 291549
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1fN;->f:Ljava/util/List;

    .line 291550
    return-void
.end method

.method public static a(LX/0QB;)LX/1fN;
    .locals 6

    .prologue
    .line 291529
    sget-object v0, LX/1fN;->h:LX/1fN;

    if-nez v0, :cond_1

    .line 291530
    const-class v1, LX/1fN;

    monitor-enter v1

    .line 291531
    :try_start_0
    sget-object v0, LX/1fN;->h:LX/1fN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291532
    if-eqz v2, :cond_0

    .line 291533
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 291534
    new-instance v5, LX/1fN;

    const/16 v3, 0xe8

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {v5, p0, v3, v4}, LX/1fN;-><init>(LX/0Ot;Landroid/os/Handler;LX/0TD;)V

    .line 291535
    move-object v0, v5

    .line 291536
    sput-object v0, LX/1fN;->h:LX/1fN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291537
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291538
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291539
    :cond_1
    sget-object v0, LX/1fN;->h:LX/1fN;

    return-object v0

    .line 291540
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291541
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291526
    iget-object v0, p0, LX/1fN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    invoke-virtual {v0, p1}, LX/0pn;->c(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    move-result-object v0

    .line 291527
    invoke-virtual {p0}, LX/1fN;->a()V

    .line 291528
    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 291519
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1fN;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 291520
    :goto_0
    monitor-exit p0

    return-void

    .line 291521
    :cond_0
    :try_start_1
    iget-boolean v0, p0, LX/1fN;->g:Z

    if-eqz v0, :cond_1

    .line 291522
    iget-object v0, p0, LX/1fN;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/1fN;->a:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 291523
    :cond_1
    iget-object v0, p0, LX/1fN;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/1fN;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, 0x6f1b9d67

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 291524
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1fN;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 291525
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/69n;)V
    .locals 2

    .prologue
    .line 291513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1fN;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291514
    iget-object v0, p0, LX/1fN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    iget-object v1, p0, LX/1fN;->b:LX/1fO;

    .line 291515
    iput-object v1, v0, LX/0pn;->A:LX/1fO;

    .line 291516
    :cond_0
    iget-object v0, p0, LX/1fN;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291517
    monitor-exit p0

    return-void

    .line 291518
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 291508
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1fN;->g:Z

    .line 291509
    iget-object v0, p0, LX/1fN;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/69n;

    .line 291510
    iget-object v2, p0, LX/1fN;->e:LX/0TD;

    new-instance v3, Lcom/facebook/api/feedcache/db/FeedDb$3;

    invoke-direct {v3, p0, v0}, Lcom/facebook/api/feedcache/db/FeedDb$3;-><init>(LX/1fN;LX/69n;)V

    const v0, -0x457df4d8

    invoke-static {v2, v3, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 291511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 291512
    :cond_0
    monitor-exit p0

    return-void
.end method
