.class public LX/1fG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static i:Z


# instance fields
.field public a:Landroid/net/Uri;

.field private b:Ljava/lang/String;

.field public c:I

.field private d:I

.field public e:LX/1fH;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/6VT;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0So;

.field public h:LX/2xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 291132
    const/4 v0, 0x0

    sput-boolean v0, LX/1fG;->i:Z

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 291133
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1fG;-><init>(Landroid/net/Uri;Ljava/lang/String;LX/0So;)V

    .line 291134
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;LX/0So;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291136
    iput-object p1, p0, LX/1fG;->a:Landroid/net/Uri;

    .line 291137
    iput-object p2, p0, LX/1fG;->b:Ljava/lang/String;

    .line 291138
    iput v1, p0, LX/1fG;->c:I

    .line 291139
    iput v1, p0, LX/1fG;->d:I

    .line 291140
    new-instance v0, LX/1fH;

    invoke-direct {v0, p0}, LX/1fH;-><init>(LX/1fG;)V

    iput-object v0, p0, LX/1fG;->e:LX/1fH;

    .line 291141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1fG;->f:Ljava/util/ArrayList;

    .line 291142
    iput-object p3, p0, LX/1fG;->g:LX/0So;

    .line 291143
    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 291144
    const/4 v0, 0x0

    sput-boolean v0, LX/1fG;->i:Z

    .line 291145
    return-void
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 291146
    const/4 v0, 0x1

    sput-boolean v0, LX/1fG;->i:Z

    .line 291147
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 291148
    instance-of v0, p1, LX/1fG;

    if-nez v0, :cond_0

    .line 291149
    const/4 v0, 0x0

    .line 291150
    :goto_0
    return v0

    .line 291151
    :cond_0
    check-cast p1, LX/1fG;

    .line 291152
    iget-object v0, p0, LX/1fG;->a:Landroid/net/Uri;

    iget-object v1, p1, LX/1fG;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 291153
    iget-object v0, p0, LX/1fG;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method
