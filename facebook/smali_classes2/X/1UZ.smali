.class public final enum LX/1UZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1UZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1UZ;

.field public static final enum ATTACHMENT_BOTTOM:LX/1UZ;

.field public static final enum ATTACHMENT_TOP:LX/1UZ;

.field public static final enum DEFAULT:LX/1UZ;

.field public static final enum DEFAULT_HEADER:LX/1UZ;

.field public static final enum DEFAULT_TEXT:LX/1UZ;

.field public static final enum DEFAULT_TEXT_HEADER:LX/1UZ;

.field public static final enum LEGACY_DEFAULT:LX/1UZ;

.field public static final enum LEGACY_ZERO:LX/1UZ;

.field public static final enum SHARED_ATTACHMENT:LX/1UZ;

.field public static final enum STORY_EDGE:LX/1UZ;

.field public static final enum SUBSTORY_HEADER:LX/1UZ;

.field public static final enum ZERO:LX/1UZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 256251
    new-instance v0, LX/1UZ;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->DEFAULT:LX/1UZ;

    .line 256252
    new-instance v0, LX/1UZ;

    const-string v1, "DEFAULT_TEXT"

    invoke-direct {v0, v1, v4}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->DEFAULT_TEXT:LX/1UZ;

    .line 256253
    new-instance v0, LX/1UZ;

    const-string v1, "ZERO"

    invoke-direct {v0, v1, v5}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->ZERO:LX/1UZ;

    .line 256254
    new-instance v0, LX/1UZ;

    const-string v1, "DEFAULT_HEADER"

    invoke-direct {v0, v1, v6}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->DEFAULT_HEADER:LX/1UZ;

    .line 256255
    new-instance v0, LX/1UZ;

    const-string v1, "STORY_EDGE"

    invoke-direct {v0, v1, v7}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->STORY_EDGE:LX/1UZ;

    .line 256256
    new-instance v0, LX/1UZ;

    const-string v1, "LEGACY_DEFAULT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->LEGACY_DEFAULT:LX/1UZ;

    .line 256257
    new-instance v0, LX/1UZ;

    const-string v1, "LEGACY_ZERO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->LEGACY_ZERO:LX/1UZ;

    .line 256258
    new-instance v0, LX/1UZ;

    const-string v1, "DEFAULT_TEXT_HEADER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->DEFAULT_TEXT_HEADER:LX/1UZ;

    .line 256259
    new-instance v0, LX/1UZ;

    const-string v1, "SUBSTORY_HEADER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->SUBSTORY_HEADER:LX/1UZ;

    .line 256260
    new-instance v0, LX/1UZ;

    const-string v1, "ATTACHMENT_TOP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->ATTACHMENT_TOP:LX/1UZ;

    .line 256261
    new-instance v0, LX/1UZ;

    const-string v1, "ATTACHMENT_BOTTOM"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->ATTACHMENT_BOTTOM:LX/1UZ;

    .line 256262
    new-instance v0, LX/1UZ;

    const-string v1, "SHARED_ATTACHMENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/1UZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1UZ;->SHARED_ATTACHMENT:LX/1UZ;

    .line 256263
    const/16 v0, 0xc

    new-array v0, v0, [LX/1UZ;

    sget-object v1, LX/1UZ;->DEFAULT:LX/1UZ;

    aput-object v1, v0, v3

    sget-object v1, LX/1UZ;->DEFAULT_TEXT:LX/1UZ;

    aput-object v1, v0, v4

    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    aput-object v1, v0, v5

    sget-object v1, LX/1UZ;->DEFAULT_HEADER:LX/1UZ;

    aput-object v1, v0, v6

    sget-object v1, LX/1UZ;->STORY_EDGE:LX/1UZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1UZ;->LEGACY_DEFAULT:LX/1UZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1UZ;->LEGACY_ZERO:LX/1UZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1UZ;->DEFAULT_TEXT_HEADER:LX/1UZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1UZ;->SUBSTORY_HEADER:LX/1UZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1UZ;->ATTACHMENT_TOP:LX/1UZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1UZ;->ATTACHMENT_BOTTOM:LX/1UZ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1UZ;->SHARED_ATTACHMENT:LX/1UZ;

    aput-object v2, v0, v1

    sput-object v0, LX/1UZ;->$VALUES:[LX/1UZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 256264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1UZ;
    .locals 1

    .prologue
    .line 256265
    const-class v0, LX/1UZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1UZ;

    return-object v0
.end method

.method public static values()[LX/1UZ;
    .locals 1

    .prologue
    .line 256266
    sget-object v0, LX/1UZ;->$VALUES:[LX/1UZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1UZ;

    return-object v0
.end method


# virtual methods
.method public final isSharedStoryAttachmentPadding()Z
    .locals 1

    .prologue
    .line 256267
    sget-object v0, LX/1UZ;->SHARED_ATTACHMENT:LX/1UZ;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isZero()Z
    .locals 1

    .prologue
    .line 256268
    sget-object v0, LX/1UZ;->ZERO:LX/1UZ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/1UZ;->LEGACY_ZERO:LX/1UZ;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
