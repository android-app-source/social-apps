.class public LX/1Ku;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hj;
.implements LX/0hk;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233329
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233330
    iput-object p1, p0, LX/1Ku;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 233331
    iput-object p2, p0, LX/1Ku;->b:LX/0Or;

    .line 233332
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 233333
    if-eqz p3, :cond_0

    const-string v0, "try_show_survey_on_result_integration_point_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 233334
    :cond_0
    :goto_0
    return-void

    .line 233335
    :cond_1
    const-string v0, "try_show_survey_on_result_integration_point_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 233336
    const-string v0, "try_show_survey_on_result_extra_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 233337
    iget-object v0, p0, LX/1Ku;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 233338
    iput-object v2, v0, LX/0gt;->a:Ljava/lang/String;

    .line 233339
    move-object v2, v0

    .line 233340
    instance-of v0, v1, Landroid/os/Bundle;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 233341
    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v2, v0}, LX/0gt;->a(Landroid/os/Bundle;)LX/0gt;

    .line 233342
    :cond_2
    iget-object v0, p0, LX/1Ku;->c:LX/1Iu;

    .line 233343
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 233344
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 233345
    iget-object v0, p0, LX/1Ku;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->B:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 233346
    if-nez v1, :cond_0

    .line 233347
    :goto_0
    return-void

    .line 233348
    :cond_0
    iget-object v0, p0, LX/1Ku;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 233349
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 233350
    move-object v0, v0

    .line 233351
    sget-object v1, LX/1x2;->LCAU:LX/1x2;

    invoke-virtual {v0, v1}, LX/0gt;->a(LX/1x2;)LX/0gt;

    move-result-object v1

    iget-object v0, p0, LX/1Ku;->c:LX/1Iu;

    .line 233352
    iget-object v2, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 233353
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0gt;->a(Landroid/content/Context;)V

    .line 233354
    iget-object v0, p0, LX/1Ku;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->B:LX/0Tn;

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 233355
    return-void
.end method
