.class public final LX/1u8;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:D

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 338291
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 338292
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    iput-object v0, p0, LX/1u8;->b:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 338293
    const/4 v0, 0x0

    iput-object v0, p0, LX/1u8;->j:LX/0x2;

    .line 338294
    instance-of v0, p0, LX/1u8;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 338295
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/1u8;
    .locals 4

    .prologue
    .line 338278
    new-instance v1, LX/1u8;

    invoke-direct {v1}, LX/1u8;-><init>()V

    .line 338279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 338280
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    iput-object v0, v1, LX/1u8;->b:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 338281
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1u8;->c:Ljava/lang/String;

    .line 338282
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1u8;->d:Ljava/lang/String;

    .line 338283
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v0

    iput-boolean v0, v1, LX/1u8;->e:Z

    .line 338284
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1u8;->f:Ljava/lang/String;

    .line 338285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    iput-object v0, v1, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 338286
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v2

    iput-wide v2, v1, LX/1u8;->h:D

    .line 338287
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1u8;->i:Ljava/lang/String;

    .line 338288
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 338289
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/1u8;->j:LX/0x2;

    .line 338290
    return-object v1
.end method


# virtual methods
.method public final a(D)LX/1u8;
    .locals 1

    .prologue
    .line 338260
    iput-wide p1, p0, LX/1u8;->h:D

    .line 338261
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)LX/1u8;
    .locals 0

    .prologue
    .line 338276
    iput-object p1, p0, LX/1u8;->b:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 338277
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)LX/1u8;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338274
    iput-object p1, p0, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 338275
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/1u8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338272
    iput-object p1, p0, LX/1u8;->c:Ljava/lang/String;

    .line 338273
    return-object p0
.end method

.method public final a(Z)LX/1u8;
    .locals 0

    .prologue
    .line 338270
    iput-boolean p1, p0, LX/1u8;->e:Z

    .line 338271
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 2

    .prologue
    .line 338268
    new-instance v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;-><init>(LX/1u8;)V

    .line 338269
    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/1u8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338266
    iput-object p1, p0, LX/1u8;->d:Ljava/lang/String;

    .line 338267
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/1u8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338264
    iput-object p1, p0, LX/1u8;->f:Ljava/lang/String;

    .line 338265
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/1u8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338262
    iput-object p1, p0, LX/1u8;->i:Ljava/lang/String;

    .line 338263
    return-object p0
.end method
