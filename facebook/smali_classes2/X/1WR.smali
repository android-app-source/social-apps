.class public LX/1WR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1WR;


# instance fields
.field public final a:LX/0if;

.field private final b:LX/1WS;

.field public final c:LX/0tJ;


# direct methods
.method public constructor <init>(LX/0if;LX/1WS;LX/0tJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268450
    iput-object p1, p0, LX/1WR;->a:LX/0if;

    .line 268451
    iput-object p2, p0, LX/1WR;->b:LX/1WS;

    .line 268452
    iput-object p3, p0, LX/1WR;->c:LX/0tJ;

    .line 268453
    return-void
.end method

.method public static a(LX/0QB;)LX/1WR;
    .locals 6

    .prologue
    .line 268464
    sget-object v0, LX/1WR;->d:LX/1WR;

    if-nez v0, :cond_1

    .line 268465
    const-class v1, LX/1WR;

    monitor-enter v1

    .line 268466
    :try_start_0
    sget-object v0, LX/1WR;->d:LX/1WR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 268467
    if-eqz v2, :cond_0

    .line 268468
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 268469
    new-instance p0, LX/1WR;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    .line 268470
    new-instance v4, LX/1WS;

    invoke-direct {v4}, LX/1WS;-><init>()V

    .line 268471
    move-object v4, v4

    .line 268472
    move-object v4, v4

    .line 268473
    check-cast v4, LX/1WS;

    invoke-static {v0}, LX/0tJ;->a(LX/0QB;)LX/0tJ;

    move-result-object v5

    check-cast v5, LX/0tJ;

    invoke-direct {p0, v3, v4, v5}, LX/1WR;-><init>(LX/0if;LX/1WS;LX/0tJ;)V

    .line 268474
    move-object v0, p0

    .line 268475
    sput-object v0, LX/1WR;->d:LX/1WR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268476
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 268477
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 268478
    :cond_1
    sget-object v0, LX/1WR;->d:LX/1WR;

    return-object v0

    .line 268479
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 268480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 268459
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 268460
    :cond_0
    :goto_0
    return-void

    .line 268461
    :cond_1
    iget-object v0, p0, LX/1WR;->a:LX/0if;

    .line 268462
    sget-object v1, LX/0ig;->l:LX/0ih;

    move-object v1, v1

    .line 268463
    invoke-static {p0, p1}, LX/1WR;->h(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;)S

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3, p2}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    goto :goto_0
.end method

.method public static h(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;)S
    .locals 2

    .prologue
    .line 268458
    iget-object v0, p0, LX/1WR;->b:LX/1WS;

    invoke-static {p1}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1WS;->a(Ljava/lang/String;)S

    move-result v0

    return v0
.end method


# virtual methods
.method public final d(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 268456
    const-string v0, "clicked_on_bling_bar"

    invoke-static {p0, p1, v0}, LX/1WR;->a(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 268457
    return-void
.end method

.method public final f(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 268454
    const-string v0, "clicked_on_like"

    invoke-static {p0, p1, v0}, LX/1WR;->a(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 268455
    return-void
.end method
