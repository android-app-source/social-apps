.class public final enum LX/1lB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1lB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1lB;

.field public static final enum DATE_PICKER_STYLE:LX/1lB;

.field public static final enum DAY_HOUR_FUTURE_STYLE:LX/1lB;

.field public static final enum DURATION_LARGEST_UNIT_STYLE:LX/1lB;

.field public static final enum EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

.field public static final enum EVENTS_RELATIVE_STYLE:LX/1lB;

.field public static final enum EXACT_STREAM_RELATIVE_STYLE:LX/1lB;

.field public static final enum EXACT_TIME_DATE_DOT_STYLE:LX/1lB;

.field public static final enum EXACT_TIME_DATE_LOWERCASE_STYLE:LX/1lB;

.field public static final enum EXACT_TIME_DATE_STYLE:LX/1lB;

.field public static final enum FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

.field public static final enum HOUR_MINUTE_STYLE:LX/1lB;

.field public static final enum MONTH_DAY_LONG_STYLE:LX/1lB;

.field public static final enum MONTH_DAY_YEAR_LONG_STYLE:LX/1lB;

.field public static final enum MONTH_DAY_YEAR_SHORT_STYLE:LX/1lB;

.field public static final enum MONTH_DAY_YEAR_STYLE:LX/1lB;

.field public static final enum NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

.field public static final enum NUMERIC_MONTH_DAY_YEAR_STYLE:LX/1lB;

.field public static final enum RFC1123_STYLE:LX/1lB;

.field public static final enum SHORTEST_RELATIVE_FUTURE_STYLE:LX/1lB;

.field public static final enum SHORTEST_RELATIVE_PAST_STYLE:LX/1lB;

.field public static final enum SHORT_DATE_STYLE:LX/1lB;

.field public static final enum STREAM_RELATIVE_STYLE:LX/1lB;

.field public static final enum THREAD_DATE_STYLE:LX/1lB;

.field public static final enum WEEK_DAY_STYLE:LX/1lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 311355
    new-instance v0, LX/1lB;

    const-string v1, "HOUR_MINUTE_STYLE"

    invoke-direct {v0, v1, v3}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    .line 311356
    new-instance v0, LX/1lB;

    const-string v1, "WEEK_DAY_STYLE"

    invoke-direct {v0, v1, v4}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->WEEK_DAY_STYLE:LX/1lB;

    .line 311357
    new-instance v0, LX/1lB;

    const-string v1, "STREAM_RELATIVE_STYLE"

    invoke-direct {v0, v1, v5}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    .line 311358
    new-instance v0, LX/1lB;

    const-string v1, "NOTIFICATIONS_STREAM_RELATIVE_STYLE"

    invoke-direct {v0, v1, v6}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

    .line 311359
    new-instance v0, LX/1lB;

    const-string v1, "EXACT_STREAM_RELATIVE_STYLE"

    invoke-direct {v0, v1, v7}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->EXACT_STREAM_RELATIVE_STYLE:LX/1lB;

    .line 311360
    new-instance v0, LX/1lB;

    const-string v1, "MONTH_DAY_YEAR_STYLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    .line 311361
    new-instance v0, LX/1lB;

    const-string v1, "MONTH_DAY_YEAR_SHORT_STYLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->MONTH_DAY_YEAR_SHORT_STYLE:LX/1lB;

    .line 311362
    new-instance v0, LX/1lB;

    const-string v1, "MONTH_DAY_YEAR_LONG_STYLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->MONTH_DAY_YEAR_LONG_STYLE:LX/1lB;

    .line 311363
    new-instance v0, LX/1lB;

    const-string v1, "MONTH_DAY_LONG_STYLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->MONTH_DAY_LONG_STYLE:LX/1lB;

    .line 311364
    new-instance v0, LX/1lB;

    const-string v1, "NUMERIC_MONTH_DAY_YEAR_STYLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->NUMERIC_MONTH_DAY_YEAR_STYLE:LX/1lB;

    .line 311365
    new-instance v0, LX/1lB;

    const-string v1, "EVENTS_RELATIVE_STYLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->EVENTS_RELATIVE_STYLE:LX/1lB;

    .line 311366
    new-instance v0, LX/1lB;

    const-string v1, "EVENTS_RELATIVE_DATE_STYLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    .line 311367
    new-instance v0, LX/1lB;

    const-string v1, "FUZZY_RELATIVE_DATE_STYLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    .line 311368
    new-instance v0, LX/1lB;

    const-string v1, "EXACT_TIME_DATE_STYLE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->EXACT_TIME_DATE_STYLE:LX/1lB;

    .line 311369
    new-instance v0, LX/1lB;

    const-string v1, "EXACT_TIME_DATE_DOT_STYLE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->EXACT_TIME_DATE_DOT_STYLE:LX/1lB;

    .line 311370
    new-instance v0, LX/1lB;

    const-string v1, "EXACT_TIME_DATE_LOWERCASE_STYLE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->EXACT_TIME_DATE_LOWERCASE_STYLE:LX/1lB;

    .line 311371
    new-instance v0, LX/1lB;

    const-string v1, "DATE_PICKER_STYLE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    .line 311372
    new-instance v0, LX/1lB;

    const-string v1, "SHORT_DATE_STYLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    .line 311373
    new-instance v0, LX/1lB;

    const-string v1, "THREAD_DATE_STYLE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->THREAD_DATE_STYLE:LX/1lB;

    .line 311374
    new-instance v0, LX/1lB;

    const-string v1, "SHORTEST_RELATIVE_PAST_STYLE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->SHORTEST_RELATIVE_PAST_STYLE:LX/1lB;

    .line 311375
    new-instance v0, LX/1lB;

    const-string v1, "SHORTEST_RELATIVE_FUTURE_STYLE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->SHORTEST_RELATIVE_FUTURE_STYLE:LX/1lB;

    .line 311376
    new-instance v0, LX/1lB;

    const-string v1, "DAY_HOUR_FUTURE_STYLE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->DAY_HOUR_FUTURE_STYLE:LX/1lB;

    .line 311377
    new-instance v0, LX/1lB;

    const-string v1, "RFC1123_STYLE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->RFC1123_STYLE:LX/1lB;

    .line 311378
    new-instance v0, LX/1lB;

    const-string v1, "DURATION_LARGEST_UNIT_STYLE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/1lB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lB;->DURATION_LARGEST_UNIT_STYLE:LX/1lB;

    .line 311379
    const/16 v0, 0x18

    new-array v0, v0, [LX/1lB;

    sget-object v1, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    aput-object v1, v0, v3

    sget-object v1, LX/1lB;->WEEK_DAY_STYLE:LX/1lB;

    aput-object v1, v0, v4

    sget-object v1, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    aput-object v1, v0, v5

    sget-object v1, LX/1lB;->NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

    aput-object v1, v0, v6

    sget-object v1, LX/1lB;->EXACT_STREAM_RELATIVE_STYLE:LX/1lB;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1lB;->MONTH_DAY_YEAR_SHORT_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1lB;->MONTH_DAY_YEAR_LONG_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1lB;->MONTH_DAY_LONG_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1lB;->NUMERIC_MONTH_DAY_YEAR_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1lB;->EVENTS_RELATIVE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/1lB;->EXACT_TIME_DATE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/1lB;->EXACT_TIME_DATE_DOT_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/1lB;->EXACT_TIME_DATE_LOWERCASE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/1lB;->THREAD_DATE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/1lB;->SHORTEST_RELATIVE_PAST_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/1lB;->SHORTEST_RELATIVE_FUTURE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/1lB;->DAY_HOUR_FUTURE_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/1lB;->RFC1123_STYLE:LX/1lB;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/1lB;->DURATION_LARGEST_UNIT_STYLE:LX/1lB;

    aput-object v2, v0, v1

    sput-object v0, LX/1lB;->$VALUES:[LX/1lB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 311354
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1lB;
    .locals 1

    .prologue
    .line 311353
    const-class v0, LX/1lB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1lB;

    return-object v0
.end method

.method public static values()[LX/1lB;
    .locals 1

    .prologue
    .line 311352
    sget-object v0, LX/1lB;->$VALUES:[LX/1lB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1lB;

    return-object v0
.end method
