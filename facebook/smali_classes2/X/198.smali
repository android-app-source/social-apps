.class public LX/198;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/198;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1YD;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1YD;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/SparseIntArray;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 207361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/198;->a:Ljava/util/List;

    .line 207363
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/198;->c:Landroid/util/SparseIntArray;

    .line 207364
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/198;->b:Ljava/util/List;

    .line 207365
    return-void
.end method

.method public static a(LX/0QB;)LX/198;
    .locals 3

    .prologue
    .line 207349
    sget-object v0, LX/198;->e:LX/198;

    if-nez v0, :cond_1

    .line 207350
    const-class v1, LX/198;

    monitor-enter v1

    .line 207351
    :try_start_0
    sget-object v0, LX/198;->e:LX/198;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 207352
    if-eqz v2, :cond_0

    .line 207353
    :try_start_1
    new-instance v0, LX/198;

    invoke-direct {v0}, LX/198;-><init>()V

    .line 207354
    move-object v0, v0

    .line 207355
    sput-object v0, LX/198;->e:LX/198;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207356
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 207357
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207358
    :cond_1
    sget-object v0, LX/198;->e:LX/198;

    return-object v0

    .line 207359
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 207360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1YD;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 207342
    const/4 v1, 0x0

    .line 207343
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/198;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 207344
    iget-object v0, p0, LX/198;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1YD;

    .line 207345
    if-nez v1, :cond_1

    .line 207346
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 207347
    :cond_1
    invoke-interface {v0}, LX/1YD;->b()LX/3lx;

    move-result-object v3

    invoke-interface {v1}, LX/1YD;->b()LX/3lx;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3lx;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-ltz v3, :cond_0

    move-object v0, v1

    goto :goto_1

    .line 207348
    :cond_2
    return-object v1
.end method

.method public final a(LX/1YD;)V
    .locals 4

    .prologue
    .line 207336
    invoke-interface {p1}, LX/1YD;->b()LX/3lx;

    move-result-object v0

    invoke-virtual {v0}, LX/3lx;->ordinal()I

    move-result v0

    .line 207337
    iget-object v1, p0, LX/198;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    .line 207338
    iget-object v2, p0, LX/198;->c:Landroid/util/SparseIntArray;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 207339
    if-nez v1, :cond_0

    .line 207340
    iget-object v0, p0, LX/198;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207341
    :cond_0
    return-void
.end method

.method public final b(LX/1YD;)V
    .locals 3

    .prologue
    .line 207366
    invoke-interface {p1}, LX/1YD;->b()LX/3lx;

    move-result-object v0

    invoke-virtual {v0}, LX/3lx;->ordinal()I

    move-result v0

    .line 207367
    iget-object v1, p0, LX/198;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 207368
    if-gez v1, :cond_0

    .line 207369
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unset a marker which was not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207370
    :cond_0
    iget-object v2, p0, LX/198;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 207371
    if-nez v1, :cond_1

    .line 207372
    iget-object v0, p0, LX/198;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 207373
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 207334
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/198;->d:Z

    .line 207335
    return-void
.end method

.method public final c(LX/1YD;)V
    .locals 1

    .prologue
    .line 207330
    iget-boolean v0, p0, LX/198;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/198;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207331
    :cond_0
    :goto_0
    return-void

    .line 207332
    :cond_1
    iget-object v0, p0, LX/198;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207333
    invoke-virtual {p0, p1}, LX/198;->a(LX/1YD;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 207328
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/198;->d:Z

    .line 207329
    return-void
.end method

.method public final e()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 207323
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/198;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 207324
    iget-object v0, p0, LX/198;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1YD;

    invoke-virtual {p0, v0}, LX/198;->b(LX/1YD;)V

    .line 207325
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 207326
    :cond_0
    iget-object v0, p0, LX/198;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 207327
    return-void
.end method
