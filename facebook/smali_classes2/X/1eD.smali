.class public final LX/1eD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 287819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I
    .locals 18

    .prologue
    .line 287831
    if-nez p1, :cond_0

    .line 287832
    const/4 v2, 0x0

    .line 287833
    :goto_0
    return v2

    .line 287834
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 287835
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 287836
    const/4 v2, 0x0

    .line 287837
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k()LX/0Px;

    move-result-object v4

    .line 287838
    if-eqz v4, :cond_4

    .line 287839
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    new-array v7, v2, [I

    .line 287840
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 287841
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v7, v3

    .line 287842
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 287843
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v2}, LX/186;->a([IZ)I

    move-result v2

    move v3, v2

    .line 287844
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v7

    .line 287845
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 287846
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 287847
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 287848
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1eD;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v11

    .line 287849
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v12

    .line 287850
    const/4 v2, 0x0

    .line 287851
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r()LX/0Px;

    move-result-object v13

    .line 287852
    if-eqz v13, :cond_3

    .line 287853
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    new-array v14, v2, [I

    .line 287854
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 287855
    invoke-virtual {v13, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v14, v4

    .line 287856
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 287857
    :cond_2
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 287858
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I

    move-result v4

    .line 287859
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 287860
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->u()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LX/186;->c(Ljava/util/List;)I

    move-result v14

    .line 287861
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/1eD;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v15

    .line 287862
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->x()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 287863
    const/16 v17, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 287864
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 287865
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 287866
    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v3}, LX/186;->b(II)V

    .line 287867
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 287868
    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 287869
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 287870
    const/4 v3, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 287871
    const/4 v3, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 287872
    const/16 v3, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 287873
    const/16 v3, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 287874
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 287875
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 287876
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 287877
    const/16 v2, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 287878
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->w()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 287879
    const/16 v2, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 287880
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 287881
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 287820
    if-nez p1, :cond_0

    .line 287821
    :goto_0
    return v0

    .line 287822
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287823
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 287824
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 287825
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 287826
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287827
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 287828
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 287829
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287830
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 287426
    if-nez p1, :cond_0

    .line 287427
    :goto_0
    return v0

    .line 287428
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287429
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 287430
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 287431
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 287432
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 287433
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287434
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I
    .locals 6

    .prologue
    .line 287798
    if-nez p1, :cond_0

    .line 287799
    const/4 v0, 0x0

    .line 287800
    :goto_0
    return v0

    .line 287801
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 287802
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287803
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 287804
    if-nez v2, :cond_1

    .line 287805
    :goto_1
    move v2, v3

    .line 287806
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->aE()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 287807
    const/4 v4, 0x5

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 287808
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v0}, LX/186;->b(II)V

    .line 287809
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287810
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 287811
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 287812
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287813
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 287814
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 287815
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 287816
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 287817
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 287818
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 287791
    if-nez p1, :cond_0

    .line 287792
    :goto_0
    return v0

    .line 287793
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287794
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 287795
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287796
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287797
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 287784
    if-nez p1, :cond_0

    .line 287785
    :goto_0
    return v0

    .line 287786
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287787
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 287788
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287789
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287790
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTimelineAppSection;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 287777
    if-nez p1, :cond_0

    .line 287778
    :goto_0
    return v0

    .line 287779
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineAppSection;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287780
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 287781
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287782
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287783
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLinkableUtilAddLikeSentenceLinkGraphQL"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 287724
    if-nez p0, :cond_1

    .line 287725
    :cond_0
    :goto_0
    return-object v2

    .line 287726
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 287727
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 287728
    if-nez p0, :cond_3

    .line 287729
    :goto_1
    move v1, v4

    .line 287730
    if-eqz v1, :cond_0

    .line 287731
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 287732
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 287733
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 287734
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 287735
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 287736
    const-string v1, "LinkifyUtilConverter.getLinkableUtilAddLikeSentenceLinkGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 287737
    :cond_2
    new-instance v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 287738
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v5

    .line 287739
    if-eqz v5, :cond_7

    .line 287740
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 287741
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 287742
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    const/4 v7, 0x0

    .line 287743
    if-nez v1, :cond_8

    .line 287744
    :goto_3
    move v1, v7

    .line 287745
    aput v1, v6, v3

    .line 287746
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 287747
    :cond_4
    invoke-virtual {v0, v6, v8}, LX/186;->a([IZ)I

    move-result v1

    move v3, v1

    .line 287748
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v6

    .line 287749
    if-eqz v6, :cond_6

    .line 287750
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    new-array v7, v1, [I

    move v5, v4

    .line 287751
    :goto_5
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_5

    .line 287752
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    const/4 v9, 0x0

    .line 287753
    if-nez v1, :cond_9

    .line 287754
    :goto_6
    move v1, v9

    .line 287755
    aput v1, v7, v5

    .line 287756
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_5

    .line 287757
    :cond_5
    invoke-virtual {v0, v7, v8}, LX/186;->a([IZ)I

    move-result v1

    .line 287758
    :goto_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 287759
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 287760
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 287761
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 287762
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 287763
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 287764
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_6
    move v1, v4

    goto :goto_7

    :cond_7
    move v3, v4

    goto :goto_4

    .line 287765
    :cond_8
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 287766
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->b()I

    move-result v9

    invoke-virtual {v0, v7, v9, v7}, LX/186;->a(III)V

    .line 287767
    const/4 v9, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v10

    invoke-virtual {v0, v9, v10, v7}, LX/186;->a(III)V

    .line 287768
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    .line 287769
    invoke-virtual {v0, v7}, LX/186;->d(I)V

    goto :goto_3

    .line 287770
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v10

    invoke-static {v0, v10}, LX/1eD;->b(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I

    move-result v10

    .line 287771
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 287772
    invoke-virtual {v0, v9, v10}, LX/186;->b(II)V

    .line 287773
    const/4 v10, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v11

    invoke-virtual {v0, v10, v11, v9}, LX/186;->a(III)V

    .line 287774
    const/4 v10, 0x2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v11

    invoke-virtual {v0, v10, v11, v9}, LX/186;->a(III)V

    .line 287775
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 287776
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto :goto_6
.end method

.method public static b(LX/186;Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 287709
    if-nez p1, :cond_0

    .line 287710
    :goto_0
    return v2

    .line 287711
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v3

    .line 287712
    if-eqz v3, :cond_2

    .line 287713
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 287714
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 287715
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-static {p0, v0}, LX/1eD;->c(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I

    move-result v0

    aput v0, v4, v1

    .line 287716
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 287717
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 287718
    :goto_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 287719
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->b()I

    move-result v1

    invoke-virtual {p0, v2, v1, v2}, LX/186;->a(III)V

    .line 287720
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v1

    invoke-virtual {p0, v5, v1, v2}, LX/186;->a(III)V

    .line 287721
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 287722
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 287723
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public static b(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I
    .locals 13

    .prologue
    .line 287882
    if-nez p1, :cond_0

    .line 287883
    const/4 v0, 0x0

    .line 287884
    :goto_0
    return v0

    .line 287885
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    .line 287886
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 287887
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTimelineAppSection;)I

    move-result v4

    .line 287888
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I

    move-result v5

    .line 287889
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 287890
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 287891
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 287892
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v9

    .line 287893
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v10

    .line 287894
    const/4 v0, 0x0

    .line 287895
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v11

    .line 287896
    if-eqz v11, :cond_2

    .line 287897
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v0

    new-array v12, v0, [I

    .line 287898
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 287899
    invoke-virtual {v11, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I

    move-result v0

    aput v0, v12, v1

    .line 287900
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 287901
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v12, v0}, LX/186;->a([IZ)I

    move-result v0

    .line 287902
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287903
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 287904
    const/16 v12, 0xf

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 287905
    const/4 v12, 0x0

    invoke-virtual {p0, v12, v2}, LX/186;->b(II)V

    .line 287906
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 287907
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 287908
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 287909
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 287910
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 287911
    const/4 v2, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->o()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 287912
    const/4 v2, 0x7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->p()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 287913
    const/16 v2, 0x8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->q()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 287914
    const/16 v2, 0x9

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 287915
    const/16 v2, 0xa

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 287916
    const/16 v2, 0xb

    invoke-virtual {p0, v2, v10}, LX/186;->b(II)V

    .line 287917
    const/16 v2, 0xc

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 287918
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287919
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 287920
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287921
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 287668
    if-nez p1, :cond_0

    .line 287669
    :goto_0
    return v2

    .line 287670
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 287671
    if-eqz v3, :cond_2

    .line 287672
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 287673
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 287674
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    const/4 v6, 0x0

    .line 287675
    if-nez v0, :cond_3

    .line 287676
    :goto_2
    move v0, v6

    .line 287677
    aput v0, v4, v1

    .line 287678
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 287679
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 287680
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287681
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 287682
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 287683
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 287684
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 287685
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 287686
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    const/4 v8, 0x0

    .line 287687
    if-nez v7, :cond_4

    .line 287688
    :goto_4
    move v7, v8

    .line 287689
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 287690
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 287691
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 287692
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 287693
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    .line 287694
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 287695
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    const/4 v12, 0x0

    .line 287696
    if-nez v11, :cond_5

    .line 287697
    :goto_5
    move v11, v12

    .line 287698
    const/4 v12, 0x3

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 287699
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 287700
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v10}, LX/186;->b(II)V

    .line 287701
    const/4 v8, 0x2

    invoke-virtual {p0, v8, v11}, LX/186;->b(II)V

    .line 287702
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 287703
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4

    .line 287704
    :cond_5
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 287705
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 287706
    invoke-virtual {p0, v12, v0}, LX/186;->b(II)V

    .line 287707
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 287708
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_5
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 287633
    if-nez p0, :cond_1

    .line 287634
    :cond_0
    :goto_0
    return-object v2

    .line 287635
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 287636
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 287637
    if-nez p0, :cond_3

    .line 287638
    :goto_1
    move v1, v4

    .line 287639
    if-eqz v1, :cond_0

    .line 287640
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 287641
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 287642
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 287643
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 287644
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 287645
    const-string v1, "LinkifyUtilConverter.getLinkableUtilAddLinksGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 287646
    :cond_2
    new-instance v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 287647
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v5

    .line 287648
    if-eqz v5, :cond_7

    .line 287649
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 287650
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 287651
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    invoke-static {v0, v1}, LX/1eD;->b(LX/186;Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;)I

    move-result v1

    aput v1, v6, v3

    .line 287652
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 287653
    :cond_4
    invoke-virtual {v0, v6, v8}, LX/186;->a([IZ)I

    move-result v1

    move v3, v1

    .line 287654
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v6

    .line 287655
    if-eqz v6, :cond_6

    .line 287656
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    new-array v7, v1, [I

    move v5, v4

    .line 287657
    :goto_4
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_5

    .line 287658
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {v0, v1}, LX/1eD;->c(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v1

    aput v1, v7, v5

    .line 287659
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4

    .line 287660
    :cond_5
    invoke-virtual {v0, v7, v8}, LX/186;->a([IZ)I

    move-result v1

    .line 287661
    :goto_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 287662
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 287663
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 287664
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 287665
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 287666
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 287667
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_6
    move v1, v4

    goto :goto_5

    :cond_7
    move v3, v4

    goto :goto_3
.end method

.method private static c(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 287607
    if-nez p1, :cond_0

    .line 287608
    :goto_0
    return v0

    .line 287609
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 287610
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 287611
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 287612
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 287613
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    const/4 v6, 0x0

    .line 287614
    if-nez v5, :cond_1

    .line 287615
    :goto_1
    move v5, v6

    .line 287616
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 287617
    const/16 v7, 0x9

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 287618
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287619
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 287620
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 287621
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 287622
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 287623
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 287624
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287625
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 287626
    :cond_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 287627
    const/4 v8, 0x3

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 287628
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v8

    invoke-virtual {p0, v6, v8, v6}, LX/186;->a(III)V

    .line 287629
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v7}, LX/186;->b(II)V

    .line 287630
    const/4 v7, 0x2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 287631
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 287632
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static c(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 287598
    if-nez p1, :cond_0

    .line 287599
    :goto_0
    return v0

    .line 287600
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-static {p0, v1}, LX/1eD;->d(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I

    move-result v1

    .line 287601
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 287602
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287603
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 287604
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 287605
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287606
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 287571
    if-nez p0, :cond_1

    .line 287572
    :cond_0
    :goto_0
    return-object v2

    .line 287573
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 287574
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 287575
    if-nez p0, :cond_3

    .line 287576
    :goto_1
    move v1, v4

    .line 287577
    if-eqz v1, :cond_0

    .line 287578
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 287579
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 287580
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 287581
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 287582
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 287583
    const-string v1, "LinkifyUtilConverter.getLinkableUtilApplyActorsLinksGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 287584
    :cond_2
    new-instance v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 287585
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v5

    .line 287586
    if-eqz v5, :cond_5

    .line 287587
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 287588
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 287589
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {v0, v1}, LX/1eD;->c(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v1

    aput v1, v6, v3

    .line 287590
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 287591
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 287592
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 287593
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 287594
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 287595
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 287596
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 287597
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_3
.end method

.method private static d(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I
    .locals 13

    .prologue
    .line 287531
    if-nez p1, :cond_0

    .line 287532
    const/4 v0, 0x0

    .line 287533
    :goto_0
    return v0

    .line 287534
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    .line 287535
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 287536
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->k()Lcom/facebook/graphql/model/GraphQLTimelineAppSection;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTimelineAppSection;)I

    move-result v4

    .line 287537
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->l()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I

    move-result v5

    .line 287538
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 287539
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 287540
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 287541
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v9

    .line 287542
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {p0, v0}, LX/1eD;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v10

    .line 287543
    const/4 v0, 0x0

    .line 287544
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v11

    .line 287545
    if-eqz v11, :cond_2

    .line 287546
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v0

    new-array v12, v0, [I

    .line 287547
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 287548
    invoke-virtual {v11, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    invoke-static {p0, v0}, LX/1eD;->a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I

    move-result v0

    aput v0, v12, v1

    .line 287549
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 287550
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v12, v0}, LX/186;->a([IZ)I

    move-result v0

    .line 287551
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287552
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 287553
    const/16 v12, 0xf

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 287554
    const/4 v12, 0x0

    invoke-virtual {p0, v12, v2}, LX/186;->b(II)V

    .line 287555
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 287556
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 287557
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 287558
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 287559
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 287560
    const/4 v2, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->o()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 287561
    const/4 v2, 0x7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->p()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 287562
    const/16 v2, 0x8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->q()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 287563
    const/16 v2, 0x9

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 287564
    const/16 v2, 0xa

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 287565
    const/16 v2, 0xb

    invoke-virtual {p0, v2, v10}, LX/186;->b(II)V

    .line 287566
    const/16 v2, 0xc

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 287567
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287568
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 287569
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287570
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 287524
    if-nez p1, :cond_0

    .line 287525
    :goto_0
    return v0

    .line 287526
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 287527
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 287528
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 287529
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 287530
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1y8;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 287497
    if-nez p0, :cond_1

    .line 287498
    :cond_0
    :goto_0
    return-object v2

    .line 287499
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 287500
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 287501
    if-nez p0, :cond_3

    .line 287502
    :goto_1
    move v1, v4

    .line 287503
    if-eqz v1, :cond_0

    .line 287504
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 287505
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 287506
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 287507
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 287508
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 287509
    const-string v1, "LinkifyUtilConverter.getLinkableUtilApplyAggregatedLinksGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 287510
    :cond_2
    new-instance v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 287511
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v5

    .line 287512
    if-eqz v5, :cond_5

    .line 287513
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 287514
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 287515
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    invoke-static {v0, v1}, LX/1eD;->b(LX/186;Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;)I

    move-result v1

    aput v1, v6, v3

    .line 287516
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 287517
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 287518
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 287519
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 287520
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 287521
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 287522
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 287523
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_3
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/8sH;
    .locals 15
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLinkableUtilBoldLinksOnlyGraphQL"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 287435
    if-nez p0, :cond_1

    .line 287436
    :cond_0
    :goto_0
    return-object v2

    .line 287437
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 287438
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 287439
    if-nez p0, :cond_3

    .line 287440
    :goto_1
    move v1, v4

    .line 287441
    if-eqz v1, :cond_0

    .line 287442
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 287443
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 287444
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 287445
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 287446
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 287447
    const-string v1, "LinkifyUtilConverter.getLinkableUtilBoldLinksOnlyGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 287448
    :cond_2
    new-instance v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilBoldLinksOnlyGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilBoldLinksOnlyGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 287449
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v5

    .line 287450
    if-eqz v5, :cond_7

    .line 287451
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 287452
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 287453
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    const/4 v7, 0x0

    .line 287454
    if-nez v1, :cond_8

    .line 287455
    :goto_3
    move v1, v7

    .line 287456
    aput v1, v6, v3

    .line 287457
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 287458
    :cond_4
    invoke-virtual {v0, v6, v8}, LX/186;->a([IZ)I

    move-result v1

    move v3, v1

    .line 287459
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v6

    .line 287460
    if-eqz v6, :cond_6

    .line 287461
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    new-array v7, v1, [I

    move v5, v4

    .line 287462
    :goto_5
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_5

    .line 287463
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    const/4 v9, 0x0

    .line 287464
    if-nez v1, :cond_9

    .line 287465
    :goto_6
    move v1, v9

    .line 287466
    aput v1, v7, v5

    .line 287467
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_5

    .line 287468
    :cond_5
    invoke-virtual {v0, v7, v8}, LX/186;->a([IZ)I

    move-result v1

    .line 287469
    :goto_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 287470
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 287471
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 287472
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 287473
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 287474
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 287475
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_6
    move v1, v4

    goto :goto_7

    :cond_7
    move v3, v4

    goto :goto_4

    .line 287476
    :cond_8
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 287477
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->b()I

    move-result v9

    invoke-virtual {v0, v7, v9, v7}, LX/186;->a(III)V

    .line 287478
    const/4 v9, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->c()I

    move-result v10

    invoke-virtual {v0, v9, v10, v7}, LX/186;->a(III)V

    .line 287479
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    .line 287480
    invoke-virtual {v0, v7}, LX/186;->d(I)V

    goto :goto_3

    .line 287481
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v10

    const/4 v11, 0x0

    .line 287482
    if-nez v10, :cond_a

    .line 287483
    :goto_8
    move v10, v11

    .line 287484
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 287485
    invoke-virtual {v0, v9, v10}, LX/186;->b(II)V

    .line 287486
    const/4 v10, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v11

    invoke-virtual {v0, v10, v11, v9}, LX/186;->a(III)V

    .line 287487
    const/4 v10, 0x2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v11

    invoke-virtual {v0, v10, v11, v9}, LX/186;->a(III)V

    .line 287488
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 287489
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto :goto_6

    .line 287490
    :cond_a
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    invoke-virtual {v0, v12}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v12

    .line 287491
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 287492
    const/4 v14, 0x2

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 287493
    invoke-virtual {v0, v11, v12}, LX/186;->b(II)V

    .line 287494
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v13}, LX/186;->b(II)V

    .line 287495
    invoke-virtual {v0}, LX/186;->d()I

    move-result v11

    .line 287496
    invoke-virtual {v0, v11}, LX/186;->d(I)V

    goto :goto_8
.end method
