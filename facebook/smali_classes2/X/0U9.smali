.class public final LX/0U9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/prefs/shared/cache/FbSharedPreferencesCache$OnChangesListener;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/prefs/shared/cache/FbSharedPreferencesCache$OnChangesListener;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 64209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64210
    iput-object p1, p0, LX/0U9;->a:LX/0QB;

    .line 64211
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64208
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0U9;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64207
    const/4 v0, 0x0

    return v0
.end method
