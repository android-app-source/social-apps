.class public final LX/0oa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 141680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141681
    iput-object p1, p0, LX/0oa;->a:LX/0QB;

    .line 141682
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 141761
    new-instance v0, LX/0oa;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0oa;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 141762
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 141763
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0oa;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 141684
    packed-switch p2, :pswitch_data_0

    .line 141685
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141686
    :pswitch_0
    new-instance v0, LX/0rA;

    const/16 v1, 0x1b43

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0rA;-><init>(LX/0Ot;)V

    .line 141687
    move-object v0, v0

    .line 141688
    :goto_0
    return-object v0

    .line 141689
    :pswitch_1
    new-instance v0, LX/0rC;

    const/16 v1, 0x1cbb

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0rC;-><init>(LX/0Ot;)V

    .line 141690
    move-object v0, v0

    .line 141691
    goto :goto_0

    .line 141692
    :pswitch_2
    new-instance v0, LX/0rD;

    const/16 v1, 0x1cbc

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0rD;-><init>(LX/0Ot;)V

    .line 141693
    move-object v0, v0

    .line 141694
    goto :goto_0

    .line 141695
    :pswitch_3
    new-instance v0, LX/0rE;

    const/16 v1, 0x1cbd

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0rE;-><init>(LX/0Ot;)V

    .line 141696
    move-object v0, v0

    .line 141697
    goto :goto_0

    .line 141698
    :pswitch_4
    new-instance v1, LX/0rF;

    const/16 v0, 0x6b5

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {p1}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v0

    check-cast v0, LX/0Ym;

    invoke-direct {v1, p0, v0}, LX/0rF;-><init>(LX/0Ot;LX/0Ym;)V

    .line 141699
    move-object v0, v1

    .line 141700
    goto :goto_0

    .line 141701
    :pswitch_5
    new-instance v0, LX/AjV;

    const/16 v1, 0x1cbf

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AjV;-><init>(LX/0Ot;)V

    .line 141702
    move-object v0, v0

    .line 141703
    goto :goto_0

    .line 141704
    :pswitch_6
    new-instance v0, LX/AjW;

    const/16 v1, 0x6b5

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AjW;-><init>(LX/0Ot;)V

    .line 141705
    move-object v0, v0

    .line 141706
    goto :goto_0

    .line 141707
    :pswitch_7
    new-instance v0, LX/2tM;

    const/16 v1, 0x1cc1

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2tM;-><init>(LX/0Ot;)V

    .line 141708
    move-object v0, v0

    .line 141709
    goto :goto_0

    .line 141710
    :pswitch_8
    new-instance v0, LX/AjX;

    const/16 v1, 0x1cc2

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AjX;-><init>(LX/0Ot;)V

    .line 141711
    move-object v0, v0

    .line 141712
    goto :goto_0

    .line 141713
    :pswitch_9
    new-instance v0, LX/AjY;

    const/16 v1, 0x1cc2

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AjY;-><init>(LX/0Ot;)V

    .line 141714
    move-object v0, v0

    .line 141715
    goto :goto_0

    .line 141716
    :pswitch_a
    new-instance v0, LX/DNZ;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DNZ;-><init>(LX/0Ot;)V

    .line 141717
    move-object v0, v0

    .line 141718
    goto/16 :goto_0

    .line 141719
    :pswitch_b
    new-instance v0, LX/DNa;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DNa;-><init>(LX/0Ot;)V

    .line 141720
    move-object v0, v0

    .line 141721
    goto/16 :goto_0

    .line 141722
    :pswitch_c
    new-instance v0, LX/DNb;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DNb;-><init>(LX/0Ot;)V

    .line 141723
    move-object v0, v0

    .line 141724
    goto/16 :goto_0

    .line 141725
    :pswitch_d
    new-instance v0, LX/DNi;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DNi;-><init>(LX/0Ot;)V

    .line 141726
    move-object v0, v0

    .line 141727
    goto/16 :goto_0

    .line 141728
    :pswitch_e
    new-instance v0, LX/DNr;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DNr;-><init>(LX/0Ot;)V

    .line 141729
    move-object v0, v0

    .line 141730
    goto/16 :goto_0

    .line 141731
    :pswitch_f
    new-instance v0, LX/DNs;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DNs;-><init>(LX/0Ot;)V

    .line 141732
    move-object v0, v0

    .line 141733
    goto/16 :goto_0

    .line 141734
    :pswitch_10
    new-instance v0, LX/DNt;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DNt;-><init>(LX/0Ot;)V

    .line 141735
    move-object v0, v0

    .line 141736
    goto/16 :goto_0

    .line 141737
    :pswitch_11
    new-instance v0, LX/DO7;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DO7;-><init>(LX/0Ot;)V

    .line 141738
    move-object v0, v0

    .line 141739
    goto/16 :goto_0

    .line 141740
    :pswitch_12
    new-instance v0, LX/DO8;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DO8;-><init>(LX/0Ot;)V

    .line 141741
    move-object v0, v0

    .line 141742
    goto/16 :goto_0

    .line 141743
    :pswitch_13
    new-instance v0, LX/DO9;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DO9;-><init>(LX/0Ot;)V

    .line 141744
    move-object v0, v0

    .line 141745
    goto/16 :goto_0

    .line 141746
    :pswitch_14
    new-instance v0, LX/DOA;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DOA;-><init>(LX/0Ot;)V

    .line 141747
    move-object v0, v0

    .line 141748
    goto/16 :goto_0

    .line 141749
    :pswitch_15
    new-instance v0, LX/DOB;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DOB;-><init>(LX/0Ot;)V

    .line 141750
    move-object v0, v0

    .line 141751
    goto/16 :goto_0

    .line 141752
    :pswitch_16
    new-instance v0, LX/DOC;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DOC;-><init>(LX/0Ot;)V

    .line 141753
    move-object v0, v0

    .line 141754
    goto/16 :goto_0

    .line 141755
    :pswitch_17
    new-instance v0, LX/DOG;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DOG;-><init>(LX/0Ot;)V

    .line 141756
    move-object v0, v0

    .line 141757
    goto/16 :goto_0

    .line 141758
    :pswitch_18
    new-instance v0, LX/DOH;

    const/16 v1, 0x240f

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DOH;-><init>(LX/0Ot;)V

    .line 141759
    move-object v0, v0

    .line 141760
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 141683
    const/16 v0, 0x19

    return v0
.end method
