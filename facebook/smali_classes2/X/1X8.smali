.class public final enum LX/1X8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1X8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1X8;

.field public static final enum FOLLOWUP_FEEDUNIT:LX/1X8;

.field public static final enum GAP_PART_DEFINITION:LX/1X8;

.field public static final enum MAYBE_HAS_COMMENTS_BELOW:LX/1X8;

.field public static final enum NEED_BOTTOM_DIVIDER:LX/1X8;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 270575
    new-instance v0, LX/1X8;

    const-string v1, "GAP_PART_DEFINITION"

    invoke-direct {v0, v1, v2}, LX/1X8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    .line 270576
    new-instance v0, LX/1X8;

    const-string v1, "FOLLOWUP_FEEDUNIT"

    invoke-direct {v0, v1, v3}, LX/1X8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X8;->FOLLOWUP_FEEDUNIT:LX/1X8;

    .line 270577
    new-instance v0, LX/1X8;

    const-string v1, "NEED_BOTTOM_DIVIDER"

    invoke-direct {v0, v1, v4}, LX/1X8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    .line 270578
    new-instance v0, LX/1X8;

    const-string v1, "MAYBE_HAS_COMMENTS_BELOW"

    invoke-direct {v0, v1, v5}, LX/1X8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1X8;->MAYBE_HAS_COMMENTS_BELOW:LX/1X8;

    .line 270579
    const/4 v0, 0x4

    new-array v0, v0, [LX/1X8;

    sget-object v1, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    aput-object v1, v0, v2

    sget-object v1, LX/1X8;->FOLLOWUP_FEEDUNIT:LX/1X8;

    aput-object v1, v0, v3

    sget-object v1, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    aput-object v1, v0, v4

    sget-object v1, LX/1X8;->MAYBE_HAS_COMMENTS_BELOW:LX/1X8;

    aput-object v1, v0, v5

    sput-object v0, LX/1X8;->$VALUES:[LX/1X8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 270572
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1X8;
    .locals 1

    .prologue
    .line 270574
    const-class v0, LX/1X8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1X8;

    return-object v0
.end method

.method public static values()[LX/1X8;
    .locals 1

    .prologue
    .line 270573
    sget-object v0, LX/1X8;->$VALUES:[LX/1X8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1X8;

    return-object v0
.end method
