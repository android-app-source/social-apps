.class public LX/13N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/13N;


# instance fields
.field public final a:LX/13O;


# direct methods
.method public constructor <init>(LX/13O;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176598
    iput-object p1, p0, LX/13N;->a:LX/13O;

    .line 176599
    return-void
.end method

.method public static a(LX/0QB;)LX/13N;
    .locals 4

    .prologue
    .line 176619
    sget-object v0, LX/13N;->b:LX/13N;

    if-nez v0, :cond_1

    .line 176620
    const-class v1, LX/13N;

    monitor-enter v1

    .line 176621
    :try_start_0
    sget-object v0, LX/13N;->b:LX/13N;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 176622
    if-eqz v2, :cond_0

    .line 176623
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 176624
    new-instance p0, LX/13N;

    invoke-static {v0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v3

    check-cast v3, LX/13O;

    invoke-direct {p0, v3}, LX/13N;-><init>(LX/13O;)V

    .line 176625
    move-object v0, p0

    .line 176626
    sput-object v0, LX/13N;->b:LX/13N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176627
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 176628
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 176629
    :cond_1
    sget-object v0, LX/13N;->b:LX/13N;

    return-object v0

    .line 176630
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 176631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2fy;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 176611
    sget-object v0, LX/2fz;->a:[I

    invoke-virtual {p0}, LX/2fy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 176612
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown CounterType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176613
    :pswitch_0
    const-string v0, "qp_impression_counter"

    .line 176614
    :goto_0
    return-object v0

    .line 176615
    :pswitch_1
    const-string v0, "qp_primary_action_counter"

    goto :goto_0

    .line 176616
    :pswitch_2
    const-string v0, "qp_secondary_action_counter"

    goto :goto_0

    .line 176617
    :pswitch_3
    const-string v0, "qp_dismiss_action_counter"

    goto :goto_0

    .line 176618
    :pswitch_4
    const-string v0, "qp_dismiss_event_counter"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/2fy;)I
    .locals 2

    .prologue
    .line 176610
    iget-object v0, p0, LX/13N;->a:LX/13O;

    invoke-static {p2}, LX/13N;->a(LX/2fy;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/13O;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;LX/2fy;)J
    .locals 2

    .prologue
    .line 176609
    iget-object v0, p0, LX/13N;->a:LX/13O;

    invoke-static {p2}, LX/13N;->a(LX/2fy;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/13O;->b(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)I
    .locals 3

    .prologue
    .line 176608
    iget-object v0, p0, LX/13N;->a:LX/13O;

    invoke-static {p2}, LX/13N;->a(LX/2fy;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/13O;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final clearUserData()V
    .locals 7

    .prologue
    .line 176603
    invoke-static {}, LX/2fy;->values()[LX/2fy;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 176604
    iget-object v4, p0, LX/13N;->a:LX/13O;

    invoke-static {v3}, LX/13N;->a(LX/2fy;)Ljava/lang/String;

    move-result-object v3

    .line 176605
    iget-object v5, v4, LX/13O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    invoke-static {v3}, LX/13O;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 176606
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 176607
    :cond_0
    return-void
.end method

.method public final d(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)J
    .locals 3

    .prologue
    .line 176602
    iget-object v0, p0, LX/13N;->a:LX/13O;

    invoke-static {p2}, LX/13N;->a(LX/2fy;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/13O;->b(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)V
    .locals 3

    .prologue
    .line 176600
    iget-object v0, p0, LX/13N;->a:LX/13O;

    invoke-static {p2}, LX/13N;->a(LX/2fy;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/13O;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176601
    return-void
.end method
