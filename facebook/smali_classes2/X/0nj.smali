.class public final LX/0nj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/4re;

.field private b:LX/4rf;

.field private c:LX/4rk;

.field private d:LX/4ri;

.field private e:LX/4rj;

.field private f:LX/4rh;

.field private g:LX/4rg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 136558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136559
    iput-object v0, p0, LX/0nj;->a:LX/4re;

    .line 136560
    iput-object v0, p0, LX/0nj;->b:LX/4rf;

    .line 136561
    iput-object v0, p0, LX/0nj;->c:LX/4rk;

    .line 136562
    iput-object v0, p0, LX/0nj;->d:LX/4ri;

    .line 136563
    iput-object v0, p0, LX/0nj;->e:LX/4rj;

    .line 136564
    iput-object v0, p0, LX/0nj;->f:LX/4rh;

    .line 136565
    iput-object v0, p0, LX/0nj;->g:LX/4rg;

    .line 136566
    return-void
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 136567
    invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    .line 136568
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 136569
    new-instance v2, LX/4rc;

    invoke-direct {v2, v1, v0, p0}, LX/4rc;-><init>(Ljava/lang/Class;ILjava/lang/Object;)V

    return-object v2
.end method

.method public static a(Ljava/util/Set;[Ljava/lang/Object;)Ljava/util/HashSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TT;>;[TT;)",
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 136550
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 136551
    if-eqz p0, :cond_0

    .line 136552
    invoke-virtual {v1, p0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 136553
    :cond_0
    if-eqz p1, :cond_1

    .line 136554
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 136555
    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 136556
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136557
    :cond_1
    return-object v1
.end method

.method public static a([Ljava/lang/Object;)Ljava/util/HashSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 136544
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 136545
    if-eqz p0, :cond_0

    .line 136546
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 136547
    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 136548
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136549
    :cond_0
    return-object v1
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)[TT;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 136527
    array-length v3, p0

    move v1, v2

    .line 136528
    :goto_0
    if-ge v1, v3, :cond_3

    .line 136529
    aget-object v0, p0, v1

    if-ne v0, p1, :cond_2

    .line 136530
    if-nez v1, :cond_1

    move-object v0, p0

    .line 136531
    :cond_0
    :goto_1
    return-object v0

    .line 136532
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 136533
    invoke-static {p0, v2, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 136534
    aput-object p1, v0, v2

    .line 136535
    add-int/lit8 v1, v1, 0x1

    .line 136536
    sub-int v2, v3, v1

    .line 136537
    if-lez v2, :cond_0

    .line 136538
    invoke-static {p0, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 136539
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 136540
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    add-int/lit8 v1, v3, 0x1

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 136541
    if-lez v3, :cond_4

    .line 136542
    invoke-static {p0, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 136543
    :cond_4
    aput-object p1, v0, v2

    goto :goto_1
.end method

.method public static b([Ljava/lang/Object;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/lang/Iterable",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 136526
    new-instance v0, LX/2Ve;

    invoke-direct {v0, p0}, LX/2Ve;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/4re;
    .locals 1

    .prologue
    .line 136570
    iget-object v0, p0, LX/0nj;->a:LX/4re;

    if-nez v0, :cond_0

    .line 136571
    new-instance v0, LX/4re;

    invoke-direct {v0}, LX/4re;-><init>()V

    iput-object v0, p0, LX/0nj;->a:LX/4re;

    .line 136572
    :cond_0
    iget-object v0, p0, LX/0nj;->a:LX/4re;

    return-object v0
.end method

.method public final b()LX/4rf;
    .locals 1

    .prologue
    .line 136523
    iget-object v0, p0, LX/0nj;->b:LX/4rf;

    if-nez v0, :cond_0

    .line 136524
    new-instance v0, LX/4rf;

    invoke-direct {v0}, LX/4rf;-><init>()V

    iput-object v0, p0, LX/0nj;->b:LX/4rf;

    .line 136525
    :cond_0
    iget-object v0, p0, LX/0nj;->b:LX/4rf;

    return-object v0
.end method

.method public final c()LX/4rk;
    .locals 1

    .prologue
    .line 136520
    iget-object v0, p0, LX/0nj;->c:LX/4rk;

    if-nez v0, :cond_0

    .line 136521
    new-instance v0, LX/4rk;

    invoke-direct {v0}, LX/4rk;-><init>()V

    iput-object v0, p0, LX/0nj;->c:LX/4rk;

    .line 136522
    :cond_0
    iget-object v0, p0, LX/0nj;->c:LX/4rk;

    return-object v0
.end method

.method public final d()LX/4ri;
    .locals 1

    .prologue
    .line 136517
    iget-object v0, p0, LX/0nj;->d:LX/4ri;

    if-nez v0, :cond_0

    .line 136518
    new-instance v0, LX/4ri;

    invoke-direct {v0}, LX/4ri;-><init>()V

    iput-object v0, p0, LX/0nj;->d:LX/4ri;

    .line 136519
    :cond_0
    iget-object v0, p0, LX/0nj;->d:LX/4ri;

    return-object v0
.end method

.method public final e()LX/4rj;
    .locals 1

    .prologue
    .line 136514
    iget-object v0, p0, LX/0nj;->e:LX/4rj;

    if-nez v0, :cond_0

    .line 136515
    new-instance v0, LX/4rj;

    invoke-direct {v0}, LX/4rj;-><init>()V

    iput-object v0, p0, LX/0nj;->e:LX/4rj;

    .line 136516
    :cond_0
    iget-object v0, p0, LX/0nj;->e:LX/4rj;

    return-object v0
.end method

.method public final f()LX/4rh;
    .locals 1

    .prologue
    .line 136508
    iget-object v0, p0, LX/0nj;->f:LX/4rh;

    if-nez v0, :cond_0

    .line 136509
    new-instance v0, LX/4rh;

    invoke-direct {v0}, LX/4rh;-><init>()V

    iput-object v0, p0, LX/0nj;->f:LX/4rh;

    .line 136510
    :cond_0
    iget-object v0, p0, LX/0nj;->f:LX/4rh;

    return-object v0
.end method

.method public final g()LX/4rg;
    .locals 1

    .prologue
    .line 136511
    iget-object v0, p0, LX/0nj;->g:LX/4rg;

    if-nez v0, :cond_0

    .line 136512
    new-instance v0, LX/4rg;

    invoke-direct {v0}, LX/4rg;-><init>()V

    iput-object v0, p0, LX/0nj;->g:LX/4rg;

    .line 136513
    :cond_0
    iget-object v0, p0, LX/0nj;->g:LX/4rg;

    return-object v0
.end method
