.class public LX/0qk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g3;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0g3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 148351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0qk;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 148348
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148349
    invoke-interface {v0}, LX/0g3;->a()V

    goto :goto_0

    .line 148350
    :cond_0
    return-void
.end method

.method public final a(ILX/1lq;ILX/0ta;)V
    .locals 2

    .prologue
    .line 148318
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148319
    invoke-interface {v0, p1, p2, p3, p4}, LX/0g3;->a(ILX/1lq;ILX/0ta;)V

    goto :goto_0

    .line 148320
    :cond_0
    return-void
.end method

.method public final a(LX/0gf;)V
    .locals 2

    .prologue
    .line 148345
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148346
    invoke-interface {v0, p1}, LX/0g3;->a(LX/0gf;)V

    goto :goto_0

    .line 148347
    :cond_0
    return-void
.end method

.method public final a(LX/0gf;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 148342
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148343
    invoke-interface {v0, p1, p2}, LX/0g3;->a(LX/0gf;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 148344
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 148339
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148340
    invoke-interface {v0, p1}, LX/0g3;->a(Z)V

    goto :goto_0

    .line 148341
    :cond_0
    return-void
.end method

.method public final a(ZILX/0ta;)V
    .locals 2

    .prologue
    .line 148353
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148354
    invoke-interface {v0, p1, p2, p3}, LX/0g3;->a(ZILX/0ta;)V

    goto :goto_0

    .line 148355
    :cond_0
    return-void
.end method

.method public final a(ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V
    .locals 9

    .prologue
    .line 148336
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    .line 148337
    invoke-interface/range {v0 .. v7}, LX/0g3;->a(ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V

    goto :goto_0

    .line 148338
    :cond_0
    return-void
.end method

.method public final a(LX/0g3;)Z
    .locals 1

    .prologue
    .line 148332
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148333
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148334
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 148335
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0gf;)V
    .locals 2

    .prologue
    .line 148329
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148330
    invoke-interface {v0, p1}, LX/0g3;->b(LX/0gf;)V

    goto :goto_0

    .line 148331
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 148324
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148325
    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0g3;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_1
    move v1, v0

    .line 148326
    goto :goto_0

    .line 148327
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 148328
    :cond_1
    return v1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 148321
    iget-object v0, p0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0g3;

    .line 148322
    invoke-interface {v0}, LX/0g4;->d()V

    goto :goto_0

    .line 148323
    :cond_0
    return-void
.end method
