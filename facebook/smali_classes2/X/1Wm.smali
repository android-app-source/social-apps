.class public LX/1Wm;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/39a;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1Wm",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/39a;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 269442
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 269443
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1Wm;->b:LX/0Zi;

    .line 269444
    iput-object p1, p0, LX/1Wm;->a:LX/0Ot;

    .line 269445
    return-void
.end method

.method public static a(LX/0QB;)LX/1Wm;
    .locals 4

    .prologue
    .line 269431
    const-class v1, LX/1Wm;

    monitor-enter v1

    .line 269432
    :try_start_0
    sget-object v0, LX/1Wm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 269433
    sput-object v2, LX/1Wm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 269434
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269435
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 269436
    new-instance v3, LX/1Wm;

    const/16 p0, 0x862

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Wm;-><init>(LX/0Ot;)V

    .line 269437
    move-object v0, v3

    .line 269438
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269439
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Wm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269440
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269441
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 269446
    check-cast p2, LX/39Y;

    .line 269447
    iget-object v0, p0, LX/1Wm;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/39a;

    iget-object v1, p2, LX/39Y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/39Y;->b:LX/1Wj;

    iget-object v3, p2, LX/39Y;->c:LX/1X1;

    const/4 p2, 0x6

    .line 269448
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    iget-object p0, v2, LX/1Wj;->c:LX/1Wh;

    iget p0, p0, LX/1Wh;->b:F

    float-to-int p0, p0

    invoke-interface {v4, v5, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    iget-object p0, v2, LX/1Wj;->c:LX/1Wh;

    iget p0, p0, LX/1Wh;->c:F

    float-to-int p0, p0

    invoke-interface {v4, v5, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    iget-object v5, v2, LX/1Wj;->c:LX/1Wh;

    iget v5, v5, LX/1Wh;->a:F

    float-to-int v5, v5

    invoke-interface {v4, p2, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    .line 269449
    iget-object v5, v2, LX/1Wj;->g:LX/1Wl;

    sget-object p0, LX/1Wl;->VISIBLE:LX/1Wl;

    invoke-virtual {v5, p0}, LX/1Wl;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 269450
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const p0, 0x7f0a0442

    invoke-virtual {v5, p0}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const p0, 0x7f0b0033

    invoke-interface {v5, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    const p0, 0x7f0b010f

    invoke-interface {v5, p2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 269451
    :cond_0
    iget-object v5, v0, LX/39a;->b:LX/1dr;

    invoke-virtual {v5, v1}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v5

    .line 269452
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {v2, p0, v5}, LX/1Wj;->b(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 269453
    iget-object p2, v0, LX/39a;->b:LX/1dr;

    invoke-virtual {p2, v1, p0}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/graphics/drawable/Drawable;)V

    .line 269454
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object p2

    invoke-virtual {p2, p0}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object p0

    invoke-virtual {p0}, LX/1n6;->b()LX/1dc;

    move-result-object p0

    move-object p0, p0

    .line 269455
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p2

    invoke-interface {p2, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object p2

    invoke-interface {p2, p0}, LX/1Dh;->b(LX/1dc;)LX/1Dh;

    move-result-object p0

    invoke-interface {v4, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 269456
    iget-object p0, v2, LX/1Wj;->h:LX/1Wl;

    sget-object p2, LX/1Wl;->VISIBLE:LX/1Wl;

    invoke-virtual {p0, p2}, LX/1Wl;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 269457
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object p0

    const p2, 0x7f0a0462

    invoke-virtual {p0, p2}, LX/25Q;->i(I)LX/25Q;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b0033

    invoke-interface {p0, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    invoke-interface {v4, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 269458
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {v2, p0, v5}, LX/1Wj;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 269459
    iget-object p2, v0, LX/39a;->b:LX/1dr;

    invoke-virtual {p2, v1, p0}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/graphics/drawable/Drawable;)V

    .line 269460
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object p2

    invoke-virtual {p2, p0}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object p0

    invoke-virtual {p0}, LX/1n6;->b()LX/1dc;

    move-result-object p0

    move-object v5, p0

    .line 269461
    invoke-interface {v4, v5}, LX/1Dh;->b(LX/1dc;)LX/1Dh;

    move-result-object v4

    sget-object v5, LX/39a;->a:Landroid/util/SparseArray;

    invoke-interface {v4, v5}, LX/1Dh;->b(Landroid/util/SparseArray;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 269462
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 269429
    invoke-static {}, LX/1dS;->b()V

    .line 269430
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/39Z;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1Wm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 269421
    new-instance v1, LX/39Y;

    invoke-direct {v1, p0}, LX/39Y;-><init>(LX/1Wm;)V

    .line 269422
    iget-object v2, p0, LX/1Wm;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/39Z;

    .line 269423
    if-nez v2, :cond_0

    .line 269424
    new-instance v2, LX/39Z;

    invoke-direct {v2, p0}, LX/39Z;-><init>(LX/1Wm;)V

    .line 269425
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/39Z;->a$redex0(LX/39Z;LX/1De;IILX/39Y;)V

    .line 269426
    move-object v1, v2

    .line 269427
    move-object v0, v1

    .line 269428
    return-object v0
.end method
