.class public final LX/1qC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Or",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# instance fields
.field public final mAnnotation:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public final mScopeUnawareInjector:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330150
    iput-object p1, p0, LX/1qC;->mScopeUnawareInjector:LX/0QB;

    .line 330151
    iput-object p2, p0, LX/1qC;->mAnnotation:Ljava/lang/Class;

    .line 330152
    return-void
.end method

.method public static getInstance(LX/0QB;Ljava/lang/Class;Ljava/lang/Class;)LX/1qM;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            "Ljava/lang/Class",
            "<",
            "LX/1qM;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/1qM;"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    invoke-static {v0}, LX/0QA;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v1, "com.facebook.appirater.api.annotation.AppiraterQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p2, LX/BZs;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-static {p0}, LX/3g8;->b(LX/0QB;)LX/3g8;

    move-result-object v1

    check-cast v1, LX/3g8;

    new-instance p1, LX/BZo;

    invoke-direct {p1}, LX/BZo;-><init>()V

    move-object p1, p1

    move-object p1, p1

    check-cast p1, LX/BZo;

    invoke-direct {p2, v0, v1, p1}, LX/BZs;-><init>(LX/18V;LX/3g8;LX/BZo;)V

    move-object v0, p2

    check-cast v0, LX/1qM;

    goto :goto_0

    :sswitch_1
    const-string v1, "com.facebook.photos.upload.service.UploadServiceQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/8L7;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto :goto_0

    :sswitch_2
    const-string v1, "com.facebook.messaging.send.service.PendingSendQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Ito;->a(LX/0QB;)LX/Ito;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto :goto_0

    :sswitch_3
    const-string v1, "com.facebook.devicebasedlogin.protocol.DeviceBasedLoginQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;->a(LX/0QB;)Lcom/facebook/devicebasedlogin/protocol/DeviceBasedLoginServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto :goto_0

    :sswitch_4
    const-string v1, "com.facebook.payments.history.protocol.PaymentHistoryProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FRL;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto :goto_0

    :sswitch_5
    const-string v1, "com.facebook.messaging.media.upload.udp.UDPServiceQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FIX;->b(LX/0QB;)LX/FIX;

    move-result-object v0

    check-cast v0, LX/FIX;

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto :goto_0

    :sswitch_6
    const-string v1, "com.facebook.feed.protocol.NewsFeedFetchQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Al5;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "com.facebook.privacy.service.PrivacyDataQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/8PY;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_8
    const-string v1, "com.facebook.api.reportable_entity.ReportableEntityNegativeActionsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/8wk;->a(LX/0QB;)LX/8wk;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_9
    const-string v1, "com.facebook.search.service.GraphSearchDataQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FgT;->b(LX/0QB;)LX/FgT;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_a
    const-string v1, "com.facebook.notifications.server.NotificationsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/3St;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_b
    const-string v1, "com.facebook.timeline.services.ProfileActionQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/G4T;->a(LX/0QB;)LX/G4T;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "com.facebook.fbservice.service.AuthQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2Xh;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {p0}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(LX/0QB;)Lcom/facebook/katana/server/handler/Fb4aAuthHandler;

    move-result-object v1

    check-cast v1, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;

    invoke-static {p0}, Lcom/facebook/auth/login/AuthServiceHandler;->a(LX/0QB;)Lcom/facebook/auth/login/AuthServiceHandler;

    move-result-object p1

    check-cast p1, Lcom/facebook/auth/login/AuthServiceHandler;

    invoke-static {v0, v1, p1}, LX/28c;->a(Ljava/lang/Boolean;Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Lcom/facebook/auth/login/AuthServiceHandler;)LX/1qM;

    move-result-object v0

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "com.facebook.adinterfaces.api.AdInterfacesMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/GDT;->a(LX/0QB;)LX/GDT;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "com.facebook.stickers.service.StickersDownloadQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/3dr;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "com.facebook.messaging.send.service.SendQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Ito;->a(LX/0QB;)LX/Ito;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "com.facebook.timeline.service.TimelineSectionQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2c3;->a(LX/0QB;)LX/2c3;

    move-result-object v0

    check-cast v0, LX/2c3;

    invoke-static {p0}, LX/26s;->a(LX/0QB;)LX/26s;

    move-result-object v1

    check-cast v1, LX/26s;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->b(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHandler;

    move-result-object p1

    check-cast p1, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;

    invoke-static {p0}, LX/G4S;->b(LX/0QB;)LX/G4S;

    move-result-object p2

    check-cast p2, LX/G4S;

    invoke-static {v0, v1, p1, p2}, LX/FqQ;->a(LX/2c3;LX/26s;Lcom/facebook/composer/publish/ComposerPublishServiceHandler;LX/G4S;)LX/1qM;

    move-result-object v0

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_11
    const-string v1, "com.facebook.messaging.contacts.picker.service.ContactPickerNearbyQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Jin;->a(LX/0QB;)LX/Jin;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "com.facebook.push.externalcloud.annotations.RegistrationQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/push/registration/RegistrationHandler;->a(LX/0QB;)Lcom/facebook/push/registration/RegistrationHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_13
    const-string v1, "com.facebook.sideloading.SideloadingServiceQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/sideloading/SideloadingServiceHandler;->a(LX/0QB;)Lcom/facebook/sideloading/SideloadingServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_14
    const-string v1, "com.facebook.adspayments.protocol.PaymentsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/GNr;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_15
    const-string v1, "com.facebook.messaging.media.upload.PhotoUploadHiResParallelQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FHJ;->a(LX/0QB;)LX/FHJ;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_16
    const-string v1, "com.facebook.messaging.media.upload.PhotoUploadHiResQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FHJ;->a(LX/0QB;)LX/FHJ;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_17
    const-string v1, "com.facebook.interstitial.service.InterstitialQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/6Y2;->a(LX/0QB;)LX/6Y2;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_18
    const-string v1, "com.facebook.backgroundlocation.settings.write.BackgroundLocationSettingsWriteQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Baa;->a(LX/0QB;)LX/Baa;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_19
    const-string v1, "com.facebook.pages.common.friendinviter.service.FriendInviterMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, LX/Dsx;

    const/16 v1, 0xb83

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 p1, 0x2b62

    invoke-static {p0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const/16 p2, 0x2b61

    invoke-static {p0, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, p1, p2}, LX/Dsx;-><init>(LX/0Or;LX/0Ot;LX/0Ot;)V

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_1a
    const-string v1, "com.facebook.nux.service.NuxQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/8DF;->a(LX/0QB;)LX/8DF;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_1b
    const-string v1, "com.facebook.messaging.media.upload.MediaUploadQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FHJ;->a(LX/0QB;)LX/FHJ;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_1c
    const-string v1, "com.facebook.friendsnearby.pingdialog.FriendsNearbyPingMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/IG8;->a(LX/0QB;)LX/IG8;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_1d
    const-string v1, "com.facebook.contactlogs.service.ContactLogsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/DAM;->a(LX/0QB;)LX/DAM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_1e
    const-string v1, "com.facebook.messaging.tincan.messenger.annotations.TincanCachingServiceChain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JpM;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_1f
    const-string v1, "com.facebook.location.foreground.ForegroundLocationQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JcM;->a(LX/0QB;)LX/JcM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_20
    const-string v1, "com.facebook.stickers.service.StickersQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/3dr;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_21
    const-string v1, "com.facebook.growth.service.GrowthQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/F9k;->a(LX/0QB;)LX/F9k;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_22
    const-string v1, "com.facebook.events.annotation.EventMethodQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p1, LX/IBu;

    const/16 v0, 0xb83

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    new-instance v0, LX/Blu;

    invoke-direct {v0}, LX/Blu;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/Blu;

    new-instance v1, LX/Blx;

    invoke-direct {v1}, LX/Blx;-><init>()V

    move-object v1, v1

    move-object v1, v1

    check-cast v1, LX/Blx;

    invoke-direct {p1, p2, v0, v1}, LX/IBu;-><init>(LX/0Or;LX/Blu;LX/Blx;)V

    move-object v0, p1

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_23
    const-string v1, "com.facebook.captcha.annotations.CaptchaQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, LX/GVj;

    const/16 v1, 0xb83

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 p1, 0x1889

    invoke-static {p0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const/16 p2, 0x188a

    invoke-static {p0, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, p1, p2}, LX/GVj;-><init>(LX/0Or;LX/0Ot;LX/0Ot;)V

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_24
    const-string v1, "com.facebook.messaging.media.download.MediaDownloadQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FFz;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_25
    const-string v1, "com.facebook.zero.annotations.ZeroTokenQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/zero/protocol/ZeroTokenHandler;->a(LX/0QB;)Lcom/facebook/zero/protocol/ZeroTokenHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_26
    const-string v1, "com.facebook.backgroundlocation.reporting.BackgroundLocationReportingQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v1

    check-cast v1, LX/11H;

    invoke-static {p0}, LX/3C6;->a(LX/0QB;)LX/3C6;

    move-result-object v2

    check-cast v2, LX/3C6;

    invoke-static {p0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object p1

    check-cast p1, LX/0aU;

    invoke-direct {p2, v0, v1, v2, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;-><init>(Landroid/content/Context;LX/11H;LX/3C6;LX/0aU;)V

    move-object v0, p2

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_27
    const-string v1, "com.facebook.contacts.service.DynamicContactDataQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/6Og;->a(LX/0QB;)LX/6Og;

    move-result-object v0

    check-cast v0, LX/6Og;

    invoke-static {p0}, LX/6Oh;->a(LX/0QB;)LX/6Oh;

    move-result-object v1

    check-cast v1, LX/6Oh;

    invoke-static {v0, v1}, LX/6Of;->a(LX/6Og;LX/6Oh;)LX/1qM;

    move-result-object v0

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_28
    const-string v1, "com.facebook.photos.data.service.PhotosServiceQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/8Gg;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_29
    const-string v1, "com.facebook.messaging.media.upload.PhotoUploadQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FHJ;->a(LX/0QB;)LX/FHJ;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_2a
    const-string v1, "com.facebook.nearby.protocol.NearbyMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/nearby/server/NearbyServiceHandler;->a(LX/0QB;)Lcom/facebook/nearby/server/NearbyServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_2b
    const-string v1, "com.facebook.payments.paymentmethods.picker.protocol.PickerProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/709;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_2c
    const-string v1, "com.facebook.contacts.service.AddressBookQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/contacts/service/ContactsServiceHandler;->a(LX/0QB;)Lcom/facebook/contacts/service/ContactsServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_2d
    const-string v1, "com.facebook.feedplugins.storygallerysurvey.service.StoryGallerySurveyActionsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;->a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_2e
    const-string v1, "com.facebook.payments.invoice.protocol.InvoiceProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/IyU;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_2f
    const-string v1, "com.facebook.saved.server.SavedQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/5uk;->a(LX/0QB;)LX/5uk;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_30
    const-string v1, "com.facebook.platform.common.server.PlatformOperationQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2QV;->a(LX/0QB;)LX/2QV;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_31
    const-string v1, "com.facebook.quickinvite.protocol.service.QuickInviteQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->a(LX/0QB;)Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_32
    const-string v1, "com.facebook.payments.auth.pin.protocol.PaymentPinQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/6nt;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_33
    const-string v1, "com.facebook.payments.checkout.protocol.CheckoutProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/6sT;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_34
    const-string v1, "com.facebook.payments.shipping.protocol.ShippingAddressProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/731;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_35
    const-string v1, "com.facebook.vault.service.VaultQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/ERx;->a(LX/0QB;)LX/ERx;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_36
    const-string v1, "com.facebook.katana.activity.codegenerator.data.CodeGeneratorServiceQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->a(LX/0QB;)Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_37
    const-string v1, "com.facebook.messaging.localfetch.LocalFetchQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FFy;->a(LX/0QB;)LX/FFy;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_38
    const-string v1, "com.facebook.messaging.accountswitch.protocol.SwitchAccountsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;->a(LX/0QB;)Lcom/facebook/messaging/accountswitch/protocol/SwitchAccountsServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_39
    const-string v1, "com.facebook.feed.protocol.NewsFeedPostingQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Al6;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_3a
    const-string v1, "com.facebook.contacts.service.ContactsFetcherQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/contacts/service/ContactsServiceHandler;->a(LX/0QB;)Lcom/facebook/contacts/service/ContactsServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_3b
    const-string v1, "com.facebook.messaging.emoji.service.MessagingEmojiQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, LX/CMV;

    invoke-static {p0}, LX/CMW;->b(LX/0QB;)LX/CMW;

    move-result-object v0

    check-cast v0, LX/CMW;

    invoke-direct {v1, v0}, LX/CMV;-><init>(LX/CMW;)V

    move-object v0, v1

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_3c
    const-string v1, "com.facebook.megaphone.api.MegaphoneQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/B9H;->a(LX/0QB;)LX/B9H;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_3d
    const-string v1, "com.facebook.messaging.sync.service.MessagesSyncQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JrW;->a(LX/0QB;)LX/JrW;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_3e
    const-string v1, "com.facebook.share.protocol.ShareMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/BOO;->a(LX/0QB;)LX/BOO;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_3f
    const-string v1, "com.facebook.si.annotations.LinkshimQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p1, LX/D0T;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lC;

    invoke-direct {p1, v0, v1, v2}, LX/D0T;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0WJ;LX/0lC;)V

    move-object v0, p1

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_40
    const-string v1, "com.facebook.payments.paymentmethods.cardform.protocol.CardFormProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/6yr;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_41
    const-string v1, "com.facebook.languages.switcher.service.LanguageSwitcherQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Gy0;->a(LX/0QB;)LX/Gy0;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_42
    const-string v1, "com.facebook.aldrin.service.AldrinQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2Z1;->a(LX/0QB;)LX/2Z1;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_43
    const-string v1, "com.facebook.messaging.media.upload.MediaGetFbidQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FGZ;->a(LX/0QB;)LX/FGZ;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_44
    const-string v1, "com.facebook.payments.contactinfo.protocol.ContactInfoProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/6wM;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_45
    const-string v1, "com.facebook.friends.service.FriendingQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/86a;->a(LX/0QB;)LX/86a;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_46
    const-string v1, "com.facebook.structuredsurvey.api.PostSurveyQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->a(LX/0QB;)Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_47
    const-string v1, "com.facebook.places.pagetopics.FetchPageTopicsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/9kI;->a(LX/0QB;)LX/9kI;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_48
    const-string v1, "com.facebook.payments.p2p.protocol.PaymentQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Izv;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_49
    const-string v1, "com.facebook.pages.adminedpages.service.AdminedPagesQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/3f1;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_4a
    const-string v1, "com.facebook.groups.service.GroupsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/B2u;->a(LX/0QB;)LX/B2u;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_4b
    const-string v1, "com.facebook.common.pagesprotocol.PagesReviewMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/D9w;->a(LX/0QB;)LX/D9w;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_4c
    const-string v1, "com.facebook.zero.upsell.annotations.UpsellPromoQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;

    const/16 v0, 0xb83

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    new-instance v0, LX/7YJ;

    invoke-direct {v0}, LX/7YJ;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/7YJ;

    new-instance p2, LX/7YK;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {p2, v1}, LX/7YK;-><init>(Landroid/content/res/Resources;)V

    move-object v1, p2

    check-cast v1, LX/7YK;

    const/16 p2, 0x15ab

    invoke-static {p0, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-direct {v2, p1, v0, v1, p2}, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;-><init>(LX/0Or;LX/7YJ;LX/7YK;LX/0Or;)V

    move-object v0, v2

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_4d
    const-string v1, "com.facebook.greetingcards.create.GreetingCardQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;

    const/16 v3, 0xb83

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/7mC;->b(LX/0QB;)LX/7mC;

    move-result-object v4

    check-cast v4, LX/7mC;

    invoke-static {p0}, LX/8OJ;->b(LX/0QB;)LX/8OJ;

    move-result-object v5

    check-cast v5, LX/8OJ;

    invoke-static {p0}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v6

    check-cast v6, LX/73w;

    invoke-static {p0}, LX/8LE;->b(LX/0QB;)LX/8Ne;

    move-result-object v7

    check-cast v7, LX/8Ne;

    invoke-direct/range {v2 .. v7}, Lcom/facebook/greetingcards/create/GreetingCardUploadHandler;-><init>(LX/0Or;LX/7mC;LX/8OJ;LX/73w;LX/8Ne;)V

    move-object v0, v2

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_4e
    const-string v1, "com.facebook.messaging.service.multicache.PushTraceQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Jp4;->a(LX/0QB;)LX/Jp4;

    move-result-object v0

    check-cast v0, LX/Jp4;

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_4f
    const-string v1, "com.facebook.messaging.service.multicache.FacebookCachingServiceChain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JpG;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_50
    const-string v1, "com.facebook.messaging.media.service.LocalMediaQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Ii6;->b(LX/0QB;)LX/Ii6;

    move-result-object v0

    check-cast v0, LX/Ii6;

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_51
    const-string v1, "com.facebook.messaging.tincan.messenger.TincanQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/IuL;->a(LX/0QB;)LX/IuL;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_52
    const-string v1, "com.facebook.api.negative_feedback.NegativeFeedbackActionsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/facebook/api/negative_feedback/NegativeFeedbackActionHandler;

    const/16 v0, 0xb83

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    new-instance v0, LX/8wf;

    invoke-direct {v0}, LX/8wf;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/8wf;

    invoke-direct {v1, v2, v0}, Lcom/facebook/api/negative_feedback/NegativeFeedbackActionHandler;-><init>(LX/0Or;LX/8wf;)V

    move-object v0, v1

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_53
    const-string v1, "com.facebook.bookmark.client.BookmarkSyncQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2lv;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_54
    const-string v1, "com.facebook.katana.server.AuthenticateCredentialsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Gwf;->a(LX/0QB;)LX/Gwf;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_55
    const-string v1, "com.facebook.messaging.blocking.api.GetBlockedPeopleQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;->a(LX/0QB;)Lcom/facebook/messaging/blocking/api/GetBlockedPeopleServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_56
    const-string v1, "com.facebook.pages.browser.data.service.PagesBrowserMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/IwZ;->a(LX/0QB;)LX/IwZ;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_57
    const-string v1, "com.facebook.contacts.service.ContactsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/contacts/service/ContactsServiceHandler;->a(LX/0QB;)Lcom/facebook/contacts/service/ContactsServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_58
    const-string v1, "com.facebook.facecast.protocol.FacecastServiceQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v2, LX/Aaj;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    const/16 v4, 0x1bef

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1bec

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1bed

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1bee

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1be7    # 1.001E-41f

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1be6    # 1.0008E-41f

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1beb

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v2 .. v10}, LX/Aaj;-><init>(LX/18V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    move-object v0, v2

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_59
    const-string v1, "com.facebook.account.recovery.common.protocol.AccountRecoveryQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v2, LX/GBJ;

    const/16 v3, 0xb83

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x6b

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1671

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1673

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1670

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1674

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, LX/GBJ;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    move-object v0, v2

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_5a
    const-string v1, "com.facebook.registration.service.AccountRegistrationQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/HaF;->a(LX/0QB;)LX/HaF;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_5b
    const-string v1, "com.facebook.messaging.media.upload.PhotoUploadParallelQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FHJ;->a(LX/0QB;)LX/FHJ;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_5c
    const-string v1, "com.facebook.katana.server.SimpleDataFetchQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Gwh;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_5d
    const-string v1, "com.facebook.payments.settings.protocol.PaymentSettingsProtocolQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FRx;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_5e
    const-string v1, "com.facebook.feed.annotations.ForNewsfeed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/3nJ;->b(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_5f
    const-string v1, "com.facebook.uberbar.api.FetchUberbarResultQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/G5S;->a(LX/0QB;)LX/G5S;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_60
    const-string v1, "com.facebook.timeline.aboutpage.service.TimelineCollectionsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/J8m;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_61
    const-string v1, "com.facebook.auth.login.CheckApprovedMachineQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;->a(LX/0QB;)Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_62
    const-string v1, "com.facebook.messaging.payment.sync.service.PaymentsSyncQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/ImT;->a(LX/0QB;)LX/ImT;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_63
    const-string v1, "com.facebook.katana.server.LoginApprovalResendCodeQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;->a(LX/0QB;)Lcom/facebook/katana/server/handler/LoginApprovalResendCodeServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_64
    const-string v1, "com.facebook.composer.system.savedsession.memsync.ComposerSavedSessionQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/1qL;->a(LX/0QB;)LX/1qL;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_65
    const-string v1, "com.facebook.search.service.GraphSearchLogQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FgT;->b(LX/0QB;)LX/FgT;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_66
    const-string v1, "com.facebook.goodwill.publish.GoodwillPublishUploadQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->b(LX/0QB;)Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_67
    const-string v1, "com.facebook.feed.protocol.NewsFeedMainQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/3nI;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_68
    const-string v1, "com.facebook.messaging.service.multicache.MultiCacheThreadsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JpI;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_69
    const-string v1, "com.facebook.profile.inforequest.service.InfoRequestQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/J7P;->a(LX/0QB;)LX/J7P;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_6a
    const-string v1, "com.facebook.contacts.upload.ContactsUploadQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/94x;->a(LX/0QB;)LX/94x;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_6b
    const-string v1, "com.facebook.messaging.media.upload.VideoTranscodeQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FHX;->a(LX/0QB;)LX/FHX;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_6c
    const-string v1, "com.facebook.pages.common.actionbar.blueservice.PagesCommonActionBarMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/H8A;->a(LX/0QB;)LX/H8A;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_6d
    const-string v1, "com.facebook.commerce.storefront.api.StoreFrontQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;->a(LX/0QB;)Lcom/facebook/commerce/storefront/api/StoreFrontServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_6e
    const-string v1, "com.facebook.timeline.service.TimelineHeaderQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/G4O;->b(LX/0QB;)LX/G4O;

    move-result-object v0

    check-cast v0, LX/G4O;

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_6f
    const-string v1, "com.facebook.abtest.qe.service.module.QuickExperimentQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->a(LX/0QB;)Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_70
    const-string v1, "com.facebook.feed.protocol.UFIQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Al7;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_71
    const-string v1, "com.facebook.messaging.business.nativesignup.annotations.NativeSignUpQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v3, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v0

    check-cast v0, LX/18V;

    new-instance v1, LX/Ia5;

    invoke-direct {v1}, LX/Ia5;-><init>()V

    move-object v1, v1

    move-object v1, v1

    check-cast v1, LX/Ia5;

    new-instance v2, LX/Ia7;

    invoke-direct {v2}, LX/Ia7;-><init>()V

    move-object v2, v2

    move-object v2, v2

    check-cast v2, LX/Ia7;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;-><init>(LX/18V;LX/Ia5;LX/Ia7;)V

    move-object v0, v3

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_72
    const-string v1, "com.facebook.dbllite.protocol.DblLiteQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->a(LX/0QB;)Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_73
    const-string v1, "com.facebook.messaging.service.multicache.SmsCachingServiceChain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JpL;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_74
    const-string v1, "com.facebook.config.background.impl.ConfigBackgroundQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {p0}, LX/28W;->a(LX/0QB;)LX/28W;

    move-result-object v4

    check-cast v4, LX/28W;

    invoke-static {p0}, LX/3g3;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v5

    new-instance v6, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance v8, LX/3g4;

    invoke-direct {v8, p0}, LX/3g4;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, v8}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct/range {v2 .. v8}, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;-><init>(LX/0aG;LX/28W;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    move-object v0, v2

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_75
    const-string v1, "com.facebook.messaging.media.upload.PhotoTranscodeQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FHD;->a(LX/0QB;)LX/FHD;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_76
    const-string v1, "com.facebook.api.negative_feedback.NegativeFeedbackMessageActionsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionHandler;

    const/16 v0, 0xb83

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    new-instance v0, LX/8wi;

    invoke-direct {v0}, LX/8wi;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/8wi;

    invoke-direct {v1, v2, v0}, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionHandler;-><init>(LX/0Or;LX/8wi;)V

    move-object v0, v1

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_77
    const-string v1, "com.facebook.confirmation.service.AccountConfirmationQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/Ej5;->a(LX/0QB;)LX/Ej5;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_78
    const-string v1, "com.facebook.messaging.media.upload.VideoTranscodeUploadQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/FIC;->a(LX/0QB;)LX/FIC;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_79
    const-string v1, "com.facebook.messaging.service.multicache.PushQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JpJ;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_7a
    const-string v1, "com.facebook.friendsnearby.server.FriendsNearbyMethodsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/IHu;->a(LX/0QB;)LX/IHu;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_7b
    const-string v1, "com.facebook.commerce.productdetails.api.ProductDetailsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/GW5;->a(LX/0QB;)LX/GW5;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_7c
    const-string v1, "com.facebook.messaging.service.multicache.LowPriorityThreadsQueue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/JpH;->a(LX/0QB;)LX/1qM;

    move-result-object v0

    check-cast v0, LX/1qM;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7eb8442d -> :sswitch_0
        -0x7e33c32f -> :sswitch_1
        -0x7b949b54 -> :sswitch_2
        -0x7b42988a -> :sswitch_3
        -0x7a93db0b -> :sswitch_4
        -0x7323f7ce -> :sswitch_5
        -0x714cbab7 -> :sswitch_6
        -0x714b1f47 -> :sswitch_7
        -0x70b207bd -> :sswitch_8
        -0x6f6b1c0b -> :sswitch_9
        -0x6b1cea2d -> :sswitch_a
        -0x657ed4f0 -> :sswitch_b
        -0x610716e0 -> :sswitch_c
        -0x5e45d09a -> :sswitch_d
        -0x5d47c7c3 -> :sswitch_e
        -0x59b90e71 -> :sswitch_f
        -0x58c328c8 -> :sswitch_10
        -0x588b5ff9 -> :sswitch_11
        -0x538a7b0f -> :sswitch_12
        -0x50cf32e3 -> :sswitch_13
        -0x4f6692a6 -> :sswitch_14
        -0x4f4da600 -> :sswitch_15
        -0x4b361e39 -> :sswitch_16
        -0x46d7d45b -> :sswitch_17
        -0x4637b8fe -> :sswitch_18
        -0x456f0410 -> :sswitch_19
        -0x42ea5d8f -> :sswitch_1a
        -0x3e87aa56 -> :sswitch_1b
        -0x3bc6361a -> :sswitch_1c
        -0x3a5a2dab -> :sswitch_1d
        -0x37b8bbe3 -> :sswitch_1e
        -0x366e91f4 -> :sswitch_1f
        -0x3570101b -> :sswitch_20
        -0x32c8a6bb -> :sswitch_21
        -0x322f8565 -> :sswitch_22
        -0x31ebb3b0 -> :sswitch_23
        -0x30a82d24 -> :sswitch_24
        -0x2ef7a2cb -> :sswitch_25
        -0x2e79faea -> :sswitch_26
        -0x2e158d53 -> :sswitch_27
        -0x2e0dd078 -> :sswitch_28
        -0x2c5fd564 -> :sswitch_29
        -0x298aa3e0 -> :sswitch_2a
        -0x25ee4027 -> :sswitch_2b
        -0x233a0113 -> :sswitch_2c
        -0x1b9da3cc -> :sswitch_2d
        -0x1b9bd965 -> :sswitch_2e
        -0x163e698d -> :sswitch_2f
        -0x158a4bdb -> :sswitch_30
        -0x118b32a9 -> :sswitch_31
        -0x10ec185b -> :sswitch_32
        -0x10442d61 -> :sswitch_33
        -0xc36cb71 -> :sswitch_34
        -0xb9836d1 -> :sswitch_35
        -0xa286cd8 -> :sswitch_36
        -0x6aacff8 -> :sswitch_37
        -0x5520fad -> :sswitch_38
        -0x1ef46bf -> :sswitch_39
        -0x13b7f4a -> :sswitch_3a
        -0x618937 -> :sswitch_3b
        0xf21f16 -> :sswitch_3c
        0x247b1c3 -> :sswitch_3d
        0x4199d72 -> :sswitch_3e
        0x6411817 -> :sswitch_3f
        0x6ce35ad -> :sswitch_40
        0x7b54a80 -> :sswitch_41
        0xcb83e25 -> :sswitch_42
        0xe358eea -> :sswitch_43
        0xec5ef5b -> :sswitch_44
        0x10263c9a -> :sswitch_45
        0x10a9df1b -> :sswitch_46
        0x141568f8 -> :sswitch_47
        0x158c371b -> :sswitch_48
        0x17d4c2af -> :sswitch_49
        0x18b9d0e5 -> :sswitch_4a
        0x18f7f0e1 -> :sswitch_4b
        0x1bb41fcf -> :sswitch_4c
        0x1d10bf75 -> :sswitch_4d
        0x1e41b90d -> :sswitch_4e
        0x21e60d9e -> :sswitch_4f
        0x21ffd030 -> :sswitch_50
        0x267b6a8d -> :sswitch_51
        0x27f9aefc -> :sswitch_52
        0x293ae63a -> :sswitch_53
        0x293b84d8 -> :sswitch_54
        0x2b81c364 -> :sswitch_55
        0x30269b95 -> :sswitch_56
        0x32df6a45 -> :sswitch_57
        0x33676cb7 -> :sswitch_58
        0x3395fbdf -> :sswitch_59
        0x34c26ade -> :sswitch_5a
        0x35b0fdd5 -> :sswitch_5b
        0x397e5281 -> :sswitch_5c
        0x3e80c641 -> :sswitch_5d
        0x3f00321b -> :sswitch_5e
        0x3f77fd51 -> :sswitch_5f
        0x4229c61a -> :sswitch_60
        0x42a47859 -> :sswitch_61
        0x43d1c4ea -> :sswitch_62
        0x4546732b -> :sswitch_63
        0x47b40871 -> :sswitch_64
        0x4b18b9ff -> :sswitch_65
        0x4d51e333 -> :sswitch_66
        0x4f25adc6 -> :sswitch_67
        0x4fa49fd8 -> :sswitch_68
        0x4fb07396 -> :sswitch_69
        0x52b1b050 -> :sswitch_6a
        0x5523f179 -> :sswitch_6b
        0x5cff7d2f -> :sswitch_6c
        0x61a5ae47 -> :sswitch_6d
        0x62c13ef8 -> :sswitch_6e
        0x6355c936 -> :sswitch_6f
        0x657fda1a -> :sswitch_70
        0x66bea0e2 -> :sswitch_71
        0x6b29857e -> :sswitch_72
        0x6c7630d9 -> :sswitch_73
        0x6d196c02 -> :sswitch_74
        0x6d6c4ef0 -> :sswitch_75
        0x728969d3 -> :sswitch_76
        0x73d750e6 -> :sswitch_77
        0x75f09df8 -> :sswitch_78
        0x7696f630 -> :sswitch_79
        0x7709f91d -> :sswitch_7a
        0x787b0927 -> :sswitch_7b
        0x789d3079 -> :sswitch_7c
    .end sparse-switch
.end method

.method public static hasBinding(LX/0QB;Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            "Ljava/lang/Class",
            "<",
            "LX/1qM;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const-string v2, "com.facebook.appirater.api.annotation.AppiraterQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v2, "com.facebook.photos.upload.service.UploadServiceQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_2
    const-string v2, "com.facebook.messaging.send.service.PendingSendQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_3
    const-string v2, "com.facebook.devicebasedlogin.protocol.DeviceBasedLoginQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_4
    const-string v2, "com.facebook.payments.history.protocol.PaymentHistoryProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_5
    const-string v2, "com.facebook.messaging.media.upload.udp.UDPServiceQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_6
    const-string v2, "com.facebook.feed.protocol.NewsFeedFetchQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_7
    const-string v2, "com.facebook.privacy.service.PrivacyDataQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_8
    const-string v2, "com.facebook.api.reportable_entity.ReportableEntityNegativeActionsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_9
    const-string v2, "com.facebook.search.service.GraphSearchDataQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_a
    const-string v2, "com.facebook.notifications.server.NotificationsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_b
    const-string v2, "com.facebook.timeline.services.ProfileActionQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_c
    const-string v2, "com.facebook.fbservice.service.AuthQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_d
    const-string v2, "com.facebook.adinterfaces.api.AdInterfacesMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :sswitch_e
    const-string v2, "com.facebook.stickers.service.StickersDownloadQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "com.facebook.messaging.send.service.SendQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_10
    const-string v2, "com.facebook.timeline.service.TimelineSectionQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "com.facebook.messaging.contacts.picker.service.ContactPickerNearbyQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_12
    const-string v2, "com.facebook.push.externalcloud.annotations.RegistrationQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_13
    const-string v2, "com.facebook.sideloading.SideloadingServiceQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_14
    const-string v2, "com.facebook.adspayments.protocol.PaymentsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_15
    const-string v2, "com.facebook.messaging.media.upload.PhotoUploadHiResParallelQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_16
    const-string v2, "com.facebook.messaging.media.upload.PhotoUploadHiResQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_17
    const-string v2, "com.facebook.interstitial.service.InterstitialQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_18
    const-string v2, "com.facebook.backgroundlocation.settings.write.BackgroundLocationSettingsWriteQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_19
    const-string v2, "com.facebook.pages.common.friendinviter.service.FriendInviterMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_1a
    const-string v2, "com.facebook.nux.service.NuxQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_1b
    const-string v2, "com.facebook.messaging.media.upload.MediaUploadQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_1c
    const-string v2, "com.facebook.friendsnearby.pingdialog.FriendsNearbyPingMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_1d
    const-string v2, "com.facebook.contactlogs.service.ContactLogsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_1e
    const-string v2, "com.facebook.messaging.tincan.messenger.annotations.TincanCachingServiceChain"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_1f
    const-string v2, "com.facebook.location.foreground.ForegroundLocationQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_20
    const-string v2, "com.facebook.stickers.service.StickersQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_21
    const-string v2, "com.facebook.growth.service.GrowthQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_22
    const-string v2, "com.facebook.events.annotation.EventMethodQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_23
    const-string v2, "com.facebook.captcha.annotations.CaptchaQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_24
    const-string v2, "com.facebook.messaging.media.download.MediaDownloadQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_25
    const-string v2, "com.facebook.zero.annotations.ZeroTokenQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_26
    const-string v2, "com.facebook.backgroundlocation.reporting.BackgroundLocationReportingQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_27
    const-string v2, "com.facebook.contacts.service.DynamicContactDataQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_28
    const-string v2, "com.facebook.photos.data.service.PhotosServiceQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_29
    const-string v2, "com.facebook.messaging.media.upload.PhotoUploadQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_2a
    const-string v2, "com.facebook.nearby.protocol.NearbyMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_2b
    const-string v2, "com.facebook.payments.paymentmethods.picker.protocol.PickerProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_2c
    const-string v2, "com.facebook.contacts.service.AddressBookQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_2d
    const-string v2, "com.facebook.feedplugins.storygallerysurvey.service.StoryGallerySurveyActionsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_2e
    const-string v2, "com.facebook.payments.invoice.protocol.InvoiceProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_2f
    const-string v2, "com.facebook.saved.server.SavedQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_30
    const-string v2, "com.facebook.platform.common.server.PlatformOperationQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_31
    const-string v2, "com.facebook.quickinvite.protocol.service.QuickInviteQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_32
    const-string v2, "com.facebook.payments.auth.pin.protocol.PaymentPinQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_33
    const-string v2, "com.facebook.payments.checkout.protocol.CheckoutProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_34
    const-string v2, "com.facebook.payments.shipping.protocol.ShippingAddressProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_35
    const-string v2, "com.facebook.vault.service.VaultQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_36
    const-string v2, "com.facebook.katana.activity.codegenerator.data.CodeGeneratorServiceQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_37
    const-string v2, "com.facebook.messaging.localfetch.LocalFetchQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_38
    const-string v2, "com.facebook.messaging.accountswitch.protocol.SwitchAccountsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_39
    const-string v2, "com.facebook.feed.protocol.NewsFeedPostingQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_3a
    const-string v2, "com.facebook.contacts.service.ContactsFetcherQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_3b
    const-string v2, "com.facebook.messaging.emoji.service.MessagingEmojiQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_3c
    const-string v2, "com.facebook.megaphone.api.MegaphoneQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_3d
    const-string v2, "com.facebook.messaging.sync.service.MessagesSyncQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_3e
    const-string v2, "com.facebook.share.protocol.ShareMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_3f
    const-string v2, "com.facebook.si.annotations.LinkshimQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_40
    const-string v2, "com.facebook.payments.paymentmethods.cardform.protocol.CardFormProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_41
    const-string v2, "com.facebook.languages.switcher.service.LanguageSwitcherQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_42
    const-string v2, "com.facebook.aldrin.service.AldrinQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_43
    const-string v2, "com.facebook.messaging.media.upload.MediaGetFbidQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_44
    const-string v2, "com.facebook.payments.contactinfo.protocol.ContactInfoProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_45
    const-string v2, "com.facebook.friends.service.FriendingQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_46
    const-string v2, "com.facebook.structuredsurvey.api.PostSurveyQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_47
    const-string v2, "com.facebook.places.pagetopics.FetchPageTopicsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_48
    const-string v2, "com.facebook.payments.p2p.protocol.PaymentQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_49
    const-string v2, "com.facebook.pages.adminedpages.service.AdminedPagesQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_4a
    const-string v2, "com.facebook.groups.service.GroupsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_4b
    const-string v2, "com.facebook.common.pagesprotocol.PagesReviewMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_4c
    const-string v2, "com.facebook.zero.upsell.annotations.UpsellPromoQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_4d
    const-string v2, "com.facebook.greetingcards.create.GreetingCardQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_4e
    const-string v2, "com.facebook.messaging.service.multicache.PushTraceQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_4f
    const-string v2, "com.facebook.messaging.service.multicache.FacebookCachingServiceChain"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_50
    const-string v2, "com.facebook.messaging.media.service.LocalMediaQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_51
    const-string v2, "com.facebook.messaging.tincan.messenger.TincanQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_52
    const-string v2, "com.facebook.api.negative_feedback.NegativeFeedbackActionsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_53
    const-string v2, "com.facebook.bookmark.client.BookmarkSyncQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_54
    const-string v2, "com.facebook.katana.server.AuthenticateCredentialsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_55
    const-string v2, "com.facebook.messaging.blocking.api.GetBlockedPeopleQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_56
    const-string v2, "com.facebook.pages.browser.data.service.PagesBrowserMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_57
    const-string v2, "com.facebook.contacts.service.ContactsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_58
    const-string v2, "com.facebook.facecast.protocol.FacecastServiceQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_59
    const-string v2, "com.facebook.account.recovery.common.protocol.AccountRecoveryQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_5a
    const-string v2, "com.facebook.registration.service.AccountRegistrationQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_5b
    const-string v2, "com.facebook.messaging.media.upload.PhotoUploadParallelQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_5c
    const-string v2, "com.facebook.katana.server.SimpleDataFetchQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_5d
    const-string v2, "com.facebook.payments.settings.protocol.PaymentSettingsProtocolQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_5e
    const-string v2, "com.facebook.feed.annotations.ForNewsfeed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_5f
    const-string v2, "com.facebook.uberbar.api.FetchUberbarResultQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_60
    const-string v2, "com.facebook.timeline.aboutpage.service.TimelineCollectionsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_61
    const-string v2, "com.facebook.auth.login.CheckApprovedMachineQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_62
    const-string v2, "com.facebook.messaging.payment.sync.service.PaymentsSyncQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_63
    const-string v2, "com.facebook.katana.server.LoginApprovalResendCodeQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_64
    const-string v2, "com.facebook.composer.system.savedsession.memsync.ComposerSavedSessionQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_65
    const-string v2, "com.facebook.search.service.GraphSearchLogQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_66
    const-string v2, "com.facebook.goodwill.publish.GoodwillPublishUploadQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_67
    const-string v2, "com.facebook.feed.protocol.NewsFeedMainQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_68
    const-string v2, "com.facebook.messaging.service.multicache.MultiCacheThreadsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_69
    const-string v2, "com.facebook.profile.inforequest.service.InfoRequestQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_6a
    const-string v2, "com.facebook.contacts.upload.ContactsUploadQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_6b
    const-string v2, "com.facebook.messaging.media.upload.VideoTranscodeQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_6c
    const-string v2, "com.facebook.pages.common.actionbar.blueservice.PagesCommonActionBarMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_6d
    const-string v2, "com.facebook.commerce.storefront.api.StoreFrontQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_6e
    const-string v2, "com.facebook.timeline.service.TimelineHeaderQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_6f
    const-string v2, "com.facebook.abtest.qe.service.module.QuickExperimentQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_70
    const-string v2, "com.facebook.feed.protocol.UFIQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_71
    const-string v2, "com.facebook.messaging.business.nativesignup.annotations.NativeSignUpQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_72
    const-string v2, "com.facebook.dbllite.protocol.DblLiteQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_73
    const-string v2, "com.facebook.messaging.service.multicache.SmsCachingServiceChain"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_74
    const-string v2, "com.facebook.config.background.impl.ConfigBackgroundQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_75
    const-string v2, "com.facebook.messaging.media.upload.PhotoTranscodeQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_76
    const-string v2, "com.facebook.api.negative_feedback.NegativeFeedbackMessageActionsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_77
    const-string v2, "com.facebook.confirmation.service.AccountConfirmationQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_78
    const-string v2, "com.facebook.messaging.media.upload.VideoTranscodeUploadQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_79
    const-string v2, "com.facebook.messaging.service.multicache.PushQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_7a
    const-string v2, "com.facebook.friendsnearby.server.FriendsNearbyMethodsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_7b
    const-string v2, "com.facebook.commerce.productdetails.api.ProductDetailsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_7c
    const-string v2, "com.facebook.messaging.service.multicache.LowPriorityThreadsQueue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7eb8442d -> :sswitch_0
        -0x7e33c32f -> :sswitch_1
        -0x7b949b54 -> :sswitch_2
        -0x7b42988a -> :sswitch_3
        -0x7a93db0b -> :sswitch_4
        -0x7323f7ce -> :sswitch_5
        -0x714cbab7 -> :sswitch_6
        -0x714b1f47 -> :sswitch_7
        -0x70b207bd -> :sswitch_8
        -0x6f6b1c0b -> :sswitch_9
        -0x6b1cea2d -> :sswitch_a
        -0x657ed4f0 -> :sswitch_b
        -0x610716e0 -> :sswitch_c
        -0x5e45d09a -> :sswitch_d
        -0x5d47c7c3 -> :sswitch_e
        -0x59b90e71 -> :sswitch_f
        -0x58c328c8 -> :sswitch_10
        -0x588b5ff9 -> :sswitch_11
        -0x538a7b0f -> :sswitch_12
        -0x50cf32e3 -> :sswitch_13
        -0x4f6692a6 -> :sswitch_14
        -0x4f4da600 -> :sswitch_15
        -0x4b361e39 -> :sswitch_16
        -0x46d7d45b -> :sswitch_17
        -0x4637b8fe -> :sswitch_18
        -0x456f0410 -> :sswitch_19
        -0x42ea5d8f -> :sswitch_1a
        -0x3e87aa56 -> :sswitch_1b
        -0x3bc6361a -> :sswitch_1c
        -0x3a5a2dab -> :sswitch_1d
        -0x37b8bbe3 -> :sswitch_1e
        -0x366e91f4 -> :sswitch_1f
        -0x3570101b -> :sswitch_20
        -0x32c8a6bb -> :sswitch_21
        -0x322f8565 -> :sswitch_22
        -0x31ebb3b0 -> :sswitch_23
        -0x30a82d24 -> :sswitch_24
        -0x2ef7a2cb -> :sswitch_25
        -0x2e79faea -> :sswitch_26
        -0x2e158d53 -> :sswitch_27
        -0x2e0dd078 -> :sswitch_28
        -0x2c5fd564 -> :sswitch_29
        -0x298aa3e0 -> :sswitch_2a
        -0x25ee4027 -> :sswitch_2b
        -0x233a0113 -> :sswitch_2c
        -0x1b9da3cc -> :sswitch_2d
        -0x1b9bd965 -> :sswitch_2e
        -0x163e698d -> :sswitch_2f
        -0x158a4bdb -> :sswitch_30
        -0x118b32a9 -> :sswitch_31
        -0x10ec185b -> :sswitch_32
        -0x10442d61 -> :sswitch_33
        -0xc36cb71 -> :sswitch_34
        -0xb9836d1 -> :sswitch_35
        -0xa286cd8 -> :sswitch_36
        -0x6aacff8 -> :sswitch_37
        -0x5520fad -> :sswitch_38
        -0x1ef46bf -> :sswitch_39
        -0x13b7f4a -> :sswitch_3a
        -0x618937 -> :sswitch_3b
        0xf21f16 -> :sswitch_3c
        0x247b1c3 -> :sswitch_3d
        0x4199d72 -> :sswitch_3e
        0x6411817 -> :sswitch_3f
        0x6ce35ad -> :sswitch_40
        0x7b54a80 -> :sswitch_41
        0xcb83e25 -> :sswitch_42
        0xe358eea -> :sswitch_43
        0xec5ef5b -> :sswitch_44
        0x10263c9a -> :sswitch_45
        0x10a9df1b -> :sswitch_46
        0x141568f8 -> :sswitch_47
        0x158c371b -> :sswitch_48
        0x17d4c2af -> :sswitch_49
        0x18b9d0e5 -> :sswitch_4a
        0x18f7f0e1 -> :sswitch_4b
        0x1bb41fcf -> :sswitch_4c
        0x1d10bf75 -> :sswitch_4d
        0x1e41b90d -> :sswitch_4e
        0x21e60d9e -> :sswitch_4f
        0x21ffd030 -> :sswitch_50
        0x267b6a8d -> :sswitch_51
        0x27f9aefc -> :sswitch_52
        0x293ae63a -> :sswitch_53
        0x293b84d8 -> :sswitch_54
        0x2b81c364 -> :sswitch_55
        0x30269b95 -> :sswitch_56
        0x32df6a45 -> :sswitch_57
        0x33676cb7 -> :sswitch_58
        0x3395fbdf -> :sswitch_59
        0x34c26ade -> :sswitch_5a
        0x35b0fdd5 -> :sswitch_5b
        0x397e5281 -> :sswitch_5c
        0x3e80c641 -> :sswitch_5d
        0x3f00321b -> :sswitch_5e
        0x3f77fd51 -> :sswitch_5f
        0x4229c61a -> :sswitch_60
        0x42a47859 -> :sswitch_61
        0x43d1c4ea -> :sswitch_62
        0x4546732b -> :sswitch_63
        0x47b40871 -> :sswitch_64
        0x4b18b9ff -> :sswitch_65
        0x4d51e333 -> :sswitch_66
        0x4f25adc6 -> :sswitch_67
        0x4fa49fd8 -> :sswitch_68
        0x4fb07396 -> :sswitch_69
        0x52b1b050 -> :sswitch_6a
        0x5523f179 -> :sswitch_6b
        0x5cff7d2f -> :sswitch_6c
        0x61a5ae47 -> :sswitch_6d
        0x62c13ef8 -> :sswitch_6e
        0x6355c936 -> :sswitch_6f
        0x657fda1a -> :sswitch_70
        0x66bea0e2 -> :sswitch_71
        0x6b29857e -> :sswitch_72
        0x6c7630d9 -> :sswitch_73
        0x6d196c02 -> :sswitch_74
        0x6d6c4ef0 -> :sswitch_75
        0x728969d3 -> :sswitch_76
        0x73d750e6 -> :sswitch_77
        0x75f09df8 -> :sswitch_78
        0x7696f630 -> :sswitch_79
        0x7709f91d -> :sswitch_7a
        0x787b0927 -> :sswitch_7b
        0x789d3079 -> :sswitch_7c
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 330147
    iget-object v0, p0, LX/1qC;->mScopeUnawareInjector:LX/0QB;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1qC;->mAnnotation:Ljava/lang/Class;

    invoke-static {v0, v1, v2}, LX/1qC;->getInstance(LX/0QB;Ljava/lang/Class;Ljava/lang/Class;)LX/1qM;

    move-result-object v0

    move-object v0, v0

    .line 330148
    return-object v0
.end method
