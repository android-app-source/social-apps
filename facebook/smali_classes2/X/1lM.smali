.class public final LX/1lM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:Ljava/nio/ByteBuffer;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0w5;

.field public final g:I

.field public final h:[B

.field public final i:[B

.field public final j:[B

.field public final k:LX/0ta;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JJJLjava/nio/ByteBuffer;[B[BLjava/util/Set;LX/0w5;I[BLX/0ta;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p8    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ",
            "Ljava/nio/ByteBuffer;",
            "[B[B",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0w5;",
            "I[B",
            "LX/0ta;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 311479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311480
    iput-wide p1, p0, LX/1lM;->a:J

    .line 311481
    iput-wide p3, p0, LX/1lM;->b:J

    .line 311482
    iput-wide p5, p0, LX/1lM;->c:J

    .line 311483
    iput-object p7, p0, LX/1lM;->d:Ljava/nio/ByteBuffer;

    .line 311484
    iput-object p8, p0, LX/1lM;->i:[B

    .line 311485
    iput-object p9, p0, LX/1lM;->j:[B

    .line 311486
    iput-object p10, p0, LX/1lM;->e:Ljava/util/Set;

    .line 311487
    iput-object p11, p0, LX/1lM;->f:LX/0w5;

    .line 311488
    iput p12, p0, LX/1lM;->g:I

    .line 311489
    iput-object p13, p0, LX/1lM;->h:[B

    .line 311490
    iput-object p14, p0, LX/1lM;->k:LX/0ta;

    .line 311491
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1lM;->l:Ljava/lang/String;

    .line 311492
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1lM;->m:Ljava/util/Map;

    .line 311493
    return-void
.end method
