.class public LX/0XH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;LX/0WS;)LX/03R;
    .locals 1

    .prologue
    .line 77718
    invoke-virtual {p1, p0}, LX/0WS;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77719
    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/03R;->YES:LX/03R;

    .line 77720
    :goto_0
    return-object v0

    .line 77721
    :cond_0
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 77722
    :cond_1
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public static a(LX/0XG;LX/0WS;)Lcom/facebook/user/model/User;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 77830
    const-string v1, "uid"

    invoke-virtual {p1, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77831
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77832
    :goto_0
    return-object v0

    .line 77833
    :cond_0
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    .line 77834
    invoke-virtual {v2, p0, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 77835
    new-instance v1, Lcom/facebook/user/model/Name;

    const-string v3, "first_name"

    invoke-virtual {p1, v3, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "last_name"

    invoke-virtual {p1, v4, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "name"

    invoke-virtual {p1, v5, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77836
    iput-object v1, v2, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 77837
    const-string v1, "birth_date_year"

    invoke-virtual {p1, v1, v6}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v1

    const-string v3, "birth_date_month"

    invoke-virtual {p1, v3, v6}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "birth_date_day"

    invoke-virtual {p1, v4, v6}, LX/0WS;->a(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, LX/0XI;->a(III)LX/0XI;

    .line 77838
    const-string v1, "gender"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77839
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77840
    sget-object v1, LX/0XJ;->UNKNOWN:LX/0XJ;

    .line 77841
    :goto_1
    move-object v1, v1

    .line 77842
    iput-object v1, v2, LX/0XI;->m:LX/0XJ;

    .line 77843
    const/4 v1, 0x0

    .line 77844
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 77845
    const-string v4, "emails"

    invoke-virtual {p1, v4, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    .line 77846
    if-nez v4, :cond_2

    .line 77847
    :goto_2
    move-object v1, v1

    .line 77848
    iput-object v1, v2, LX/0XI;->c:Ljava/util/List;

    .line 77849
    const-string v1, "phones"

    invoke-virtual {p1, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77850
    iput-object v1, v2, LX/0XI;->f:Ljava/lang/String;

    .line 77851
    const-string v1, "pic_square"

    invoke-virtual {p1, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77852
    iput-object v1, v2, LX/0XI;->n:Ljava/lang/String;

    .line 77853
    const-string v1, "profile_pic_square"

    invoke-virtual {p1, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77854
    iput-object v1, v2, LX/0XI;->q:Ljava/lang/String;

    .line 77855
    const-string v1, "pic_cover"

    invoke-virtual {p1, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77856
    iput-object v1, v2, LX/0XI;->o:Ljava/lang/String;

    .line 77857
    const-string v1, "rank"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, LX/0WS;->a(Ljava/lang/String;F)F

    move-result v1

    .line 77858
    iput v1, v2, LX/0XI;->t:F

    .line 77859
    const-string v1, "is_pushable"

    invoke-static {v1, p1}, LX/0XH;->a(Ljava/lang/String;LX/0WS;)LX/03R;

    move-result-object v1

    .line 77860
    iput-object v1, v2, LX/0XI;->u:LX/03R;

    .line 77861
    const-string v1, "is_employee"

    invoke-virtual {p1, v1, v6}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 77862
    iput-boolean v1, v2, LX/0XI;->v:Z

    .line 77863
    const-string v1, "is_work_user"

    invoke-virtual {p1, v1, v6}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 77864
    iput-boolean v1, v2, LX/0XI;->w:Z

    .line 77865
    const-string v1, "type"

    invoke-virtual {p1, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77866
    iput-object v0, v2, LX/0XI;->y:Ljava/lang/String;

    .line 77867
    const-string v0, "is_partial"

    invoke-virtual {p1, v0, v6}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 77868
    iput-boolean v0, v2, LX/0XI;->M:Z

    .line 77869
    const-string v0, "is_minor"

    invoke-virtual {p1, v0, v6}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 77870
    iput-boolean v0, v2, LX/0XI;->N:Z

    .line 77871
    const-string v0, "profile_picture_is_silhouette"

    invoke-static {v0, p1}, LX/0XH;->a(Ljava/lang/String;LX/0WS;)LX/03R;

    move-result-object v0

    .line 77872
    iput-object v0, v2, LX/0XI;->O:LX/03R;

    .line 77873
    const-string v0, "montage_thread_fbid"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v0, v4, v5}, LX/0WS;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 77874
    iput-wide v0, v2, LX/0XI;->X:J

    .line 77875
    const-string v0, "can_see_viewer_montage_thread"

    invoke-virtual {p1, v0, v6}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 77876
    iput-boolean v0, v2, LX/0XI;->Y:Z

    .line 77877
    const-string v0, "is_deactivated_allowed_on_messenger"

    invoke-virtual {p1, v0, v6}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 77878
    iput-boolean v0, v2, LX/0XI;->S:Z

    .line 77879
    const-string v0, "is_messenger_only_deactivated"

    invoke-virtual {p1, v0, v6}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 77880
    iput-boolean v0, v2, LX/0XI;->af:Z

    .line 77881
    const/4 v0, 0x0

    .line 77882
    const-string v1, "messenger_montage_audience_mode"

    invoke-virtual {p1, v1, v0}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77883
    :try_start_0
    invoke-static {v1}, LX/0XN;->valueOf(Ljava/lang/String;)LX/0XN;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 77884
    :goto_3
    move-object v0, v0

    .line 77885
    iput-object v0, v2, LX/0XI;->ah:LX/0XN;

    .line 77886
    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    goto/16 :goto_0

    .line 77887
    :cond_1
    :try_start_1
    invoke-static {v1}, LX/0XJ;->valueOf(Ljava/lang/String;)LX/0XJ;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto/16 :goto_1

    .line 77888
    :catch_0
    sget-object v1, LX/0XJ;->UNKNOWN:LX/0XJ;

    goto/16 :goto_1

    .line 77889
    :cond_2
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 77890
    new-instance v5, Lcom/facebook/user/model/UserEmailAddress;

    const/4 p0, 0x0

    invoke-direct {v5, v1, p0}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 77891
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_2

    :catch_1
    goto :goto_3
.end method

.method public static a(Lcom/facebook/user/model/User;LX/1gW;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 77726
    iget-object v0, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v0

    .line 77727
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    :goto_0
    const-string v4, "No ID in logged-in user"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 77728
    const-string v0, "uid"

    invoke-interface {p1, v0, v3}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 77729
    iget-object v0, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 77730
    if-nez v0, :cond_10

    .line 77731
    :goto_1
    const-string v0, "birth_date_year"

    .line 77732
    iget v3, p0, Lcom/facebook/user/model/User;->C:I

    move v3, v3

    .line 77733
    invoke-interface {p1, v0, v3}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    .line 77734
    const-string v0, "birth_date_month"

    .line 77735
    iget v3, p0, Lcom/facebook/user/model/User;->D:I

    move v3, v3

    .line 77736
    invoke-interface {p1, v0, v3}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    .line 77737
    const-string v0, "birth_date_day"

    .line 77738
    iget v3, p0, Lcom/facebook/user/model/User;->E:I

    move v3, v3

    .line 77739
    invoke-interface {p1, v0, v3}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    .line 77740
    iget-object v0, p0, Lcom/facebook/user/model/User;->h:LX/0XJ;

    move-object v0, v0

    .line 77741
    if-eqz v0, :cond_0

    .line 77742
    const-string v3, "gender"

    invoke-virtual {v0}, LX/0XJ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v0}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 77743
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/User;->c:LX/0Px;

    move-object v0, v0

    .line 77744
    if-nez v0, :cond_11

    .line 77745
    :cond_1
    :goto_2
    const-string v0, "phones"

    .line 77746
    iget-object v3, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 77747
    invoke-static {p0}, Lcom/facebook/user/model/User;->az(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    .line 77748
    :cond_2
    iget-object v3, p0, Lcom/facebook/user/model/User;->ak:Ljava/lang/String;

    move-object v3, v3

    .line 77749
    invoke-static {v0, v3, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 77750
    const-string v0, "pic_square"

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 77751
    const-string v0, "profile_pic_square"

    .line 77752
    iget-object v3, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 77753
    const/4 v3, 0x0

    .line 77754
    iget-object v4, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    if-nez v4, :cond_14

    .line 77755
    :cond_3
    :goto_3
    move-object v3, v3

    .line 77756
    iput-object v3, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    .line 77757
    :cond_4
    iget-object v3, p0, Lcom/facebook/user/model/User;->am:Ljava/lang/String;

    move-object v3, v3

    .line 77758
    invoke-static {v0, v3, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 77759
    const-string v0, "pic_cover"

    .line 77760
    iget-object v3, p0, Lcom/facebook/user/model/User;->j:Ljava/lang/String;

    move-object v3, v3

    .line 77761
    invoke-static {v0, v3, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 77762
    const-string v0, "rank"

    .line 77763
    iget v3, p0, Lcom/facebook/user/model/User;->m:F

    move v3, v3

    .line 77764
    invoke-interface {p1, v0, v3}, LX/1gW;->a(Ljava/lang/String;F)LX/1gW;

    .line 77765
    iget-object v0, p0, Lcom/facebook/user/model/User;->n:LX/03R;

    move-object v0, v0

    .line 77766
    sget-object v3, LX/03R;->UNSET:LX/03R;

    if-eq v0, v3, :cond_5

    .line 77767
    const-string v3, "is_pushable"

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    invoke-interface {p1, v3, v0}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77768
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->o:Z

    move v0, v0

    .line 77769
    if-eqz v0, :cond_6

    .line 77770
    const-string v0, "is_employee"

    invoke-interface {p1, v0, v1}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77771
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->p:Z

    move v0, v0

    .line 77772
    if-eqz v0, :cond_7

    .line 77773
    const-string v0, "is_work_user"

    invoke-interface {p1, v0, v1}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77774
    :cond_7
    const-string v0, "type"

    .line 77775
    iget-object v3, p0, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    move-object v3, v3

    .line 77776
    invoke-static {v0, v3, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 77777
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->F:Z

    move v0, v0

    .line 77778
    if-eqz v0, :cond_8

    .line 77779
    const-string v0, "is_partial"

    invoke-interface {p1, v0, v1}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77780
    :cond_8
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->G:Z

    move v0, v0

    .line 77781
    if-eqz v0, :cond_9

    .line 77782
    const-string v0, "is_minor"

    invoke-interface {p1, v0, v1}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77783
    :cond_9
    iget-object v0, p0, Lcom/facebook/user/model/User;->H:LX/03R;

    move-object v0, v0

    .line 77784
    sget-object v3, LX/03R;->UNSET:LX/03R;

    if-eq v0, v3, :cond_a

    .line 77785
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    invoke-interface {p1, v3, v0}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77786
    :cond_a
    const-string v0, "montage_thread_fbid"

    .line 77787
    iget-wide v5, p0, Lcom/facebook/user/model/User;->N:J

    move-wide v2, v5

    .line 77788
    invoke-interface {p1, v0, v2, v3}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    .line 77789
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->O:Z

    move v0, v0

    .line 77790
    if-eqz v0, :cond_b

    .line 77791
    const-string v0, "can_see_viewer_montage_thread"

    invoke-interface {p1, v0, v1}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77792
    :cond_b
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->P:Z

    move v0, v0

    .line 77793
    if-eqz v0, :cond_c

    .line 77794
    const-string v0, "is_deactivated_allowed_on_messenger"

    invoke-interface {p1, v0, v1}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77795
    :cond_c
    iget-boolean v0, p0, Lcom/facebook/user/model/User;->aa:Z

    move v0, v0

    .line 77796
    if-eqz v0, :cond_d

    .line 77797
    const-string v0, "is_messenger_only_deactivated"

    invoke-interface {p1, v0, v1}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 77798
    :cond_d
    iget-object v0, p0, Lcom/facebook/user/model/User;->ac:LX/0XN;

    move-object v0, v0

    .line 77799
    if-eqz v0, :cond_e

    .line 77800
    const-string v1, "messenger_montage_audience_mode"

    invoke-virtual {v0}, LX/0XN;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 77801
    :cond_e
    return-void

    :cond_f
    move v0, v2

    .line 77802
    goto/16 :goto_0

    .line 77803
    :cond_10
    const-string v3, "first_name"

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 77804
    const-string v3, "last_name"

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    .line 77805
    const-string v3, "name"

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p1}, LX/0XH;->a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V

    goto/16 :goto_1

    .line 77806
    :cond_11
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    .line 77807
    if-lez v5, :cond_1

    .line 77808
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6, v5}, Ljava/util/HashSet;-><init>(I)V

    .line 77809
    const/4 v3, 0x0

    move v4, v3

    :goto_4
    if-ge v4, v5, :cond_13

    .line 77810
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/UserEmailAddress;

    .line 77811
    if-eqz v3, :cond_12

    .line 77812
    iget-object v7, v3, Lcom/facebook/user/model/UserEmailAddress;->a:Ljava/lang/String;

    move-object v3, v7

    .line 77813
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_12

    .line 77814
    invoke-interface {v6, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77815
    :cond_12
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    .line 77816
    :cond_13
    const-string v3, "emails"

    invoke-interface {p1, v3, v6}, LX/1gW;->a(Ljava/lang/String;Ljava/util/Set;)LX/1gW;

    goto/16 :goto_2

    .line 77817
    :cond_14
    iget-object v4, p0, Lcom/facebook/user/model/User;->al:Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v4}, Lcom/facebook/user/model/PicSquare;->a()LX/0Px;

    move-result-object v5

    .line 77818
    if-eqz v5, :cond_3

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 77819
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 77820
    const/4 v3, 0x0

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_5
    if-ge v4, v7, :cond_15

    .line 77821
    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/PicSquareUrlWithSize;

    .line 77822
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 77823
    const-string v9, "profile_pic_size"

    iget v10, v3, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 77824
    const-string v9, "profile_pic_url"

    iget-object v3, v3, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-virtual {v8, v9, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 77825
    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77826
    :goto_6
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    .line 77827
    :catch_0
    move-exception v3

    .line 77828
    const-string v8, "User"

    const-string v9, "Profile square pic serialization"

    invoke-static {v8, v9, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 77829
    :cond_15
    invoke-virtual {v6}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/1gW;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77723
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77724
    :goto_0
    return-void

    .line 77725
    :cond_0
    invoke-interface {p2, p0, p1}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    goto :goto_0
.end method
