.class public final LX/1kC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "LX/1RN;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;


# direct methods
.method public constructor <init>(Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;)V
    .locals 0

    .prologue
    .line 309109
    iput-object p1, p0, LX/1kC;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 309110
    iget-object v0, p0, LX/1kC;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->d:LX/1QZ;

    iget-object v0, v0, LX/1QZ;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x930001

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 309111
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 309112
    check-cast p1, LX/0Px;

    .line 309113
    iget-object v0, p0, LX/1kC;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->d:LX/1QZ;

    iget-object v0, v0, LX/1QZ;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x930001

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 309114
    iget-object v0, p0, LX/1kC;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->d:LX/1QZ;

    iget-object v1, p0, LX/1kC;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v1, v1, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->d:LX/1QZ;

    iget-object v1, v1, LX/1QZ;->A:LX/1EG;

    iget-object v2, p0, LX/1kC;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-boolean v2, v2, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->a:Z

    iget-object v3, p0, LX/1kC;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-boolean v3, v3, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->c:Z

    invoke-virtual {v0, p1, v1, v2, v3}, LX/1QZ;->a(LX/0Px;LX/1EG;ZZ)V

    .line 309115
    return-void
.end method
