.class public LX/1cn;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field public A:LX/1cw;

.field public B:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;"
        }
    .end annotation
.end field

.field public D:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;"
        }
    .end annotation
.end field

.field public F:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;"
        }
    .end annotation
.end field

.field public G:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;"
        }
    .end annotation
.end field

.field public I:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;"
        }
    .end annotation
.end field

.field public final a:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cu;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1cu;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/CharSequence;

.field public j:Ljava/lang/Object;

.field public k:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Z

.field private final o:LX/1co;

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1cn;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/1cp;

.field public r:[I

.field public s:Z

.field public t:J

.field public u:Z

.field private v:LX/1cq;

.field private w:Z

.field public x:LX/1dM;

.field public y:LX/1dR;

.field public z:LX/1dd;


# direct methods
.method public constructor <init>(LX/1De;)V
    .locals 1

    .prologue
    .line 282964
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1cn;-><init>(LX/1De;Landroid/util/AttributeSet;)V

    .line 282965
    return-void
.end method

.method public constructor <init>(LX/1De;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 282966
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 282967
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/1cn;->a:LX/0YU;

    .line 282968
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/1cn;->c:LX/0YU;

    .line 282969
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/1cn;->e:LX/0YU;

    .line 282970
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/1cn;->g:LX/0YU;

    .line 282971
    new-instance v0, LX/1co;

    invoke-direct {v0, p0}, LX/1co;-><init>(LX/1cn;)V

    iput-object v0, p0, LX/1cn;->o:LX/1co;

    .line 282972
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1cn;->p:Ljava/util/List;

    .line 282973
    new-array v0, v2, [I

    iput-object v0, p0, LX/1cn;->r:[I

    .line 282974
    iput-boolean v2, p0, LX/1cn;->w:Z

    .line 282975
    invoke-virtual {p0, v2}, LX/1cn;->setWillNotDraw(Z)V

    .line 282976
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1cn;->setChildrenDrawingOrderEnabled(Z)V

    .line 282977
    iget-object v0, p1, LX/1De;->d:LX/1cp;

    move-object v0, v0

    .line 282978
    iput-object v0, p0, LX/1cn;->q:LX/1cp;

    .line 282979
    new-instance v0, LX/1cq;

    invoke-direct {v0, p0}, LX/1cq;-><init>(LX/1cn;)V

    iput-object v0, p0, LX/1cn;->v:LX/1cq;

    .line 282980
    invoke-static {p1}, LX/1cs;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/1cn;->b(Z)V

    .line 282981
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 282982
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1cn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 282983
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 282984
    new-instance v0, LX/1De;

    invoke-direct {v0, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2}, LX/1cn;-><init>(LX/1De;Landroid/util/AttributeSet;)V

    .line 282985
    return-void
.end method

.method public static a(LX/1cn;ILandroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 282986
    iget-object v0, p0, LX/1cn;->f:LX/0YU;

    invoke-static {p1, v0}, LX/1ct;->a(ILX/0YU;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282987
    iget-object v0, p0, LX/1cn;->f:LX/0YU;

    .line 282988
    invoke-virtual {v0, p1}, LX/0YU;->b(I)V

    .line 282989
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 282990
    instance-of v0, p2, LX/1cu;

    if-eqz v0, :cond_0

    .line 282991
    iget-object v0, p0, LX/1cn;->h:LX/0YU;

    invoke-static {p1, v0}, LX/1ct;->a(ILX/0YU;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 282992
    iget-object v0, p0, LX/1cn;->h:LX/0YU;

    .line 282993
    invoke-virtual {v0, p1}, LX/0YU;->b(I)V

    .line 282994
    :cond_0
    :goto_1
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1cn;->invalidate(Landroid/graphics/Rect;)V

    .line 282995
    invoke-static {p0}, LX/1cn;->h(LX/1cn;)V

    .line 282996
    return-void

    .line 282997
    :cond_1
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    .line 282998
    invoke-virtual {v0, p1}, LX/0YU;->b(I)V

    .line 282999
    goto :goto_0

    .line 283000
    :cond_2
    iget-object v0, p0, LX/1cn;->g:LX/0YU;

    .line 283001
    invoke-virtual {v0, p1}, LX/0YU;->b(I)V

    .line 283002
    goto :goto_1
.end method

.method private static b(LX/1cn;ILX/1cv;)V
    .locals 5

    .prologue
    .line 283003
    invoke-virtual {p2}, LX/1cv;->t()Landroid/graphics/Rect;

    move-result-object v1

    .line 283004
    if-nez v1, :cond_0

    .line 283005
    :goto_0
    return-void

    .line 283006
    :cond_0
    iget-object v0, p0, LX/1cn;->A:LX/1cw;

    if-nez v0, :cond_1

    .line 283007
    new-instance v0, LX/1cw;

    invoke-direct {v0, p0}, LX/1cw;-><init>(LX/1cn;)V

    iput-object v0, p0, LX/1cn;->A:LX/1cw;

    .line 283008
    iget-object v0, p0, LX/1cn;->A:LX/1cw;

    invoke-virtual {p0, v0}, LX/1cn;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 283009
    :cond_1
    iget-object v2, p0, LX/1cn;->A:LX/1cw;

    .line 283010
    iget-object v0, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 283011
    check-cast v0, Landroid/view/View;

    .line 283012
    iget-object v3, v2, LX/1cw;->c:LX/0YU;

    .line 283013
    sget-object v4, LX/1cx;->a:LX/0Zj;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1cx;

    .line 283014
    if-nez v4, :cond_2

    .line 283015
    new-instance v4, LX/1cx;

    invoke-direct {v4}, LX/1cx;-><init>()V

    .line 283016
    :cond_2
    iput-object v0, v4, LX/1cx;->b:Landroid/view/View;

    .line 283017
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p0

    iput p0, v4, LX/1cx;->d:I

    .line 283018
    iget-object p0, v4, LX/1cx;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 283019
    iget-object p0, v4, LX/1cx;->f:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 283020
    iget-object p0, v4, LX/1cx;->f:Landroid/graphics/Rect;

    iget p2, v4, LX/1cx;->d:I

    neg-int p2, p2

    iget v2, v4, LX/1cx;->d:I

    neg-int v2, v2

    invoke-virtual {p0, p2, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 283021
    move-object v4, v4

    .line 283022
    invoke-virtual {v3, p1, v4}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 283023
    goto :goto_0
.end method

.method public static b(LX/1cn;LX/1cv;II)V
    .locals 1

    .prologue
    .line 283024
    invoke-virtual {p1}, LX/1cv;->t()Landroid/graphics/Rect;

    move-result-object v0

    .line 283025
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1cn;->A:LX/1cw;

    if-nez v0, :cond_1

    .line 283026
    :cond_0
    :goto_0
    return-void

    .line 283027
    :cond_1
    iget-object v0, p0, LX/1cn;->A:LX/1cw;

    .line 283028
    iget-object p0, v0, LX/1cw;->c:LX/0YU;

    invoke-virtual {p0, p3}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 283029
    iget-object p0, v0, LX/1cw;->d:LX/0YU;

    if-nez p0, :cond_3

    .line 283030
    sget-object p0, LX/1cw;->b:LX/0Zj;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0YU;

    .line 283031
    if-nez p0, :cond_2

    .line 283032
    new-instance p0, LX/0YU;

    const/4 p1, 0x4

    invoke-direct {p0, p1}, LX/0YU;-><init>(I)V

    .line 283033
    :cond_2
    move-object p0, p0

    .line 283034
    iput-object p0, v0, LX/1cw;->d:LX/0YU;

    .line 283035
    :cond_3
    iget-object p0, v0, LX/1cw;->c:LX/0YU;

    iget-object p1, v0, LX/1cw;->d:LX/0YU;

    invoke-static {p3, p0, p1}, LX/1ct;->a(ILX/0YU;LX/0YU;)V

    .line 283036
    :cond_4
    iget-object p0, v0, LX/1cw;->c:LX/0YU;

    iget-object p1, v0, LX/1cw;->d:LX/0YU;

    invoke-static {p2, p3, p0, p1}, LX/1ct;->a(IILX/0YU;LX/0YU;)V

    .line 283037
    iget-object p0, v0, LX/1cw;->d:LX/0YU;

    if-eqz p0, :cond_5

    iget-object p0, v0, LX/1cw;->d:LX/0YU;

    invoke-virtual {p0}, LX/0YU;->a()I

    move-result p0

    if-nez p0, :cond_5

    .line 283038
    iget-object p0, v0, LX/1cw;->d:LX/0YU;

    .line 283039
    sget-object p1, LX/1cw;->b:LX/0Zj;

    invoke-virtual {p1, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283040
    const/4 p0, 0x0

    iput-object p0, v0, LX/1cw;->d:LX/0YU;

    .line 283041
    :cond_5
    goto :goto_0
.end method

.method public static b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 283042
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 283043
    invoke-virtual {p0}, Landroid/view/View;->cancelPendingInputEvents()V

    .line 283044
    :cond_0
    sget-object v0, LX/0vv;->a:LX/0w2;

    invoke-interface {v0, p0}, LX/0w2;->p(Landroid/view/View;)V

    .line 283045
    return-void
.end method

.method public static c(LX/1cn;ILX/1cv;)V
    .locals 2

    .prologue
    .line 283046
    iget-object v0, p0, LX/1cn;->A:LX/1cw;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1cv;->t()Landroid/graphics/Rect;

    move-result-object v0

    if-nez v0, :cond_1

    .line 283047
    :cond_0
    :goto_0
    return-void

    .line 283048
    :cond_1
    iget-object v0, p0, LX/1cn;->A:LX/1cw;

    .line 283049
    iget-object v1, v0, LX/1cw;->d:LX/0YU;

    if-eqz v1, :cond_3

    .line 283050
    iget-object v1, v0, LX/1cw;->d:LX/0YU;

    invoke-virtual {v1, p1}, LX/0YU;->g(I)I

    move-result p0

    .line 283051
    if-ltz p0, :cond_3

    .line 283052
    iget-object v1, v0, LX/1cw;->d:LX/0YU;

    invoke-virtual {v1, p0}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1cx;

    .line 283053
    iget-object p2, v0, LX/1cw;->d:LX/0YU;

    invoke-virtual {p2, p0}, LX/0YU;->d(I)V

    .line 283054
    invoke-virtual {v1}, LX/1cx;->a()V

    .line 283055
    const/4 v1, 0x1

    .line 283056
    :goto_1
    move v1, v1

    .line 283057
    if-eqz v1, :cond_2

    .line 283058
    :goto_2
    goto :goto_0

    .line 283059
    :cond_2
    iget-object v1, v0, LX/1cw;->c:LX/0YU;

    invoke-virtual {v1, p1}, LX/0YU;->g(I)I

    move-result p0

    .line 283060
    iget-object v1, v0, LX/1cw;->c:LX/0YU;

    invoke-virtual {v1, p0}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1cx;

    .line 283061
    iget-object p2, v0, LX/1cw;->c:LX/0YU;

    invoke-virtual {p2, p0}, LX/0YU;->d(I)V

    .line 283062
    invoke-virtual {v1}, LX/1cx;->a()V

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static h(LX/1cn;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 283063
    iget-object v0, p0, LX/1cn;->b:LX/0YU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1cn;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 283064
    iget-object v0, p0, LX/1cn;->b:LX/0YU;

    invoke-static {v0}, LX/1cy;->a(LX/0YU;)V

    .line 283065
    iput-object v1, p0, LX/1cn;->b:LX/0YU;

    .line 283066
    :cond_0
    iget-object v0, p0, LX/1cn;->d:LX/0YU;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1cn;->d:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 283067
    iget-object v0, p0, LX/1cn;->d:LX/0YU;

    invoke-static {v0}, LX/1cy;->a(LX/0YU;)V

    .line 283068
    iput-object v1, p0, LX/1cn;->d:LX/0YU;

    .line 283069
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)LX/1cv;
    .locals 1

    .prologue
    .line 283070
    iget-object v0, p0, LX/1cn;->a:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    return-object v0
.end method

.method public final a(ILX/1cv;Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 283071
    iget-object v0, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 283072
    instance-of v1, v0, Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 283073
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0, p1, p2}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 283074
    iget-object v0, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 283075
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 283076
    iget-object v1, p2, LX/1cv;->t:LX/1cz;

    move-object v1, v1

    .line 283077
    if-eqz v1, :cond_2

    .line 283078
    iget-object v1, p2, LX/1cv;->t:LX/1cz;

    move-object v1, v1

    .line 283079
    iget v2, p2, LX/1cv;->u:I

    move v2, v2

    .line 283080
    invoke-static {p0, v1, p3, v2}, LX/1ct;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;I)V

    .line 283081
    :goto_0
    instance-of v1, v0, LX/1cu;

    if-eqz v1, :cond_0

    .line 283082
    iget-object v1, p0, LX/1cn;->g:LX/0YU;

    check-cast v0, LX/1cu;

    invoke-virtual {v1, p1, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 283083
    :cond_0
    :goto_1
    iget-object v0, p0, LX/1cn;->a:LX/0YU;

    invoke-virtual {v0, p1, p2}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 283084
    invoke-static {p2}, LX/1ct;->a(LX/1cv;)V

    .line 283085
    return-void

    .line 283086
    :cond_1
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 283087
    iget-object v1, p0, LX/1cn;->c:LX/0YU;

    invoke-virtual {v1, p1, p2}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 283088
    check-cast v0, Landroid/view/View;

    .line 283089
    iget v1, p2, LX/1cv;->u:I

    move v1, v1

    .line 283090
    const/4 p3, 0x1

    const/4 v3, -0x1

    .line 283091
    invoke-static {v1}, LX/1cv;->d(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setDuplicateParentStateEnabled(Z)V

    .line 283092
    iput-boolean p3, p0, LX/1cn;->s:Z

    .line 283093
    instance-of v2, v0, LX/1cn;

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_3

    .line 283094
    invoke-static {v0}, LX/0vv;->q(Landroid/view/View;)V

    .line 283095
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 283096
    :goto_2
    invoke-static {p0, p1, p2}, LX/1cn;->b(LX/1cn;ILX/1cv;)V

    goto :goto_1

    .line 283097
    :cond_2
    iget v1, p2, LX/1cv;->u:I

    move v1, v1

    .line 283098
    invoke-static {p0, v0, p3, v1}, LX/1ct;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;I)V

    goto :goto_0

    .line 283099
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 283100
    if-nez v2, :cond_4

    .line 283101
    invoke-virtual {p0}, LX/1cn;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 283102
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 283103
    :cond_4
    iget-boolean v2, p0, LX/1cn;->u:Z

    if-eqz v2, :cond_5

    .line 283104
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v3, v2, p3}, LX/1cn;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_2

    .line 283105
    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v3, v2}, LX/1cn;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 283106
    iget-boolean v0, p0, LX/1cn;->n:Z

    if-ne v0, p1, :cond_1

    .line 283107
    :cond_0
    :goto_0
    return-void

    .line 283108
    :cond_1
    iput-boolean p1, p0, LX/1cn;->n:Z

    .line 283109
    iget-boolean v0, p0, LX/1cn;->n:Z

    if-nez v0, :cond_0

    .line 283110
    iget-boolean v0, p0, LX/1cn;->l:Z

    if-eqz v0, :cond_2

    .line 283111
    invoke-virtual {p0}, LX/1cn;->invalidate()V

    .line 283112
    iput-boolean v1, p0, LX/1cn;->l:Z

    .line 283113
    :cond_2
    iget-boolean v0, p0, LX/1cn;->m:Z

    if-eqz v0, :cond_0

    .line 283114
    invoke-virtual {p0}, LX/1cn;->b()V

    .line 283115
    iput-boolean v1, p0, LX/1cn;->m:Z

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 283116
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 283117
    iget-boolean v0, p0, LX/1cn;->w:Z

    if-nez v0, :cond_0

    .line 283118
    :goto_0
    return-void

    .line 283119
    :cond_0
    iget-boolean v0, p0, LX/1cn;->n:Z

    if-eqz v0, :cond_1

    .line 283120
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1cn;->m:Z

    goto :goto_0

    .line 283121
    :cond_1
    iget-object v0, p0, LX/1cn;->v:LX/1cq;

    invoke-virtual {v0}, LX/1cq;->b()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 283122
    iget-boolean v0, p0, LX/1cn;->w:Z

    if-ne p1, v0, :cond_1

    .line 283123
    :cond_0
    return-void

    .line 283124
    :cond_1
    if-eqz p1, :cond_3

    .line 283125
    iget-object v0, p0, LX/1cn;->v:LX/1cq;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 283126
    :goto_0
    iput-boolean p1, p0, LX/1cn;->w:Z

    .line 283127
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/1cn;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 283128
    invoke-virtual {p0, v1}, LX/1cn;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 283129
    instance-of v3, v0, LX/1cn;

    if-eqz v3, :cond_2

    .line 283130
    check-cast v0, LX/1cn;

    invoke-virtual {v0, p1}, LX/1cn;->b(Z)V

    .line 283131
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 283132
    :cond_3
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    goto :goto_0
.end method

.method public cq_()V
    .locals 0

    .prologue
    .line 283133
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 283134
    iget-boolean v0, p0, LX/1cn;->u:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 283135
    iget-object v0, p0, LX/1cn;->o:LX/1co;

    .line 283136
    iput-object p1, v0, LX/1co;->b:Landroid/graphics/Canvas;

    .line 283137
    const/4 v1, 0x0

    iput v1, v0, LX/1co;->c:I

    .line 283138
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 283139
    iget-object v0, p0, LX/1cn;->o:LX/1co;

    invoke-static {v0}, LX/1co;->a$redex0(LX/1co;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283140
    iget-object v0, p0, LX/1cn;->o:LX/1co;

    invoke-static {v0}, LX/1co;->b$redex0(LX/1co;)V

    .line 283141
    :cond_0
    iget-object v0, p0, LX/1cn;->o:LX/1co;

    .line 283142
    const/4 v1, 0x0

    iput-object v1, v0, LX/1co;->b:Landroid/graphics/Canvas;

    .line 283143
    sget-boolean v1, LX/1V5;->b:Z

    if-eqz v1, :cond_6

    .line 283144
    const/4 v3, 0x0

    .line 283145
    sget-object v2, LX/1d0;->a:Landroid/graphics/Paint;

    if-nez v2, :cond_1

    .line 283146
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 283147
    sput-object v2, LX/1d0;->a:Landroid/graphics/Paint;

    const v4, 0x66c29bff

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 283148
    :cond_1
    sget-object v2, LX/1d0;->b:Landroid/graphics/Paint;

    if-nez v2, :cond_2

    .line 283149
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 283150
    sput-object v2, LX/1d0;->b:Landroid/graphics/Paint;

    const v4, 0x44d3ffce

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 283151
    :cond_2
    invoke-static {p0}, LX/1d0;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 283152
    invoke-virtual {p0}, LX/1cn;->getWidth()I

    move-result v2

    int-to-float v5, v2

    invoke-virtual {p0}, LX/1cn;->getHeight()I

    move-result v2

    int-to-float v6, v2

    sget-object v7, LX/1d0;->a:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 283153
    :cond_3
    invoke-virtual {p0}, LX/1cn;->getMountItemCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v8, v2

    :goto_0
    if-ltz v8, :cond_5

    .line 283154
    invoke-virtual {p0, v8}, LX/1cn;->a(I)LX/1cv;

    move-result-object v2

    .line 283155
    iget-object v3, v2, LX/1cv;->a:LX/1X1;

    move-object v3, v3

    .line 283156
    invoke-static {v3}, LX/1X1;->d(LX/1X1;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v3}, LX/1X1;->b(LX/1X1;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 283157
    iget-object v3, v2, LX/1cv;->b:Ljava/lang/Object;

    move-object v2, v3

    .line 283158
    check-cast v2, Landroid/view/View;

    .line 283159
    invoke-static {v2}, LX/1d0;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 283160
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v6, v2

    sget-object v7, LX/1d0;->b:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 283161
    :cond_4
    add-int/lit8 v2, v8, -0x1

    move v8, v2

    goto :goto_0

    .line 283162
    :cond_5
    iget-object v2, p0, LX/1cn;->A:LX/1cw;

    move-object v2, v2

    .line 283163
    if-eqz v2, :cond_6

    .line 283164
    sget-object v3, LX/1d0;->b:Landroid/graphics/Paint;

    .line 283165
    iget-object v4, v2, LX/1cw;->c:LX/0YU;

    invoke-virtual {v4}, LX/0YU;->a()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move v5, v4

    :goto_1
    if-ltz v5, :cond_6

    .line 283166
    iget-object v4, v2, LX/1cw;->c:LX/0YU;

    invoke-virtual {v4, v5}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1cx;

    iget-object v4, v4, LX/1cx;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v4, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 283167
    add-int/lit8 v4, v5, -0x1

    move v5, v4

    goto :goto_1

    .line 283168
    :cond_6
    sget-boolean v1, LX/1V5;->c:Z

    if-eqz v1, :cond_7

    .line 283169
    invoke-static {p0, p1}, LX/1d0;->c(LX/1cn;Landroid/graphics/Canvas;)V

    .line 283170
    :cond_7
    return-void
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 282954
    iget-object v0, p0, LX/1cn;->v:LX/1cq;

    invoke-virtual {v0, p1}, LX/1cq;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final drawableStateChanged()V
    .locals 5

    .prologue
    .line 282955
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 282956
    const/4 v0, 0x0

    iget-object v1, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 282957
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 282958
    iget-object v1, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 282959
    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 282960
    iget v4, v0, LX/1cv;->u:I

    move v0, v4

    .line 282961
    invoke-static {p0, v1, v0}, LX/1ct;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;I)V

    .line 282962
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 282963
    :cond_0
    return-void
.end method

.method public final getChildDrawingOrder(II)I
    .locals 6

    .prologue
    .line 282880
    const/4 v2, 0x0

    .line 282881
    iget-boolean v0, p0, LX/1cn;->s:Z

    if-nez v0, :cond_1

    .line 282882
    :goto_0
    iget-object v0, p0, LX/1cn;->o:LX/1co;

    invoke-static {v0}, LX/1co;->a$redex0(LX/1co;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282883
    iget-object v0, p0, LX/1cn;->o:LX/1co;

    invoke-static {v0}, LX/1co;->b$redex0(LX/1co;)V

    .line 282884
    :cond_0
    iget-object v0, p0, LX/1cn;->r:[I

    aget v0, v0, p2

    return v0

    .line 282885
    :cond_1
    invoke-virtual {p0}, LX/1cn;->getChildCount()I

    move-result v0

    .line 282886
    iget-object v1, p0, LX/1cn;->r:[I

    array-length v1, v1

    if-ge v1, v0, :cond_2

    .line 282887
    add-int/lit8 v0, v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, LX/1cn;->r:[I

    .line 282888
    :cond_2
    iget-object v0, p0, LX/1cn;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v3

    move v1, v2

    .line 282889
    :goto_1
    if-ge v1, v3, :cond_3

    .line 282890
    iget-object v0, p0, LX/1cn;->c:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 282891
    iget-object v4, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v4

    .line 282892
    check-cast v0, Landroid/view/View;

    .line 282893
    iget-object v4, p0, LX/1cn;->r:[I

    invoke-virtual {p0, v0}, LX/1cn;->indexOfChild(Landroid/view/View;)I

    move-result v0

    aput v0, v4, v1

    .line 282894
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 282895
    :cond_3
    iget-object v0, p0, LX/1cn;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_4

    .line 282896
    iget-object v0, p0, LX/1cn;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 282897
    iget-object v5, p0, LX/1cn;->r:[I

    add-int p1, v1, v3

    invoke-virtual {p0, v0}, LX/1cn;->indexOfChild(Landroid/view/View;)I

    move-result v0

    aput v0, v5, p1

    .line 282898
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 282899
    :cond_4
    iput-boolean v2, p0, LX/1cn;->s:Z

    goto :goto_0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 282879
    iget-object v0, p0, LX/1cn;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getContentDescriptions()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282868
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 282869
    const/4 v0, 0x0

    iget-object v1, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 282870
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 282871
    iget-object v4, v0, LX/1cv;->d:Ljava/lang/CharSequence;

    move-object v0, v4

    .line 282872
    if-eqz v0, :cond_0

    .line 282873
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282874
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 282875
    :cond_1
    invoke-virtual {p0}, LX/1cn;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 282876
    if-eqz v0, :cond_2

    .line 282877
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282878
    :cond_2
    return-object v2
.end method

.method public getImageContent()LX/1d1;
    .locals 4

    .prologue
    .line 282856
    iget-object v0, p0, LX/1cn;->a:LX/0YU;

    invoke-static {v0}, LX/1ct;->a(LX/0YU;)Ljava/util/List;

    move-result-object v0

    .line 282857
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 282858
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 282859
    instance-of v2, v1, LX/1d1;

    if-eqz v2, :cond_0

    check-cast v1, LX/1d1;

    .line 282860
    :goto_0
    move-object v0, v1

    .line 282861
    return-object v0

    .line 282862
    :cond_0
    sget-object v1, LX/1d1;->b:LX/1d1;

    goto :goto_0

    .line 282863
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 282864
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 282865
    instance-of p0, v1, LX/1d1;

    if-eqz p0, :cond_2

    .line 282866
    check-cast v1, LX/1d1;

    invoke-interface {v1}, LX/1d1;->bH_()Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 282867
    :cond_3
    new-instance v1, LX/1d2;

    invoke-direct {v1, v2}, LX/1d2;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method

.method public getMountItemCount()I
    .locals 1

    .prologue
    .line 282855
    iget-object v0, p0, LX/1cn;->a:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    return v0
.end method

.method public getParentHostMarker()J
    .locals 2

    .prologue
    .line 282854
    iget-wide v0, p0, LX/1cn;->t:J

    return-wide v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 282851
    iget-object v0, p0, LX/1cn;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 282852
    iget-object v0, p0, LX/1cn;->j:Ljava/lang/Object;

    .line 282853
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getTag(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 282847
    iget-object v0, p0, LX/1cn;->k:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 282848
    iget-object v0, p0, LX/1cn;->k:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 282849
    if-eqz v0, :cond_0

    .line 282850
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextContent()LX/1d3;
    .locals 4

    .prologue
    .line 282835
    iget-object v0, p0, LX/1cn;->a:LX/0YU;

    invoke-static {v0}, LX/1ct;->a(LX/0YU;)Ljava/util/List;

    move-result-object v0

    .line 282836
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 282837
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 282838
    instance-of v2, v1, LX/1d3;

    if-eqz v2, :cond_0

    check-cast v1, LX/1d3;

    .line 282839
    :goto_0
    move-object v0, v1

    .line 282840
    return-object v0

    .line 282841
    :cond_0
    sget-object v1, LX/1d3;->b:LX/1d3;

    goto :goto_0

    .line 282842
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 282843
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 282844
    instance-of p0, v1, LX/1d3;

    if-eqz p0, :cond_2

    .line 282845
    check-cast v1, LX/1d3;

    invoke-interface {v1}, LX/1d3;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 282846
    :cond_3
    new-instance v1, LX/1d4;

    invoke-direct {v1, v2}, LX/1d4;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 282831
    iget-boolean v0, p0, LX/1cn;->n:Z

    if-eqz v0, :cond_0

    .line 282832
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1cn;->l:Z

    .line 282833
    :goto_0
    return-void

    .line 282834
    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->invalidate()V

    goto :goto_0
.end method

.method public final invalidate(IIII)V
    .locals 1

    .prologue
    .line 282900
    iget-boolean v0, p0, LX/1cn;->n:Z

    if-eqz v0, :cond_0

    .line 282901
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1cn;->l:Z

    .line 282902
    :goto_0
    return-void

    .line 282903
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->invalidate(IIII)V

    goto :goto_0
.end method

.method public final invalidate(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 282904
    iget-boolean v0, p0, LX/1cn;->n:Z

    if-eqz v0, :cond_0

    .line 282905
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1cn;->l:Z

    .line 282906
    :goto_0
    return-void

    .line 282907
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 4

    .prologue
    .line 282908
    invoke-super {p0}, Landroid/view/ViewGroup;->jumpDrawablesToCurrentState()V

    .line 282909
    const/4 v0, 0x0

    iget-object v1, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 282910
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 282911
    iget-object v3, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v3

    .line 282912
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 282913
    invoke-static {v0}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;)V

    .line 282914
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 282915
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 282916
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1cn;->u:Z

    .line 282917
    invoke-virtual {p0}, LX/1cn;->cq_()V

    .line 282918
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1cn;->u:Z

    .line 282919
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    const v2, -0x645b376e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 282920
    const/4 v2, 0x0

    .line 282921
    iget-object v0, p0, LX/1cn;->g:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_2

    .line 282922
    iget-object v0, p0, LX/1cn;->g:LX/0YU;

    invoke-virtual {v0, v3}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cu;

    .line 282923
    invoke-interface {v0, p1}, LX/1cu;->a(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0, p1, p0}, LX/1cu;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 282924
    :goto_1
    if-nez v0, :cond_0

    .line 282925
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 282926
    :cond_0
    const v1, 0x110975cb

    invoke-static {v1, v4}, LX/02F;->a(II)V

    return v0

    .line 282927
    :cond_1
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final requestLayout()V
    .locals 2

    .prologue
    .line 282928
    move-object v1, p0

    .line 282929
    :goto_0
    instance-of v0, v1, LX/1cn;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 282930
    check-cast v0, LX/1cn;

    .line 282931
    invoke-virtual {v0}, LX/1cn;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282932
    :goto_1
    return-void

    .line 282933
    :cond_0
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    .line 282934
    :cond_1
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    goto :goto_1
.end method

.method public setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V
    .locals 1

    .prologue
    .line 282935
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 282936
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1cn;->w:Z

    .line 282937
    return-void
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 282938
    iput-object p1, p0, LX/1cn;->i:Ljava/lang/CharSequence;

    .line 282939
    invoke-virtual {p0}, LX/1cn;->b()V

    .line 282940
    return-void
.end method

.method public setImportantForAccessibility(I)V
    .locals 1

    .prologue
    .line 282941
    iget-boolean v0, p0, LX/1cn;->w:Z

    if-eqz v0, :cond_0

    .line 282942
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    .line 282943
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 282945
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 282946
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    .line 282947
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0, v3}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 282948
    iget-object v1, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 282949
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 282950
    if-nez p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 282951
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 282952
    goto :goto_1

    .line 282953
    :cond_1
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 282944
    const/4 v0, 0x1

    return v0
.end method
