.class public LX/0he;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile p:LX/0he;


# instance fields
.field public b:Z

.field private c:I

.field public d:J

.field private e:I

.field private f:Z

.field public g:LX/7gV;

.field public final h:LX/7gT;

.field public i:LX/7gU;

.field public j:LX/0hf;

.field public k:I

.field public l:I

.field public m:I

.field public n:Z

.field public o:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118035
    const-class v0, LX/0he;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0he;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7gT;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 118031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118032
    sget-object v0, LX/7gU;->UNKNOWN:LX/7gU;

    iput-object v0, p0, LX/0he;->i:LX/7gU;

    .line 118033
    iput-object p1, p0, LX/0he;->h:LX/7gT;

    .line 118034
    return-void
.end method

.method public static a(LX/0QB;)LX/0he;
    .locals 4

    .prologue
    .line 118018
    sget-object v0, LX/0he;->p:LX/0he;

    if-nez v0, :cond_1

    .line 118019
    const-class v1, LX/0he;

    monitor-enter v1

    .line 118020
    :try_start_0
    sget-object v0, LX/0he;->p:LX/0he;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 118021
    if-eqz v2, :cond_0

    .line 118022
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 118023
    new-instance p0, LX/0he;

    invoke-static {v0}, LX/7gT;->a(LX/0QB;)LX/7gT;

    move-result-object v3

    check-cast v3, LX/7gT;

    invoke-direct {p0, v3}, LX/0he;-><init>(LX/7gT;)V

    .line 118024
    move-object v0, p0

    .line 118025
    sput-object v0, LX/0he;->p:LX/0he;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118026
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 118027
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 118028
    :cond_1
    sget-object v0, LX/0he;->p:LX/0he;

    return-object v0

    .line 118029
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 118030
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 118016
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0he;->o:Ljava/lang/String;

    .line 118017
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 117960
    iput p1, p0, LX/0he;->k:I

    .line 117961
    iget-object v0, p0, LX/0he;->h:LX/7gT;

    .line 117962
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 117963
    sget-object p0, LX/7gS;->BADGING_NUMBER:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117964
    sget-object p0, LX/7gR;->DIRECT_DIVEBAR_BADGE:LX/7gR;

    invoke-static {v0, p0, v1}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 117965
    return-void
.end method

.method public final a(Ljava/lang/String;IZIJ)V
    .locals 9

    .prologue
    .line 118001
    iput-boolean p3, p0, LX/0he;->b:Z

    .line 118002
    iput-wide p5, p0, LX/0he;->d:J

    .line 118003
    sget-object v0, LX/0hf;->ROW:LX/0hf;

    iput-object v0, p0, LX/0he;->j:LX/0hf;

    .line 118004
    sget-object v0, LX/7gU;->CLICK:LX/7gU;

    iput-object v0, p0, LX/0he;->i:LX/7gU;

    .line 118005
    sget-object v0, LX/7gV;->DIRECT:LX/7gV;

    iput-object v0, p0, LX/0he;->g:LX/7gV;

    .line 118006
    iget-object v1, p0, LX/0he;->h:LX/7gT;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    .line 118007
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118008
    sget-object v8, LX/7gS;->TARGET_ID:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118009
    sget-object v8, LX/7gS;->ROW_INDEX:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118010
    sget-object v8, LX/7gS;->IS_REPLAY:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 118011
    sget-object v8, LX/7gS;->THREAD_COUNT:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118012
    sget-object v8, LX/7gS;->SESSION_TIME:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 118013
    sget-object v8, LX/7gR;->OPEN_ROW:LX/7gR;

    invoke-static {v1, v8, v0}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 118014
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 118015
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 117983
    iget-object v0, p0, LX/0he;->g:LX/7gV;

    sget-object v1, LX/7gV;->DIRECT:LX/7gV;

    if-eq v0, v1, :cond_1

    .line 117984
    :cond_0
    :goto_0
    return-void

    .line 117985
    :cond_1
    iget-boolean v0, p0, LX/0he;->f:Z

    if-eqz v0, :cond_0

    .line 117986
    iput-boolean v2, p0, LX/0he;->f:Z

    .line 117987
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, LX/0he;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, LX/0he;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/0he;->i:LX/7gU;

    invoke-virtual {v2}, LX/7gU;->getGesture()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/0he;->j:LX/0hf;

    invoke-virtual {v2}, LX/0hf;->getSource()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, LX/0he;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-wide v2, p0, LX/0he;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 117988
    iget-object v1, p0, LX/0he;->h:LX/7gT;

    iget v4, p0, LX/0he;->e:I

    iget v6, p0, LX/0he;->c:I

    iget-object v7, p0, LX/0he;->i:LX/7gU;

    iget-object v8, p0, LX/0he;->j:LX/0hf;

    iget-boolean v9, p0, LX/0he;->b:Z

    iget-wide v10, p0, LX/0he;->d:J

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    .line 117989
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 117990
    sget-object p1, LX/7gS;->TARGET_ID:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117991
    sget-object p1, LX/7gS;->THREAD_INDEX:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117992
    sget-object p1, LX/7gS;->THREAD_ID:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117993
    sget-object p1, LX/7gS;->REPLY_COUNT:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117994
    sget-object p1, LX/7gS;->THREAD_COUNT:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117995
    sget-object p1, LX/7gS;->GESTURE:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7}, LX/7gU;->getGesture()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117996
    sget-object p1, LX/7gS;->SOURCE:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8}, LX/0hf;->getSource()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117997
    sget-object p1, LX/7gS;->IS_REPLAY:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117998
    sget-object p1, LX/7gS;->SESSION_TIME:LX/7gS;

    invoke-virtual {p1}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 117999
    sget-object p1, LX/7gR;->OPEN_THREAD:LX/7gR;

    invoke-static {v1, p1, v0}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 118000
    sget-object v0, LX/7gU;->UNKNOWN:LX/7gU;

    iput-object v0, p0, LX/0he;->i:LX/7gU;

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 117981
    const/4 v0, 0x0

    iput-object v0, p0, LX/0he;->o:Ljava/lang/String;

    .line 117982
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10

    .prologue
    .line 117970
    iget-object v1, p0, LX/0he;->h:LX/7gT;

    iget v4, p0, LX/0he;->e:I

    iget v6, p0, LX/0he;->c:I

    iget-boolean v7, p0, LX/0he;->b:Z

    iget-wide v8, p0, LX/0he;->d:J

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    .line 117971
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 117972
    sget-object p0, LX/7gS;->TARGET_ID:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117973
    sget-object p0, LX/7gS;->THREAD_ID:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117974
    sget-object p0, LX/7gS;->THREAD_INDEX:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117975
    sget-object p0, LX/7gS;->REPLY_COUNT:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117976
    sget-object p0, LX/7gS;->THREAD_COUNT:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117977
    sget-object p0, LX/7gS;->SESSION_TIME:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 117978
    sget-object p0, LX/7gS;->IS_REPLAY:LX/7gS;

    invoke-virtual {p0}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117979
    sget-object p0, LX/7gR;->SEND_REPLY:LX/7gR;

    invoke-static {v1, p0, v0}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 117980
    return-void
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 117966
    iput p1, p0, LX/0he;->c:I

    .line 117967
    iput p2, p0, LX/0he;->e:I

    .line 117968
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0he;->f:Z

    .line 117969
    return-void
.end method
