.class public LX/1Sw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/rows/core/HasSize;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1R7;

.field public final c:LX/1R7;

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 249278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249279
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Sw;->a:Ljava/util/List;

    .line 249280
    const/16 v0, 0x32

    invoke-static {v0}, LX/1R7;->a(I)LX/1R7;

    move-result-object v0

    iput-object v0, p0, LX/1Sw;->b:LX/1R7;

    .line 249281
    const/16 v0, 0x64

    invoke-static {v0}, LX/1R7;->a(I)LX/1R7;

    move-result-object v0

    iput-object v0, p0, LX/1Sw;->c:LX/1R7;

    .line 249282
    const/4 v0, 0x0

    iput v0, p0, LX/1Sw;->d:I

    .line 249283
    return-void
.end method

.method private static a(ILX/1R7;)V
    .locals 2

    .prologue
    .line 249327
    if-ltz p0, :cond_0

    .line 249328
    iget v0, p1, LX/1R7;->b:I

    move v0, v0

    .line 249329
    if-lt p0, v0, :cond_1

    .line 249330
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "rowIndex "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " would be out of bounds of collection of size "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 249331
    iget v1, p1, LX/1R7;->b:I

    move v1, v1

    .line 249332
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 249333
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 249334
    :cond_1
    return-void
.end method

.method public static c(LX/1Sw;)V
    .locals 1

    .prologue
    .line 249335
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    invoke-virtual {v0}, LX/1R7;->c()V

    .line 249336
    iget-object v0, p0, LX/1Sw;->c:LX/1R7;

    invoke-virtual {v0}, LX/1R7;->c()V

    .line 249337
    const/4 v0, 0x0

    iput v0, p0, LX/1Sw;->d:I

    .line 249338
    return-void
.end method

.method public static d(LX/1Sw;)I
    .locals 2

    .prologue
    .line 249308
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    .line 249309
    iget v1, v0, LX/1R7;->b:I

    move v0, v1

    .line 249310
    if-nez v0, :cond_0

    .line 249311
    const/4 v0, -0x1

    .line 249312
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/1Sw;->b:LX/1R7;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LX/1R7;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public static h(LX/1Sw;I)V
    .locals 4

    .prologue
    .line 249313
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    .line 249314
    iget v1, v0, LX/1R7;->b:I

    move v0, v1

    .line 249315
    if-nez v0, :cond_0

    .line 249316
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1R7;->b(I)V

    .line 249317
    :cond_0
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    .line 249318
    iget v1, v0, LX/1R7;->b:I

    move v0, v1

    .line 249319
    add-int/lit8 v0, v0, -0x1

    .line 249320
    if-lt v0, p1, :cond_2

    .line 249321
    :cond_1
    return-void

    .line 249322
    :cond_2
    iget-object v1, p0, LX/1Sw;->b:LX/1R7;

    invoke-virtual {v1, v0}, LX/1R7;->c(I)I

    move-result v1

    .line 249323
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    :goto_0
    if-gt v1, p1, :cond_1

    .line 249324
    iget-object v0, p0, LX/1Sw;->a:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v0

    add-int/2addr v2, v0

    .line 249325
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    invoke-virtual {v0, v2}, LX/1R7;->b(I)V

    .line 249326
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 4

    .prologue
    .line 249294
    iget-object v0, p0, LX/1Sw;->c:LX/1R7;

    .line 249295
    iget v1, v0, LX/1R7;->b:I

    move v0, v1

    .line 249296
    add-int/lit8 v0, v0, -0x1

    .line 249297
    if-gt p1, v0, :cond_2

    .line 249298
    :cond_0
    iget-object v0, p0, LX/1Sw;->c:LX/1R7;

    invoke-virtual {v0, p1}, LX/1R7;->c(I)I

    move-result v0

    return v0

    .line 249299
    :cond_1
    iget v0, p0, LX/1Sw;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1Sw;->d:I

    .line 249300
    :cond_2
    iget v0, p0, LX/1Sw;->d:I

    iget-object v1, p0, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/1Sw;->c:LX/1R7;

    .line 249301
    iget v1, v0, LX/1R7;->b:I

    move v0, v1

    .line 249302
    if-gt v0, p1, :cond_0

    .line 249303
    iget-object v0, p0, LX/1Sw;->a:Ljava/util/List;

    iget v1, p0, LX/1Sw;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 249304
    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v1

    .line 249305
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 249306
    iget-object v2, p0, LX/1Sw;->c:LX/1R7;

    iget v3, p0, LX/1Sw;->d:I

    invoke-virtual {v2, v3}, LX/1R7;->b(I)V

    .line 249307
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 249289
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 249290
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 249291
    :cond_1
    invoke-static {p0, p1}, LX/1Sw;->h(LX/1Sw;I)V

    .line 249292
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    invoke-static {p1, v0}, LX/1Sw;->a(ILX/1R7;)V

    .line 249293
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    invoke-virtual {v0, p1}, LX/1R7;->c(I)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 2

    .prologue
    .line 249284
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/1Sw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 249285
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 249286
    :cond_1
    add-int/lit8 v0, p1, 0x1

    invoke-static {p0, v0}, LX/1Sw;->h(LX/1Sw;I)V

    .line 249287
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    invoke-static {p1, v0}, LX/1Sw;->a(ILX/1R7;)V

    .line 249288
    iget-object v0, p0, LX/1Sw;->b:LX/1R7;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, LX/1R7;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method
