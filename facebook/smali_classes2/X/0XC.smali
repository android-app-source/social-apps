.class public LX/0XC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sl;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/0XC;


# instance fields
.field private final b:Ljava/lang/Object;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0So;

.field private final e:LX/0W3;

.field private final f:LX/0Or;
    .annotation runtime Lcom/facebook/backgroundworklog/performanceobserver/IsBackgroundWorkPerformanceMetricsEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0XB;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private volatile h:D

.field private volatile i:J

.field private volatile j:LX/03R;

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "LX/0Yj;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77680
    const-class v0, LX/0XC;

    sput-object v0, LX/0XC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0So;LX/0W3;LX/0Or;)V
    .locals 2
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/backgroundworklog/performanceobserver/IsBackgroundWorkPerformanceMetricsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;",
            "LX/0So;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 77670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77671
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0XC;->b:Ljava/lang/Object;

    .line 77672
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/0XC;->h:D

    .line 77673
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0XC;->i:J

    .line 77674
    iput-object p1, p0, LX/0XC;->c:LX/0Ot;

    .line 77675
    iput-object p2, p0, LX/0XC;->d:LX/0So;

    .line 77676
    iput-object p3, p0, LX/0XC;->e:LX/0W3;

    .line 77677
    iput-object p4, p0, LX/0XC;->f:LX/0Or;

    .line 77678
    const/4 v0, 0x0

    iput-object v0, p0, LX/0XC;->j:LX/03R;

    .line 77679
    return-void
.end method

.method public static a(LX/0QB;)LX/0XC;
    .locals 7

    .prologue
    .line 77657
    sget-object v0, LX/0XC;->m:LX/0XC;

    if-nez v0, :cond_1

    .line 77658
    const-class v1, LX/0XC;

    monitor-enter v1

    .line 77659
    :try_start_0
    sget-object v0, LX/0XC;->m:LX/0XC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 77660
    if-eqz v2, :cond_0

    .line 77661
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 77662
    new-instance v5, LX/0XC;

    const/16 v3, 0xf12

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    const/16 p0, 0x301

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v6, v3, v4, p0}, LX/0XC;-><init>(LX/0Ot;LX/0So;LX/0W3;LX/0Or;)V

    .line 77663
    move-object v0, v5

    .line 77664
    sput-object v0, LX/0XC;->m:LX/0XC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77665
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 77666
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 77667
    :cond_1
    sget-object v0, LX/0XC;->m:LX/0XC;

    return-object v0

    .line 77668
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 77669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77507
    if-nez p0, :cond_1

    .line 77508
    const-string v0, "NO_INTENT"

    .line 77509
    :cond_0
    :goto_0
    return-object v0

    .line 77510
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77511
    if-nez v0, :cond_0

    .line 77512
    const-string v0, "NO_ACTION"

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 77651
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77652
    monitor-enter p0

    .line 77653
    :try_start_0
    iget-object v1, p0, LX/0XC;->k:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 77654
    const/16 v1, 0x7c

    invoke-static {v1}, LX/0PO;->on(C)LX/0PO;

    move-result-object v1

    invoke-virtual {v1}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v1

    iget-object v2, p0, LX/0XC;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, LX/0PO;->appendTo(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 77655
    :cond_0
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 77656
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 8

    .prologue
    .line 77638
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ThreadId#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 77639
    iget-object v1, p0, LX/0XC;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    .line 77640
    new-instance v1, LX/0Yj;

    invoke-direct {v1, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 77641
    iput-object v0, v1, LX/0Yj;->e:Ljava/lang/String;

    .line 77642
    move-object v0, v1

    .line 77643
    const-string v1, "CauseInfo"

    invoke-direct {p0, p3}, LX/0XC;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    invoke-direct {p0}, LX/0XC;->c()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/0Yj;->a(D)LX/0Yj;

    move-result-object v1

    .line 77644
    new-instance v2, LX/0Yj;

    invoke-direct {v2, v1}, LX/0Yj;-><init>(LX/0Yj;)V

    .line 77645
    iget-object v0, p0, LX/0XC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    .line 77646
    iput-wide p4, v1, LX/0Yj;->g:J

    .line 77647
    move-object v1, v1

    .line 77648
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 77649
    iget-object v0, p0, LX/0XC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 77650
    return-void
.end method

.method private a(J)Z
    .locals 5

    .prologue
    .line 77636
    invoke-direct {p0}, LX/0XC;->d()J

    move-result-wide v0

    .line 77637
    iget-object v2, p0, LX/0XC;->d:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v2, p1

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)Z
    .locals 2

    .prologue
    .line 77629
    invoke-direct {p0, p1}, LX/0XC;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77630
    iget-object v1, p0, LX/0XC;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 77631
    :try_start_0
    invoke-direct {p0, p1}, LX/0XC;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77632
    iget-object v0, p0, LX/0XC;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, LX/0XC;->j:LX/03R;

    .line 77633
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77634
    :cond_1
    iget-object v0, p0, LX/0XC;->j:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0

    .line 77635
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b()LX/0XB;
    .locals 1

    .prologue
    .line 77621
    monitor-enter p0

    .line 77622
    :try_start_0
    iget-object v0, p0, LX/0XC;->g:Ljava/lang/ref/WeakReference;

    .line 77623
    monitor-exit p0

    .line 77624
    if-nez v0, :cond_0

    .line 77625
    const/4 v0, 0x0

    .line 77626
    :goto_0
    return-object v0

    .line 77627
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 77628
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0XB;

    goto :goto_0
.end method

.method private b(Z)Z
    .locals 2

    .prologue
    .line 77620
    iget-object v0, p0, LX/0XC;->j:LX/03R;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, LX/0XC;->j:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()D
    .locals 6

    .prologue
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    .line 77612
    iget-wide v0, p0, LX/0XC;->h:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_1

    .line 77613
    iget-object v1, p0, LX/0XC;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 77614
    :try_start_0
    iget-wide v2, p0, LX/0XC;->h:D

    cmpl-double v0, v2, v4

    if-nez v0, :cond_0

    .line 77615
    iget-object v0, p0, LX/0XC;->e:LX/0W3;

    sget-wide v2, LX/0X5;->ax:J

    const/16 v4, 0x64

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v0

    .line 77616
    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    int-to-double v4, v0

    mul-double/2addr v2, v4

    iput-wide v2, p0, LX/0XC;->h:D

    .line 77617
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77618
    :cond_1
    iget-wide v0, p0, LX/0XC;->h:D

    return-wide v0

    .line 77619
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static c(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 77611
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "NO_CLASS"

    goto :goto_0
.end method

.method private d()J
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 77604
    iget-wide v0, p0, LX/0XC;->i:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 77605
    iget-object v1, p0, LX/0XC;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 77606
    :try_start_0
    iget-wide v2, p0, LX/0XC;->i:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 77607
    iget-object v0, p0, LX/0XC;->e:LX/0W3;

    sget-wide v2, LX/0X5;->aw:J

    const-wide/16 v4, 0xc8

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    iput-wide v2, p0, LX/0XC;->i:J

    .line 77608
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77609
    :cond_1
    iget-wide v0, p0, LX/0XC;->i:J

    return-wide v0

    .line 77610
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 77572
    if-eq p1, v0, :cond_1

    .line 77573
    :cond_0
    :goto_0
    return-void

    .line 77574
    :cond_1
    invoke-direct {p0, v0}, LX/0XC;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77575
    invoke-virtual {p0}, LX/0XC;->a()Z

    move-result v1

    .line 77576
    const/4 v0, 0x0

    .line 77577
    monitor-enter p0

    .line 77578
    :try_start_0
    const/4 v3, 0x0

    .line 77579
    instance-of v2, p3, Ljava/util/List;

    const-string v4, "Must be list of marker configs"

    invoke-static {v2, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 77580
    check-cast p3, Ljava/util/List;

    .line 77581
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/0Yj;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    const-string v4, "Must be non-empty list of marker configs"

    invoke-static {v2, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 77582
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v5

    .line 77583
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v4, v3

    .line 77584
    :goto_2
    if-ge v4, v5, :cond_4

    .line 77585
    invoke-interface {p3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Yj;

    .line 77586
    iget-object v3, p0, LX/0XC;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v3, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->e(LX/0Yj;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 77587
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77588
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    :cond_3
    move v2, v3

    .line 77589
    goto :goto_1

    .line 77590
    :cond_4
    move-object v2, p1

    .line 77591
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 77592
    monitor-exit p0

    goto :goto_0

    .line 77593
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 77594
    :cond_5
    :try_start_1
    iget-object v3, p0, LX/0XC;->k:Ljava/util/ArrayList;

    if-nez v3, :cond_6

    .line 77595
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, LX/0XC;->k:Ljava/util/ArrayList;

    .line 77596
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, LX/0XC;->l:Ljava/util/ArrayList;

    .line 77597
    :cond_6
    iget-object v3, p0, LX/0XC;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77598
    iget-object v3, p0, LX/0XC;->l:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77599
    if-nez v1, :cond_7

    .line 77600
    invoke-direct {p0}, LX/0XC;->b()LX/0XB;

    move-result-object v0

    .line 77601
    :cond_7
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77602
    if-eqz v0, :cond_0

    .line 77603
    invoke-interface {v0}, LX/0XB;->a()V

    goto/16 :goto_0
.end method

.method public final a(JLandroid/content/Intent;Ljava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/content/Intent;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 77565
    invoke-direct {p0, p1, p2}, LX/0XC;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0XC;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77566
    :cond_0
    :goto_0
    return-void

    .line 77567
    :cond_1
    const v1, 0x580001

    const-string v2, "BackgroundWorkHandleBroadcast"

    .line 77568
    invoke-static {p3}, LX/0XC;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 77569
    invoke-static {p4}, LX/0XC;->c(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    .line 77570
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 77571
    move-object v0, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, LX/0XC;->a(ILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final a(JLjava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77562
    invoke-direct {p0, p1, p2}, LX/0XC;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0XC;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77563
    :cond_0
    :goto_0
    return-void

    .line 77564
    :cond_1
    const v1, 0x580003

    const-string v2, "BackgroundWorkServiceOnCreate"

    invoke-static {p3}, LX/0XC;->c(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, LX/0XC;->a(ILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final a(JLjava/lang/Class;Landroid/content/Intent;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77555
    invoke-direct {p0, p1, p2}, LX/0XC;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0XC;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77556
    :cond_0
    :goto_0
    return-void

    .line 77557
    :cond_1
    const v1, 0x580004

    const-string v2, "BackgroundWorkServiceOnStart"

    .line 77558
    invoke-static {p3}, LX/0XC;->c(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 77559
    invoke-static {p4}, LX/0XC;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 77560
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 77561
    move-object v0, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, LX/0XC;->a(ILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 77546
    invoke-direct {p0, p1, p2}, LX/0XC;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0XC;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77547
    :cond_0
    :goto_0
    return-void

    .line 77548
    :cond_1
    const v1, 0x580002

    const-string v2, "GeneralBackgroundWorkFromLogger"

    .line 77549
    if-eqz p3, :cond_2

    .line 77550
    :goto_1
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77551
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 77552
    move-object v0, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, LX/0XC;->a(ILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 77553
    :cond_2
    const-string p3, "NO_TYPE"

    goto :goto_1

    .line 77554
    :cond_3
    const-string v0, "NO_NAME"

    goto :goto_2
.end method

.method public final a(LX/0XB;)V
    .locals 1

    .prologue
    .line 77543
    monitor-enter p0

    .line 77544
    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0XC;->g:Ljava/lang/ref/WeakReference;

    .line 77545
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 77516
    invoke-direct {p0, v6}, LX/0XC;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    .line 77517
    :goto_0
    return v0

    .line 77518
    :cond_0
    monitor-enter p0

    .line 77519
    :try_start_0
    iget-object v0, p0, LX/0XC;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 77520
    monitor-exit p0

    move v0, v6

    goto :goto_0

    :cond_1
    move v5, v6

    move v1, v6

    .line 77521
    :goto_1
    iget-object v0, p0, LX/0XC;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_4

    .line 77522
    iget-object v0, p0, LX/0XC;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move v3, v6

    move v4, v1

    .line 77523
    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_3

    .line 77524
    iget-object v1, p0, LX/0XC;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Yj;

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->e(LX/0Yj;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77525
    const/4 v1, 0x1

    move v2, v1

    move v1, v3

    .line 77526
    :goto_3
    add-int/lit8 v3, v1, 0x1

    move v4, v2

    goto :goto_2

    .line 77527
    :cond_2
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 77528
    add-int/lit8 v1, v3, -0x1

    move v2, v4

    goto :goto_3

    .line 77529
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 77530
    iget-object v0, p0, LX/0XC;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 77531
    iget-object v0, p0, LX/0XC;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 77532
    add-int/lit8 v0, v5, -0x1

    .line 77533
    :goto_4
    add-int/lit8 v5, v0, 0x1

    move v1, v4

    goto :goto_1

    .line 77534
    :cond_4
    iget-object v0, p0, LX/0XC;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 77535
    const/4 v0, 0x0

    iput-object v0, p0, LX/0XC;->k:Ljava/util/ArrayList;

    .line 77536
    const/4 v0, 0x0

    iput-object v0, p0, LX/0XC;->l:Ljava/util/ArrayList;

    .line 77537
    invoke-direct {p0}, LX/0XC;->b()LX/0XB;

    move-result-object v0

    .line 77538
    :goto_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77539
    if-eqz v0, :cond_5

    .line 77540
    invoke-interface {v0}, LX/0XB;->a()V

    :cond_5
    move v0, v1

    .line 77541
    goto :goto_0

    .line 77542
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move-object v0, v7

    goto :goto_5

    :cond_7
    move v0, v5

    goto :goto_4
.end method

.method public final b(JLjava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77513
    invoke-direct {p0, p1, p2}, LX/0XC;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0XC;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77514
    :cond_0
    :goto_0
    return-void

    .line 77515
    :cond_1
    const v1, 0x580005

    const-string v2, "BackgroundWorkServiceOnDestroy"

    invoke-static {p3}, LX/0XC;->c(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, LX/0XC;->a(ILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method
