.class public final LX/0kg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field public final synthetic a:LX/0kb;


# direct methods
.method public constructor <init>(LX/0kb;)V
    .locals 0

    .prologue
    .line 127734
    iput-object p1, p0, LX/0kg;->a:LX/0kb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 127735
    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 127736
    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 127737
    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 127738
    iget-object v0, p0, LX/0kg;->a:LX/0kb;

    iget-object v0, v0, LX/0kb;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;

    invoke-direct {v1, p0}, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;-><init>(LX/0kg;)V

    const v2, 0x6583864b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 127739
    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 127740
    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 127741
    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 127742
    return-void
.end method
