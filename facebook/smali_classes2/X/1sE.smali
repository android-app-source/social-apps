.class public LX/1sE;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/1rl;

.field public final d:LX/1ra;

.field private final e:LX/1sM;

.field public final f:Ljava/util/concurrent/ExecutorService;

.field public g:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/location/write/graphql/LocationMutationsModels$LocationUpdateMutationModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 334235
    const-class v0, LX/1sE;

    sput-object v0, LX/1sE;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Tf;LX/0tX;LX/1rl;LX/1ra;LX/1sF;)V
    .locals 7
    .param p1    # LX/0Tf;
        .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 334236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334237
    iput-object p2, p0, LX/1sE;->b:LX/0tX;

    .line 334238
    iput-object p3, p0, LX/1sE;->c:LX/1rl;

    .line 334239
    iput-object p4, p0, LX/1sE;->d:LX/1ra;

    .line 334240
    new-instance v1, LX/1sL;

    invoke-direct {v1}, LX/1sL;-><init>()V

    move-object v1, v1

    .line 334241
    iget-object v2, p0, LX/1sE;->c:LX/1rl;

    .line 334242
    sget-short v3, LX/1rm;->k:S

    sget-wide v5, LX/0X5;->eR:J

    const/4 v4, 0x0

    invoke-static {v2, v3, v5, v6, v4}, LX/1rl;->a(LX/1rl;SJZ)Z

    move-result v3

    move v2, v3

    .line 334243
    iput-boolean v2, v1, LX/1sL;->a:Z

    .line 334244
    move-object v1, v1

    .line 334245
    iget-object v2, p0, LX/1sE;->c:LX/1rl;

    .line 334246
    sget v3, LX/1rm;->l:I

    sget-wide v5, LX/0X5;->eQ:J

    const/16 v4, 0x3e8

    invoke-static {v2, v3, v5, v6, v4}, LX/1rl;->a(LX/1rl;IJI)I

    move-result v3

    move v2, v3

    .line 334247
    iput v2, v1, LX/1sL;->b:I

    .line 334248
    move-object v1, v1

    .line 334249
    iget-object v2, p0, LX/1sE;->c:LX/1rl;

    .line 334250
    sget v3, LX/1rm;->m:I

    sget-wide v5, LX/0X5;->eP:J

    const/16 v4, -0x96

    invoke-static {v2, v3, v5, v6, v4}, LX/1rl;->a(LX/1rl;IJI)I

    move-result v3

    move v2, v3

    .line 334251
    iput v2, v1, LX/1sL;->c:I

    .line 334252
    move-object v1, v1

    .line 334253
    new-instance v2, LX/1sG;

    iget-boolean v3, v1, LX/1sL;->a:Z

    iget v4, v1, LX/1sL;->b:I

    iget v5, v1, LX/1sL;->c:I

    invoke-direct {v2, v3, v4, v5}, LX/1sG;-><init>(ZII)V

    move-object v1, v2

    .line 334254
    move-object v0, v1

    .line 334255
    new-instance v4, LX/1sM;

    invoke-static {p5}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-static {p5}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v2

    check-cast v2, LX/0yD;

    invoke-static {p5}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {v4, v0, v1, v2, v3}, LX/1sM;-><init>(LX/1sG;LX/0dC;LX/0yD;LX/0SG;)V

    .line 334256
    move-object v0, v4

    .line 334257
    iput-object v0, p0, LX/1sE;->e:LX/1sM;

    .line 334258
    iput-object p1, p0, LX/1sE;->f:Ljava/util/concurrent/ExecutorService;

    .line 334259
    return-void
.end method

.method public static b(LX/0QB;)LX/1sE;
    .locals 6

    .prologue
    .line 334260
    new-instance v0, LX/1sE;

    invoke-static {p0}, LX/1rZ;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, LX/0Tf;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1rl;->a(LX/0QB;)LX/1rl;

    move-result-object v3

    check-cast v3, LX/1rl;

    invoke-static {p0}, LX/1ra;->a(LX/0QB;)LX/1ra;

    move-result-object v4

    check-cast v4, LX/1ra;

    const-class v5, LX/1sF;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1sF;

    invoke-direct/range {v0 .. v5}, LX/1sE;-><init>(LX/0Tf;LX/0tX;LX/1rl;LX/1ra;LX/1sF;)V

    .line 334261
    return-object v0
.end method

.method public static b(LX/1sE;LX/2TS;)LX/2vN;
    .locals 14

    .prologue
    .line 334262
    new-instance v1, LX/2vN;

    invoke-direct {v1}, LX/2vN;-><init>()V

    .line 334263
    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 334264
    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_28

    const-string v0, "FOREGROUND"

    .line 334265
    :goto_0
    const-string v2, "app_use_state"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334266
    :cond_0
    iget-object v0, p0, LX/1sE;->e:LX/1sM;

    .line 334267
    iget-object v2, v0, LX/1sM;->b:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 334268
    const-string v2, "device_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334269
    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v0, :cond_4

    .line 334270
    iget-object v0, p0, LX/1sE;->e:LX/1sM;

    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    .line 334271
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 334272
    new-instance v5, LX/2vP;

    invoke-direct {v5}, LX/2vP;-><init>()V

    .line 334273
    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 334274
    const-string v6, "latitude"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 334275
    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 334276
    const-string v6, "longitude"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 334277
    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 334278
    const-string v6, "accuracy_meters"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 334279
    iget-object v4, v0, LX/1sM;->c:LX/0yD;

    invoke-virtual {v4, v2}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v6

    long-to-int v4, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 334280
    const-string v6, "age_ms"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334281
    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->d()LX/0am;

    move-result-object v4

    .line 334282
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 334283
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    .line 334284
    const-string v6, "altitude_meters"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 334285
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->e()LX/0am;

    move-result-object v4

    .line 334286
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 334287
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 334288
    const-string v6, "bearing_degrees"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 334289
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->f()LX/0am;

    move-result-object v4

    .line 334290
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 334291
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 334292
    const-string v6, "speed_meters_per_second"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 334293
    :cond_3
    new-instance v4, LX/2vQ;

    invoke-direct {v4}, LX/2vQ;-><init>()V

    .line 334294
    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 334295
    const-string v6, "locations"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 334296
    move-object v0, v4

    .line 334297
    const-string v2, "location_manager_info"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334298
    :cond_4
    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    if-nez v0, :cond_5

    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    if-eqz v0, :cond_b

    .line 334299
    :cond_5
    new-instance v0, LX/2vR;

    invoke-direct {v0}, LX/2vR;-><init>()V

    .line 334300
    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    if-eqz v2, :cond_8

    .line 334301
    iget-object v2, p0, LX/1sE;->e:LX/1sM;

    iget-object v3, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v3, v3, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    .line 334302
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 334303
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    .line 334304
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 334305
    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v6, :cond_7

    .line 334306
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/wifiscan/WifiScanResult;

    .line 334307
    new-instance v8, LX/2vS;

    invoke-direct {v8}, LX/2vS;-><init>()V

    iget-object v9, v4, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    .line 334308
    const-string v10, "hardware_address"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334309
    move-object v8, v8

    .line 334310
    iget-object v9, v4, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    .line 334311
    const-string v10, "network_name"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334312
    move-object v8, v8

    .line 334313
    iget v9, v4, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 334314
    const-string v10, "rssi_dbm"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334315
    move-object v8, v8

    .line 334316
    iget-object v9, v2, LX/1sM;->d:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    iget-wide v12, v4, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    sub-long/2addr v10, v12

    long-to-int v9, v10

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 334317
    const-string v10, "age_ms"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334318
    move-object v8, v8

    .line 334319
    iget-object v9, v2, LX/1sM;->a:LX/1sG;

    iget-boolean v9, v9, LX/1sG;->a:Z

    if-eqz v9, :cond_6

    .line 334320
    iget-object v4, v4, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    .line 334321
    const-string v9, "frequency_mhz"

    invoke-virtual {v8, v9, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334322
    :cond_6
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334323
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 334324
    :cond_7
    move-object v2, v7

    .line 334325
    const-string v3, "scan_results"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 334326
    :cond_8
    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    if-eqz v2, :cond_a

    .line 334327
    iget-object v2, p0, LX/1sE;->e:LX/1sM;

    iget-object v3, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v3, v3, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 334328
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 334329
    new-instance v4, LX/2vT;

    invoke-direct {v4}, LX/2vT;-><init>()V

    .line 334330
    iget-object v5, v3, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    .line 334331
    const-string v6, "hardware_address"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334332
    iget-object v5, v3, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    .line 334333
    const-string v6, "network_name"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334334
    iget v5, v3, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 334335
    const-string v6, "rssi_dbm"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334336
    iget-object v5, v2, LX/1sM;->a:LX/1sG;

    iget-boolean v5, v5, LX/1sG;->a:Z

    if-eqz v5, :cond_9

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_9

    .line 334337
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_29

    const/4 v5, 0x1

    :goto_2
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 334338
    iget-object v5, v3, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    .line 334339
    const-string v6, "frequency_mhz"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334340
    :cond_9
    move-object v2, v4

    .line 334341
    const-string v3, "connected"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334342
    :cond_a
    const-string v2, "wifi_info"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334343
    :cond_b
    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    if-nez v0, :cond_c

    iget-object v0, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v0, v0, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    if-eqz v0, :cond_27

    .line 334344
    :cond_c
    new-instance v0, LX/2vU;

    invoke-direct {v0}, LX/2vU;-><init>()V

    .line 334345
    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    if-eqz v2, :cond_d

    .line 334346
    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    iget-object v2, v2, Lcom/facebook/location/GeneralCellInfo;->a:Ljava/lang/String;

    .line 334347
    const-string v3, "phone_type"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334348
    move-object v2, v0

    .line 334349
    iget-object v3, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v3, v3, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    iget-object v3, v3, Lcom/facebook/location/GeneralCellInfo;->b:Ljava/lang/String;

    .line 334350
    const-string v4, "sim_country_iso"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334351
    move-object v2, v2

    .line 334352
    iget-object v3, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v3, v3, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    iget-object v3, v3, Lcom/facebook/location/GeneralCellInfo;->c:Ljava/lang/String;

    .line 334353
    const-string v4, "sim_operator_mcc_mnc"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334354
    move-object v2, v2

    .line 334355
    iget-object v3, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v3, v3, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    iget-object v3, v3, Lcom/facebook/location/GeneralCellInfo;->d:Ljava/lang/String;

    .line 334356
    const-string v4, "sim_operator_name"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334357
    move-object v2, v2

    .line 334358
    iget-object v3, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v3, v3, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    iget-boolean v3, v3, Lcom/facebook/location/GeneralCellInfo;->e:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 334359
    const-string v4, "has_icc_card"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 334360
    :cond_d
    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    if-eqz v2, :cond_26

    .line 334361
    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    invoke-static {v2}, LX/1sM;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 334362
    const-string v3, "scan_results"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 334363
    iget-object v2, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v2, v2, Lcom/facebook/location/LocationSignalDataPackage;->f:Ljava/util/List;

    const/4 v4, 0x0

    .line 334364
    if-eqz v2, :cond_e

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-ge v3, v5, :cond_2a

    :cond_e
    move-object v3, v4

    .line 334365
    :cond_f
    :goto_3
    move-object v2, v3

    .line 334366
    iget-object v3, p1, LX/2TS;->a:Lcom/facebook/location/LocationSignalDataPackage;

    iget-object v3, v3, Lcom/facebook/location/LocationSignalDataPackage;->e:Lcom/facebook/location/GeneralCellInfo;

    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 334367
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 334368
    if-eqz v2, :cond_23

    .line 334369
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v10

    move v8, v9

    :goto_4
    if-ge v8, v10, :cond_23

    .line 334370
    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/CellInfo;

    .line 334371
    new-instance v11, LX/2vV;

    invoke-direct {v11}, LX/2vV;-><init>()V

    .line 334372
    instance-of v5, v4, Landroid/telephony/CellInfoCdma;

    if-eqz v5, :cond_12

    .line 334373
    check-cast v4, Landroid/telephony/CellInfoCdma;

    if-nez v3, :cond_11

    move-object v5, v6

    :goto_5
    invoke-static {v4, v5}, LX/1sM;->a(Landroid/telephony/CellInfoCdma;Lcom/facebook/location/CdmaCellInfo;)LX/2vZ;

    move-result-object v4

    .line 334374
    const-string v5, "cdma_info"

    invoke-virtual {v11, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334375
    :goto_6
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334376
    :cond_10
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_4

    .line 334377
    :cond_11
    iget-object v5, v3, Lcom/facebook/location/GeneralCellInfo;->k:Lcom/facebook/location/CdmaCellInfo;

    goto :goto_5

    .line 334378
    :cond_12
    instance-of v5, v4, Landroid/telephony/CellInfoGsm;

    if-eqz v5, :cond_17

    .line 334379
    check-cast v4, Landroid/telephony/CellInfoGsm;

    const p0, 0x7fffffff

    .line 334380
    new-instance v5, LX/2vW;

    invoke-direct {v5}, LX/2vW;-><init>()V

    .line 334381
    invoke-virtual {v4}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v12

    .line 334382
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v13

    if-eq v13, p0, :cond_13

    .line 334383
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vW;->a(Ljava/lang/Integer;)LX/2vW;

    .line 334384
    :cond_13
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v13

    if-eq v13, p0, :cond_14

    .line 334385
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vW;->b(Ljava/lang/Integer;)LX/2vW;

    .line 334386
    :cond_14
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v13

    if-eq v13, p0, :cond_15

    .line 334387
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vW;->c(Ljava/lang/Integer;)LX/2vW;

    .line 334388
    :cond_15
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v13

    if-eq v13, p0, :cond_16

    .line 334389
    invoke-virtual {v12}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, LX/2vW;->d(Ljava/lang/Integer;)LX/2vW;

    .line 334390
    :cond_16
    invoke-virtual {v4}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v12

    .line 334391
    invoke-virtual {v12}, Landroid/telephony/CellSignalStrengthGsm;->getDbm()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, LX/2vW;->e(Ljava/lang/Integer;)LX/2vW;

    .line 334392
    move-object v4, v5

    .line 334393
    const-string v5, "gsm_info"

    invoke-virtual {v11, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334394
    goto :goto_6

    .line 334395
    :cond_17
    instance-of v5, v4, Landroid/telephony/CellInfoLte;

    if-eqz v5, :cond_1d

    .line 334396
    check-cast v4, Landroid/telephony/CellInfoLte;

    const p0, 0x7fffffff

    .line 334397
    new-instance v5, LX/2vX;

    invoke-direct {v5}, LX/2vX;-><init>()V

    .line 334398
    invoke-virtual {v4}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v12

    .line 334399
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v13

    if-eq v13, p0, :cond_18

    .line 334400
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vX;->a(Ljava/lang/Integer;)LX/2vX;

    .line 334401
    :cond_18
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v13

    if-eq v13, p0, :cond_19

    .line 334402
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vX;->b(Ljava/lang/Integer;)LX/2vX;

    .line 334403
    :cond_19
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v13

    if-eq v13, p0, :cond_1a

    .line 334404
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vX;->c(Ljava/lang/Integer;)LX/2vX;

    .line 334405
    :cond_1a
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v13

    if-eq v13, p0, :cond_1b

    .line 334406
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vX;->d(Ljava/lang/Integer;)LX/2vX;

    .line 334407
    :cond_1b
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v13

    if-eq v13, p0, :cond_1c

    .line 334408
    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, LX/2vX;->e(Ljava/lang/Integer;)LX/2vX;

    .line 334409
    :cond_1c
    invoke-virtual {v4}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v12

    .line 334410
    invoke-virtual {v12}, Landroid/telephony/CellSignalStrengthLte;->getDbm()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vX;->f(Ljava/lang/Integer;)LX/2vX;

    .line 334411
    invoke-virtual {v12}, Landroid/telephony/CellSignalStrengthLte;->getTimingAdvance()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, LX/2vX;->g(Ljava/lang/Integer;)LX/2vX;

    .line 334412
    move-object v4, v5

    .line 334413
    const-string v5, "lte_info"

    invoke-virtual {v11, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334414
    goto/16 :goto_6

    .line 334415
    :cond_1d
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x12

    if-lt v5, v12, :cond_10

    instance-of v5, v4, Landroid/telephony/CellInfoWcdma;

    if-eqz v5, :cond_10

    .line 334416
    check-cast v4, Landroid/telephony/CellInfoWcdma;

    const p0, 0x7fffffff

    .line 334417
    new-instance v5, LX/2vY;

    invoke-direct {v5}, LX/2vY;-><init>()V

    .line 334418
    invoke-virtual {v4}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v12

    .line 334419
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v13

    if-eq v13, p0, :cond_1e

    .line 334420
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vY;->a(Ljava/lang/Integer;)LX/2vY;

    .line 334421
    :cond_1e
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v13

    if-eq v13, p0, :cond_1f

    .line 334422
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vY;->b(Ljava/lang/Integer;)LX/2vY;

    .line 334423
    :cond_1f
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v13

    if-eq v13, p0, :cond_20

    .line 334424
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vY;->c(Ljava/lang/Integer;)LX/2vY;

    .line 334425
    :cond_20
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v13

    if-eq v13, p0, :cond_21

    .line 334426
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v5, v13}, LX/2vY;->d(Ljava/lang/Integer;)LX/2vY;

    .line 334427
    :cond_21
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v13

    if-eq v13, p0, :cond_22

    .line 334428
    invoke-virtual {v12}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, LX/2vY;->e(Ljava/lang/Integer;)LX/2vY;

    .line 334429
    :cond_22
    invoke-virtual {v4}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v12

    .line 334430
    invoke-virtual {v12}, Landroid/telephony/CellSignalStrengthWcdma;->getDbm()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, LX/2vY;->f(Ljava/lang/Integer;)LX/2vY;

    .line 334431
    move-object v4, v5

    .line 334432
    const-string v5, "wcdma_info"

    invoke-virtual {v11, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334433
    goto/16 :goto_6

    .line 334434
    :cond_23
    if-eqz v3, :cond_25

    iget-object v4, v3, Lcom/facebook/location/GeneralCellInfo;->f:Ljava/lang/String;

    if-eqz v4, :cond_25

    iget-object v4, v3, Lcom/facebook/location/GeneralCellInfo;->f:Ljava/lang/String;

    const-string v5, "UNKNOWN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_25

    .line 334435
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_24

    .line 334436
    new-instance v4, LX/2vV;

    invoke-direct {v4}, LX/2vV;-><init>()V

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334437
    :cond_24
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2vV;

    iget-object v5, v3, Lcom/facebook/location/GeneralCellInfo;->f:Ljava/lang/String;

    .line 334438
    const-string v8, "network_type"

    invoke-virtual {v4, v8, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334439
    move-object v4, v4

    .line 334440
    iget-object v5, v3, Lcom/facebook/location/GeneralCellInfo;->h:Ljava/lang/String;

    .line 334441
    const-string v8, "network_operator_mcc_mnc"

    invoke-virtual {v4, v8, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334442
    move-object v4, v4

    .line 334443
    iget-object v5, v3, Lcom/facebook/location/GeneralCellInfo;->i:Ljava/lang/String;

    .line 334444
    const-string v8, "network_operator_name"

    invoke-virtual {v4, v8, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334445
    move-object v4, v4

    .line 334446
    iget-object v5, v3, Lcom/facebook/location/GeneralCellInfo;->g:Ljava/lang/String;

    .line 334447
    const-string v8, "network_country_iso"

    invoke-virtual {v4, v8, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 334448
    move-object v4, v4

    .line 334449
    iget-boolean v5, v3, Lcom/facebook/location/GeneralCellInfo;->j:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 334450
    const-string v8, "is_network_roaming"

    invoke-virtual {v4, v8, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 334451
    :cond_25
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2d

    :goto_7
    move-object v2, v6

    .line 334452
    const-string v3, "connected"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 334453
    :cond_26
    const-string v2, "cell_info"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 334454
    :cond_27
    return-object v1

    .line 334455
    :cond_28
    const-string v0, "BACKGROUND"

    goto/16 :goto_0

    .line 334456
    :cond_29
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 334457
    :cond_2a
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 334458
    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    move v5, v3

    :goto_8
    if-ge v5, v7, :cond_2c

    .line 334459
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/CellInfo;

    .line 334460
    invoke-static {v3}, LX/2TF;->a(Landroid/telephony/CellInfo;)Z

    move-result v8

    if-eqz v8, :cond_2b

    .line 334461
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 334462
    :cond_2b
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_8

    .line 334463
    :cond_2c
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 334464
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_f

    move-object v3, v4

    goto/16 :goto_3

    :cond_2d
    move-object v6, v7

    goto :goto_7
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 334465
    iget-object v0, p0, LX/1sE;->g:LX/1Mv;

    if-eqz v0, :cond_0

    .line 334466
    iget-object v0, p0, LX/1sE;->g:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 334467
    const/4 v0, 0x0

    iput-object v0, p0, LX/1sE;->g:LX/1Mv;

    .line 334468
    iget-object v0, p0, LX/1sE;->d:LX/1ra;

    const-wide/high16 v2, -0x8000000000000000L

    .line 334469
    iput-wide v2, v0, LX/1ra;->h:J

    .line 334470
    iput-wide v2, v0, LX/1ra;->f:J

    .line 334471
    :cond_0
    return-void
.end method
