.class public LX/0qm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0qp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qp",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0qn;


# direct methods
.method public constructor <init>(LX/0qn;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 148415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148416
    new-instance v0, LX/0qo;

    invoke-direct {v0, p0}, LX/0qo;-><init>(LX/0qm;)V

    iput-object v0, p0, LX/0qm;->a:Ljava/util/Comparator;

    .line 148417
    new-instance v0, LX/0qp;

    iget-object v1, p0, LX/0qm;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1}, LX/0qp;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/0qm;->b:LX/0qp;

    .line 148418
    iput-object p1, p0, LX/0qm;->c:LX/0qn;

    .line 148419
    return-void
.end method

.method public static b(LX/0QB;)LX/0qm;
    .locals 2

    .prologue
    .line 148443
    new-instance v1, LX/0qm;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-direct {v1, v0}, LX/0qm;-><init>(LX/0qn;)V

    .line 148444
    return-object v1
.end method

.method private g(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 148435
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 148436
    :goto_0
    return-object v0

    .line 148437
    :cond_0
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 148438
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_1

    .line 148439
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 148440
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148441
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 148442
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 148432
    invoke-virtual {p0, p1}, LX/0qm;->f(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 148433
    if-eqz v0, :cond_0

    .line 148434
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p2}, LX/0qm;->e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148429
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    .line 148430
    iget-object p0, v0, LX/0qp;->e:Ljava/util/List;

    move-object v0, p0

    .line 148431
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V
    .locals 5

    .prologue
    .line 148420
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 148421
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 148422
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 148423
    instance-of v4, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_0

    .line 148424
    iget-object v4, p0, LX/0qm;->c:LX/0qn;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4, v1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 148425
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148426
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148427
    invoke-virtual {p0, v0}, LX/0qm;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 148428
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 148372
    if-nez p1, :cond_0

    .line 148373
    :goto_0
    return-void

    .line 148374
    :cond_0
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0, p1}, LX/0qp;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 148396
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148397
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148398
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0, p1}, LX/0qp;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148399
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    .line 148400
    new-instance v1, LX/1u8;

    invoke-direct {v1}, LX/1u8;-><init>()V

    .line 148401
    iput-object p2, v1, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 148402
    move-object v1, v1

    .line 148403
    const/4 p0, 0x0

    .line 148404
    iput-object p0, v1, LX/1u8;->d:Ljava/lang/String;

    .line 148405
    move-object v1, v1

    .line 148406
    sget-object p0, LX/0ql;->b:Ljava/lang/String;

    .line 148407
    iput-object p0, v1, LX/1u8;->i:Ljava/lang/String;

    .line 148408
    move-object v1, v1

    .line 148409
    const-string p0, "synthetic_cursor"

    .line 148410
    iput-object p0, v1, LX/1u8;->c:Ljava/lang/String;

    .line 148411
    move-object v1, v1

    .line 148412
    invoke-virtual {v1}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    move-object v1, v1

    .line 148413
    invoke-virtual {v0, p1, v1}, LX/0qp;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148414
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 148445
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148446
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 148447
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    .line 148448
    :cond_0
    invoke-direct {p0, v0}, LX/0qm;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148449
    if-eqz v0, :cond_1

    .line 148450
    invoke-virtual {p0, v0, p1}, LX/0qm;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 148451
    const/4 v0, 0x1

    .line 148452
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 148395
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->size()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 148389
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148390
    const/4 v0, 0x0

    .line 148391
    :goto_0
    return v0

    .line 148392
    :cond_0
    invoke-direct {p0, p1}, LX/0qm;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148393
    invoke-virtual {p0, v0}, LX/0qm;->a(Ljava/lang/String;)V

    .line 148394
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 148388
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 148386
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0}, LX/0qp;->clear()V

    .line 148387
    return-void
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 148383
    if-nez p1, :cond_0

    .line 148384
    const/4 v0, 0x0

    .line 148385
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0, p1}, LX/0qp;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 2

    .prologue
    .line 148379
    invoke-direct {p0, p1}, LX/0qm;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148380
    if-eqz v0, :cond_0

    .line 148381
    iget-object v1, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v1, v0}, LX/0qp;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 148382
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 148375
    iget-object v0, p0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v0, p1}, LX/0qp;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 148376
    if-nez v0, :cond_0

    .line 148377
    const/4 v0, 0x0

    .line 148378
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method
