.class public abstract LX/1RI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1RJ;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1RJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1RJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 246025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246026
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 246027
    iput-object p1, p0, LX/1RI;->a:Ljava/util/Set;

    .line 246028
    return-void
.end method


# virtual methods
.method public final a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 1
    .param p1    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246021
    invoke-virtual {p0, p1}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 246022
    if-eqz v0, :cond_0

    .line 246023
    invoke-interface {v0, p1, p2}, LX/1RJ;->a(LX/1RN;Landroid/content/Context;)LX/AlW;

    move-result-object v0

    .line 246024
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/1RN;)V
    .locals 1
    .param p1    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246017
    invoke-virtual {p0, p1}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 246018
    if-eqz v0, :cond_0

    .line 246019
    invoke-interface {v0, p1}, LX/1RJ;->a(LX/1RN;)V

    .line 246020
    :cond_0
    return-void
.end method

.method public a(LX/2xq;LX/1RN;)V
    .locals 1
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246013
    invoke-virtual {p0, p2}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 246014
    if-eqz v0, :cond_0

    .line 246015
    invoke-interface {v0, p1, p2}, LX/1RJ;->a(LX/2xq;LX/1RN;)V

    .line 246016
    :cond_0
    return-void
.end method

.method public a(ZLX/1RN;)V
    .locals 1
    .param p2    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246009
    invoke-virtual {p0, p2}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 246010
    if-eqz v0, :cond_0

    .line 246011
    invoke-interface {v0, p1, p2}, LX/1RJ;->a(ZLX/1RN;)V

    .line 246012
    :cond_0
    return-void
.end method

.method public final b(LX/1RN;)V
    .locals 1
    .param p1    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246005
    invoke-virtual {p0, p1}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 246006
    if-eqz v0, :cond_0

    .line 246007
    invoke-interface {v0, p1}, LX/1RJ;->b(LX/1RN;)V

    .line 246008
    :cond_0
    return-void
.end method

.method public b(LX/2xq;LX/1RN;)V
    .locals 1
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246001
    invoke-virtual {p0, p2}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 246002
    if-eqz v0, :cond_0

    .line 246003
    invoke-interface {v0, p1, p2}, LX/1RJ;->b(LX/2xq;LX/1RN;)V

    .line 246004
    :cond_0
    return-void
.end method

.method public final c(LX/1RN;)V
    .locals 1
    .param p1    # LX/1RN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 245990
    invoke-virtual {p0, p1}, LX/1RI;->d(LX/1RN;)LX/1RJ;

    move-result-object v0

    .line 245991
    if-eqz v0, :cond_0

    .line 245992
    invoke-interface {v0, p1}, LX/1RJ;->c(LX/1RN;)V

    .line 245993
    :cond_0
    return-void
.end method

.method public d(LX/1RN;)LX/1RJ;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 245994
    const/4 v1, 0x0

    .line 245995
    iget-object v0, p0, LX/1RI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RJ;

    .line 245996
    invoke-interface {v0, p1}, LX/1RK;->e(LX/1RN;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 245997
    if-eqz v1, :cond_1

    .line 245998
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Two prompt view controllers should not be enabled for the same prompt object"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v1

    :cond_1
    move-object v1, v0

    .line 245999
    goto :goto_0

    .line 246000
    :cond_2
    return-object v1
.end method
