.class public final LX/1tK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1fT;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1fT;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 336375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336376
    iput-object p1, p0, LX/1tK;->a:LX/0QB;

    .line 336377
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/1fT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336378
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/1tK;

    invoke-direct {v2, p0}, LX/1tK;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 336379
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1tK;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 336380
    packed-switch p2, :pswitch_data_0

    .line 336381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336382
    :pswitch_0
    invoke-static {p1}, LX/2gY;->a(LX/0QB;)LX/2gY;

    move-result-object v0

    .line 336383
    :goto_0
    return-object v0

    .line 336384
    :pswitch_1
    new-instance p0, LX/2ga;

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p1}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-direct {p0, v0, v1}, LX/2ga;-><init>(LX/0lC;LX/0Xl;)V

    .line 336385
    move-object v0, p0

    .line 336386
    goto :goto_0

    .line 336387
    :pswitch_2
    invoke-static {p1}, LX/2Ht;->a(LX/0QB;)LX/2Ht;

    move-result-object v0

    goto :goto_0

    .line 336388
    :pswitch_3
    invoke-static {p1}, LX/1fS;->a(LX/0QB;)LX/1fS;

    move-result-object v0

    goto :goto_0

    .line 336389
    :pswitch_4
    invoke-static {p1}, LX/2gb;->a(LX/0QB;)LX/2gb;

    move-result-object v0

    goto :goto_0

    .line 336390
    :pswitch_5
    invoke-static {p1}, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->a(LX/0QB;)Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    move-result-object v0

    goto :goto_0

    .line 336391
    :pswitch_6
    invoke-static {p1}, LX/2ge;->a(LX/0QB;)LX/2ge;

    move-result-object v0

    goto :goto_0

    .line 336392
    :pswitch_7
    invoke-static {p1}, LX/2a5;->getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttPushHandler__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2a5;

    move-result-object v0

    goto :goto_0

    .line 336393
    :pswitch_8
    invoke-static {p1}, LX/2a7;->a(LX/0QB;)LX/2a7;

    move-result-object v0

    goto :goto_0

    .line 336394
    :pswitch_9
    invoke-static {p1}, LX/2a8;->a(LX/0QB;)LX/2a8;

    move-result-object v0

    goto :goto_0

    .line 336395
    :pswitch_a
    invoke-static {p1}, LX/2bB;->a(LX/0QB;)LX/2bB;

    move-result-object v0

    goto :goto_0

    .line 336396
    :pswitch_b
    invoke-static {p1}, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->a(LX/0QB;)Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;

    move-result-object v0

    goto :goto_0

    .line 336397
    :pswitch_c
    invoke-static {p1}, LX/09G;->a(LX/0QB;)LX/09G;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 336398
    const/16 v0, 0xd

    return v0
.end method
