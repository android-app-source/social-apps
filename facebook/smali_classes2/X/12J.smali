.class public LX/12J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/12K;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/12J;


# instance fields
.field public final a:LX/0yW;

.field public final b:LX/0dO;

.field private final c:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0ad;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/03V;

.field public k:LX/122;

.field public l:LX/0yL;


# direct methods
.method public constructor <init>(LX/0yW;LX/0Ot;LX/0dO;LX/0Or;LX/0Ot;LX/0ad;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/03V;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yW;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;",
            "LX/0dO;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175141
    iput-object p1, p0, LX/12J;->a:LX/0yW;

    .line 175142
    iput-object p2, p0, LX/12J;->h:LX/0Ot;

    .line 175143
    iput-object p3, p0, LX/12J;->b:LX/0dO;

    .line 175144
    iput-object p4, p0, LX/12J;->c:LX/0Or;

    .line 175145
    iput-object p5, p0, LX/12J;->i:LX/0Ot;

    .line 175146
    iput-object p6, p0, LX/12J;->d:LX/0ad;

    .line 175147
    iput-object p7, p0, LX/12J;->e:Lcom/facebook/content/SecureContextHelper;

    .line 175148
    iput-object p8, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 175149
    iput-object p9, p0, LX/12J;->g:LX/0Ot;

    .line 175150
    iput-object p10, p0, LX/12J;->j:LX/03V;

    .line 175151
    iget-object v0, p0, LX/12J;->a:LX/0yW;

    invoke-virtual {v0, p0}, LX/0yW;->a(LX/12K;)V

    .line 175152
    return-void
.end method

.method public static a(LX/0QB;)LX/12J;
    .locals 14

    .prologue
    .line 175243
    sget-object v0, LX/12J;->m:LX/12J;

    if-nez v0, :cond_1

    .line 175244
    const-class v1, LX/12J;

    monitor-enter v1

    .line 175245
    :try_start_0
    sget-object v0, LX/12J;->m:LX/12J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 175246
    if-eqz v2, :cond_0

    .line 175247
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 175248
    new-instance v3, LX/12J;

    invoke-static {v0}, LX/0yW;->a(LX/0QB;)LX/0yW;

    move-result-object v4

    check-cast v4, LX/0yW;

    const/16 v5, 0x4e0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0dO;->a(LX/0QB;)LX/0dO;

    move-result-object v6

    check-cast v6, LX/0dO;

    const/16 v7, 0x312

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xbc

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v12, 0x2e3

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-direct/range {v3 .. v13}, LX/12J;-><init>(LX/0yW;LX/0Ot;LX/0dO;LX/0Or;LX/0Ot;LX/0ad;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/03V;)V

    .line 175249
    move-object v0, v3

    .line 175250
    sput-object v0, LX/12J;->m:LX/12J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175251
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 175252
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175253
    :cond_1
    sget-object v0, LX/12J;->m:LX/12J;

    return-object v0

    .line 175254
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 175255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/12J;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 175236
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 175237
    const-string v1, "ref"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175238
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 175239
    const-string v2, "dialtone://switch_to_dialtone"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 175240
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 175241
    new-instance v0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$9;

    invoke-direct {v0, p0, v1}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$9;-><init>(LX/12J;Landroid/content/Intent;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 175242
    return-void
.end method

.method public static a$redex0(LX/12J;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 175230
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 175231
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 175232
    move-object v0, v0

    .line 175233
    const-string v1, "carrier_id"

    iget-object v2, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 175234
    iget-object v0, p0, LX/12J;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 175235
    return-void
.end method

.method public static b(LX/12J;LX/2XZ;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 175192
    iget-object v0, p0, LX/12J;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12J;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/12J;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    if-eq p1, v0, :cond_1

    .line 175193
    :cond_0
    :goto_0
    return-void

    .line 175194
    :cond_1
    iget-object v0, p0, LX/12J;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v2

    .line 175195
    iget-object v0, p0, LX/12J;->b:LX/0dO;

    sget-object v3, LX/0yY;->ZERO_BALANCE_DETECTION:LX/0yY;

    invoke-virtual {v0, v3}, LX/0dP;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 175196
    invoke-direct {p0, v2}, LX/12J;->b(Landroid/app/Activity;)V

    goto :goto_0

    .line 175197
    :cond_2
    iget-object v0, p0, LX/12J;->b:LX/0dO;

    sget-object v3, LX/0yY;->DIALTONE_SWITCHER_ZERO_BALANCE_REMINDER:LX/0yY;

    invoke-virtual {v0, v3}, LX/0dP;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175198
    invoke-virtual {p0}, LX/12J;->b()J

    move-result-wide v5

    const-wide/32 v7, 0x5265c00

    cmp-long v5, v5, v7

    if-lez v5, :cond_5

    const/4 v5, 0x1

    :goto_1
    move v0, v5

    .line 175199
    if-eqz v0, :cond_0

    .line 175200
    invoke-virtual {p0}, LX/12J;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 175201
    :pswitch_0
    const-string v0, "dialtone_zb_automode_impression"

    invoke-static {p0, v0}, LX/12J;->a$redex0(LX/12J;Ljava/lang/String;)V

    .line 175202
    invoke-static {p0}, LX/12J;->k(LX/12J;)V

    .line 175203
    const-string v0, "dialtone_ref_zb_auto_mode"

    invoke-static {p0, v2, v0}, LX/12J;->a$redex0(LX/12J;Landroid/app/Activity;Ljava/lang/String;)V

    .line 175204
    goto :goto_0

    .line 175205
    :sswitch_0
    const-string v4, "auto"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_2

    :sswitch_1
    const-string v1, "dialog"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v1, "tooltip"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v1, "none"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x3

    goto :goto_2

    .line 175206
    :pswitch_1
    invoke-static {p0}, LX/12J;->g(LX/12J;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 175207
    :goto_3
    goto :goto_0

    .line 175208
    :pswitch_2
    iget-object v0, p0, LX/12J;->b:LX/0dO;

    sget-object v1, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {v0, v1}, LX/0dP;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 175209
    :cond_4
    :goto_4
    goto/16 :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_1

    .line 175210
    :cond_6
    invoke-static {p0}, LX/12J;->m(LX/12J;)V

    .line 175211
    const-string v0, "dialtone_zb_dialog_impression"

    invoke-static {p0, v0}, LX/12J;->a$redex0(LX/12J;Ljava/lang/String;)V

    .line 175212
    invoke-static {p0}, LX/12J;->k(LX/12J;)V

    .line 175213
    const v3, 0x7f020b56

    .line 175214
    const v1, 0x7f08064a

    .line 175215
    const v0, 0x7f08064b

    .line 175216
    iget-object v4, p0, LX/12J;->b:LX/0dO;

    sget-object v5, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {v4, v5}, LX/0dP;->a(LX/0yY;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 175217
    const v3, 0x7f02011b

    .line 175218
    const v1, 0x7f08064c

    .line 175219
    const v0, 0x7f08064d

    .line 175220
    :cond_7
    new-instance v4, LX/6WI;

    invoke-direct {v4, v2}, LX/6WI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, LX/6WI;->c(I)LX/6WI;

    move-result-object v3

    const v4, 0x7f080649

    invoke-virtual {v3, v4}, LX/6WI;->a(I)LX/6WI;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/6WI;->b(I)LX/6WI;

    move-result-object v1

    new-instance v3, LX/Bhu;

    invoke-direct {v3, p0, v2}, LX/Bhu;-><init>(LX/12J;Landroid/app/Activity;)V

    invoke-virtual {v1, v0, v3}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    const v1, 0x7f080022

    new-instance v3, LX/Bht;

    invoke-direct {v3, p0}, LX/Bht;-><init>(LX/12J;)V

    invoke-virtual {v0, v1, v3}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    .line 175221
    new-instance v1, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$8;

    invoke-direct {v1, p0, v0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$8;-><init>(LX/12J;LX/6WI;)V

    invoke-virtual {v2, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 175222
    :cond_8
    invoke-static {p0}, LX/12J;->g(LX/12J;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 175223
    iget-object v0, p0, LX/12J;->l:LX/0yL;

    if-nez v0, :cond_9

    .line 175224
    new-instance v0, LX/Bhq;

    invoke-direct {v0, p0}, LX/Bhq;-><init>(LX/12J;)V

    iput-object v0, p0, LX/12J;->l:LX/0yL;

    .line 175225
    iget-object v0, p0, LX/12J;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    iget-object v1, p0, LX/12J;->l:LX/0yL;

    invoke-virtual {v0, v1}, LX/0yc;->a(LX/0yL;)V

    .line 175226
    :cond_9
    const-string v0, "dialtone_zb_tooltip_impression"

    invoke-static {p0, v0}, LX/12J;->a$redex0(LX/12J;Ljava/lang/String;)V

    .line 175227
    invoke-static {p0}, LX/12J;->k(LX/12J;)V

    .line 175228
    invoke-static {p0}, LX/12J;->m(LX/12J;)V

    .line 175229
    new-instance v0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$2;

    invoke-direct {v0, p0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$2;-><init>(LX/12J;)V

    invoke-virtual {v2, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_4

    :sswitch_data_0
    .sparse-switch
        -0x4f6602b8 -> :sswitch_1
        -0x43f42ffd -> :sswitch_2
        0x2dddaf -> :sswitch_0
        0x33af38 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/app/Activity;)V
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    .line 175177
    const-string v0, "dialtone_zb_dialog_impression"

    invoke-static {p0, v0}, LX/12J;->a$redex0(LX/12J;Ljava/lang/String;)V

    .line 175178
    iget-object v0, p0, LX/12J;->d:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-wide v2, LX/49i;->h:J

    invoke-interface/range {v0 .. v5}, LX/0ad;->a(LX/0c0;JJ)J

    move-result-wide v0

    .line 175179
    iget-object v2, p0, LX/12J;->d:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-char v6, LX/49i;->f:C

    const-string v7, ""

    invoke-interface {v2, v3, v6, v7}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 175180
    iget-object v3, p0, LX/12J;->d:LX/0ad;

    sget-object v6, LX/0c0;->Live:LX/0c0;

    sget-char v7, LX/49i;->c:C

    const-string v8, ""

    invoke-interface {v3, v6, v7, v8}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 175181
    iget-object v6, p0, LX/12J;->d:LX/0ad;

    sget-object v7, LX/0c0;->Live:LX/0c0;

    sget-char v8, LX/49i;->a:C

    const-string v9, ""

    invoke-interface {v6, v7, v8, v9}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 175182
    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 175183
    :cond_0
    iget-object v0, p0, LX/12J;->j:LX/03V;

    const-string v1, "ZeroBalanceController_handleNewDialogExperiment"

    const-string v2, "Missing values from QE"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175184
    :cond_1
    :goto_0
    return-void

    .line 175185
    :cond_2
    invoke-static {p0}, LX/12J;->p(LX/12J;)J

    move-result-wide v4

    .line 175186
    const-wide/16 v8, 0x3e8

    mul-long/2addr v0, v8

    cmp-long v0, v4, v0

    if-ltz v0, :cond_1

    .line 175187
    invoke-static {p0}, LX/12J;->l(LX/12J;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 175188
    new-instance v0, LX/6WI;

    invoke-direct {v0, p1}, LX/6WI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, LX/6WI;->a(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/6WI;->b(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v0

    new-instance v1, LX/Bhs;

    invoke-direct {v1, p0, p1}, LX/Bhs;-><init>(LX/12J;Landroid/app/Activity;)V

    invoke-virtual {v0, v6, v1}, LX/6WI;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    new-instance v1, LX/Bhr;

    invoke-direct {v1, p0}, LX/Bhr;-><init>(LX/12J;)V

    .line 175189
    iget-object v2, v0, LX/6WI;->a:LX/31a;

    iput-object v1, v2, LX/31a;->r:Landroid/content/DialogInterface$OnCancelListener;

    .line 175190
    move-object v0, v0

    .line 175191
    new-instance v1, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$5;

    invoke-direct {v1, p0, v0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceController$5;-><init>(LX/12J;LX/6WI;)V

    invoke-virtual {p1, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static g(LX/12J;)Z
    .locals 4

    .prologue
    .line 175175
    invoke-virtual {p0}, LX/12J;->c()I

    move-result v0

    .line 175176
    int-to-long v0, v0

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(LX/12J;)J
    .locals 4

    .prologue
    .line 175174
    iget-object v0, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->K:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static k(LX/12J;)V
    .locals 4

    .prologue
    .line 175171
    iget-object v0, p0, LX/12J;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 175172
    iget-object v2, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0dQ;->K:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 175173
    return-void
.end method

.method private static l(LX/12J;)Z
    .locals 6

    .prologue
    .line 175169
    iget-object v0, p0, LX/12J;->d:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-wide v2, LX/49i;->g:J

    const-wide/16 v4, 0x0

    invoke-interface/range {v0 .. v5}, LX/0ad;->a(LX/0c0;JJ)J

    move-result-wide v0

    .line 175170
    invoke-virtual {p0}, LX/12J;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(LX/12J;)V
    .locals 3

    .prologue
    .line 175167
    iget-object v0, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->L:LX/0Tn;

    invoke-virtual {p0}, LX/12J;->c()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 175168
    return-void
.end method

.method public static n(LX/12J;)V
    .locals 3

    .prologue
    .line 175165
    iget-object v0, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->L:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 175166
    return-void
.end method

.method private static p(LX/12J;)J
    .locals 6

    .prologue
    .line 175164
    iget-object v0, p0, LX/12J;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dQ;->M:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 175160
    iget-object v0, p0, LX/12J;->d:LX/0ad;

    sget-char v1, LX/Bhz;->a:C

    const-string v2, "none"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175161
    if-nez v0, :cond_0

    .line 175162
    const-string v0, "none"

    .line 175163
    :cond_0
    return-object v0
.end method

.method public final a(LX/2XZ;)V
    .locals 0

    .prologue
    .line 175158
    invoke-static {p0, p1}, LX/12J;->b(LX/12J;LX/2XZ;)V

    .line 175159
    return-void
.end method

.method public final a(ZLX/1lF;)V
    .locals 0

    .prologue
    .line 175157
    return-void
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 175154
    invoke-static {p0}, LX/12J;->j(LX/12J;)J

    move-result-wide v2

    .line 175155
    iget-object v0, p0, LX/12J;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 175156
    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 175153
    iget-object v0, p0, LX/12J;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->L:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method
