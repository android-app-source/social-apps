.class public LX/17U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:Ljava/lang/String;

.field private static volatile f:LX/17U;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/17V;

.field public final c:LX/0Zb;

.field public final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 198046
    const-class v0, LX/17U;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/17U;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/17V;LX/0Zb;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 198040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198041
    iput-object p1, p0, LX/17U;->a:Lcom/facebook/content/SecureContextHelper;

    .line 198042
    iput-object p2, p0, LX/17U;->b:LX/17V;

    .line 198043
    iput-object p3, p0, LX/17U;->c:LX/0Zb;

    .line 198044
    iput-object p4, p0, LX/17U;->d:LX/03V;

    .line 198045
    return-void
.end method

.method public static a(LX/0QB;)LX/17U;
    .locals 7

    .prologue
    .line 198027
    sget-object v0, LX/17U;->f:LX/17U;

    if-nez v0, :cond_1

    .line 198028
    const-class v1, LX/17U;

    monitor-enter v1

    .line 198029
    :try_start_0
    sget-object v0, LX/17U;->f:LX/17U;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 198030
    if-eqz v2, :cond_0

    .line 198031
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 198032
    new-instance p0, LX/17U;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/17U;-><init>(Lcom/facebook/content/SecureContextHelper;LX/17V;LX/0Zb;LX/03V;)V

    .line 198033
    move-object v0, p0

    .line 198034
    sput-object v0, LX/17U;->f:LX/17U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198035
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 198036
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 198037
    :cond_1
    sget-object v0, LX/17U;->f:LX/17U;

    return-object v0

    .line 198038
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 198039
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 198011
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 198025
    iget-object v0, p0, LX/17U;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 198026
    return-void
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;LX/0lF;ZLX/1vY;)V
    .locals 5
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 198012
    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/17U;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198013
    :goto_0
    if-eqz p3, :cond_0

    invoke-virtual {p3}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 198014
    :cond_0
    iget-object v0, p0, LX/17U;->d:LX/03V;

    const-string v1, "missing tracking codes"

    invoke-static {p1}, LX/17U;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "missing tracking codes trace"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 198015
    :goto_1
    return-void

    .line 198016
    :catch_0
    sget-object v0, LX/17U;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to launch for result from fragment for intent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 198017
    :cond_1
    iget-object v0, p0, LX/17U;->b:LX/17V;

    invoke-static {p1}, LX/17U;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 198018
    const-string v2, "native_newsfeed"

    move-object v2, v2

    .line 198019
    invoke-virtual {v0, v1, p4, p3, v2}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 198020
    invoke-static {v0, p5}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1vY;)V

    .line 198021
    const-string v1, "browser_metrics_join_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 198022
    if-eqz v1, :cond_2

    .line 198023
    const-string v2, "browser_metrics_join_key"

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 198024
    :cond_2
    iget-object v1, p0, LX/17U;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_1
.end method
