.class public LX/0vj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:LX/0vl;


# instance fields
.field private a:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 158061
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 158062
    new-instance v0, LX/0vk;

    invoke-direct {v0}, LX/0vk;-><init>()V

    sput-object v0, LX/0vj;->b:LX/0vl;

    .line 158063
    :goto_0
    return-void

    .line 158064
    :cond_0
    new-instance v0, LX/3tZ;

    invoke-direct {v0}, LX/3tZ;-><init>()V

    sput-object v0, LX/0vj;->b:LX/0vl;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 158058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158059
    sget-object v0, LX/0vj;->b:LX/0vl;

    invoke-interface {v0, p1}, LX/0vl;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0vj;->a:Ljava/lang/Object;

    .line 158060
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 158056
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/0vl;->a(Ljava/lang/Object;II)V

    .line 158057
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 158055
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/0vl;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(F)Z
    .locals 2

    .prologue
    .line 158054
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/0vl;->a(Ljava/lang/Object;F)Z

    move-result v0

    return v0
.end method

.method public final a(FF)Z
    .locals 2

    .prologue
    .line 158053
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/0vl;->b(Ljava/lang/Object;F)Z

    move-result v0

    return v0
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 158048
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/0vl;->a(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)Z
    .locals 2

    .prologue
    .line 158052
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/0vl;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 158050
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/0vl;->b(Ljava/lang/Object;)V

    .line 158051
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 158049
    sget-object v0, LX/0vj;->b:LX/0vl;

    iget-object v1, p0, LX/0vj;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/0vl;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
