.class public abstract LX/0Rh;
.super LX/0P1;
.source ""

# interfaces
.implements LX/0Ri;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P1",
        "<TK;TV;>;",
        "LX/0Ri",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60421
    invoke-direct {p0}, LX/0P1;-><init>()V

    return-void
.end method

.method public static a()LX/0Rh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Rh",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 60420
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    return-object v0
.end method

.method public static a(Ljava/util/Map;)LX/0Rh;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)",
            "LX/0Rh",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 60408
    instance-of v0, p0, LX/0Rh;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 60409
    check-cast v0, LX/0Rh;

    .line 60410
    invoke-virtual {v0}, LX/0P1;->isPartialView()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60411
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 60412
    sget-object v1, LX/0P1;->EMPTY_ENTRY_ARRAY:[Ljava/util/Map$Entry;

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/util/Map$Entry;

    check-cast v1, [Ljava/util/Map$Entry;

    .line 60413
    array-length p0, v1

    packed-switch p0, :pswitch_data_0

    .line 60414
    invoke-static {v1}, LX/0Rg;->a([Ljava/util/Map$Entry;)LX/0Rg;

    move-result-object v1

    :goto_1
    move-object v0, v1

    .line 60415
    goto :goto_0

    .line 60416
    :pswitch_0
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 60417
    goto :goto_1

    .line 60418
    :pswitch_1
    const/4 p0, 0x0

    aget-object v1, v1, p0

    .line 60419
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;)",
            "LX/0Rh",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 60407
    new-instance v0, LX/12u;

    invoke-direct {v0, p0, p1}, LX/12u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static d()LX/4y1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/4y1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 60406
    new-instance v0, LX/4y1;

    invoke-direct {v0}, LX/4y1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60405
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic a_()LX/0Ri;
    .locals 1

    .prologue
    .line 60404
    invoke-virtual {p0}, LX/0Rh;->e()LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b_()Ljava/util/Set;
    .locals 1

    .prologue
    .line 60399
    invoke-virtual {p0}, LX/0Rh;->f()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public abstract e()LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rh",
            "<TV;TK;>;"
        }
    .end annotation
.end method

.method public final f()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 60403
    invoke-virtual {p0}, LX/0Rh;->e()LX/0Rh;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic values()LX/0Py;
    .locals 1

    .prologue
    .line 60402
    invoke-virtual {p0}, LX/0Rh;->f()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 60401
    invoke-virtual {p0}, LX/0Rh;->f()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60400
    new-instance v0, LX/4y3;

    invoke-direct {v0, p0}, LX/4y3;-><init>(LX/0Rh;)V

    return-object v0
.end method
