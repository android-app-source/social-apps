.class public abstract LX/1cd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/imagepipeline/producers/Consumer",
        "<TT;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 282370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282371
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1cd;->a:Z

    .line 282372
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 282373
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "unhandled exception"

    .line 282374
    sget-object v2, LX/03J;->a:LX/03G;

    const/4 p0, 0x6

    invoke-interface {v2, p0}, LX/03G;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282375
    sget-object v2, LX/03J;->a:LX/03G;

    invoke-static {v0}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, p0, v1, p1}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 282376
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 282369
    return-void
.end method

.method public abstract a(Ljava/lang/Object;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Throwable;)V
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 282362
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cd;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 282363
    :goto_0
    monitor-exit p0

    return-void

    .line 282364
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1cd;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282365
    :try_start_2
    invoke-virtual {p0}, LX/1cd;->a()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 282366
    :catch_0
    move-exception v0

    .line 282367
    :try_start_3
    invoke-direct {p0, v0}, LX/1cd;->a(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 282368
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(F)V
    .locals 1

    .prologue
    .line 282356
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cd;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 282357
    :goto_0
    monitor-exit p0

    return-void

    .line 282358
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, LX/1cd;->a(F)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 282359
    :catch_0
    move-exception v0

    .line 282360
    :try_start_2
    invoke-direct {p0, v0}, LX/1cd;->a(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 282361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Object;Z)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 282349
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cd;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 282350
    :goto_0
    monitor-exit p0

    return-void

    .line 282351
    :cond_0
    :try_start_1
    iput-boolean p2, p0, LX/1cd;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282352
    :try_start_2
    invoke-virtual {p0, p1, p2}, LX/1cd;->a(Ljava/lang/Object;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 282353
    :catch_0
    move-exception v0

    .line 282354
    :try_start_3
    invoke-direct {p0, v0}, LX/1cd;->a(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 282355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 282342
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1cd;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 282343
    :goto_0
    monitor-exit p0

    return-void

    .line 282344
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1cd;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282345
    :try_start_2
    invoke-virtual {p0, p1}, LX/1cd;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 282346
    :catch_0
    move-exception v0

    .line 282347
    :try_start_3
    invoke-direct {p0, v0}, LX/1cd;->a(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 282348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
