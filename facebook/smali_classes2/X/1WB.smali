.class public LX/1WB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/graphics/Typeface;

.field private static final b:Ljava/lang/String;

.field private static i:LX/0Xm;


# instance fields
.field private final c:LX/0ad;

.field private final d:LX/03V;

.field private final e:LX/0tO;

.field public f:F

.field private g:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/1z6;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/1z6;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 267700
    const-class v0, LX/1WB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1WB;->b:Ljava/lang/String;

    .line 267701
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    sput-object v0, LX/1WB;->a:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/03V;LX/0tO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267703
    const/high16 v0, 0x41600000    # 14.0f

    iput v0, p0, LX/1WB;->f:F

    .line 267704
    iput-object p1, p0, LX/1WB;->c:LX/0ad;

    .line 267705
    iput-object p2, p0, LX/1WB;->d:LX/03V;

    .line 267706
    iput-object p3, p0, LX/1WB;->e:LX/0tO;

    .line 267707
    return-void
.end method

.method private static a([Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 267708
    if-eqz p0, :cond_0

    aget-object v0, p0, p1

    invoke-static {v0}, LX/1WB;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    aget-object v0, p0, p1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1WB;
    .locals 6

    .prologue
    .line 267718
    const-class v1, LX/1WB;

    monitor-enter v1

    .line 267719
    :try_start_0
    sget-object v0, LX/1WB;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267720
    sput-object v2, LX/1WB;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267721
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267722
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267723
    new-instance p0, LX/1WB;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v5

    check-cast v5, LX/0tO;

    invoke-direct {p0, v3, v4, v5}, LX/1WB;-><init>(LX/0ad;LX/03V;LX/0tO;)V

    .line 267724
    move-object v0, p0

    .line 267725
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267726
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1WB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267727
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267728
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 267709
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 267710
    const-string v1, " length limit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267711
    const-string v1, " font size: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267712
    const-string v1, " top padding: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267713
    const-string v1, " bottom padding: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267714
    const-string v1, " font style: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267715
    const-string v1, " truncation limit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267716
    const-string v1, " align centering: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267717
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 267729
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b([Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 267699
    if-eqz p0, :cond_0

    aget-object v0, p0, p1

    invoke-static {v0}, LX/1WB;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    aget-object v0, p0, p1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private c()Ljava/util/LinkedHashMap;
    .locals 12
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/1z6;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 267632
    new-instance v10, Ljava/util/LinkedHashMap;

    invoke-direct {v10}, Ljava/util/LinkedHashMap;-><init>()V

    .line 267633
    iget-object v0, p0, LX/1WB;->e:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v10

    .line 267634
    :goto_0
    return-object v0

    .line 267635
    :cond_0
    iget-object v0, p0, LX/1WB;->e:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->h()I

    move-result v1

    .line 267636
    iget-object v0, p0, LX/1WB;->e:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->n()I

    move-result v2

    .line 267637
    if-nez v1, :cond_1

    move-object v0, v10

    .line 267638
    goto :goto_0

    .line 267639
    :cond_1
    int-to-float v0, v2

    iget v3, p0, LX/1WB;->f:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_2

    const-string v6, "sans-serif-black"

    .line 267640
    :goto_1
    const-string v0, "sans-serif"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    int-to-float v0, v2

    iget v3, p0, LX/1WB;->f:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    move v7, v9

    .line 267641
    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    new-instance v0, LX/1z6;

    iget-object v3, p0, LX/1WB;->e:LX/0tO;

    invoke-virtual {v3}, LX/0tO;->p()I

    move-result v3

    iget-object v4, p0, LX/1WB;->e:LX/0tO;

    invoke-virtual {v4}, LX/0tO;->p()I

    move-result v4

    const/4 v5, 0x0

    const/4 v8, -0x1

    invoke-direct/range {v0 .. v9}, LX/1z6;-><init>(IIIILjava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v10, v11, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v10

    .line 267642
    goto :goto_0

    .line 267643
    :cond_2
    const-string v6, "sans-serif"

    goto :goto_1

    .line 267644
    :cond_3
    const/4 v7, 0x0

    goto :goto_2
.end method

.method private d()Ljava/util/LinkedHashMap;
    .locals 22
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/1z6;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267663
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WB;->c:LX/0ad;

    sget-char v2, LX/0wk;->h:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 267664
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WB;->c:LX/0ad;

    sget-char v3, LX/0wk;->f:C

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 267665
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WB;->c:LX/0ad;

    sget-char v4, LX/0wk;->j:C

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 267666
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WB;->c:LX/0ad;

    sget-char v5, LX/0wk;->c:C

    const/4 v6, 0x0

    invoke-interface {v1, v5, v6}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 267667
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WB;->c:LX/0ad;

    sget-char v6, LX/0wk;->g:C

    const/4 v7, 0x0

    invoke-interface {v1, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 267668
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WB;->c:LX/0ad;

    sget-char v7, LX/0wk;->k:C

    const/4 v8, 0x0

    invoke-interface {v1, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 267669
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1WB;->c:LX/0ad;

    sget-char v8, LX/0wk;->a:C

    const/4 v9, 0x0

    invoke-interface {v1, v8, v9}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 267670
    new-instance v12, Ljava/util/LinkedHashMap;

    invoke-direct {v12}, Ljava/util/LinkedHashMap;-><init>()V

    .line 267671
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-nez v6, :cond_1

    :cond_0
    move-object v1, v12

    .line 267672
    :goto_0
    return-object v1

    .line 267673
    :cond_1
    const-string v1, "[^0-9-]+"

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 267674
    const-string v1, "[^0-9-]+"

    invoke-virtual {v3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 267675
    const-string v1, "[^0-9-]+"

    invoke-virtual {v4, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 267676
    const-string v1, "[^0-9-]+"

    invoke-virtual {v5, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 267677
    const-string v1, ","

    invoke-virtual {v6, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 267678
    if-nez v7, :cond_6

    const/4 v1, 0x0

    move-object v14, v1

    .line 267679
    :goto_1
    if-nez v8, :cond_7

    const/4 v1, 0x0

    move-object v11, v1

    .line 267680
    :goto_2
    array-length v0, v15

    move/from16 v20, v0

    .line 267681
    move-object/from16 v0, v16

    array-length v1, v0

    move/from16 v0, v20

    if-ne v1, v0, :cond_3

    move-object/from16 v0, v17

    array-length v1, v0

    move/from16 v0, v20

    if-ne v1, v0, :cond_3

    move-object/from16 v0, v18

    array-length v1, v0

    move/from16 v0, v20

    if-ne v1, v0, :cond_3

    move-object/from16 v0, v19

    array-length v1, v0

    move/from16 v0, v20

    if-ne v1, v0, :cond_3

    if-eqz v14, :cond_2

    array-length v1, v14

    move/from16 v0, v20

    if-ne v1, v0, :cond_3

    :cond_2
    if-eqz v11, :cond_8

    array-length v1, v11

    move/from16 v0, v20

    if-eq v1, v0, :cond_8

    .line 267682
    :cond_3
    move-object/from16 v0, p0

    iget-object v9, v0, LX/1WB;->d:LX/03V;

    sget-object v10, LX/1WB;->b:Ljava/lang/String;

    const-string v1, "String arrays are mismatched: "

    if-nez v7, :cond_4

    const-string v7, ""

    :cond_4
    if-nez v8, :cond_5

    const-string v8, ""

    :cond_5
    invoke-static/range {v1 .. v8}, LX/1WB;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v10, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v12

    .line 267683
    goto :goto_0

    .line 267684
    :cond_6
    const-string v1, "[^0-9-]+"

    invoke-virtual {v7, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move-object v14, v1

    goto :goto_1

    .line 267685
    :cond_7
    const-string v1, "[^0-9-]+"

    invoke-virtual {v8, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    goto :goto_2

    .line 267686
    :cond_8
    const/4 v1, 0x0

    move v13, v1

    :goto_3
    move/from16 v0, v20

    if-ge v13, v0, :cond_c

    .line 267687
    :try_start_0
    aget-object v1, v15, v13

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 267688
    aget-object v1, v16, v13

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 267689
    aget-object v1, v17, v13

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    .line 267690
    aget-object v1, v18, v13

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 267691
    aget-object v1, v19, v13

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 267692
    invoke-static {v14, v13}, LX/1WB;->a([Ljava/lang/String;I)I

    move-result v9

    .line 267693
    invoke-static {v11, v13}, LX/1WB;->b([Ljava/lang/String;I)I

    move-result v10

    .line 267694
    new-instance v1, LX/1z6;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v7, "light"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    const-string v7, "sans-serif-light"

    :goto_4
    const/4 v8, 0x0

    invoke-direct/range {v1 .. v10}, LX/1z6;-><init>(IIIILjava/lang/String;Ljava/lang/String;III)V

    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267695
    :goto_5
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto :goto_3

    .line 267696
    :cond_9
    const-string v7, "sans-serif"
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 267697
    :catch_0
    move-object/from16 v0, p0

    iget-object v9, v0, LX/1WB;->d:LX/03V;

    sget-object v10, LX/1WB;->b:Ljava/lang/String;

    const-string v1, "Cannot parse number: "

    aget-object v2, v15, v13

    aget-object v3, v16, v13

    aget-object v4, v17, v13

    aget-object v5, v18, v13

    aget-object v6, v19, v13

    if-nez v14, :cond_a

    const-string v7, "null"

    :goto_6
    if-nez v11, :cond_b

    const-string v8, "null"

    :goto_7
    invoke-static/range {v1 .. v8}, LX/1WB;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v10, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_a
    aget-object v7, v14, v13

    goto :goto_6

    :cond_b
    aget-object v8, v11, v13

    goto :goto_7

    :cond_c
    move-object v1, v12

    .line 267698
    goto/16 :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 6

    .prologue
    .line 267645
    iget-object v0, p0, LX/1WB;->c:LX/0ad;

    sget-wide v2, LX/0wk;->m:J

    const-wide/16 v4, 0xfa

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1z6;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267646
    invoke-static {p1}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, LX/1WB;->a(Ljava/lang/String;Z)LX/1z6;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)LX/1z6;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267647
    iget-object v0, p0, LX/1WB;->g:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 267648
    invoke-direct {p0}, LX/1WB;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/1WB;->g:Ljava/util/LinkedHashMap;

    .line 267649
    :cond_0
    iget-object v0, p0, LX/1WB;->h:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_1

    .line 267650
    invoke-direct {p0}, LX/1WB;->c()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/1WB;->h:Ljava/util/LinkedHashMap;

    .line 267651
    :cond_1
    if-eqz p2, :cond_3

    iget-object v0, p0, LX/1WB;->e:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->d()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    .line 267652
    iget-object v0, p0, LX/1WB;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 267653
    invoke-static {p1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v3, v1, :cond_2

    .line 267654
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1z6;

    .line 267655
    :goto_0
    return-object v0

    .line 267656
    :cond_3
    iget-object v0, p0, LX/1WB;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 267657
    invoke-static {p1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v3, v1, :cond_4

    .line 267658
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1z6;

    goto :goto_0

    .line 267659
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 1

    .prologue
    .line 267660
    invoke-virtual {p0, p1}, LX/1WB;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1z6;

    move-result-object v0

    .line 267661
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, v0, LX/1z6;->h:I

    goto :goto_0
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 267662
    iget-object v0, p0, LX/1WB;->c:LX/0ad;

    sget-wide v2, LX/0wk;->l:J

    const-wide/16 v4, 0x96

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method
