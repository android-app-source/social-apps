.class public LX/1CE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/0So;

.field private final c:LX/03V;

.field private final d:I

.field private final e:I

.field private final f:LX/0W3;

.field public g:Ljava/lang/Throwable;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:J


# direct methods
.method public constructor <init>(LX/0Sh;LX/0So;LX/03V;IILX/0W3;)V
    .locals 2

    .prologue
    .line 215637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215638
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, LX/1CE;->a:LX/0Sh;

    .line 215639
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    iput-object v0, p0, LX/1CE;->b:LX/0So;

    .line 215640
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/1CE;->c:LX/03V;

    .line 215641
    iput p4, p0, LX/1CE;->d:I

    .line 215642
    iput p5, p0, LX/1CE;->e:I

    .line 215643
    iput-object p6, p0, LX/1CE;->f:LX/0W3;

    .line 215644
    const/16 v0, 0xa

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1CE;->h:Ljava/util/List;

    .line 215645
    const/4 v0, 0x1

    iput v0, p0, LX/1CE;->i:I

    .line 215646
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1CE;->m:J

    .line 215647
    return-void
.end method

.method public static a(LX/1CE;ZLX/7K6;)V
    .locals 6

    .prologue
    .line 215648
    iget-object v0, p0, LX/1CE;->c:LX/03V;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1CE;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 215649
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0xc8

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 215650
    iget-object v0, p0, LX/1CE;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 215651
    if-nez v0, :cond_0

    .line 215652
    const-string v0, "(null)"

    .line 215653
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    const/16 v4, 0x398

    if-gt v3, v4, :cond_2

    .line 215654
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 215655
    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 215656
    :cond_1
    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215657
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215658
    const-string v0, "\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 215659
    :cond_2
    const-string v0, "{\'count\':%d,\'reason\':\'%s\',\'msgs\':[%s]}"

    iget-object v2, p0, LX/1CE;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, p2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 215660
    const-string v1, "VideoPlayerError.%s.%d"

    iget-object v2, p0, LX/1CE;->k:Ljava/lang/String;

    iget v3, p0, LX/1CE;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 215661
    iget-object v2, p0, LX/1CE;->f:LX/0W3;

    sget-wide v4, LX/0X5;->ih:J

    const/16 v3, 0x3e8

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v2

    .line 215662
    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    iget-object v1, p0, LX/1CE;->g:Ljava/lang/Throwable;

    .line 215663
    iput-object v1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 215664
    move-object v0, v0

    .line 215665
    iput v2, v0, LX/0VK;->e:I

    .line 215666
    move-object v0, v0

    .line 215667
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 215668
    iget-object v1, p0, LX/1CE;->c:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 215669
    :cond_3
    iget-object v0, p0, LX/1CE;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 215670
    const/4 v0, 0x0

    iput-object v0, p0, LX/1CE;->g:Ljava/lang/Throwable;

    .line 215671
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1CE;->m:J

    .line 215672
    if-eqz p1, :cond_4

    .line 215673
    iget v0, p0, LX/1CE;->i:I

    iget v1, p0, LX/1CE;->d:I

    mul-int/2addr v0, v1

    iget v1, p0, LX/1CE;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/1CE;->i:I

    .line 215674
    :goto_1
    return-void

    .line 215675
    :cond_4
    const/4 v0, 0x1

    iput v0, p0, LX/1CE;->i:I

    goto :goto_1
.end method

.method public static a$redex0(LX/1CE;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 215676
    iget-wide v0, p0, LX/1CE;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1CE;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/1CE;->m:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 215677
    sget-object v0, LX/7K6;->TIMEOUT:LX/7K6;

    invoke-static {p0, v4, v0}, LX/1CE;->a(LX/1CE;ZLX/7K6;)V

    .line 215678
    :cond_0
    iget-object v0, p0, LX/1CE;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215679
    :goto_0
    return-void

    .line 215680
    :cond_1
    iget-object v0, p0, LX/1CE;->j:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1CE;->k:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 215681
    :cond_2
    sget-object v0, LX/7K6;->ID_NULL:LX/7K6;

    invoke-static {p0, v4, v0}, LX/1CE;->a(LX/1CE;ZLX/7K6;)V

    .line 215682
    :cond_3
    :goto_1
    const-wide/16 v9, 0x0

    .line 215683
    iget-wide v5, p0, LX/1CE;->m:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_4

    .line 215684
    iget-object v5, p0, LX/1CE;->b:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    const-wide/16 v7, 0x1388

    add-long/2addr v5, v7

    iput-wide v5, p0, LX/1CE;->m:J

    .line 215685
    :cond_4
    iget-boolean v5, p0, LX/1CE;->l:Z

    if-eqz v5, :cond_6

    .line 215686
    :goto_2
    goto :goto_0

    .line 215687
    :cond_5
    iget-object v0, p0, LX/1CE;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/1CE;->i:I

    if-lt v0, v1, :cond_3

    .line 215688
    const/4 v0, 0x1

    sget-object v1, LX/7K6;->COUNT:LX/7K6;

    invoke-static {p0, v0, v1}, LX/1CE;->a(LX/1CE;ZLX/7K6;)V

    goto :goto_1

    .line 215689
    :cond_6
    iget-wide v5, p0, LX/1CE;->m:J

    iget-object v7, p0, LX/1CE;->b:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v7

    sub-long/2addr v5, v7

    invoke-static {v9, v10, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    .line 215690
    iget-object v7, p0, LX/1CE;->a:LX/0Sh;

    new-instance v8, Lcom/facebook/video/engine/SoftReportSender$1;

    invoke-direct {v8, p0}, Lcom/facebook/video/engine/SoftReportSender$1;-><init>(LX/1CE;)V

    invoke-virtual {v7, v8, v5, v6}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 215691
    const/4 v5, 0x1

    iput-boolean v5, p0, LX/1CE;->l:Z

    goto :goto_2
.end method
