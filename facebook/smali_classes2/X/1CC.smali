.class public LX/1CC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/1CC;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/03V;

.field public final c:LX/0So;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:J

.field public h:I


# direct methods
.method public constructor <init>(LX/03V;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215599
    const-class v0, LX/1CC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1CC;->a:Ljava/lang/String;

    .line 215600
    iput-object p1, p0, LX/1CC;->b:LX/03V;

    .line 215601
    iput-object p2, p0, LX/1CC;->c:LX/0So;

    .line 215602
    invoke-static {p0}, LX/1CC;->m(LX/1CC;)V

    .line 215603
    return-void
.end method

.method public static a(LX/0QB;)LX/1CC;
    .locals 5

    .prologue
    .line 215562
    sget-object v0, LX/1CC;->i:LX/1CC;

    if-nez v0, :cond_1

    .line 215563
    const-class v1, LX/1CC;

    monitor-enter v1

    .line 215564
    :try_start_0
    sget-object v0, LX/1CC;->i:LX/1CC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 215565
    if-eqz v2, :cond_0

    .line 215566
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 215567
    new-instance p0, LX/1CC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/1CC;-><init>(LX/03V;LX/0So;)V

    .line 215568
    move-object v0, p0

    .line 215569
    sput-object v0, LX/1CC;->i:LX/1CC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215570
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 215571
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 215572
    :cond_1
    sget-object v0, LX/1CC;->i:LX/1CC;

    return-object v0

    .line 215573
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 215574
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 215575
    invoke-virtual {p0}, LX/1CC;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215576
    iget-object v0, p0, LX/1CC;->b:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/1CC;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Method called without active session"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215577
    :cond_0
    return-void
.end method

.method public static m(LX/1CC;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 215578
    const/4 v0, 0x0

    iput-object v0, p0, LX/1CC;->d:Ljava/lang/String;

    .line 215579
    iput-boolean v2, p0, LX/1CC;->e:Z

    .line 215580
    iput-boolean v2, p0, LX/1CC;->f:Z

    .line 215581
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1CC;->g:J

    .line 215582
    iput v2, p0, LX/1CC;->h:I

    .line 215583
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 215584
    invoke-virtual {p0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215585
    sget-object v0, LX/0JS;->VIDEO_HOME_SESSION_ID:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    iget-object v1, p0, LX/1CC;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 215586
    :cond_0
    return-void
.end method

.method public final g()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 215587
    const-string v0, "getSessionDuration"

    invoke-direct {p0, v0}, LX/1CC;->a(Ljava/lang/String;)V

    .line 215588
    iget-wide v0, p0, LX/1CC;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    move-wide v0, v2

    .line 215589
    :goto_0
    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 215590
    iget-object v2, p0, LX/1CC;->b:LX/03V;

    iget-object v3, p0, LX/1CC;->a:Ljava/lang/String;

    const-string v4, "Session duration is negative."

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215591
    :cond_0
    return-wide v0

    .line 215592
    :cond_1
    iget-object v0, p0, LX/1CC;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v4, p0, LX/1CC;->g:J

    sub-long/2addr v0, v4

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 215593
    iget-object v0, p0, LX/1CC;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 215594
    const-string v0, "isPaused"

    invoke-direct {p0, v0}, LX/1CC;->a(Ljava/lang/String;)V

    .line 215595
    iget-boolean v0, p0, LX/1CC;->e:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 215596
    const-string v0, "isBackgrounded"

    invoke-direct {p0, v0}, LX/1CC;->a(Ljava/lang/String;)V

    .line 215597
    iget-boolean v0, p0, LX/1CC;->f:Z

    return v0
.end method
