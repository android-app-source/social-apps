.class public final LX/1ew;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ex;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1ex",
        "<",
        "LX/1FL;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1BV;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/1cd;

.field public final synthetic d:LX/1cW;

.field public final synthetic e:LX/1cK;


# direct methods
.method public constructor <init>(LX/1cK;LX/1BV;Ljava/lang/String;LX/1cd;LX/1cW;)V
    .locals 0

    .prologue
    .line 289429
    iput-object p1, p0, LX/1ew;->e:LX/1cK;

    iput-object p2, p0, LX/1ew;->a:LX/1BV;

    iput-object p3, p0, LX/1ew;->b:Ljava/lang/String;

    iput-object p4, p0, LX/1ew;->c:LX/1cd;

    iput-object p5, p0, LX/1ew;->d:LX/1cW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1eg;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 289412
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 289413
    invoke-virtual {p1}, LX/1eg;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/1eg;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LX/1eg;->e()Ljava/lang/Exception;

    move-result-object v0

    instance-of v0, v0, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 289414
    if-eqz v0, :cond_1

    .line 289415
    iget-object v0, p0, LX/1ew;->a:LX/1BV;

    iget-object v1, p0, LX/1ew;->b:Ljava/lang/String;

    const-string v2, "DiskCacheProducer"

    invoke-interface {v0, v1, v2, v6}, LX/1BV;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 289416
    iget-object v0, p0, LX/1ew;->c:LX/1cd;

    invoke-virtual {v0}, LX/1cd;->b()V

    .line 289417
    :goto_1
    return-object v6

    .line 289418
    :cond_1
    invoke-virtual {p1}, LX/1eg;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289419
    iget-object v0, p0, LX/1ew;->a:LX/1BV;

    iget-object v1, p0, LX/1ew;->b:Ljava/lang/String;

    const-string v2, "DiskCacheProducer"

    invoke-virtual {p1}, LX/1eg;->e()Ljava/lang/Exception;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3, v6}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 289420
    iget-object v0, p0, LX/1ew;->e:LX/1cK;

    iget-object v0, v0, LX/1cK;->a:LX/1cF;

    iget-object v1, p0, LX/1ew;->c:LX/1cd;

    iget-object v2, p0, LX/1ew;->d:LX/1cW;

    invoke-interface {v0, v1, v2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    goto :goto_1

    .line 289421
    :cond_2
    invoke-virtual {p1}, LX/1eg;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FL;

    .line 289422
    if-eqz v0, :cond_3

    .line 289423
    iget-object v1, p0, LX/1ew;->a:LX/1BV;

    iget-object v2, p0, LX/1ew;->b:Ljava/lang/String;

    const-string v3, "DiskCacheProducer"

    iget-object v4, p0, LX/1ew;->a:LX/1BV;

    iget-object v5, p0, LX/1ew;->b:Ljava/lang/String;

    invoke-static {v4, v5, v7}, LX/1cK;->a(LX/1BV;Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 289424
    iget-object v1, p0, LX/1ew;->c:LX/1cd;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, LX/1cd;->b(F)V

    .line 289425
    iget-object v1, p0, LX/1ew;->c:LX/1cd;

    invoke-virtual {v1, v0, v7}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 289426
    invoke-virtual {v0}, LX/1FL;->close()V

    goto :goto_1

    .line 289427
    :cond_3
    iget-object v0, p0, LX/1ew;->a:LX/1BV;

    iget-object v1, p0, LX/1ew;->b:Ljava/lang/String;

    const-string v2, "DiskCacheProducer"

    iget-object v3, p0, LX/1ew;->a:LX/1BV;

    iget-object v4, p0, LX/1ew;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, LX/1cK;->a(LX/1BV;Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 289428
    iget-object v0, p0, LX/1ew;->e:LX/1cK;

    iget-object v0, v0, LX/1cK;->a:LX/1cF;

    iget-object v1, p0, LX/1ew;->c:LX/1cd;

    iget-object v2, p0, LX/1ew;->d:LX/1cW;

    invoke-interface {v0, v1, v2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
