.class public LX/1BW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zd;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1BW;


# instance fields
.field private a:Z

.field private final b:LX/0ZL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ZL",
            "<",
            "LX/2xo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1BQ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213196
    invoke-virtual {p1}, LX/1BQ;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/1BW;->a:Z

    .line 213197
    new-instance v0, LX/0ZL;

    .line 213198
    iget v1, p1, LX/1BQ;->d:I

    move v1, v1

    .line 213199
    invoke-direct {v0, v1}, LX/0ZL;-><init>(I)V

    iput-object v0, p0, LX/1BW;->b:LX/0ZL;

    .line 213200
    return-void
.end method

.method public static a(LX/0QB;)LX/1BW;
    .locals 4

    .prologue
    .line 213201
    sget-object v0, LX/1BW;->c:LX/1BW;

    if-nez v0, :cond_1

    .line 213202
    const-class v1, LX/1BW;

    monitor-enter v1

    .line 213203
    :try_start_0
    sget-object v0, LX/1BW;->c:LX/1BW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 213204
    if-eqz v2, :cond_0

    .line 213205
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 213206
    new-instance p0, LX/1BW;

    invoke-static {v0}, LX/1BQ;->b(LX/0QB;)LX/1BQ;

    move-result-object v3

    check-cast v3, LX/1BQ;

    invoke-direct {p0, v3}, LX/1BW;-><init>(LX/1BQ;)V

    .line 213207
    move-object v0, p0

    .line 213208
    sput-object v0, LX/1BW;->c:LX/1BW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213209
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 213210
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213211
    :cond_1
    sget-object v0, LX/1BW;->c:LX/1BW;

    return-object v0

    .line 213212
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 213213
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2xo;)V
    .locals 1

    .prologue
    .line 213214
    iget-boolean v0, p0, LX/1BW;->a:Z

    if-eqz v0, :cond_0

    .line 213215
    iget-object v0, p0, LX/1BW;->b:LX/0ZL;

    invoke-virtual {v0, p1}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 213216
    :cond_0
    return-void
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213217
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213218
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 213219
    iget-object v0, p0, LX/1BW;->b:LX/0ZL;

    invoke-virtual {v0}, LX/0ZL;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 213220
    iget-object v0, p0, LX/1BW;->b:LX/0ZL;

    invoke-virtual {v0}, LX/0ZL;->d()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/1BW;->b:LX/0ZL;

    invoke-virtual {v0, v1}, LX/0ZL;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xo;

    invoke-virtual {v0}, LX/2xo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213221
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 213222
    :cond_0
    return-object v2
.end method
