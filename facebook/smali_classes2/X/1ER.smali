.class public LX/1ER;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1ER;


# instance fields
.field public final a:LX/1ES;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/1ES;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219899
    iput-object p1, p0, LX/1ER;->a:LX/1ES;

    .line 219900
    iput-object p2, p0, LX/1ER;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 219901
    iput-object p3, p0, LX/1ER;->c:LX/0Uh;

    .line 219902
    return-void
.end method

.method public static a(LX/0QB;)LX/1ER;
    .locals 6

    .prologue
    .line 219885
    sget-object v0, LX/1ER;->d:LX/1ER;

    if-nez v0, :cond_1

    .line 219886
    const-class v1, LX/1ER;

    monitor-enter v1

    .line 219887
    :try_start_0
    sget-object v0, LX/1ER;->d:LX/1ER;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 219888
    if-eqz v2, :cond_0

    .line 219889
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 219890
    new-instance p0, LX/1ER;

    invoke-static {v0}, LX/1ES;->a(LX/0QB;)LX/1ES;

    move-result-object v3

    check-cast v3, LX/1ES;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/1ER;-><init>(LX/1ES;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V

    .line 219891
    move-object v0, p0

    .line 219892
    sput-object v0, LX/1ER;->d:LX/1ER;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219893
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 219894
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 219895
    :cond_1
    sget-object v0, LX/1ER;->d:LX/1ER;

    return-object v0

    .line 219896
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 219897
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 3

    .prologue
    .line 219884
    iget-object v0, p0, LX/1ER;->c:LX/0Uh;

    const/16 v1, 0x334

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
