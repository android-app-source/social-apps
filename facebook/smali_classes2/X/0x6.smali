.class public LX/0x6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 161916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161917
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 161918
    iput-object v0, p0, LX/0x6;->a:LX/0Ot;

    .line 161919
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 161920
    iput-object v0, p0, LX/0x6;->b:LX/0Ot;

    .line 161921
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 161922
    iput-object v0, p0, LX/0x6;->c:LX/0Ot;

    .line 161923
    return-void
.end method

.method public static c(LX/0x6;)V
    .locals 4

    .prologue
    .line 161904
    const-string v0, "FbMainTabActivity.setupAudioConfigurator"

    const v1, -0x69ee70b8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 161905
    :try_start_0
    iget-object v0, p0, LX/0x6;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 161906
    iget-object v1, p0, LX/0x6;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f030142

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 161907
    const v2, 0x7f0d060d

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/sounds/configurator/AudioConfigurator;

    .line 161908
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 161909
    iget-object v0, p0, LX/0x6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 161910
    iput-object v0, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 161911
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161912
    const v0, 0x68bd1249

    invoke-static {v0}, LX/02m;->a(I)V

    .line 161913
    return-void

    .line 161914
    :catchall_0
    move-exception v0

    const v1, 0x42047819

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 161915
    iget-object v0, p0, LX/0x6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/13d;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
