.class public LX/1c7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1c8;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1c7;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/4CK;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/4CM;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0W3;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0W3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/4CK;",
            ">;",
            "LX/0Or",
            "<",
            "LX/4CM;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 281546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281547
    iput-object p1, p0, LX/1c7;->a:LX/0Or;

    .line 281548
    iput-object p2, p0, LX/1c7;->b:LX/0Or;

    .line 281549
    iput-object p3, p0, LX/1c7;->c:LX/0W3;

    .line 281550
    return-void
.end method

.method public static a(LX/0QB;)LX/1c7;
    .locals 6

    .prologue
    .line 281558
    sget-object v0, LX/1c7;->d:LX/1c7;

    if-nez v0, :cond_1

    .line 281559
    const-class v1, LX/1c7;

    monitor-enter v1

    .line 281560
    :try_start_0
    sget-object v0, LX/1c7;->d:LX/1c7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 281561
    if-eqz v2, :cond_0

    .line 281562
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 281563
    new-instance v4, LX/1c7;

    const/16 v3, 0x221f

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v3, 0x2220

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {v4, v5, p0, v3}, LX/1c7;-><init>(LX/0Or;LX/0Or;LX/0W3;)V

    .line 281564
    move-object v0, v4

    .line 281565
    sput-object v0, LX/1c7;->d:LX/1c7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281566
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 281567
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 281568
    :cond_1
    sget-object v0, LX/1c7;->d:LX/1c7;

    return-object v0

    .line 281569
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 281570
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 281557
    iget-object v0, p0, LX/1c7;->c:LX/0W3;

    sget-wide v2, LX/0X5;->eW:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method private b()LX/0Or;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/1c8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281555
    iget-object v1, p0, LX/1c7;->c:LX/0W3;

    sget-wide v3, LX/0X5;->eZ:J

    invoke-interface {v1, v3, v4}, LX/0W4;->a(J)Z

    move-result v1

    move v0, v1

    .line 281556
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1c7;->b:LX/0Or;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1c7;->a:LX/0Or;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1ln;)Z
    .locals 1

    .prologue
    .line 281554
    invoke-direct {p0}, LX/1c7;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/1c7;->b()LX/0Or;

    move-result-object v0

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1c8;

    invoke-interface {v0, p1}, LX/1c8;->a(LX/1ln;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1ln;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 281551
    invoke-direct {p0}, LX/1c7;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281552
    const/4 v0, 0x0

    .line 281553
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, LX/1c7;->b()LX/0Or;

    move-result-object v0

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1c8;

    invoke-interface {v0, p1}, LX/1c8;->b(LX/1ln;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
