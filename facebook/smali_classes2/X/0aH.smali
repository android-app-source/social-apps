.class public LX/0aH;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/1MG;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84435
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 84436
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;LX/0SI;)LX/1MG;
    .locals 15

    .prologue
    .line 84437
    new-instance v0, LX/1MG;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageManager;

    const/16 v3, 0x5c0

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v5

    check-cast v5, LX/0VT;

    invoke-static {p0}, LX/1MH;->b(LX/0QB;)LX/1MH;

    move-result-object v6

    check-cast v6, LX/1MH;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {p0}, LX/0l4;->createInstance__com_facebook_fbservice_ops_CriticalServiceExceptionChecker__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0l4;

    move-result-object v13

    check-cast v13, LX/0l4;

    invoke-static {p0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v14

    check-cast v14, LX/0Xl;

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    invoke-direct/range {v0 .. v14}, LX/1MG;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0VT;LX/1MH;LX/03V;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;LX/0SI;LX/0l4;LX/0Xl;)V

    .line 84438
    return-object v0
.end method
