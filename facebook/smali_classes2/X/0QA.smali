.class public abstract LX/0QA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QB;


# static fields
.field private static final a:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "Landroid/content/Context;",
            "LX/0S6;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lcom/facebook/base/app/AbstractApplicationLike;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57955
    new-instance v0, LX/0QE;

    new-instance v1, LX/0QL;

    invoke-direct {v1}, LX/0QL;-><init>()V

    invoke-direct {v0, v1}, LX/0QE;-><init>(LX/0QM;)V

    sput-object v0, LX/0QA;->a:LX/0QJ;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57957
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0RI",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57917
    new-instance v0, LX/52Y;

    const/4 v1, 0x0

    const-class v2, Ljava/util/Set;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/reflect/Type;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-direct {v0, v1, v2, v3}, LX/52Y;-><init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    .line 57918
    invoke-static {v0}, LX/0RL;->a(Ljava/lang/reflect/Type;)LX/0RL;

    move-result-object v0

    .line 57919
    if-eqz p1, :cond_0

    .line 57920
    invoke-static {v0, p1}, LX/0RI;->a(LX/0RL;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    .line 57921
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0RI;->a(LX/0RL;)LX/0RI;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 57953
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 57954
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57947
    const-string v0, "FbInjector.injectMe()"

    const v1, 0x62ad1591

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 57948
    :try_start_0
    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 57949
    invoke-virtual {v0, p0, p1}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57950
    const v0, 0x866b381

    invoke-static {v0}, LX/02m;->a(I)V

    .line 57951
    return-void

    .line 57952
    :catchall_0
    move-exception v0

    const v1, -0x7cd898f1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 57958
    new-instance v0, LX/4fr;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid annotation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(LX/0RI;)LX/0RI;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0RI",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 57934
    new-instance v0, LX/52Y;

    const/4 v1, 0x0

    const-class v2, Ljava/util/Set;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/reflect/Type;

    const/4 v4, 0x0

    .line 57935
    iget-object v5, p0, LX/0RI;->b:LX/0RL;

    move-object v5, v5

    .line 57936
    iget-object v6, v5, LX/0RL;->b:Ljava/lang/reflect/Type;

    move-object v5, v6

    .line 57937
    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v3}, LX/52Y;-><init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    .line 57938
    invoke-static {v0}, LX/0RL;->a(Ljava/lang/reflect/Type;)LX/0RL;

    move-result-object v0

    .line 57939
    invoke-virtual {p0}, LX/0RI;->c()Ljava/lang/annotation/Annotation;

    move-result-object v1

    .line 57940
    invoke-virtual {p0}, LX/0RI;->b()Ljava/lang/Class;

    move-result-object v2

    .line 57941
    if-eqz v1, :cond_0

    .line 57942
    new-instance v2, LX/0RI;

    invoke-static {v1}, LX/0RI;->a(Ljava/lang/annotation/Annotation;)LX/0RK;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/0RI;-><init>(LX/0RL;LX/0RK;)V

    move-object v0, v2

    .line 57943
    :goto_0
    return-object v0

    .line 57944
    :cond_0
    if-eqz v2, :cond_1

    .line 57945
    invoke-static {v0, v2}, LX/0RI;->a(LX/0RL;Ljava/lang/Class;)LX/0RI;

    move-result-object v0

    goto :goto_0

    .line 57946
    :cond_1
    invoke-static {v0}, LX/0RI;->a(LX/0RL;)LX/0RI;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Lcom/facebook/base/app/AbstractApplicationLike;
    .locals 3

    .prologue
    .line 57923
    sget-object v0, LX/0QA;->b:Lcom/facebook/base/app/AbstractApplicationLike;

    if-eqz v0, :cond_0

    .line 57924
    sget-object v0, LX/0QA;->b:Lcom/facebook/base/app/AbstractApplicationLike;

    .line 57925
    :goto_0
    return-object v0

    .line 57926
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 57927
    :goto_1
    instance-of v1, v0, LX/002;

    if-eqz v1, :cond_1

    .line 57928
    check-cast v0, LX/002;

    invoke-interface {v0}, LX/002;->h_()Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 57929
    :cond_1
    instance-of v1, v0, Lcom/facebook/base/app/AbstractApplicationLike;

    if-eqz v1, :cond_2

    .line 57930
    check-cast v0, Lcom/facebook/base/app/AbstractApplicationLike;

    goto :goto_0

    .line 57931
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Injector is not supported in process "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/00G;->g()LX/00G;

    move-result-object v2

    .line 57932
    iget-object p0, v2, LX/00G;->b:Ljava/lang/String;

    move-object v2, p0

    .line 57933
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static get(Landroid/content/Context;)LX/0QA;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 57922
    sget-object v0, LX/0QA;->a:LX/0QJ;

    invoke-interface {v0, p0}, LX/0QJ;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QA;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Class;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation
.end method

.method public abstract a(LX/0RI;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<*>;)Z"
        }
    .end annotation
.end method
