.class public LX/1sY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 334960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334961
    iput-object p1, p0, LX/1sY;->a:Landroid/content/Context;

    .line 334962
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 334963
    invoke-static {p0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 334964
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 334965
    const-string v1, "application/vnd.android.package-archive"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 334966
    const/16 p1, 0x16

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 334967
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-ge v3, v4, :cond_2

    .line 334968
    :cond_0
    :goto_0
    move v1, v1

    .line 334969
    if-eqz v1, :cond_1

    .line 334970
    const-string v1, "android.intent.action.INSTALL_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334971
    :goto_1
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 334972
    const-string v1, "android.intent.extra.INSTALLER_PACKAGE_NAME"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334973
    return-object v0

    .line 334974
    :cond_1
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 334975
    :cond_2
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v3, p1, :cond_3

    move v1, v2

    .line 334976
    goto :goto_0

    .line 334977
    :cond_3
    invoke-static {p0}, LX/EkM;->b(Landroid/content/Context;)V

    .line 334978
    sget v3, LX/EkM;->b:I

    move v3, v3

    .line 334979
    if-lez v3, :cond_4

    if-gt v3, p1, :cond_4

    move v1, v2

    .line 334980
    goto :goto_0

    .line 334981
    :cond_4
    const-string v3, "android.permission.REQUEST_INSTALL_PACKAGES"

    invoke-virtual {p0, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v3

    .line 334982
    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method
