.class public LX/0iW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field public final b:[Landroid/view/View;

.field private final c:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Z

.field private final e:I

.field private final f:I

.field public g:I

.field public h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121006
    new-instance v0, LX/3tC;

    invoke-direct {v0}, LX/3tC;-><init>()V

    sput-object v0, LX/0iW;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public varargs constructor <init>(ZIILandroid/view/View;[Landroid/view/View;)V
    .locals 1
    .param p4    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 120958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120959
    iput-object p5, p0, LX/0iW;->b:[Landroid/view/View;

    .line 120960
    iput-boolean p1, p0, LX/0iW;->d:Z

    .line 120961
    iput p2, p0, LX/0iW;->e:I

    .line 120962
    iput p3, p0, LX/0iW;->f:I

    .line 120963
    iput-object p4, p0, LX/0iW;->c:Landroid/view/View;

    .line 120964
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0iW;->i:Ljava/util/List;

    .line 120965
    iget-object v0, p0, LX/0iW;->b:[Landroid/view/View;

    const/4 p1, 0x0

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 120966
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 120967
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 120968
    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 121004
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, LX/0iW;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 121005
    return-void
.end method

.method private static b(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 121002
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, LX/0iW;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 121003
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 120993
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/0iW;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 120994
    iget-object v0, p0, LX/0iW;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v2}, LX/0iW;->a(Landroid/view/View;I)V

    .line 120995
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 120996
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 120997
    :goto_1
    if-ge v2, v3, :cond_2

    .line 120998
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-boolean v1, p0, LX/0iW;->d:Z

    if-eqz v1, :cond_1

    iget v1, p0, LX/0iW;->g:I

    :goto_2
    invoke-static {v0, v1}, LX/0iW;->a(Landroid/view/View;I)V

    .line 120999
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 121000
    :cond_1
    iget v1, p0, LX/0iW;->g:I

    neg-int v1, v1

    goto :goto_2

    .line 121001
    :cond_2
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 120981
    iget v0, p0, LX/0iW;->h:I

    if-lez v0, :cond_0

    iget v0, p0, LX/0iW;->h:I

    move v1, v0

    :goto_0
    move v2, v3

    .line 120982
    :goto_1
    iget-object v0, p0, LX/0iW;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 120983
    iget-object v0, p0, LX/0iW;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-boolean v4, p0, LX/0iW;->d:Z

    if-eqz v4, :cond_1

    neg-int v4, v1

    :goto_2
    invoke-static {v0, v4}, LX/0iW;->b(Landroid/view/View;I)V

    .line 120984
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 120985
    :cond_0
    iget v0, p0, LX/0iW;->g:I

    move v1, v0

    goto :goto_0

    :cond_1
    move v4, v1

    .line 120986
    goto :goto_2

    .line 120987
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 120988
    :goto_3
    if-ge v3, v4, :cond_4

    .line 120989
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v2, p0, LX/0iW;->h:I

    if-lez v2, :cond_3

    iget v2, p0, LX/0iW;->g:I

    sub-int/2addr v2, v1

    :goto_4
    invoke-static {v0, v2}, LX/0iW;->b(Landroid/view/View;I)V

    .line 120990
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 120991
    :cond_3
    iget v2, p0, LX/0iW;->f:I

    goto :goto_4

    .line 120992
    :cond_4
    return-void
.end method

.method public final onPreDraw()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120969
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/0iW;->b:[Landroid/view/View;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 120970
    iget v2, p0, LX/0iW;->g:I

    iget-object v3, p0, LX/0iW;->b:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, LX/0iW;->g:I

    .line 120971
    iget-object v2, p0, LX/0iW;->c:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0iW;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, LX/0iW;->b:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 120972
    :cond_0
    iget-object v2, p0, LX/0iW;->i:Ljava/util/List;

    iget-object v3, p0, LX/0iW;->b:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120973
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120974
    :cond_2
    iget v0, p0, LX/0iW;->g:I

    iget v2, p0, LX/0iW;->e:I

    add-int/2addr v0, v2

    iput v0, p0, LX/0iW;->g:I

    .line 120975
    iget-object v0, p0, LX/0iW;->c:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 120976
    iget-object v0, p0, LX/0iW;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, LX/0iW;->h:I

    .line 120977
    :cond_3
    iget-object v0, p0, LX/0iW;->b:[Landroid/view/View;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 120978
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/0iW;->b:[Landroid/view/View;

    array-length v1, v1

    if-lez v1, :cond_4

    iget v1, p0, LX/0iW;->g:I

    if-lez v1, :cond_4

    .line 120979
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 120980
    :cond_4
    const/4 v0, 0x1

    return v0
.end method
