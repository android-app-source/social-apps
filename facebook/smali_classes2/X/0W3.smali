.class public LX/0W3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;
.implements LX/0W4;


# instance fields
.field public final a:LX/0Wx;

.field private final b:LX/0X0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0X0",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0W4;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

.field private e:LX/0W4;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Wx;)V
    .locals 4

    .prologue
    .line 75436
    new-instance v0, LX/0X0;

    const-wide/32 v2, 0xdbba00

    invoke-direct {v0, p1, v2, v3}, LX/0X0;-><init>(LX/0SG;J)V

    invoke-direct {p0, p2, v0}, LX/0W3;-><init>(LX/0Wx;LX/0X0;)V

    .line 75437
    return-void
.end method

.method private constructor <init>(LX/0Wx;LX/0X0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Wx;",
            "LX/0X0",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75407
    iput-object p2, p0, LX/0W3;->b:LX/0X0;

    .line 75408
    iput-object p1, p0, LX/0W3;->a:LX/0Wx;

    .line 75409
    iget-object v0, p0, LX/0W3;->a:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    move-result-object v0

    iput-object v0, p0, LX/0W3;->d:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 75410
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0W3;->c:Ljava/util/ArrayList;

    .line 75411
    const/4 v0, 0x0

    iput-object v0, p0, LX/0W3;->e:LX/0W4;

    .line 75412
    return-void
.end method

.method private j(J)LX/0W4;
    .locals 1

    .prologue
    .line 75413
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v0

    invoke-virtual {p0, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(JD)D
    .locals 3

    .prologue
    .line 75414
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75415
    invoke-interface {v0, p1, p2, p3, p4}, LX/0W4;->a(JD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JI)I
    .locals 1

    .prologue
    .line 75416
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75417
    invoke-interface {v0, p1, p2, p3}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public final a(JJ)J
    .locals 3

    .prologue
    .line 75418
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75419
    invoke-interface {v0, p1, p2, p3, p4}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()LX/0W4;
    .locals 6

    .prologue
    .line 75420
    iget-object v0, p0, LX/0W3;->e:LX/0W4;

    .line 75421
    if-nez v0, :cond_0

    .line 75422
    iget-object v0, p0, LX/0W3;->a:LX/0Wx;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0W3;->a:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->a()LX/0eT;

    move-result-object v1

    .line 75423
    :goto_0
    new-instance v0, LX/0X7;

    iget-object v2, p0, LX/0W3;->a:LX/0Wx;

    iget-object v3, p0, LX/0W3;->b:LX/0X0;

    iget-object v4, p0, LX/0W3;->d:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-static {}, LX/0X8;->a()Ljava/util/Map;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/0X7;-><init>(LX/0eT;LX/0Wx;LX/0X0;Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;Ljava/util/Map;)V

    .line 75424
    iput-object v0, p0, LX/0W3;->e:LX/0W4;

    .line 75425
    :cond_0
    return-object v0

    .line 75426
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(I)LX/0W4;
    .locals 3

    .prologue
    .line 75427
    iget-object v1, p0, LX/0W3;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 75428
    :try_start_0
    iget-object v0, p0, LX/0W3;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75429
    iget-object v0, p0, LX/0W3;->c:Ljava/util/ArrayList;

    invoke-virtual {p0}, LX/0W3;->a()LX/0W4;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75430
    :cond_0
    iget-object v0, p0, LX/0W3;->c:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W4;

    monitor-exit v1

    return-object v0

    .line 75431
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(JLjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75432
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75433
    invoke-interface {v0, p1, p2, p3}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Z
    .locals 1

    .prologue
    .line 75434
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75435
    invoke-interface {v0, p1, p2}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final a(JZ)Z
    .locals 1

    .prologue
    .line 75438
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75439
    invoke-interface {v0, p1, p2, p3}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final b(JD)V
    .locals 1

    .prologue
    .line 75400
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75401
    invoke-interface {v0, p1, p2, p3, p4}, LX/0W4;->b(JD)V

    .line 75402
    return-void
.end method

.method public final b(JJ)V
    .locals 1

    .prologue
    .line 75403
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75404
    invoke-interface {v0, p1, p2, p3, p4}, LX/0W4;->b(JJ)V

    .line 75405
    return-void
.end method

.method public final b(J)Z
    .locals 1

    .prologue
    .line 75365
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75366
    invoke-interface {v0, p1, p2}, LX/0W4;->b(J)Z

    move-result v0

    return v0
.end method

.method public final b(JZ)Z
    .locals 1

    .prologue
    .line 75367
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75368
    invoke-interface {v0, p1, p2, p3}, LX/0W4;->b(JZ)Z

    move-result v0

    return v0
.end method

.method public final c(J)J
    .locals 3

    .prologue
    .line 75398
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75399
    invoke-interface {v0, p1, p2}, LX/0W4;->c(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 75369
    iget-object v0, p0, LX/0W3;->a:LX/0Wx;

    invoke-interface {v0, p0}, LX/0Wx;->registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z

    .line 75370
    iget-object v0, p0, LX/0W3;->b:LX/0X0;

    .line 75371
    iget-object v1, v0, LX/0X0;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 75372
    iget-object v0, p0, LX/0W3;->a:LX/0Wx;

    invoke-interface {v0}, LX/0Wx;->c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    move-result-object v0

    iput-object v0, p0, LX/0W3;->d:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 75373
    iget-object v1, p0, LX/0W3;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 75374
    :try_start_0
    iget-object v0, p0, LX/0W3;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 75375
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75376
    const/4 v0, 0x0

    iput-object v0, p0, LX/0W3;->e:LX/0W4;

    .line 75377
    return-void

    .line 75378
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final c(JZ)V
    .locals 1

    .prologue
    .line 75379
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75380
    invoke-interface {v0, p1, p2, p3}, LX/0W4;->c(JZ)V

    .line 75381
    return-void
.end method

.method public final d(J)J
    .locals 3

    .prologue
    .line 75382
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75383
    invoke-interface {v0, p1, p2}, LX/0W4;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75384
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75385
    invoke-interface {v0, p1, p2}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75386
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75387
    invoke-interface {v0, p1, p2}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(J)D
    .locals 3

    .prologue
    .line 75388
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75389
    invoke-interface {v0, p1, p2}, LX/0W4;->g(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final h(J)D
    .locals 3

    .prologue
    .line 75390
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75391
    invoke-interface {v0, p1, p2}, LX/0W4;->h(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final i(J)V
    .locals 1

    .prologue
    .line 75392
    invoke-direct {p0, p1, p2}, LX/0W3;->j(J)LX/0W4;

    move-result-object v0

    .line 75393
    invoke-interface {v0, p1, p2}, LX/0W4;->i(J)V

    .line 75394
    return-void
.end method

.method public final onConfigChanged([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75395
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 75396
    :cond_0
    :goto_0
    return-void

    .line 75397
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/0W3;->e:LX/0W4;

    goto :goto_0
.end method
