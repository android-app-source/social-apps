.class public LX/0mR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/00I;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/00I;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 132531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132532
    iput-object p1, p0, LX/0mR;->a:LX/0Ot;

    .line 132533
    iput-object p2, p0, LX/0mR;->b:LX/0Ot;

    .line 132534
    return-void
.end method

.method public static a(LX/0QB;)LX/0mR;
    .locals 1

    .prologue
    .line 132527
    invoke-static {p0}, LX/0mR;->b(LX/0QB;)LX/0mR;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0mR;
    .locals 3

    .prologue
    .line 132528
    new-instance v0, LX/0mR;

    const-class v1, LX/00I;

    invoke-interface {p0, v1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3e5

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/0mR;-><init>(LX/0Ot;LX/0Ot;)V

    .line 132529
    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132530
    iget-object v0, p0, LX/0mR;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WV;

    invoke-virtual {v0}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
