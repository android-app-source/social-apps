.class public LX/0fU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile o:LX/0fU;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0fc;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/app/Activity;",
            "LX/0fc;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0fc;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/03V;

.field public final g:LX/0Xl;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public i:LX/0Yb;

.field public final j:LX/0So;

.field public k:J

.field public l:LX/0i3;

.field public m:LX/10y;

.field public n:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 108210
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "user_left_app_at"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0fU;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0So;)V
    .locals 2
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 108198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108199
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/0fU;->e:Ljava/util/Set;

    .line 108200
    iput-object p1, p0, LX/0fU;->f:LX/03V;

    .line 108201
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0fU;->b:Ljava/util/List;

    .line 108202
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->e()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/0fU;->d:Ljava/util/Map;

    .line 108203
    const/4 v0, 0x0

    iput v0, p0, LX/0fU;->c:I

    .line 108204
    iput-object p2, p0, LX/0fU;->g:LX/0Xl;

    .line 108205
    iput-object p3, p0, LX/0fU;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 108206
    iput-object p4, p0, LX/0fU;->j:LX/0So;

    .line 108207
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    iput-object v0, p0, LX/0fU;->n:Ljava/lang/String;

    .line 108208
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0fU;->k:J

    .line 108209
    return-void
.end method

.method public static a(LX/0QB;)LX/0fU;
    .locals 7

    .prologue
    .line 108185
    sget-object v0, LX/0fU;->o:LX/0fU;

    if-nez v0, :cond_1

    .line 108186
    const-class v1, LX/0fU;

    monitor-enter v1

    .line 108187
    :try_start_0
    sget-object v0, LX/0fU;->o:LX/0fU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 108188
    if-eqz v2, :cond_0

    .line 108189
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 108190
    new-instance p0, LX/0fU;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0fU;-><init>(LX/03V;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0So;)V

    .line 108191
    move-object v0, p0

    .line 108192
    sput-object v0, LX/0fU;->o:LX/0fU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108193
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 108194
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 108195
    :cond_1
    sget-object v0, LX/0fU;->o:LX/0fU;

    return-object v0

    .line 108196
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 108197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Landroid/app/Activity;)Z
    .locals 1
    .param p0    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 108184
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)J
    .locals 4

    .prologue
    .line 108180
    iget-wide v2, p0, LX/0fU;->k:J

    .line 108181
    if-eqz p1, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    iput-wide v0, p0, LX/0fU;->k:J

    .line 108182
    return-wide v2

    :cond_0
    move-wide v0, v2

    .line 108183
    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 108211
    iget-object v0, p0, LX/0fU;->i:LX/0Yb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 108212
    iget-object v0, p0, LX/0fU;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 108213
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/0fU;->l:LX/0i3;

    .line 108214
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 108173
    iget-object v0, p0, LX/0fU;->b:Ljava/util/List;

    sget-object v1, LX/0fc;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 108174
    iget-object v0, p0, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108175
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fc;

    invoke-virtual {v0}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 108176
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 108177
    :cond_1
    iget-object v0, p0, LX/0fU;->f:LX/03V;

    const-string v1, "activity_stack_size"

    invoke-virtual {p0}, LX/0fU;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 108178
    iget-object v0, p0, LX/0fU;->f:LX/03V;

    const-string v1, "activity_creation_count"

    iget v2, p0, LX/0fU;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 108179
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 108167
    iget-object v0, p0, LX/0fU;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fc;

    .line 108168
    if-eqz v0, :cond_0

    .line 108169
    iget-object v1, p0, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 108170
    iget-object v1, p0, LX/0fU;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108171
    iget-object v1, p0, LX/0fU;->e:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 108172
    :cond_0
    return-void
.end method

.method public final e()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0fc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108166
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/0fU;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final e(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 108162
    iget-object v0, p0, LX/0fU;->m:LX/10y;

    if-eqz v0, :cond_0

    .line 108163
    iget-object v0, p0, LX/0fU;->m:LX/10y;

    .line 108164
    iget-object p0, v0, LX/10y;->a:LX/10x;

    invoke-virtual {p0, p1}, LX/10x;->e(Landroid/app/Activity;)V

    .line 108165
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 108161
    iget-object v0, p0, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
