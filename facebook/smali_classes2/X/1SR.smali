.class public LX/1SR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247890
    iput-object p1, p0, LX/1SR;->a:LX/0Ot;

    .line 247891
    return-void
.end method

.method public static b(LX/0QB;)LX/1SR;
    .locals 2

    .prologue
    .line 247892
    new-instance v0, LX/1SR;

    const/16 v1, 0xc83

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1SR;-><init>(LX/0Ot;)V

    .line 247893
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 247894
    iget-object v0, p0, LX/1SR;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    .line 247895
    if-nez v0, :cond_0

    move v0, v1

    .line 247896
    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
