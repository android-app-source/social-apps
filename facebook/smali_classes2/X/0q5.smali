.class public LX/0q5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0q5;


# instance fields
.field public final a:Z

.field private b:LX/03R;

.field private c:LX/03R;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 146969
    const-wide/16 v5, -0x1

    const/4 v1, 0x0

    .line 146970
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 146971
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-ge v2, v7, :cond_2

    move v2, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146972
    :goto_0
    :try_start_1
    move v2, v2

    .line 146973
    if-nez v2, :cond_1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 146974
    :cond_0
    :goto_1
    move v0, v1

    .line 146975
    invoke-direct {p0, v0}, LX/0q5;-><init>(Z)V

    .line 146976
    return-void

    .line 146977
    :cond_1
    :try_start_2
    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J

    move-result-wide v3

    cmp-long v2, v3, v5

    if-eqz v2, :cond_0

    .line 146978
    invoke-static {}, Landroid/net/TrafficStats;->getTotalTxBytes()J

    move-result-wide v3

    cmp-long v2, v3, v5

    if-eqz v2, :cond_0

    .line 146979
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v3

    cmp-long v2, v3, v5

    if-eqz v2, :cond_0

    .line 146980
    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v3

    cmp-long v2, v3, v5

    if-eqz v2, :cond_0

    .line 146981
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    .line 146982
    invoke-static {v2}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v3

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    .line 146983
    invoke-static {v2}, Landroid/net/TrafficStats;->getUidTxBytes(I)J
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-wide v3

    cmp-long v2, v3, v5

    if-eqz v2, :cond_0

    .line 146984
    const/4 v1, 0x1

    goto :goto_1

    .line 146985
    :catch_0
    move-exception v2

    .line 146986
    invoke-static {v2}, LX/0q5;->a(Ljava/lang/RuntimeException;)V

    goto :goto_1

    .line 146987
    :cond_2
    :try_start_3
    const-class v2, Landroid/net/TrafficStats;

    const-string v7, "getStatsService"

    invoke-static {p1, v2, v7}, LX/0q5;->a(LX/03V;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 146988
    if-nez v2, :cond_3

    move v2, v3

    .line 146989
    goto :goto_0

    .line 146990
    :cond_3
    const/4 v7, 0x0

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v2, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 146991
    if-nez v2, :cond_4

    move v2, v4

    .line 146992
    goto :goto_0

    .line 146993
    :cond_4
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "getMobileIfaces"

    invoke-static {p1, v7, v8}, LX/0q5;->a(LX/03V;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 146994
    if-nez v7, :cond_5

    move v2, v3

    .line 146995
    goto :goto_0

    .line 146996
    :cond_5
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v7, v2, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    .line 146997
    if-eqz v2, :cond_6

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v7, 0x0

    invoke-interface {v2, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    move-result v2

    if-nez v2, :cond_6

    move v2, v3

    goto/16 :goto_0

    :cond_6
    move v2, v4

    goto/16 :goto_0

    .line 146998
    :catch_1
    move-exception v2

    .line 146999
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "FbTrafficStats_exception_"

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception in trustTrafficStatsToNotCrash."

    invoke-virtual {p1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v4

    .line 147000
    goto/16 :goto_0
.end method

.method private constructor <init>(Z)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 147001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147002
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0q5;->b:LX/03R;

    .line 147003
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0q5;->c:LX/03R;

    .line 147004
    iput-boolean p1, p0, LX/0q5;->a:Z

    .line 147005
    return-void
.end method

.method public static a(LX/0QB;)LX/0q5;
    .locals 4

    .prologue
    .line 146956
    sget-object v0, LX/0q5;->d:LX/0q5;

    if-nez v0, :cond_1

    .line 146957
    const-class v1, LX/0q5;

    monitor-enter v1

    .line 146958
    :try_start_0
    sget-object v0, LX/0q5;->d:LX/0q5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 146959
    if-eqz v2, :cond_0

    .line 146960
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 146961
    new-instance p0, LX/0q5;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/0q5;-><init>(LX/03V;)V

    .line 146962
    move-object v0, p0

    .line 146963
    sput-object v0, LX/0q5;->d:LX/0q5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146964
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 146965
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146966
    :cond_1
    sget-object v0, LX/0q5;->d:LX/0q5;

    return-object v0

    .line 146967
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 146968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/03V;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 146950
    const/4 v1, 0x0

    :try_start_0
    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {p1, p2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 146951
    :goto_0
    if-nez v1, :cond_0

    .line 146952
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FbTrafficStats_missingMethod_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Method not found."

    invoke-virtual {p0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146953
    :goto_1
    return-object v0

    :catch_0
    move-object v1, v0

    goto :goto_0

    .line 146954
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    move-object v0, v1

    .line 146955
    goto :goto_1
.end method

.method public static a(Ljava/lang/RuntimeException;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "RethrownThrowableArgument"
        }
    .end annotation

    .prologue
    .line 147006
    invoke-virtual {p0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Landroid/os/DeadObjectException;

    if-eqz v0, :cond_0

    .line 147007
    const-string v0, "FbTrafficStats"

    const-string v1, "netstats connection lost"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 147008
    :cond_0
    throw p0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 146945
    iget-object v0, p0, LX/0q5;->c:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 146946
    invoke-static {}, LX/0qB;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    iput-object v0, p0, LX/0q5;->c:LX/03R;

    .line 146947
    :cond_0
    iget-object v0, p0, LX/0q5;->c:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 146948
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 146949
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 146935
    invoke-direct {p0}, LX/0q5;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146936
    :try_start_0
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/0qB;->a(III)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, p2, v1}, LX/0qB;->a(III)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b(Lcom/facebook/device/resourcemonitor/DataUsageBytes;)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v0

    move-object v0, v0
    :try_end_0
    .catch LX/49U; {:try_start_0 .. :try_end_0} :catch_0

    .line 146937
    :goto_0
    return-object v0

    .line 146938
    :catch_0
    new-instance v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    invoke-direct {v0, v6, v7, v6, v7}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;-><init>(JJ)V

    goto :goto_0

    .line 146939
    :cond_0
    :try_start_1
    invoke-static {p1}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v2

    .line 146940
    invoke-static {p1}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v4

    .line 146941
    new-instance v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;-><init>(JJ)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 146942
    :catch_1
    move-exception v0

    .line 146943
    invoke-static {v0}, LX/0q5;->a(Ljava/lang/RuntimeException;)V

    .line 146944
    new-instance v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    invoke-direct {v0, v6, v7, v6, v7}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;-><init>(JJ)V

    goto :goto_0
.end method

.method public final b(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    .locals 3

    .prologue
    .line 146923
    invoke-direct {p0}, LX/0q5;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146924
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p1, p2, v0}, LX/0qB;->a(III)Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    :try_end_0
    .catch LX/49U; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 146925
    :goto_0
    return-object v0

    .line 146926
    :catch_0
    move-exception v0

    .line 146927
    const-string v1, "FbTrafficStats"

    const-string v2, "Unable to parse data usage from system file"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 146928
    :cond_0
    sget-object v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->a:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    goto :goto_0
.end method

.method public final c(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    .locals 3

    .prologue
    .line 146929
    invoke-direct {p0}, LX/0q5;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146930
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, p2, v0}, LX/0qB;->a(III)Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    :try_end_0
    .catch LX/49U; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 146931
    :goto_0
    return-object v0

    .line 146932
    :catch_0
    move-exception v0

    .line 146933
    const-string v1, "FbTrafficStats"

    const-string v2, "Unable to parse data usage from system file"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 146934
    :cond_0
    sget-object v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->a:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    goto :goto_0
.end method
