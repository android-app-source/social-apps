.class public LX/16R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/16R;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:LX/16P;


# direct methods
.method public constructor <init>(ILX/16P;)V
    .locals 1

    .prologue
    .line 185246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185247
    iput p1, p0, LX/16R;->a:I

    .line 185248
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16P;

    iput-object v0, p0, LX/16R;->b:LX/16P;

    .line 185249
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185233
    iget-object v0, p0, LX/16R;->b:LX/16P;

    iget-object v0, v0, LX/16P;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 185240
    check-cast p1, LX/16R;

    .line 185241
    if-nez p1, :cond_0

    .line 185242
    const/4 v0, -0x1

    .line 185243
    :goto_0
    return v0

    .line 185244
    :cond_0
    sget-object v0, LX/3lo;->a:LX/3lo;

    move-object v0, v0

    .line 185245
    iget v1, p0, LX/16R;->a:I

    iget v2, p1, LX/16R;->a:I

    invoke-virtual {v0, v1, v2}, LX/3lo;->a(II)LX/3lo;

    move-result-object v0

    invoke-virtual {p0}, LX/16R;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/16R;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/3lo;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/3lo;

    move-result-object v0

    invoke-virtual {v0}, LX/3lo;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 185236
    instance-of v1, p1, LX/16R;

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 185237
    :cond_0
    :goto_0
    return v0

    .line 185238
    :cond_1
    check-cast p1, LX/16R;

    .line 185239
    iget v1, p0, LX/16R;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, LX/16R;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/16R;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/16R;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 185235
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, LX/16R;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, LX/16R;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185234
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "rank"

    iget v2, p0, LX/16R;->a:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "LazyInterstitialControllerHolder"

    iget-object v2, p0, LX/16R;->b:LX/16P;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
