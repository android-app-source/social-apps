.class public LX/1kL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ry;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axw;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AyR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0ad;

.field public final e:LX/0SG;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Ljava/util/concurrent/Executor;

.field public final h:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation
.end field

.field private final k:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/io/Serializable;",
            ">;",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0SG;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Axw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AyR;",
            ">;",
            "LX/0ad;",
            "LX/0SG;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309416
    new-instance v0, LX/1kM;

    invoke-direct {v0, p0}, LX/1kM;-><init>(LX/1kL;)V

    iput-object v0, p0, LX/1kL;->j:LX/0Vd;

    .line 309417
    new-instance v0, LX/1kN;

    invoke-direct {v0, p0}, LX/1kN;-><init>(LX/1kL;)V

    iput-object v0, p0, LX/1kL;->k:LX/0QK;

    .line 309418
    iput-object p1, p0, LX/1kL;->a:LX/0Ot;

    .line 309419
    iput-object p2, p0, LX/1kL;->b:LX/0Ot;

    .line 309420
    iput-object p3, p0, LX/1kL;->c:LX/0Ot;

    .line 309421
    iput-object p4, p0, LX/1kL;->d:LX/0ad;

    .line 309422
    iput-object p5, p0, LX/1kL;->e:LX/0SG;

    .line 309423
    iput-object p6, p0, LX/1kL;->f:Ljava/util/concurrent/ExecutorService;

    .line 309424
    iput-object p7, p0, LX/1kL;->g:Ljava/util/concurrent/Executor;

    .line 309425
    iput-object p8, p0, LX/1kL;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 309426
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309427
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/1kL;->i:LX/0Px;

    if-eqz v0, :cond_1

    .line 309428
    iget-object v0, p0, LX/1kL;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x930001

    const/16 v2, 0x19

    const-string v3, "fetcher"

    const-class v4, LX/1kL;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;Ljava/lang/String;)V

    .line 309429
    iget-object v0, p0, LX/1kL;->i:LX/0Px;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 309430
    :cond_0
    :goto_0
    return-object v0

    .line 309431
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/1kL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    invoke-virtual {v0}, LX/Axt;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v1, v0

    .line 309432
    :goto_1
    iget-object v0, p0, LX/1kL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axw;

    .line 309433
    iget-object v2, v0, LX/Axw;->a:LX/0TD;

    new-instance v3, LX/Axu;

    invoke-direct {v3, v0}, LX/Axu;-><init>(LX/Axw;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 309434
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    invoke-static {v2}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 309435
    iget-object v1, p0, LX/1kL;->k:LX/0QK;

    iget-object v2, p0, LX/1kL;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 309436
    if-nez p1, :cond_0

    .line 309437
    iget-object v1, p0, LX/1kL;->j:LX/0Vd;

    iget-object v2, p0, LX/1kL;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 309438
    :cond_2
    iget-object v0, p0, LX/1kL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    .line 309439
    iget-object v1, v0, LX/Axt;->p:Lcom/google/common/util/concurrent/SettableFuture;

    if-nez v1, :cond_3

    invoke-virtual {v0}, LX/Axt;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    :goto_2
    move-object v0, v1

    .line 309440
    move-object v1, v0

    goto :goto_1

    :cond_3
    iget-object v1, v0, LX/Axt;->p:Lcom/google/common/util/concurrent/SettableFuture;

    goto :goto_2
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309441
    const-class v0, LX/Ayb;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309442
    const/4 v0, 0x0

    iput-object v0, p0, LX/1kL;->i:LX/0Px;

    .line 309443
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 309444
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 309445
    iget-object v0, p0, LX/1kL;->d:LX/0ad;

    sget-short v1, LX/1kO;->I:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309446
    const-class v0, LX/Ayb;

    return-object v0
.end method
