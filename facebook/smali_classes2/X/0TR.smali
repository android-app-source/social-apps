.class public LX/0TR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0TR;


# instance fields
.field public final a:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/facebook/common/executors/ConstrainedExecutorsStatusController$ExecutorsStatusListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 63027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63028
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0TR;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 63029
    return-void
.end method

.method public static a(LX/0QB;)LX/0TR;
    .locals 3

    .prologue
    .line 63015
    sget-object v0, LX/0TR;->b:LX/0TR;

    if-nez v0, :cond_1

    .line 63016
    const-class v1, LX/0TR;

    monitor-enter v1

    .line 63017
    :try_start_0
    sget-object v0, LX/0TR;->b:LX/0TR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 63018
    if-eqz v2, :cond_0

    .line 63019
    :try_start_1
    new-instance v0, LX/0TR;

    invoke-direct {v0}, LX/0TR;-><init>()V

    .line 63020
    move-object v0, v0

    .line 63021
    sput-object v0, LX/0TR;->b:LX/0TR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63022
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 63023
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63024
    :cond_1
    sget-object v0, LX/0TR;->b:LX/0TR;

    return-object v0

    .line 63025
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 63026
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a()Z
    .locals 4

    .prologue
    .line 63008
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x3f40624de0000000L    # 5.000000237487257E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0TS;Ljava/lang/String;JZ)V
    .locals 9

    .prologue
    .line 63009
    invoke-virtual {p1}, LX/0TS;->a()I

    move-result v7

    .line 63010
    iget v0, p1, LX/0TS;->e:I

    move v2, v0

    .line 63011
    iget-object v0, p0, LX/0TR;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2EX;

    .line 63012
    iget-object v1, p1, LX/0TS;->c:Ljava/lang/String;

    move-object v1, v1

    .line 63013
    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v0 .. v7}, LX/2EX;->a(Ljava/lang/String;ILjava/lang/String;JZI)V

    goto :goto_0

    .line 63014
    :cond_0
    return-void
.end method
