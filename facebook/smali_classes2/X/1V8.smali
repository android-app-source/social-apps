.class public abstract LX/1V8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/14w;


# direct methods
.method public constructor <init>(LX/14w;)V
    .locals 1

    .prologue
    .line 257394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257395
    invoke-virtual {p0}, LX/1V8;->a()Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, LX/1V8;->a:Ljava/util/EnumMap;

    .line 257396
    iput-object p1, p0, LX/1V8;->b:LX/14w;

    .line 257397
    return-void
.end method


# virtual methods
.method public final a(LX/1Ua;Lcom/facebook/feed/rows/core/props/FeedProps;F)I
    .locals 3
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ua;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;F)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 257380
    invoke-virtual {p1}, LX/1Ua;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257381
    :goto_0
    return v1

    .line 257382
    :cond_0
    if-eqz p2, :cond_2

    .line 257383
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257384
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 257385
    :goto_1
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1V8;->b:LX/14w;

    invoke-virtual {v0, p2}, LX/14w;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 257386
    :goto_2
    invoke-virtual {p0, p1}, LX/1V8;->a(LX/1Ua;)LX/1Ub;

    move-result-object v2

    .line 257387
    iget-object p0, v2, LX/1Ub;->d:LX/1Uc;

    move-object v2, p0

    .line 257388
    invoke-interface {v2, v0}, LX/1Uc;->a(I)F

    move-result v0

    mul-float/2addr v0, p3

    .line 257389
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 257390
    if-ltz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    move v1, v0

    .line 257391
    goto :goto_0

    .line 257392
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 257393
    goto :goto_2
.end method

.method public final a(LX/1Ua;)LX/1Ub;
    .locals 2

    .prologue
    .line 257379
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/1V8;->a(LX/1Ua;LX/1X9;I)LX/1Ub;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/1Ua;LX/1X9;I)LX/1Ub;
.end method

.method public abstract a()Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(LX/1Ua;LX/1X9;I)LX/1Ub;
.end method

.method public b()Ljava/util/EnumMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257378
    iget-object v0, p0, LX/1V8;->a:Ljava/util/EnumMap;

    return-object v0
.end method
