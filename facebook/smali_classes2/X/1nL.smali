.class public LX/1nL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/1nJ;


# direct methods
.method public constructor <init>(LX/0ad;LX/1nJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 316083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316084
    iput-object p1, p0, LX/1nL;->a:LX/0ad;

    .line 316085
    iput-object p2, p0, LX/1nL;->b:LX/1nJ;

    .line 316086
    return-void
.end method

.method public static b(LX/0QB;)LX/1nL;
    .locals 3

    .prologue
    .line 316081
    new-instance v2, LX/1nL;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/1nJ;->a(LX/0QB;)LX/1nJ;

    move-result-object v1

    check-cast v1, LX/1nJ;

    invoke-direct {v2, v0, v1}, LX/1nL;-><init>(LX/0ad;LX/1nJ;)V

    .line 316082
    return-object v2
.end method


# virtual methods
.method public final a(LX/8qC;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 316087
    new-instance v0, LX/8qO;

    invoke-direct {v0}, LX/8qO;-><init>()V

    invoke-virtual {v0}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, LX/1nL;->a(LX/8qC;Landroid/content/Context;Lcom/facebook/ufiservices/flyout/PopoverParams;Z)V

    .line 316088
    return-void
.end method

.method public final a(LX/8qC;Landroid/content/Context;Lcom/facebook/ufiservices/flyout/PopoverParams;Z)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 316046
    iget-object v0, p0, LX/1nL;->b:LX/1nJ;

    .line 316047
    iget-boolean v1, v0, LX/1nJ;->a:Z

    move v0, v1

    .line 316048
    if-eqz v0, :cond_0

    .line 316049
    :goto_0
    return-void

    .line 316050
    :cond_0
    const-class v0, LX/0ew;

    invoke-static {p2, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 316051
    const-class v1, Landroid/app/Activity;

    invoke-static {p2, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 316052
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 316053
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 316054
    iget-boolean v2, p3, Lcom/facebook/ufiservices/flyout/PopoverParams;->a:Z

    move v5, v2

    .line 316055
    iget-object v2, p0, LX/1nL;->a:LX/0ad;

    sget-char v3, LX/8rG;->a:C

    const-string v6, "CONTROL"

    invoke-interface {v2, v3, v6}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 316056
    const-string v3, "CONTROL"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 316057
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    if-eqz v5, :cond_3

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    :goto_1
    if-eqz v5, :cond_1

    invoke-static {p2}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v4

    :cond_1
    move-object v0, p2

    move-object v1, p1

    move v5, p4

    .line 316058
    new-instance v6, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-direct {v6}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;-><init>()V

    .line 316059
    iput-object v1, v6, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    .line 316060
    move-object v6, v6

    .line 316061
    invoke-virtual {v6, v2, v3, v4, v5}, Lcom/facebook/widget/popover/PopoverFragment;->a(LX/0gc;Landroid/view/Window;Landroid/view/View;Z)V

    .line 316062
    new-instance v6, LX/8th;

    invoke-direct {v6, v0}, LX/8th;-><init>(Landroid/content/Context;)V

    .line 316063
    iget-object v0, v6, LX/8th;->a:LX/1nJ;

    move-object v6, v0

    .line 316064
    if-eqz v6, :cond_2

    .line 316065
    invoke-virtual {v6}, LX/1nJ;->b()V

    .line 316066
    :cond_2
    goto :goto_0

    :cond_3
    move-object v3, v4

    goto :goto_1

    .line 316067
    :cond_4
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "VIEW_ANIMATION"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 316068
    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v2

    if-nez v2, :cond_6

    .line 316069
    sget-object v2, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->s:Ljava/lang/String;

    const-string v3, "Unsafe to commit stateful transactions."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 316070
    :cond_5
    :goto_2
    goto :goto_0

    .line 316071
    :cond_6
    new-instance v2, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;

    invoke-direct {v2}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;-><init>()V

    .line 316072
    iput-object p1, v2, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->t:LX/8qC;

    .line 316073
    move-object v2, v2

    .line 316074
    const/4 v3, 0x2

    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 316075
    iput-boolean v1, v2, Lcom/facebook/widget/popover/SimplePopoverFragment;->n:Z

    .line 316076
    const-string v3, "chromeless:content:fragment:tag"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 316077
    new-instance v2, LX/8th;

    invoke-direct {v2, p2}, LX/8th;-><init>(Landroid/content/Context;)V

    .line 316078
    iget-object v3, v2, LX/8th;->a:LX/1nJ;

    move-object v2, v3

    .line 316079
    if-eqz v2, :cond_5

    .line 316080
    invoke-virtual {v2}, LX/1nJ;->b()V

    goto :goto_2
.end method
