.class public LX/15Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/15Z;


# instance fields
.field public a:Z

.field public b:LX/0T2;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180870
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/15Z;->a:Z

    .line 180871
    return-void
.end method

.method public static a(LX/0QB;)LX/15Z;
    .locals 3

    .prologue
    .line 180872
    sget-object v0, LX/15Z;->c:LX/15Z;

    if-nez v0, :cond_1

    .line 180873
    const-class v1, LX/15Z;

    monitor-enter v1

    .line 180874
    :try_start_0
    sget-object v0, LX/15Z;->c:LX/15Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180875
    if-eqz v2, :cond_0

    .line 180876
    :try_start_1
    new-instance v0, LX/15Z;

    invoke-direct {v0}, LX/15Z;-><init>()V

    .line 180877
    move-object v0, v0

    .line 180878
    sput-object v0, LX/15Z;->c:LX/15Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180879
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180880
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180881
    :cond_1
    sget-object v0, LX/15Z;->c:LX/15Z;

    return-object v0

    .line 180882
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0T2;)V
    .locals 0

    .prologue
    .line 180884
    iput-object p1, p0, LX/15Z;->b:LX/0T2;

    .line 180885
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 180886
    iget-boolean v0, p0, LX/15Z;->a:Z

    return v0
.end method
