.class public abstract LX/0P1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Map;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/util/Map",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static final EMPTY_ENTRY_ARRAY:[Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Map$Entry",
            "<**>;"
        }
    .end annotation
.end field


# instance fields
.field private transient entrySet:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field private transient keySet:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<TK;>;"
        }
    .end annotation
.end field

.field private transient multimapView:LX/2Q6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Q6",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private transient values:LX/0Py;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Py",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54869
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/util/Map$Entry;

    sput-object v0, LX/0P1;->EMPTY_ENTRY_ARRAY:[Ljava/util/Map$Entry;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()LX/0P2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0P2",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54834
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    return-object v0
.end method

.method public static checkNoConflict(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/Map$Entry",
            "<**>;",
            "Ljava/util/Map$Entry",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 54835
    if-nez p0, :cond_0

    .line 54836
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Multiple entries with same "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54837
    :cond_0
    return-void
.end method

.method public static copyOf(Ljava/lang/Iterable;)LX/0P1;
    .locals 2
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;>;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54838
    sget-object v0, LX/0P1;->EMPTY_ENTRY_ARRAY:[Ljava/util/Map$Entry;

    invoke-static {p0, v0}, LX/0Ph;->a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    check-cast v0, [Ljava/util/Map$Entry;

    .line 54839
    array-length v1, v0

    packed-switch v1, :pswitch_data_0

    .line 54840
    invoke-static {v0}, LX/0PA;->a([Ljava/util/Map$Entry;)LX/0PA;

    move-result-object v0

    :goto_0
    return-object v0

    .line 54841
    :pswitch_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 54842
    goto :goto_0

    .line 54843
    :pswitch_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 54844
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static copyOf(Ljava/util/Map;)LX/0P1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54845
    instance-of v0, p0, LX/0P1;

    if-eqz v0, :cond_0

    instance-of v0, p0, LX/146;

    if-nez v0, :cond_0

    move-object v0, p0

    .line 54846
    check-cast v0, LX/0P1;

    .line 54847
    invoke-virtual {v0}, LX/0P1;->isPartialView()Z

    move-result v1

    if-nez v1, :cond_1

    .line 54848
    :goto_0
    return-object v0

    .line 54849
    :cond_0
    instance-of v0, p0, Ljava/util/EnumMap;

    if-eqz v0, :cond_1

    .line 54850
    check-cast p0, Ljava/util/EnumMap;

    invoke-static {p0}, LX/0P1;->copyOfEnumMap(Ljava/util/EnumMap;)LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 54851
    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/lang/Iterable;)LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method private static copyOfEnumMap(Ljava/util/EnumMap;)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Enum",
            "<TK;>;V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/EnumMap",
            "<TK;+TV;>;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54852
    new-instance v1, Ljava/util/EnumMap;

    invoke-direct {v1, p0}, Ljava/util/EnumMap;-><init>(Ljava/util/EnumMap;)V

    .line 54853
    invoke-virtual {v1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 54854
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 54855
    :cond_0
    invoke-virtual {v1}, Ljava/util/EnumMap;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 54856
    new-instance v0, LX/4y6;

    invoke-direct {v0, v1}, LX/4y6;-><init>(Ljava/util/EnumMap;)V

    :goto_1
    move-object v0, v0

    .line 54857
    return-object v0

    .line 54858
    :pswitch_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 54859
    goto :goto_1

    .line 54860
    :pswitch_1
    invoke-virtual {v1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 54861
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;)",
            "LX/0P3",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54862
    new-instance v0, LX/0P3;

    invoke-direct {v0, p0, p1}, LX/0P3;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static of()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54863
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 54864
    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54865
    invoke-static {p0, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;TK;TV;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54866
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Map$Entry;

    const/4 v1, 0x0

    invoke-static {p0, p1}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0PA;->a([Ljava/util/Map$Entry;)LX/0PA;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;TK;TV;TK;TV;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54867
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/util/Map$Entry;

    const/4 v1, 0x0

    invoke-static {p0, p1}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p4, p5}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0PA;->a([Ljava/util/Map$Entry;)LX/0PA;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;TK;TV;TK;TV;TK;TV;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54868
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/util/Map$Entry;

    const/4 v1, 0x0

    invoke-static {p0, p1}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p4, p5}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p6, p7}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0PA;->a([Ljava/util/Map$Entry;)LX/0PA;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;TK;TV;TK;TV;TK;TV;TK;TV;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54829
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/util/Map$Entry;

    const/4 v1, 0x0

    invoke-static {p0, p1}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2, p3}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p4, p5}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p6, p7}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {p8, p9}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0PA;->a([Ljava/util/Map$Entry;)LX/0PA;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public asMultimap()LX/2Q6;
    .locals 4
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2Q6",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54870
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54871
    sget-object v0, LX/4xU;->a:LX/4xU;

    move-object v0, v0

    .line 54872
    :cond_0
    :goto_0
    return-object v0

    .line 54873
    :cond_1
    iget-object v0, p0, LX/0P1;->multimapView:LX/2Q6;

    .line 54874
    if-nez v0, :cond_0

    new-instance v0, LX/2Q6;

    new-instance v1, LX/4yE;

    invoke-direct {v1, p0}, LX/4yE;-><init>(LX/0P1;)V

    invoke-virtual {p0}, LX/0P1;->size()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/2Q6;-><init>(LX/0P1;ILjava/util/Comparator;)V

    iput-object v0, p0, LX/0P1;->multimapView:LX/2Q6;

    goto :goto_0
.end method

.method public final clear()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 54875
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54876
    invoke-virtual {p0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54877
    invoke-virtual {p0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Py;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract createEntrySet()LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end method

.method public createKeySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 54830
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54831
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 54832
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/12l;

    invoke-direct {v0, p0}, LX/12l;-><init>(LX/0P1;)V

    goto :goto_0
.end method

.method public entrySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 54802
    iget-object v0, p0, LX/0P1;->entrySet:LX/0Rf;

    .line 54803
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0P1;->createEntrySet()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0P1;->entrySet:LX/0Rf;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54804
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54805
    if-ne p0, p1, :cond_0

    .line 54806
    const/4 v0, 0x1

    .line 54807
    :goto_0
    move v0, v0

    .line 54808
    return v0

    .line 54809
    :cond_0
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 54810
    check-cast p1, Ljava/util/Map;

    .line 54811
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 54812
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 54813
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-static {v0}, LX/0RA;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 54814
    invoke-virtual {p0}, LX/0P1;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHashCodeFast()Z
    .locals 1

    .prologue
    .line 54815
    const/4 v0, 0x0

    return v0
.end method

.method public abstract isPartialView()Z
.end method

.method public keyIterator()LX/0Rc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 54816
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    .line 54817
    new-instance v1, LX/12r;

    invoke-direct {v1, p0, v0}, LX/12r;-><init>(LX/0P1;LX/0Rc;)V

    return-object v1
.end method

.method public keySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 54818
    iget-object v0, p0, LX/0P1;->keySet:LX/0Rf;

    .line 54819
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0P1;->createKeySet()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0P1;->keySet:LX/0Rf;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54820
    invoke-virtual {p0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 54821
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 54822
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 54823
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54824
    invoke-static {p0}, LX/0PM;->c(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public values()LX/0Py;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 54825
    iget-object v0, p0, LX/0P1;->values:LX/0Py;

    .line 54826
    if-nez v0, :cond_0

    new-instance v0, LX/1kc;

    invoke-direct {v0, p0}, LX/1kc;-><init>(LX/0P1;)V

    iput-object v0, p0, LX/0P1;->values:LX/0Py;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 54827
    invoke-virtual {p0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54828
    new-instance v0, LX/4y2;

    invoke-direct {v0, p0}, LX/4y2;-><init>(LX/0P1;)V

    return-object v0
.end method
