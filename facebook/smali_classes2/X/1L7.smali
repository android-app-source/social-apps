.class public LX/1L7;
.super LX/1L8;
.source ""

# interfaces
.implements LX/1L9;
.implements LX/0hk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1L8;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/0hk;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/967;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0bH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233520
    invoke-direct {p0}, LX/1L8;-><init>()V

    .line 233521
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 233522
    iput-object v0, p0, LX/1L7;->a:LX/0Ot;

    .line 233523
    return-void
.end method

.method private c(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 233536
    iget-object v0, p0, LX/1L7;->c:LX/1Iu;

    .line 233537
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 233538
    if-eqz v0, :cond_0

    .line 233539
    iget-object v0, p0, LX/1L7;->c:LX/1Iu;

    .line 233540
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 233541
    check-cast v0, LX/0qq;

    invoke-virtual {v0, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 233542
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 233533
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 233534
    invoke-direct {p0, p1}, LX/1L7;->c(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 233535
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 233530
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 233531
    invoke-direct {p0, p1}, LX/1L7;->c(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 233532
    return-void
.end method

.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 233543
    check-cast p1, LX/1Za;

    .line 233544
    iget-object v2, p1, LX/1Za;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 233545
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 233546
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    .line 233547
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 233548
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 233549
    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 233550
    :cond_0
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 233551
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 233552
    iget-object v0, p0, LX/1L7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/967;

    iget-object v1, p1, LX/1Za;->b:Ljava/lang/String;

    const-string v3, "newsfeed_page_like"

    const-string v4, "native_newsfeed"

    const-string v5, "newsfeed_ufi"

    iget-object v6, p1, LX/1Za;->d:Ljava/lang/String;

    const/4 v7, 0x0

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, LX/967;->a(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/1L9;)V

    .line 233553
    :cond_1
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 233529
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 233527
    iget-object v0, p0, LX/1L7;->b:LX/0bH;

    invoke-virtual {v0, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 233528
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 233526
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 233524
    iget-object v0, p0, LX/1L7;->b:LX/0bH;

    invoke-virtual {v0, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 233525
    return-void
.end method
